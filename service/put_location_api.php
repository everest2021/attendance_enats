<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$file = '/var/www/html/attendance_gml/service/service.txt';
//$file = 'C:\xampp\htdocs\attendance_jml\service/service.txt';
$handle = fopen($file, 'a+'); 
// $message = 'tdcsfas';
// fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($message, true)  . "\n");
//fclose($handle); 
//echo 'aaaa';exit;
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Punchdataapi = new Punchdataapi();

$value = $Punchdataapi->putorder($datas, $handle);
fclose($handle); 
exit(json_encode($value));
class Punchdataapi {
  	public $conn;

  	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
  	}

  	public function escape($value, $conn){
		return $conn->real_escape_string($value);
	}

  	public function getLastId($conn){
		return $conn->insert_id;
	}

	public function query($sql, $conn) {
		$query = $conn->query($sql);

		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();

				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}

				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;

				unset($data);

				$query->close();

				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

  	public function putorder($data = array(), $handle){
		fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($data, true)  . "\n");

		if(!isset($data['lat'])){
	    	$lat = '';//'111';
		} else {
			$lat = $data['lat'];
		}

		if(!isset($data['lon'])){
	    	$lon = '';//'2222';
		} else {
			$lon = $data['lon'];
		}

		if(!isset($data['currentDateisostring'])){
	    	$currentDateisostring = '';
			$date = '';//'2018-08-24';
			$time = '';//'09:52:55';
		} else {
			$currentDateisostring = $data['currentDateisostring'];
			$date = date('Y-m-d', strtotime($currentDateisostring));
			$time = date('H:i:s', strtotime($currentDateisostring));
		}

		if(!isset($data['emp_id'])){
	    	$emp_id = '';//'1000';
		} else {
			$emp_id = $data['emp_id'];
		}

		$tran_datas = $this->query("SELECT * FROM `oc_transaction` WHERE  `emp_id` = '".$emp_id."' AND `date` = '".$date."' ", $this->conn);
		if($tran_datas->num_rows != 0){
			$tran_data = $tran_datas->row;
			$last_data_sql = "SELECT * FROM `oc_transaction_punches` WHERE `emp_id` = '".$emp_id."' AND `date` = '".$date."' ";
			$last_datas = $this->query($last_data_sql, $this->conn);
			if($last_datas->num_rows > 0){
				$first = 0;
				$last_data = $last_datas->row;
				$last_date_time = $last_data['date'].' '.$last_data['time'];
			} else {
				$first = 1;
				$last_data = $last_datas->row;
				$last_date_time = $date.' '.$time;
			}
			$in = 0;
			if($first == 1){
				$in = 1;
			} else {
				$current_date_time = $date.' '.$time;
				$start_date = new DateTime($last_date_time);
				$since_start = $start_date->diff(new DateTime($current_date_time));
				if(($since_start->h == 0 && $since_start->i >= 1) || $since_start->h > 1){
					$in = 1;	
				}
			}
			if($in == 1){
				$insert_sql = "INSERT INTO `oc_transaction_punches` SET 
								`emp_id` = '".$emp_id."',
								`date` = '".$date."',
								`time` = '".$time."',
								`lat` = '".$lat."',
								`lon` = '".$lon."' ";
				$this->query($insert_sql, $this->conn);
			}
			$result = array();
			$result['success'] = 1;
			return $result;
		} else {
			$result['success'] = 0;
    		return $result;
    	}
	}
}
?>