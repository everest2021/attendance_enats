<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "db_attendance_econ";

$file = 'C:\xampp\htdocs\attendance_econ\system/logs/service.txt';
$handle = fopen($file, 'a+'); 
$message = 'in process';
fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($message, true)  . "\n");
//fclose($handle); 
//echo 'aaaa';exit;

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}

function getLastId($conn){
	return $conn->insert_id;
}

function hoursToMinutes($hours) {
	if (strstr($hours, ':')){
		# Split hours and minutes.
		$separatedData = split(':', $hours);
		$minutesInHours    = $separatedData[0] * 60;
		$minutesInDecimals = $separatedData[1];
		$totalMinutes = $minutesInHours + $minutesInDecimals;
	} else {
		$totalMinutes = $hours * 60;
	}
	//echo $totalMinutes;exit;
	return $totalMinutes;
}

function query($sql, $conn) {
	$query = $conn->query($sql);

	if (!$conn->errno){
		if (isset($query->num_rows)) {
			$data = array();

			while ($row = $query->fetch_assoc()) {
				$data[] = $row;
			}

			$result = new stdClass();
			$result->num_rows = $query->num_rows;
			$result->row = isset($data[0]) ? $data[0] : array();
			$result->rows = $data;

			unset($data);

			$query->close();

			return $result;
		} else{
			return true;
		}
	} else {
		throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
		exit();
	}
}

//Data Process Empty
$current_date = date('Y-m-d');
$start_date = '2017-12-01';//date('Y-m-d', strtotime($current_date . ' -60 day'));//'2017-06-26';
$end_date = date('Y-m-d', strtotime($current_date . ' +1 day'));
//$end_date = '2018-01-31';
$day = array();
$days = GetDays($start_date, $end_date);
foreach ($days as $dkey => $dvalue) {
	$dates = explode('-', $dvalue);
	$day[$dkey]['day'] = $dates[2];
	$day[$dkey]['date'] = $dvalue;
}
$batch_id = '0';
foreach($day as $dkeys => $dvalues){
	$filter_date_start = $dvalues['date'];
	//echo $filter_date_start;exit;
	$results = getemployees_empty($dvalues['date'], $conn);
	foreach ($results as $rkey => $rvalue) {
		if(isset($rvalue['name']) && $rvalue['name'] != ''){
			$emp_name = $rvalue['name'];
			$day_date = date('j', strtotime($filter_date_start));
			$month = date('n', strtotime($filter_date_start));
			$year = date('Y', strtotime($filter_date_start));

			$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
			//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvalue['emp_code']."' ";
			$shift_schedule = query($update3, $conn)->row;
			$schedule_raw = explode('_', $shift_schedule[$day_date]);
			if(!isset($schedule_raw[2])){
				$schedule_raw[2]= 1;
			}
			// echo '<pre>';
			// print_r($schedule_raw);
			// exit;
			if($schedule_raw[0] == 'S'){
				$shift_data = getshiftdata($schedule_raw[1], $conn);
				if(isset($shift_data['shift_id'])){
					$shift_intime = $shift_data['in_time'];
					$shift_outtime = $shift_data['out_time'];
					$shift_outtime_flexi = Date('H:i:s', strtotime($shift_data['out_time'] .' +60 minutes'));
					$shift_id = $shift_data['shift_id'];
					$shift_code = $shift_data['shift_code'];
					
					$day = date('j', strtotime($filter_date_start));
					$month = date('n', strtotime($filter_date_start));
					$year = date('Y', strtotime($filter_date_start));

					$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
					$trans_exist = query($trans_exist_sql, $conn);
					if($trans_exist->num_rows == 0){
						$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."' ";
						//echo $sql.';';
						//echo '<br />';
						query($sql, $conn);
					}
				} else {
					$shift_data = getshiftdata('1', $conn);
					$shift_intime = $shift_data['in_time'];
					$shift_outtime = $shift_data['out_time'];
					$shift_outtime_flexi = Date('H:i:s', strtotime($shift_data['out_time'] .' +60 minutes'));
					$shift_id = $shift_data['shift_id'];
					$shift_code = $shift_data['shift_code'];
					
					$day = date('j', strtotime($filter_date_start));
					$month = date('n', strtotime($filter_date_start));
					$year = date('Y', strtotime($filter_date_start));

					$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
					$trans_exist = query($trans_exist_sql, $conn);
					if($trans_exist->num_rows == 0){
						$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."' ";
						//echo $sql.';';
						//echo '<br />';
						query($sql, $conn);
					}
				}
			} elseif ($schedule_raw[0] == 'W') {
				$shift_data = getshiftdata($schedule_raw[2], $conn);
				if(!isset($shift_data['shift_id'])){
					$shift_data = getshiftdata('1', $conn);
				}
				$shift_intime = $shift_data['in_time'];
				$shift_outtime = $shift_data['out_time'];
				$shift_outtime_flexi = Date('H:i:s', strtotime($shift_data['out_time'] .' +60 minutes'));
				$shift_id = $shift_data['shift_id'];
				$shift_code = $shift_data['shift_code'];

				$act_intime = '00:00:00';
				$act_outtime = '00:00:00';
				$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
				$trans_exist = query($trans_exist_sql, $conn);
				
				if($trans_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."' ";
					//echo $sql.';';
					//echo '<br />';
					query($sql, $conn);
				}
			} elseif ($schedule_raw[0] == 'H') {
				$shift_data = getshiftdata($schedule_raw[2], $conn);
				if(!isset($shift_data['shift_id'])){
					$shift_data = getshiftdata('1', $conn);
				}
				$shift_intime = $shift_data['in_time'];
				$shift_outtime = $shift_data['out_time'];
				$shift_outtime_flexi = Date('H:i:s', strtotime($shift_data['out_time'] .' +60 minutes'));
				$shift_id = $shift_data['shift_id'];
				$shift_code = $shift_data['shift_code'];

				$act_intime = '00:00:00';
				$act_outtime = '00:00:00';
				
				$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
				$trans_exist = query($trans_exist_sql, $conn);
				if($trans_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."' ";
					//echo $sql.';';
					//echo '<br />';
					query($sql, $conn);
				}
			} elseif ($schedule_raw[0] == 'HD') {
				$shift_data = getshiftdata($schedule_raw[2], $conn);
				if(!isset($shift_data['shift_id'])){
					$shift_data = getshiftdata('1', $conn);
				}
				$shift_intime = $shift_data['in_time'];
				$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
				$shift_outtime_flexi = Date('H:i:s', strtotime($shift_outtime .' +60 minutes'));
				//$shift_outtime = Date('H:i:s', strtotime($shift_outtime .' +00 minutes'));
				$act_intime = '00:00:00';
				$act_outtime = '00:00:00';
				$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
				$trans_exist = query($trans_exist_sql, $conn);
				if($trans_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."',  `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."' ";
					//echo $sql.';';
					//echo '<br />';
					query($sql, $conn);
				}
			}
		}	
	}
}
$sql = "UPDATE `oc_employee` SET `is_new` = '1' ";
query($sql, $conn);

//Data Process
$batch_ids = query("SELECT `batch_id` FROM `oc_transaction` ORDER BY `batch_id` DESC LIMIT 1", $conn);
if($batch_ids->num_rows > 0){
	$batch_id = $batch_ids->row['batch_id'] + 1;
} else {
	$batch_id = 1;
}
$last_logs_ids = query("SELECT `log_id` FROM `oc_attendance` ORDER BY log_id DESC LIMIT 1 ", $conn);
if($last_logs_ids->num_rows > 0){
	$last_logs_id = $last_logs_ids->row['log_id'] + 1;
} else {
	$last_logs_id = 1;
}
//echo $last_logs_id;exit;


/*
$serverName = "ENCUBE090-PC";
//$connectionInfo = array("Database"=>"eBioserver", "UID"=>"sa", "PWD"=>"1234");
$connectionInfo = array("Database"=>"eBioserver");
$conn1 = sqlsrv_connect($serverName, $connectionInfo);
if($conn1) {
	 //echo "Connection established.<br />";
}else{
	 //echo "Connection could not be established.<br />";
	 $dat['connection'] = 'Connection could not be established';
	 //echo '<pre>';
	 //print_r(sqlsrv_errors());
	 //exit;
	 //die( print_r( sqlsrv_errors(), true));
}


$sql = "SELECT * FROM dbo.ENCUBE WHERE DeviceLogId >= '".$last_logs_id."' ";
$params = array();
$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
$stmt = sqlsrv_query($conn1, $sql, $params, $options);
if( $stmt === false) {
   //echo'<pre>';
   //print_r(sqlsrv_errors());
   //exit;
}
$exp_datas = array();
while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
	$user_id = $row['UserId'];
	$device_id = $row['DeviceId'];
	// $user_id = sprintf('%04d', $row['UserId']);
	// $device_id = sprintf('%02d', $row['DeviceId']);
	$log_id = $row['DeviceLogId'];
	$logdates = $row['LogDate'];
	if($logdates != null){	
		$logdate = $logdates->format("Y-m-d");
		$logtimes = $row['LogDate'];
		$logtime = $logtimes->format("H:i:s");
		$download_dates = $row['DownloadDate'];
		$download_date = $download_dates->format("Y-m-d");
		$exp_datas[] = array(
			'user_id' => $user_id,
			'log_id' => $log_id,
			'download_date' => $download_date,
			'date' => $logdate,
			'time' => $logtime,
			'device_id' => $device_id,
		);
	}
}
*/
// echo '<pre>';
// print_r($api_post_data);
// exit;
$url_curl = "http://103.246.243.78:8012/attendance_api/get_data.php";
$datas['last_logs_id'] = $last_logs_id;
$data = json_encode($datas);
//open connection
$ch = curl_init($url_curl);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
    'Content-Type: application/json',                                                                                
    'Content-Length: ' . strlen($data))                                                                       
); 
curl_setopt($ch,CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_TIMEOUT, 30);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//execute post
$result = curl_exec($ch);
//close connection
curl_close($ch);
//decode the result
$api_post_data = json_decode($result, true);
// echo '<pre>';
// var_dump($result);
// echo '<br />';
// echo '<pre>';
// print_r($api_post_data);
// exit;
if(!empty($api_post_data['exp_datas']) && isset($api_post_data['exp_datas'][0]['user_id'])){
	//echo 'in';exit;
	$exp_datas = $api_post_data['exp_datas'];
	$employees = array();
	$employees_final = array();
	foreach($exp_datas as $data) {
		//$tdata = trim($data);
		$emp_id  = $data['user_id'];//substr($tdata, 0, 5);
		$in_time = $data['time'];//substr($tdata, 6, 5);
		if($emp_id != '') {
			$result = getEmployees_dat($emp_id, $conn);
			// echo '<pre>';
			// print_r($result);
			// exit;
			if(isset($result['emp_code'])){
				$employees[] = array(
					'employee_id' => $result['emp_code'],
					'emp_name' => $result['name'],
					'card_id' => '',
					'in_time' => $in_time,
					'device_id' => $data['device_id'],
					'punch_date' => $data['date'],
					'download_date' => $data['download_date'],
					'log_id' => $data['log_id'],
					'fdate' => date('Y-m-d H:i:s', strtotime($data['date'].' '.$in_time))
				);
			} else {
				$employees[] = array(
					'employee_id' => $emp_id,
					'emp_name' => '',
					'card_id' => $emp_id,
					'in_time' => $in_time,
					'device_id' => $data['device_id'],
					'punch_date' => $data['date'],
					'download_date' => $data['download_date'],
					'log_id' => $data['log_id'],
					'fdate' => date('Y-m-d H:i:s', strtotime($data['date'].' '.$in_time))
				);
			}
		}
	}
	usort($employees, "sortByOrder");
	$o_emp = array();
	foreach ($employees as $ekey => $evalue) {
		$employees_final[] = $evalue;
	}

	// echo '<pre>';
	// print_r($employees_final);
	// exit;

	$filter_unit = 0;
	if(isset($employees_final) && count($employees_final) > 0){
		foreach ($employees_final as $fkey => $employee) {
			$exist = getorderhistory($employee, $conn);
			if($exist == 0){
				$mystring = $employee['in_time'];
				$findme   = ':';
				$pos = strpos($mystring, $findme);
				if($pos !== false){
					$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 3, 2). ":" . substr($employee['in_time'], 6, 2);
				} else {
					$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 2, 2). ":" . substr($employee['in_time'], 4, 2);
				}
				$employee['in_time'] = $in_times;
				insert_attendance_data($employee, $conn);
			}
		}
	}


	// $update2 = "SELECT `date` FROM `oc_transaction` WHERE `month_close_status` = '1' ORDER BY `date` DESC LIMIT 1";
	// $day_status = query($update2, $conn);
	// if($day_status->num_rows > 0){
	// 	$last_date = $day_status->row['date'];
	// 	$start_date = date('Y-m-d', strtotime($last_date . ' +1 day'));
	// } else {
	// 	$start_date = '2017-10-02';//date('Y-m-d');
	// }

	$start_date = '2017-12-01';//date('Y-m-d');
	$unprocessed_dates = query("SELECT a.`punch_date`, a.`emp_id`, a.`id`, a.`device_id` FROM `oc_attendance` a LEFT JOIN `oc_employee` e ON(e.`emp_code` = a.`emp_id`) WHERE e.`status` = '1' AND a.`status` = '0' AND a.`punch_date` >= '".$start_date."' ORDER BY a.`punch_date` ASC, a.`punch_time` ASC ", $conn);
	// echo '<pre>';
	// print_r($unprocessed_dates);
	// exit;
	$current_processed_data = array();
	foreach($unprocessed_dates->rows as $dkey => $dvalue){
		$current_processed_data[$dvalue['id']] = $dvalue['id'];
		$filter_date_start = $dvalue['punch_date'];
		$data['filter_name_id'] = $dvalue['emp_id']; 
		$rvalue = getempdata($dvalue['emp_id'], $dvalue['punch_date'], $conn);
		//$emp_salary = getempsalary($dvalue['emp_id'], $conn);
		$device_id = $dvalue['device_id'];
		
		$is_processed_status = query("SELECT `status` FROM `oc_attendance` WHERE `id` = '".$dvalue['id']."' ", $conn)->row['status'];	
		if(isset($rvalue['name']) && $rvalue['name'] != '' && $is_processed_status == '0'){
			$emp_name = $rvalue['name'];
			$department = $rvalue['department'];
			$unit = $rvalue['unit'];
			$group = '';

			$day_date = date('j', strtotime($filter_date_start));
			$day = date('j', strtotime($filter_date_start));
			$month = date('n', strtotime($filter_date_start));
			$year = date('Y', strtotime($filter_date_start));

			$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
			$shift_schedule = query($update3, $conn)->row;
			$schedule_raw = explode('_', $shift_schedule[$day_date]);
			if(!isset($schedule_raw[2])){
				$schedule_raw[2] = 1;
			}
			if($schedule_raw[0] == 'S'){
				$shift_data = getshiftdata($schedule_raw[1], $conn);
				if(!isset($shift_data['shift_id'])){
					$shift_data = getshiftdata('1', $conn);
				}
				$shift_intime = $shift_data['in_time'];
				$shift_outtime = $shift_data['out_time'];
				$shift_outtime_flexi = Date('H:i:s', strtotime($shift_data['out_time'] .' +60 minutes'));
				$shift_id = $shift_data['shift_id'];
				$shift_code = $shift_data['shift_code'];
				$shift_allowance = $shift_data['shift_allowance'];
				
				$act_shift_id = $shift_id;
				$act_shift_code = $shift_code;
				$act_shift_allowance = $shift_allowance;
				$act_shift_intime = $shift_intime;
				$act_shift_outtime = $shift_outtime;
				$act_shift_outtime_flexi = $shift_outtime_flexi;
				$act_shift_allowance = $shift_allowance;

				$act_intime = '00:00:00';
				$act_outtime = '00:00:00';
				$act_in_punch_date = $filter_date_start;
				$act_out_punch_date = $filter_date_start;
				$act_intimes = getrawattendance_in_time($rvalue['emp_code'], $dvalue['punch_date'], $shift_data, $conn);
				if(isset($act_intimes['punch_time'])) {
					$act_intime = $act_intimes['punch_time'];
					$act_in_punch_date = $act_intimes['punch_date'];
					/*
					$array = getshiftdata('0', $conn);
					$act_array = array();
					$act_array_min = array();
					$hour_array = array();
					$min_array = array();
					$time_array = array();
					$zero_hour_count = 0;
					foreach ($array as $akey => $avalue) {
						$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
						$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$avalue['in_time']));
						if($since_start->d == 0){
							$time_array[] = $since_start->h.':'.$since_start->i.':'.$since_start->s;
							$hour_array[] = $since_start->h;
							$act_array[] = $avalue;
							if($since_start->h == 0){
								$act_array_min[] = $avalue;
								$min_array[] = $since_start->i;
								$zero_hour_count ++;
							}
						}
					}
					if($zero_hour_count >= 2){
						$shift_work_hour = '0';
						if($min_array){
							$shift_work_hour = min($min_array);
						}
						$shift_worked_data = array();
						foreach ($min_array as $akey => $avalue) {
							if($avalue == $shift_work_hour){
								$shift_worked_data = $act_array_min[$akey];
							}
						}
					} else {
						$shift_work_hour = '0';
						if($hour_array){
							$shift_work_hour = min($hour_array);
						}
						$shift_worked_data = array();
						foreach ($hour_array as $akey => $avalue) {
							if($avalue == $shift_work_hour){
								$shift_worked_data = $act_array[$akey];
							}
						}	
					}
					if($shift_worked_data){
						$act_shift_intime = $shift_worked_data['in_time'];
						$act_shift_outtime = $shift_worked_data['out_time'];
						$act_shift_id = $shift_worked_data['shift_id'];
						$act_shift_code = $shift_worked_data['shift_code'];
						$act_shift_allowance = $shift_worked_data['shift_allowance'];
					}
					*/
				}
				$act_outtimes = getrawattendance_out_time($rvalue['emp_code'], $dvalue['punch_date'], $act_intime, $act_in_punch_date, $shift_data, $conn);
				if(isset($act_outtimes['punch_time'])) {
					$act_outtime = $act_outtimes['punch_time'];
					$act_out_punch_date = $act_outtimes['punch_date'];
				}
				if($act_intime == $act_outtime){
					$act_outtime = '00:00:00';
				}
				$abnormal_status = 0;
				$first_half = 0;
				$second_half = 0;
				$late_time = '00:00:00';
				$early_time = '00:00:00';
				$working_time = '00:00:00';
				$late_mark = 0;
				$early_mark = 0;
				$shift_early = 0;
				if($act_intime != '00:00:00'){
					$shift_intime_plus_one = Date('H:i:s', strtotime($act_shift_intime .' +60 minutes'));
					$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
					$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
					if($since_start->h > 12){
						$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
					}
					if($since_start->h > 0){
						$late_hour = $since_start->h;
						$late_min = $since_start->i;
						$late_sec = $since_start->s;
					} else {
						$late_hour = $since_start->h;
						$late_min = $since_start->i;
						$late_sec = $since_start->s;
					}
					$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
					if($since_start->invert == 1){
						$late_time = '00:00:00';
					} else {
						$late_mark = 1;
					}
					$shift_intime_minus_one = Date('H:i:s', strtotime($act_shift_intime .' -0 minutes'));
					$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
					$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
					if($since_start->h > 12){
						$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
					}
					if($since_start->invert == 1){
						$shift_early = 1;
					}
					$start_date = new DateTime($act_in_punch_date.' '.$act_shift_intime);
					$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_shift_outtime));
					$shift_working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);

					if($act_outtime != '00:00:00'){
						if($shift_early == 1){
							$shift_intime_minus_one = Date('H:i:s', strtotime($act_shift_intime .' -0 minutes'));
							if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
								$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
							} else {
								$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
							}
						} else {
							$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
						}
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
						/*
						if(strtotime($shift_working_time) == strtotime('09:30:00')){
							if( ($since_start->h == 9 && $since_start->i >= 30) || $since_start->h > 9){
								$first_half = 1;
								$second_half = 1;
							} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
								$first_half = 1;
								$second_half = 0;
							} else {
								$first_half = 0;
								$second_half = 0;
							}
						} elseif (strtotime($shift_working_time) == strtotime('09:00:00')) {
							if( ($since_start->h == 9 && $since_start->i >= 0) || $since_start->h > 9){
								$first_half = 1;
								$second_half = 1;
							} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
								$first_half = 1;
								$second_half = 0;
							} else {
								$first_half = 0;
								$second_half = 0;
							}
						} elseif (strtotime($shift_working_time) == strtotime('08:30:00')) {
							if( ($since_start->h == 8 && $since_start->i >= 30) || $since_start->h > 8){
								$first_half = 1;
								$second_half = 1;
							} elseif( ($since_start->h == 4 && $since_start->i >= 0) || $since_start->h > 4){
								$first_half = 1;
								$second_half = 0;
							} else {
								$first_half = 0;
								$second_half = 0;
							}
						}
						*/
						if($rvalue['unit_id'] == 1 || $rvalue['unit_id'] == 3 || $rvalue['unit_id'] == 4){
							if( ($since_start->h == 9 && $since_start->i >= 30) || $since_start->h > 9){
								$first_half = 1;
								$second_half = 1;
							} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
								$first_half = 1;
								$second_half = 0;
							} else {
								$first_half = 0;
								$second_half = 0;
							}
						} elseif ($rvalue['unit_id'] == 5) {
							if( ($since_start->h == 9 && $since_start->i >= 0) || $since_start->h > 9){
								$first_half = 1;
								$second_half = 1;
							} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
								$first_half = 1;
								$second_half = 0;
							} else {
								$first_half = 0;
								$second_half = 0;
							}
						} else {
							if( ($since_start->h == 8 && $since_start->i >= 30) || $since_start->h > 8){
								$first_half = 1;
								$second_half = 1;
							} elseif( ($since_start->h == 4 && $since_start->i >= 0) || $since_start->h > 4){
								$first_half = 1;
								$second_half = 0;
							} else {
								$first_half = 0;
								$second_half = 0;
							}
						}
						$working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
					} else {
						$first_half = 0;
						$second_half = 0;
					}
				} else {
					$first_half = 0;
					$second_half = 0;
				}
				$early_time = '00:00:00';
				$over_time = '00:00:00';
				$over_time_amount = '0';
				$over_time_esic = '0';
				$after_shift = '0';
				if($abnormal_status == 0){ //if abnormal status is zero calculate further 
					if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and actouttime 
						$start_date = new DateTime($act_in_punch_date.' '.$act_shift_outtime_flexi);
						$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
						if($since_start->invert == 0 && ($since_start->h > 0 || $since_start->i > 0)){
							$after_shift = 1;
						} else {
							$after_shift = 0;
						}
						$shift_outtime_minus_one = Date('H:i:s', strtotime($act_shift_outtime .' -0 minutes'));
						$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
						if($since_start->h > 12){
							$shift_outtime_minus_one = Date('H:i:s', strtotime($act_shift_outtime .' -0 minutes'));
							$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
						}
						$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
						if($since_start->invert == 0){
							$early_time = '00:00:00';
						} else {
							$early_mark = 1;
						}
						$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
						if($since_start->invert == 1){
							$over_time = '00:00:00';
						} else {
							if(strtotime($shift_working_time) == strtotime('09:30:00')){
								$shift_working_hours = '09:30:00';
								$start_date = new DateTime($act_in_punch_date.' '.$shift_working_hours);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$working_time));
								if($since_start->invert == 0){
									$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
								} else {
									$over_time = '00:00:00';
								}
							} elseif(strtotime($shift_working_time) == strtotime('09:00:00')){
								$shift_working_hours = '09:00:00';
								$start_date = new DateTime($act_in_punch_date.' '.$shift_working_hours);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$working_time));
								if($since_start->invert == 0){
									$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
								} else {
									$over_time = '00:00:00';
								}
							} elseif(strtotime($shift_working_time) == strtotime('08:30:00')){
								$shift_working_hours = '08:30:00';
								$start_date = new DateTime($act_in_punch_date.' '.$shift_working_hours);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$working_time));
								if($since_start->invert == 0){
									$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
								} else {
									$over_time = '00:00:00';
								}
							}
							/*
							if($late_time != '00:00:00'){
								$late_time_exp = explode(':', $late_time);
								$hour = $late_time_exp[0];
								$min = $late_time_exp[1];
								$hour_min = $hour.':'.$min;
								$minutes_compare = hoursToMinutes($hour_min);
								$over_time1 = $over_time;
								$over_time_exp = explode(':', $over_time);
								$hour = $over_time_exp[0];
								$min = $over_time_exp[1];
								$hour_min = $hour.':'.$min;
								$over_time_minutes_compare = hoursToMinutes($hour_min);
								if($over_time_minutes_compare > $minutes_compare){
									$over_time = Date('H:i:s', strtotime($over_time1 .' -'. $minutes_compare .' minutes'));
								} else {
									$over_time = '00:00:00';
								}
							} else {
								$over_time = $over_time;
							}
							*/
							/*
							$overtime_exp = explode(':', $over_time);
							$hour = $overtime_exp[0];
							$min = $overtime_exp[1];
							$hour_min = $hour.':'.$min;
							$minutes_compare = hoursToMinutes($hour_min);
							$over_time_hours = '';
							if($minutes_compare > 0 && $minutes_compare <= 44){
								$over_time_hours = '00';
							} elseif($minutes_compare >= 45 && $minutes_compare <= 104){
								$over_time_hours = '01';
							} elseif($minutes_compare >= 105 && $minutes_compare <= 164){
								$over_time_hours = '02';
							} elseif($minutes_compare >= 165 && $minutes_compare <= 224){
								$over_time_hours = '03';
							} elseif($minutes_compare >= 225 && $minutes_compare <= 284){
								$over_time_hours = '04';
							} elseif($minutes_compare >= 285 && $minutes_compare <= 344){
								$over_time_hours = '05';
							} elseif($minutes_compare >= 345 && $minutes_compare <= 405){
								$over_time_hours = '06';
							} elseif($minutes_compare >= 405 && $minutes_compare <= 464){
								$over_time_hours = '07';
							} elseif($minutes_compare >= 465 && $minutes_compare <= 524){
								$over_time_hours = '08';
							} elseif($minutes_compare >= 525 && $minutes_compare <= 584){
								$over_time_hours = '09';
							} elseif($minutes_compare >= 585 && $minutes_compare <= 644){
								$over_time_hours = '10';
							} elseif($minutes_compare >= 645 && $minutes_compare <= 704){
								$over_time_hours = '11';
							} elseif($minutes_compare >= 705 && $minutes_compare <= 764){
								$over_time_hours = '12';
							} elseif($minutes_compare >= 765 && $minutes_compare <= 824){
								$over_time_hours = '13';
							} elseif($minutes_compare >= 825 && $minutes_compare <= 884){
								$over_time_hours = '14';
							} elseif($minutes_compare >= 885 && $minutes_compare <= 944){
								$over_time_hours = '15';
							} elseif($minutes_compare >= 945 && $minutes_compare <= 1004){
								$over_time_hours = '16';
							}
							// if($emp_salary['basic'] > 0 && $emp_salary['vda'] > 0){
							// 	$over_time_amount_1 = (($emp_salary['basic'] + $emp_salary['vda']) / 8);
							// 	$over_time_amount = $over_time_amount_1 * 2 * $over_time_hours;
							// }
							$over_time_amount = '0.00';
							//$over_time = $over_time_hours.':00';
							$over_time_esic = round(($over_time_amount * 1.75) / 100);
							*/
						}
					}					
				} else {
					$first_half = 0;
					$second_half = 0;
				}
				$abs_stat = 0;
				if($working_time == '00:00:00'){
					//$late_time = '00:00:00';
					$early_time = '00:00:00';
					$over_time = '00:00:00';
				}
				if($first_half == 1 && $second_half == 1){
					$present_status = 1;
					$absent_status = 0;
				} elseif($first_half == 1 && $second_half == 0){
					$present_status = 0.5;
					$absent_status = 0.5;
				} elseif($first_half == 0 && $second_half == 1){
					$present_status = 0.5;
					$absent_status = 0.5;
				} else {
					$present_status = 0;
					$absent_status = 1;
				}
				// echo 'shift Id :' . $shift_id;
				// echo '<br />';
				// echo 'shift Code :' . $shift_code;
				// echo '<br />';
				// echo 'shift Intime :' . $shift_intime;
				// echo '<br />';
				// echo 'shift Outtime :' . $shift_outtime;
				// echo '<br />';
				// echo 'shift Outtime Flexi:' . $shift_outtime_flexi;
				// echo '<br />';
				// echo 'shift Allowance :' . $shift_allowance;
				// echo '<br />';
				// echo 'Actual shift Id :' . $act_shift_id;
				// echo '<br />';
				// echo 'Actual shift Code :' . $act_shift_code;
				// echo '<br />';
				// echo 'Actual shift Intime :' . $act_shift_intime;
				// echo '<br />';
				// echo 'Actual shift Outtime :' . $act_shift_outtime;
				// echo '<br />';
				// echo 'Actual shift Outtime Flexi:' . $act_shift_outtime_flexi;
				// echo '<br />';
				// echo 'Actual shift Allowance :' . $act_shift_allowance;
				// echo '<br />';
				// echo 'Actual In Date :' . $act_in_punch_date;
				// echo '<br />';
				// echo 'Actual In time :' . $act_intime;
				// echo '<br />';
				// echo 'Actual Out Date :' . $act_out_punch_date;
				// echo '<br />';
				// echo 'Actual Out time :' . $act_outtime;
				// echo '<br />';
				// echo 'Working time :' . $working_time;
				// echo '<br />';
				// echo 'Shift Early :' . $shift_early;
				// echo '<br />';
				// echo 'Late Time :' . $late_time;
				// echo '<br />';
				// echo 'Late Mark :' . $late_mark;
				// echo '<br />';
				// echo 'Early Time :' . $early_time;
				// echo '<br />';
				// echo 'Early Mark :' . $early_mark;
				// echo '<br />';
				// echo 'Over Time :' . $over_time;
				// echo '<br />';
				// echo 'Over Time Amount :' . $over_time_amount;
				// echo '<br />';
				// echo 'Over Time ESIC :' . $over_time_esic;
				// echo '<br />';
				// echo 'After Shift :' . $after_shift;
				// echo '<br />';
				// echo 'First Half :' . $first_half;
				// echo '<br />';
				// echo 'Second Half :' . $second_half;
				// echo '<br />';
				// echo 'Present Status :' . $present_status;
				// echo '<br />';
				// echo 'Absent Status :' . $absent_status;
				// echo '<br />';
				// exit;

				$day = date('j', strtotime($filter_date_start));
				$month = date('n', strtotime($filter_date_start));
				$year = date('Y', strtotime($filter_date_start));
				$trans_exist_sql = "SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$dvalue['punch_date']."' ";
				$trans_exist = query($trans_exist_sql, $conn);
				if($trans_exist->num_rows == 0){
					if($abs_stat == 1){
						$act_intime = '00:00:00';
						$act_outtime = '00:00:00';
						$abnormal_status = 0;
						$act_out_punch_date = $filter_date_start;
						$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_allowance` = '".$act_shift_allowance."', `act_shift_id` = '".$act_shift_id."', `act_shift_code` = '".$act_shift_code."', `act_shift_intime` = '".$act_shift_intime."', `act_shift_outtime` = '".$act_shift_outtime."', `act_shift_outtime_flexi` = '".$act_shift_outtime_flexi."', `act_shift_allowance` = '".$act_shift_allowance."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `late_mark` = '".$late_mark."', `early_time` = '".$early_time."', `early_mark` = '".$early_mark."', `over_time` = '".$over_time."', `over_time_amount` = '".$over_time_amount."', `over_time_esic` = '".$over_time_esic."', `after_shift` = '".$after_shift."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."' ";
					} else {
						$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_allowance` = '".$act_shift_allowance."', `act_shift_id` = '".$act_shift_id."', `act_shift_code` = '".$act_shift_code."', `act_shift_intime` = '".$act_shift_intime."', `act_shift_outtime` = '".$act_shift_outtime."', `act_shift_outtime_flexi` = '".$act_shift_outtime_flexi."', `act_shift_allowance` = '".$act_shift_allowance."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `late_mark` = '".$late_mark."', `early_time` = '".$early_time."', `early_mark` = '".$early_mark."', `over_time` = '".$over_time."', `over_time_amount` = '".$over_time_amount."', `over_time_esic` = '".$over_time_esic."', `after_shift` = '".$after_shift."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."' ";
					}
					//fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql, true)  . "\n");
					query($sql, $conn);
					$transaction_id = getLastId($conn);
				} else {
					if($abs_stat == 1){
						$act_intime = '00:00:00';
						$act_outtime = '00:00:00';
						$abnormal_status = 0;
						$act_out_punch_date = $filter_date_start;
						$sql = "UPDATE `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_allowance` = '".$act_shift_allowance."', `act_shift_id` = '".$act_shift_id."', `act_shift_code` = '".$act_shift_code."', `act_shift_intime` = '".$act_shift_intime."', `act_shift_outtime` = '".$act_shift_outtime."', `act_shift_outtime_flexi` = '".$act_shift_outtime_flexi."', `act_shift_allowance` = '".$act_shift_allowance."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `late_mark` = '".$late_mark."', `early_time` = '".$early_time."', `early_mark` = '".$early_mark."', `over_time` = '".$over_time."', `over_time_amount` = '".$over_time_amount."', `over_time_esic` = '".$over_time_esic."', `after_shift` = '".$after_shift."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$act_in_punch_date."' ";
					} else {
						$sql = "UPDATE `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_allowance` = '".$act_shift_allowance."', `act_shift_id` = '".$act_shift_id."', `act_shift_code` = '".$act_shift_code."', `act_shift_intime` = '".$act_shift_intime."', `act_shift_outtime` = '".$act_shift_outtime."', `act_shift_outtime_flexi` = '".$act_shift_outtime_flexi."', `act_shift_allowance` = '".$act_shift_allowance."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `late_mark` = '".$late_mark."', `early_time` = '".$early_time."', `early_mark` = '".$early_mark."', `over_time` = '".$over_time."', `over_time_amount` = '".$over_time_amount."', `over_time_esic` = '".$over_time_esic."', `after_shift` = '".$after_shift."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$act_in_punch_date."' ";
					}
					//fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql, true)  . "\n");
					query($sql, $conn);
					$transaction_id = query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$act_in_punch_date."' ", $conn)->row['transaction_id'];
				}
			} elseif ($schedule_raw[0] == 'W') {
				$shift_data = getshiftdata($schedule_raw[2], $conn);
				if(!isset($shift_data['shift_id'])){
					$shift_data = getshiftdata('1', $conn);
				}
				$shift_intime = $shift_data['in_time'];
				$shift_outtime = $shift_data['out_time'];
				$shift_outtime_flexi = Date('H:i:s', strtotime($shift_data['out_time'] .' +60 minutes'));
				$shift_id = $shift_data['shift_id'];
				$shift_code = $shift_data['shift_code'];
				$shift_allowance = $shift_data['shift_allowance'];
				
				$act_shift_id = $shift_id;
				$act_shift_code = $shift_code;
				$act_shift_allowance = $shift_allowance;
				$act_shift_intime = $shift_intime;
				$act_shift_outtime = $shift_outtime;
				$act_shift_outtime_flexi = $shift_outtime_flexi;
				$act_shift_allowance = $shift_allowance;

				$act_intime = '00:00:00';
				$act_outtime = '00:00:00';
				$act_in_punch_date = $filter_date_start;
				$act_out_punch_date = $filter_date_start;
				$act_intimes = getrawattendance_in_time($rvalue['emp_code'], $dvalue['punch_date'], $shift_data, $conn);
				if(isset($act_intimes['punch_time'])) {
					$act_intime = $act_intimes['punch_time'];
					$act_in_punch_date = $act_intimes['punch_date'];
					/*
					$array = getshiftdata('0', $conn);
					$act_array = array();
					$act_array_min = array();
					$hour_array = array();
					$min_array = array();
					$time_array = array();
					$zero_hour_count = 0;
					foreach ($array as $akey => $avalue) {
						$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
						$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$avalue['in_time']));
						if($since_start->d == 0){
							$time_array[] = $since_start->h.':'.$since_start->i.':'.$since_start->s;
							$hour_array[] = $since_start->h;
							$act_array[] = $avalue;
							if($since_start->h == 0){
								$act_array_min[] = $avalue;
								$min_array[] = $since_start->i;
								$zero_hour_count ++;
							}
						}
					}
					if($zero_hour_count >= 2){
						$shift_work_hour = '0';
						if($min_array){
							$shift_work_hour = min($min_array);
						}
						$shift_worked_data = array();
						foreach ($min_array as $akey => $avalue) {
							if($avalue == $shift_work_hour){
								$shift_worked_data = $act_array_min[$akey];
							}
						}
					} else {
						$shift_work_hour = '0';
						if($hour_array){
							$shift_work_hour = min($hour_array);
						}
						$shift_worked_data = array();
						foreach ($hour_array as $akey => $avalue) {
							if($avalue == $shift_work_hour){
								$shift_worked_data = $act_array[$akey];
							}
						}	
					}
					if($shift_worked_data){
						$act_shift_intime = $shift_worked_data['in_time'];
						$act_shift_outtime = $shift_worked_data['out_time'];
						$act_shift_id = $shift_worked_data['shift_id'];
						$act_shift_code = $shift_worked_data['shift_code'];
						$act_shift_allowance = $shift_worked_data['shift_allowance'];
					}
					*/
				}
				$act_outtimes = getrawattendance_out_time($rvalue['emp_code'], $dvalue['punch_date'], $act_intime, $act_in_punch_date, $shift_data, $conn);
				if(isset($act_outtimes['punch_time'])) {
					$act_outtime = $act_outtimes['punch_time'];
					$act_out_punch_date = $act_outtimes['punch_date'];
				}
				if($act_intime == $act_outtime){
					$act_outtime = '00:00:00';
				}
				$abnormal_status = 0;
				$first_half = 0;
				$second_half = 0;
				$late_time = '00:00:00';
				$early_time = '00:00:00';
				$working_time = '00:00:00';
				$late_mark = 0;
				$early_mark = 0;
				$shift_early = 0;
				if($act_intime != '00:00:00'){
					$shift_intime_plus_one = Date('H:i:s', strtotime($act_shift_intime .' +60 minutes'));
					$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
					$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
					if($since_start->h > 12){
						$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
					}
					if($since_start->h > 0){
						$late_hour = $since_start->h;
						$late_min = $since_start->i;
						$late_sec = $since_start->s;
					} else {
						$late_hour = $since_start->h;
						$late_min = $since_start->i;
						$late_sec = $since_start->s;
					}
					$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
					if($since_start->invert == 1){
						$late_time = '00:00:00';
					} else {
						$late_mark = 1;
					}
					$shift_intime_minus_one = Date('H:i:s', strtotime($act_shift_intime .' -0 minutes'));
					$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
					$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
					if($since_start->h > 12){
						$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
					}
					if($since_start->invert == 1){
						$shift_early = 1;
					}
					$start_date = new DateTime($act_in_punch_date.' '.$act_shift_intime);
					$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_shift_outtime));
					$shift_working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);

					if($act_outtime != '00:00:00'){
						if($shift_early == 1){
							$shift_intime_minus_one = Date('H:i:s', strtotime($act_shift_intime .' -0 minutes'));
							if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
								$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
							} else {
								$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
							}
						} else {
							$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
						}
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
						/*
						if(strtotime($shift_working_time) == strtotime('09:30:00')){
							if( ($since_start->h == 9 && $since_start->i >= 30) || $since_start->h > 9){
								$first_half = 1;
								$second_half = 1;
							} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
								$first_half = 1;
								$second_half = 0;
							} else {
								$first_half = 0;
								$second_half = 0;
							}
						} elseif (strtotime($shift_working_time) == strtotime('09:00:00')) {
							if( ($since_start->h == 9 && $since_start->i >= 0) || $since_start->h > 9){
								$first_half = 1;
								$second_half = 1;
							} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
								$first_half = 1;
								$second_half = 0;
							} else {
								$first_half = 0;
								$second_half = 0;
							}
						} elseif (strtotime($shift_working_time) == strtotime('08:30:00')) {
							if( ($since_start->h == 8 && $since_start->i >= 30) || $since_start->h > 8){
								$first_half = 1;
								$second_half = 1;
							} elseif( ($since_start->h == 4 && $since_start->i >= 0) || $since_start->h > 4){
								$first_half = 1;
								$second_half = 0;
							} else {
								$first_half = 0;
								$second_half = 0;
							}
						}
						*/
						if($rvalue['unit_id'] == 1 || $rvalue['unit_id'] == 3 || $rvalue['unit_id'] == 4){
							if( ($since_start->h == 9 && $since_start->i >= 30) || $since_start->h > 9){
								$first_half = 1;
								$second_half = 1;
							} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
								$first_half = 1;
								$second_half = 0;
							} else {
								$first_half = 0;
								$second_half = 0;
							}
						} elseif ($rvalue['unit_id'] == 5) {
							if( ($since_start->h == 9 && $since_start->i >= 0) || $since_start->h > 9){
								$first_half = 1;
								$second_half = 1;
							} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
								$first_half = 1;
								$second_half = 0;
							} else {
								$first_half = 0;
								$second_half = 0;
							}
						} else {
							if( ($since_start->h == 8 && $since_start->i >= 30) || $since_start->h > 8){
								$first_half = 1;
								$second_half = 1;
							} elseif( ($since_start->h == 4 && $since_start->i >= 0) || $since_start->h > 4){
								$first_half = 1;
								$second_half = 0;
							} else {
								$first_half = 0;
								$second_half = 0;
							}
						}
						$working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
					} else {
						$first_half = 0;
						$second_half = 0;
					}
				} else {
					$first_half = 0;
					$second_half = 0;
				}
				$early_time = '00:00:00';
				$over_time = '00:00:00';
				$over_time_amount = '0';
				$over_time_esic = '0';
				$after_shift = 0;
				if($abnormal_status == 0){ //if abnormal status is zero calculate further 
					if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and actouttime 
						$start_date = new DateTime($act_in_punch_date.' '.$act_shift_outtime_flexi);
						$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
						if($since_start->invert == 0 && ($since_start->h > 0 || $since_start->i > 0)){
							$after_shift = 1;
						} else {
							$after_shift = 0;
						}

						$shift_outtime_minus_one = Date('H:i:s', strtotime($act_shift_outtime .' -0 minutes'));
						$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
						if($since_start->h > 12){
							$shift_outtime_minus_one = Date('H:i:s', strtotime($act_shift_outtime .' -0 minutes'));
							$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
						}
						$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
						if($since_start->invert == 0){
							$early_time = '00:00:00';
						} else {
							$early_mark = 1;
						}
						$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
						if($since_start->invert == 1){
							$over_time = '00:00:00';
						} else {
							if(strtotime($shift_working_time) == strtotime('09:30:00')){
								$shift_working_hours = '09:30:00';
								$start_date = new DateTime($act_in_punch_date.' '.$shift_working_hours);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$working_time));
								if($since_start->invert == 0){
									$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
								} else {
									$over_time = '00:00:00';
								}
							} elseif(strtotime($shift_working_time) == strtotime('09:00:00')){
								$shift_working_hours = '09:00:00';
								$start_date = new DateTime($act_in_punch_date.' '.$shift_working_hours);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$working_time));
								if($since_start->invert == 0){
									$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
								} else {
									$over_time = '00:00:00';
								}
							} elseif(strtotime($shift_working_time) == strtotime('08:30:00')){
								$shift_working_hours = '08:30:00';
								$start_date = new DateTime($act_in_punch_date.' '.$shift_working_hours);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$working_time));
								if($since_start->invert == 0){
									$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
								} else {
									$over_time = '00:00:00';
								}
							}
							/*
							if($late_time != '00:00:00'){
								$late_time_exp = explode(':', $late_time);
								$hour = $late_time_exp[0];
								$min = $late_time_exp[1];
								$hour_min = $hour.':'.$min;
								$minutes_compare = hoursToMinutes($hour_min);
								$over_time1 = $over_time;
								$over_time = Date('H:i:s', strtotime($over_time1 .' -'. $minutes_compare .' minutes'));
							} else {
								$over_time = $over_time;
							}
							$overtime_exp = explode(':', $over_time);
							$hour = $overtime_exp[0];
							$min = $overtime_exp[1];
							$hour_min = $hour.':'.$min;
							$minutes_compare = hoursToMinutes($hour_min);
							$over_time_hours = '';
							if($minutes_compare > 0 && $minutes_compare <= 44){
								$over_time_hours = '00';
							} elseif($minutes_compare >= 45 && $minutes_compare <= 104){
								$over_time_hours = '01';
							} elseif($minutes_compare >= 105 && $minutes_compare <= 164){
								$over_time_hours = '02';
							} elseif($minutes_compare >= 165 && $minutes_compare <= 224){
								$over_time_hours = '03';
							} elseif($minutes_compare >= 225 && $minutes_compare <= 284){
								$over_time_hours = '04';
							} elseif($minutes_compare >= 285 && $minutes_compare <= 344){
								$over_time_hours = '05';
							} elseif($minutes_compare >= 345 && $minutes_compare <= 405){
								$over_time_hours = '06';
							} elseif($minutes_compare >= 405 && $minutes_compare <= 464){
								$over_time_hours = '07';
							} elseif($minutes_compare >= 465 && $minutes_compare <= 524){
								$over_time_hours = '08';
							} elseif($minutes_compare >= 525 && $minutes_compare <= 584){
								$over_time_hours = '09';
							} elseif($minutes_compare >= 585 && $minutes_compare <= 644){
								$over_time_hours = '10';
							} elseif($minutes_compare >= 645 && $minutes_compare <= 704){
								$over_time_hours = '11';
							} elseif($minutes_compare >= 705 && $minutes_compare <= 764){
								$over_time_hours = '12';
							} elseif($minutes_compare >= 765 && $minutes_compare <= 824){
								$over_time_hours = '13';
							} elseif($minutes_compare >= 825 && $minutes_compare <= 884){
								$over_time_hours = '14';
							} elseif($minutes_compare >= 885 && $minutes_compare <= 944){
								$over_time_hours = '15';
							} elseif($minutes_compare >= 945 && $minutes_compare <= 1004){
								$over_time_hours = '16';
							}
							// if($emp_salary['basic'] > 0 && $emp_salary['vda'] > 0){
							// 	$over_time_amount_1 = (($emp_salary['basic'] + $emp_salary['vda']) / 8);
							// 	$over_time_amount = $over_time_amount_1 * 2 * $over_time_hours;
							// }
							$over_time_amount = '0.00';
							//$over_time = $over_time_hours.':00';
							$over_time_esic = round(($over_time_amount * 1.75) / 100);
							*/
						}
					}					
				} else {
					$first_half = 0;
					$second_half = 0;
				}
				$abs_stat = 0;
				if($working_time == '00:00:00'){
					//$late_time = '00:00:00';
					$early_time = '00:00:00';
					$over_time = '00:00:00';
				}
				if($first_half == 1 && $second_half == 1){
					$present_status = 1;
					$absent_status = 0;
				} elseif($first_half == 1 && $second_half == 0){
					$present_status = 0.5;
					$absent_status = 0.5;
				} elseif($first_half == 0 && $second_half == 1){
					$present_status = 0.5;
					$absent_status = 0.5;
				} else {
					$present_status = 0;
					$absent_status = 1;
				}

				$day = date('j', strtotime($filter_date_start));
				$month = date('n', strtotime($filter_date_start));
				$year = date('Y', strtotime($filter_date_start));
				$trans_exist_sql = "SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$dvalue['punch_date']."' ";
				$trans_exist = query($trans_exist_sql, $conn);
				if($trans_exist->num_rows == 0){
					if($abs_stat == 1){
						$act_intime = '00:00:00';
						$act_outtime = '00:00:00';
						$abnormal_status = 0;
						$act_out_punch_date = $filter_date_start;
						$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_allowance` = '".$act_shift_allowance."', `act_shift_id` = '".$act_shift_id."', `act_shift_code` = '".$act_shift_code."', `act_shift_intime` = '".$act_shift_intime."', `act_shift_outtime` = '".$act_shift_outtime."', `act_shift_outtime_flexi` = '".$act_shift_outtime_flexi."', `act_shift_allowance` = '".$act_shift_allowance."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `late_mark` = '".$late_mark."', `early_time` = '".$early_time."', `early_mark` = '".$early_mark."', `over_time` = '".$over_time."', `over_time_amount` = '".$over_time_amount."', `over_time_esic` = '".$over_time_esic."', `after_shift` = '".$after_shift."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', compli_status = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."' ";
					} else {
						$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_allowance` = '".$act_shift_allowance."', `act_shift_id` = '".$act_shift_id."', `act_shift_code` = '".$act_shift_code."', `act_shift_intime` = '".$act_shift_intime."', `act_shift_outtime` = '".$act_shift_outtime."', `act_shift_outtime_flexi` = '".$act_shift_outtime_flexi."', `act_shift_allowance` = '".$act_shift_allowance."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `late_mark` = '".$late_mark."', `early_time` = '".$early_time."', `early_mark` = '".$early_mark."', `over_time` = '".$over_time."', `over_time_amount` = '".$over_time_amount."', `over_time_esic` = '".$over_time_esic."', `after_shift` = '".$after_shift."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', compli_status = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."' ";
					}
					query($sql, $conn);
					$transaction_id = getLastId($conn);
				} else {
					if($abs_stat == 1){
						$act_intime = '00:00:00';
						$act_outtime = '00:00:00';
						$abnormal_status = 0;
						$act_out_punch_date = $filter_date_start;
						$sql = "UPDATE `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_allowance` = '".$act_shift_allowance."', `act_shift_id` = '".$act_shift_id."', `act_shift_code` = '".$act_shift_code."', `act_shift_intime` = '".$act_shift_intime."', `act_shift_outtime` = '".$act_shift_outtime."', `act_shift_outtime_flexi` = '".$act_shift_outtime_flexi."', `act_shift_allowance` = '".$act_shift_allowance."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `late_mark` = '".$late_mark."', `early_time` = '".$early_time."', `early_mark` = '".$early_mark."', `over_time` = '".$over_time."', `over_time_amount` = '".$over_time_amount."', `over_time_esic` = '".$over_time_esic."', `after_shift` = '".$after_shift."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', compli_status = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$act_in_punch_date."' ";
					} else {
						$sql = "UPDATE `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_allowance` = '".$act_shift_allowance."', `act_shift_id` = '".$act_shift_id."', `act_shift_code` = '".$act_shift_code."', `act_shift_intime` = '".$act_shift_intime."', `act_shift_outtime` = '".$act_shift_outtime."', `act_shift_outtime_flexi` = '".$act_shift_outtime_flexi."', `act_shift_allowance` = '".$act_shift_allowance."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `late_mark` = '".$late_mark."', `early_time` = '".$early_time."', `early_mark` = '".$early_mark."', `over_time` = '".$over_time."', `over_time_amount` = '".$over_time_amount."', `over_time_esic` = '".$over_time_esic."', `after_shift` = '".$after_shift."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', compli_status = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$act_in_punch_date."' ";
					}
					query($sql, $conn);
					$transaction_id = query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$act_in_punch_date."' ", $conn)->row['transaction_id'];
				}
				//echo $sql.';';
				//echo '<br />';
			} elseif ($schedule_raw[0] == 'H') {
				$shift_data = getshiftdata($schedule_raw[2], $conn);
				if(!isset($shift_data['shift_id'])){
					$shift_data = getshiftdata('1', $conn);
				}
				$shift_intime = $shift_data['in_time'];
				$shift_outtime = $shift_data['out_time'];
				$shift_outtime_flexi = Date('H:i:s', strtotime($shift_data['out_time'] .' +60 minutes'));
				$shift_id = $shift_data['shift_id'];
				$shift_code = $shift_data['shift_code'];
				$shift_allowance = $shift_data['shift_allowance'];
				
				$act_shift_id = $shift_id;
				$act_shift_code = $shift_code;
				$act_shift_allowance = $shift_allowance;
				$act_shift_intime = $shift_intime;
				$act_shift_outtime = $shift_outtime;
				$act_shift_outtime_flexi = $shift_outtime_flexi;
				$act_shift_allowance = $shift_allowance;

				$act_intime = '00:00:00';
				$act_outtime = '00:00:00';
				$act_in_punch_date = $filter_date_start;
				$act_out_punch_date = $filter_date_start;
				$act_intimes = getrawattendance_in_time($rvalue['emp_code'], $dvalue['punch_date'], $shift_data, $conn);
				if(isset($act_intimes['punch_time'])) {
					$act_intime = $act_intimes['punch_time'];
					$act_in_punch_date = $act_intimes['punch_date'];
					/*
					$array = getshiftdata('0', $conn);
					$act_array = array();
					$act_array_min = array();
					$hour_array = array();
					$min_array = array();
					$time_array = array();
					$zero_hour_count = 0;
					foreach ($array as $akey => $avalue) {
						$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
						$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$avalue['in_time']));
						if($since_start->d == 0){
							$time_array[] = $since_start->h.':'.$since_start->i.':'.$since_start->s;
							$hour_array[] = $since_start->h;
							$act_array[] = $avalue;
							if($since_start->h == 0){
								$act_array_min[] = $avalue;
								$min_array[] = $since_start->i;
								$zero_hour_count ++;
							}
						}
					}
					if($zero_hour_count >= 2){
						$shift_work_hour = '0';
						if($min_array){
							$shift_work_hour = min($min_array);
						}
						$shift_worked_data = array();
						foreach ($min_array as $akey => $avalue) {
							if($avalue == $shift_work_hour){
								$shift_worked_data = $act_array_min[$akey];
							}
						}
					} else {
						$shift_work_hour = '0';
						if($hour_array){
							$shift_work_hour = min($hour_array);
						}
						$shift_worked_data = array();
						foreach ($hour_array as $akey => $avalue) {
							if($avalue == $shift_work_hour){
								$shift_worked_data = $act_array[$akey];
							}
						}	
					}
					if($shift_worked_data){
						$act_shift_intime = $shift_worked_data['in_time'];
						$act_shift_outtime = $shift_worked_data['out_time'];
						$act_shift_id = $shift_worked_data['shift_id'];
						$act_shift_code = $shift_worked_data['shift_code'];
						$act_shift_allowance = $shift_worked_data['shift_allowance'];
					}
					*/
				}
				$act_outtimes = getrawattendance_out_time($rvalue['emp_code'], $dvalue['punch_date'], $act_intime, $act_in_punch_date, $shift_data, $conn);
				if(isset($act_outtimes['punch_time'])) {
					$act_outtime = $act_outtimes['punch_time'];
					$act_out_punch_date = $act_outtimes['punch_date'];
				}
				if($act_intime == $act_outtime){
					$act_outtime = '00:00:00';
				}
				$abnormal_status = 0;
				$first_half = 0;
				$second_half = 0;
				$late_time = '00:00:00';
				$early_time = '00:00:00';
				$working_time = '00:00:00';
				$late_mark = 0;
				$early_mark = 0;
				$shift_early = 0;
				if($act_intime != '00:00:00'){
					$shift_intime_plus_one = Date('H:i:s', strtotime($act_shift_intime .' +60 minutes'));
					$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
					$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
					if($since_start->h > 12){
						$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
					}
					if($since_start->h > 0){
						$late_hour = $since_start->h;
						$late_min = $since_start->i;
						$late_sec = $since_start->s;
					} else {
						$late_hour = $since_start->h;
						$late_min = $since_start->i;
						$late_sec = $since_start->s;
					}
					$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
					if($since_start->invert == 1){
						$late_time = '00:00:00';
					} else {
						$late_mark = 1;
					}
					$shift_intime_minus_one = Date('H:i:s', strtotime($act_shift_intime .' -0 minutes'));
					$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
					$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
					if($since_start->h > 12){
						$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
					}
					if($since_start->invert == 1){
						$shift_early = 1;
					}
					$start_date = new DateTime($act_in_punch_date.' '.$act_shift_intime);
					$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_shift_outtime));
					$shift_working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);

					if($act_outtime != '00:00:00'){
						if($shift_early == 1){
							$shift_intime_minus_one = Date('H:i:s', strtotime($act_shift_intime .' -0 minutes'));
							if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
								$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
							} else {
								$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
							}
						} else {
							$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
						}
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
						/*
						if(strtotime($shift_working_time) == strtotime('09:30:00')){
							if( ($since_start->h == 9 && $since_start->i >= 30) || $since_start->h > 9){
								$first_half = 1;
								$second_half = 1;
							} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
								$first_half = 1;
								$second_half = 0;
							} else {
								$first_half = 0;
								$second_half = 0;
							}
						} elseif (strtotime($shift_working_time) == strtotime('09:00:00')) {
							if( ($since_start->h == 9 && $since_start->i >= 0) || $since_start->h > 9){
								$first_half = 1;
								$second_half = 1;
							} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
								$first_half = 1;
								$second_half = 0;
							} else {
								$first_half = 0;
								$second_half = 0;
							}
						} elseif (strtotime($shift_working_time) == strtotime('08:30:00')) {
							if( ($since_start->h == 8 && $since_start->i >= 30) || $since_start->h > 8){
								$first_half = 1;
								$second_half = 1;
							} elseif( ($since_start->h == 4 && $since_start->i >= 0) || $since_start->h > 4){
								$first_half = 1;
								$second_half = 0;
							} else {
								$first_half = 0;
								$second_half = 0;
							}
						}
						*/
						if($rvalue['unit_id'] == 1 || $rvalue['unit_id'] == 3 || $rvalue['unit_id'] == 4){
							if( ($since_start->h == 9 && $since_start->i >= 30) || $since_start->h > 9){
								$first_half = 1;
								$second_half = 1;
							} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
								$first_half = 1;
								$second_half = 0;
							} else {
								$first_half = 0;
								$second_half = 0;
							}
						} elseif ($rvalue['unit_id'] == 5) {
							if( ($since_start->h == 9 && $since_start->i >= 0) || $since_start->h > 9){
								$first_half = 1;
								$second_half = 1;
							} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
								$first_half = 1;
								$second_half = 0;
							} else {
								$first_half = 0;
								$second_half = 0;
							}
						} else {
							if( ($since_start->h == 8 && $since_start->i >= 30) || $since_start->h > 8){
								$first_half = 1;
								$second_half = 1;
							} elseif( ($since_start->h == 4 && $since_start->i >= 0) || $since_start->h > 4){
								$first_half = 1;
								$second_half = 0;
							} else {
								$first_half = 0;
								$second_half = 0;
							}
						}
						$working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
					} else {
						$first_half = 0;
						$second_half = 0;
					}
				} else {
					$first_half = 0;
					$second_half = 0;
				}
				$early_time = '00:00:00';
				$over_time = '00:00:00';
				$over_time_amount = '0';
				$over_time_esic = '0';
				$after_shift = 0;
				if($abnormal_status == 0){ //if abnormal status is zero calculate further 
					if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and actouttime 
						$start_date = new DateTime($act_in_punch_date.' '.$act_shift_outtime_flexi);
						$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
						if($since_start->invert == 0 && ($since_start->h > 0 || $since_start->i > 0)){
							$after_shift = 1;
						} else {
							$after_shift = 0;
						}

						$shift_outtime_minus_one = Date('H:i:s', strtotime($act_shift_outtime .' -0 minutes'));
						$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
						if($since_start->h > 12){
							$shift_outtime_minus_one = Date('H:i:s', strtotime($act_shift_outtime .' -0 minutes'));
							$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
						}
						$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
						if($since_start->invert == 0){
							$early_time = '00:00:00';
						} else {
							$early_mark = 1;
						}
						$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
						if($since_start->invert == 1){
							$over_time = '00:00:00';
						} else {
							if(strtotime($shift_working_time) == strtotime('09:30:00')){
								$shift_working_hours = '09:30:00';
								$start_date = new DateTime($act_in_punch_date.' '.$shift_working_hours);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$working_time));
								if($since_start->invert == 0){
									$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
								} else {
									$over_time = '00:00:00';
								}
							} elseif(strtotime($shift_working_time) == strtotime('09:00:00')){
								$shift_working_hours = '09:00:00';
								$start_date = new DateTime($act_in_punch_date.' '.$shift_working_hours);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$working_time));
								if($since_start->invert == 0){
									$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
								} else {
									$over_time = '00:00:00';
								}
							} elseif(strtotime($shift_working_time) == strtotime('08:30:00')){
								$shift_working_hours = '08:30:00';
								$start_date = new DateTime($act_in_punch_date.' '.$shift_working_hours);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$working_time));
								if($since_start->invert == 0){
									$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
								} else {
									$over_time = '00:00:00';
								}
							}
							/*
							if($late_time != '00:00:00'){
								$late_time_exp = explode(':', $late_time);
								$hour = $late_time_exp[0];
								$min = $late_time_exp[1];
								$hour_min = $hour.':'.$min;
								$minutes_compare = hoursToMinutes($hour_min);
								$over_time1 = $over_time;
								$over_time = Date('H:i:s', strtotime($over_time1 .' -'. $minutes_compare .' minutes'));
							} else {
								$over_time = $over_time;
							}
							$overtime_exp = explode(':', $over_time);
							$hour = $overtime_exp[0];
							$min = $overtime_exp[1];
							$hour_min = $hour.':'.$min;
							$minutes_compare = hoursToMinutes($hour_min);
							$over_time_hours = '';
							if($minutes_compare > 0 && $minutes_compare <= 44){
								$over_time_hours = '00';
							} elseif($minutes_compare >= 45 && $minutes_compare <= 104){
								$over_time_hours = '01';
							} elseif($minutes_compare >= 105 && $minutes_compare <= 164){
								$over_time_hours = '02';
							} elseif($minutes_compare >= 165 && $minutes_compare <= 224){
								$over_time_hours = '03';
							} elseif($minutes_compare >= 225 && $minutes_compare <= 284){
								$over_time_hours = '04';
							} elseif($minutes_compare >= 285 && $minutes_compare <= 344){
								$over_time_hours = '05';
							} elseif($minutes_compare >= 345 && $minutes_compare <= 405){
								$over_time_hours = '06';
							} elseif($minutes_compare >= 405 && $minutes_compare <= 464){
								$over_time_hours = '07';
							} elseif($minutes_compare >= 465 && $minutes_compare <= 524){
								$over_time_hours = '08';
							} elseif($minutes_compare >= 525 && $minutes_compare <= 584){
								$over_time_hours = '09';
							} elseif($minutes_compare >= 585 && $minutes_compare <= 644){
								$over_time_hours = '10';
							} elseif($minutes_compare >= 645 && $minutes_compare <= 704){
								$over_time_hours = '11';
							} elseif($minutes_compare >= 705 && $minutes_compare <= 764){
								$over_time_hours = '12';
							} elseif($minutes_compare >= 765 && $minutes_compare <= 824){
								$over_time_hours = '13';
							} elseif($minutes_compare >= 825 && $minutes_compare <= 884){
								$over_time_hours = '14';
							} elseif($minutes_compare >= 885 && $minutes_compare <= 944){
								$over_time_hours = '15';
							} elseif($minutes_compare >= 945 && $minutes_compare <= 1004){
								$over_time_hours = '16';
							}
							// if($emp_salary['basic'] > 0 && $emp_salary['vda'] > 0){
							// 	$over_time_amount_1 = (($emp_salary['basic'] + $emp_salary['vda']) / 8);
							// 	$over_time_amount = $over_time_amount_1 * 2 * $over_time_hours;
							// }
							$over_time_amount = '0.00';
							//$over_time = $over_time_hours.':00';
							$over_time_esic = round(($over_time_amount * 1.75) / 100);
							*/
						}
					}					
				} else {
					$first_half = 0;
					$second_half = 0;
				}
				$abs_stat = 0;
				if($working_time == '00:00:00'){
					//$late_time = '00:00:00';
					$early_time = '00:00:00';
					$over_time = '00:00:00';
				}
				if($first_half == 1 && $second_half == 1){
					$present_status = 1;
					$absent_status = 0;
				} elseif($first_half == 1 && $second_half == 0){
					$present_status = 0.5;
					$absent_status = 0.5;
				} elseif($first_half == 0 && $second_half == 1){
					$present_status = 0.5;
					$absent_status = 0.5;
				} else {
					$present_status = 0;
					$absent_status = 1;
				}

				$day = date('j', strtotime($filter_date_start));
				$month = date('n', strtotime($filter_date_start));
				$year = date('Y', strtotime($filter_date_start));
				$trans_exist_sql = "SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$dvalue['punch_date']."' ";
				$trans_exist = query($trans_exist_sql, $conn);
				if($trans_exist->num_rows == 0){
					if($abs_stat == 1){
						$act_intime = '00:00:00';
						$act_outtime = '00:00:00';
						$abnormal_status = 0;
						$act_out_punch_date = $filter_date_start;
						$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_allowance` = '".$act_shift_allowance."', `act_shift_id` = '".$act_shift_id."', `act_shift_code` = '".$act_shift_code."', `act_shift_intime` = '".$act_shift_intime."', `act_shift_outtime` = '".$act_shift_outtime."', `act_shift_outtime_flexi` = '".$act_shift_outtime_flexi."', `act_shift_allowance` = '".$act_shift_allowance."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `late_mark` = '".$late_mark."', `early_time` = '".$early_time."', `early_mark` = '".$early_mark."', `over_time` = '".$over_time."', `over_time_amount` = '".$over_time_amount."', `over_time_esic` = '".$over_time_esic."', `after_shift` = '".$after_shift."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', `abnormal_status` = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."' ";
					} else {
						$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_allowance` = '".$act_shift_allowance."', `act_shift_id` = '".$act_shift_id."', `act_shift_code` = '".$act_shift_code."', `act_shift_intime` = '".$act_shift_intime."', `act_shift_outtime` = '".$act_shift_outtime."', `act_shift_outtime_flexi` = '".$act_shift_outtime_flexi."', `act_shift_allowance` = '".$act_shift_allowance."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `late_mark` = '".$late_mark."', `early_time` = '".$early_time."', `early_mark` = '".$early_mark."', `over_time` = '".$over_time."', `over_time_amount` = '".$over_time_amount."', `over_time_esic` = '".$over_time_esic."', `after_shift` = '".$after_shift."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', `abnormal_status` = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."' ";
					}
					query($sql, $conn);
					$transaction_id = getLastId($conn);
				} else {
					if($abs_stat == 1){
						$act_intime = '00:00:00';
						$act_outtime = '00:00:00';
						$abnormal_status = 0;
						$act_out_punch_date = $filter_date_start;
						$sql = "UPDATE `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_allowance` = '".$act_shift_allowance."', `act_shift_id` = '".$act_shift_id."', `act_shift_code` = '".$act_shift_code."', `act_shift_intime` = '".$act_shift_intime."', `act_shift_outtime` = '".$act_shift_outtime."', `act_shift_outtime_flexi` = '".$act_shift_outtime_flexi."', `act_shift_allowance` = '".$act_shift_allowance."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `late_mark` = '".$late_mark."', `early_time` = '".$early_time."', `early_mark` = '".$early_mark."', `over_time` = '".$over_time."', `over_time_amount` = '".$over_time_amount."', `over_time_esic` = '".$over_time_esic."', `after_shift` = '".$after_shift."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', `abnormal_status` = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$act_in_punch_date."' ";
					} else {
						$sql = "UPDATE `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_allowance` = '".$act_shift_allowance."', `act_shift_id` = '".$act_shift_id."', `act_shift_code` = '".$act_shift_code."', `act_shift_intime` = '".$act_shift_intime."', `act_shift_outtime` = '".$act_shift_outtime."', `act_shift_outtime_flexi` = '".$act_shift_outtime_flexi."', `act_shift_allowance` = '".$act_shift_allowance."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `late_mark` = '".$late_mark."', `early_time` = '".$early_time."', `early_mark` = '".$early_mark."', `over_time` = '".$over_time."', `over_time_amount` = '".$over_time_amount."', `over_time_esic` = '".$over_time_esic."', `after_shift` = '".$after_shift."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', `abnormal_status` = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$act_in_punch_date."' ";
					}
					query($sql, $conn);
					$transaction_id = query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$act_in_punch_date."' ", $conn)->row['transaction_id'];
				}
				//echo $sql.';';
				//echo '<br />';
			} elseif($schedule_raw[0] == 'HD'){
				$shift_data = getshiftdata($schedule_raw[2], $conn);
				if(!isset($shift_data['shift_id'])){
					$shift_data = getshiftdata('1', $conn);
				}
				$shift_intime = $shift_data['in_time'];
				$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
				$shift_outtime_flexi = Date('H:i:s', strtotime($shift_outtime .' +60 minutes'));
				$shift_id = $shift_data['shift_id'];
				$shift_code = $shift_data['shift_code'];
				$shift_allowance = $shift_data['shift_allowance'];
				
				$act_shift_id = $shift_id;
				$act_shift_code = $shift_code;
				$act_shift_allowance = $shift_allowance;
				$act_shift_intime = $shift_intime;
				$act_shift_outtime = $shift_outtime;
				$act_shift_outtime_flexi = $shift_outtime_flexi;
				$act_shift_allowance = $shift_allowance;

				$act_intime = '00:00:00';
				$act_outtime = '00:00:00';
				$act_in_punch_date = $filter_date_start;
				$act_out_punch_date = $filter_date_start;
				$act_intimes = getrawattendance_in_time($rvalue['emp_code'], $dvalue['punch_date'], $shift_data, $conn);
				if(isset($act_intimes['punch_time'])) {
					$act_intime = $act_intimes['punch_time'];
					$act_in_punch_date = $act_intimes['punch_date'];
					/*
					$array = getshiftdata('0', $conn);
					$act_array = array();
					$act_array_min = array();
					$hour_array = array();
					$min_array = array();
					$time_array = array();
					$zero_hour_count = 0;
					foreach ($array as $akey => $avalue) {
						$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
						$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$avalue['in_time']));
						if($since_start->d == 0){
							$time_array[] = $since_start->h.':'.$since_start->i.':'.$since_start->s;
							$hour_array[] = $since_start->h;
							$act_array[] = $avalue;
							if($since_start->h == 0){
								$act_array_min[] = $avalue;
								$min_array[] = $since_start->i;
								$zero_hour_count ++;
							}
						}
					}
					if($zero_hour_count >= 2){
						$shift_work_hour = '0';
						if($min_array){
							$shift_work_hour = min($min_array);
						}
						$shift_worked_data = array();
						foreach ($min_array as $akey => $avalue) {
							if($avalue == $shift_work_hour){
								$shift_worked_data = $act_array_min[$akey];
							}
						}
					} else {
						$shift_work_hour = '0';
						if($hour_array){
							$shift_work_hour = min($hour_array);
						}
						$shift_worked_data = array();
						foreach ($hour_array as $akey => $avalue) {
							if($avalue == $shift_work_hour){
								$shift_worked_data = $act_array[$akey];
							}
						}	
					}
					if($shift_worked_data){
						$act_shift_intime = $shift_worked_data['in_time'];
						$act_shift_outtime = $shift_worked_data['out_time'];
						$act_shift_id = $shift_worked_data['shift_id'];
						$act_shift_code = $shift_worked_data['shift_code'];
						$act_shift_allowance = $shift_worked_data['shift_allowance'];
					}
					*/
				}
				$act_outtimes = getrawattendance_out_time($rvalue['emp_code'], $dvalue['punch_date'], $act_intime, $act_in_punch_date, $shift_data, $conn);
				if(isset($act_outtimes['punch_time'])) {
					$act_outtime = $act_outtimes['punch_time'];
					$act_out_punch_date = $act_outtimes['punch_date'];
				}
				if($act_intime == $act_outtime){
					$act_outtime = '00:00:00';
				}
				$abnormal_status = 0;
				$first_half = 0;
				$second_half = 0;
				$late_time = '00:00:00';
				$early_time = '00:00:00';
				$working_time = '00:00:00';
				$late_mark = 0;
				$early_mark = 0;
				$shift_early = 0;
				if($act_intime != '00:00:00'){
					$shift_intime_plus_one = Date('H:i:s', strtotime($act_shift_intime .' +60 minutes'));
					$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
					$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
					if($since_start->h > 12){
						$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
					}
					if($since_start->h > 0){
						$late_hour = $since_start->h;
						$late_min = $since_start->i;
						$late_sec = $since_start->s;
					} else {
						$late_hour = $since_start->h;
						$late_min = $since_start->i;
						$late_sec = $since_start->s;
					}
					$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
					if($since_start->invert == 1){
						$late_time = '00:00:00';
					} else {
						$late_mark = 1;
					}
					$shift_intime_minus_one = Date('H:i:s', strtotime($act_shift_intime .' -0 minutes'));
					$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
					$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
					if($since_start->h > 12){
						$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
					}
					if($since_start->invert == 1){
						$shift_early = 1;
					}
					$start_date = new DateTime($act_in_punch_date.' '.$act_shift_intime);
					$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_shift_outtime));
					$shift_working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);

					if($act_outtime != '00:00:00'){
						if($shift_early == 1){
							$shift_intime_minus_one = Date('H:i:s', strtotime($act_shift_intime .' -0 minutes'));
							if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
								$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
							} else {
								$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
							}
						} else {
							$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
						}
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
						if(strtotime($shift_working_time) == strtotime('04:00:00')){
							if( ($since_start->h == 4 && $since_start->i >= 00) || $since_start->h > 4){
								$first_half = 1;
								$second_half = 1;
							} elseif( ($since_start->h == 2 && $since_start->i >= 00) || $since_start->h > 2){
								$first_half = 1;
								$second_half = 0;
							} else {
								$first_half = 0;
								$second_half = 0;
							}
						}
						$working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
					} else {
						$first_half = 0;
						$second_half = 0;
					}
				} else {
					$first_half = 0;
					$second_half = 0;
				}
				$early_time = '00:00:00';
				$over_time = '00:00:00';
				$over_time_amount = '0';
				$over_time_esic = '0';
				$after_shift = 0;
				if($abnormal_status == 0){ //if abnormal status is zero calculate further 
					if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and actouttime 
						$start_date = new DateTime($act_in_punch_date.' '.$act_shift_outtime_flexi);
						$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
						if($since_start->invert == 0 && ($since_start->h > 0 || $since_start->i > 0)){
							$after_shift = 1;
						} else {
							$after_shift = 0;
						}

						$shift_outtime_minus_one = Date('H:i:s', strtotime($act_shift_outtime .' -0 minutes'));
						$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
						if($since_start->h > 12){
							$shift_outtime_minus_one = Date('H:i:s', strtotime($act_shift_outtime .' -0 minutes'));
							$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
						}
						$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
						if($since_start->invert == 0){
							$early_time = '00:00:00';
						} else {
							$early_mark = 1;
						}
						$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
						if($since_start->invert == 1){
							$over_time = '00:00:00';
						} else {
							if(strtotime($shift_working_time) == strtotime('04:00:00')){
								$shift_working_hours = '04:00:00';
								$start_date = new DateTime($act_in_punch_date.' '.$shift_working_hours);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$working_time));
								if($since_start->invert == 0){
									$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
								} else {
									$over_time = '00:00:00';
								}
							}
							/*
							if($late_time != '00:00:00'){
								$late_time_exp = explode(':', $late_time);
								$hour = $late_time_exp[0];
								$min = $late_time_exp[1];
								$hour_min = $hour.':'.$min;
								$minutes_compare = hoursToMinutes($hour_min);
								$over_time1 = $over_time;
								$over_time = Date('H:i:s', strtotime($over_time1 .' -'. $minutes_compare .' minutes'));
							} else {
								$over_time = $over_time;
							}
							$overtime_exp = explode(':', $over_time);
							$hour = $overtime_exp[0];
							$min = $overtime_exp[1];
							$hour_min = $hour.':'.$min;
							$minutes_compare = hoursToMinutes($hour_min);
							$over_time_hours = '';
							if($minutes_compare > 0 && $minutes_compare <= 44){
								$over_time_hours = '00';
							} elseif($minutes_compare >= 45 && $minutes_compare <= 104){
								$over_time_hours = '01';
							} elseif($minutes_compare >= 105 && $minutes_compare <= 164){
								$over_time_hours = '02';
							} elseif($minutes_compare >= 165 && $minutes_compare <= 224){
								$over_time_hours = '03';
							} elseif($minutes_compare >= 225 && $minutes_compare <= 284){
								$over_time_hours = '04';
							} elseif($minutes_compare >= 285 && $minutes_compare <= 344){
								$over_time_hours = '05';
							} elseif($minutes_compare >= 345 && $minutes_compare <= 405){
								$over_time_hours = '06';
							} elseif($minutes_compare >= 405 && $minutes_compare <= 464){
								$over_time_hours = '07';
							} elseif($minutes_compare >= 465 && $minutes_compare <= 524){
								$over_time_hours = '08';
							} elseif($minutes_compare >= 525 && $minutes_compare <= 584){
								$over_time_hours = '09';
							} elseif($minutes_compare >= 585 && $minutes_compare <= 644){
								$over_time_hours = '10';
							} elseif($minutes_compare >= 645 && $minutes_compare <= 704){
								$over_time_hours = '11';
							} elseif($minutes_compare >= 705 && $minutes_compare <= 764){
								$over_time_hours = '12';
							} elseif($minutes_compare >= 765 && $minutes_compare <= 824){
								$over_time_hours = '13';
							} elseif($minutes_compare >= 825 && $minutes_compare <= 884){
								$over_time_hours = '14';
							} elseif($minutes_compare >= 885 && $minutes_compare <= 944){
								$over_time_hours = '15';
							} elseif($minutes_compare >= 945 && $minutes_compare <= 1004){
								$over_time_hours = '16';
							}
							// if($emp_salary['basic'] > 0 && $emp_salary['vda'] > 0){
							// 	$over_time_amount_1 = (($emp_salary['basic'] + $emp_salary['vda']) / 8);
							// 	$over_time_amount = $over_time_amount_1 * 2 * $over_time_hours;
							// }
							$over_time_amount = '0.00';
							//$over_time = $over_time_hours.':00';
							$over_time_esic = round(($over_time_amount * 1.75) / 100);
							*/
						}
					}					
				} else {
					$first_half = 0;
					$second_half = 0;
				}
				$abs_stat = 0;
				if($working_time == '00:00:00'){
					//$late_time = '00:00:00';
					$early_time = '00:00:00';
					$over_time = '00:00:00';
				}
				if($first_half == 1 && $second_half == 1){
					$present_status = 1;
					$absent_status = 0;
				} elseif($first_half == 1 && $second_half == 0){
					$present_status = 0.5;
					$absent_status = 0.5;
				} elseif($first_half == 0 && $second_half == 1){
					$present_status = 0.5;
					$absent_status = 0.5;
				} else {
					$present_status = 0;
					$absent_status = 1;
				}

				$day = date('j', strtotime($filter_date_start));
				$month = date('n', strtotime($filter_date_start));
				$year = date('Y', strtotime($filter_date_start));
				$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$dvalue['punch_date']."' ";
				$trans_exist = query($trans_exist_sql, $conn);
				if($trans_exist->num_rows == 0){
					if($abs_stat == 1){
						$act_intime = '00:00:00';
						$act_outtime = '00:00:00';
						$abnormal_status = 0;
						$act_out_punch_date = $filter_date_start;
						$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_allowance` = '".$act_shift_allowance."', `act_shift_id` = '".$act_shift_id."', `act_shift_code` = '".$act_shift_code."', `act_shift_intime` = '".$act_shift_intime."', `act_shift_outtime` = '".$act_shift_outtime."', `act_shift_outtime_flexi` = '".$act_shift_outtime_flexi."', `act_shift_allowance` = '".$act_shift_allowance."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `late_mark` = '".$late_mark."', `early_time` = '".$early_time."', `early_mark` = '".$early_mark."', `over_time` = '".$over_time."', `over_time_amount` = '".$over_time_amount."', `over_time_esic` = '".$over_time_esic."', `after_shift` = '".$after_shift."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."' ";
					} else {
						$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_allowance` = '".$act_shift_allowance."', `act_shift_id` = '".$act_shift_id."', `act_shift_code` = '".$act_shift_code."', `act_shift_intime` = '".$act_shift_intime."', `act_shift_outtime` = '".$act_shift_outtime."', `act_shift_outtime_flexi` = '".$act_shift_outtime_flexi."', `act_shift_allowance` = '".$act_shift_allowance."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `late_mark` = '".$late_mark."', `early_time` = '".$early_time."', `early_mark` = '".$early_mark."', `over_time` = '".$over_time."', `over_time_amount` = '".$over_time_amount."', `over_time_esic` = '".$over_time_esic."', `after_shift` = '".$after_shift."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."' ";
					}
					query($sql, $conn);
					$transaction_id = getLastId($conn);
				} else {
					if($abs_stat == 1){
						$act_intime = '00:00:00';
						$act_outtime = '00:00:00';
						$abnormal_status = 0;
						$act_out_punch_date = $filter_date_start;
						$sql = "UPDATE `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_allowance` = '".$act_shift_allowance."', `act_shift_id` = '".$act_shift_id."', `act_shift_code` = '".$act_shift_code."', `act_shift_intime` = '".$act_shift_intime."', `act_shift_outtime` = '".$act_shift_outtime."', `act_shift_outtime_flexi` = '".$act_shift_outtime_flexi."', `act_shift_allowance` = '".$act_shift_allowance."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `late_mark` = '".$late_mark."', `early_time` = '".$early_time."', `early_mark` = '".$early_mark."', `over_time` = '".$over_time."', `over_time_amount` = '".$over_time_amount."', `over_time_esic` = '".$over_time_esic."', `after_shift` = '".$after_shift."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$act_in_punch_date."' ";
					} else {
						$sql = "UPDATE `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_allowance` = '".$act_shift_allowance."', `act_shift_id` = '".$act_shift_id."', `act_shift_code` = '".$act_shift_code."', `act_shift_intime` = '".$act_shift_intime."', `act_shift_outtime` = '".$act_shift_outtime."', `act_shift_outtime_flexi` = '".$act_shift_outtime_flexi."', `act_shift_allowance` = '".$act_shift_allowance."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `late_mark` = '".$late_mark."', `early_time` = '".$early_time."', `early_mark` = '".$early_mark."', `over_time` = '".$over_time."', `over_time_amount` = '".$over_time_amount."', `over_time_esic` = '".$over_time_esic."', `after_shift` = '".$after_shift."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$act_in_punch_date."' ";
					}
					query($sql, $conn);
					$transaction_id = query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$act_in_punch_date."' ", $conn)->row['transaction_id'];
				}
			}
			if($act_in_punch_date == $act_out_punch_date) {
				if($act_intime != '00:00:00' && $act_outtime != '00:00:00'){
					$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` >= '".$act_intime."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
					//echo $update;exit;
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($update, true)  . "\n");
					query($update, $conn);
				} elseif($act_intime != '00:00:00' && $act_outtime == '00:00:00') {
					$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` = '".$act_intime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
					//echo $update;exit;
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($update, true)  . "\n");
					query($update, $conn);
				} elseif($act_intime == '00:00:00' && $act_outtime != '00:00:00') {
					$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_out_punch_date."' AND `punch_time` = '".$act_outtime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
					//echo $update;exit;
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($update, true)  . "\n");
					query($update, $conn);
				}
				// $update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `emp_id` = '".$rvalue['emp_code']."' ";
				// //echo $update;exit;
				// query($update, $conn);
			} else {
				$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` >= '".$act_intime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($update, true)  . "\n");
				query($update, $conn);
				
				$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_out_punch_date."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($update, true)  . "\n");
				query($update, $conn);
			}
		}
		//$sql = "UPDATE `oc_transaction` SET `day_close_status` = '1' WHERE `date` = '".$filter_date_start."'";
		//query($sql, $conn);
	}
	$unprocessed = getUnprocessedLeaveTillDate_2($conn);
	foreach($unprocessed as $data) {
		if(strtotime($data['date']) < strtotime(date('Y-m-d'))){
			checkLeave($data['date'], $data['emp_id'], $conn, $handle);
		}
	}
	$sql = "SELECT `emp_id`, `date` FROM `oc_transaction` WHERE `leave_status` = '1' AND `absent_status` = '1' ORDER BY `date` ";
	$resultsss = query($sql, $conn);
	fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($resultsss, true)  . "\n");
	foreach($resultsss->rows as $rkeys => $rvalues){
		checkLeave1($rvalues['date'], $rvalues['emp_id'], $conn, $handle);	
	}
	$message = 'Data Pulled Successfully';
} else {
	$message = 'No Data Found / Connection issue !. Please try again later';
}
$conn->close();

function sortByOrder($a, $b) {
	$v1 = strtotime($a['fdate']);
	$v2 = strtotime($b['fdate']);
	return $v1 - $v2; // $v2 - $v1 to reverse direction
	// if ($a['punch_date'] == $b['punch_date']) {
		//return ($a['in_time'] > $b['in_time']) ? -1 : 1;;
	//}
	//return $a['punch_date'] - $b['punch_date'];
}

function GetDays($sStartDate, $sEndDate){  
	// Firstly, format the provided dates.  
	// This function works best with YYYY-MM-DD  
	// but other date formats will work thanks  
	// to strtotime().  
	$sStartDate = date("Y-m-d", strtotime($sStartDate));  
	$sEndDate = date("Y-m-d", strtotime($sEndDate));  
	// Start the variable off with the start date  
	$aDays[] = $sStartDate;  
	// Set a 'temp' variable, sCurrentDate, with  
	// the start date - before beginning the loop  
	$sCurrentDate = $sStartDate;  
	// While the current date is less than the end date  
	while($sCurrentDate < $sEndDate){  
	// Add a day to the current date  
	$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
		// Add this new day to the aDays array  
	$aDays[] = $sCurrentDate;  
	}
	// Once the loop has finished, return the  
	// array of days.  
	return $aDays;  
}

function array_sort($array, $on, $order=SORT_ASC){

	$new_array = array();
	$sortable_array = array();

	if (count($array) > 0) {
		foreach ($array as $k => $v) {
			if (is_array($v)) {
				foreach ($v as $k2 => $v2) {
					if ($k2 == $on) {
						$sortable_array[$k] = $v2;
					}
				}
			} else {
				$sortable_array[$k] = $v;
			}
		}

		switch ($order) {
			case SORT_ASC:
				asort($sortable_array);
				break;
			case SORT_DESC:
				arsort($sortable_array);
				break;
		}

		foreach ($sortable_array as $k => $v) {
			$new_array[$k] = $array[$k];
		}
	}

	return $new_array;
}
function getemployees_empty($punch_date, $conn) {
	$sql = "SELECT `emp_code`, `department_id`, `department`, `unit_id`, `unit`, `name` FROM `oc_employee` WHERE (DATE(`dol`) = '0000-00-00' OR DATE(`dol`) > '".$punch_date."') AND `is_new` = '0' ";
	$sql .= " ORDER BY `shift_type` ";		
	$query = query($sql, $conn);
	return $query->rows;
}
function getEmployees_dat($emp_code, $conn) {
	$sql = "SELECT `department`, `department_id`, `unit`, `unit_id`, `doj`, `name`, `emp_code` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."'";
	$query = query($sql, $conn);
	return $query->row;
}
function getempdata($emp_code, $punch_date, $conn) {
	$sql = "SELECT `emp_code`, `department_id`, `department`, `unit_id`, `unit`, `name`, `doj` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' AND (DATE(`dol`) = '0000-00-00' OR DATE(`dol`) > '".$punch_date."') ";
	$query = query($sql, $conn);
	return $query->row;
}
function getempsalary($emp_code, $conn){
	$sql = "SELECT `name`, `value` FROM `oc_employee_salary` WHERE `emp_code` = '".$emp_code."' AND (`name` = 'Basic' OR `name` = 'VDA')";
	$query = query($sql, $conn);
	$salary_data = array();
	$salary_data['basic'] = '0';
	$salary_data['vda'] = '0';
	foreach($query->rows as $ekey => $evalue){
		if($evalue['name'] == 'Basic'){
			$salary_data['basic'] = $evalue['value'];
		} elseif($evalue['name'] == 'VDA'){
			$salary_data['vda'] = $evalue['value'];
		}
	}
	// echo '<pre>';
	// print_r($salary_data);
	// exit;
	return $salary_data;	
}
function insert_attendance_data($data, $conn) {
	$sql = query("INSERT INTO `oc_attendance` SET `emp_id` = '".$data['employee_id']."', `card_id` = '".$data['card_id']."', `punch_date` = '".$data['punch_date']."', `punch_time` = '".$data['in_time']."', `device_id` = '".$data['device_id']."', `status` = '0', `download_date` = '".$data['download_date']."', `log_id` = '".$data['log_id']."' ", $conn);
}
function getrawattendance_group_date_custom($emp_code, $date, $conn) {
	$query = query("SELECT * FROM `oc_attendance` WHERE `punch_date` = '".$date."' AND `emp_id` = '".$emp_code."' GROUP by `punch_date` ", $conn);
	return $query->row;
}
function getshiftdata($shift_id, $conn) {
	$sql = "SELECT * FROM oc_shift WHERE 1=1 ";
	if($shift_id){
		$sql .= " AND `shift_id` = '".$shift_id."' ";
	}
	$query = query($sql, $conn);
	//$query = query("SELECT * FROM oc_shift WHERE `shift_id` = '".$shift_id."' ", $conn);
	if($query->num_rows > 0){
		if($shift_id){
			return $query->row;
		} else {
			return $query->rows;
		}
	} else {
		return array();
	}
}

function getrawattendance_in_time($emp_id, $punch_date, $shift_data, $conn) {
	//if($shift_data['next_day'] == '1'){
		$future_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
		//$query = query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."' OR `punch_date` = '".$future_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC", $conn);
		$query = query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."' OR `punch_date` = '".$future_date."') ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC", $conn);
		$array = $query->row;
	//} else {
		//$query = query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC", $conn);
		//$array = $query->row;
	//}
	// elseif($shift_data['previous_day'] == '1'){
	// 	$previous_date = date('Y-m-d', strtotime($punch_date .' -1 day'));
	// 	$query = query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."' OR `punch_date` = '".$previous_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC, `id` ASC ", $conn);
	// 	//$query = query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC", $conn);
	// 	$array = $query->row;
	// } 
	return $array;
}

function getrawattendance_out_time($emp_id, $punch_date, $act_intime, $act_punch_date, $shift_data, $conn) {
	//if($shift_data['next_day'] == '1'){
		$future_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
		//$query = query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."' OR `punch_date` = '".$future_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC", $conn);
		$query = query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."' OR `punch_date` = '".$future_date."') ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC", $conn);
	//} else { 
		//$query = query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC", $conn);
	//}
	// elseif($shift_data['previous_day'] == '1'){
	// 	$previous_date = date('Y-m-d', strtotime($punch_date .' -1 day'));
	// 	$query = query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."' OR `punch_date` = '".$previous_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC", $conn);
	// }
	$act_outtime = array();
	if($query->num_rows > 0){
		//echo '<pre>';
		//print_r($act_intime);

		$first_punch = $query->rows['0'];
		$array = $query->rows;
		$comp_array = array();
		$hour_array = array();
		
		//echo '<pre>';
		//print_r($array);

		foreach ($array as $akey => $avalue) {
			$start_date = new DateTime($act_punch_date.' '.$act_intime);
			$since_start = $start_date->diff(new DateTime($avalue['punch_date'].' '.$avalue['punch_time']));
			
			//echo '<pre>';
			//print_r($since_start);

			if($since_start->d == 0){
				$comp_array[] = $since_start->h.':'.$since_start->i.':'.$since_start->s;
				$hour_array[] = $since_start->h;
				$act_array[] = $avalue;
			}
		}

		//echo '<pre>';
		//print_r($hour_array);

		//echo '<pre>';
		//print_r($comp_array);

		foreach ($hour_array as $ckey => $cvalue) {
			if($cvalue > 15){
				unset($hour_array[$ckey]);
			}
		}

		//echo '<pre>';
		//print_r($hour_array);
		
		$act_outtimes = '0';
		if($hour_array){
			$act_outtimes = max($hour_array);
		}

		//echo '<pre>';
		//print_r($act_outtimes);

		foreach ($hour_array as $akey => $avalue) {
			if($avalue == $act_outtimes){
				$act_outtime = $act_array[$akey];
			}
		}
	}
	//echo '<pre>';
	//print_r($act_outtime);
	//exit;		
	return $act_outtime;
}
function getorderhistory($data, $conn){
	$sql = "SELECT * FROM `oc_attendance` WHERE `punch_date` = '".$data['punch_date']."' AND `punch_time` = '".$data['in_time']."' AND `emp_id` = '".$data['employee_id']."' ";
	$query = query($sql, $conn);
	if($query->num_rows > 0){
		return 1;
	} else {
		return 0;
	}
}

function getUnprocessedLeaveTillDate_2($conn) {
	$sql = "SELECT lt.*, (SELECT e.name FROM `oc_employee` e WHERE e.emp_code = lt.emp_id ) AS ename FROM `oc_leave_transaction` lt WHERE lt.p_status = '0' AND (lt.a_status = '1') ";
	$data = query($sql, $conn);
	if($data->rows) {
		return $data->rows;
	} else {
		return array();
	}
}

function checkLeave($date, $emp_id, $conn, $handle) {
	$query = query("SELECT * FROM oc_leave_transaction WHERE `emp_id` = '".$emp_id."' AND `date` = '".$date."' AND `p_status` = '0' AND `a_status` = '1' ", $conn);
	if($query->num_rows > 0) {
		if($query->row['type'] != ''){
			$trans_data = query("SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$emp_id."' AND `date` = '".$date."' ", $conn)->row;
			if($query->row['type'] == 'F'){
				query("UPDATE `oc_transaction` SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', `absent_status` = '0', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);		
				$sql_query = "UPDATE `oc_transaction` SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', absent_status = '0', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");

				query("UPDATE `oc_leave_transaction` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ", $conn);	
				$sql_query = "UPDATE `oc_leave_transaction` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");

				query("UPDATE `oc_leave_transaction_temp` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ", $conn);	
				$sql_query = "UPDATE `oc_leave_transaction_temp` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
			} elseif($query->row['type'] == '1'){
				query("UPDATE `oc_transaction` SET `firsthalf_status` = '".$query->row['leave_type']."', `leave_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);			
				if($trans_data['halfday_status'] != 0){
					query("UPDATE `oc_transaction` SET `secondhalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);
					$sql_query = "UPDATE `oc_transaction` SET `secondhalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
				} elseif($trans_data['secondhalf_status'] == 'COF'){
					query("UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);
					$sql_query = "UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
				} elseif($trans_data['secondhalf_status'] != '1' && $trans_data['secondhalf_status'] != '0'){
					query("UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);
					$sql_query = "UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
				} elseif($trans_data['secondhalf_status'] == '1'){
					query("UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);
					$sql_query = "UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
				} elseif($trans_data['secondhalf_status'] == '0'){
					query("UPDATE `oc_transaction` SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);
					$sql_query = "UPDATE `oc_transaction` SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
				} 
				query("UPDATE `oc_leave_transaction` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ", $conn);	
				$sql_query = "UPDATE `oc_leave_transaction` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");

				query("UPDATE `oc_leave_transaction_temp` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ", $conn);	
				$sql_query = "UPDATE `oc_leave_transaction_temp` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
			} elseif($query->row['type'] == '2'){
				query("UPDATE `oc_transaction` SET `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);			
				if($trans_data['halfday_status'] != 0){
					query("UPDATE `oc_transaction` SET `firsthalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);
					$sql_query = "UPDATE `oc_transaction` SET `firsthalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
				} elseif($trans_data['firsthalf_status'] == 'COF'){
					query("UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);
					$sql_query = "UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
				} elseif($trans_data['firsthalf_status'] != '1' && $trans_data['firsthalf_status'] != '0'){
					query("UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);
					$sql_query = "UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
				} elseif($trans_data['firsthalf_status'] == '1'){
					query("UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);
					$sql_query = "UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
				} elseif($trans_data['firsthalf_status'] == '0'){
					query("UPDATE `oc_transaction` SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);
					$sql_query = "UPDATE `oc_transaction` SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
				}
				query("UPDATE `oc_leave_transaction` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ", $conn);	
				$sql_query = "UPDATE `oc_leave_transaction` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");

				query("UPDATE `oc_leave_transaction_temp` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ", $conn);	
				$sql_query = "UPDATE `oc_leave_transaction_temp` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
			}
		} else {
			query("UPDATE `oc_transaction` SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', absent_status = '0', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);		
			$sql_query = "UPDATE `oc_transaction` SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', absent_status = '0', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
			fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");

			query("UPDATE `oc_leave_transaction` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);		
			$sql_query = "UPDATE `oc_leave_transaction` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
			fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");

			query("UPDATE `oc_leave_transaction_temp` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);	
			$sql_query = "UPDATE `oc_leave_transaction_temp` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
			fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
		}
	}	
}

function checkLeave1($date, $emp_id, $conn, $handle) {
	$query = query("SELECT * FROM oc_leave_transaction WHERE `emp_id` = '".$emp_id."' AND `date` = '".$date."' AND `a_status` = '1' ", $conn);
	if($query->num_rows > 0) {
		if($query->row['type'] != ''){
			$trans_data = query("SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$emp_id."' AND `date` = '".$date."' ", $conn)->row;
			if($query->row['type'] == 'F'){
				query("UPDATE `oc_transaction` SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', `absent_status` = '0', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);		
				$sql_query = "UPDATE `oc_transaction` SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', absent_status = '0', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");

				query("UPDATE `oc_leave_transaction` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ", $conn);	
				$sql_query = "UPDATE `oc_leave_transaction` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");

				query("UPDATE `oc_leave_transaction_temp` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ", $conn);	
				$sql_query = "UPDATE `oc_leave_transaction_temp` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
			} elseif($query->row['type'] == '1'){
				query("UPDATE `oc_transaction` SET `firsthalf_status` = '".$query->row['leave_type']."', `leave_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);			
				if($trans_data['halfday_status'] != 0){
					query("UPDATE `oc_transaction` SET `secondhalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);
					$sql_query = "UPDATE `oc_transaction` SET `secondhalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
				} elseif($trans_data['secondhalf_status'] == 'COF'){
					query("UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);
					$sql_query = "UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
				} elseif($trans_data['secondhalf_status'] != '1' && $trans_data['secondhalf_status'] != '0'){
					query("UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);
					$sql_query = "UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
				} elseif($trans_data['secondhalf_status'] == '1'){
					query("UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);
					$sql_query = "UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
				} elseif($trans_data['secondhalf_status'] == '0'){
					query("UPDATE `oc_transaction` SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);
					$sql_query = "UPDATE `oc_transaction` SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
				} 
				query("UPDATE `oc_leave_transaction` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ", $conn);	
				$sql_query = "UPDATE `oc_leave_transaction` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");

				query("UPDATE `oc_leave_transaction_temp` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ", $conn);	
				$sql_query = "UPDATE `oc_leave_transaction_temp` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
			} elseif($query->row['type'] == '2'){
				query("UPDATE `oc_transaction` SET `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);			
				if($trans_data['halfday_status'] != 0){
					query("UPDATE `oc_transaction` SET `firsthalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);
					$sql_query = "UPDATE `oc_transaction` SET `firsthalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
				} elseif($trans_data['firsthalf_status'] == 'COF'){
					query("UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);
					$sql_query = "UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
				} elseif($trans_data['firsthalf_status'] != '1' && $trans_data['firsthalf_status'] != '0'){
					query("UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);
					$sql_query = "UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
				} elseif($trans_data['firsthalf_status'] == '1'){
					query("UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);
					$sql_query = "UPDATE `oc_transaction` SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
				} elseif($trans_data['firsthalf_status'] == '0'){
					query("UPDATE `oc_transaction` SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);
					$sql_query = "UPDATE `oc_transaction` SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
				}
				query("UPDATE `oc_leave_transaction` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ", $conn);	
				$sql_query = "UPDATE `oc_leave_transaction` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");

				query("UPDATE `oc_leave_transaction_temp` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ", $conn);	
				$sql_query = "UPDATE `oc_leave_transaction_temp` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
				fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
			}
		} else {
			query("UPDATE `oc_transaction` SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', absent_status = '0', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);		
			$sql_query = "UPDATE `oc_transaction` SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', absent_status = '0', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
			fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");

			query("UPDATE `oc_leave_transaction` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);		
			$sql_query = "UPDATE `oc_leave_transaction` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
			fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");

			query("UPDATE `oc_leave_transaction_temp` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ", $conn);	
			$sql_query = "UPDATE `oc_leave_transaction_temp` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ";
			fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($sql_query, true)  . "\n");
		}
	}	
}
fclose($handle);
echo $message;exit;
?>