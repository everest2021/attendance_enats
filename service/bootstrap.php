<?php
require_once('../admin/config.php');
require_once(DIR_SYSTEM . 'engine/registry.php');
require_once(DIR_SYSTEM . "library/db.php");
require_once(DIR_SYSTEM . "engine/loader.php");
require_once(DIR_SYSTEM . "library/log.php");
require_once(DIR_SYSTEM . "library/template.php");
require_once(DIR_SYSTEM . "library/mail.php");
require_once(DIR_SYSTEM . "library/session.php");

// Registry
$registry = new Registry();

// Database 
$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, true);
$registry->set('db', $db);

// Log 
$registry->set('log', new Log('service.txt'));

// Session
$session = new Session();
$registry->set('session', $session);