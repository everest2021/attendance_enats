<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "db_attendance_econ";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}

function getLastId($conn){
	return $conn->insert_id;
}

function query($sql, $conn) {
	$query = $conn->query($sql);

	if (!$conn->errno){
		if (isset($query->num_rows)) {
			$data = array();

			while ($row = $query->fetch_assoc()) {
				$data[] = $row;
			}

			$result = new stdClass();
			$result->num_rows = $query->num_rows;
			$result->row = isset($data[0]) ? $data[0] : array();
			$result->rows = $data;

			unset($data);

			$query->close();

			return $result;
		} else{
			return true;
		}
	} else {
		throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
		exit();
	}
}

if(date('j') === '20') {
	$today_date = date('Y-m-d');
	$current_month = date('n', strtotime(date('Y-m-d') . ' +0 month'));
	if($current_month == 1){
		$current_year = date('Y') + 0;
	} else {
		$current_year = date('Y');
	}
	$employee_datas_sql = "SELECT `emp_code`, `department_id`, `department`, `unit`, `unit_id` FROM `oc_employee` WHERE `doc` <> '0000-00-00' AND (DATE(`dol`) = '0000-00-00' OR DATE(`dol`) > '".$today_date."') ";
	//echo $employee_datas_sql;exit;
	$employee_datas_sql = query($employee_datas_sql, $conn)->rows;
	// echo '<pre>';
	// print_r($employee_datas_sql);
	// exit;
	$bal_to_add = 2;
	foreach($employee_datas_sql as $ekey => $evalue){
		$is_exist_leaves = query("SELECT `leave_id`, `pl_acc` FROM `oc_leave` WHERE `emp_id` = '".$evalue['emp_code']."' AND `close_status` = '0' ", $conn);
		if($is_exist_leaves->num_rows > 0){
			//$update_sql = "UPDATE `oc_leave` SET `pl_acc` = (`pl_acc` + ".$bal_to_add.") WHERE `leave_id` = '".$is_exist_leaves->row['leave_id']."' ";
			// echo $update_sql;
			// echo '<br />';
			//exit;
			//query($update_sql, $conn);
			$insert_sql = "INSERT INTO `oc_leave_credit_transaction` SET `emp_code` = '".$evalue['emp_code']."', `leave_value` = '".$bal_to_add."', `leave_name` = 'PL', `month` = '".$current_month."', `year` = '".$current_year."', `dot` = '".$today_date."', `department` = '".$evalue['department']."', `department_id` = '".$evalue['department_id']."', `unit` = '".$evalue['unit']."', `unit_id` = '".$evalue['unit_id']."' ";
			// echo $insert_sql;
			// echo '<br />';
			// echo '<br />';
			//exit;
			query($insert_sql, $conn);
		}
	}
	//exit;
}

// if(date('j') === '18') {
// 	$today_date = '2018-04-20';//date('Y-m-d');
// 	$current_month = '4';//date('n', strtotime(date('Y-m-d') . ' +0 month'));
// 	if($current_month == 1){
// 		$current_year = '2018';//date('Y') + 0;
// 	} else {
// 		$current_year = '2018';//date('Y');
// 	}
// 	$employee_datas_sql = "SELECT `emp_code`, `department_id`, `department`, `unit`, `unit_id` FROM `oc_employee` WHERE `doc` <> '0000-00-00' AND (DATE(`dol`) = '0000-00-00' OR DATE(`dol`) > '".$today_date."') ";
// 	//echo $employee_datas_sql;exit;
// 	$employee_datas_sql = query($employee_datas_sql, $conn)->rows;
// 	// echo '<pre>';
// 	// print_r($employee_datas_sql);
// 	// exit;
// 	$bal_to_add = 2;
// 	foreach($employee_datas_sql as $ekey => $evalue){
// 		$is_exist_leaves = query("SELECT `leave_id`, `pl_acc` FROM `oc_leave` WHERE `emp_id` = '".$evalue['emp_code']."' AND `close_status` = '0' ", $conn);
// 		if($is_exist_leaves->num_rows > 0){
// 			$update_sql = "UPDATE `oc_leave` SET `pl_acc` = (`pl_acc` + ".$bal_to_add.") WHERE `leave_id` = '".$is_exist_leaves->row['leave_id']."' ";
// 			// echo $update_sql;
// 			// echo '<br />';
// 			//exit;
// 			//query($update_sql, $conn);
// 			$insert_sql = "INSERT INTO `oc_leave_credit_transaction` SET `emp_code` = '".$evalue['emp_code']."', `leave_value` = '".$bal_to_add."', `leave_name` = 'PL', `month` = '".$current_month."', `year` = '".$current_year."', `dot` = '".$today_date."', `department` = '".$evalue['department']."', `department_id` = '".$evalue['department_id']."', `unit` = '".$evalue['unit']."', `unit_id` = '".$evalue['unit_id']."' ";
// 			// echo $insert_sql;
// 			// echo '<br />';
// 			// echo '<br />';
// 			//exit;
// 			//query($insert_sql, $conn);
// 		}
// 	}
// 	echo 'Done';exit;
// 	exit;
// }
$conn->close();
echo 'Done';exit;
?>