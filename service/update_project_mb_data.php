<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$file = '/var/www/html/attendance_gml/service/service.txt';
$handle = fopen($file, 'a+'); 
// $message = 'tdcsfas';
// fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($message, true)  . "\n");
//fclose($handle); 
//echo 'aaaa';exit;
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Punchdataapi = new Punchdataapi();

$value = $Punchdataapi->putorder($datas, $handle);
fclose($handle); 
exit(json_encode($value));
class Punchdataapi {
  	public $conn;

  	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
  	}

  	public function escape($value, $conn){
		return $conn->real_escape_string($value);
	}

  	public function getLastId($conn){
		return $conn->insert_id;
	}

	public function query($sql, $conn) {
		$query = $conn->query($sql);

		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();

				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}

				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;

				unset($data);

				$query->close();

				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

  	public function putorder($data = array(), $handle){
		fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($data, true)  . "\n");
		
		if(!isset($data['project_mb_datass']) || $data['project_mb_datass'] == ''){
			$exist = 0;
	    	$project_mb_datass = array();
			$project_mb_datass = array(
				'0' => array(
					'location' => '12121212',
					'lat' => '11111',
					'lon' => '222222',
					'chainage_from' => '222222',
					'chainage_to' => '222222',
					'length' => '222222',
					'depth' => '222222',
					'strata' => '222222',
					'open_trenching_moiling_hdd' => '222222',
					'edge_of_road' => '222222',
					'center_of_road' => '222222',
					'sample_abd' => '222222',
					'culvert_bridge_road_railway_length' => '222222',
					'hdpe_duct' => '222222',
					'gi_glass_b' => '222222',
					'dwc' => '222222',
					'rcc' => '222222',
					'stone' => '222222',
					'pcc' => '222222',
					'couplers' => '222222',
					'jc_ptc_hh' => '222222',
					'rm' => '222222',
					'remarks' => '222222',
				),
			);
			//echo count($project_mb_datass);exit;
			$exist = 1;
		} else {
			$exist = 0;
			if(count($data['project_mb_datass']) > 0){
				$exist = 1;
			}	
			$project_mb_datass = $data['project_mb_datass'];
			foreach($project_mb_datass as $lkey => $lvalue){
				if(!isset($lvalue['location'])){
					$project_mb_datass[$lkey]['location'] = '';
				}
				if(!isset($lvalue['lat'])){
					$project_mb_datass[$lkey]['lat'] = '';
				}
				if(!isset($lvalue['lon'])){
					$project_mb_datass[$lkey]['lon'] = '';
				}
				if(!isset($lvalue['chainage_from'])){
					$project_mb_datass[$lkey]['chainage_from'] = '';
				}
				if(!isset($lvalue['chainage_to'])){
					$project_mb_datass[$lkey]['chainage_to'] = '';
				}
				if(!isset($lvalue['length'])){
					$project_mb_datass[$lkey]['length'] = '';
				}
				if(!isset($lvalue['depth'])){
					$project_mb_datass[$lkey]['depth'] = '';
				}
				if(!isset($lvalue['strata'])){
					$project_mb_datass[$lkey]['strata'] = '';
				}
				if(!isset($lvalue['open_trenching_moiling_hdd'])){
					$project_mb_datass[$lkey]['open_trenching_moiling_hdd'] = '';
				}
				if(!isset($lvalue['edge_of_road'])){
					$project_mb_datass[$lkey]['edge_of_road'] = '';
				}
				if(!isset($lvalue['center_of_road'])){
					$project_mb_datass[$lkey]['center_of_road'] = '';
				}
				if(!isset($lvalue['sample_abd'])){
					$project_mb_datass[$lkey]['sample_abd'] = '';
				}
				if(!isset($lvalue['culvert_bridge_road_railway_length'])){
					$project_mb_datass[$lkey]['culvert_bridge_road_railway_length'] = '';
				}
				if(!isset($lvalue['hdpe_duct'])){
					$project_mb_datass[$lkey]['hdpe_duct'] = '';
				}
				if(!isset($lvalue['gi_glass_b'])){
					$project_mb_datass[$lkey]['gi_glass_b'] = '';
				}
				if(!isset($lvalue['dwc'])){
					$project_mb_datass[$lkey]['dwc'] = '';
				}
				if(!isset($lvalue['rcc'])){
					$project_mb_datass[$lkey]['rcc'] = '';
				}
				if(!isset($lvalue['stone'])){
					$project_mb_datass[$lkey]['stone'] = '';
				}
				if(!isset($lvalue['pcc'])){
					$project_mb_datass[$lkey]['pcc'] = '';
				}
				if(!isset($lvalue['couplers'])){
					$project_mb_datass[$lkey]['couplers'] = '';
				}
				if(!isset($lvalue['jc_ptc_hh'])){
					$project_mb_datass[$lkey]['jc_ptc_hh'] = '';
				}
				if(!isset($lvalue['rm'])){
					$project_mb_datass[$lkey]['rm'] = '';
				}
				if(!isset($lvalue['remarks'])){
					$project_mb_datass[$lkey]['remarks'] = '';
				}
			}
		}

		if(!isset($data['emp_id'])){
	    	$emp_id = '1000';//'1000';
		} else {
			$emp_id = $data['emp_id'];
		}

		if(!isset($data['project_id'])){
	    	$project_id = '1';//'1';
		} else {
			$project_id = $data['project_id'];
		}

		if(!isset($data['project_name'])){
	    	$project_name = 'Project 1';
		} else {
			$project_name = $data['project_name'];
		}

		if(!isset($data['signature'])){
	    	$signature = '';
		} else {
			$signature = $data['signature'];
		}

		if(!isset($data['ofc_blowing'])){
	    	$ofc_blowing = '';
		} else {
			$ofc_blowing = $data['ofc_blowing'];
		}

		if(!isset($data['start_meter_reading'])){
	    	$start_meter_reading = '';
		} else {
			$start_meter_reading = $data['start_meter_reading'];
		}

		if(!isset($data['end_meter_reading'])){
	    	$end_meter_reading = '';
		} else {
			$end_meter_reading = $data['end_meter_reading'];
		}

		// echo '<pre>';
		// print_r($project_mb_datass);
		// exit;

		if($exist == 1 && $project_id != ''){
			$project_mb_datas_sql = "SELECT `id` FROM `oc_project_mb_data` WHERE `project_id` = '".$project_id."' ";
			$project_mb_datas = $this->query($project_mb_datas_sql, $this->conn);
			if($project_mb_datas->num_rows > 0){
				$project_mb_data = $project_mb_datas->row;
				$id = $project_mb_data['id'];
				$delete_proj_mb_point_sql = "DELETE FROM `oc_project_mb_point_data` WHERE `project_mb_data_id` = '".$id."' ";
				$this->query($delete_proj_mb_point_sql, $this->conn);				
			}
			$delete_proj_mb_sql = "DELETE FROM `oc_project_mb_data` WHERE `project_id` = '".$project_id."' ";
			$this->query($delete_proj_mb_sql, $this->conn);

			$last_proj_mb_no_sql = "SELECT `project_mb_no` FROM `oc_project_mb_data` ORDER BY `project_mb_no` DESC LIMIT 1";
			$last_proj_mb_nos = $this->query($last_proj_mb_no_sql, $this->conn);
			if($last_proj_mb_nos->num_rows > 0){
				$last_proj_mb_no = $last_proj_mb_nos->row;
				$project_mb_no = $last_proj_mb_no['project_mb_no'] + 1;
			} else {
				$project_mb_no = 1;
			}
			$project_data = $this->query("SELECT * FROM `oc_project` WHERE `id` = '".$project_id."' ", $this->conn)->row;

			$fold_name = $project_mb_no;	
			$raw_path = DIR_DOWNLOAD;
			$raw_upload_path = $raw_path.'project_mb_upload/'.$fold_name.'/';
			$raw_http_path = HTTP_DOWNLOAD.'project_mb_upload/'.$fold_name.'/';
			if(!file_exists(DIR_DOWNLOAD.'project_mb_upload/'.$fold_name)){
				@mkdir(DIR_DOWNLOAD.'project_mb_upload/'.$fold_name);
				@chmod(DIR_DOWNLOAD.'project_mb_upload/'.$fold_name, 0777);
			}
			$signature_name = '';
			$signature_path = '';
			if($signature != ''){
				$signature_name = 'Signature_'.$project_mb_no.'.png';
				$signature_path = $raw_http_path.$signature_name;
				$upload_path = $raw_upload_path.$signature_name;
				$signature_raw = explode(';', $signature);
				if(isset($signature_raw[1])){
					$signature_raw1 = explode(',', $signature_raw[1]);
					if(isset($signature_raw1[1])){
						$signature_base64 = base64_decode($signature_raw1[1]);
						file_put_contents($upload_path, $signature_base64);
						@chmod($upload_path, 0777);
					} else {
						$signature_name = '';
					}
				} else {
					$signature_name = '';
				}
			}

			$insert_sql = "INSERT INTO `oc_project_mb_data` SET 
								`emp_id` = '".$emp_id."',
								`ofc_blowing` = '".$ofc_blowing."',
								`start_meter_reading` = '".$start_meter_reading."',
								`end_meter_reading` = '".$end_meter_reading."',
								`project_mb_no` = '".$project_mb_no."',
								`date` = '".date('Y-m-d')."',
								`project_id` = '".$project_id."',
								`project_name` = '".$project_name."',
								`route_name` = '".$project_data['route_name']."',
								`link_section` = '".$project_data['link_section']."',
								`work_order_no` = '".$project_data['work_order_no']."',
								`contractor_name` = 'GML INFRASTRUCTURES PVT. LTD.',
								`doc_no` = 'FF/DEP/MB 1.0.1',
								`dept` = 'Deployment',
								`rev_no` = '1',
								`signature_name` = '".$signature_name."',
								`signature_path` = '".$signature_path."' ";
			$this->query($insert_sql, $this->conn);
			$project_mb_data_id = $this->getLastId($this->conn);

			foreach($project_mb_datass as $lkey => $lvalue){
				$insert_sql = "INSERT INTO `oc_project_mb_point_data` SET 
								`project_mb_data_id` = '".$project_mb_data_id."',
								`location` = '".$lvalue['location']."',
								`lat` = '".$lvalue['lat']."',
								`lon` = '".$lvalue['lon']."',
								`chainage_from` = '".$lvalue['chainage_from']."',
								`chainage_to` = '".$lvalue['chainage_to']."',
								`length` = '".$lvalue['length']."',
								`depth` = '".$lvalue['depth']."',
								`strata` = '".$lvalue['strata']."',
								`open_trenching_moiling_hdd` = '".$lvalue['open_trenching_moiling_hdd']."',
								`edge_of_road` = '".$lvalue['edge_of_road']."',
								`center_of_road` = '".$lvalue['center_of_road']."',
								`sample_abd` = '".$lvalue['sample_abd']."',
								`culvert_bridge_road_railway_length` = '".$lvalue['culvert_bridge_road_railway_length']."',
								`hdpe_duct` = '".$lvalue['hdpe_duct']."',
								`gi_glass_b` = '".$lvalue['gi_glass_b']."',
								`dwc` = '".$lvalue['dwc']."',
								`rcc` = '".$lvalue['rcc']."',
								`stone` = '".$lvalue['stone']."',
								`pcc` = '".$lvalue['pcc']."',
								`couplers` = '".$lvalue['couplers']."',
								`jc_ptc_hh` = '".$lvalue['jc_ptc_hh']."',
								`rm` = '".$lvalue['rm']."',
								`remarks` = '".$lvalue['remarks']."' ";
				$this->query($insert_sql, $this->conn);
			}
			$result = array();
			$result['success'] = 1;
			$result['project_id'] = $project_id;
			$result['project_name'] = $project_name;
			return $result;
		} else {
			$result['success'] = 0;
    		return $result;
		}
	}

  	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
	
}
?>