<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');

//$file = 'C:\xampp\htdocs\attendance_jml\service/service.txt';
$file = '/var/www/html/attendance_gml/service/service.txt';
$handle = fopen($file, 'a+'); 
// $message = 'tdcsfas';
// fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($message, true)  . "\n");
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Loginapi = new Loginapi();
$value = $Loginapi->getuserdata($datas, $handle);
fclose($handle);
exit(json_encode($value));
class Loginapi {
  	public $conn;

  	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
		$this->conn->set_charset("utf8");
		$this->conn->query("SET SQL_MODE = ''");
  	}

  	public function getLastId($conn){
		return $conn->insert_id;
	}

	public function escape($value) {
		return $this->conn->real_escape_string($value);
	}

	public function query($sql, $conn) {
		$query = $conn->query($sql);

		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();

				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}

				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;

				unset($data);

				$query->close();

				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

  	public function getuserdata($data = array(), $handle){
	    if(!isset($data['username'])){
	    	$data['username'] = '1000';
		}
		if(!isset($data['password'])){
	    	$data['password'] = '1000';
		}
		fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($data, true)  . "\n");
		$result = array();
	    $user_datas = $this->query("SELECT * FROM `oc_employee` WHERE `emp_code` = '" . $this->escape($data['username'], $this->conn) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->escape($data['password'], $this->conn) . "'))))) OR password = '" . $this->escape(md5($this->escape($data['password'], $this->conn))) . "') AND status = '1' ", $this->conn);
	    //$user_datas = $this->query("SELECT * FROM `oc_user` WHERE `status` = '1' AND `user_id` <> '1' AND `username` = '".$data['username']."' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->escape($data['password']) . "'))))) OR password = '" . $this->escape(md5($data['password'])) . "') ", $this->conn);
		if ($user_datas->num_rows > 0) {
			foreach($user_datas->rows as $nkey => $nvalue){
				if($nvalue['is_login'] == 0){
					$result['user_data'] = array(
						'user_id' => $nvalue['emp_code'],
						'username' => $nvalue['name'],
						'email' => $nvalue['email'],
						'displayName' => $nvalue['first_name'].' '.$nvalue['last_name'],
						'welcome_text' => 'Welcome ' . $nvalue['first_name'].' '.$nvalue['last_name'],
						'photo' => $nvalue['emp_photo_source'],
						'status' => $nvalue['status'],
						'exist_status' => 1,
					);
					$update_sql = "UPDATE `oc_employee` SET `is_login` = '1' WHERE `employee_id` = '".$nvalue['employee_id']."' ";
					$this->query($update_sql, $this->conn);
				} else {
					$result['user_data'] = array(
						'user_id' => '',
						'username' => '',
						'email' => '',
						'displayName' => '',
						'welcome_text' => '',
						'photo' => '',
						'status' => 0,
						'exist_status' => 2,
					);
				}
				/*
				$is_employee_exist = $this->query("SELECT `emp_code` FROM `oc_employee` WHERE `reporting_to` = '".$nvalue['emp_code']."' ", $this->conn);
				if($is_employee_exist->num_rows > 0){
				} else {
					$result['user_data'] = array(
						'user_id' => '',
						'username' => '',
						'email' => '',
						'displayName' => '',
						'welcome_text' => '',
						'photo' => '',
						'status' => 0,
						'exist_status' => 0,
					);
				}
				*/
			}	
	    } else {
	    	$result['user_data'] = array(
				'user_id' => '',
				'username' => '',
				'email' => '',
				'displayName' => '',
				'welcome_text' => '',
				'photo' => '',
				'status' => 0,
				'exist_status' => 0,
			);
	    }
	    // echo '<pre>';
	    // print_r($result);
	    // exit;
		return $result;
  	}

  	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}
?>