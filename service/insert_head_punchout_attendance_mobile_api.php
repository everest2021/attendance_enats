<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$file = '/var/www/html/attendance_gml/service/service.txt';
$handle = fopen($file, 'a+'); 
// $message = 'tdcsfas';
// fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($message, true)  . "\n");
//fclose($handle); 
//echo 'aaaa';exit;
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Punchdataapi = new Punchdataapi();

$value = $Punchdataapi->putorder($datas, $handle);
fclose($handle); 
exit(json_encode($value));
class Punchdataapi {
  	public $conn;

  	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
  	}

  	public function escape($value, $conn){
		return $conn->real_escape_string($value);
	}

  	public function getLastId($conn){
		return $conn->insert_id;
	}

	public function query($sql, $conn) {
		$query = $conn->query($sql);

		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();

				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}

				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;

				unset($data);

				$query->close();

				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

  	public function putorder($data = array(), $handle){
		fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r('in out time head', true)  . "\n");
		fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($data, true)  . "\n");
	   	
		if(!isset($data['project_id'])){
	    	$project_id = '';
		} else {
			$project_id = $data['project_id'];
		}

		if(!isset($data['lat_out'])){
	    	$latitude_out = '';
		} else {
			$latitude_out = $data['lat_out'];
		}

		if(!isset($data['lon_out'])){
	    	$longitude_out = '';
		} else {
			$longitude_out = $data['lon_out'];
		}

		if(!isset($data['emp_id'])){
	    	$emp_id = '';
		} else {
			$emp_id = $data['emp_id'];
		}

		if(!isset($data['description'])){
	    	$description = '';
		} else {
			$description = $data['description'];
		}

		if(!isset($data['location_datas']) || $data['location_datas'] == ''){
	    	$location_datas = array();
		} else {
			$location_datas = $data['location_datas'];
		}
		/*
		$location_datas = array(
			'0' => array(
				'lat' => '19.408817',
	            'lon' => '72.848643',
	            'date' => '2018-08-23',
	            'time' => '14:50:46',
			),
			'1' => array(
				'lat' => '19.408817',
	            'lon' => '72.848643',
	            'date' => '2018-08-23',
	            'time' => '14:52:46',
			),
			'2' => array(
				'lat' => '19.408817',
	            'lon' => '72.848643',
	            'date' => '2018-08-23',
	            'time' => '15:30:46',
			),
		);
		*/
		$dss = $this->query("SELECT * FROM `oc_project_transection` pt LEFT JOIN `oc_project` p ON(p.`id` = pt.`project_id`) WHERE p.`id` = '".$project_id."'", $this->conn);
		fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r("SELECT * FROM `oc_project_transection` pt LEFT JOIN `oc_project` p ON(p.`id` = pt.`project_id`) WHERE p.`id` = '".$project_id."'", true)  . "\n");
		fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($dss, true)  . "\n");
		if($dss->num_rows > 0){
			$ds = $dss->row;
			$dist = $this->distance($latitude_out, $longitude_out, $ds['latitude'], $ds['longitude'], "K");
			fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($dist, true)  . "\n");
			if($dist <= '3') {
				$today_date = date('Y-m-d');
				$today_time = date('H:i:s');
				$tran_datas = $this->query("SELECT * FROM `oc_transaction` WHERE `project_id` = '".$project_id."' AND `emp_id` = '".$emp_id."' AND `date` = '".$today_date."' ", $this->conn);
				
				if($tran_datas->num_rows != 0){
					$tran_data = $tran_datas->row;
					if($tran_data['leave_status'] != 0) {
						$firsthalf_status = $tran_data['firsthalf_status'];
						$secondhalf_status = $tran_data['secondhalf_status'];
					} elseif($tran_data['weekly_off'] != 0) {
						$firsthalf_status = 'WO';
						$secondhalf_status = 'WO';
					} elseif($tran_data['holiday_id'] != 0) {
						$firsthalf_status = 'HLD';
						$secondhalf_status = 'HLD';
					} else{
						$firsthalf_status = '1';
						$secondhalf_status = '1';
					}

					if($tran_data['act_intime'] != '00:00:00') {
						$start_date = new DateTime($today_date.' '.$tran_data['act_intime']);
						$since_start = $start_date->diff(new DateTime($today_date.' '.$today_time));
						$working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
					} else{
						$working_time = '00:00:00';
					}

					$this->query("UPDATE `oc_transaction` SET 
						`emp_id` =  '".$emp_id."',
						`latitude_out` =  '".$latitude_out."',
						`longitude_out` =  '".$longitude_out."',
						`present_status` = '1',
						`absent_status` = '0',
						`working_time` = '".$working_time."',
						`firsthalf_status` = '".$firsthalf_status."',
						`secondhalf_status` = '".$secondhalf_status."',
						`act_outtime` =  '".$today_time."',
						`description` =  '".$description."'
						WHERE `project_id` = '".$project_id."' AND `date` = '".$today_date."' AND `emp_id` =  '".$emp_id."' " , $this->conn);
					
					if(!empty($location_datas)){
						foreach($location_datas as $lkey => $lvalue){
							$location_datas[$lkey]['date'] = date('Y-m-d', strtotime($lvalue['date_time']));
							$location_datas[$lkey]['time'] = date('H:i:s', strtotime($lvalue['date_time']));
						}
						$last_date_time = '';
						foreach($location_datas as $lkey => $lvalue){
							$in = 0;
							if($lkey == 0){
								$last_date_time = $lvalue['date'].' '.$lvalue['time'];
								$in = 1;
								$location_datas_insert[] = $lvalue;
							} else {
								$current_date_time = $lvalue['date'].' '.$lvalue['time'];
								$start_date = new DateTime($last_date_time);
								$since_start = $start_date->diff(new DateTime($current_date_time));
								if(($since_start->h == 0 && $since_start->i >= 1) || $since_start->h > 1){
									$location_datas_insert[] = $lvalue;
									$last_date_time = $lvalue['date'].' '.$lvalue['time'];
								}
							}
						}
						$transaction_id = $tran_data['transaction_id'];
						foreach($location_datas_insert as $lkey => $lvalue){
							$insert_sql = "INSERT INTO `oc_transaction_punches` SET 
											`emp_id` = '".$emp_id."',
											`transaction_id` = '".$transaction_id."',
											`date` = '".$lvalue['date']."',
											`time` = '".$lvalue['time']."',
											`lat` = '".$lvalue['lat']."',
											`lon` = '".$lvalue['lon']."' ";
							$this->query($insert_sql, $this->conn);
						}
					}
					$result = array();
					$result['success'] = 1;
					return $result;
				} else {
					$result['success'] = 0;
		    		return $result;
		    	}
		   	} else {
				$result['success'] = 2;
				return $result;
			}
		} else {
			$result['success'] = 2;
			return $result;
		}
	}

	public function distance($latitude_out, $longitude_out, $latitude, $longitude, $unit) {

	  	$theta = $longitude_out - $longitude;
	  	$dist = sin(deg2rad($latitude_out)) * sin(deg2rad($latitude)) +  cos(deg2rad($latitude_out)) * cos(deg2rad($latitude)) * cos(deg2rad($theta));
	  	$dist = acos($dist);
	  	$dist = rad2deg($dist);
	  	$miles = $dist * 60 * 1.1515;
	  	$unit = strtoupper($unit);

	  	if ($unit == "K") {
	    	return ($miles * 1.609344);
	  	} else if ($unit == "N") {
	      	return ($miles * 0.8684);
	    } else {
	      	return $miles;
	    }
	}
	//echo distance(19.408158, 72.848712, 19.403765, 72.798928, "K") . " KM<br>";
	//exit;

  	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
	
}
?>