<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$file = '/var/www/html/attendance_gml/service/service.txt';
$handle = fopen($file, 'a+'); 
// $message = 'tdcsfas';
// fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($message, true)  . "\n");
//fclose($handle); 
//echo 'aaaa';exit;
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Punchdataapi = new Punchdataapi();

$value = $Punchdataapi->putorder($datas, $handle);
fclose($handle); 
exit(json_encode($value));
class Punchdataapi {
  	public $conn;

  	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
  	}

  	public function escape($value, $conn){
		return $conn->real_escape_string($value);
	}

  	public function getLastId($conn){
		return $conn->insert_id;
	}

	public function query($sql, $conn) {
		$query = $conn->query($sql);

		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();

				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}

				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;

				unset($data);

				$query->close();

				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

  	public function putorder($data = array(), $handle){
		fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($data, true)  . "\n");
		if(!isset($data['location_datas']) || $data['location_datas'] == ''){
	    	$location_datas = array();
		} else {
			$location_datas = $data['location_datas'];
		}

		if(!isset($data['emp_id'])){
	    	$emp_id = '';//'1000';
		} else {
			$emp_id = $data['emp_id'];
		}

		if(!isset($data['project_id'])){
	    	$project_id = '';//'1000';
		} else {
			$project_id = $data['project_id'];
		}

		if(!isset($data['description'])){
	    	$description = '';//'1000';
		} else {
			$description = $data['description'];
		}

		/*
		$location_datas = array(
			'0' => array(
				'lat' => '19.408817',
	            'lon' => '72.848643',
	            'date' => '2018-08-23',
	            'time' => '14:50:46',
			),
			'1' => array(
				'lat' => '19.408817',
	            'lon' => '72.848643',
	            'date' => '2018-08-23',
	            'time' => '14:52:46',
			),
			'2' => array(
				'lat' => '19.408817',
	            'lon' => '72.848643',
	            'date' => '2018-08-23',
	            'time' => '15:30:46',
			),
		);
		*/

		if(!empty($location_datas) || $project_id != ''){
			foreach($location_datas as $lkey => $lvalue){
				$location_datas[$lkey]['date'] = date('Y-m-d', strtotime($lvalue['date_time']));
				$location_datas[$lkey]['time'] = date('H:i:s', strtotime($lvalue['date_time']));
			}
			$last_date_time = '';
			$location_datas_insert = array();
			foreach($location_datas as $lkey => $lvalue){
				$in = 0;
				if($lkey == 0){
					$last_date_time = $lvalue['date'].' '.$lvalue['time'];
					$in = 1;
					$location_datas_insert[] = $lvalue;
				} else {
					$current_date_time = $lvalue['date'].' '.$lvalue['time'];
					$start_date = new DateTime($last_date_time);
					$since_start = $start_date->diff(new DateTime($current_date_time));
					if(($since_start->h == 0 && $since_start->i >= 1) || $since_start->h > 1){
						$location_datas_insert[] = $lvalue;
						$last_date_time = $lvalue['date'].' '.$lvalue['time'];
					}
				}
			}
			foreach($location_datas_insert as $lkey => $lvalue){
				$insert_sql = "INSERT INTO `oc_transaction_punches` SET 
								`emp_id` = '".$emp_id."',
								`date` = '".$lvalue['date']."',
								`time` = '".$lvalue['time']."',
								`lat` = '".$lvalue['lat']."',
								`lon` = '".$lvalue['lon']."' ";
				$this->query($insert_sql, $this->conn);
			}
			if($project_id != ''){
				$today_date = date('Y-m-d');
				$tran_datas = $this->query("SELECT * FROM `oc_transaction` WHERE `project_id` = '".$project_id."' AND `emp_id` = '".$emp_id."' AND `date` = '".$today_date."' ", $this->conn);
				if($tran_datas->num_rows > 0){
					$this->query("UPDATE `oc_transaction` SET `description` =  '".$description."' WHERE `project_id` = '".$project_id."' AND `date` = '".$today_date."' AND `emp_id` =  '".$emp_id."' ", $this->conn);	
				}
			}
			$result = array();
			$result['success'] = 1;
			$result['project_id'] = $project_id;
			return $result;
		} else {
			$result['success'] = 0;
    		return $result;
		}
	}

  	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
	
}
?>