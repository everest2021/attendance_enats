<?php
require_once('config.php'); 
session_start();
$servername = DB_HOSTNAME;//"localhost";
$username = DB_USERNAME;
$password = DB_PASSWORD;
$dbname = DB_DATABASE;
$baseurl = BASE_URL;


// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}

function getLastId($conn){
	return $conn->insert_id;
}

function query($sql, $conn) {
	$query = $conn->query($sql);

	if (!$conn->errno){
		if (isset($query->num_rows)) {
			$data = array();

			while ($row = $query->fetch_assoc()) {
				$data[] = $row;
			}

			$result = new stdClass();
			$result->num_rows = $query->num_rows;
			$result->row = isset($data[0]) ? $data[0] : array();
			$result->rows = $data;

			unset($data);

			$query->close();

			return $result;
		} else{
			return true;
		}
	} else {
		throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
		exit();
	}
}
$current_date = date('Y-m-d');
if(isset($_GET['start_date'])){
	$start_date = $_GET['start_date'];
} else {
	$start_date = '2018-08-01';
}
if(isset($_GET['end_date'])){
	$end_date = $_GET['end_date'];
} else {
	$end_date = '2018-10-30';//date('Y-m-d', strtotime($current_date . ' +1 day'));
}
if(isset($_GET['employee_list_string'])){
	$employee_list_string = $_GET['employee_list_string'];
} else {
	$employee_list_string = '';
}

// echo '<pre>';
// print_r($_GET);
// exit;

$update2 = "SELECT `date` FROM `oc_transaction` WHERE `month_close_status` = '1' ORDER BY `date` DESC LIMIT 1";
$day_status = query($update2, $conn);
if($day_status->num_rows > 0){
	$last_date = $day_status->row['date'];
	$compare_start_date = date('Y-m-d', strtotime($last_date . ' +1 day'));
} else {
	$compare_start_date = '2018-07-01';//date('Y-m-d');
}
$day = array();
$days = GetDays($start_date, $end_date);
foreach ($days as $dkey => $dvalue) {
	if(strtotime($dvalue) > strtotime($compare_start_date)){
		$dates = explode('-', $dvalue);
		$day[$dkey]['day'] = $dates[2];
		$day[$dkey]['date'] = $dvalue;
	}
}
// echo '<pre>';
// print_r($day);
// exit;
// $sql = "TRUNCATE TABLE `oc_transaction`";
// query($sql, $conn);
// $sql = "UPDATE `oc_attendance` SET `status` = '0', `transaction_id` = '0' ";
// query($sql, $conn);
//$sql = "TRUNCATE TABLE `oc_attendance`";
//query($sql, $conn);

//$filter_date_start = date('Y-m-d', strtotime($current_date . ' +1 day'));

$batch_id = '0';
foreach($day as $dkeys => $dvalues){
	$filter_date_start = $dvalues['date'];
	//echo $filter_date_start;exit;
	$results = getemployees($dvalues['date'], $employee_list_string, $conn);
	// echo '<pre>';
	// print_r($results);
	// exit;
	foreach ($results as $rkey => $rvalue) {
		if(isset($rvalue['name']) && $rvalue['name'] != ''){
			$emp_name = $rvalue['name'];
			$day_date = date('j', strtotime($filter_date_start));
			$month = date('n', strtotime($filter_date_start));
			$year = date('Y', strtotime($filter_date_start));
			$emp_is_head_data = ("SELECT * FROM `oc_employee` WHERE reporting_to = '".$rvalue['emp_code']."' ");
			$resp = query($emp_is_head_data, $conn);
			if ($resp->num_rows > 0) {
				$is_head = 1;
			} else {
				$is_head = 0;
			}
			// echo '<pre>';
			// print_r($rvalue);
			// exit();
			$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
			//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvalue['emp_code']."' ";
			$shift_schedule = query($update3, $conn)->row;
			$schedule_raw = explode('_', $shift_schedule[$day_date]);
			if(!isset($schedule_raw[2])){
				$schedule_raw[2]= 1;
			}
			// echo '<pre>';
			// print_r($schedule_raw);
			// exit;
			if($schedule_raw[0] == 'S'){
				$shift_data = getshiftdata($schedule_raw[1], $conn);
				if(isset($shift_data['shift_id'])){
					$shift_intime = $shift_data['in_time'];
					$shift_outtime = $shift_data['out_time'];
					$shift_outtime_flexi = Date('H:i:s', strtotime($shift_data['out_time'] .' +60 minutes'));
					$shift_id = $shift_data['shift_id'];
					$shift_code = $shift_data['shift_code'];
					
					$day = date('j', strtotime($filter_date_start));
					$month = date('n', strtotime($filter_date_start));
					$year = date('Y', strtotime($filter_date_start));

					$project_datas = query("SELECT pt.`id`, pt.`project_id` FROM `oc_project_transection` pt LEFT JOIN `oc_project_employee` pe ON(pe.`project_id` = pt.`id`) WHERE pe.`employee_id` = '".$rvalue['emp_code']."' AND '".$filter_date_start."' BETWEEN `start_date` AND `end_date` ", $conn)->rows;
					foreach($project_datas as $pkey => $pvalue){
						$trans_exist_sql = "SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' AND `project_id` = '".$pvalue['project_id']."' ";
						$trans_exist = query($trans_exist_sql, $conn);
						if($trans_exist->num_rows == 0){
							$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."',  `unit_id` = '".$rvalue['unit_id']."', `reporting_to` = '".$rvalue['reporting_to']."', `is_head` = '".$is_head."', `project_id` = '".$pvalue['project_id']."' ";
							// echo $sql.';';
							// echo '<br />';
							query($sql, $conn);
						} else {
							$sql = "UPDATE `oc_transaction` SET `project_id` = '".$pvalue['project_id']."' WHERE `transaction_id` = '".$trans_exist->row['transaction_id']."' ";
							// echo $sql.';';
							// echo '<br />';
							query($sql, $conn);
						}
					}
				} else {
					$shift_data = getshiftdata('1', $conn);
					$shift_intime = $shift_data['in_time'];
					$shift_outtime = $shift_data['out_time'];
					$shift_outtime_flexi = Date('H:i:s', strtotime($shift_data['out_time'] .' +60 minutes'));
					$shift_id = $shift_data['shift_id'];
					$shift_code = $shift_data['shift_code'];
					
					$day = date('j', strtotime($filter_date_start));
					$month = date('n', strtotime($filter_date_start));
					$year = date('Y', strtotime($filter_date_start));

					$project_datas = query("SELECT pt.`id`, pt.`project_id` FROM `oc_project_transection` pt LEFT JOIN `oc_project_employee` pe ON(pe.`project_id` = pt.`id`) WHERE pe.`employee_id` = '".$rvalue['emp_code']."' AND '".$filter_date_start."' BETWEEN `start_date` AND `end_date` ", $conn)->rows;
					foreach($project_datas as $pkey => $pvalue){
						$trans_exist_sql = "SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' AND `project_id` = '".$pvalue['project_id']."' ";
						$trans_exist = query($trans_exist_sql, $conn);
						if($trans_exist->num_rows == 0){
							$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."', `reporting_to` = '".$rvalue['reporting_to']."', `is_head` = '".$is_head."', `project_id` = '".$pvalue['project_id']."' ";
							//echo $sql.';';
							//echo '<br />';
							query($sql, $conn);
						} else {
							$sql = "UPDATE `oc_transaction` SET `project_id` = '".$pvalue['project_id']."' WHERE `transaction_id` = '".$trans_exist->row['transaction_id']."' ";
							// echo $sql.';';
							// echo '<br />';
							query($sql, $conn);
						}
					}
				}
			} elseif ($schedule_raw[0] == 'W') {
				$shift_data = getshiftdata($schedule_raw[2], $conn);
				if(!isset($shift_data['shift_id'])){
					$shift_data = getshiftdata('1', $conn);
				}
				$shift_intime = $shift_data['in_time'];
				$shift_outtime = $shift_data['out_time'];
				$shift_outtime_flexi = Date('H:i:s', strtotime($shift_data['out_time'] .' +60 minutes'));
				$shift_id = $shift_data['shift_id'];
				$shift_code = $shift_data['shift_code'];
				$act_intime = '00:00:00';
				$act_outtime = '00:00:00';
				
				$project_datas = query("SELECT pt.`id`, pt.`project_id` FROM `oc_project_transection` pt LEFT JOIN `oc_project_employee` pe ON(pe.`project_id` = pt.`id`) WHERE pe.`employee_id` = '".$rvalue['emp_code']."' AND '".$filter_date_start."' BETWEEN `start_date` AND `end_date` ", $conn)->rows;
				// echo '<pre>';
				// print_r($project_datas);
				// exit;
				foreach($project_datas as $pkey => $pvalue){
					$trans_exist_sql = "SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' AND `project_id` = '".$pvalue['project_id']."' ";
					$trans_exist = query($trans_exist_sql, $conn);
					if($trans_exist->num_rows == 0){
						$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."', `reporting_to` = '".$rvalue['reporting_to']."', `is_head` = '".$is_head."', `project_id` = '".$pvalue['project_id']."' ";
						//echo $sql.';';
						//echo '<br />';
						query($sql, $conn);
					} else {
						$sql = "UPDATE `oc_transaction` SET `project_id` = '".$pvalue['project_id']."' WHERE `transaction_id` = '".$trans_exist->row['transaction_id']."' ";
						// echo $sql.';';
						// echo '<br />';
						query($sql, $conn);
					}
				}
			} elseif ($schedule_raw[0] == 'H') {
				$shift_data = getshiftdata($schedule_raw[2], $conn);
				if(!isset($shift_data['shift_id'])){
					$shift_data = getshiftdata('1', $conn);
				}
				$shift_intime = $shift_data['in_time'];
				$shift_outtime = $shift_data['out_time'];
				$shift_outtime_flexi = Date('H:i:s', strtotime($shift_data['out_time'] .' +60 minutes'));
				$shift_id = $shift_data['shift_id'];
				$shift_code = $shift_data['shift_code'];
				$act_intime = '00:00:00';
				$act_outtime = '00:00:00';
				
				$project_datas = query("SELECT pt.`id`, pt.`project_id` FROM `oc_project_transection` pt LEFT JOIN `oc_project_employee` pe ON(pe.`project_id` = pt.`id`) WHERE pe.`employee_id` = '".$rvalue['emp_code']."' AND '".$filter_date_start."' BETWEEN `start_date` AND `end_date` ", $conn)->rows;
				foreach($project_datas as $pkey => $pvalue){
					$trans_exist_sql = "SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' AND `project_id` = '".$pvalue['project_id']."' ";
					$trans_exist = query($trans_exist_sql, $conn);
					if($trans_exist->num_rows == 0){
						$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."', `reporting_to` = '".$rvalue['reporting_to']."', `is_head` = '".$is_head."', `project_id` = '".$pvalue['project_id']."' ";
						//echo $sql.';';
						//echo '<br />';
						query($sql, $conn);
					} else {
						$sql = "UPDATE `oc_transaction` SET `project_id` = '".$pvalue['project_id']."' WHERE `transaction_id` = '".$trans_exist->row['transaction_id']."' ";
						// echo $sql.';';
						// echo '<br />';
						query($sql, $conn);
					}
				}
			} elseif ($schedule_raw[0] == 'HD') {
				$shift_data = getshiftdata($schedule_raw[2], $conn);
				if(!isset($shift_data['shift_id'])){
					$shift_data = getshiftdata('1', $conn);
				}
				$shift_intime = $shift_data['in_time'];
				$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
				$shift_outtime_flexi = Date('H:i:s', strtotime($shift_outtime .' +60 minutes'));
				//$shift_outtime = Date('H:i:s', strtotime($shift_outtime .' +00 minutes'));
				$act_intime = '00:00:00';
				$act_outtime = '00:00:00';

				$project_datas = query("SELECT pt.`id`, pt.`project_id` FROM `oc_project_transection` pt LEFT JOIN `oc_project_employee` pe ON(pe.`project_id` = pt.`id`) WHERE pe.`employee_id` = '".$rvalue['emp_code']."' AND '".$filter_date_start."' BETWEEN `start_date` AND `end_date` ", $conn)->rows;
				foreach($project_datas as $pkey => $pvalue){
					$trans_exist_sql = "SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' AND `project_id` = '".$pvalue['project_id']."' ";
					$trans_exist = query($trans_exist_sql, $conn);
					if($trans_exist->num_rows == 0){
						$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."',  `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '1', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."', `reporting_to` = '".$rvalue['reporting_to']."', `is_head` = '".$is_head."', `project_id` = '".$pvalue['project_id']."' ";
						//echo $sql.';';
						//echo '<br />';
						query($sql, $conn);
					} else {
						$sql = "UPDATE `oc_transaction` SET `project_id` = '".$pvalue['project_id']."' WHERE `transaction_id` = '".$trans_exist->row['transaction_id']."' ";
						// echo $sql.';';
						// echo '<br />';
						query($sql, $conn);
					}
				}
			}
		}	
	}
}
$conn->close();
if(isset($_GET['project_assignment'])){
	$url = $baseurl.'/admin/index.php?route=catalog/project_assignment&token='.$_SESSION['token'];
	header('Location: ' . $url);
} elseif(isset($_GET['home'])){
	$_SESSION['success'] = 'Blank Data Inserted Successfully';
	$url = $baseurl.'/admin/index.php?route=common/home&token='.$_SESSION['token'];
	header('Location: ' . $url);
}
echo 'Done';exit;

function sortByOrder($a, $b) {
	$v1 = strtotime($a['fdate']);
	$v2 = strtotime($b['fdate']);
	return $v1 - $v2; // $v2 - $v1 to reverse direction
	// if ($a['punch_date'] == $b['punch_date']) {
		//return ($a['in_time'] > $b['in_time']) ? -1 : 1;;
	//}
	//return $a['punch_date'] - $b['punch_date'];
}

function GetDays($sStartDate, $sEndDate){  
	// Firstly, format the provided dates.  
	// This function works best with YYYY-MM-DD  
	// but other date formats will work thanks  
	// to strtotime().  
	$sStartDate = date("Y-m-d", strtotime($sStartDate));  
	$sEndDate = date("Y-m-d", strtotime($sEndDate));  
	// Start the variable off with the start date  
	$aDays[] = $sStartDate;  
	// Set a 'temp' variable, sCurrentDate, with  
	// the start date - before beginning the loop  
	$sCurrentDate = $sStartDate;  
	// While the current date is less than the end date  
	while($sCurrentDate < $sEndDate){  
	// Add a day to the current date  
	$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
		// Add this new day to the aDays array  
	$aDays[] = $sCurrentDate;  
	}
	// Once the loop has finished, return the  
	// array of days.  
	return $aDays;  
}

function array_sort($array, $on, $order=SORT_ASC){

	$new_array = array();
	$sortable_array = array();

	if (count($array) > 0) {
		foreach ($array as $k => $v) {
			if (is_array($v)) {
				foreach ($v as $k2 => $v2) {
					if ($k2 == $on) {
						$sortable_array[$k] = $v2;
					}
				}
			} else {
				$sortable_array[$k] = $v;
			}
		}

		switch ($order) {
			case SORT_ASC:
				asort($sortable_array);
				break;
			case SORT_DESC:
				arsort($sortable_array);
				break;
		}

		foreach ($sortable_array as $k => $v) {
			$new_array[$k] = $array[$k];
		}
	}

	return $new_array;
}
function getemployees($punch_date, $employee_list_string, $conn) {
	$sql = "SELECT `emp_code`, `department_id`, `department`, `unit_id`, `unit`, `name` , `reporting_to` FROM `oc_employee` WHERE (DATE(`dol`) = '0000-00-00' OR DATE(`dol`) > '".$punch_date."') ";
	if($employee_list_string != ''){
		$sql .= " AND `emp_code` IN (" . $employee_list_string . ") ";
	}
	$sql .= " ORDER BY `shift_type` ";		
	//echo $sql;exit;
	$query = query($sql, $conn);
	return $query->rows;
}
function getshiftdata($shift_id, $conn) {
	$query = query("SELECT * FROM oc_shift WHERE `shift_id` = '".$shift_id."' ", $conn);
	if($query->num_rows > 0){
		return $query->row;
	} else {
		return array();
	}
}
echo 'Done';exit;
?>