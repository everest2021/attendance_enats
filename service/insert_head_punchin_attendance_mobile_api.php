<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$file = '/var/www/html/attendance_gml/service/service.txt';
$handle = fopen($file, 'a+'); 
// $message = 'tdcsfas';
// fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($message, true)  . "\n");
//fclose($handle); 
//echo 'aaaa';exit;
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Punchdataapi = new Punchdataapi();

$value = $Punchdataapi->putorder($datas, $handle);
fclose($handle); 
exit(json_encode($value));
class Punchdataapi {
  	public $conn;

  	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
  	}

  	public function escape($value, $conn){
		return $conn->real_escape_string($value);
	}

  	public function getLastId($conn){
		return $conn->insert_id;
	}

	public function query($sql, $conn) {
		$query = $conn->query($sql);

		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();

				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}

				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;

				unset($data);

				$query->close();

				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

  	public function putorder($data = array(), $handle){
		fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($data, true)  . "\n");
	   	
		if(!isset($data['project_id'])){
	    	$project_id = '';
		} else {
			$project_id = $data['project_id'];
		}

		if(!isset($data['lat_in'])){
	    	$latitude_in = '';
		} else {
			$latitude_in = $data['lat_in'];
		}

		if(!isset($data['lon_in'])){
	    	$longitude_in = '';
		} else {
			$longitude_in = $data['lon_in'];
		}

		if(!isset($data['emp_id'])){
	    	$emp_id = '';
		} else {
			$emp_id = $data['emp_id'];
		}

		if(!isset($data['description'])){
	    	$description = '';
		} else {
			$description = $data['description'];
		}

		$dss = $this->query("SELECT * FROM `oc_project_transection` pt LEFT JOIN `oc_project` p ON(p.`id` = pt.`project_id`) WHERE p.`id` = '".$project_id."'", $this->conn);
		fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($dss, true)  . "\n");
		if($dss->num_rows > 0){
			$ds = $dss->row;
			$dist = $this->distance($latitude_in, $longitude_in, $ds['latitude'], $ds['longitude'], "K");
			fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($dist, true)  . "\n");
			if($dist <= '3') {
				$today_date = date('Y-m-d');
				$today_time = date('H:i:s');
				$punchin_done = $this->query("SELECT * FROM `oc_transaction` WHERE `project_id` = '".$project_id."' AND `emp_id` = '".$emp_id."' AND `date` = '".$today_date."' ", $this->conn);
				if($punchin_done->num_rows != 0){
					$this->query("UPDATE `oc_transaction` SET 
						`latitude_in` =  '".$latitude_in."',
						`longitude_in` =  '".$longitude_in."',
						`act_intime` =  '".$today_time."',
						`description` =  '".$description."'
						 WHERE `project_id` = '".$project_id."' AND `date` = '".$today_date."' AND `emp_id` =  '".$emp_id."'", $this->conn);

					$result = array();
					$result['success'] = 1;
					fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($result, true)  . "\n");
					return $result;
				} else {
					$result['success'] = 0;
			    	fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($result, true)  . "\n");
			    	return $result;
			    }
			} else {
				$result['success'] = 2;
		    	fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($result, true)  . "\n");
		    	return $result;
			} 	
		} else {
			$result['success'] = 2;
		    fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($result, true)  . "\n");
		    return $result;
		}
	}

	public function distance($latitude_in, $longitude_in, $latitude, $longitude, $unit) {

	  	$theta = $longitude_in - $longitude;
	  	$dist = sin(deg2rad($latitude_in)) * sin(deg2rad($latitude)) +  cos(deg2rad($latitude_in)) * cos(deg2rad($latitude)) * cos(deg2rad($theta));
	  	$dist = acos($dist);
	  	$dist = rad2deg($dist);
	  	$miles = $dist * 60 * 1.1515;
	  	$unit = strtoupper($unit);

	  	if ($unit == "K") {
	    	return ($miles * 1.609344);
	  	} else if ($unit == "N") {
	      	return ($miles * 0.8684);
	    } else {
	      	return $miles;
	    }
	}

  	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
	
}
?>