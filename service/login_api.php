<?php 
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once("bootstrap.php");
$loginApi = new loginApi($registry);
//$data = file_get_contents('php://input');
//$datas = json_decode($data,true);
$datas = $_GET;
$value = $loginApi->postdata($datas);
exit(json_encode($value));

class loginApi {
  public $db;
  public $log;
  public $url;
  public $session;

  public function __construct($registry) {
    $this->db = $registry->get('db');
    $this->log = $registry->get('log');
    $this->url = $registry->get('url');
    $this->session = $registry->get('session');
  }

  public function postdata($data) {
    $this->log->write(print_r($data,true));
    
    unset($this->session->data['user_id']);
    unset($this->session->data['emp_code']);
    unset($this->session->data['is_dept']);
    unset($this->session->data['dept_name']);
    unset($this->session->data['d_emp_id']);
    unset($this->session->data['is_user']);
    unset($this->session->data['is_super']);
    unset($this->session->data['is_super1']);
    unset($this->session->data['dept_names']);
    unset($this->session->data['unit']);
    $user_query = $this->db->query("SELECT `user_id`, `is_super` FROM `oc_user` WHERE `user_id` = '".$data['user_id']."' ");
    //$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE user_id = '1'");
    if ($user_query->num_rows) {
      $this->session->data['user_id'] = $user_query->row['user_id'];
      if($user_query->row['is_super'] == '1'){
        $this->session->data['is_super1'] = $user_query->row['is_super'];
      }
      $this->session->data['token'] = md5(mt_rand());
      $response['success'] = 1;
      $response['message'] = 'Login Successfull';
    } else {
      $user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "employee WHERE emp_code = '" . $this->db->escape($data['user_id']) . "' AND status = '1' ");
      if ($user_query->num_rows) {
        $this->session->data['user_id'] = $user_query->row['emp_code'];
        if($user_query->row['is_super'] == '1'){
          $this->session->data['is_super'] = $user_query->row['is_super'];
          $this->session->data['d_emp_id'] = $user_query->row['emp_code'];
        } elseif($user_query->row['is_dept'] == '1'){
          $this->session->data['is_dept'] = $user_query->row['is_dept'];
          $this->session->data['dept_name'] = $user_query->row['department'];
          $this->session->data['d_emp_id'] = $user_query->row['emp_code'];
          $this->session->data['dept_names'] = $user_query->row['dept_head_list'];
          $this->session->data['unit'] = $user_query->row['unit'];
        } else {
          $this->session->data['is_user'] = '1';
          $this->session->data['emp_code'] = $user_query->row['emp_code'];
        }
        $this->session->data['token'] = md5(mt_rand());
        $response['success'] = 1;
        $response['message'] = 'Login Successfull';
      } else {
        $response['success'] = 0;
        $response['message'] = 'User Not Found, Please Create it';
      }
    }
    if($response['success'] == 1){
      $url = 'http://localhost:8012/attendance_econ/admin/index.php?route=catalog/employee1';
      header('Location: ' . $url);
    } else {
      return $response;
    }
  }

  /**
   * Redirect with POST data.
   *
   * @param string $url URL.
   * @param array $post_data POST data. Example: array('foo' => 'var', 'id' => 123)
   * @param array $headers Optional. Extra headers to send.
   */
  public function redirect_post($url, $data, $headers = array()) {
    $params = array(
        'http' => array(
            'method' => 'POST',
            'content' => http_build_query($data)
        )
    );
    if (!is_null($headers)) {
        $params['http']['header'] = '';
        foreach ($headers as $k => $v) {
            $params['http']['header'] .= "$k: $v\n";
        }
    }
    $ctx = stream_context_create($params);
    $fp = @fopen($url, 'rb', false, $ctx);
    if ($fp) {
        echo @stream_get_contents($fp);
        die();
    } else {
        // Error
        throw new Exception("Error loading '$url', $php_errormsg");
    }
  }
}
?>