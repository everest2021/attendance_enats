<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');

$file = '/var/www/html/attendance_gml/service/service.txt';
$handle = fopen($file, 'a+'); 
// $message = 'tdcsfas';
// fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($message, true)  . "\n");
//fclose($handle); 
//echo 'aaaa';exit;
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemdataapi = new Itemdataapi();
$value = $Itemdataapi->getitemdata($datas, $handle);
fclose($handle); 
exit(json_encode($value));
class Itemdataapi {
	public $conn;

	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}

	public function getLastId($conn){
		return $conn->insert_id;
	}

	public function escape($value, $conn){
		return $conn->real_escape_string($value);
	}

	public function query($sql, $conn) {
		$query = $conn->query($sql);

		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();

				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}

				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;

				unset($data);

				$query->close();

				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitemdata($data = array(), $handle){
		if(!isset($data['emp_id'])){
			$data['emp_id'] = '1';
		}
		fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($data, true)  . "\n");
		$result = array();
		$expense_datas=$this->query("SELECT * FROM `oc_expenses_emp_transaction` WHERE `emp_id` = '".$data['emp_id']."' ORDER BY `date` DESC ",$this->conn);
		if($expense_datas->num_rows > 0){
			foreach($expense_datas->rows as $nkey => $nvalue){
				$result['expense_datas'][] = array(
					'id' => $nvalue['id'],
					'type' => $nvalue['type'],
					'description' => $nvalue['description'],
					'emp_name' => $nvalue['emp_name'],
					'amount' => $nvalue['amount'],
					'date' => date('d-M-Y', strtotime($nvalue['date'])),
				);
			}
			$result['success'] = 1;
		} else {
			$result['success'] = 0;	
		}
		// echo '<pre>';
		// print_r($result);
		// exit;
		fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($result, true)  . "\n");
		return $result;
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}
?>