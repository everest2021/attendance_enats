<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');

$file = '/var/www/html/attendance_gml/service/service.txt';

$handle = fopen($file, 'a+'); 
// $message = 'tdcsfas';
// fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($message, true)  . "\n");
//fclose($handle); 
//echo 'aaaa';exit;
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Trandataapi = new Trandataapi();
$value = $Trandataapi->gettrandata($datas, $handle);
fclose($handle); 
exit(json_encode($value));
class Trandataapi {
	public $conn;

	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}

	public function getLastId($conn){
		return $conn->insert_id;
	}

	public function escape($value, $conn){
		return $conn->real_escape_string($value);
	}

	public function query($sql, $conn) {
		$query = $conn->query($sql);

		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();

				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}

				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;

				unset($data);

				$query->close();

				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function gettrandata($data = array(), $handle){
		if(!isset($data['emp_id'])){
	    	$data['emp_id'] = '1000';
		}

		if(!isset($data['project_id'])){
	    	$data['project_id'] = '1';
		}

		$today_date = date('Y-m-d');

		$b_datas=$this->query("SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$data['emp_id']."' AND `project_id` = '".$data['project_id']."' AND `date` = '".$today_date."' " ,$this->conn);

		//echo "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$data['emp_id']."' AND `project_id` = '".$data['project_id']."'";
		//exit;

		if($b_datas->num_rows > 0){
			$data = $b_datas->row;
			if($data['latitude_in'] != '') {
				$latitude_in = $data['latitude_in'];
			} else {
				$latitude_in = '0';
			}
			if($data['longitude_in'] != '') {
				$longitude_in = $data['longitude_in'];	
			} else {
				$longitude_in = '0';
			}
			if($data['latitude_out'] != '') {
				$latitude_out = $data['latitude_out'];
			} else {
				$latitude_out = '0';
			}
			if($data['longitude_out'] != '') {
				$longitude_out = $data['longitude_out'];	
			} else {
				$longitude_out = '0';
			}
			$result['latitude_in'] = $latitude_in;
			$result['longitude_in'] = $longitude_in;
			$result['latitude_out'] = $latitude_out;
			$result['longitude_out'] = $longitude_out;
			if($data['act_intime'] != '00:00:00') {
				$open_status ='1';
			} else {
				$open_status ='0';
			}
			$result['open_status'] = $open_status;
			$result['description'] = $data['description'];
			$result['project_id'] = $data['project_id'];
			$result['status'] = '1';
		} else {
			$result['status'] = '0';
		}

		 /*echo '<pre>';
		 print_r($result);
		 exit;*/
		fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($result, true)  . "\n");
		return $result;
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}
?>