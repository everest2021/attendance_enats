<?php 
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once("bootstrap.php");
$loginApi = new loginApi($registry);
//$data = file_get_contents('php://input');
//$datas = json_decode($data,true);
$datas = $_GET;
$value = $loginApi->postdata();
exit(json_encode($value));

class loginApi {
  public $db;
  public $log;
  public $url;
  public $session;

  public function __construct($registry) {
    $this->db = $registry->get('db');
    $this->log = $registry->get('log');
    $this->url = $registry->get('url');
    $this->session = $registry->get('session');
  }

  public function postdata() {
    unset($this->session->data['token']);
    unset($this->session->data['user_id']);
    unset($this->session->data['emp_code']);
    unset($this->session->data['is_dept']);
    unset($this->session->data['dept_name']);
    unset($this->session->data['d_emp_id']);
    unset($this->session->data['is_user']);
    unset($this->session->data['is_super']);
    unset($this->session->data['is_super1']);
    unset($this->session->data['dept_names']);
    unset($this->session->data['unit']);
    $response['success'] = 1;
    return $response;
  }

  /**
   * Redirect with POST data.
   *
   * @param string $url URL.
   * @param array $post_data POST data. Example: array('foo' => 'var', 'id' => 123)
   * @param array $headers Optional. Extra headers to send.
   */
  public function redirect_post($url, $data, $headers = array()) {
    $params = array(
        'http' => array(
            'method' => 'POST',
            'content' => http_build_query($data)
        )
    );
    if (!is_null($headers)) {
        $params['http']['header'] = '';
        foreach ($headers as $k => $v) {
            $params['http']['header'] .= "$k: $v\n";
        }
    }
    $ctx = stream_context_create($params);
    $fp = @fopen($url, 'rb', false, $ctx);
    if ($fp) {
        echo @stream_get_contents($fp);
        die();
    } else {
        // Error
        throw new Exception("Error loading '$url', $php_errormsg");
    }
  }
}
?>