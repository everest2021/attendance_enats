<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
//$file = '/var/www/html/hotel/hotel_api/service.txt';
$file = '/var/www/html/attendance_gml/service/service.txt';
$handle = fopen($file, 'a+'); 
// $message = 'tdcsfas';
// fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($message, true)  . "\n");
//fclose($handle); 
//echo 'aaaa';exit;
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemdataapi = new Itemdataapi();

$value = $Itemdataapi->putorder($datas, $handle);
fclose($handle); 
exit(json_encode($value));
class Itemdataapi {
  	public $conn;

  	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
  	}

  	public function escape($value, $conn){
		return $conn->real_escape_string($value);
	}

  	public function getLastId($conn){
		return $conn->insert_id;
	}

	public function query($sql, $conn) {
		$query = $conn->query($sql);

		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();

				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}

				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;

				unset($data);

				$query->close();

				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

  	public function putorder($data = array(), $handle){
		fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($data, true)  . "\n");
	   	
	   	if(!isset($data['date'])){
	   		$date = '';
	   	} else {
	   		$date = $data['date'];
	   	}
	   	if(!isset($data['type'])){
	   		$type_id = '';
	   	} else {
	   		$type_id = $data['type'];
	   	}
	   	if(!isset($data['amount'])){
	   		$amount = '';
	   	} else {
	   		$amount = $data['amount'];
	   	}
	   	if(!isset($data['description'])){
	    	$description = '';
		} else {
			$description = $data['description'];
		}
		if(!isset($data['photo'])){
	    	$photo = '';
		} else {
			$photo = $data['photo'];
		}
		if(!isset($data['user_id'])){
	    	$user_id = '';
		} else {
			$user_id = $data['user_id'];
		}

		$in1 = 0;
		$result['error_user_id'] = 0;
		if($user_id == '' || $user_id == '0'){
			$in1 = 1;
			$result['error_user_id'] = 1;
		}

		$in = 0;
		$result['error_date'] = 0;
		if($date == '' || $date == '0000-00-00' || $date == '00-00-0000'){
			$in = 1;
			$result['error_date'] = 1;
		}
		$result['error_amount'] = 0;
		if($type_id == '' || $type_id == '0'){
			$in = 1;
			$result['error_type'] = 1;
		}
		if($amount == '' || $amount == '0'){
			$in = 1;
			$result['error_amount'] = 1;
		}
		$result['error_description'] = 0;
		if($description == ''){
			//$in = 1;
			//$result['error_description'] = 1;
		}
		$result['error_photo'] = 0;
		if($photo == ''){
			//$in = 1;
			//$result['error_photo'] = 1;
		}
		
	    if($in == 0 && $in1 == 0){
		   	//$booking_done = $this->query("SELECT `date` FROM `oc_expenses_emp_transaction` WHERE `date` = '".$date."'", $this->conn);
			//if($booking_done->num_rows == 0){
				$emp_datas = $this->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$user_id."' ", $this->conn);
				if($emp_datas->num_rows > 0){
					$emp_name = $emp_datas->row['name'];
				} else {
					$emp_name = '';
				}

				$expenses = $this->query("SELECT `name` FROM `oc_expence` WHERE `id` = '".$type_id."' ", $this->conn);
				if($expenses->num_rows > 0){
					$type = $expenses->row['name'];
				} else {
					$type = '';
				}

				$upload_http_path = '';
				$photo_name = '';
				if($photo != ''){
					$last_ids = $this->query("SELECT `id` FROM `oc_expenses_emp_transaction` ORDER BY `id` DESC LIMIT 1 ", $this->conn);
					if($last_ids->num_rows > 0){
						$tran_id = $last_ids->row['id'] + 1;
					} else {
						$tran_id = 1;
					}
					$raw_path = DIR_DOWNLOAD;
					$fold_name = $tran_id;	
					$raw_upload_path = $raw_path.'expenses_upload/'.$fold_name.'/';
					$raw_http_path = HTTP_DOWNLOAD.'expenses_upload/'.$fold_name.'/';
					if(!file_exists(DIR_DOWNLOAD.'expenses_upload/'.$fold_name)){
						@mkdir(DIR_DOWNLOAD.'expenses_upload/'.$fold_name);
						@chmod(DIR_DOWNLOAD.'expenses_upload/'.$fold_name, 0777);
					}
					$photo_name = 'Evidence_Image_'.$tran_id.'.jpg';
					$upload_path = $raw_upload_path.$photo_name;
					$upload_http_path = $raw_http_path.$photo_name;
					$photo = base64_decode($photo);
					file_put_contents($upload_path, $photo);
					@chmod($upload_path, 0777);
				}

				$this->query(" INSERT INTO  `oc_expenses_emp_transaction` SET 
						`id` = '".$tran_id."',
						`emp_id` =  '".$user_id."',
						`emp_name` =  '".$emp_name."',
						`date` =  '".$date."',
						`type_id` =  '".$type_id."',
						`type` =  '".$type."',
						`amount` =  '".$this->escape($amount, $this->conn)."',
						`description` =  '".$this->escape($description, $this->conn)."',
						`photo` =  '".$this->escape($photo_name, $this->conn)."',
						`photo_source` = '".$this->escape($upload_http_path, $this->conn)."' ", $this->conn);
				$result = array();
				$result['success'] = 1;
				return $result;
			//} else {
				//$result['success'] = 0;
		    	//return $result;
		    //}
		} else {
			if($in1 == 1){
				$result['success'] = 3;
			} else {
				$result['success'] = 2;
			}
		    return $result;
		} 
	}

  	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
	
}
?>