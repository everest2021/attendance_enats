<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    
			<div class="heading"><h1><img src="view/image/admin_theme/base5builder_impulsepro/icon-users-large.png" alt="" /> <?php echo $heading_title; ?></h1>
			

      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <tr>
            <td><span class="required">*</span> <?php echo 'Employee Name'; ?></td>
            <td>
              <input type="text" name="name" value="<?php echo $name; ?>" />
            </td>
          </tr>
          <tr>
            <td><?php echo $entry_password; ?></td>
            <td>
              <input type="password" name="password" value="<?php echo $password; ?>"  />
              <?php if ($error_password) { ?>
              <span class="error"><?php echo $error_password; ?></span>
              <?php } ?>
            </td>
          </tr>
          <tr>
            <td><?php echo $entry_confirm; ?></td>
            <td>
              <input type="password" name="confirm" value="<?php echo $confirm; ?>" />
              <?php if ($error_confirm) { ?>
              <span class="error"><?php echo $error_confirm; ?></span>
              <?php } ?>
            </td>
          </tr>
          <tr>
            <td style="text-align: center;" colspan="2">
              <div class="buttons">
                <a onclick="$('#form').submit();" class="button" style='background: #04bc4e url("../../admin/view/image/admin_theme/base5builder_impulsepro/icon-button-save.png") no-repeat 18px 5px;padding: 0 36px 0 49px;line-height: 40px;margin: 0 0 0 10px;font-weight: bold;color: #FFFFFF;cursor: pointer;display: inline-block;'><?php echo $button_save; ?></a>
                <a href="<?php echo $cancel; ?>" class="button" style='background: #f02c2c url("../../admin/view/image/admin_theme/base5builder_impulsepro/icon-button-cancel.png") no-repeat 16px 5px;padding: 0 36px 0 49px;line-height: 40px;margin: 0 0 0 10px;font-weight: bold;color: #FFFFFF;cursor: pointer;display: inline-block;'><?php echo $button_cancel; ?></a>
              </div>
            </td>
          </tr>
        </table>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?> 