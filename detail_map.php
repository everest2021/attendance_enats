<?php
require_once('service/config.php');
// Create connection
$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}
// echo '<pre>';
// print_r($_GET);
// exit;
if(isset($_GET['project_mb_data_id'])){
	$id = $_GET['project_mb_data_id'];
} else {
	$id  = 1000;
}
// if(isset($_GET['date'])){
// 	$date = $_GET['date'];
// } else {
// 	$date = '2018-08-28';
// }

$point_locations = query("SELECT * FROM `oc_project_mb_point_data` WHERE  `project_mb_data_id` = '".$id."'  ORDER BY `id` ASC ", $conn);
$point_location = array();
if($point_locations->num_rows != 0){
	foreach($point_locations->rows as $tkey => $tvalue){
		$point_location[] = array(
			//'date_time' => date('d-m-Y', strtotime($tvalue['date'])).' '.date('h:i:s A', strtotime($tvalue['time'])),
			'lat' => $tvalue['lat'],
			'lon' => $tvalue['lon'],
		);	
	}
}
// echo '<pre>';
// print_r($point_location);
// exit;

function query($sql, $conn) {
	$query = $conn->query($sql);

	if (!$conn->errno){
		if (isset($query->num_rows)) {
			$data = array();

			while ($row = $query->fetch_assoc()) {
				$data[] = $row;
			}

			$result = new stdClass();
			$result->num_rows = $query->num_rows;
			$result->row = isset($data[0]) ? $data[0] : array();
			$result->rows = $data;

			unset($data);

			$query->close();

			return $result;
		} else{
			return true;
		}
	} else {
		throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
		exit();
	}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Google Map</title>

		<link href="style.css" rel="stylesheet">
	</head>
	<body>
		<div id="map"></div>
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBaRGDx1pT3TmF3RPXtSSS1XpmZeIr6_oQ&callback=initMap"></script>
	</body>
	<script type="text/javascript">
		<?php 
		$js_array = json_encode($point_location);
		echo "var locations = ". $js_array . ";\n";		
		?>
		function initMap() {
			var map = new google.maps.Map(document.getElementById('map'), {
				zoom: 13,
				center: new google.maps.LatLng(locations[0]['lat'], locations[0]['lon']),
				mapTypeId: google.maps.MapTypeId.ROADMAP
			});
			var infowindow = new google.maps.InfoWindow({});
			var marker, i;
			for (i = 0; i < locations.length; i++) {
				marker = new google.maps.Marker({
					position: new google.maps.LatLng(locations[i]['lat'], locations[i]['lon']),
					map: map
				});

				google.maps.event.addListener(marker, 'click', (function (marker, i) {
					return function () {
						infowindow.setContent("Route");
						infowindow.open(map, marker);
					}
				})(marker, i));
			}
		}
	</script>
</html>