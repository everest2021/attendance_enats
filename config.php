<?php
session_start();
date_default_timezone_set("Asia/Kolkata");
// HTTP
define('HTTP_SERVER', 'http://localhost/attendance_punch/admin/');
define('HTTP_CATALOG', 'http://localhost/attendance_punch/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/attendance_punch/admin/');
define('HTTPS_CATALOG', 'http://localhost/attendance_punch/');

// DIR
define('DIR_APPLICATION', 'D:\xampp\htdocs\attendance_punch\admin/');
define('DIR_SYSTEM', 'D:\xampp\htdocs\attendance_punch\system/');
define('DIR_DATABASE', 'D:\xampp\htdocs\attendance_punch\system/database/');
define('DIR_LANGUAGE', 'D:\xampp\htdocs\attendance_punch\admin/language/');
define('DIR_TEMPLATE', 'D:\xampp\htdocs\attendance_punch\admin/view/template/');
define('DIR_CONFIG', 'D:\xampp\htdocs\attendance_punch\system/config/');
define('DIR_IMAGE', 'D:\xampp\htdocs\attendance_punch\image/');
define('DIR_CACHE', 'D:\xampp\htdocs\attendance_punch\system/cache/');
define('DIR_DOWNLOAD', 'D:\xampp\htdocs\attendance_punch\download/');
define('DIR_LOGS', 'D:\xampp\htdocs\attendance_punch\system/logs/');
define('DIR_CATALOG', 'D:\xampp\htdocs\attendance_punch\catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'db_attendance_gml');
define('DB_PREFIX', 'oc_');
//}
?>
