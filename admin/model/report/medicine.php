<?php
class ModelReportMedicine extends Model {
	public function getTotalMedicine($data) {
		$sql = "SELECT medicine_name, medicine_price, SUM(medicine_quantity) as med_qty, SUM(medicine_total) as med_total FROM `oc_transaction` WHERE 1=1";
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`dot`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(`dot`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}
		$sql .= ' GROUP BY medicine_name ORDER BY med_qty DESC';
		$query = $this->db->query($sql);
		return $query->num_rows;
	}

	public function getMedicine($data) {
		$sql = "SELECT medicine_name, medicine_price, SUM(medicine_quantity) as med_qty, SUM(medicine_total) as med_total FROM `oc_transaction` WHERE 1=1";
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`dot`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(`dot`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}
		$sql .= ' GROUP BY medicine_name ORDER BY med_qty DESC';		
		$query = $this->db->query($sql);
		return $query->rows;
	}
}
?>
