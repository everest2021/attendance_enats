<?php
class ModelReportAttendance extends Model {
	public function getAttendance($data) {
		$sql = "SELECT * FROM `oc_transaction` t LEFT JOIN `oc_employee` e ON (e.`emp_code` = t.`emp_id`) WHERE 1=1";
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(t.`date`) = '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_unit'])) {
			$sql .= " AND (t.`unit_id`) = '" . $this->db->escape(($data['filter_unit'])) . "'";
		}

		if (!empty($data['filter_department'])) {
			$sql .= " AND (t.`department_id`) = '" . $this->db->escape(($data['filter_department'])) . "'";
		}

		if(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			if($data['filter_reporting_head'] == 1){
				$sql .= " AND (e.`reporting_to` = '" . $this->db->escape($this->session->data['emp_code']) . "' OR e.`emp_code` = '" . $this->db->escape($this->session->data['emp_code']) . "')";		
			} else {
				$sql .= " AND t.`emp_id` = '" . $this->db->escape($this->session->data['emp_code']) . "'";		
			}
		}

		if ($data['filter_status'] == 1) {
			$sql .= " AND (t.`present_status` = '1' OR t.`present_status` = '0.5' OR t.`weekly_off` <> '0' OR t.`holiday_id` <> '0' OR t.`leave_status` <> '0' OR (`date` = '".date('Y-m-d')."' AND `act_intime` <> '00:00:00'))  ";
		} elseif($data['filter_status'] == 2) {
			$sql .= " AND ( (t.`absent_status` = '1' AND `date` <> '".date('Y-m-d')."') OR (`date` = '".date('Y-m-d')."' AND `act_intime` = '00:00:00') )";
			//$sql .= " AND (`absent_status` = '1' OR `absent_status` = '0.5')";
		} else {
			//$sql .= " AND (`present_status` = '1' OR `present_status` = '0.5' OR  `weekly_off` <> '0' OR `holiday_id` <> '0' OR `halfday_status` <> '0' OR `leave_status` <> '0')  ";
		}
		$sql .= " AND (DATE(e.`dol`) = '0000-00-00' OR DATE(e.`dol`) > '".$data['filter_date_start']."') ";
		//$sql .= " AND `emp_id` = '21463' ";
		$sql .= ' ORDER BY t.`act_intime` DESC';	
		//echo $sql;exit;	
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getclose_status($data) {
		$sql = "SELECT * FROM `oc_transaction` WHERE 1=1";
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`date`) = '" . $this->db->escape($data['filter_date_start']) . "'";
		}
		if (!empty($data['unit'])) {
			$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
		}
		if (!empty($data['department'])) {
			$sql .= " AND LOWER(`department`) = '" . $this->db->escape(strtolower($data['department'])) . "'";
		}
		if (!empty($data['group'])) {
			$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['group'])) . "'";
		}
		$sql .= " AND `day_close_status` = 1";
		$sql .= " ORDER BY `act_intime` DESC";	
		//echo $sql;exit;	
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTodaysAttendance($data) {
		$sql = "SELECT * FROM `oc_transaction` WHERE 1=1";
		$sql .= " AND DATE(`date`) = '".date('Y-m-d')."' AND `act_intime` <> '00:00:00' ";
		
		if (!empty($data['filter_unit'])) {
			$sql .= " AND `unit_id` = '" . $this->db->escape(strtolower($data['filter_unit'])) . "'";
		}
		if (!empty($data['filter_department'])) {
			$sql .= " AND `department_id` = '" . $this->db->escape(strtolower($data['filter_department'])) . "'";
		}
		if (!empty($data['filter_group'])) {
			$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['group'])) . "'";
		}

		$sql .= ' ORDER BY `act_intime` DESC';
		//echo $sql;exit;		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTodaysAttendance_new($data) {
		$sql = "SELECT * FROM `oc_attendance_new` WHERE 1=1";
		$sql .= " AND DATE(`punch_date`) = '".date('Y-m-d')."' AND `punch_time` <> '00:00:00' ";
		//$sql .= " AND DATE(`punch_date`) = '2016-12-28' AND `punch_time` <> '00:00:00' ";
		
		if (!empty($data['filter_department'])) {
			$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($data['filter_department'])) . "'";
		}
		if (!empty($data['filter_department'])) {
			$sql .= " AND LOWER(`dept`) = '" . $this->db->escape(strtolower($data['filter_department'])) . "'";
		}
		if (!empty($data['filter_group'])) {
			$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['filter_group'])) . "'";
		}

		$sql .= ' ORDER BY `punch_time` DESC';
		//echo $sql;exit;		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getdepartment_list() {
		$sql = "SELECT `department`, `department_id` FROM `oc_employee` WHERE `department` <> '' GROUP BY `department` ";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getgroup_list() {
		$sql = "SELECT `group` FROM `oc_employee` GROUP BY `group` ";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getNextDate($unit) {
		$sql = "SELECT `date` FROM " . DB_PREFIX . "transaction WHERE `day_close_status` = '1' ";
		if($unit){
			$sql .= " AND `unit` = '".$unit."' ";
		}
		$sql .= " GROUP BY `date` ORDER BY `date` DESC LIMIT 0,1";
		$query = $this->db->query($sql);
		if(isset($query->rows[0]['date'])) {
			return date('Y-m-d', strtotime($query->rows[0]['date'] .' +1 day'));	
		} else {
			return date('Y-m-d', strtotime('2016-10-01'));
		}
		
	}

	public function getNextDate_1() {
		$query = $this->db->query("SELECT `date` FROM " . DB_PREFIX . "transaction WHERE `month_close_status` = '1' GROUP BY `date` ORDER BY `date` DESC LIMIT 0,1");
		if(isset($query->rows[0]['date'])) {
			return date('Y-m-d', strtotime($query->rows[0]['date'] .' +1 day'));	
		} else {
			return date('Y-m-d', strtotime('2016-10-01'));
		}
		
	}
}
?>