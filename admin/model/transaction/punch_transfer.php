<?php
class ModelTransactionPunchTransfer extends Model {
	public function addpunch_transfer($data) {
		// $data['dept_head_list'] = array();
		// $full_name = $data['emp_name'];
		// $full_name = preg_replace('/\s+/', ' ', $full_name);
		     // echo"<pre>";print_r($data);exit;
		    $fdevice_name = $this->db->query("SELECT device_serial_no FROM oc_devices WHERE id = '" . $data['from_device']."' ")->row;
		    $pdevice_name = $this->db->query("SELECT device_serial_no FROM oc_devices WHERE id = '" . $data['to_device']."' ")->row;
		     		// exit;
		$this->db->query("INSERT INTO " . DB_PREFIX . "punch_transfer SET 
							emp_name = '" . $this->db->escape($data['emp_name']) . "', 
							emp_code = '" . $this->db->escape($data['emp_code']) . "',
							from_device_name = '" . $this->db->escape($fdevice_name['device_serial_no']) . "', 
							to_device_id = '" . $this->db->escape($data['to_device']) . "', 
							from_device_id = '" . $this->db->escape($data['from_device']) . "',
							to_device_name = '" . $this->db->escape($pdevice_name['device_serial_no']) . "'");
							
	}

	public function editpunch_transfer($emp_id,$data) { //echo "<pre>";print_r($data);exit;
		$fdevice_name = $this->db->query("SELECT device_serial_no FROM oc_devices WHERE id = '" . $data['from_device']."' ")->row;
		$pdevice_name = $this->db->query("SELECT device_serial_no FROM oc_devices WHERE id = '" . $data['to_device']."' ")->row;
		
		$this->db->query("UPDATE " . DB_PREFIX . "punch_transfer SET 
							emp_name = '" . $this->db->escape($data['emp_name']) . "', 
							emp_code = '" . $this->db->escape($data['emp_code']) . "',
							from_device_name = '" . $this->db->escape($fdevice_name['device_serial_no']) . "', 
							to_device_id = '" . $this->db->escape($data['to_device']) . "', 
							from_device_id = '" . $this->db->escape($data['from_device']) . "',
							to_device_name = '" . $this->db->escape($pdevice_name['device_serial_no']) . "'
							WHERE id = '" . $emp_id . "'");
		
	}

	public function deletepunch_transfer($emp_id) {
		// echo"DELETE FROM " . DB_PREFIX . "punch_transfer WHERE id = '" . $emp_id . "'";exit;
		$this->db->query("DELETE FROM " . DB_PREFIX . "punch_transfer WHERE id = '" . $emp_id . "'");
	}	

	public function getpunch_transfer($emp_id) {
		 // echo"SELECT DISTINCT * FROM " . DB_PREFIX . "punch_transfer WHERE id = '" . $emp_id . "'";exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "punch_transfer WHERE id = '" . $emp_id . "'");
		return $query->row;
	}

	public function getcatname($category_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "categories WHERE id = '" . (int)$category_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	// public function getdeptname($department_id) {
	// 	$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "department WHERE id = '" . (int)$department_id . "'");
	// 	if($query->num_rows > 0){
	// 		return $query->row['name'];
	// 	} else {
	// 		return '';
	// 	}
	// }

	

	

	public function getlocname($location_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "location WHERE id = '" . (int)$location_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	public function gettransfer_punchs($data = array()) { //echo "innn";exit;
		$sql = "SELECT * FROM `" . DB_PREFIX . "punch_transfer` WHERE 1=1 ";
		
		$query = $this->db->query($sql);
		//echo "<pre>";print_r($query);exit;

		return $query->rows;
	}

	public function getpunch_transfers($data = array()) { //echo "innn";exit;
		$sql = "SELECT * FROM `" . DB_PREFIX . "employee` WHERE 1=1 ";
		if (!empty($data['emp_name'])) {
			$data['emp_name'] = html_entity_decode($data['emp_name']);
			//$data['emp_name'] = preg_replace('/\s+/', ' ', $data['emp_name']);
			$sql .= " AND LOWER(`emp_name`) LIKE '%" . $this->db->escape(strtolower($data['emp_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['emp_name'])) . "'";
		}

		if (isset($data['emp_code']) && !empty($data['emp_code'])) {
			$emp_code = $data['emp_code'];
			$sql .= " AND `device_emp_code` = '" . $this->db->escape($emp_code) . "' ";
		}


		
		//$this->log->write($sql);			
		//echo $sql;exit;
		$query = $this->db->query($sql);
		//echo "<pre>";print_r($query);exit;

		return $query->rows;
	}

	public function getTotalpunch_transfers($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "punch_transfer WHERE 1=1 ";
		
		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			//$data['filter_name'] = preg_replace('/\s+/', ' ', $data['filter_name']);
			$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
		}

		if (isset($data['filter_code']) && !empty($data['filter_code'])) {
			$sql .= " AND `device_emp_code` = '" . $this->db->escape($data['filter_code']) . "' ";
		}

		if (isset($data['filter_emp_code']) && !empty($data['filter_emp_code'])) {
			$filter_emp_code = $data['filter_emp_code'];
			$sql .= " AND `device_emp_code` = '" . $this->db->escape($filter_emp_code) . "' ";
		}

		// if (isset($data['filter_department']) && !empty($data['filter_department'])) {
		// 	$sql .= " AND LOWER(department) = '" . $this->db->escape(strtolower($data['filter_department'])) . "' ";
		// }

		// if (isset($data['filter_departments']) && !empty($data['filter_departments'])) {
		// 	$sql .= " AND LOWER(`department`) IN (" . strtolower($data['filter_departments']) . ") ";
		// }

		if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
			$sql .= " AND LOWER(unit) = '" . $this->db->escape(strtolower($data['filter_unit'])) . "' ";
		}

		if (isset($data['filter_shift_type'])) {
			$sql .= " AND shift_type = 'R' ";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		// if (isset($data['filter_department_id']) && !empty($data['filter_department_id'])) {
		// 	$sql .= " AND `department_id` = '" . $this->db->escape($data['filter_department_id']) . "' ";
		// }

		if (isset($data['filter_unit_id']) && !empty($data['filter_unit_id'])) {
			$sql .= " AND `unit_id` = '" . $this->db->escape($data['filter_unit_id']) . "' ";
		}

		if (isset($data['filter_shift_id']) && !empty($data['filter_shift_id'])) {
			$sql .= " AND `shift_id` = '" . $this->db->escape($data['filter_shift_id']) . "' ";
		}


		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	

	public function getUnit() {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "unit ");
		return $query->rows;
	}

	// public function getDepartment() {
	// 	$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "department ");
	// 	return $query->rows;
	// }

	public function getshift() {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift ");
		return $query->rows;
	}
}
?>