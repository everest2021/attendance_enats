<?php
class ModelTransactionTransaction extends Model {
	
	public function getrawattendance_group() {
		$query = $this->db->query("SELECT emp_id, punch_date, card_id FROM " . DB_PREFIX . "attendance WHERE `status` = '0' GROUP by `emp_id`, `punch_date` ");
		return $query->rows;
	}

	public function getrawattendance_group_date($date) {
		$query = $this->db->query("SELECT `emp_id`, `punch_date`, `card_id`, `manual_status`, `leave`, `holiday`, `weekly_off` FROM `" . DB_PREFIX . "attendance` WHERE `punch_date` = '".$date."' GROUP by `emp_id`, `punch_date` ");
		return $query->rows;
	}

	public function getrawattendance_group_date_custom($emp_code, $date) {
		$query = $this->db->query("SELECT `emp_id`, `punch_date`, `card_id`, `manual_status`, `leave`, `holiday`, `weekly_off`, `present`, absent, `device_id` FROM `" . DB_PREFIX . "attendance` WHERE `punch_date` = '".$date."' AND `emp_id` = '".$emp_code."' GROUP by `punch_date` ");
		//$this->log->write("SELECT `emp_id`, `punch_date`, `card_id`, `manual_status`, `leave`, `holiday`, `weekly_off`, `present`, absent FROM `" . DB_PREFIX . "attendance` WHERE `punch_date` = '".$date."' AND `emp_id` = '".$emp_code."' GROUP by `punch_date` ");
		return $query->row;
	}

	// public function getrawattendance_in_time($emp_id, $punch_date, $shift_intime) {
	// 	// echo $emp_id;
	// 	// echo '<br />';
	// 	//$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "attendance WHERE emp_id = '".$emp_id."' AND `punch_date` = '".$punch_date."' ORDER by STR_TO_DATE(punch_time,'%h:%i:%s %p') ASC LIMIT 1 ");
	// 	$prev_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
	// 	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."' OR `punch_date` = '".$punch_date."') ORDER by `punch_date` ASC, time(`punch_time`) ASC ");
	// 	//echo "SELECT * FROM " . DB_PREFIX . "attendance WHERE emp_id = '".$emp_id."' AND `punch_date` = '".$punch_date."' ORDER by STR_TO_DATE(punch_time,'%h:%i:%s %p') ASC LIMIT 1 ";
	// 	//exit;
	// 	$array = $query->rows;
	// 	$comp_array = array();
	// 	$hour_array = array();
	// 	$act_intime = array();
		
	// 	// echo '<pre>';
	// 	// print_r($array);
	// 	// exit;
		
	// 	if(count($array) > 1){
	// 		foreach ($array as $akey => $avalue) {
	// 			$s_exp = explode(':', $shift_intime);
	// 			if($s_exp[0] == '00'){
	// 				$punch_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
	// 			}
	// 			$start_date = new DateTime($punch_date.' '.$shift_intime);
	// 			$since_start = $start_date->diff(new DateTime($avalue['punch_date'].' '.$avalue['punch_time']));
				
	// 			// echo '<pre>';
	// 			// print_r($punch_date.' '.$shift_intime);
	// 			// echo '<pre>';
	// 			// print_r($avalue['punch_date'].' '.$avalue['punch_time']);
	// 			// echo '<pre>';
	// 			// print_r($since_start);
				
	// 			if($since_start->d == 0 && $since_start->h < 12 && $since_start->invert == 1){
	// 				$comp_array[] = $since_start->h.':'.$since_start->i.':'.$since_start->s;
	// 				$hour_array[] = $since_start->h;
	// 				$act_array[] = $avalue;
	// 			} elseif($since_start->d == 0 && $since_start->h < 12 && $since_start->invert == 0){
	// 				$comp_array[] = $since_start->h.':'.$since_start->i.':'.$since_start->s;
	// 				$hour_array[] = $since_start->h;
	// 				$act_array[] = $avalue;
	// 			}
	// 		}
	// 	}
		
	// 	// echo '<pre>';
	// 	// print_r($array);
	// 	// echo '<pre>';
	// 	// print_r($comp_array);
	// 	// echo '<pre>';
	// 	// print_r($hour_array);
	// 	// echo '<pre>';
	// 	// print_r($act_array);
	// 	// exit;
	// 	if($hour_array){
	// 		$hour_array = array_unique($hour_array);
	// 		asort($hour_array);
	// 		foreach ($hour_array as $key => $value) {
	// 			$act_intime = $act_array[$key];
	// 			$act_intime['status'] = 1;
	// 			break;
	// 		}
	// 	} else {
	// 		$act_intime['punch_time'] = $array[0]['punch_time'];
	// 		$act_intime['punch_date'] = $array[0]['punch_date'];
	// 		$act_intime['status'] = 0;
	// 	}
	// 	// echo '<pre>';
	// 	// print_r($act_intime);
	// 	// exit;
		
	// 	return $act_intime;
	// }

	public function getrawattendance_in_time($emp_id, $punch_date) {
		$future_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."' OR `punch_date` = '".$future_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC, `id` ASC ");
		$array = $query->row;
		return $array;
	}

	//public function getrawattendance_out_time($emp_id, $punch_date) {
		//$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."') AND `status` = '0' ORDER by `id` DESC ");
		//$array = $query->row;
		//return $array;
	//}

	public function getrawattendance_out_time($emp_id, $punch_date, $act_intime, $act_punch_date) {
		$future_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."' OR `punch_date` = '".$future_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC");
		$act_outtime = array();
		if($query->num_rows > 0){
			$first_punch = $query->rows['0'];
			$array = $query->rows;
			$comp_array = array();
			$hour_array = array();
			
			// echo '<pre>';
			// print_r($array);

			foreach ($array as $akey => $avalue) {
				$start_date = new DateTime($act_punch_date.' '.$act_intime);
				$since_start = $start_date->diff(new DateTime($avalue['punch_date'].' '.$avalue['punch_time']));
				if($since_start->d == 0){
					$comp_array[] = $since_start->h.':'.$since_start->i.':'.$since_start->s;
					$hour_array[] = $since_start->h;
					$act_array[] = $avalue;
				}
			}

			// echo '<pre>';
			// print_r($hour_array);

			// echo '<pre>';
			// print_r($comp_array);

			foreach ($hour_array as $ckey => $cvalue) {
				if($cvalue > 18){
					unset($hour_array[$ckey]);
				}
			}

			// echo '<pre>';
			// print_r($hour_array);
			
			$act_outtimes = max($hour_array);

			// echo '<pre>';
			// print_r($act_outtimes);

			foreach ($hour_array as $akey => $avalue) {
				if($avalue == $act_outtimes){
					$act_outtime = $act_array[$akey];
				}
			}
		}
		// echo '<pre>';
		// print_r($act_outtime);
		// exit;		
		return $act_outtime;
	}

	public function getempdata($emp_id) {
		$query = $this->db->query("SELECT CONCAT_WS(' ',first_name,last_name) AS emp_name, `name`, `shift_id`, `weekly_off`, `department`, `unit`, `group`, `shift_type`, `emp_code`, `department`, `unit`, `department_id`, `unit_id` FROM " . DB_PREFIX . "employee WHERE `emp_code` = '".$emp_id."' ");
		if($query->num_rows > 0){
			return $query->row;
		} else {
			return array();
		}
	}

	public function getshiftdata($shift_id) {
		$sql = "SELECT * FROM oc_shift WHERE 1=1 ";
		if($shift_id){
			$sql .= " AND `shift_id` = '".$shift_id."' ";
		}
		$query = $this->db->query($sql);
		if($query->num_rows > 0){
			if($shift_id){
				return $query->row;
			} else {
				return $query->rows;
			}
		} else {
			return array();
		}
	}

	public function getEmployees_dat($emp_code) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "employee` WHERE `emp_code` = '".$emp_code."'";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getpltakenyear($emp_code) {
		$sql = "SELECT `id` FROM `" . DB_PREFIX . "leave_transaction` WHERE `emp_id` = '".$emp_code."' AND `leave_type` = 'PL' AND `year` = '".date('Y')."' AND `encash` <> '' AND `a_status` = '1' ";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getleave_transaction_data($batch_id) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "leave_transaction` WHERE `batch_id` = '".$batch_id."' ";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function deleteorderhistory($date){
		$sql = "DELETE FROM `" . DB_PREFIX . "attendance` WHERE `punch_date` = '".$date."'";
		$this->db->query($sql);
	}

	public function deleteorderhistory_new($date){
		$sql = "DELETE FROM `" . DB_PREFIX . "attendance_new` WHERE DATE(`punch_date`) = '".$date."' OR DATE(`punch_date`) < '".$date."' ";
		$this->db->query($sql);
	}

	public function getorderhistory($data){
		$sql = "SELECT * FROM `" . DB_PREFIX . "attendance` WHERE `punch_date` = '".$data['punch_date']."' AND `punch_time` = '".$data['in_time']."' AND `emp_id` = '".$data['employee_id']."' ";
		$query = $this->db->query($sql);
		if($query->num_rows > 0){
			return 1;
		} else {
			return 0;
		}
	}

	public function insert_attendance_data($data) {
		//$this->log->write("INSERT INTO `oc_attendance` SET `emp_id` = '".$data['employee_id']."', `card_id` = '".$data['card_id']."', `punch_date` = '".$data['punch_date']."', `punch_time` = '".$data['in_time']."', `status` = '0' ");
		$sql = $this->db->query("INSERT INTO `oc_attendance` SET `emp_id` = '".$data['employee_id']."', `card_id` = '".$data['card_id']."', `punch_date` = '".$data['punch_date']."', `punch_time` = '".$data['in_time']."', `status` = '0' ");
		//$id = $this->db->getLastId();
		// echo $id;
		// echo '<br />';
		// if ($sql == true) {
		// 	echo 'true';
		// } else {
		// 	echo 'false';
		// }
		// exit;
		//echo 'done';exit;
	}

	public function insert_attendance_data_new($data) {
		$sql = $this->db->query("INSERT INTO `oc_attendance_new` SET `emp_id` = '".$data['employee_id']."', `emp_name` = '".$this->db->escape($data['emp_name'])."', `punch_date` = '".$data['punch_date']."', `punch_time` = '".$data['in_time']."', `shift_intime` = '".$data['shift_intime']."', `shift_outtime` = '".$data['shift_outtime']."', `act_shift_intime` = '".$data['act_shift_intime']."', `act_shift_outtime` = '".$data['act_shift_outtime']."', `status` = '0', `department` = '".$this->db->escape($data['department'])."', `department_id` = '".$this->db->escape($data['department_id'])."', `unit` = '".$this->db->escape($data['unit'])."', `unit_id` = '".$this->db->escape($data['unit_id'])."', `late_time` = '".$data['late_time']."', `device_id` = '".$data['device_id']."', `download_date` = '".$data['download_date']."', `log_id` = '".$data['log_id']."', `shift_id` = '".$data['shift_id']."', `shift_code` = '".$data['shift_code']."', `act_shift_id` = '".$data['act_shift_id']."', `act_shift_code` = '".$data['act_shift_code']."' ");
	}

	public function getprevmonthtransdata($filter_month, $filter_year) {
		//$sql = "SELECT * FROM `" . DB_PREFIX . "shift_schedule` WHERE `month` = '".$this->db->escape($filter_month)."' AND `year` = '".$this->db->escape($filter_year)."' "; 
		$sql = "SELECT * FROM `" . DB_PREFIX . "shift_schedule` WHERE 1=1 "; 
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getexistdata($filter_month, $filter_year) {
		//$sql = "SELECT * FROM `" . DB_PREFIX . "shift_schedule` WHERE `month` = '".$this->db->escape($filter_month)."' AND `year` = '".$this->db->escape($filter_year)."' "; 
		$sql = "SELECT * FROM `" . DB_PREFIX . "shift_schedule` WHERE 1=1"; 
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->num_rows;
	}

	public function getemployee_exist($id) {
		$sql = "SELECT `status` FROM `" . DB_PREFIX . "employee` where `emp_code` = '".$id."' "; 
		//$this->log->write($sql);
		$query = $this->db->query($sql);
		$status = 0;
		if($query->num_rows > 0){
			$statuss = $query->row['status'];
			if($statuss == 1){
				$status = 1;
			} else {
				$status = 0;
			}
		}
		return $status;
	}

	public function getshift_schedule($emp_code, $day) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$emp_code . "' AND `month` = '".date('n')."' AND `year` = '".date('Y')."' ");
		return $query->row;
	}

	public function getempexist($emp_code) {
		$query = $this->db->query("SELECT count(*) AS total FROM " . DB_PREFIX . "employee WHERE emp_code = '" . (int)$emp_code . "' ");
		if(isset($query->row['total'])){
			return $query->row['total'];
		} else{
			return 0;
		}
	}

	public function gettransaction_data($transaction_id) {
		$sql = "SELECT * FROM `oc_transaction` WHERE `transaction_id` = '".$transaction_id."' ";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getleavetransaction($data = array()) {
		$sql = "SELECT * FROM `oc_leave_transaction` WHERE 1=1 ";
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}

		if (!empty($data['filter_name_id_1'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id_1']) . "'";
		}

		if (isset($this->session->data['emp_code']) && !$this->user->isAdmin()) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($this->session->data['emp_code']) . "'";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND `dot` = '" . $this->db->escape($data['filter_date']) . "'";
		}

		if (!empty($data['filter_unit'])) {
			$sql .= " AND `unit_id` = '" . $this->db->escape($data['filter_unit']) . "'";
		}

		if (isset($data['filter_batch']) && !empty($data['filter_batch'])) {
			$sql .= " AND `batch_id` = '" . $this->db->escape($data['filter_batch']) . "'";
		}

		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$sql .= " GROUP BY batch_id ORDER BY batch_id DESC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;exit;

		$query = $this->db->query($sql);
		return $query->rows;
	}

	// public function gettotal_leave_days($batch_id) {
	// 	$sql = "SELECT COUNT(*) as days FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' ";
	// 	$query = $this->db->query($sql);
	// 	return $query->row['days'];
	// }

	public function gettotal_leave_days($batch_id) {
		$sql = "SELECT `days`, `leave_amount` FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getleave_from($batch_id) {
		$sql = "SELECT `date` FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' ORDER by date(date) ASC LIMIT 1 ";
		$query = $this->db->query($sql);
		return $query->row['date'];
	}

	public function getleave_to($batch_id) {
		$sql = "SELECT `date` FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' ORDER by date(date) DESC LIMIT 1 ";
		$query = $this->db->query($sql);
		return $query->row['date'];
	}

	public function getTotalleaveetransaction($data = array()) {
		$sql = "SELECT * FROM `oc_leave_transaction` WHERE 1=1 ";
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}

		if (isset($this->session->data['emp_code']) && !$this->user->isAdmin()) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($this->session->data['emp_code']) . "'";
		}

		if (!empty($data['filter_name_id_1'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id_1']) . "'";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND `dot` = '" . $this->db->escape($data['filter_date']) . "'";
		}

		if (!empty($data['filter_unit'])) {
			$sql .= " AND `unit_id` = '" . $this->db->escape($data['filter_unit']) . "'";
		}

		if (isset($data['filter_batch']) && !empty($data['filter_batch'])) {
			$sql .= " AND `batch_id` = '" . $this->db->escape($data['filter_batch']) . "'";
		}
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$sql .= " GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->num_rows;
	}

	public function gettotal_bal($emp_code) {
		$sql = "SELECT `pl_bal`, `cl_bal`, `sl_bal`, `pl_acc`, `cl_acc`, `sl_acc`, `cof_acc` FROM `oc_leave` WHERE `emp_id` = '".$emp_code."' AND `close_status` = '0' ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function gettotal_bal_p($emp_code, $type) {
		$sql = "SELECT COUNT(*) as `bal_p`, encash FROM `oc_leave_transaction` WHERE `emp_id` = '" . $emp_code . "' AND `leave_amount`= '' AND `leave_type`= '".$type."' AND (`p_status` = '0') AND `a_status` = '1' ";
		//echo $sql;exit;
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function gettotal_bal_p1($emp_code, $type) {
		$sql = "SELECT SUM(`leave_amount`) as leave_amount FROM `oc_leave_transaction` WHERE `emp_id` = '" . $emp_code . "' AND `days` = '' AND `leave_type`= '".$type."' AND (`p_status` = '0') AND `a_status` = '1' ";
		//echo $sql;exit;
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function gettotal_bal_pro($emp_code, $type) {
		$sql = "SELECT `year` FROM `oc_leave` WHERE `emp_id` = '".$emp_code."' AND `close_status` = '0' ";
		//$this->log->write($sql);
		$query = $this->db->query($sql);
		$year = $query->row['year'];
		$comp_date = $year.'-01-01';
		$sql = "SELECT COUNT(*) as `bal_p`, `encash` FROM `oc_leave_transaction` WHERE `emp_id` = '" . $emp_code . "' AND `leave_amount` = '' AND `leave_type`= '".$type."' AND (`p_status` = '1') AND `a_status` = '1' AND date(`date`) >= '".$comp_date."' ";
		//echo $sql;exit;
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function gettotal_bal_pro1($emp_code, $type) {
		$sql = "SELECT `year` FROM `oc_leave` WHERE `emp_id` = '".$emp_code."' AND `close_status` = '0' ";
		//$this->log->write($sql);
		$query = $this->db->query($sql);
		$year = $query->row['year'];
		$comp_date = $year.'-01-01';
		$sql = "SELECT SUM(`leave_amount`) as leave_amount FROM `oc_leave_transaction` WHERE `emp_id` = '" . $emp_code . "' AND `days` = '' AND `leave_type`= '".$type."' AND (`p_status` = '1') AND `a_status` = '1' AND date(`date`) >= '".$comp_date."' ";
		//echo $sql;exit;
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function gettransaction_data_leave($emp_code, $data) {
		$sql = "SELECT * FROM `oc_transaction` WHERE 1=1";
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`date`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}
		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(`date`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}
		$sql .= " AND emp_id = '".$emp_code."' ";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTotalleaveetransaction_ess($data = array()) {
		$sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1 ";
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}

		if (!empty($data['filter_name_id_1'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id_1']) . "'";
		}

		if (isset($this->session->data['emp_code'])) {
			if(isset($data['filter_reporting'])){
				$sql .= " AND `reporting_to` = '" . $this->db->escape($this->session->data['emp_code']) . "'";
			} else {
				if(!$this->user->isAdmin()){
					$sql .= " AND `emp_id` = '" . $this->db->escape($this->session->data['emp_code']) . "'";
				}
			}	
		}

		// if (isset($this->session->data['dept_name'])) {
		// 	$sql .= " AND LOWER(dept_name) = '" . $this->db->escape(strtolower($this->session->data['dept_name'])) . "' ";
		// }

		if (!empty($data['filter_dept'])) {
			$sql .= " AND LOWER(dept_id) = '" . $this->db->escape(strtolower($data['filter_dept'])) . "' ";
		}

		if (isset($data['filter_depts']) && !empty($data['filter_depts'])) {
			$sql .= " AND LOWER(dept_id) IN (" . strtolower($data['filter_depts']) . ") ";
		}

		if (!empty($data['filter_group'])) {
			$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['filter_group'])) . "' ";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND `dot` >= '" . $this->db->escape($data['filter_date']) . "'";
		}

		if (!empty($data['filter_date_to'])) {
			$sql .= " AND `dot` <= '" . $this->db->escape($data['filter_date_to']) . "'";
		}

		if (!empty($data['filter_leave_from'])) {
			$sql .= " AND `date` >= '" . $this->db->escape($data['filter_leave_from']) . "'";
		}

		if (!empty($data['filter_leave_to'])) {
			$sql .= " AND `date` <= '" . $this->db->escape($data['filter_leave_to']) . "'";
		}

		if (!empty($data['filter_unit'])) {
			$sql .= " AND `unit_id` = '" . $this->db->escape($data['filter_unit']) . "'";
		}

		if (!empty($data['filter_leavetype'])) {
			$sql .= " AND `leave_type` = '" . $this->db->escape($data['filter_leavetype']) . "'";
		}

		if (!empty($data['filter_approval_1_by'])) {
			$sql .= " AND `approved_1_by` = '" . $this->db->escape($data['filter_approval_1_by']) . "'";
		}

		if (!empty($data['filter_approval_1'])) {
			if($data['filter_approval_1'] == '1'){
				$sql .= " AND `approval_1` = '0'";
			} else {
				$sql .= " AND `approval_1` = '1'";	
			}
		}

		if (!empty($data['filter_approval_2'])) {
			if($data['filter_approval_2'] == '1'){
				$sql .= " AND `approval_2` = '0'";
			} else {
				$sql .= " AND `approval_2` = '1'";	
			}
		}

		if (!empty($data['filter_proc'])) {
			if($data['filter_proc'] == '1'){
				$sql .= " AND `a_status` = '0'";
			} else {
				$sql .= " AND `a_status` = '1'";	
			}
		}
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$sql .= " GROUP BY batch_id ";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->num_rows;
	}

	public function getleavetransaction_ess($data = array()) {
		$sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1 ";
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}

		if (!empty($data['filter_name_id_1'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id_1']) . "'";
		}

		if (isset($this->session->data['emp_code'])) {
			if(isset($data['filter_reporting'])){
				$sql .= " AND `reporting_to` = '" . $this->db->escape($this->session->data['emp_code']) . "'";
			} else {
				if(!$this->user->isAdmin()){
					$sql .= " AND `emp_id` = '" . $this->db->escape($this->session->data['emp_code']) . "'";
				}
			}	
		}

		// if (isset($this->session->data['dept_name'])) {
		// 	$sql .= " AND LOWER(dept_name) = '" . $this->db->escape(strtolower($this->session->data['dept_name'])) . "' ";
		// }

		if (!empty($data['filter_dept'])) {
			$sql .= " AND LOWER(dept_id) = '" . $this->db->escape(strtolower($data['dept_id'])) . "' ";
		}

		if (isset($data['filter_depts']) && !empty($data['filter_depts'])) {
			$sql .= " AND LOWER(dept_id) IN (" . strtolower($data['filter_depts']) . ") ";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND `dot` >= '" . $this->db->escape($data['filter_date']) . "'";
		}

		if (!empty($data['filter_date_to'])) {
			$sql .= " AND `dot` <= '" . $this->db->escape($data['filter_date_to']) . "'";
		}

		if (!empty($data['filter_leave_from'])) {
			$sql .= " AND `date` >= '" . $this->db->escape($data['filter_leave_from']) . "'";
		}

		if (!empty($data['filter_leave_to'])) {
			$sql .= " AND `date` <= '" . $this->db->escape($data['filter_leave_to']) . "'";
		}

		if (!empty($data['filter_unit'])) {
			$sql .= " AND `unit_id` = '" . $this->db->escape($data['filter_unit']) . "'";
		}

		if (!empty($data['filter_group'])) {
			$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['filter_group'])) . "' ";
		}

		if (!empty($data['filter_leavetype'])) {
			$sql .= " AND `leave_type` = '" . $this->db->escape($data['filter_leavetype']) . "'";
		}

		if (!empty($data['filter_approval_1_by'])) {
			$sql .= " AND `approved_1_by` = '" . $this->db->escape($data['filter_approval_1_by']) . "'";
		}

		if (!empty($data['filter_approval_1'])) {
			if($data['filter_approval_1'] == '1'){
				$sql .= " AND `approval_1` = '0'";
			} else if($data['filter_approval_1'] == '3') {
				$sql .= " AND `approval_1` = '2'";
			} else {
				$sql .= " AND `approval_1` = '1'";	
			}
		}

		if (!empty($data['filter_approval_2'])) {
			if($data['filter_approval_2'] == '1'){
				$sql .= " AND `approval_2` = '0'";
			} else {
				$sql .= " AND `approval_2` = '1'";	
			}
		}

		if (!empty($data['filter_proc'])) {
			if($data['filter_proc'] == '1'){
				$sql .= " AND `a_status` = '0'";
			} else {
				$sql .= " AND `a_status` = '1'";	
			}
		}

		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$sql .= " GROUP BY batch_id ORDER BY batch_id DESC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//$this->log->write($sql);
		//echo $sql;exit;
		$query = $this->db->query($sql);
		//$this->log->write(print_r($query,true));
		return $query->rows;
	}

	public function gettotal_leave_days_ess($batch_id) {
		$sql = "SELECT `days`, `leave_amount` FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$batch_id."' ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getleave_from_ess($batch_id) {
		$sql = "SELECT `date` FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$batch_id."' ORDER by date(date) ASC LIMIT 1 ";
		$query = $this->db->query($sql);
		if(isset($query->row['date'])){
			return $query->row['date'];
		} else {
			return '';
		}
	}

	public function getleave_to_ess($batch_id) {
		$sql = "SELECT `date` FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$batch_id."' ORDER by date(date) DESC LIMIT 1 ";
		$query = $this->db->query($sql);
		if(isset($query->row['date'])){
			return $query->row['date'];
		} else {
			return '';
		}
	}

	public function gettour_from_ess($batch_id) {
		$sql = "SELECT `date` FROM `oc_manual_punch` WHERE `batch_id` = '".$batch_id."' ORDER by date(date) ASC LIMIT 1 ";
		$query = $this->db->query($sql);
		if(isset($query->row['date'])){
			return $query->row['date'];
		} else {
			return '';
		}
	}

	public function gettour_to_ess($batch_id) {
		$sql = "SELECT `date` FROM `oc_manual_punch` WHERE `batch_id` = '".$batch_id."' ORDER by date(date) DESC LIMIT 1 ";
		$query = $this->db->query($sql);
		if(isset($query->row['date'])){
			return $query->row['date'];
		} else {
			return '';
		}
	}

	public function getleave_transaction_data_ess($batch_id) {
		$sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$batch_id."' ";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getpltakenyear_ess($emp_code) {
		$sql = "SELECT `id` FROM `oc_leave_transaction_temp` WHERE `emp_id` = '".$emp_code."' AND `leave_type` = 'PL' AND `year` = '".date('Y')."' AND `encash` <> '' AND `a_status` = '1' ";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function gettotal_bal_ess($emp_code) {
		$sql = "SELECT `pl_bal`, `cl_bal`, `sl_bal`,`lwp_bal`, `pl_acc`, `cl_acc`, `sl_acc`, `lwp_acc` FROM `oc_leave` WHERE `emp_id` = '".$emp_code."' AND `close_status` = '0' ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getprevious_balances($emp_code, $filter_date_start, $filter_date_end, $rvalue, $type) {
		$this->load->model('report/common_report');

		$sql = "SELECT `pl_acc`, `year` FROM `oc_leave` WHERE `emp_id` = '".$emp_code."' AND `close_status` = '0' ";
		$query = $this->db->query($sql);
		$previous_balance = 0;
		if($query->num_rows > 0){
			$pl_open_balance = $query->row['pl_acc'];
			$current_year = $query->row['year'];

			$end_date_month = date('m', strtotime($filter_date_start));
			$end_date_year = date('Y', strtotime($filter_date_start));

			if($end_date_month == 12){
				$pl_bal1 = 0;
				$pl_bal2 = 0;
				$previous_decucted_days = 0;
				$leave_credit_count = 0;
			} else {
				$prev_year = $current_year - 1;
				$start_date = $prev_year.'-12-26';
				$start_date_constant = $current_year.'-01-01';
				$end_date = $end_date_year.'-'.$end_date_month.'-25';
				
				// echo $start_date;
				// echo '<br />';
				// echo $end_date;
				// echo '<br />';
				// exit;

				$sql = "SELECT COUNT(*) as `bal_p`, `encash` FROM `oc_leave_transaction_temp` WHERE `emp_id` = '" . $emp_code . "' AND `leave_amount` = '' AND `leave_type`= '".$type."' AND `a_status` = '1' AND date(`date`) >= '".$start_date."' AND date(`date`) <= '".$end_date."' ";
				//echo $sql;exit;
				$bal1_datass = $this->db->query($sql);
				$pl_bal1 = 0;
				if($bal1_datass->num_rows > 0){
					$bal1_datas = $bal1_datass->row['bal_p'];
					if($bal1_datas != ''){
						$pl_bal1 = $bal1_datas;
					} else {
						$pl_bal1 = 0;
					}
				}
				
				$sql2 = "SELECT SUM(`leave_amount`) as leave_amount FROM `oc_leave_transaction_temp` WHERE `emp_id` = '" . $emp_code . "' AND `days` = '' AND `leave_type`= '".$type."' AND `a_status` = '1' AND date(`date`) >= '".$start_date."' AND date(`date`) <= '".$end_date."' ";
				$bal2_datass = $this->db->query($sql2);
				$pl_bal2 = 0;
				if($bal2_datass->num_rows > 0){
					$bal2_datas = $bal2_datass->row['leave_amount'];
					if($bal2_datas != ''){
						$pl_bal2 = $bal2_datas;
					} else {
						$pl_bal2 = 0;
					}
				}
				//echo $pl_bal1;exit;
			

				$data['filter_date_start'] = $start_date;
				$data['filter_date_end'] = $end_date;
				$current_date = date('Y-m-d');
				// if($rvalue['doj'] != '0000-00-00' && strtotime($rvalue['doj']) > strtotime($data['filter_date_start'])){
				// 	$data['filter_date_start'] = $rvalue['doj'];
				// }
				// if($rvalue['dol'] != '0000-00-00' && strtotime($rvalue['dol']) < strtotime($data['filter_date_end'])){
				// 	$data['filter_date_end'] = $rvalue['dol'];
				// }
				$start_date = $data['filter_date_start'];
				$end_date = $data['filter_date_end'];
				//$start_date = '2017-12-26';
				//$end_date = '2018-02-25';
				$months_array = $this->GetMonth($start_date, $end_date);
				// echo '<pre>';
				// print_r($months_array);
				// exit;
				$total_deduction_days_rounded = 0;
				$total_late_early_days_rounded = 0;
				foreach($months_array as $mkey => $mvalue){
					$cyear = date('Y', strtotime($mvalue));
					$cmonth = date('n', strtotime($mvalue));
					
					if($cmonth == 12){
						$nyear = $cyear + 1;
					} else {
						$nyear = $cyear;
					}
					
					if($cmonth == 12){
						$nmonth = 1;
					} else {
						$nmonth = $cmonth + 1;
					}
					$start_date = $cyear.'-'.$cmonth.'-26';
					$data['filter_date_start'] = $start_date;
					if($rvalue['doj'] != '0000-00-00' && strtotime($rvalue['doj']) > strtotime($data['filter_date_start'])){
						$data['filter_date_start'] = $rvalue['doj'];
					}
					$end_date = $nyear.'-'.$nmonth.'-25';
					$data['filter_date_end'] = $end_date;
					if($rvalue['dol'] != '0000-00-00' && strtotime($rvalue['dol']) < strtotime($data['filter_date_end'])){
						$data['filter_date_end'] = $rvalue['dol'];
					}
					if(strtotime($data['filter_date_end']) > strtotime($current_date)){
						$data['filter_date_end'] = date("Y-m-d", strtotime("-1 day", strtotime($current_date)));
					}
					$start_date = $data['filter_date_start'];
					$end_date = $data['filter_date_end'];
					// echo $start_date;
					// echo '<br />';
					// echo $end_date;
					// echo '<br />';
					// exit;
					$total_month_dayss = $this->GetDays($start_date, $end_date);
					// echo '<pre>';
					// print_r($total_month_dayss);
					// exit;
					$total_month_days = 0;
					foreach($total_month_dayss as $tkeys => $tvalue){
						$total_month_days ++;
					}
					$transaction_datas = $this->model_report_common_report->gettransaction_data($rvalue['emp_code'], $data);
					$transaction_inn = '0';
					$working_hours_array = array();
					$actual_working_hours_array = array();
					$late_early_count = 0;
					$total_absent_count = 0;
					foreach($transaction_datas as $tkey => $tvalue){
						if($tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0' && $tvalue['working_time'] != '00:00:00'){
							if($tvalue['after_shift'] == 1){
								$tvalue['act_outtime'] = $tvalue['shift_outtime_flexi'];
							}
							$shift_intime_minus_one = Date('H:i:s', strtotime($tvalue['shift_intime'] .' -0 minutes'));
							$start_date = new DateTime($tvalue['date'].' '.$shift_intime_minus_one);
							$since_start = $start_date->diff(new DateTime($tvalue['date'].' '.$tvalue['act_intime']));
							if($since_start->h > 12){
								$start_date = new DateTime($tvalue['date'].' '.$shift_intime_minus_one);
								$since_start = $start_date->diff(new DateTime($tvalue['date_out'].' '.$tvalue['act_intime']));
							}
							$shift_early = 0;
							if($since_start->invert == 1){
								$shift_early = 1;
							}
							if($shift_early == 1){
								$shift_intime_minus_one = Date('H:i:s', strtotime($tvalue['shift_intime'] .' -0 minutes'));
								if(strtotime($tvalue['act_outtime']) < strtotime($shift_intime_minus_one)){
									$start_date = new DateTime($tvalue['date'].' '.$tvalue['act_intime']);
								} else {
									$start_date = new DateTime($tvalue['date'].' '.$shift_intime_minus_one);
								}
							} else {
								$start_date = new DateTime($tvalue['date'].' '.$tvalue['act_intime']);
							}
							$since_start = $start_date->diff(new DateTime($tvalue['date'].' '.$tvalue['act_outtime']));
							$working_hour = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
							$tvalue['working_time'] = $working_hour;
							$working_hours_array[$tvalue['date']] = $tvalue['working_time'];				
						} else {
							// if($tvalue['date'] == date('Y-m-d') && $tvalue['act_intime'] != '00:00:00'){
							// 	$current_time = date('H:i:s');
							// 	$start_date = new DateTime($tvalue['date'].' '.$tvalue['act_intime']);
							// 	$since_start = $start_date->diff(new DateTime($tvalue['date_out'].' '.$current_time));
							// 	$working_hours_array[$tvalue['date']] = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);	
							// }
						}
						//if(($tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0') || ($tvalue['weekly_off'] != '0' && $tvalue['working_time'] != '00:00:00') || ($tvalue['holiday_id'] != '0' && $tvalue['working_time'] != '00:00:00') || ($tvalue['leave_status'] != '0' && $tvalue['working_time'] != '00:00:00') ){
						$current_date = date('Y-m-d');
						if($tvalue['date'] < $current_date && $tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0' && $tvalue['act_intime'] <> '00:00:00' && $tvalue['act_outtime'] <> '00:00:00'){
							$start_date = new DateTime($tvalue['date'].' '.$tvalue['shift_intime']);
							$since_start = $start_date->diff(new DateTime($tvalue['date_out'].' '.$tvalue['shift_outtime']));
							$shift_working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
							$actual_working_hours_array[$tvalue['date']] = $shift_working_time;	
						} else {
							if($tvalue['date'] >= $current_date && $tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0' && $tvalue['act_intime'] = '00:00:00' && $tvalue['act_outtime'] = '00:00:00'){
								$start_date = new DateTime($tvalue['date'].' '.$tvalue['shift_intime']);
								$since_start = $start_date->diff(new DateTime($tvalue['date_out'].' '.$tvalue['shift_outtime']));
								$shift_working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
								$actual_working_hours_array[$tvalue['date']] = $shift_working_time;	
							}
						}
						if($tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0'){
							$late_early_count = $late_early_count + $tvalue['late_mark'];
							$late_early_count = $late_early_count + $tvalue['early_mark'];
						}
						
						if($tvalue['date'] < $current_date){
							if($tvalue['absent_status'] == 1 && $tvalue['working_time'] == '00:00:00' && $tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0'){
								$total_absent_count = $total_absent_count + 1;
							} elseif($tvalue['absent_status'] == 0.5){
								//$total_absent_count = $total_absent_count + 0.5;
							}
						}
					}
					//echo $total_absent_count;exit;

					$time = 0;
					$hours = 0;
					$minutes = 0;
					foreach ($working_hours_array as $time_val) {
						$times = explode(':', $time_val);
						$hours += $times[0];
						$minutes += $times[1];
					}
					$min_min = 0;
					$min_hours = 0;
					if($minutes > 0){
						$min_hours = floor($minutes / 60);
		    			$min_min = ($minutes % 60);
		    			$min_min = sprintf('%02d', $min_min);
		    		}
		    		$total_working_hours = sprintf('%02d', ($hours + $min_hours)).'.'.sprintf('%02d', $min_min);
					$time = 0;
					$hours = 0;
					$minutes = 0;
					foreach ($actual_working_hours_array as $time_val) {
						$times = explode(':', $time_val);
						$hours += $times[0];
						$minutes += $times[1];
					}
					$min_min = 0;
					$min_hours = 0;
					if($minutes > 0){
						$min_hours = floor($minutes / 60);
		    			$min_min = ($minutes % 60);
		    			$min_min = sprintf('%02d', $min_min);
		    		}
		    		
		    		$total_actual_working_hours = sprintf('%02d', ($hours + $min_hours)).'.'.sprintf('%02d', $min_min);
					if($total_actual_working_hours > $total_working_hours){
						$extra_hours = 0;
						//$difference_hours = $total_actual_working_hours - $total_working_hours;
						$total_actual_working_hours_min = $this->hoursToMinutes($total_actual_working_hours);
						$total_working_hours_min = $this->hoursToMinutes($total_working_hours);
						$difference_minutes = $total_actual_working_hours_min - $total_working_hours_min;
						$min_hours = floor($difference_minutes / 60);
						$min_min = ($difference_minutes % 60);
						$difference_hours = $min_hours.'.'.$min_min; 
					} else {
						$difference_hours = 0;
						//$extra_hours = $total_working_hours - $total_actual_working_hours;
						$total_actual_working_hours_min = $this->hoursToMinutes($total_actual_working_hours);
						$total_working_hours_min = $this->hoursToMinutes($total_working_hours);
						$difference_minutes = $total_working_hours_min - $total_actual_working_hours_min;
						$min_hours = floor($difference_minutes / 60);
						$min_min = ($difference_minutes % 60);
						$extra_hours = $min_hours.'.'.$min_min; 
					}
					
					$deduction_days_rounded = floor($difference_hours / 9.00);
					$deduction_days = round($difference_hours / 9.00, 2);
					$deduction_days = sprintf("%.2f", $deduction_days);
					$deduction_days_exp = explode('.', $deduction_days);
					if(isset($deduction_days_exp[1])){
						if($deduction_days_exp[1] == '00' || $deduction_days_exp[1] == 00){
							$deduction_days_exp[1] = 0;
						}
						if($deduction_days_exp[1] > 0 && $deduction_days_exp[1] <= 50){
							$deduction_days_rounded = $deduction_days_rounded + 0.5; 
						} else {
							if($deduction_days_exp[1] != 0){
								$deduction_days_rounded = $deduction_days_rounded + 1;
							}
						}
					}
					// echo $difference_hours;
					// echo '<br />';
					// echo $deduction_days;
					// echo '<br />';
					// echo '<pre>';
					// print_r($deduction_days_exp);
					// echo '<br />';
					// echo $deduction_days_rounded;
					// echo '<br />';
					// exit;
					//$late_early_count = 9;
					$late_early_days_rounded = floor($late_early_count / 6);
					$late_early_days = round($late_early_count / 6, 2);
					$late_early_days = sprintf("%.2f", $late_early_days);
					$late_early_days_exp = explode('.', $late_early_days);
					if(isset($late_early_days_exp[1])){
						if($late_early_days_exp[1] == '00' || $late_early_days_exp[1] == 00){
							$late_early_days_exp[1] = 0;
						}
						if($late_early_days_exp[1] >= 50){
							$late_early_days_rounded = $late_early_days_rounded + 0.5; 
						}
					}
					$total_deduction_days_rounded = $total_deduction_days_rounded + $deduction_days_rounded; 
					$total_late_early_days_rounded = $total_late_early_days_rounded + $late_early_days_rounded; 
				}
				$previous_decucted_days = $total_deduction_days_rounded + $total_late_early_days_rounded;
				
				$filter_date_start_month = date('n', strtotime($start_date_constant));
				$filter_date_end_month = date('n', strtotime($filter_date_end));
				$credit_leave_datas = $this->db->query("SELECT SUM(`leave_value`) AS `leave_credited` FROM `oc_leave_credit_transaction` WHERE `emp_code` = '".$emp_code."' AND `month` >= '".$filter_date_start_month."' AND `month` <= '".$filter_date_end_month."' AND `year` = '".$current_year."' ");
				//echo "SELECT SUM(`leave_value`) AS `leave_credited` FROM `oc_leave_credit_transaction` WHERE `emp_code` = '".$emp_code."' AND `month` >= '".$filter_date_start_month."' AND `month` <= '".$filter_date_end_month."' AND `year` = '".$current_year."' ";exit;
				$leave_credit_count = 0;
				if($credit_leave_datas->num_rows > 0){
					$credit_leave_data = $credit_leave_datas->row;
					if($credit_leave_data['leave_credited'] != ''){
						$leave_credit_count = $credit_leave_data['leave_credited'];
					}
				}	
				//echo $leave_credit_count;exit;			
			}
			// echo $pl_open_balance;
			// echo '<br />';
			// echo $leave_credit_count;
			// echo '<br />';
			// echo $pl_bal1;
			// echo '<br />';
			// echo $pl_bal2;
			// echo '<br />';
			// echo $previous_decucted_days;
			// echo '<br />';
			// echo 'total_late_early_days_rounded - ' . $total_deduction_days_rounded;
			// echo '<br />';
			// echo 'total_late_early_days_rounded - ' . $total_late_early_days_rounded;
			// echo '<br />';
			// echo 'previous_decucted_days - ' . $previous_decucted_days;
			// echo '<br />';
			//exit;
			$previous_balance = ($pl_open_balance + $leave_credit_count) - ($pl_bal1 + $pl_bal2 + $previous_decucted_days);
			if($previous_balance < 0){
				$previous_balance = 0;
			}
		}
		//echo $previous_balance;exit;
		return $previous_balance;
	}

	public function get_current_opening_balances($emp_code, $type, $open_val = 1) {
		$this->load->model('report/common_report');

		$sql = "SELECT * FROM `" . DB_PREFIX . "employee` WHERE `emp_code` = '".$emp_code."'";
		$query = $this->db->query($sql);
		$rvalue = $query->row;		

		$sql = "SELECT `pl_acc`, `year` FROM `oc_leave` WHERE `emp_id` = '".$emp_code."' AND `close_status` = '0' ";
		$query = $this->db->query($sql);
		$previous_balance = 0;
		if($query->num_rows > 0){
			$pl_open_balance = $query->row['pl_acc'];
			$current_year = $query->row['year'];

			$prev_year = $current_year - 1;
			$start_date = $prev_year.'-12-26';
			$start_date_constant = $current_year.'-01-01';

			$sql = "SELECT COUNT(*) as `bal_p`, `encash` FROM `oc_leave_transaction_temp` WHERE `emp_id` = '" . $emp_code . "' AND `leave_amount` = '' AND `leave_type`= '".$type."' AND `a_status` = '1' AND date(`date`) >= '".$start_date."' ";
			$bal1_datass = $this->db->query($sql);
			$pl_bal1 = 0;
			if($bal1_datass->num_rows > 0){
				$bal1_datas = $bal1_datass->row['bal_p'];
				if($bal1_datas != ''){
					$pl_bal1 = $bal1_datas;
				} else {
					$pl_bal1 = 0;
				}
			}
			
			$sql2 = "SELECT SUM(`leave_amount`) as leave_amount FROM `oc_leave_transaction_temp` WHERE `emp_id` = '" . $emp_code . "' AND `days` = '' AND `leave_type`= '".$type."' AND `a_status` = '1' AND date(`date`) >= '".$start_date."' ";
			$bal2_datass = $this->db->query($sql2);
			$pl_bal2 = 0;
			if($bal2_datass->num_rows > 0){
				$bal2_datas = $bal2_datass->row['leave_amount'];
				if($bal2_datas != ''){
					$pl_bal2 = $bal2_datas;
				} else {
					$pl_bal2 = 0;
				}
			}

			$current_date = date('Y-m-d');
			$data['filter_date_start'] = $start_date;
			$data['filter_date_end'] = date("Y-m-d", strtotime("-1 day", strtotime($current_date)));
			// $data['filter_date_start'] = $start_date;
			// if($rvalue['doj'] != '0000-00-00' && strtotime($rvalue['doj']) > strtotime($data['filter_date_start'])){
			// 	$data['filter_date_start'] = $rvalue['doj'];
			// }
			// $data['filter_date_end'] = date("Y-m-d", strtotime("-1 day", strtotime($current_date)));
			// if($rvalue['dol'] != '0000-00-00' && strtotime($rvalue['dol']) < strtotime($data['filter_date_end'])){
			// 	$data['filter_date_end'] = $rvalue['dol'];
			// }
			$start_date = $data['filter_date_start'];
			$end_date = $data['filter_date_end'];
			$end_date_constant = $data['filter_date_end'];
			
			$months_array = $this->GetMonth($start_date, $end_date);
			$total_deduction_days_rounded = 0;
			$total_late_early_days_rounded = 0;
			foreach($months_array as $mkey => $mvalue){
				$cyear = date('Y', strtotime($mvalue));
				$cmonth = date('n', strtotime($mvalue));
				if($cmonth == 12){
					$nyear = $cyear + 1;
				} else {
					$nyear = $cyear;
				}
				
				if($cmonth == 12){
					$nmonth = 1;
				} else {
					$nmonth = $cmonth + 1;
				}

				$data['filter_date_start'] = $cyear.'-'.$cmonth.'-26';
				if($rvalue['doj'] != '0000-00-00' && strtotime($rvalue['doj']) > strtotime($data['filter_date_start'])){
					$data['filter_date_start'] = $rvalue['doj'];
				}
				$data['filter_date_end'] = $nyear.'-'.$nmonth.'-25';
				if($rvalue['dol'] != '0000-00-00' && strtotime($rvalue['dol']) < strtotime($data['filter_date_end'])){
					$data['filter_date_end'] = $rvalue['dol'];
				}
				if(strtotime($data['filter_date_end']) > strtotime($current_date)){
					$data['filter_date_end'] = date("Y-m-d", strtotime("-1 day", strtotime($current_date)));
				}
				$start_date = $data['filter_date_start'];
				$end_date = $data['filter_date_end'];
				$total_month_dayss = $this->GetDays($start_date, $end_date);
				// echo '<pre>';
				// print_r($total_month_dayss);
				// exit;
				$total_month_days = 0;
				foreach($total_month_dayss as $tkeys => $tvalue){
					$total_month_days ++;
				}
				$transaction_datas = $this->model_report_common_report->gettransaction_data($rvalue['emp_code'], $data);
				$transaction_inn = '0';
				$working_hours_array = array();
				$actual_working_hours_array = array();
				$late_early_count = 0;
				$total_absent_count = 0;
				foreach($transaction_datas as $tkey => $tvalue){
					if($tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0' && $tvalue['working_time'] != '00:00:00'){
						if($tvalue['after_shift'] == 1){
							$tvalue['act_outtime'] = $tvalue['shift_outtime_flexi'];
						}
						$shift_intime_minus_one = Date('H:i:s', strtotime($tvalue['shift_intime'] .' -0 minutes'));
						$start_date = new DateTime($tvalue['date'].' '.$shift_intime_minus_one);
						$since_start = $start_date->diff(new DateTime($tvalue['date'].' '.$tvalue['act_intime']));
						if($since_start->h > 12){
							$start_date = new DateTime($tvalue['date'].' '.$shift_intime_minus_one);
							$since_start = $start_date->diff(new DateTime($tvalue['date_out'].' '.$tvalue['act_intime']));
						}
						$shift_early = 0;
						if($since_start->invert == 1){
							$shift_early = 1;
						}
						if($shift_early == 1){
							$shift_intime_minus_one = Date('H:i:s', strtotime($tvalue['shift_intime'] .' -0 minutes'));
							if(strtotime($tvalue['act_outtime']) < strtotime($shift_intime_minus_one)){
								$start_date = new DateTime($tvalue['date'].' '.$tvalue['act_intime']);
							} else {
								$start_date = new DateTime($tvalue['date'].' '.$shift_intime_minus_one);
							}
						} else {
							$start_date = new DateTime($tvalue['date'].' '.$tvalue['act_intime']);
						}
						$since_start = $start_date->diff(new DateTime($tvalue['date'].' '.$tvalue['act_outtime']));
						$working_hour = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
						$tvalue['working_time'] = $working_hour;
						$working_hours_array[$tvalue['date']] = $tvalue['working_time'];				
					} else {
						// if($tvalue['date'] == date('Y-m-d') && $tvalue['act_intime'] != '00:00:00'){
						// 	$current_time = date('H:i:s');
						// 	$start_date = new DateTime($tvalue['date'].' '.$tvalue['act_intime']);
						// 	$since_start = $start_date->diff(new DateTime($tvalue['date_out'].' '.$current_time));
						// 	$working_hours_array[$tvalue['date']] = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);	
						// }
					}
					//if(($tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0') || ($tvalue['weekly_off'] != '0' && $tvalue['working_time'] != '00:00:00') || ($tvalue['holiday_id'] != '0' && $tvalue['working_time'] != '00:00:00') || ($tvalue['leave_status'] != '0' && $tvalue['working_time'] != '00:00:00') ){
					$current_date = date('Y-m-d');
					if($tvalue['date'] < $current_date && $tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0' && $tvalue['act_intime'] <> '00:00:00' && $tvalue['act_outtime'] <> '00:00:00'){
						$start_date = new DateTime($tvalue['date'].' '.$tvalue['shift_intime']);
						$since_start = $start_date->diff(new DateTime($tvalue['date_out'].' '.$tvalue['shift_outtime']));
						$shift_working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
						$actual_working_hours_array[$tvalue['date']] = $shift_working_time;	
					} else {
						if($tvalue['date'] >= $current_date && $tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0' && $tvalue['act_intime'] = '00:00:00' && $tvalue['act_outtime'] = '00:00:00'){
							$start_date = new DateTime($tvalue['date'].' '.$tvalue['shift_intime']);
							$since_start = $start_date->diff(new DateTime($tvalue['date_out'].' '.$tvalue['shift_outtime']));
							$shift_working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
							$actual_working_hours_array[$tvalue['date']] = $shift_working_time;	
						}
					}
					if($tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0'){
						$late_early_count = $late_early_count + $tvalue['late_mark'];
						$late_early_count = $late_early_count + $tvalue['early_mark'];
					}
					
					if($tvalue['date'] < $current_date){
						if($tvalue['absent_status'] == 1 && $tvalue['working_time'] == '00:00:00' && $tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0'){
							$total_absent_count = $total_absent_count + 1;
						} elseif($tvalue['absent_status'] == 0.5){
							//$total_absent_count = $total_absent_count + 0.5;
						}
					}
				}
				//echo $total_absent_count;exit;

				$time = 0;
				$hours = 0;
				$minutes = 0;
				foreach ($working_hours_array as $time_val) {
					$times = explode(':', $time_val);
					$hours += $times[0];
					$minutes += $times[1];
				}
				$min_min = 0;
				$min_hours = 0;
				if($minutes > 0){
					$min_hours = floor($minutes / 60);
	    			$min_min = ($minutes % 60);
	    			$min_min = sprintf('%02d', $min_min);
	    		}
	    		$total_working_hours = sprintf('%02d', ($hours + $min_hours)).'.'.sprintf('%02d', $min_min);
				$time = 0;
				$hours = 0;
				$minutes = 0;
				foreach ($actual_working_hours_array as $time_val) {
					$times = explode(':', $time_val);
					$hours += $times[0];
					$minutes += $times[1];
				}
				$min_min = 0;
				$min_hours = 0;
				if($minutes > 0){
					$min_hours = floor($minutes / 60);
	    			$min_min = ($minutes % 60);
	    			$min_min = sprintf('%02d', $min_min);
	    		}
	    		
	    		$total_actual_working_hours = sprintf('%02d', ($hours + $min_hours)).'.'.sprintf('%02d', $min_min);
				if($total_actual_working_hours > $total_working_hours){
					$extra_hours = 0;
					//$difference_hours = $total_actual_working_hours - $total_working_hours;
					$total_actual_working_hours_min = $this->hoursToMinutes($total_actual_working_hours);
					$total_working_hours_min = $this->hoursToMinutes($total_working_hours);
					$difference_minutes = $total_actual_working_hours_min - $total_working_hours_min;
					$min_hours = floor($difference_minutes / 60);
					$min_min = ($difference_minutes % 60);
					$difference_hours = $min_hours.'.'.$min_min; 
				} else {
					$difference_hours = 0;
					//$extra_hours = $total_working_hours - $total_actual_working_hours;
					$total_actual_working_hours_min = $this->hoursToMinutes($total_actual_working_hours);
					$total_working_hours_min = $this->hoursToMinutes($total_working_hours);
					$difference_minutes = $total_working_hours_min - $total_actual_working_hours_min;
					$min_hours = floor($difference_minutes / 60);
					$min_min = ($difference_minutes % 60);
					$extra_hours = $min_hours.'.'.$min_min; 
				}
				
				$deduction_days_rounded = floor($difference_hours / 9.00);
				$deduction_days = round($difference_hours / 9.00, 2);
				$deduction_days = sprintf("%.2f", $deduction_days);
				$deduction_days_exp = explode('.', $deduction_days);
				if(isset($deduction_days_exp[1])){
					if($deduction_days_exp[1] == '00' || $deduction_days_exp[1] == 00){
						$deduction_days_exp[1] = 0;
					}
					if($deduction_days_exp[1] > 0 && $deduction_days_exp[1] <= 50){
						$deduction_days_rounded = $deduction_days_rounded + 0.5; 
					} else {
						if($deduction_days_exp[1] != 0){
							$deduction_days_rounded = $deduction_days_rounded + 1;
						}
					}
				}
				// echo $difference_hours;
				// echo '<br />';
				// echo $deduction_days;
				// echo '<br />';
				// echo '<pre>';
				// print_r($deduction_days_exp);
				// echo '<br />';
				// echo $deduction_days_rounded;
				// echo '<br />';
				// exit;
				//$late_early_count = 9;
				$late_early_days_rounded = floor($late_early_count / 6);
				$late_early_days = round($late_early_count / 6, 2);
				$late_early_days = sprintf("%.2f", $late_early_days);
				$late_early_days_exp = explode('.', $late_early_days);
				if(isset($late_early_days_exp[1])){
					if($late_early_days_exp[1] == '00' || $late_early_days_exp[1] == 00){
						$late_early_days_exp[1] = 0;
					}
					if($late_early_days_exp[1] >= 50){
						$late_early_days_rounded = $late_early_days_rounded + 0.5; 
					}
				}
				$total_deduction_days_rounded = $total_deduction_days_rounded + $deduction_days_rounded; 
				$total_late_early_days_rounded = $total_late_early_days_rounded + $late_early_days_rounded; 
			}
			$previous_decucted_days = $total_deduction_days_rounded + $total_late_early_days_rounded;

			$filter_date_start_month = date('n', strtotime($start_date_constant));
			$filter_date_end_month = date('n', strtotime($end_date_constant));
			$credit_leave_datas = $this->db->query("SELECT SUM(`leave_value`) AS `leave_credited` FROM `oc_leave_credit_transaction` WHERE `emp_code` = '".$emp_code."' AND `month` >= '".$filter_date_start_month."' AND `month` <= '".$filter_date_end_month."' AND `year` = '".$current_year."' ");
			$leave_credit_count = 0;
			if($credit_leave_datas->num_rows > 0){
				$credit_leave_data = $credit_leave_datas->row;
				if($credit_leave_data['leave_credited'] != ''){
					$leave_credit_count = $credit_leave_data['leave_credited'];
				}
			}

			// echo $pl_open_balance;
			// echo '<br />';
			// echo $pl_bal1;
			// echo '<br />';
			// echo $pl_bal2;
			// echo '<br />';
			// echo 'total_late_early_days_rounded - ' . $total_deduction_days_rounded;
			// echo '<br />';
			// echo 'total_late_early_days_rounded - ' . $total_late_early_days_rounded;
			// echo '<br />';
			// echo 'previous_decucted_days - ' . $previous_decucted_days;
			// echo '<br />';
			// exit;
			$previous_balance = 0;
			if($open_val == 1){
				$previous_balance = ($pl_open_balance + $leave_credit_count) - ($pl_bal1 + $pl_bal2 + $previous_decucted_days);
			} elseif($open_val == 2){
				$previous_balance = $previous_decucted_days;
			} elseif($open_val == 3){
				$previous_balance = $leave_credit_count;
			}
			if($previous_balance < 0){
				$previous_balance = 0;
			}
		}
		//echo $previous_balance;exit;
		return $previous_balance;
	}

	public function gettotal_bal_p_ess($emp_code, $type) {
		$sql = "SELECT COUNT(*) as `bal_p`, encash FROM `oc_leave_transaction_temp` WHERE `emp_id` = '" . $emp_code . "' AND `leave_amount`= '' AND `leave_type`= '".$type."' AND (`p_status` = '0') AND `a_status` = '1' ";
		//echo $sql;exit;
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function gettotal_bal_p1_ess($emp_code, $type) {
		$sql = "SELECT SUM(`leave_amount`) as leave_amount FROM `oc_leave_transaction_temp` WHERE `emp_id` = '" . $emp_code . "' AND `days` = '' AND `leave_type`= '".$type."' AND (`p_status` = '0') AND `a_status` = '1' ";
		//echo $sql;exit;
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function gettotal_bal_pro_ess($emp_code, $type) {
		$sql = "SELECT `year` FROM `oc_leave` WHERE `emp_id` = '".$emp_code."' AND `close_status` = '0' ";
		//$this->log->write($sql);
		$query = $this->db->query($sql);
		$year = $query->row['year'];
		$comp_date = $year.'-01-01';
		$sql = "SELECT COUNT(*) as `bal_p`, `encash` FROM `oc_leave_transaction_temp` WHERE `emp_id` = '" . $emp_code . "' AND `leave_amount` = '' AND `leave_type`= '".$type."' AND `a_status` = '1' AND date(`date`) >= '".$comp_date."' ";
		//echo $sql;exit;
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function gettotal_bal_pro1_ess($emp_code, $type) {
		$sql = "SELECT `year` FROM `oc_leave` WHERE `emp_id` = '".$emp_code."' AND `close_status` = '0' ";
		//$this->log->write($sql);
		$query = $this->db->query($sql);
		$year = $query->row['year'];
		$comp_date = $year.'-01-01';
		$sql = "SELECT SUM(`leave_amount`) as leave_amount FROM `oc_leave_transaction_temp` WHERE `emp_id` = '" . $emp_code . "' AND `days` = '' AND `leave_type`= '".$type."' AND `a_status` = '1' AND date(`date`) >= '".$comp_date."' ";
		//echo $sql;exit;
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function gettotal_bal_pro_ess_date($emp_code, $type, $filter_date_start, $filter_date_end) {
		$sql = "SELECT COUNT(*) as `bal_p`, `encash` FROM `oc_leave_transaction_temp` WHERE `emp_id` = '" . $emp_code . "' AND `leave_amount` = '' AND `leave_type`= '".$type."' AND `a_status` = '1' AND `p_status` = '1' AND date(`date`) >= '".$filter_date_start."' AND date(`date`) <= '".$filter_date_end."' ";
		//echo $sql;exit;
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function gettotal_bal_pro1_ess_date($emp_code, $type, $filter_date_start, $filter_date_end) {
		$sql = "SELECT SUM(`leave_amount`) as leave_amount FROM `oc_leave_transaction_temp` WHERE `emp_id` = '" . $emp_code . "' AND `days` = '' AND `leave_type`= '".$type."' AND `a_status` = '1' AND `p_status` = '1' AND date(`date`) >= '".$filter_date_start."' AND date(`date`) <= '".$filter_date_end."' ";
		//echo $sql;exit;
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function gettotal_bal_pro2_ess($emp_code) {
		$this->load->model('report/common_report');

		$minus_count = 0;
		$sql = "SELECT `year`, `pl_acc` FROM `oc_leave` WHERE `emp_id` = '".$emp_code."' AND `close_status` = '0' ";
		$query = $this->db->query($sql);
		$year = $query->row['year'];
		$pl_acc = $query->row['pl_acc'];
		$comp_date = $year.'-01-01';
		$comp_date_end = $year.'-'.date('n').'-'.date('d');

		$sql = "SELECT COUNT(`late_mark`) as late_mark FROM `oc_transaction` WHERE `emp_id` = '" . $emp_code . "' AND `late_mark` = '1' AND `date` >= '".$comp_date."' AND `date` <= '".$comp_date_end."' ";
		$query = $this->db->query($sql);
		$minus_count = $minus_count + $query->row['late_mark'];

		$sql = "SELECT COUNT(`early_mark`) as early_mark FROM `oc_transaction` WHERE `emp_id` = '" . $emp_code . "' AND `early_mark` = '1' AND `date` >= '".$comp_date."' AND `date` <= '".$comp_date_end."' ";
		$query = $this->db->query($sql);
		$minus_count = $minus_count + $query->row['early_mark'];
		

		$data['filter_date_start'] = $comp_date;
		$data['filter_date_end'] = $comp_date_end;
		$transaction_datas = $this->model_report_common_report->gettransaction_data($emp_code, $data);
		$transaction_inn = '0';
		$working_hours_array = array();
		$actual_working_hours_array = array();
		$actual_working_hours = '00:00:00';
		foreach($transaction_datas as $tkey => $tvalue){
			if($tvalue['working_time'] != '00:00:00'){
				$working_hours_array[] = $tvalue['working_time'];				
			}
			if($tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0'){
				$start_date = new DateTime($tvalue['date'].' '.$tvalue['shift_intime']);
				$since_start = $start_date->diff(new DateTime($tvalue['date_out'].' '.$tvalue['shift_outtime']));
				$shift_working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
				$actual_working_hours_array[] = $shift_working_time;	
				if($tkey == 0){
					$actual_working_hours = $shift_working_time;
				}
			}
		}
		$time = 0;
		$hours = 0;
		$minutes = 0;
		foreach ($working_hours_array as $time_val) {
			$times = explode(':', $time_val);
			$hours += $times[0];
			$minutes += $times[1];
		}
		$min_min = 0;
		$min_hours = 0;
		if($minutes > 0){
			$min_hours = floor($minutes / 60);
			$min_min = ($minutes % 60);
			$min_min = sprintf('%02d', $min_min);
		}
		$total_working_hours = sprintf('%02d', ($hours + $min_hours)).'.'.sprintf('%02d', $min_min);
		
		$time = 0;
		$hours = 0;
		$minutes = 0;
		foreach ($actual_working_hours_array as $time_val) {
			$times = explode(':', $time_val);
			$hours += $times[0];
			$minutes += $times[1];
		}
		$min_min = 0;
		$min_hours = 0;
		if($minutes > 0){
			$min_hours = floor($minutes / 60);
			$min_min = ($minutes % 60);
			$min_min = sprintf('%02d', $min_min);
		}
		$total_actual_working_hours = sprintf('%02d', ($hours + $min_hours)).'.'.sprintf('%02d', $min_min);
		if($total_actual_working_hours > $total_working_hours){
			$difference_hours = $total_actual_working_hours - $total_working_hours;
			$extra_hours = 0;
		} else {
			$difference_hours = 0;
			$extra_hours = $total_working_hours - $total_actual_working_hours;
		}
		//$difference_hours = 22.30;
		$leave_count_rounded = round($difference_hours / 9);
		$leave_count = round($difference_hours / 9, 2);
		$leave_count_exp = explode('.', $leave_count);
		if(isset($leave_count_exp[1]) && $leave_count_exp[1] < 50){
			$leave_count_rounded = $leave_count_rounded + 0.5; 
		}
		// echo $leave_count;
		// echo '<br />';
		// echo $leave_count_rounded;
		// echo '<br />';
		// exit;
		$minus_count = $minus_count + $leave_count_rounded;		
		// echo '<pre>';
		// print_r($total_actual_working_hours);
		// echo '<pre>';
		// print_r($total_working_hours);
		// echo '<pre>';
		// print_r($difference_hours);
		// echo '<pre>';
		// print_r($extra_hours);
		// echo '<pre>';
		// print_r($leave_count);
		// echo '<pre>';
		// echo $pl_acc;
		// exit;
		return $minus_count;
	}

	public function getTotalrequest($data = array()) {
		$sql = "SELECT * FROM `oc_requestform` WHERE 1=1 ";
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND `doi` = '" . $this->db->escape($data['filter_date']) . "'";
		}

		if (isset($data['filter_dept']) && !empty($data['filter_dept'])) {
			$sql .= " AND `dept_name` = '" . $this->db->escape($data['filter_dept']) . "'";
		}

		if (isset($data['filter_depts']) && !empty($data['filter_depts'])) {
			$sql .= " AND LOWER(dept_name) IN (" . strtolower($data['filter_depts']) . ") ";
		}

		if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
			$sql .= " AND `unit` = '" . $this->db->escape($data['filter_unit']) . "'";
		}

		if (!empty($data['filter_approval_1'])) {
			if($data['filter_approval_1'] == '1'){
				$sql .= " AND `approval_1` = '0'";
			} elseif($data['filter_approval_1'] == '2') {
				$sql .= " AND (`approval_1` = '1' OR `approval_1` = '3' OR `approval_1` = '4') ";	
			} elseif($data['filter_approval_1'] == '3') {
				$sql .= " AND `approval_1` = '2'";	
			}
		}
		
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		//$sql .= " GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->num_rows;
	}

	public function getrequests($data = array()) {
		$sql = "SELECT * FROM `oc_requestform` WHERE 1=1 ";
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND `doi` = '" . $this->db->escape($data['filter_date']) . "'";
		}

		if (!empty($data['filter_approval_1'])) {
			if($data['filter_approval_1'] == '1'){
				$sql .= " AND `approval_1` = '0'";
			} elseif($data['filter_approval_1'] == '2') {
				$sql .= " AND (`approval_1` = '1' OR `approval_1` = '3' OR `approval_1` = '4') ";
			} elseif($data['filter_approval_1'] == '3') {
				$sql .= " AND `approval_1` = '2'";	
			}
		}

		if (isset($data['filter_dept']) && !empty($data['filter_dept'])) {
			$sql .= " AND `dept_name` = '" . $this->db->escape($data['filter_dept']) . "'";
		}

		if (isset($data['filter_depts']) && !empty($data['filter_depts'])) {
			$sql .= " AND LOWER(dept_name) IN (" . strtolower($data['filter_depts']) . ") ";
		}

		if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
			$sql .= " AND `unit` = '" . $this->db->escape($data['filter_unit']) . "'";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTotalrequest_unit($data = array()) {
		$sql = "SELECT * FROM `oc_requestform` WHERE 1=1 ";
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND `doi` = '" . $this->db->escape($data['filter_date']) . "'";
		}

		if (isset($data['filter_dept']) && !empty($data['filter_dept'])) {
			$sql .= " AND `dept_name` = '" . $this->db->escape($data['filter_dept']) . "'";
		}

		if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
			$sql .= " AND `unit` = '" . $this->db->escape($data['filter_unit']) . "'";
		}

		if (!empty($data['filter_approval_1'])) {
			if($data['filter_approval_1'] == '1'){
				$sql .= " AND `approval_1` = '1'";
			} elseif($data['filter_approval_1'] == '4') {
				$sql .= " AND `approval_1` = '4'";	
			}
		} else {
			$sql .= " AND (`approval_1` = '1' OR `approval_1` = '4') ";
		}
		
		//$sql .= " GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->num_rows;
	}

	public function getrequests_unit($data = array()) {
		$sql = "SELECT * FROM `oc_requestform` WHERE 1=1 ";
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND `doi` = '" . $this->db->escape($data['filter_date']) . "'";
		}

		if (isset($data['filter_dept']) && !empty($data['filter_dept'])) {
			$sql .= " AND `dept_name` = '" . $this->db->escape($data['filter_dept']) . "'";
		}

		if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
			$sql .= " AND `unit` = '" . $this->db->escape($data['filter_unit']) . "'";
		}

		if (!empty($data['filter_approval_1'])) {
			if($data['filter_approval_1'] == '1'){
				$sql .= " AND `approval_1` = '1'";
			} elseif($data['filter_approval_1'] == '4') {
				$sql .= " AND `approval_1` = '4'";	
			}
		} else {
			$sql .= " AND (`approval_1` = '1' OR `approval_1` = '4') ";
		}
		
		//$sql .= " GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getleave_data($batch_id) {
		$sql = "SELECT `leave_type` FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$batch_id."' LIMIT 1 ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	

	public function getleavetransaction_ess_new($data = array(),$emp_code) {
		$sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1 ";
		
		$sql .= " AND `reporting_to` = '".$emp_code."'";
		
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}

		if (!empty($data['filter_name_id_1'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id_1']) . "'";
		}

		// if (isset($this->session->data['emp_code'])) {
		// 	$sql .= " AND `emp_id` = '" . $this->db->escape($this->session->data['emp_code']) . "'";
		// }

		// if (isset($this->session->data['dept_name'])) {
		// 	$sql .= " AND LOWER(dept_name) = '" . $this->db->escape(strtolower($this->session->data['dept_name'])) . "' ";
		// }

		if (!empty($data['filter_dept'])) {
			$sql .= " AND LOWER(dept_id) = '" . $this->db->escape(strtolower($data['filter_dept'])) . "' ";
		}

		if (isset($data['filter_depts']) && !empty($data['filter_depts'])) {
			$sql .= " AND LOWER(dept_id) IN (" . strtolower($data['filter_depts']) . ") ";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND `dot` = '" . $this->db->escape($data['filter_date']) . "'";
		}

		if (!empty($data['filter_unit'])) {
			$sql .= " AND `unit_id` = '" . $this->db->escape($data['filter_unit']) . "'";
		}

		if (!empty($data['filter_group'])) {
			$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['filter_group'])) . "' ";
		}

		if (!empty($data['filter_leavetype'])) {
			$sql .= " AND `leave_type` = '" . $this->db->escape($data['filter_leavetype']) . "'";
		}

		if (!empty($data['filter_approval_1_by'])) {
			$sql .= " AND `approved_1_by` = '" . $this->db->escape($data['filter_approval_1_by']) . "'";
		}

		if (!empty($data['filter_approval_1'])) {
			if($data['filter_approval_1'] == '1'){
				$sql .= " AND `approval_1` = '0'";
			} else if($data['filter_approval_1'] == '3') {
				$sql .= " AND `approval_1` = '2'";
			} else {
				$sql .= " AND `approval_1` = '1'";	
			}
		}

		if (!empty($data['filter_approval_2'])) {
			if($data['filter_approval_2'] == '1'){
				$sql .= " AND `approval_2` = '0'";
			} else {
				$sql .= " AND `approval_2` = '1'";	
			}
		}

		if (!empty($data['filter_proc'])) {
			if($data['filter_proc'] == '1'){
				$sql .= " AND `a_status` = '0'";
			} else {
				$sql .= " AND `a_status` = '1'";	
			}
		}

		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$sql .= " GROUP BY batch_id ORDER BY batch_id DESC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//$this->log->write($sql);
		//echo $sql;exit;
		$query = $this->db->query($sql);
		//$this->log->write(print_r($query,true));
		return $query->rows;
	}

	public function getmanualTransaction_ess($data = array()) {
		$sql = "SELECT * FROM `oc_manual_punch` WHERE 1=1 ";
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}

		if (isset($this->session->data['emp_code'])) {
			if(isset($data['filter_reporting'])){
				$sql .= " AND `reporting_to` = '" . $this->db->escape($this->session->data['emp_code']) . "'";
			} else {
				if(!$this->user->isAdmin()){
					$sql .= " AND `emp_id` = '" . $this->db->escape($this->session->data['emp_code']) . "'";
				}
			}	
		}

		if (!empty($data['dept_id'])) {
			$sql .= " AND dept_id = '" . $this->db->escape($data['dept_id']) . "' ";
		}

		if (isset($data['filter_depts']) && !empty($data['filter_depts'])) {
			$sql .= " AND LOWER(dept_id) IN (" . strtolower($data['filter_depts']) . ") ";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND `dot` >= '" . $this->db->escape($data['filter_date']) . "'";
		}

		if (!empty($data['filter_date_to'])) {
			$sql .= " AND `dot` <= '" . $this->db->escape($data['filter_date_to']) . "'";
		}

		//if(isset($data['group_by'])){
			if (!empty($data['filter_leave_from'])) {
				$sql .= " AND `date` >= '" . $this->db->escape($data['filter_leave_from']) . "'";
			}

			if (!empty($data['filter_leave_to'])) {
				$sql .= " AND `date` <= '" . $this->db->escape($data['filter_leave_to']) . "'";
			}
		//}

		if (!empty($data['filter_unit'])) {
			$sql .= " AND `unit_id` = '" . $this->db->escape($data['filter_unit']) . "'";
		}

		if (!empty($data['filter_px_record_type'])) {
			$sql .= " AND `px_record_type` = '" . $this->db->escape($data['filter_px_record_type']) . "'";
		}
		
		if (!empty($data['filter_approval_1'])) {
			if($data['filter_approval_1'] == '1'){
				$sql .= " AND `approval_1` = '0'";
			} else if($data['filter_approval_1'] == '3') {
				$sql .= " AND `approval_1` = '2'";
			} else {
				$sql .= " AND `approval_1` = '1'";	
			}
		}

		if (!empty($data['filter_proc'])) {
			if($data['filter_proc'] == '1'){
				$sql .= " AND `p_status` = '0'";
			} else {
				$sql .= " AND `p_status` = '1'";	
			}
		}

		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		if(isset($data['group_by'])){
			$sql .= " AND `type` = '2' GROUP BY `batch_id` ORDER BY `dot` DESC";
		} else {
			$sql .= " AND `type` = '1' ORDER BY `dot` DESC";	
		}
		

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTotalmanualTransaction_ess($data = array()) {
		$sql = "SELECT * FROM `oc_manual_punch` WHERE 1=1 ";
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}

		if (isset($this->session->data['emp_code'])) {
			if(isset($data['filter_reporting'])){
				$sql .= " AND `reporting_to` = '" . $this->db->escape($this->session->data['emp_code']) . "'";
			} else {
				if(!$this->user->isAdmin()){
					$sql .= " AND `emp_id` = '" . $this->db->escape($this->session->data['emp_code']) . "'";
				}
			}	
		}

		if (!empty($data['dept_id'])) {
			$sql .= " AND dept_id = '" . $this->db->escape($data['dept_id']) . "' ";
		}

		if (isset($data['filter_depts']) && !empty($data['filter_depts'])) {
			$sql .= " AND LOWER(dept_id) IN (" . strtolower($data['filter_depts']) . ") ";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND `dot` >= '" . $this->db->escape($data['filter_date']) . "'";
		}

		if (!empty($data['filter_date_to'])) {
			$sql .= " AND `dot` <= '" . $this->db->escape($data['filter_date_to']) . "'";
		}

		//if(isset($data['group_by'])){
			if (!empty($data['filter_leave_from'])) {
				$sql .= " AND `date` >= '" . $this->db->escape($data['filter_leave_from']) . "'";
			}

			if (!empty($data['filter_leave_to'])) {
				$sql .= " AND `date` <= '" . $this->db->escape($data['filter_leave_to']) . "'";
			}
		//}

		if (!empty($data['filter_unit'])) {
			$sql .= " AND `unit_id` = '" . $this->db->escape($data['filter_unit']) . "'";
		}

		if (!empty($data['filter_px_record_type'])) {
			$sql .= " AND `px_record_type` = '" . $this->db->escape($data['filter_px_record_type']) . "'";
		}

		
		if (!empty($data['filter_approval_1'])) {
			if($data['filter_approval_1'] == '1'){
				$sql .= " AND `approval_1` = '0'";
			} else if($data['filter_approval_1'] == '3') {
				$sql .= " AND `approval_1` = '2'";
			} else {
				$sql .= " AND `approval_1` = '1'";	
			}
		}

		if (!empty($data['filter_proc'])) {
			if($data['filter_proc'] == '1'){
				$sql .= " AND `p_status` = '0'";
			} else {
				$sql .= " AND `p_status` = '1'";	
			}
		}

		if(isset($data['group_by'])){
			$sql .= " AND `type` = '2' GROUP BY `batch_id` ORDER BY `dot` DESC";
		} else {
			$sql .= " AND `type` = '1' ORDER BY `dot` DESC";	
		}
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->num_rows;
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function hoursToMinutes($hours) {
		if (strstr($hours, '.')){
			# Split hours and minutes.
			$separatedData = explode('.', $hours);
			$minutesInHours    = $separatedData[0] * 60;
			$minutesInDecimals = $separatedData[1];
			$totalMinutes = $minutesInHours + $minutesInDecimals;
		} else {
			$totalMinutes = $hours * 60;
		}
		//echo $totalMinutes;exit;
		return $totalMinutes;
	}

	function GetMonth($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
			// Add a day to the current date  
			$sCurrentDate = date("Y-m-d", strtotime("+1 month", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
			if($sCurrentDate < $sEndDate){
				$aDays[] = $sCurrentDate;  
			}
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}
}	
?>