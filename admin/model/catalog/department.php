<?php
class ModelCatalogDepartment extends Model {
	public function addDepartment($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "department` SET 
							`d_name` = '" . $this->db->escape($data['d_name']) . "', 
							`code` = '" . $this->db->escape($data['code']) . "', 
							`status` = '" . $data['status'] . "'
						");

		$department_id = $this->db->getLastId(); 
	}

	public function editDepartment($department_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "department SET 
							`d_name` = '" . $this->db->escape($data['d_name']) . "', 
							`code` = '" . $this->db->escape($data['code']) . "', 
							`status` = '" . $data['status'] . "' 
							WHERE department_id = '" . (int)$department_id . "'");
	}

	public function deleteDepartment($department_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "department WHERE department_id = '" . (int)$department_id . "'");
	}	

	public function getDepartment($department_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "department WHERE department_id = '" . (int)$department_id . "'");

		return $query->row;
	}

	public function getDepartments($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "department WHERE 1=1 ";

		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND department_id = '" . $data['filter_name_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(d_name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		
		$sort_data = array(
			'd_name'
		);		

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY d_name";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}		

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalDepartments() {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "department";
		
		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND department_id = '" . $data['filter_name_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(d_name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}	
}
?>