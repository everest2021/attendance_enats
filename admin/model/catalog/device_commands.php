<?php
class ModelCatalogDevicecommands extends Model {
	public function adddevice_commands($data) {
		// echo'<pre>';
		// print_r($data);
		// exit;
		$current= date('Y-m-d H:i:s');

		//echo "C:1:DATA UPDATE USERINFO PIN=308437	Name=	Grp=100";
		//echo "C:2:DATA UPDATE USERINFO PIN=308437	Name=	Grp=1";

		if($data['select_cmd'] == 'BUFD'){
			$command = "C:1:DATA UPDATE USERINFO PIN=".$data['emp_code']."	Name=	Grp=100";
			$sort_cmd = "Block Users From Device";
		} else if($data['select_cmd'] == 'UBUFD'){
			$command = "C:2:DATA UPDATE USERINFO PIN=".$data['emp_code']."	Name=	Grp=1";
			$sort_cmd = "Un-Block Users From Device";
		}

		$this->db->query("INSERT INTO `" . DB_PREFIX . "device_commands` SET device_serial_no = '" . $this->db->escape($data['device_serial_no']) . "', command = '" . $this->db->escape($command ). "', command_name = '" . $this->db->escape($sort_cmd) . "',  short_cmd_name = '" . $this->db->escape($data['select_cmd']) . "', emp_code = '".$data['emp_code']."', status = '" . (int)$data['status'] . "', added_date ='".$current."'");
	}

	
	public function editdevice_commands($id, $data) {
		$current= date('Y-m-d H:i:s');
		if($data['select_cmd'] == 'BUFD'){
			$command = "C:1:DATA UPDATE USERINFO PIN=".$data['emp_code']."	Name=	Grp=100";
			$sort_cmd = "Block Users From Device";
		} else if($data['select_cmd'] == 'UBUFD'){
			$command = "C:2:DATA UPDATE USERINFO PIN=".$data['emp_code']."	Name=	Grp=1";
			$sort_cmd = "Un-Block Users From Device";

		}
		$this->db->query("UPDATE `" . DB_PREFIX . "device_commands` SET device_serial_no = '" . $this->db->escape($data['device_serial_no']) . "', command = '" . $this->db->escape($command ). "', command_name = '" . $this->db->escape($sort_cmd) . "',  short_cmd_name = '" . $this->db->escape($data['select_cmd']) . "', emp_code = '".$data['emp_code']."', added_date = '" . $this->db->escape($current) . "', status = '" . (int)$data['status'] . "' WHERE id = '" . (int)$id . "'");
	}
		
	public function deletedevice_commands($id) {
		$current_date = date('Y-m-d');
		$back_two_days_only = date('Y-m-d',strtotime($current_date . ' -2 day'));
		$this->db->query("DELETE FROM `" . DB_PREFIX . "device_commands` WHERE DATE(`added_date`) <= '" . $back_two_days_only . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "device_commands` WHERE id = '" . $id . "'");
	}

	public function getdevice_commands($id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "device_commands` WHERE id = '" . $id . "'");

		return $query->row;
	}

	

	public function getdevice_commander($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "device_commands WHERE 1=1 ";

		if (!empty($data['filter_device_serial_no'])) {
			$data['filter_device_serial_no'] = html_entity_decode($data['filter_device_serial_no']);
			$sql .= " AND LOWER(device_serial_no) LIKE '%" . $this->db->escape(strtolower($data['filter_device_serial_no'])) . "%'";
		}

		$sort_data = array(
			'device_serial_no',
			'id'
		
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY id";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotaldevice_commander() {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "device_commands WHERE 1=1 ";
		
		if (!empty($data['filter_device_serial_no'])) {
			$sql .= " AND LOWER(device_serial_no) LIKE '%" . $this->db->escape(strtolower($data['filter_device_serial_no'])) . "%'";
		}

		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	
}
?>