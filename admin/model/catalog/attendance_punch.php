<?php
class ModelCatalogAttendancePunch extends Model {
	public function addemployee($data) {
		$data['dept_head_list'] = array();
		$full_name = $data['emp_name'];
		$full_name = preg_replace('/\s+/', ' ', $full_name);
		// echo $full_name ;
		// exit;
		$this->db->query("INSERT INTO " . DB_PREFIX . "employee SET 
							emp_name = '" . $this->db->escape($full_name) . "', 
							card = '" . $this->db->escape($data['card_no']) . "', 
							device_emp_code = '" . $this->db->escape($data['emp_code']) . "', 
							
							device_serial_no = '" . $this->db->escape($data['device_serial_no']) . "', 
							
							
							status = '" . $this->db->escape($data['status']) . "'");
		
	}

	public function editemployee($employee_id, $data) {
		$data['dept_head_list'] = array();
		$full_name = $data['emp_name'];
		$full_name = preg_replace('/\s+/', ' ', $full_name);
		
		$this->db->query("UPDATE " . DB_PREFIX . "employee SET 
							emp_name = '" . $this->db->escape($full_name) . "',
							card = '" . $this->db->escape($data['card_no']) . "', 
							device_emp_code = '" . $this->db->escape($data['emp_code']) . "', 
							
							device_serial_no = '" . $this->db->escape($data['device_serial_no']) . "',
							
							status = '" . $this->db->escape($data['status']) . "'
							
							WHERE emp_id = '" . (int)$employee_id . "'");
		
		 
	}

	public function deleteemployee($employee_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "employee WHERE emp_id = '" . (int)$employee_id . "'");
	}	

	public function getemployee($employee_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "employee WHERE emp_id = '" . (int)$employee_id . "'");
		return $query->row;
	}

	public function getcatname($category_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "categories WHERE id = '" . (int)$category_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	// public function getdeptname($department_id) {
	// 	$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "department WHERE id = '" . (int)$department_id . "'");
	// 	if($query->num_rows > 0){
	// 		return $query->row['name'];
	// 	} else {
	// 		return '';
	// 	}
	// }

	

	

	public function getlocname($location_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "location WHERE id = '" . (int)$location_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	public function getemployees($data = array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "attendance` WHERE 1=1  ";
		
			$sql .= " ORDER BY `id` ";	
		

		
			$sql .= " DESC";
		

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}					

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	
		//$this->log->write($sql);			
		//echo $sql;exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalemployees($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "attendance WHERE 1=1 ";
	
		$sql .= " ORDER BY `id` DESC";	
			
		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function getActiveEmployee() {
		$sql = "SELECT `emp_code`, `name`, `doj`, `grade`, `group` FROM ".DB_PREFIX."employee WHERE `status` = '1'";	
		$query = $this->db->query($sql);
		return $query->rows;	
	}

	public function getUnit() {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "unit ");
		return $query->rows;
	}

	// public function getDepartment() {
	// 	$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "department ");
	// 	return $query->rows;
	// }

	public function getshift() {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift ");
		return $query->rows;
	}
}
?>