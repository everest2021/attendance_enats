<?php
class ModelCatalogProjectAssignment extends Model {
	public function addprojectassignment($data) {
		if($data['project_transaction_id'] == '0'){
			$project_datas = $this->db->query("SELECT * FROM " . DB_PREFIX . "project WHERE id = '" . (int)$data['project_id']. "'")->row;
			$this->db->query("INSERT INTO " . DB_PREFIX . "project_transection SET  
								project_name = '" . $this->db->escape($project_datas['name']) . "',
								project_id = '" . $this->db->escape($data['project_id']) . "', 
								start_date = '" . $this->db->escape(date('Y-m-d', strtotime($data['start_date']))) . "', 
								end_date = '" . $this->db->escape(date('Y-m-d', strtotime($data['end_date']))) . "'
							");
			$project_transection = $this->db->getLastId();
		} else {
			$project_transection = $data['project_transaction_id'];
		}
		foreach($data['employee_list'] as $ekey => $evalue){
			$is_exist = $this->db->query("SELECT * FROM " . DB_PREFIX . "project_employee WHERE `project_id` = '".$project_transection."' AND `employee_id` = '".$evalue."' ");
			if($is_exist->num_rows == 0){
				$this->db->query("INSERT INTO " . DB_PREFIX . "project_employee SET  employee_id = '" . $this->db->escape($evalue) . "', project_id = '" . $this->db->escape($project_transection) . "' ");		
			}
		}
    }

	public function editprojectassignment($id, $data) {
		$project_datas = $this->db->query("SELECT * FROM " . DB_PREFIX . "project WHERE id = '" . (int)$data['project_id']. "'")->row;
		$this->db->query("UPDATE " . DB_PREFIX . "project_transection SET
							project_name = '" . $this->db->escape($project_datas['name']) . "',
							project_id = '" . $this->db->escape($data['project_id']) . "', 
							start_date = '" . $this->db->escape(date('Y-m-d', strtotime($data['start_date']))) . "', 
							end_date = '" . $this->db->escape(date('Y-m-d', strtotime($data['end_date']))) . "' 
							 WHERE id = '" . (int)$id . "'
						");
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "project_employee WHERE project_id = '" . (int)$id . "'");
		foreach($data['employee_list'] as $ekey => $evalue){
			$this->db->query("INSERT INTO " . DB_PREFIX . "project_employee SET  employee_id = '" . $this->db->escape($evalue) . "',project_id = '" . $this->db->escape($id) . "' ");		
		}
	}

	public function deleteprojectassignment($id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "project_transection WHERE id = '" . (int)$id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "project_employee WHERE project_id = '" . (int)$id . "'");
	}

	public function addemployeelist($id, $type, $page) {
		$project_query = $this->db->query("SELECT DISTINCT id FROM " . DB_PREFIX . "project_transection WHERE id = '" . (int)$id . "'");

		if ($project_assignment_query->num_rows) {
			$project_assignment_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "project_transection WHERE id = '" . (int)$project_query->row['id'] . "'");

			if ($project_assignment_query->num_rows) {
				$data = unserialize($project_assignment_query->row['employee_list']);

				$data[$type][] = $page;

				$this->db->query("UPDATE " . DB_PREFIX . "project_transection SET employee_list = '" . serialize($data) . "' WHERE id = '" . (int)$project_query->row['id'] . "'");
			}
		}
	}

	public function getprojectassignment($id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "project_transection WHERE id = '" . (int)$id . "'");

		$project_assignment = array(
			'project_id'       => $query->row['project_id'],
			'project_name'		=>$query->row['project_name'],
			'start_date'       => $query->row['start_date'],
			'end_date'       => $query->row['end_date'],
			//'employee' => unserialize($query->row['employee'])
		);

		return $project_assignment;
	}

	public function getprojectassignments($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "project_transection";

		$sql .= " ORDER BY project_name";	

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalprojectassignments() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "project_transection");

 	return $query->row['total'];
	}	
}
?>