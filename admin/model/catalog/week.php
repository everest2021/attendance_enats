<?php
class ModelCatalogweek extends Model {
	public function addweek($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "week SET `name` = '" . $this->db->escape($data['name']) . "', `date` = '" . $this->db->escape($data['date']) . "', `department_mumbai` = '" . $this->db->escape((isset($data['dept_holiday_mumbai'])) ? serialize($data['dept_holiday_mumbai']) : '') . "', `department_pune` = '" . $this->db->escape((isset($data['dept_holiday_pune'])) ? serialize($data['dept_holiday_pune']) : '') . "', `department_moving` = '" . $this->db->escape((isset($data['dept_holiday_moving'])) ? serialize($data['dept_holiday_moving']) : '') . "' ");
		$week_id = $this->db->getLastId();
		//$week_id = '59';
		$day_date = date('j', strtotime($data['date']));
		$week_ids = 'W_'.$week_id;
		
		if (isset($data['dept_holiday_mumbai'])) {
			foreach ($data['dept_holiday_mumbai'] as $dkey => $dvalue) {
				$dvalue = html_entity_decode(strtolower(trim($dvalue)));
				$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'mumbai' ");
				foreach ($emp_codes->rows as $ekey => $evalue) {
					//$this->db->query("INSERT INTO " . DB_PREFIX . "employee_meta_week SET `employee_code` = '".$evalue['emp_code']."', `week_id` = '" . $week_id . "' ");
					$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' ");	
					$current_shift = 'S_1';
					if($current_shifts->num_rows > 0){
						$current_shift = $current_shifts->row[$day_date];
					}
					$current_shift_exp = explode('_', $current_shift);
					$week_idss = $week_ids;
					if($current_shift_exp[0] == 'S'){
						$week_idss = $week_ids.'_'.$current_shift_exp[1];
					}
					$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$week_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' ");
				}
			}
		}
		if (isset($data['dept_holiday_pune'])) {
			foreach ($data['dept_holiday_pune'] as $dkey => $dvalue) {
				$dvalue = html_entity_decode(strtolower(trim($dvalue)));
				$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'pune' ");
				foreach ($emp_codes->rows as $ekey => $evalue) {
					//$this->db->query("INSERT INTO " . DB_PREFIX . "employee_meta_week SET `employee_code` = '".$evalue['emp_code']."', `week_id` = '" . $week_id . "' ");
					$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' ");	
					$current_shift = 'S_1';
					if($current_shifts->num_rows > 0){
						$current_shift = $current_shifts->row[$day_date];
					}
					$current_shift_exp = explode(_, $current_shift);
					$week_idss = $week_ids;
					if($current_shift_exp[0] == 'S'){
						$week_idss = $week_ids.'_'.$current_shift_exp[1];
					}
					$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$week_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' ");
				}
			}
		}
		if (isset($data['dept_holiday_moving'])) {
			foreach ($data['dept_holiday_moving'] as $dkey => $dvalue) {
				$dvalue = html_entity_decode(strtolower(trim($dvalue)));
				$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'moving' ");
				foreach ($emp_codes->rows as $ekey => $evalue) {
					//$this->db->query("INSERT INTO " . DB_PREFIX . "employee_meta_week SET `employee_code` = '".$evalue['emp_code']."', `week_id` = '" . $week_id . "' ");
					$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' ");	
					$current_shift = 'S_1';
					if($current_shifts->num_rows > 0){
						$current_shift = $current_shifts->row[$day_date];
					}
					$current_shift_exp = explode(_, $current_shift);
					$week_idss = $week_ids;
					if($current_shift_exp[0] == 'S'){
						$week_idss = $week_ids.'_'.$current_shift_exp[1];
					}
					$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$week_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' ");
				}
			}
		}
		// if(isset($data['loc_holiday']) && isset($data['dept_holiday'])){
		// 	foreach ($data['loc_holiday'] as $lkey => $lvalue) {
		// 		foreach ($data['dept_holiday'] as $dkey => $dvalue) {
		// 			$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `unit` = '".$lvalue."' AND `department` = '".$dvalue."' ");
		// 			foreach ($emp_codes->rows as $ekey => $evalue) {
		// 				//$this->db->query("INSERT INTO " . DB_PREFIX . "employee_meta_week SET `employee_code` = '".$evalue['emp_code']."', `week_id` = '" . $week_id . "' ");
		// 				$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$week_ids."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".date('n')."' AND `year` = '".date('Y')."' ");
		// 			}
		// 		}
		// 	}
		// } elseif (isset($data['loc_holiday'])) {
		// 	foreach ($data['loc_holiday'] as $lkey => $lvalue) {
		// 		$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `unit` = '".$lvalue."' ");
		// 		foreach ($emp_codes->rows as $ekey => $evalue) {
		// 			//$this->db->query("INSERT INTO " . DB_PREFIX . "employee_meta_week SET `employee_code` = '".$evalue['emp_code']."', `week_id` = '" . $week_id . "' ");
		// 			$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$week_ids."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".date('n')."' AND `year` = '".date('Y')."' ");
		// 		}
		// 	}
		// } elseif (isset($data['dept_holiday'])) {
		// 	foreach ($data['dept_holiday'] as $dkey => $dvalue) {
		// 		$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' ");
		// 		foreach ($emp_codes->rows as $ekey => $evalue) {
		// 			//$this->db->query("INSERT INTO " . DB_PREFIX . "employee_meta_week SET `employee_code` = '".$evalue['emp_code']."', `week_id` = '" . $week_id . "' ");
		// 			$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$week_ids."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".date('n')."' AND `year` = '".date('Y')."' ");
		// 		}
		// 	}
		// }
	}

	public function editweek($week_id, $data) {
		$week_data = $this->getweek($week_id);
		$dept_holiday_mumbai = unserialize($week_data['department_mumbai']);
		if($dept_holiday_mumbai){
			foreach ($dept_holiday_mumbai as $key => $value) {
				$dept_holiday_mumbai[$key] = html_entity_decode(strtolower(trim($value)));
			}
		} else {
			$dept_holiday_mumbai = array();
		}

		$dept_holiday_pune = unserialize($week_data['department_pune']);
		if($dept_holiday_pune){
			foreach ($dept_holiday_pune as $key => $value) {
				$dept_holiday_pune[$key] = html_entity_decode(strtolower(trim($value)));
			}
		} else {
			$dept_holiday_pune = array();
		}

		$dept_holiday_moving = unserialize($week_data['department_moving']);
		if($dept_holiday_moving){
			foreach ($dept_holiday_moving as $key => $value) {
				$dept_holiday_moving[$key] = html_entity_decode(strtolower(trim($value)));
			}
		} else {
			$dept_holiday_moving = array();
		}

		$this->db->query("UPDATE " . DB_PREFIX . "week SET `name` = '" . $this->db->escape($data['name']) . "', `date` = '" . $this->db->escape($data['date']) . "', `department_mumbai` = '" . $this->db->escape((isset($data['dept_holiday_mumbai'])) ? serialize($data['dept_holiday_mumbai']) : '') . "', `department_pune` = '" . $this->db->escape((isset($data['dept_holiday_pune'])) ? serialize($data['dept_holiday_pune']) : '') . "', `department_moving` = '" . $this->db->escape((isset($data['dept_holiday_moving'])) ? serialize($data['dept_holiday_moving']) : '') . "' WHERE week_id = '" . (int)$week_id . "'");
		//$this->db->query("DELETE FROM " . DB_PREFIX . "employee_meta_week WHERE week_id = '" . (int)$week_id . "'");
		$day_date = date('j', strtotime($data['date']));
		$week_ids = 'W_'.$week_id;
		
		$this->load->model('report/attendance');
		$department_datas = $this->model_report_attendance->getdepartment_list();
		$department_data = array();
		//$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[strtolower(trim($dvalue['department']))] = strtolower(trim($dvalue['department']));
		}

		if($this->user->getId() == 3){
			$this->log->write('in mumbai');
			if (isset($data['dept_holiday_mumbai'])) {
				if($dept_holiday_mumbai){
					$d_dept = array();
					foreach ($dept_holiday_mumbai as $d1key => $d1value) {
						foreach ($data['dept_holiday_mumbai'] as $dkey => $dvalue) {
							$dvalue = html_entity_decode(strtolower(trim($dvalue)));
							if(!in_array($dvalue, $d_dept)){
								$d_dept[] = $dvalue;
								$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'mumbai' ");
								foreach ($emp_codes->rows as $ekey => $evalue) {
									// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$week_ids."' WHERE `emp_code` = '".$evalue['emp_code']."' ";
									// echo '<br />';
									$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' ");	
									$current_shift = 'S_1';
									if($current_shifts->num_rows > 0){
										$current_shift = $current_shifts->row[$day_date];
									}
									$current_shift_exp = explode('_', $current_shift);
									$week_idss = $week_ids;
									if(isset($current_shift_exp[2])){
										$week_idss = $week_ids.'_'.$current_shift_exp[2];
									} else {
										if($current_shift_exp[0] == 'S'){
											$week_idss = $week_ids.'_'.$current_shift_exp[1];
										}
									}
									$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$week_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' ");
									$this->log->write("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$week_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' ");
								}
							} 
						}
					}
					foreach ($dept_holiday_mumbai as $d1key => $d1value) {
						$d1value = html_entity_decode(strtolower(trim($d1value)));
						if(!in_array($d1value, $d_dept)){
							$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'mumbai' ");
							foreach ($emp_codes->rows as $ekey => $evalue) {
								// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ";
								// echo '<br />';
								$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ");
								$this->log->write("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ");
							}
						}
					}
				} else {
					foreach ($data['dept_holiday_mumbai'] as $dkey => $dvalue) {
						$dvalue = html_entity_decode(strtolower(trim($dvalue)));
						$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'mumbai' ");
						foreach ($emp_codes->rows as $ekey => $evalue) {
							$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' ");	
							$current_shift = 'S_1';
							if($current_shifts->num_rows > 0){
								$current_shift = $current_shifts->row[$day_date];
							}
							$current_shift_exp = explode('_', $current_shift);
							$week_idss = $week_ids;
							if(isset($current_shift_exp[2])){
								$week_idss = $week_ids.'_'.$current_shift_exp[2];
							} else {
								if($current_shift_exp[0] == 'S'){
									$week_idss = $week_ids.'_'.$current_shift_exp[1];
								}
							}
							$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$week_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' ");
							$this->log->write("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$week_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' ");
						}
					}
				}
			} else {
				$d_dept = array();
				foreach ($dept_holiday_mumbai as $d1key => $d1value) {
					$d1value = html_entity_decode(strtolower(trim($d1value)));
					if(!in_array($d1value, $d_dept)){
						$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'mumbai' ");
						foreach ($emp_codes->rows as $ekey => $evalue) {
							// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ";
							// echo '<br />';
							$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ");
							$this->log->write("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ");
						}
					}
				}
			}
		}

		if($this->user->getId() == 4){
			$this->log->write('in pune');
			if (isset($data['dept_holiday_pune'])) {
				if($dept_holiday_pune){
					$d_dept = array();
					foreach ($dept_holiday_pune as $d1key => $d1value) {
						foreach ($data['dept_holiday_pune'] as $dkey => $dvalue) {
							$dvalue = html_entity_decode(strtolower(trim($dvalue)));
							if(!in_array($dvalue, $d_dept)){
								$d_dept[] = $dvalue;
								$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'pune' ");
								foreach ($emp_codes->rows as $ekey => $evalue) {
									// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$week_ids."' WHERE `emp_code` = '".$evalue['emp_code']."' ";
									// echo '<br />';
									$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' ");	
									$current_shift = 'S_1';
									if($current_shifts->num_rows > 0){
										$current_shift = $current_shifts->row[$day_date];
									}
									$current_shift_exp = explode('_', $current_shift);
									$week_idss = $week_ids;
									if(isset($current_shift_exp[2])){
										$week_idss = $week_ids.'_'.$current_shift_exp[2];
									} else {
										if($current_shift_exp[0] == 'S'){
											$week_idss = $week_ids.'_'.$current_shift_exp[1];
										}
									}
									$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$week_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' ");
								}
							} 
						}
					}
					foreach ($dept_holiday_pune as $d1key => $d1value) {
						$d1value = html_entity_decode(strtolower(trim($d1value)));
						if(!in_array($d1value, $d_dept)){
							$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'pune' ");
							foreach ($emp_codes->rows as $ekey => $evalue) {
								// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ";
								// echo '<br />';
								$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ");
							}
						}
					}
				} else {
					foreach ($data['dept_holiday_pune'] as $dkey => $dvalue) {
						$dvalue = html_entity_decode(strtolower(trim($dvalue)));
						$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'pune' ");
						foreach ($emp_codes->rows as $ekey => $evalue) {
							$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' ");	
							$current_shift = 'S_1';
							if($current_shifts->num_rows > 0){
								$current_shift = $current_shifts->row[$day_date];
							}
							$current_shift_exp = explode('_', $current_shift);
							$week_idss = $week_ids;
							if(isset($current_shift_exp[2])){
								$week_idss = $week_ids.'_'.$current_shift_exp[2];
							} else {
								if($current_shift_exp[0] == 'S'){
									$week_idss = $week_ids.'_'.$current_shift_exp[1];
								}
							}
							$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$week_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' ");
						}
					}
				}
			} else {
				$d_dept = array();
				foreach ($dept_holiday_pune as $d1key => $d1value) {
					$d1value = html_entity_decode(strtolower(trim($d1value)));
					if(!in_array($d1value, $d_dept)){
						$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'pune' ");
						foreach ($emp_codes->rows as $ekey => $evalue) {
							// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ";
							// echo '<br />';
							$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ");
						}
					}
				}
			}
		}

		if($this->user->getId() == 7){
			$this->log->write('in moving');
			if (isset($data['dept_holiday_moving'])) {
				if($dept_holiday_moving){
					$d_dept = array();
					foreach ($dept_holiday_moving as $d1key => $d1value) {
						foreach ($data['dept_holiday_moving'] as $dkey => $dvalue) {
							$dvalue = html_entity_decode(strtolower(trim($dvalue)));
							if(!in_array($dvalue, $d_dept)){
								$d_dept[] = $dvalue;
								$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'moving' ");
								foreach ($emp_codes->rows as $ekey => $evalue) {
									// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$week_ids."' WHERE `emp_code` = '".$evalue['emp_code']."' ";
									// echo '<br />';
									$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' ");	
									$current_shift = 'S_1';
									if($current_shifts->num_rows > 0){
										$current_shift = $current_shifts->row[$day_date];
									}
									$current_shift_exp = explode('_', $current_shift);
									$week_idss = $week_ids;
									if(isset($current_shift_exp[2])){
										$week_idss = $week_ids.'_'.$current_shift_exp[2];
									} else {
										if($current_shift_exp[0] == 'S'){
											$week_idss = $week_ids.'_'.$current_shift_exp[1];
										}
									}
									$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$week_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' ");
								}
							} 
						}
					}
					foreach ($dept_holiday_moving as $d1key => $d1value) {
						$d1value = html_entity_decode(strtolower(trim($d1value)));
						if(!in_array($d1value, $d_dept)){
							$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'moving' ");
							foreach ($emp_codes->rows as $ekey => $evalue) {
								// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ";
								// echo '<br />';
								$this->log->write("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ");
								$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ");
							}
						}
					}
				} else {
					foreach ($data['dept_holiday_moving'] as $dkey => $dvalue) {
						$dvalue = html_entity_decode(strtolower(trim($dvalue)));
						$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'moving' ");
						foreach ($emp_codes->rows as $ekey => $evalue) {
							$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' ");	
							$current_shift = 'S_1';
							if($current_shifts->num_rows > 0){
								$current_shift = $current_shifts->row[$day_date];
							}
							$current_shift_exp = explode('_', $current_shift);
							$week_idss = $week_ids;
							if(isset($current_shift_exp[2])){
								$week_idss = $week_ids.'_'.$current_shift_exp[2];
							} else {
								if($current_shift_exp[0] == 'S'){
									$week_idss = $week_ids.'_'.$current_shift_exp[1];
								}
							}
							$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$week_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' ");
						}
					}
				}
			} else {
				$d_dept = array();
				foreach ($dept_holiday_moving as $d1key => $d1value) {
					$d1value = html_entity_decode(strtolower(trim($d1value)));
					if(!in_array($d1value, $d_dept)){
						$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'moving' ");
						foreach ($emp_codes->rows as $ekey => $evalue) {
							// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ";
							// echo '<br />';
							$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ");
						}
					}
				}
			}
		}
		
		// if (isset($data['dept_holiday_pune'])) {
		// 	foreach ($data['dept_holiday_pune'] as $dkey => $dvalue) {
		// 		$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'pune' ");
		// 		foreach ($emp_codes->rows as $ekey => $evalue) {
		// 			//$this->db->query("INSERT INTO " . DB_PREFIX . "employee_meta_week SET `employee_code` = '".$evalue['emp_code']."', `week_id` = '" . $week_id . "' ");
		// 			$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$week_ids."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".date('n')."' AND `year` = '".date('Y')."' ");
		// 		}
		// 	}
		// }
		// if (isset($data['dept_holiday_moving'])) {
		// 	foreach ($data['dept_holiday_moving'] as $dkey => $dvalue) {
		// 		$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'moving' ");
		// 		foreach ($emp_codes->rows as $ekey => $evalue) {
		// 			//$this->db->query("INSERT INTO " . DB_PREFIX . "employee_meta_week SET `employee_code` = '".$evalue['emp_code']."', `week_id` = '" . $week_id . "' ");
		// 			$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$week_ids."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".date('n')."' AND `year` = '".date('Y')."' ");
		// 		}
		// 	}
		// }
		// if(isset($data['loc_holiday']) && isset($data['dept_holiday'])){
		// 	foreach ($data['loc_holiday'] as $lkey => $lvalue) {
		// 		foreach ($data['dept_holiday'] as $dkey => $dvalue) {
		// 			$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `unit` = '".$lvalue."' AND `department` = '".$dvalue."' ");
		// 			foreach ($emp_codes->rows as $ekey => $evalue) {
		// 				//$this->db->query("INSERT INTO " . DB_PREFIX . "employee_meta_week SET `employee_code` = '".$evalue['emp_code']."', `week_id` = '" . $week_id . "' ");
		// 				$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$week_ids."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".date('n')."' AND `year` = '".date('Y')."' ");
		// 			}
		// 		}
		// 	}
		// } elseif (isset($data['loc_holiday'])) {
		// 	foreach ($data['loc_holiday'] as $lkey => $lvalue) {
		// 		$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `unit` = '".$lvalue."' ");
		// 		foreach ($emp_codes->rows as $ekey => $evalue) {
		// 			//$this->db->query("INSERT INTO " . DB_PREFIX . "employee_meta_week SET `employee_code` = '".$evalue['emp_code']."', `week_id` = '" . $week_id . "' ");
		// 			$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$week_ids."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".date('n')."' AND `year` = '".date('Y')."' ");
		// 		}
		// 	}
		// } elseif (isset($data['dept_holiday'])) {
		// 	foreach ($data['dept_holiday'] as $dkey => $dvalue) {
		// 		$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' ");
		// 		foreach ($emp_codes->rows as $ekey => $evalue) {
		// 			//$this->db->query("INSERT INTO " . DB_PREFIX . "employee_meta_week SET `employee_code` = '".$evalue['emp_code']."', `week_id` = '" . $week_id . "' ");
		// 			$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$week_ids."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".date('n')."' AND `year` = '".date('Y')."' ");
		// 		}
		// 	}
		// }
		//$this->log->write("UPDATE " . DB_PREFIX . "week SET `name` = '" . $this->db->escape($data['name']) . "', `in_time` = '" . $this->db->escape($data['in_time']) . "', `out_time` = '" . $this->db->escape($data['out_time']) . "', `weekly_off_1` = '" . $this->db->escape($data['weekly_off_1']) . "', `weekly_off_2` = '" . $this->db->escape($data['weekly_off_2']) . "' WHERE week_id = '" . (int)$week_id . "'");
	}

	public function deleteweek($week_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "week WHERE week_id = '" . (int)$week_id . "'");
	}	

	public function getweek($week_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "week WHERE week_id = '" . (int)$week_id . "'");
		return $query->row;
	}

	public function getweeks($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "week WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		$sort_data = array(
			'name',
		);	

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}					

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}				

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalweeks($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "week WHERE 1=1 ";
		
		if (!empty($data['filter_name'])) {
			$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
		}

		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}
?>
