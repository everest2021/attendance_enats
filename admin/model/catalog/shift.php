<?php
class ModelCatalogShift extends Model {
	public function addshift($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "shift SET `name` = '" . $this->db->escape($data['name']) . "', `shift_code` = '" . $this->db->escape($data['shift_code']) . "', `in_time` = '" . $this->db->escape($data['in_time']) . "', `out_time` = '" . $this->db->escape($data['out_time']) . "', `weekly_off_1` = '" . $this->db->escape($data['weekly_off_1']) . "', `weekly_off_2` = '" . $this->db->escape($data['weekly_off_2']) . "', `department_mumbai` = '" . $this->db->escape((isset($data['dept_holiday_mumbai'])) ? serialize($data['dept_holiday_mumbai']) : '') . "', `department_pune` = '" . $this->db->escape((isset($data['dept_holiday_pune'])) ? serialize($data['dept_holiday_pune']) : '') . "', `department_moving` = '" . $this->db->escape((isset($data['dept_holiday_moving'])) ? serialize($data['dept_holiday_moving']) : '') . "', `lunch` = '".$data['lunch']."' ");
		$shift_id = $this->db->getLastId();
		$shift_id = 'S_'.$shift_id;
		if(isset($data['loc_holiday']) && isset($data['dept_holiday'])){
			foreach ($data['loc_holiday'] as $lkey => $lvalue) {
				foreach ($data['dept_holiday'] as $dkey => $dvalue) {
					$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE `shift_type` = 'F' AND `unit` = '".$lvalue."' AND `department` = '".$dvalue."' ");
					foreach ($emp_codes->rows as $ekey => $evalue) {
						// $shift_s_datas = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$evalue['emp_code'] . "' ");
						// foreach($shift_s_datas->rows as $skey => $svalue){
						// 	if($svalue[''])
						// }
						$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `1` = '".$shift_id."', `2` = '".$shift_id."', `3` = '".$shift_id."', `4` = '".$shift_id."', `5` = '".$shift_id."', `6` = '".$shift_id."', `7` = '".$shift_id."', `8` = '".$shift_id."', `9` = '".$shift_id."', `10` = '".$shift_id."', `11` = '".$shift_id."', `12` = '".$shift_id."', `13` = '".$shift_id."', `14` = '".$shift_id."', `15` = '".$shift_id."', `16` = '".$shift_id."', `17` = '".$shift_id."', `18` = '".$shift_id."', `19` = '".$shift_id."', `20` = '".$shift_id."', `21` = '".$shift_id."', `22` = '".$shift_id."', `23` = '".$shift_id."', `24` = '".$shift_id."', `25` = '".$shift_id."', `26` = '".$shift_id."', `27` = '".$shift_id."', `28` = '".$shift_id."', `29` = '".$shift_id."', `30` = '".$shift_id."', `31` = '".$shift_id."' WHERE `emp_code` = '".$evalue['emp_code']."' ");
					}
				}
			}
		} elseif (isset($data['loc_holiday'])) {
			foreach ($data['loc_holiday'] as $lkey => $lvalue) {
				$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE `shift_type` = 'F' AND `unit` = '".$lvalue."' ");
				foreach ($emp_codes->rows as $ekey => $evalue) {
					$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `1` = '".$shift_id."', `2` = '".$shift_id."', `3` = '".$shift_id."', `4` = '".$shift_id."', `5` = '".$shift_id."', `6` = '".$shift_id."', `7` = '".$shift_id."', `8` = '".$shift_id."', `9` = '".$shift_id."', `10` = '".$shift_id."', `11` = '".$shift_id."', `12` = '".$shift_id."', `13` = '".$shift_id."', `14` = '".$shift_id."', `15` = '".$shift_id."', `16` = '".$shift_id."', `17` = '".$shift_id."', `18` = '".$shift_id."', `19` = '".$shift_id."', `20` = '".$shift_id."', `21` = '".$shift_id."', `22` = '".$shift_id."', `23` = '".$shift_id."', `24` = '".$shift_id."', `25` = '".$shift_id."', `26` = '".$shift_id."', `27` = '".$shift_id."', `28` = '".$shift_id."', `29` = '".$shift_id."', `30` = '".$shift_id."', `31` = '".$shift_id."' WHERE `emp_code` = '".$evalue['emp_code']."' ");
				}
			}
		} elseif (isset($data['dept_holiday'])) {
			foreach ($data['dept_holiday'] as $dkey => $dvalue) {
				$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE `shift_type` = 'F' AND `department` = '".$dvalue."' ");
				foreach ($emp_codes->rows as $ekey => $evalue) {
					$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `1` = '".$shift_id."', `2` = '".$shift_id."', `3` = '".$shift_id."', `4` = '".$shift_id."', `5` = '".$shift_id."', `6` = '".$shift_id."', `7` = '".$shift_id."', `8` = '".$shift_id."', `9` = '".$shift_id."', `10` = '".$shift_id."', `11` = '".$shift_id."', `12` = '".$shift_id."', `13` = '".$shift_id."', `14` = '".$shift_id."', `15` = '".$shift_id."', `16` = '".$shift_id."', `17` = '".$shift_id."', `18` = '".$shift_id."', `19` = '".$shift_id."', `20` = '".$shift_id."', `21` = '".$shift_id."', `22` = '".$shift_id."', `23` = '".$shift_id."', `24` = '".$shift_id."', `25` = '".$shift_id."', `26` = '".$shift_id."', `27` = '".$shift_id."', `28` = '".$shift_id."', `29` = '".$shift_id."', `30` = '".$shift_id."', `31` = '".$shift_id."' WHERE `emp_code` = '".$evalue['emp_code']."' ");
				}
			}
		}
	}

	public function new_mysql($sql) {
		$con=mysqli_connect("localhost","root","root","db_attendance_blank");
		mysqli_multi_query($con,$sql);
		do {
		    if($result = mysqli_store_result($con)){
		        mysqli_free_result($result);
		    }
		} while(mysqli_next_result($con));
		if(mysqli_error($con)) {
		    die(mysqli_error($con));
		}
		// while (mysqli_next_result($link)) {
		// 	if (!mysqli_more_results()){
		// 		break;
		// 	}
		// }
		mysqli_close($con);
	}

	public function editshift($shift_id, $data) {
		$week_data = $this->getshift($shift_id);
		$dept_holiday_mumbai = unserialize($week_data['department_mumbai']);
		if($dept_holiday_mumbai){
			foreach ($dept_holiday_mumbai as $key => $value) {
				$dept_holiday_mumbai[$key] = html_entity_decode(strtolower(trim($value)));
			}
		} else {
			$dept_holiday_mumbai = array();
		}

		$dept_holiday_pune = unserialize($week_data['department_pune']);
		if($dept_holiday_pune){
			foreach ($dept_holiday_pune as $key => $value) {
				$dept_holiday_pune[$key] = html_entity_decode(strtolower(trim($value)));
			}
		} else {
			$dept_holiday_pune = array();
		}

		$dept_holiday_moving = unserialize($week_data['department_moving']);
		if($dept_holiday_moving){
			foreach ($dept_holiday_moving as $key => $value) {
				$dept_holiday_moving[$key] = html_entity_decode(strtolower(trim($value)));
			}
		} else {
			$dept_holiday_moving = array();
		}


		$this->db->query("UPDATE " . DB_PREFIX . "shift SET `name` = '" . $this->db->escape($data['name']) . "', `shift_code` = '" . $this->db->escape($data['shift_code']) . "', `in_time` = '" . $this->db->escape($data['in_time']) . "', `out_time` = '" . $this->db->escape($data['out_time']) . "', `weekly_off_1` = '" . $this->db->escape($data['weekly_off_1']) . "', `weekly_off_2` = '" . $this->db->escape($data['weekly_off_2']) . "', `department_mumbai` = '" . $this->db->escape((isset($data['dept_holiday_mumbai'])) ? serialize($data['dept_holiday_mumbai']) : '') . "', `department_pune` = '" . $this->db->escape((isset($data['dept_holiday_pune'])) ? serialize($data['dept_holiday_pune']) : '') . "', `department_moving` = '" . $this->db->escape((isset($data['dept_holiday_moving'])) ? serialize($data['dept_holiday_moving']) : '') . "', `lunch` = '".$data['lunch']."' WHERE shift_id = '" . (int)$shift_id . "'");
		$r_shift_id = $shift_id;
		$shift_id = 'S_'.$shift_id;
		
		$this->load->model('report/attendance');
		$department_datas = $this->model_report_attendance->getdepartment_list();
		$department_data = array();
		//$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[strtolower(trim($dvalue['department']))] = strtolower(trim($dvalue['department']));
		}

		if($this->user->getId() == 3){
			$this->log->write('in mumbai');
			if (isset($data['dept_holiday_mumbai'])) {
				if($dept_holiday_mumbai){
					$d_dept = array();
					$sql = '';
					// echo '<pre>';
					// print_r($dept_holiday_mumbai);
					// echo '<pre>';
					// print_r($data['dept_holiday_mumbai']);
					// exit;
					foreach ($dept_holiday_mumbai as $d1key => $d1value) {
						foreach ($data['dept_holiday_mumbai'] as $dkey => $dvalue) {
							$dvalue = html_entity_decode(strtolower(trim($dvalue)));
							if(!in_array($dvalue, $d_dept)){
								$d_dept[] = $dvalue;
								$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'mumbai' ");
								foreach ($emp_codes->rows as $ekey => $evalue) {
									$shift_s_datas = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$evalue['emp_code'] . "' ");
									foreach($shift_s_datas->rows as $skeys => $svalues){
										$icnt = 0;
										foreach($svalues as $skey => $svalue){
											if($icnt > 0 && $icnt <= 31){
												$s_data_exp = explode('_', $svalue);
												if($s_data_exp[0] == 'H' || $s_data_exp[0] == 'HD' || $s_data_exp[0] == 'W' || $s_data_exp[0] == 'C'){
													foreach($s_data_exp as $sdxkey => $sdxvalue){
														if($sdxkey == 2){
															$s_data_exp[$sdxkey] = $r_shift_id;
														}
													}
													$svalue = implode('_', $s_data_exp);
													$sql .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($svalue) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "';";
												} else {
													$sql .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($shift_id) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "';";
												}
												
												//$this->db->query($sql);
											}
											$icnt ++;
										}
									}
									//$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `1` = '".$shift_id."', `2` = '".$shift_id."', `3` = '".$shift_id."', `4` = '".$shift_id."', `5` = '".$shift_id."', `6` = '".$shift_id."', `7` = '".$shift_id."', `8` = '".$shift_id."', `9` = '".$shift_id."', `10` = '".$shift_id."', `11` = '".$shift_id."', `12` = '".$shift_id."', `13` = '".$shift_id."', `14` = '".$shift_id."', `15` = '".$shift_id."', `16` = '".$shift_id."', `17` = '".$shift_id."', `18` = '".$shift_id."', `19` = '".$shift_id."', `20` = '".$shift_id."', `21` = '".$shift_id."', `22` = '".$shift_id."', `23` = '".$shift_id."', `24` = '".$shift_id."', `25` = '".$shift_id."', `26` = '".$shift_id."', `27` = '".$shift_id."', `28` = '".$shift_id."', `29` = '".$shift_id."', `30` = '".$shift_id."', `31` = '".$shift_id."' WHERE `emp_code` = '".$evalue['emp_code']."' ");
								}
							} 
						}
					}

					$this->new_mysql($sql);
					$sql_1 = "";
					foreach ($dept_holiday_mumbai as $d1key => $d1value) {
						$d1value = html_entity_decode(strtolower(trim($d1value)));
						if(!in_array($d1value, $d_dept)){
							$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'mumbai' ");
							foreach ($emp_codes->rows as $ekey => $evalue) {
								$shift_s_datas = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$evalue['emp_code'] . "' ");
								foreach($shift_s_datas->rows as $skeys => $svalues){
									$icnt = 0;
									foreach($svalues as $skey => $svalue){
										if($icnt > 0 && $icnt <= 31){
											$s_data_exp = explode('_', $svalue);
											if($s_data_exp[0] == 'H' || $s_data_exp[0] == 'HD' || $s_data_exp[0] == 'W' || $s_data_exp[0] == 'C'){
												foreach($s_data_exp as $sdxkey => $sdxvalue){
													if($sdxkey == 2){
														$s_data_exp[$sdxkey] = $r_shift_id;
													}
												}
												$svalue = implode('_', $s_data_exp);
												$sql_1 .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($svalue) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "';";
											} else {
												$sql_1 .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = 'S_1' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "';";
											}
											//$this->db->query($sql);
										}
										$icnt ++;
									}
								}
								//$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `1` = 'S_1', `2` = 'S_1', `3` = 'S_1', `4` = 'S_1', `5` = 'S_1', `6` = 'S_1', `7` = 'S_1', `8` = 'S_1', `9` = 'S_1', `10` = 'S_1', `11` = 'S_1', `12` = 'S_1', `13` = 'S_1', `14` = 'S_1', `15` = 'S_1', `16` = 'S_1', `17` = 'S_1', `18` = 'S_1', `19` = 'S_1', `20` = 'S_1', `21` = 'S_1', `22` = 'S_1', `23` = 'S_1', `24` = 'S_1', `25` = 'S_1', `26` = 'S_1', `27` = 'S_1', `28` = 'S_1', `29` = 'S_1', `30` = 'S_1', `31` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ");
							}
						}
					}
					$this->new_mysql($sql_1);
					
				} else {
					$sql_2 = "";
					foreach ($data['dept_holiday_mumbai'] as $dkey => $dvalue) {
						$dvalue = html_entity_decode(strtolower(trim($dvalue)));
						$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'mumbai' ");
						foreach ($emp_codes->rows as $ekey => $evalue) {
							$shift_s_datas = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$evalue['emp_code'] . "' ");
							foreach($shift_s_datas->rows as $skeys => $svalues){
								$icnt = 0;
								foreach($svalues as $skey => $svalue){
									if($icnt > 0 && $icnt <= 31){
										$s_data_exp = explode('_', $svalue);
										if($s_data_exp[0] == 'H' || $s_data_exp[0] == 'HD' || $s_data_exp[0] == 'W' || $s_data_exp[0] == 'C'){
											foreach($s_data_exp as $sdxkey => $sdxvalue){
												if($sdxkey == 2){
													$s_data_exp[$sdxkey] = $r_shift_id;
												}
											}
											$svalue = implode('_', $s_data_exp);
											$sql_2 .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($svalue) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "'; ";
										} else {
											$sql_2 .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($shift_id) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "'; ";
										}
										//$this->db->query($sql);
									}
									$icnt ++;
								}
							}
							//$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `1` = '".$shift_id."', `2` = '".$shift_id."', `3` = '".$shift_id."', `4` = '".$shift_id."', `5` = '".$shift_id."', `6` = '".$shift_id."', `7` = '".$shift_id."', `8` = '".$shift_id."', `9` = '".$shift_id."', `10` = '".$shift_id."', `11` = '".$shift_id."', `12` = '".$shift_id."', `13` = '".$shift_id."', `14` = '".$shift_id."', `15` = '".$shift_id."', `16` = '".$shift_id."', `17` = '".$shift_id."', `18` = '".$shift_id."', `19` = '".$shift_id."', `20` = '".$shift_id."', `21` = '".$shift_id."', `22` = '".$shift_id."', `23` = '".$shift_id."', `24` = '".$shift_id."', `25` = '".$shift_id."', `26` = '".$shift_id."', `27` = '".$shift_id."', `28` = '".$shift_id."', `29` = '".$shift_id."', `30` = '".$shift_id."', `31` = '".$shift_id."' WHERE `emp_code` = '".$evalue['emp_code']."' ");
						}
					}
					$this->new_mysql($sql_2);
				}
			} else {
				$sql_3 = "";
				$d_dept = array();
				foreach ($dept_holiday_mumbai as $d1key => $d1value) {
					$d1value = html_entity_decode(strtolower(trim($d1value)));
					if(!in_array($d1value, $d_dept)){
						$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'mumbai' ");
						foreach ($emp_codes->rows as $ekey => $evalue) {
							$shift_s_datas = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$evalue['emp_code'] . "' ");
							foreach($shift_s_datas->rows as $skeys => $svalues){
								$icnt = 0;
								foreach($svalues as $skey => $svalue){
									if($icnt > 0 && $icnt <= 31){
										$s_data_exp = explode('_', $svalue);
										if($s_data_exp[0] == 'H' || $s_data_exp[0] == 'HD' || $s_data_exp[0] == 'W' || $s_data_exp[0] == 'C'){
											foreach($s_data_exp as $sdxkey => $sdxvalue){
												if($sdxkey == 2){
													$s_data_exp[$sdxkey] = $r_shift_id;
												}
											}
											$svalue = implode('_', $s_data_exp);
											$sql_3 .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($svalue) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "';";
										} else {
											$sql_3 .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = 'S_1' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "';";
										}
										//$this->db->query($sql);
									}
									$icnt ++;
								}
							}
							//$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `1` = 'S_1', `2` = 'S_1', `3` = 'S_1', `4` = 'S_1', `5` = 'S_1', `6` = 'S_1', `7` = 'S_1', `8` = 'S_1', `9` = 'S_1', `10` = 'S_1', `11` = 'S_1', `12` = 'S_1', `13` = 'S_1', `14` = 'S_1', `15` = 'S_1', `16` = 'S_1', `17` = 'S_1', `18` = 'S_1', `19` = 'S_1', `20` = 'S_1', `21` = 'S_1', `22` = 'S_1', `23` = 'S_1', `24` = 'S_1', `25` = 'S_1', `26` = 'S_1', `27` = 'S_1', `28` = 'S_1', `29` = 'S_1', `30` = 'S_1', `31` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ");
						}
					}
				}
				$this->new_mysql($sql_3);
			}
		}
		
		if($this->user->getId() == 4){
			$this->log->write('in pune');
			if (isset($data['dept_holiday_pune'])) {
				if($dept_holiday_pune){
					$d_dept = array();
					$sql = "";
					foreach ($dept_holiday_pune as $d1key => $d1value) {
						foreach ($data['dept_holiday_pune'] as $dkey => $dvalue) {
							$dvalue = html_entity_decode(strtolower(trim($dvalue)));
							if(!in_array($dvalue, $d_dept)){
								$d_dept[] = $dvalue;
								$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'pune' ");
								foreach ($emp_codes->rows as $ekey => $evalue) {
									$shift_s_datas = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$evalue['emp_code'] . "' ");
									foreach($shift_s_datas->rows as $skeys => $svalues){
										$icnt = 0;
										foreach($svalues as $skey => $svalue){
											if($icnt > 0 && $icnt <= 31){
												$s_data_exp = explode('_', $svalue);
												if($s_data_exp[0] == 'H' || $s_data_exp[0] == 'HD' || $s_data_exp[0] == 'W' || $s_data_exp[0] == 'C'){
													foreach($s_data_exp as $sdxkey => $sdxvalue){
														if($sdxkey == 2){
															$s_data_exp[$sdxkey] = $r_shift_id;
														}
													}
													$svalue = implode('_', $s_data_exp);
													$sql .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($svalue) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "'; ";
												} else {
													$sql .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($shift_id) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "'; ";
												}
												//$this->db->query($sql);
											}
											$icnt ++;
										}
									}
									//$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `1` = '".$shift_id."', `2` = '".$shift_id."', `3` = '".$shift_id."', `4` = '".$shift_id."', `5` = '".$shift_id."', `6` = '".$shift_id."', `7` = '".$shift_id."', `8` = '".$shift_id."', `9` = '".$shift_id."', `10` = '".$shift_id."', `11` = '".$shift_id."', `12` = '".$shift_id."', `13` = '".$shift_id."', `14` = '".$shift_id."', `15` = '".$shift_id."', `16` = '".$shift_id."', `17` = '".$shift_id."', `18` = '".$shift_id."', `19` = '".$shift_id."', `20` = '".$shift_id."', `21` = '".$shift_id."', `22` = '".$shift_id."', `23` = '".$shift_id."', `24` = '".$shift_id."', `25` = '".$shift_id."', `26` = '".$shift_id."', `27` = '".$shift_id."', `28` = '".$shift_id."', `29` = '".$shift_id."', `30` = '".$shift_id."', `31` = '".$shift_id."' WHERE `emp_code` = '".$evalue['emp_code']."' ");
								}
							} 
						}
					}
					$this->new_mysql($sql);
					$sql_1 = "";
					foreach ($dept_holiday_pune as $d1key => $d1value) {
						$d1value = html_entity_decode(strtolower(trim($d1value)));
						if(!in_array($d1value, $d_dept)){
							$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'pune' ");
							foreach ($emp_codes->rows as $ekey => $evalue) {
								$shift_s_datas = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$evalue['emp_code'] . "' ");
								foreach($shift_s_datas->rows as $skeys => $svalues){
									$icnt = 0;
									foreach($svalues as $skey => $svalue){
										if($icnt > 0 && $icnt <= 31){
											$s_data_exp = explode('_', $svalue);
											if($s_data_exp[0] == 'H' || $s_data_exp[0] == 'HD' || $s_data_exp[0] == 'W' || $s_data_exp[0] == 'C'){
												foreach($s_data_exp as $sdxkey => $sdxvalue){
													if($sdxkey == 2){
														$s_data_exp[$sdxkey] = $r_shift_id;
													}
												}
											$svalue = implode('_', $s_data_exp);
												$sql_1 .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($svalue) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "'; ";
											} else {
												$sql_1 .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = 'S_1' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "'; ";
											}
											//$this->db->query($sql);
										}
										$icnt ++;
									}
								}
								//$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `1` = 'S_1', `2` = 'S_1', `3` = 'S_1', `4` = 'S_1', `5` = 'S_1', `6` = 'S_1', `7` = 'S_1', `8` = 'S_1', `9` = 'S_1', `10` = 'S_1', `11` = 'S_1', `12` = 'S_1', `13` = 'S_1', `14` = 'S_1', `15` = 'S_1', `16` = 'S_1', `17` = 'S_1', `18` = 'S_1', `19` = 'S_1', `20` = 'S_1', `21` = 'S_1', `22` = 'S_1', `23` = 'S_1', `24` = 'S_1', `25` = 'S_1', `26` = 'S_1', `27` = 'S_1', `28` = 'S_1', `29` = 'S_1', `30` = 'S_1', `31` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ");
							}
						}
					}
					$this->new_mysql($sql_1);
				} else {
					$sql_2 ="";
					foreach ($data['dept_holiday_pune'] as $dkey => $dvalue) {
						$dvalue = html_entity_decode(strtolower(trim($dvalue)));
						$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'pune' ");
						foreach ($emp_codes->rows as $ekey => $evalue) {
							$shift_s_datas = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$evalue['emp_code'] . "' ");
							foreach($shift_s_datas->rows as $skeys => $svalues){
								$icnt = 0;
								foreach($svalues as $skey => $svalue){
									if($icnt > 0 && $icnt <= 31){
										$s_data_exp = explode('_', $svalue);
										if($s_data_exp[0] == 'H' || $s_data_exp[0] == 'HD' || $s_data_exp[0] == 'W' || $s_data_exp[0] == 'C'){
											foreach($s_data_exp as $sdxkey => $sdxvalue){
												if($sdxkey == 2){
													$s_data_exp[$sdxkey] = $r_shift_id;
												}
											}
											$svalue = implode('_', $s_data_exp);
											$sql_2 .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($svalue) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "'; ";
										} else {
											$sql_2 .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($shift_id) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "'; ";
										}
										//$this->db->query($sql);
									}
									$icnt ++;
								}
							}
							//$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `1` = '".$shift_id."', `2` = '".$shift_id."', `3` = '".$shift_id."', `4` = '".$shift_id."', `5` = '".$shift_id."', `6` = '".$shift_id."', `7` = '".$shift_id."', `8` = '".$shift_id."', `9` = '".$shift_id."', `10` = '".$shift_id."', `11` = '".$shift_id."', `12` = '".$shift_id."', `13` = '".$shift_id."', `14` = '".$shift_id."', `15` = '".$shift_id."', `16` = '".$shift_id."', `17` = '".$shift_id."', `18` = '".$shift_id."', `19` = '".$shift_id."', `20` = '".$shift_id."', `21` = '".$shift_id."', `22` = '".$shift_id."', `23` = '".$shift_id."', `24` = '".$shift_id."', `25` = '".$shift_id."', `26` = '".$shift_id."', `27` = '".$shift_id."', `28` = '".$shift_id."', `29` = '".$shift_id."', `30` = '".$shift_id."', `31` = '".$shift_id."' WHERE `emp_code` = '".$evalue['emp_code']."' ");
						}
					}
					$this->new_mysql($sql_2);
				}
			} else {
				$sql_3 = "";
				$d_dept = array();
				foreach ($dept_holiday_pune as $d1key => $d1value) {
					$d1value = html_entity_decode(strtolower(trim($d1value)));
					if(!in_array($d1value, $d_dept)){
						$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'pune' ");
						foreach ($emp_codes->rows as $ekey => $evalue) {
							$shift_s_datas = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$evalue['emp_code'] . "' ");
							foreach($shift_s_datas->rows as $skeys => $svalues){
								$icnt = 0;
								foreach($svalues as $skey => $svalue){
									if($icnt > 0 && $icnt <= 31){
										$s_data_exp = explode('_', $svalue);
										if($s_data_exp[0] == 'H' || $s_data_exp[0] == 'HD' || $s_data_exp[0] == 'W' || $s_data_exp[0] == 'C'){
											foreach($s_data_exp as $sdxkey => $sdxvalue){
												if($sdxkey == 2){
													$s_data_exp[$sdxkey] = $r_shift_id;
												}
											}
											$svalue = implode('_', $s_data_exp);
											$sql_3 .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($svalue) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "'; ";
										} else {
											$sql_3 .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = 'S_1' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "'; ";
										}
										//$this->db->query($sql);
									}
									$icnt ++;
								}
							}
							//$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `1` = 'S_1', `2` = 'S_1', `3` = 'S_1', `4` = 'S_1', `5` = 'S_1', `6` = 'S_1', `7` = 'S_1', `8` = 'S_1', `9` = 'S_1', `10` = 'S_1', `11` = 'S_1', `12` = 'S_1', `13` = 'S_1', `14` = 'S_1', `15` = 'S_1', `16` = 'S_1', `17` = 'S_1', `18` = 'S_1', `19` = 'S_1', `20` = 'S_1', `21` = 'S_1', `22` = 'S_1', `23` = 'S_1', `24` = 'S_1', `25` = 'S_1', `26` = 'S_1', `27` = 'S_1', `28` = 'S_1', `29` = 'S_1', `30` = 'S_1', `31` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ");
						}
					}
				}
			}
			$this->new_mysql($sql_3);
		}
		//echo 'Done';exit;

		if($this->user->getId() == 7){
			$this->log->write('in moving');
			if (isset($data['dept_holiday_moving'])) {
				if($dept_holiday_moving){
					$d_dept = array();
					$sql = "";
					foreach ($dept_holiday_moving as $d1key => $d1value) {
						foreach ($data['dept_holiday_moving'] as $dkey => $dvalue) {
							$dvalue = html_entity_decode(strtolower(trim($dvalue)));
							if(!in_array($dvalue, $d_dept)){
								$d_dept[] = $dvalue;
								$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'moving' ");
								foreach ($emp_codes->rows as $ekey => $evalue) {
									$shift_s_datas = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$evalue['emp_code'] . "' ");
									foreach($shift_s_datas->rows as $skeys => $svalues){
										$icnt = 0;
										foreach($svalues as $skey => $svalue){
											if($icnt > 0 && $icnt <= 31){
												$s_data_exp = explode('_', $svalue);
												if($s_data_exp[0] == 'H' || $s_data_exp[0] == 'HD' || $s_data_exp[0] == 'W' || $s_data_exp[0] == 'C'){
													foreach($s_data_exp as $sdxkey => $sdxvalue){
														if($sdxkey == 2){
															$s_data_exp[$sdxkey] = $r_shift_id;
														}
													}
													$svalue = implode('_', $s_data_exp);
													$sql .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($svalue) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "'; ";
												} else {
													$sql .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($shift_id) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "'; ";
												}
												//$this->db->query($sql);
											}
											$icnt ++;
										}
									}
									//$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `1` = '".$shift_id."', `2` = '".$shift_id."', `3` = '".$shift_id."', `4` = '".$shift_id."', `5` = '".$shift_id."', `6` = '".$shift_id."', `7` = '".$shift_id."', `8` = '".$shift_id."', `9` = '".$shift_id."', `10` = '".$shift_id."', `11` = '".$shift_id."', `12` = '".$shift_id."', `13` = '".$shift_id."', `14` = '".$shift_id."', `15` = '".$shift_id."', `16` = '".$shift_id."', `17` = '".$shift_id."', `18` = '".$shift_id."', `19` = '".$shift_id."', `20` = '".$shift_id."', `21` = '".$shift_id."', `22` = '".$shift_id."', `23` = '".$shift_id."', `24` = '".$shift_id."', `25` = '".$shift_id."', `26` = '".$shift_id."', `27` = '".$shift_id."', `28` = '".$shift_id."', `29` = '".$shift_id."', `30` = '".$shift_id."', `31` = '".$shift_id."' WHERE `emp_code` = '".$evalue['emp_code']."' ");
								}
							} 
						}
					}
					$this->new_mysql($sql);
					$sql_1 = ""; 
					foreach ($dept_holiday_moving as $d1key => $d1value) {
						$d1value = html_entity_decode(strtolower(trim($d1value)));
						if(!in_array($d1value, $d_dept)){
							$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'moving' ");
							foreach ($emp_codes->rows as $ekey => $evalue) {
								$shift_s_datas = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$evalue['emp_code'] . "' ");
									foreach($shift_s_datas->rows as $skeys => $svalues){
										$icnt = 0;
										foreach($svalues as $skey => $svalue){
											if($icnt > 0 && $icnt <= 31){
												$s_data_exp = explode('_', $svalue);
												if($s_data_exp[0] == 'H' || $s_data_exp[0] == 'HD' || $s_data_exp[0] == 'W' || $s_data_exp[0] == 'C'){
													foreach($s_data_exp as $sdxkey => $sdxvalue){
														if($sdxkey == 2){
															$s_data_exp[$sdxkey] = $r_shift_id;
														}
													}
													$svalue = implode('_', $s_data_exp);
													$sql_1 .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($svalue) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "'; ";
												} else {
													$sql_1 .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = 'S_1' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "'; ";
												}
												//$this->db->query($sql);
											}
											$icnt ++;
										}
									}
								//$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `1` = 'S_1', `2` = 'S_1', `3` = 'S_1', `4` = 'S_1', `5` = 'S_1', `6` = 'S_1', `7` = 'S_1', `8` = 'S_1', `9` = 'S_1', `10` = 'S_1', `11` = 'S_1', `12` = 'S_1', `13` = 'S_1', `14` = 'S_1', `15` = 'S_1', `16` = 'S_1', `17` = 'S_1', `18` = 'S_1', `19` = 'S_1', `20` = 'S_1', `21` = 'S_1', `22` = 'S_1', `23` = 'S_1', `24` = 'S_1', `25` = 'S_1', `26` = 'S_1', `27` = 'S_1', `28` = 'S_1', `29` = 'S_1', `30` = 'S_1', `31` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ");
							}
						}
					}
					$this->new_mysql($sql_1);
				} else {
					$sql_2 = "";
					foreach ($data['dept_holiday_moving'] as $dkey => $dvalue) {
						$dvalue = html_entity_decode(strtolower(trim($dvalue)));
						$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'moving' ");
						foreach ($emp_codes->rows as $ekey => $evalue) {
							$shift_s_datas = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$evalue['emp_code'] . "' ");
							foreach($shift_s_datas->rows as $skeys => $svalues){
								$icnt = 0;
								foreach($svalues as $skey => $svalue){
									if($icnt > 0 && $icnt <= 31){
										$s_data_exp = explode('_', $svalue);
										if($s_data_exp[0] == 'H' || $s_data_exp[0] == 'HD' || $s_data_exp[0] == 'W' || $s_data_exp[0] == 'C'){
											foreach($s_data_exp as $sdxkey => $sdxvalue){
												if($sdxkey == 2){
													$s_data_exp[$sdxkey] = $r_shift_id;
												}
											}
											$svalue = implode('_', $s_data_exp);
											$sql_2 .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($svalue) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "'; ";
										} else {
											$sql_2 .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($shift_id) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "'; ";
										}
										//$this->db->query($sql);
									}
									$icnt ++;
								}
							}
							//$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `1` = '".$shift_id."', `2` = '".$shift_id."', `3` = '".$shift_id."', `4` = '".$shift_id."', `5` = '".$shift_id."', `6` = '".$shift_id."', `7` = '".$shift_id."', `8` = '".$shift_id."', `9` = '".$shift_id."', `10` = '".$shift_id."', `11` = '".$shift_id."', `12` = '".$shift_id."', `13` = '".$shift_id."', `14` = '".$shift_id."', `15` = '".$shift_id."', `16` = '".$shift_id."', `17` = '".$shift_id."', `18` = '".$shift_id."', `19` = '".$shift_id."', `20` = '".$shift_id."', `21` = '".$shift_id."', `22` = '".$shift_id."', `23` = '".$shift_id."', `24` = '".$shift_id."', `25` = '".$shift_id."', `26` = '".$shift_id."', `27` = '".$shift_id."', `28` = '".$shift_id."', `29` = '".$shift_id."', `30` = '".$shift_id."', `31` = '".$shift_id."' WHERE `emp_code` = '".$evalue['emp_code']."' ");
						}
					}
					$this->new_mysql($sql_2);
				}
			} else {
				$d_dept = array();
				$sql_3 = "";
				foreach ($dept_holiday_moving as $d1key => $d1value) {
					$d1value = html_entity_decode(strtolower(trim($d1value)));
					if(!in_array($d1value, $d_dept)){
						$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'moving' ");
						foreach ($emp_codes->rows as $ekey => $evalue) {
							$shift_s_datas = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$evalue['emp_code'] . "' ");
							foreach($shift_s_datas->rows as $skeys => $svalues){
								$icnt = 0;
								foreach($svalues as $skey => $svalue){
									if($icnt > 0 && $icnt <= 31){
										$s_data_exp = explode('_', $svalue);
										if($s_data_exp[0] == 'H' || $s_data_exp[0] == 'HD' || $s_data_exp[0] == 'W' || $s_data_exp[0] == 'C'){
											foreach($s_data_exp as $sdxkey => $sdxvalue){
												if($sdxkey == 2){
													$s_data_exp[$sdxkey] = $r_shift_id;
												}
											}
											$svalue = implode('_', $s_data_exp);
											$sql_3 .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($svalue) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "'; ";
										} else {
											$sql_3 .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = 'S_1' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "'; ";
										}
										//$this->db->query($sql);
									}
									$icnt ++;
								}
							}
							//$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `1` = 'S_1', `2` = 'S_1', `3` = 'S_1', `4` = 'S_1', `5` = 'S_1', `6` = 'S_1', `7` = 'S_1', `8` = 'S_1', `9` = 'S_1', `10` = 'S_1', `11` = 'S_1', `12` = 'S_1', `13` = 'S_1', `14` = 'S_1', `15` = 'S_1', `16` = 'S_1', `17` = 'S_1', `18` = 'S_1', `19` = 'S_1', `20` = 'S_1', `21` = 'S_1', `22` = 'S_1', `23` = 'S_1', `24` = 'S_1', `25` = 'S_1', `26` = 'S_1', `27` = 'S_1', `28` = 'S_1', `29` = 'S_1', `30` = 'S_1', `31` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ");
						}
					}
				}
				$this->new_mysql($sql_3);
			}
		}


		// if(isset($data['loc_holiday']) && isset($data['dept_holiday'])){
		// 	foreach ($data['loc_holiday'] as $lkey => $lvalue) {
		// 		foreach ($data['dept_holiday'] as $dkey => $dvalue) {
		// 			$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE `shift_type` = 'F' AND `unit` = '".$lvalue."' AND `department` = '".$dvalue."' ");
		// 			foreach ($emp_codes->rows as $ekey => $evalue) {
		// 				$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `1` = '".$shift_id."', `2` = '".$shift_id."', `3` = '".$shift_id."', `4` = '".$shift_id."', `5` = '".$shift_id."', `6` = '".$shift_id."', `7` = '".$shift_id."', `8` = '".$shift_id."', `9` = '".$shift_id."', `10` = '".$shift_id."', `11` = '".$shift_id."', `12` = '".$shift_id."', `13` = '".$shift_id."', `14` = '".$shift_id."', `15` = '".$shift_id."', `16` = '".$shift_id."', `17` = '".$shift_id."', `18` = '".$shift_id."', `19` = '".$shift_id."', `20` = '".$shift_id."', `21` = '".$shift_id."', `22` = '".$shift_id."', `23` = '".$shift_id."', `24` = '".$shift_id."', `25` = '".$shift_id."', `26` = '".$shift_id."', `27` = '".$shift_id."', `28` = '".$shift_id."', `29` = '".$shift_id."', `30` = '".$shift_id."', `31` = '".$shift_id."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".date('n')."' AND `year` = '".date('Y')."' ");
		// 			}
		// 		}
		// 	}
		// } elseif (isset($data['loc_holiday'])) {
		// 	foreach ($data['loc_holiday'] as $lkey => $lvalue) {
		// 		$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE `shift_type` = 'F' AND `unit` = '".$lvalue."' ");
		// 		foreach ($emp_codes->rows as $ekey => $evalue) {
		// 			$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `1` = '".$shift_id."', `2` = '".$shift_id."', `3` = '".$shift_id."', `4` = '".$shift_id."', `5` = '".$shift_id."', `6` = '".$shift_id."', `7` = '".$shift_id."', `8` = '".$shift_id."', `9` = '".$shift_id."', `10` = '".$shift_id."', `11` = '".$shift_id."', `12` = '".$shift_id."', `13` = '".$shift_id."', `14` = '".$shift_id."', `15` = '".$shift_id."', `16` = '".$shift_id."', `17` = '".$shift_id."', `18` = '".$shift_id."', `19` = '".$shift_id."', `20` = '".$shift_id."', `21` = '".$shift_id."', `22` = '".$shift_id."', `23` = '".$shift_id."', `24` = '".$shift_id."', `25` = '".$shift_id."', `26` = '".$shift_id."', `27` = '".$shift_id."', `28` = '".$shift_id."', `29` = '".$shift_id."', `30` = '".$shift_id."', `31` = '".$shift_id."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".date('n')."' AND `year` = '".date('Y')."' ");
		// 		}
		// 	}
		// } elseif (isset($data['dept_holiday'])) {
		// 	foreach ($data['dept_holiday'] as $dkey => $dvalue) {
		// 		$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE `shift_type` = 'F' AND `department` = '".$dvalue."' ");
		// 		foreach ($emp_codes->rows as $ekey => $evalue) {
		// 			$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `1` = '".$shift_id."', `2` = '".$shift_id."', `3` = '".$shift_id."', `4` = '".$shift_id."', `5` = '".$shift_id."', `6` = '".$shift_id."', `7` = '".$shift_id."', `8` = '".$shift_id."', `9` = '".$shift_id."', `10` = '".$shift_id."', `11` = '".$shift_id."', `12` = '".$shift_id."', `13` = '".$shift_id."', `14` = '".$shift_id."', `15` = '".$shift_id."', `16` = '".$shift_id."', `17` = '".$shift_id."', `18` = '".$shift_id."', `19` = '".$shift_id."', `20` = '".$shift_id."', `21` = '".$shift_id."', `22` = '".$shift_id."', `23` = '".$shift_id."', `24` = '".$shift_id."', `25` = '".$shift_id."', `26` = '".$shift_id."', `27` = '".$shift_id."', `28` = '".$shift_id."', `29` = '".$shift_id."', `30` = '".$shift_id."', `31` = '".$shift_id."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".date('n')."' AND `year` = '".date('Y')."' ");
		// 		}
		// 	}
		// }
		//$this->log->write("UPDATE " . DB_PREFIX . "shift SET `name` = '" . $this->db->escape($data['name']) . "', `in_time` = '" . $this->db->escape($data['in_time']) . "', `out_time` = '" . $this->db->escape($data['out_time']) . "', `weekly_off_1` = '" . $this->db->escape($data['weekly_off_1']) . "', `weekly_off_2` = '" . $this->db->escape($data['weekly_off_2']) . "' WHERE shift_id = '" . (int)$shift_id . "'");
	}

	public function deleteshift($shift_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "shift WHERE shift_id = '" . (int)$shift_id . "'");
	}	

	public function getshift($shift_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift WHERE shift_id = '" . (int)$shift_id . "'");
		return $query->row;
	}

	public function getshifts($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "shift WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		$sort_data = array(
			'name',
			'shift_id'
		);	

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}					

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}				

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalshifts($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "shift WHERE 1=1 ";
		
		if (!empty($data['filter_name'])) {
			$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
		}

		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}
?>
