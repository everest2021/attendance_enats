<?php
class ModelCatalogExpensetransaction extends Model {
	public function addexpensetransaction($data) {
			// 	 echo '<pre>';
		 // print_r($data);
		 // exit;
		$this->db->query("INSERT INTO " . DB_PREFIX . "expense_transaction SET  
							emp_name = '" . $this->db->escape($data['emp_name']) . "',
							emp_code = '" . $this->db->escape($data['emp_code']) . "',
							amount = '" . $this->db->escape($data['amount']) . "', 
							date = '" . $this->db->escape(date('Y-m-d', strtotime($data['date']))) . "' 
      					");
		
    }

	public function editexpensetransaction($id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "expense_transaction SET
							emp_name = '" . $this->db->escape($data['emp_name']) . "',
							emp_code = '" . $this->db->escape($data['emp_code']) . "',
							amount = '" . $this->db->escape($data['amount']) . "', 
							date = '" . $this->db->escape(date('Y-m-d', strtotime($data['date']))) . "' 
							WHERE id = '" . (int)$id . "'
						");
	}

	public function deleteexpensetransaction($id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "expense_transaction WHERE id = '" . (int)$id . "'");
	}

	public function addemployeelist($id, $type, $page) {
		$project_query = $this->db->query("SELECT DISTINCT id FROM " . DB_PREFIX . "expense_transaction WHERE id = '" . (int)$id . "'");

		if ($project_assignment_query->num_rows) {
			$project_assignment_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "expense_transaction WHERE id = '" . (int)$project_query->row['id'] . "'");

			if ($project_assignment_query->num_rows) {
				$data = unserialize($project_assignment_query->row['employee_list']);

				$data[$type][] = $page;

				$this->db->query("UPDATE " . DB_PREFIX . "expense_transaction SET employee_list = '" . serialize($data) . "' WHERE id = '" . (int)$project_query->row['id'] . "'");
			}
		}
	}

	public function getexpensetransaction($id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "expense_transaction WHERE id = '" . (int)$id . "'");
		return $query->row;
	}

	public function getexpensetransactions($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "expense_transaction WHERE 1=1 ";

		if (isset($data['filter_id']) && !empty($data['filter_id'])) {
			$sql .= " AND `emp_code` = '" . $data['filter_id'] . "' ";
		}

		if (isset($data['filter_date_start']) && !empty($data['filter_date_start'])) {
			$sql .= " AND `date` >= '" . date('Y-m-d', strtotime($data['filter_date_start'])) . "' ";
		}

		if (isset($data['filter_date_end']) && !empty($data['filter_date_end'])) {
			$sql .= " AND `date` <= '" . date('Y-m-d', strtotime($data['filter_date_end'])) . "' ";
		}

		$sort_data = array(
			'emp_name',
			'date',
		);	

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY `date`";	
		}


		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;exit;


		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalexpensetransactions() {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "expense_transaction WHERE 1=1 ";
		
		if (isset($data['filter_id']) && !empty($data['filter_id'])) {
			$sql .= " AND `emp_code` = '" . $data['filter_id'] . "' ";
		}

		if (isset($data['filter_date_start']) && !empty($data['filter_date_start'])) {
			$sql .= " AND `date` >= '" . date('Y-m-d', strtotime($data['filter_date_start'])) . "' ";
		}

		if (isset($data['filter_date_end']) && !empty($data['filter_date_end'])) {
			$sql .= " AND `date` <= '" . date('Y-m-d', strtotime($data['filter_date_end'])) . "' ";
		}
		
		$query = $this->db->query($sql);
 		return $query->row['total'];
	}	
	
}
?>