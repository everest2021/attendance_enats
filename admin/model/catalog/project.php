<?php
class ModelCatalogProject extends Model {
	public function addproject($data) {
		
		$this->db->query("INSERT INTO `" . DB_PREFIX . "project` SET name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['description']) . "', address = '" . $this->db->escape($data['address']) . "' ,latitude='" . $this->db->escape($data['latitude']) . "', longitude = '" . $this->db->escape($data['longitude']) . "' ");
	}

	public function editproject($id, $data) {
		
		$this->db->query("UPDATE `" . DB_PREFIX . "project` SET name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['description']) . "', address = '" . $this->db->escape($data['address']) . "', latitude = '" . $this->db->escape($data['latitude']) . "' ,longitude = '" . $this->db->escape($data['longitude']) . "'  WHERE id = '" . (int)$id . "' ");
	}

	public function deleteproject($id) {
		$this->db->query("DELETE  FROM `" . DB_PREFIX . "project` WHERE id = '" . (int)$id . "'");
	// echo '<pre>';
	// print_r($id);
	// exit();
	}

	public function getproject($id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "project` WHERE id = '" . (int)$id . "'");

		return $query->row;
	
	}

	
	public function getprojects($data = array()) {

		$sql = "SELECT * FROM " . DB_PREFIX . "project WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
		}

		// echo '<pre>';
		// print_r($sql);
		// exit();

		$sort_data = array(
			'id',
			'name',
			'address',
			'latitude',
			'longitude',
			'description'

		);	
		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalprojectss() {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "project WHERE 1=1 ";
		
		if (!empty($data['filter_name'])) {
			$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
		}

		$query = $this->db->query($sql);
		return $query->row['total'];
	// echo '<pre>';
	// print_r($query);
	// exit;
	}

}
?>