<?php
class ModelCatalogDevices extends Model {
	public function adddevices($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "devices` SET device_serial_no = '" . $this->db->escape($data['device_serial_no']) . "', location = '" . $data['location'] . "', time_zone = '" . $data['time_zone'] . "', type = '" . $data['type'] . "'");
	}

	public function editdevices($id, $data) {
		$this->db->query("UPDATE `" . DB_PREFIX . "devices` SET device_serial_no = '" . $this->db->escape($data['device_serial_no']) . "',  location = '" . $data['location'] . "',  time_zone = '" . $data['time_zone'] . "', type = '" . $data['type'] . "' WHERE id = '" . (int)$id . "'");

	}

	

	public function deletedevices($id) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "devices` WHERE id = '" . (int)$id . "'");
	}

	public function getdevice($id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "devices` WHERE id = '" . $id . "'");

		return $query->row;
	}

	

	public function getdevices($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "devices WHERE 1=1 ";
        // echo '<pre>';
        // print_r("SELECT * FROM " . DB_PREFIX . "devices WHERE 1=1 ");
        // exit;
		if (!empty($data['filter_device_serial_no'])) {
			$data['filter_device_serial_no'] = html_entity_decode($data['filter_device_serial_no']);
			$sql .= " AND LOWER(device_serial_no) LIKE '%" . $this->db->escape(strtolower($data['filter_device_serial_no'])) . "%'";
		}

		$sort_data = array(
			'device_serial_no',
			'id'
		
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY device_serial_no";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	
	public function getTotaldevices() {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "devices WHERE 1=1 ";
		
		if (!empty($data['filter_device_serial_no'])) {
			$sql .= " AND LOWER(device_serial_no) LIKE '%" . $this->db->escape(strtolower($data['filter_device_serial_no'])) . "%'";
		}

		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function getActiveDevices() {
		$sql = "SELECT `device_serial_no`, `location`, `time_zone`,`type` FROM ".DB_PREFIX."devices WHERE `status` = '1'";	
		// $sql = "SELECT * FROM `oc_attendance` WHERE device_id=['$status'] ORDER BY id DESK LIMIT 1";	

		$query = $this->db->query($sql);
		return $query->rows;	
	}
	// public function getTotalUsersByGroupId($user_group_id) {
	// 	$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "user` WHERE user_group_id = '" . (int)$user_group_id . "'");

	// 	return $query->row['total'];
	// }

	// public function getTotalUsersByEmail($email) {
	// 	$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "user` WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

	// 	return $query->row['total'];
	// }	
}
?>