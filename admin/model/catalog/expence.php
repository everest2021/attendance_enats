<?php
class ModelCatalogExpence extends Model {
	public function addexpence($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "expence` SET name = '" . $this->db->escape($data['name']) . "', status = '" . (int)$data['status'] . "' ");
	}

	public function editexpence($id, $data) {
		$this->db->query("UPDATE `" . DB_PREFIX . "expence` SET name = '" . $this->db->escape($data['name']) . "',  status = '" . (int)$data['status'] . "' WHERE id = '" . (int)$id . "'");

		// if ($data['password']) {
		// 	$this->db->query("UPDATE `" . DB_PREFIX . "user` SET salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "' WHERE user_id = '" . (int)$user_id . "'");
		// }
	}

	// public function editPassword($user_id, $password) {
	// 	$this->db->query("UPDATE `" . DB_PREFIX . "user` SET salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password)))) . "', code = '' WHERE user_id = '" . (int)$user_id . "'");
	// }

	// public function editUser_pass($emp_code, $data) {
	// 	if ($data['password']) {
	// 		$this->db->query("UPDATE `" . DB_PREFIX . "employee` SET salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', is_set = '1' WHERE emp_code = '" . (int)$emp_code . "'");
	// 	}
	// }

	// public function editCode($email, $code) {
	// 	$this->db->query("UPDATE `" . DB_PREFIX . "user` SET code = '" . $this->db->escape($code) . "' WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	// }

	public function deleteexpence($id) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "expence` WHERE id = '" . (int)$id . "'");
	}

	public function getexpence($id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "expence` WHERE id = '" . (int)$id . "'");

		return $query->row;
	}

	// public function getemployee($employee_code) {
	// 	$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "employee WHERE emp_code = '" . (int)$employee_code . "'");
	// 	return $query->row;
	// }

	// public function getUserByUsername($username) {
	// 	$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "user` WHERE username = '" . $this->db->escape($username) . "'");

	// 	return $query->row;
	// }

	// public function getUserByCode($code) {
	// 	$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "user` WHERE code = '" . $this->db->escape($code) . "' AND code != ''");

	// 	return $query->row;
	// }

	public function getexpences($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "expence WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
		}

		$sort_data = array(
			'name',
			'id'
		
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalexpencess() {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "expence WHERE 1=1 ";
		
		if (!empty($data['filter_name'])) {
			$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
		}

		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	// public function getTotalUsersByGroupId($user_group_id) {
	// 	$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "user` WHERE user_group_id = '" . (int)$user_group_id . "'");

	// 	return $query->row['total'];
	// }

	// public function getTotalUsersByEmail($email) {
	// 	$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "user` WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

	// 	return $query->row['total'];
	// }	
}
?>