<?php
class ModelCatalogEmployee extends Model {
	public function addemployee($data) {
		$data['dept_head_list'] = array();
		$full_name = $data['emp_name'];
		$full_name = preg_replace('/\s+/', ' ', $full_name);
		// echo $full_name ;
		// exit;
		$this->db->query("INSERT INTO " . DB_PREFIX . "employee SET 
							emp_name = '" . $this->db->escape($full_name) . "', 
							card = '" . $this->db->escape($data['card_no']) . "', 
							device_emp_code = '" . $this->db->escape($data['emp_code']) . "', 
							
							device_serial_no = '" . $this->db->escape($data['device_serial_no']) . "', 
							
							
							status = '" . $this->db->escape($data['status']) . "'");
		
	}

	public function editemployee($employee_id, $data) {
		$data['dept_head_list'] = array();
		$full_name = $data['emp_name'];
		$full_name = preg_replace('/\s+/', ' ', $full_name);
		
		$this->db->query("UPDATE " . DB_PREFIX . "employee SET 
							emp_name = '" . $this->db->escape($full_name) . "',
							card = '" . $this->db->escape($data['card_no']) . "', 
							device_emp_code = '" . $this->db->escape($data['emp_code']) . "', 
							
							device_serial_no = '" . $this->db->escape($data['device_serial_no']) . "',
							
							status = '" . $this->db->escape($data['status']) . "'
							
							WHERE emp_id = '" . (int)$employee_id . "'");
		
		 
	}

	public function deleteemployee($employee_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "employee WHERE emp_id = '" . (int)$employee_id . "'");
	}	

	public function getemployee($employee_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "employee WHERE emp_id = '" . (int)$employee_id . "'");
		return $query->row;
	}

	public function getcatname($category_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "categories WHERE id = '" . (int)$category_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	// public function getdeptname($department_id) {
	// 	$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "department WHERE id = '" . (int)$department_id . "'");
	// 	if($query->num_rows > 0){
	// 		return $query->row['name'];
	// 	} else {
	// 		return '';
	// 	}
	// }

	

	

	public function getlocname($location_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "location WHERE id = '" . (int)$location_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	public function getemployees($data = array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "employee` WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			//$data['filter_name'] = preg_replace('/\s+/', ' ', $data['filter_name']);
			$sql .= " AND LOWER(`name`) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		if (isset($data['filter_emp_code']) && !empty($data['filter_emp_code'])) {
			$filter_emp_code = $data['filter_emp_code'];
			$sql .= " AND `device_emp_code` = '" . $this->db->escape($filter_emp_code) . "' ";
		}

		
			

		

		

		if (isset($data['filter_status']) && !empty($data['filter_status'])) {
			if($data['filter_status'] == 1){
				$sql .= " AND `status` = '1' ";
			} else {
				$sql .= " AND `status` = '0' ";
			}
		}

		$sort_data = array(
			'name',
			'device_emp_code',
			'department',
			'unit',
			'shift_id',
			'status',
		);	

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY CAST(device_emp_code AS UNSIGNED)";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " DESC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}					

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	
		//$this->log->write($sql);			
		//echo $sql;exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalemployees($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "employee WHERE 1=1 ";
		
		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			//$data['filter_name'] = preg_replace('/\s+/', ' ', $data['filter_name']);
			$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
		}

		if (isset($data['filter_code']) && !empty($data['filter_code'])) {
			$sql .= " AND `device_emp_code` = '" . $this->db->escape($data['filter_code']) . "' ";
		}

		if (isset($data['filter_emp_code']) && !empty($data['filter_emp_code'])) {
			$filter_emp_code = $data['filter_emp_code'];
			$sql .= " AND `device_emp_code` = '" . $this->db->escape($filter_emp_code) . "' ";
		}

		// if (isset($data['filter_department']) && !empty($data['filter_department'])) {
		// 	$sql .= " AND LOWER(department) = '" . $this->db->escape(strtolower($data['filter_department'])) . "' ";
		// }

		// if (isset($data['filter_departments']) && !empty($data['filter_departments'])) {
		// 	$sql .= " AND LOWER(`department`) IN (" . strtolower($data['filter_departments']) . ") ";
		// }

		if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
			$sql .= " AND LOWER(unit) = '" . $this->db->escape(strtolower($data['filter_unit'])) . "' ";
		}

		if (isset($data['filter_shift_type'])) {
			$sql .= " AND shift_type = 'R' ";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		// if (isset($data['filter_department_id']) && !empty($data['filter_department_id'])) {
		// 	$sql .= " AND `department_id` = '" . $this->db->escape($data['filter_department_id']) . "' ";
		// }

		if (isset($data['filter_unit_id']) && !empty($data['filter_unit_id'])) {
			$sql .= " AND `unit_id` = '" . $this->db->escape($data['filter_unit_id']) . "' ";
		}

		if (isset($data['filter_shift_id']) && !empty($data['filter_shift_id'])) {
			$sql .= " AND `shift_id` = '" . $this->db->escape($data['filter_shift_id']) . "' ";
		}

		if (isset($data['filter_status']) && !empty($data['filter_status'])) {
			if($data['filter_status'] == 1){
				$sql .= " AND `status` = '1' ";
			} else {
				$sql .= " AND `status` = '0' ";
			}
		}

		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function getActiveEmployee() {
		$sql = "SELECT `emp_code`, `name`, `doj`, `grade`, `group` FROM ".DB_PREFIX."employee WHERE `status` = '1'";	
		$query = $this->db->query($sql);
		return $query->rows;	
	}

	public function getUnit() {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "unit ");
		return $query->rows;
	}

	// public function getDepartment() {
	// 	$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "department ");
	// 	return $query->rows;
	// }

	public function getshift() {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift ");
		return $query->rows;
	}
}
?>