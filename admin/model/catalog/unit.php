<?php
class ModelCatalogUnit extends Model {
	public function addUnit($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "unit` SET 
							`unit` = '" . $this->db->escape($data['unit']) . "', 
							`code` = '" . $this->db->escape($data['code']) . "', 
							`status` = '" . $data['status'] . "'
						");

		$unit_id = $this->db->getLastId(); 
	}

	public function editUnit($unit_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "unit SET 
							`unit` = '" . $this->db->escape($data['unit']) . "', 
							`code` = '" . $this->db->escape($data['code']) . "', 
							`status` = '" . $data['status'] . "' 
							WHERE unit_id = '" . (int)$unit_id . "'");
	}

	public function deleteUnit($unit_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "unit WHERE unit_id = '" . (int)$unit_id . "'");
	}	

	public function getUnit($unit_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "unit WHERE unit_id = '" . (int)$unit_id . "'");

		return $query->row;
	}

	public function getUnits($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "unit WHERE 1=1 ";

		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND unit_id = '" . $data['filter_name_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(unit) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		
		$sort_data = array(
			'unit'
		);		

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY unit";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}		

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalUnits() {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "unit";
		
		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND unit_id = '" . $data['filter_name_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(unit) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}	
}
?>