<?php
class ModelCatalogGrade extends Model {
	public function addGrade($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "grade` SET 
							`g_name` = '" . $this->db->escape($data['g_name']) . "', 
							`basic` = '" . $data['basic'] . "',
							`da` = '" . $data['da'] . "',
							`hra` = '" . $data['hra'] . "',
							`ta` = '" . $data['ta'] . "',
							`ea` = '" . $data['ea'] . "',
							`medical_allowance` = '" . $data['medical_allowance'] . "',
							`other_allowance` = '" . $data['other_allowance'] . "',
							`earning_total` = '" . $data['earning_total'] . "',
							`pf` = '" . $data['pf'] . "',
							`esi` = '" . $data['esi'] . "',
							`pt` = '" . $data['pt'] . "',
							`income_tax` = '" . $data['income_tax'] . "',
							`deduction_total` = '" . $data['deduction_total'] . "',
							`net_total` = '" . $data['net_total'] . "',
							`no_days` = '" . $data['no_days'] . "',
							`status` = '" . $data['status'] . "'
						");

		$grade_id = $this->db->getLastId(); 
	}

	public function editGrade($grade_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "grade SET 
							`g_name` = '" . $this->db->escape($data['g_name']) . "', 
							`basic` = '" . $data['basic'] . "',
							`da` = '" . $data['da'] . "',
							`hra` = '" . $data['hra'] . "',
							`ta` = '" . $data['ta'] . "',
							`ea` = '" . $data['ea'] . "',
							`medical_allowance` = '" . $data['medical_allowance'] . "',
							`other_allowance` = '" . $data['other_allowance'] . "',
							`earning_total` = '" . $data['earning_total'] . "',
							`pf` = '" . $data['pf'] . "',
							`esi` = '" . $data['esi'] . "',
							`pt` = '" . $data['pt'] . "',
							`income_tax` = '" . $data['income_tax'] . "',
							`deduction_total` = '" . $data['deduction_total'] . "',
							`net_total` = '" . $data['net_total'] . "',
							`no_days` = '" . $data['no_days'] . "',
							`status` = '" . $data['status'] . "' 
							WHERE grade_id = '" . (int)$grade_id . "'");
	}

	public function deleteGrade($grade_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "grade WHERE grade_id = '" . (int)$grade_id . "'");
	}	

	public function getGrade($grade_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "grade WHERE grade_id = '" . (int)$grade_id . "'");

		return $query->row;
	}

	public function getGrades($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "grade WHERE 1=1 ";

		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND grade_id = '" . $data['filter_name_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(g_name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		
		$sort_data = array(
			'g_name'
		);		

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY g_name";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}		

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalGrades() {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "grade";
		
		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND grade_id = '" . $data['filter_name_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(g_name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}	
}
?>