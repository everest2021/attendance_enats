<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">

        <b>Month</b>
        <select name="filter_month" id="filter_month">
          <?php foreach($months as $key => $value) { ?>
            <?php if ($filter_month == $key) { ?>
              <option value="<?php echo $key ?>" selected="selected"><?php echo $value; ?></option>
            <?php } else { ?>
              <option value="<?php echo $key ?>"><?php echo $value; ?></option>
            <?php } ?>
          <?php } ?>
        </select>
        <b>Year</b>
        <select name="filter_year" id="filter_year">
          <?php for($i=2015; $i<=2020; $i++) { ?>
            <?php if($filter_year == $i) { ?>
              <option value="<?php echo $i ?>" selected="selected"><?php echo $i; ?></option>
            <?php } else { ?>
              <option value="<?php echo $i ?>"><?php echo $i; ?></option>
            <?php } ?>
          <?php } ?>
        </select>
        <a style="padding: 0px 16px;" id="filter" onclick="filter();" class="button"><?php echo "Process"; ?></a>
      </div>
    </div>
    <div class="content">
    </div>
  </div>
</div>
<script type="text/javascript"><!--
$('#tabs a').tabs();
$('.date').datepicker({dateFormat: 'dd-mm-yy'});
function filter() {
  var r = confirm("Please Confirm!");
  if(r == true){
    url = 'index.php?route=transaction/proc_next_month/process&token=<?php echo $token; ?>';
    
    var filter_month = $('select[name=\'filter_month\']').attr('value');
    
    if (filter_month) {
      url += '&filter_month=' + encodeURIComponent(filter_month);
    }

    var filter_year = $('select[name=\'filter_year\']').attr('value');
    
    if (filter_year) {
      url += '&filter_year=' + encodeURIComponent(filter_year);
    }

    location = url;
    return false;
  } else {
    return false;
  }
}
//--></script>
<?php echo $footer; ?>
