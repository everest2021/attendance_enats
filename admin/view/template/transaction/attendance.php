<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
    </div>
    <div class="content sales-report">
      <table class="form">
        <tr>
          <td style="width:13%;"><?php echo "Date"; ?>
            <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="" readonly="readonly" size="12" /></td>
          <td style="width:13%">Location
            <select name="unit">
              <?php foreach($unit_data as $key => $ud) { ?>
                <?php if($key === $unit) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>

          <td style="width:13%">Department
            <select name="department">
              <?php foreach($department_data as $key => $dd) { ?>
                <?php if($key === $department) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $dd; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $dd; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>

          <td style="width:13%">Group
            <select name="group">
              <?php foreach($group_data as $key => $gd) { ?>
                <?php if($key === $group) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $gd; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $gd; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>

          <td style="width:13%">Status
            <select name="status">
              <?php foreach($statuses as $skey => $svalue) { ?>
                <?php if($skey == $status) { ?>
                  <option value='<?php echo $skey; ?>' selected="selected"><?php echo $svalue; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $skey; ?>'><?php echo $svalue; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          
          <td style="text-align: right;">
            <a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
            <a style="padding: 13px 25px;" href="<?php echo $generate; ?>" id="filter_refresh" class="button"><?php echo 'Refresh'; ?></a>
          </td>
        </tr>
      </table>
      <table class="list">
        <thead>
          <tr>
            <td class="center"><?php echo 'Sr.No'; ?></td>
            <td class="center"><?php echo 'Emp Code'; ?></td>
            <td class="center"><?php echo 'Name'; ?></td>
            <td class="center" colspan='2'><?php echo 'Shift Assigned'; ?></td>
            <td class="center" colspan='4'><?php echo 'Shift Attended'; ?></td>
            <?php if($status == 0 || $status == 1) { ?>
              <td class="center"><?php echo 'Working Hours'; ?></td>
              <td class="center"><?php echo 'Late time'; ?></td>
              <td class="center"><?php echo 'Early time'; ?></td>
            <?php } ?>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">In</td>
            <td class="center">Out</td>
            <td class="center" style="width:8%;">In Date</td>
            <td class="center">In Time</td>
            <td class="center" style="width:8%;">Out Date</td>
            <td class="center">Out TIme</td>
            <?php if($status == 0 || $status == 1) { ?>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            <?php } ?>
          </tr>
        </thead>
        <tbody>
          <?php if($results) { ?>
            <?php $i = 1; ?>
            <?php foreach($results as $result) { ?>
              <tr>
                <td>
                  <?php echo $i; ?>
                </td>
                <td>
                  <?php echo $result['emp_id']; ?>
                </td>
                <td>
                  <?php echo $result['emp_name']; ?>
                </td>
                <td>
                  <?php if($result['leave_status'] == 1) { ?>
                    <?php echo $result['firsthalf_status']; ?>
                  <?php } elseif($result['shift_intime'] != '00:00:00') { ?>
                    <?php echo $result['shift_intime']; ?>
                  <?php } elseif($result['weekly_off'] != '0') { ?>
                    <?php echo 'Weekly Off'; ?>
                  <?php } elseif($result['holiday_id'] != '0') { ?>
                    <?php echo 'Holiday'; ?>
                  <?php } elseif($result['halfday_status'] != '0') { ?>
                    <?php echo 'Half Day'; ?>
                  <?php } elseif($result['compli_status'] != '0') { ?>
                    <?php echo 'Comp Off'; ?>
                  <?php } else { ?>
                    <?php echo $result['shift_intime']; ?>
                  <?php } ?>
                </td>
                <td>
                  <?php if($result['leave_status'] == 1) { ?>
                    <?php echo $result['secondhalf_status']; ?>
                  <?php } elseif($result['shift_outtime'] != '00:00:00') { ?>
                    <?php echo $result['shift_outtime']; ?>
                  <?php } elseif($result['weekly_off'] != '0') { ?>
                    <?php echo 'Weekly Off'; ?>
                  <?php } elseif($result['holiday_id'] != '0') { ?>
                    <?php echo 'Holiday'; ?>
                  <?php } elseif($result['halfday_status'] != '0') { ?>
                    <?php echo 'Half Day'; ?>
                  <?php } elseif($result['compli_status'] != '0') { ?>
                    <?php echo 'Comp Off'; ?>
                  <?php } else { ?>
                    <?php echo $result['shift_outtime']; ?>
                  <?php } ?>
                </td>
                
                <?php if($status == 0 || $status == 1) { ?>
                  <td>
                    <?php echo $result['date']; ?>
                  </td>
                  <td>
                    <?php echo $result['act_intime']; ?>
                  </td>
                  <td>
                    <?php echo $result['date_out']; ?>
                  </td>
                  <td>
                    <?php echo $result['act_outtime']; ?>
                  </td>
                <?php } else { ?>
                  <td>
                    <?php echo $result['date']; ?>
                  </td>
                  <td>
                    <?php echo $result['act_intime']; ?>
                  </td>
                  <td>
                    <?php echo $result['date_out']; ?>
                  </td>
                  <td>
                    <?php echo $result['act_outtime']; ?>
                  </td>
                <?php } ?>

                <?php if($status == 0 || $status == 1) { ?>
                  <td>
                    <?php if($result['working_time'] == '08:00:00') { ?>
                      <span style="background-color:#13F113;">
                        <?php echo $result['working_time']; ?>
                      </span>
                    <?php } else { ?>
                        <?php echo $result['working_time']; ?>
                    <?php } ?> 
                  </td>
                  <td>
                    <?php if($result['late_time'] != '00:00:00') { ?>
                      <span style="background-color:red;">
                        <?php echo $result['late_time']; ?>
                      </span>
                    <?php } else { ?>
                      <?php echo $result['late_time']; ?>
                    <?php } ?>
                  </td>
                  <td>
                    <?php if($result['early_time'] != '00:00:00') { ?>
                      <span style="background-color:red;">
                        <?php echo $result['early_time']; ?>
                      </span>
                    <?php } else { ?>
                      <?php echo $result['early_time']; ?>
                    <?php } ?>
                  </td>
                <?php } ?>
              </tr>
              <?php $i++; ?>
            <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan="12"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=report/attendance&token=<?php echo $token; ?>';
  
  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var unit = $('select[name=\'unit\']').attr('value');
  
  if (unit) {
    url += '&unit=' + encodeURIComponent(unit);
  }

  var department = $('select[name=\'department\']').attr('value');
  
  if (department) {
    url += '&department=' + encodeURIComponent(department);
  }

  var group = $('select[name=\'group\']').attr('value');
  
  if (group) {
    url += '&group=' + encodeURIComponent(group);
  }

  var status = $('select[name=\'status\']').attr('value');
  
  if (status) {
    url += '&status=' + encodeURIComponent(status);
  }  
  
  location = url;
  return false;
}

//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
  $('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  
});
//--></script>
<?php echo $footer; ?>