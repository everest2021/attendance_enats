<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo 'Self PX Record Entry'; ?></h1>
      <div class="buttons">
        <?php if($filter_name_id != '') { ?>
          <a href="<?php echo $insert; ?>" class="button" style=""><?php echo $button_insert; ?></a>
        <?php } ?>
        <a onclick="$('#form').submit();" class="button" style=""><?php echo 'Delete'; ?></a>
      </div>
    </div>
    <div style="margin-left: 10%;">
      <div style="text-align: left;" class="buttons">
        <a onclick="filter_px_record();" class="button" style="line-height: 40px;margin: 0 0 0 10px;background-color: #6d6d6d;font-weight: bold;color: #FFFFFF;cursor: pointer;padding: 0 36px;display: inline-block;"><?php echo 'PX Records'; ?></a>
        <a onclick="filter_tour_record();" class="button"  style="line-height: 40px;margin: 0 0 0 10px;background-color: #6d6d6d;font-weight: bold;color: #FFFFFF;cursor: pointer;padding: 0 36px;display: inline-block;"><?php echo 'TOUR Records'; ?></a>
      </div>
      <div style="text-align: left;">
        <span style="font-size: 16px;">Name</span> : <b style="font-size: 16px;"><?php echo $filter_name; ?></b> &nbsp;&nbsp; | &nbsp;&nbsp;
        <span style="font-size: 16px;">Id</span> : <b style="font-size: 16px;"><?php echo $filter_name_id; ?></b> &nbsp;&nbsp; | &nbsp;&nbsp;
        <span style="font-size: 16px;">Department</span> : <b style="font-size: 16px;"><?php echo $filter_dept_name; ?></b> &nbsp;&nbsp; | &nbsp;&nbsp;
        <span style="font-size: 16px;">Unit</span> : <b style="font-size: 16px;"><?php echo $filter_unit_name; ?></b>
      </div>
    </div>
    <div class="content">
      <?php if($filter_type == 1){ ?>
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left" style="width:80px;">
                <?php echo 'DOI'; ?>
              </td>
              <td class="left" style="width: 70px;">
                <?php echo 'Date'; ?>
              </td>
              <td>
                <a><?php echo "In Time"; ?></a>
              </td>
              <td>
                <a><?php echo "Out Time"; ?></a>
              </td>
              <td>
                <a><?php echo "Modification"; ?></a>
              </td>
              <td>
                <a><?php echo "Reason"; ?></a>
              </td>
              <td>
                <a><?php echo "Type"; ?></a>
              </td>
              <td>
                <a><?php echo "PX Record Type"; ?></a>
              </td>
              <td>
                <a><?php echo "Approval Status"; ?></a>
              </td>
              <td style="display: none;">
                <a><?php echo "Reject Reason"; ?></a>
              </td>
              <td>
                <a><?php echo "Process"; ?></a>
              </td>
              <td class="right"><?php echo 'Action'; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr class="filter">
              <td>&nbsp;</td>
              <td>
                <input type="text" id="filter_date" name="filter_date" value="<?php echo $filter_date; ?>" class="date"  style="width:70px;" />
                <input type="text" id="filter_date_to" name="filter_date_to" value="<?php echo $filter_date_to; ?>" class="date"  style="width:70px;" />
              </td>
              <td>
                <input type="text" id="filter_leave_from" name="filter_leave_from" value="<?php echo $filter_leave_from; ?>" class="date"  style="width:70px;" />
                <input type="text" id="filter_leave_to" name="filter_leave_to" value="<?php echo $filter_leave_to; ?>" class="date"  style="width:70px;" />
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>
                <select name="filter_type">
                  <?php foreach($types as $key => $ud) { ?>
                    <?php if($key == $filter_type) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td>
                <select name="filter_px_record_type">
                  <?php foreach($px_record_types as $key => $ud) { ?>
                    <?php if($key == $filter_px_record_type) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td>
                <select name="filter_approval_1">
                  <?php foreach($approves as $key => $ud) { ?>
                    <?php if($key == $filter_approval_1) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td style="display: none;">&nbsp;</td>
              <td>
                <select name="filter_proc">
                  <?php foreach($process as $key => $ud) { ?>
                    <?php if($key == $filter_proc) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td align="right">
                <a onclick="filter();" class="button"><?php echo $button_filter; ?></a>
              </td>
            </tr>
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
              <?php if ($manual_list) { ?>
              <?php foreach ($manual_list as $employee) { ?>
              <tr>
                <td style="text-align: center;"><?php if ($employee['selected']) { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $employee['id']; ?>" checked="checked" />
                  <?php } else { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $employee['id']; ?>" />
                  <?php } ?></td>
                <td class="left"><?php echo $employee['dot']; ?></td>
                <td class="left"><?php echo $employee['date']; ?></td>
                <td class="left"><?php echo $employee['in_time']; ?></td>
                <td class="left"><?php echo $employee['out_time']; ?></td>
                <td class="left"><?php echo $employee['modification']; ?></td>
                <td class="left"><?php echo $employee['punch_reason']; ?></td>
                <td class="left"><?php echo $employee['type']; ?></td>
                <td class="left"><?php echo $employee['px_record_type']; ?></td>
                <td class="left"><?php echo $employee['approval_1']; ?></td>
                <td class="left" style="display: none;"><?php echo $employee['reject_reason']; ?></td>
                <td class="left"><?php echo $employee['proc_stat']; ?></td>
                <td class="right"><?php foreach ($employee['action'] as $action) { ?>
                  <?php if($action['href'] == '') { ?>
                    <p style="cursor:default;"><?php echo $action['text']; ?></p>
                  <?php } else { ?>
                    <?php if($action['text'] == 'Edit'){ ?>
                      [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                    <?php } else { ?>
                      [ <a onclick="return confirm('Are you sure?')" href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                    <?php } ?>
                  <?php } ?>
                  <?php } ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="center" colspan="13"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </form>
          </tbody>
        </table>
        <div class="pagination"><?php echo $pagination; ?></div>
      <?php } else { ?>
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left" style="width:70px;">
                <?php echo 'DOI'; ?>
              </td>
              <td style="width: 70px;">
                <a><?php echo "Date From"; ?></a>
              </td>
              <td style="width: 70px;">
                <a><?php echo "Date To"; ?></a>
              </td>
              <td>
                <a><?php echo "Reason"; ?></a>
              </td>
              <td>
                <a><?php echo "Type"; ?></a>
              </td>
              <td style="width:70px;">
                <a><?php echo "Approval Status"; ?></a>
              </td>
              <td style="display: none;">
                <a><?php echo "Reject Reason"; ?></a>
              </td>
              <td>
                <a><?php echo "Process"; ?></a>
              </td>
              <td class="right"><?php echo 'Action'; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr class="filter">
              <td>&nbsp;</td>
              <td>
                <input type="text" id="filter_date" name="filter_date" value="<?php echo $filter_date; ?>" class="date"  style="width:70px;" />
                <input type="text" id="filter_date_to" name="filter_date_to" value="<?php echo $filter_date_to; ?>" class="date"  style="width:70px;" />
              </td>
              <td>
                <input type="text" id="filter_leave_from" name="filter_leave_from" value="<?php echo $filter_leave_from; ?>" class="date"  style="width:70px;" />
              </td>
              <td>
                <input type="text" id="filter_leave_to" name="filter_leave_to" value="<?php echo $filter_leave_to; ?>" class="date"  style="width:70px;" />
              </td>
              <td>&nbsp;</td>
              <td>
                <select name="filter_type">
                  <?php foreach($types as $key => $ud) { ?>
                    <?php if($key == $filter_type) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td>
                <select name="filter_approval_1">
                  <?php foreach($approves as $key => $ud) { ?>
                    <?php if($key == $filter_approval_1) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td style="display: none;">&nbsp;</td>
              <td>
                <select name="filter_proc">
                  <?php foreach($process as $key => $ud) { ?>
                    <?php if($key == $filter_proc) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td align="right">
                <a onclick="filter();" class="button"><?php echo $button_filter; ?></a>
              </td>
            </tr>
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
              <?php if ($manual_list) { ?>
              <?php foreach ($manual_list as $employee) { ?>
              <tr>
                <td style="text-align: center;"><?php if ($employee['selected']) { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $employee['batch_id']; ?>" checked="checked" />
                  <?php } else { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $employee['batch_id']; ?>" />
                  <?php } ?></td>
                <td class="left"><?php echo $employee['dot']; ?></td>
                <td class="left"><?php echo $employee['date_from']; ?></td>
                <td class="left"><?php echo $employee['date_to']; ?></td>
                <td class="left"><?php echo $employee['punch_reason']; ?></td>
                <td class="left"><?php echo $employee['type']; ?></td>
                <td class="left"><?php echo $employee['approval_1']; ?></td>
                <td class="left" style="display: none;"><?php echo $employee['reject_reason']; ?></td>
                <td class="left"><?php echo $employee['proc_stat']; ?></td>
                <td class="right"><?php foreach ($employee['action'] as $action) { ?>
                  <?php if($action['href'] == '') { ?>
                    <p style="cursor:default;"><?php echo $action['text']; ?></p>
                  <?php } else { ?>
                    <?php if($action['text'] == 'Edit'){ ?>
                      [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                    <?php } else { ?>
                      [ <a onclick="return confirm('Are you sure?')" href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                    <?php } ?>
                  <?php } ?>
                  <?php } ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="center" colspan="11"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </form>
          </tbody>
        </table>
        <div class="pagination"><?php echo $pagination; ?></div>
      <?php } ?>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=transaction/manualpunch_ess&token=<?php echo $token; ?>';
  
  var filter_date = $('input[name=\'filter_date\']').attr('value');
  if (filter_date) {
    url += '&filter_date=' + encodeURIComponent(filter_date);
  }

  var filter_date_to = $('input[name=\'filter_date_to\']').attr('value');
  if (filter_date_to) {
    url += '&filter_date_to=' + encodeURIComponent(filter_date_to);
  }

  var filter_leave_from = $('input[name=\'filter_leave_from\']').attr('value');
  if (filter_leave_from) {
    url += '&filter_leave_from=' + encodeURIComponent(filter_leave_from);
  }

  var filter_leave_to = $('input[name=\'filter_leave_to\']').attr('value');
  if (filter_leave_to) {
    url += '&filter_leave_to=' + encodeURIComponent(filter_leave_to);
  }

  var filter_approval_1 = $('select[name=\'filter_approval_1\']').attr('value');
  if (filter_approval_1) {
    url += '&filter_approval_1=' + encodeURIComponent(filter_approval_1);
  }

  var filter_proc = $('select[name=\'filter_proc\']').attr('value');
  if (filter_proc) {
    url += '&filter_proc=' + encodeURIComponent(filter_proc);
  }

  var filter_px_record_type = $('select[name=\'filter_px_record_type\']').attr('value');
  if (filter_px_record_type) {
    url += '&filter_px_record_type=' + encodeURIComponent(filter_px_record_type);
  }

  var filter_type = $('select[name=\'filter_type\']').attr('value');
  if (filter_type) {
    url += '&filter_type=' + encodeURIComponent(filter_type);
  }

  location = url;
  return false;
}

function filter_px_record() {
  url = 'index.php?route=transaction/manualpunch_ess&token=<?php echo $token; ?>';
  
  var filter_date = $('input[name=\'filter_date\']').attr('value');
  if (filter_date) {
    url += '&filter_date=' + encodeURIComponent(filter_date);
  }

  var filter_date_to = $('input[name=\'filter_date_to\']').attr('value');
  if (filter_date_to) {
    url += '&filter_date_to=' + encodeURIComponent(filter_date_to);
  }

  var filter_leave_from = $('input[name=\'filter_leave_from\']').attr('value');
  if (filter_leave_from) {
    url += '&filter_leave_from=' + encodeURIComponent(filter_leave_from);
  }

  var filter_leave_to = $('input[name=\'filter_leave_to\']').attr('value');
  if (filter_leave_to) {
    url += '&filter_leave_to=' + encodeURIComponent(filter_leave_to);
  }

  var filter_approval_1 = $('select[name=\'filter_approval_1\']').attr('value');
  if (filter_approval_1) {
    url += '&filter_approval_1=' + encodeURIComponent(filter_approval_1);
  }

  var filter_proc = $('select[name=\'filter_proc\']').attr('value');
  if (filter_proc) {
    url += '&filter_proc=' + encodeURIComponent(filter_proc);
  }

  var filter_px_record_type = $('select[name=\'filter_px_record_type\']').attr('value');
  if (filter_px_record_type) {
    url += '&filter_px_record_type=' + encodeURIComponent(filter_px_record_type);
  }

  url += '&filter_type=1';

  location = url;
  return false;
}

function filter_tour_record() {
  url = 'index.php?route=transaction/manualpunch_ess&token=<?php echo $token; ?>';
  
  var filter_date = $('input[name=\'filter_date\']').attr('value');
  if (filter_date) {
    url += '&filter_date=' + encodeURIComponent(filter_date);
  }

  var filter_date_to = $('input[name=\'filter_date_to\']').attr('value');
  if (filter_date_to) {
    url += '&filter_date_to=' + encodeURIComponent(filter_date_to);
  }

  var filter_leave_from = $('input[name=\'filter_leave_from\']').attr('value');
  if (filter_leave_from) {
    url += '&filter_leave_from=' + encodeURIComponent(filter_leave_from);
  }

  var filter_leave_to = $('input[name=\'filter_leave_to\']').attr('value');
  if (filter_leave_to) {
    url += '&filter_leave_to=' + encodeURIComponent(filter_leave_to);
  }

  var filter_approval_1 = $('select[name=\'filter_approval_1\']').attr('value');
  if (filter_approval_1) {
    url += '&filter_approval_1=' + encodeURIComponent(filter_approval_1);
  }

  var filter_proc = $('select[name=\'filter_proc\']').attr('value');
  if (filter_proc) {
    url += '&filter_proc=' + encodeURIComponent(filter_proc);
  }

  var filter_px_record_type = $('select[name=\'filter_px_record_type\']').attr('value');
  if (filter_px_record_type) {
    url += '&filter_px_record_type=' + encodeURIComponent(filter_px_record_type);
  }

  url += '&filter_type=2';

  location = url;
  return false;
}
//--></script>
<script type="text/javascript"><!--
$('#filter_name, #filter_date, #filter_date_to').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});

$.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
    var self = this, currentCategory = '';
    $.each(items, function(index, item) {
      if (item.category != currentCategory) {
        //ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
        currentCategory = item.category;
      }
      self._renderItem(ul, item);
    });
  }
});
//--></script> 
<script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=transaction/leave_ess_dept/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.employee_id,
            emp_code: item.emp_code
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    $('input[name=\'filter_name_id\']').val(ui.item.emp_code);
            
    return false;
  },
  focus: function(event, ui) {
        return false;
    }
});
//--></script>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--
$('.date').datepicker({
    dateFormat: 'yy-mm-dd'
});
//--></script>
<?php echo $footer; ?>