<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
	<?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="warning" id="close_warn" style="display:none;"><?php echo 'Month already Closed'; ?></div>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
	<div class="heading">
	  <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
	  <div class="buttons">
		<a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
		<a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a>
		<?php if($abnormal_list != ''){ ?>
		  <a href="<?php echo $abnormal_list; ?>">Back to Abnormal List</a>
		<?php } elseif($request_list != ''){ ?>
		  <a href="<?php echo $request_list; ?>">Back to Request List</a>
		<?php } ?>
	  </div>
	</div>
	<div class="content">
	  <div id="tabs" class="htabs"><a href="#tab-general"><?php echo $tab_general; ?></a></div>
	  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
		<div id="tab-general">
		  <table class="form">
			<tr>
			  <td><span class="required">*</span> <?php echo 'Date'; ?></td>
			  <td><input readonly="readony" style="background: transparent;cursor:default;" type="text" id="dot" name="dot" value="<?php echo $dot; ?>" size="10" class="date"/>
				<?php if ($error_dot) { ?>
				<span class="error"><?php echo $error_dot; ?></span>
				<?php } ?>
			  </td>
			</tr>
			<tr>
			  <td><span class="required">*</span> <?php echo 'Employee Name'; ?></td>
			  <td>
				<input type="text" name="e_name" id="e_name" value="<?php echo $e_name; ?>" size="100" />
				<input type="hidden" name="e_name_id" id="e_name_id" value="<?php echo $e_name_id; ?>" size="100" />
				<input type="hidden" name="insert" id="insert" value="<?php echo $insert; ?>" />
				<input type="hidden" name="transaction_id" id="transaction_id" value="<?php echo $transaction_id; ?>" />
				<?php if ($error_employee) { ?>
				<span class="error"><?php echo $error_employee; ?></span>
				<?php } ?></td>
			</tr>
			<tr>
			  <td><span class="required">*</span> <?php echo 'Emp Code'; ?></td>
			  <td>
				<input type="text" id="emp_code" name="emp_code" value="<?php echo $emp_code; ?>" size="10" />
			  </td>
			</tr>

			<tr>
			  <td><span class="required">*</span><?php echo 'Project Name'; ?></td>
			  <td>
				<select name="project_id" id="project_id">
				  <option value="0" selected="selected">Please Select</option>
				  <?php foreach($project_datas as $pkey => $pvalue) { ?>
					<?php if($pvalue['project_id'] == $project_id){ ?>
					  <option value="<?php echo $pvalue['project_id']; ?>" selected="selected"><?php echo $pvalue['project_name']; ?></option>
					<?php } else { ?>
					  <option value="<?php echo $pvalue['project_id']; ?>"><?php echo $pvalue['project_name']; ?></option>
					<?php } ?>
				  <?php } ?>
				</select>
				<input type="hidden" name="hidden_project_id" id="hidden_project_id" value="<?php echo $project_id; ?>" />
			  </td>
			</tr>
			<tr style="display:none;">
			  <td><span class="required">*</span><?php echo 'Shift'; ?></td>
			  <td>
				<select disabled="disabled" style="cursor:default;background: transparent;" name="shift_id" id="shift_id">
				  <option value="0" selected="selected">Please Select</option>
				  <?php foreach($shift_data as $skey => $svalue) { ?>
					<?php if($svalue['shift_id'] === $shift_id){ ?>
					  <option value="<?php echo $svalue['shift_id']; ?>" selected="selected"><?php echo $svalue['shift_name']; ?></option>
					<?php } else { ?>
					  <option value="<?php echo $svalue['shift_id']; ?>"><?php echo $svalue['shift_name']; ?></option>
					<?php } ?>
				  <?php } ?>
				</select>
				<?php if ($error_shift_id) { ?>
				<span class="error"><?php echo $error_shift_id; ?></span>
				<?php } ?>
				<input type="hidden" name="shift_id" id="shift1_id" value="<?php echo $shift_id; ?>" />
				<input type="hidden" name="hidden_shift_id" id="hidden_shift_id" value="<?php echo $shift_id; ?>" />
			  </td>
			</tr>
			<tr style="display:none;">
			  <td>
				On Duty
			  </td>
			  <td>
				<?php if($od) { ?>
				  <input type="checkbox" name="od" id="od" value="1" checked="checked" />
				<?php } else { ?>
				  <input type="checkbox" name="od" id="od" value="1" />
				<?php } ?>
			  </td>
			</tr> 
			<tr  style="display:none;">
			  <td><span class="required">*</span> <?php echo 'Shift Time'; ?></td>
			  <td>
				<input style="background: transparent;cursor:default;" type="text" name="shift_intime" id="shift_intime" value="<?php echo $shift_intime ?>" size="10" readonly="readonly" />
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input style="background: transparent;cursor:default;" type="text" name="shift_outtime" id="shift_outtime" value="<?php echo $shift_outtime ?>" size="10" readonly="readonly" />
			  </td>
			</tr>
			<tr>
			  <td><span class="required">*</span> <?php echo 'Actual Time'; ?></td>
			  <td>
				<input style="display: none;" type="text" name="date_from" id="date_from" value="<?php echo $date_from; ?>" size="10" class="date1" />
				
				<input type="text" name="actual_intime" id="actual_intime" value="<?php echo $actual_intime; ?>" size="10" class="time" <?php //echo ($actual_intime != '' && $actual_intime != '00:00:00') ? '' : 'class="time"' ?> />
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input style="display: none;" type="text" name="date_to" id="date_to" value="<?php echo $date_to; ?>" size="10" class="date1" />
				
				<input type="text" name="actual_outtime" id="actual_outtime" value="<?php echo $actual_outtime; ?>" size="10" class="time" <?php //echo ($actual_outtime != '' && $actual_outtime != '00:00:00') ? '' : 'class="time"' ?> />
				
				<input type="hidden" name="hidden_actual_intime" id="hidden_actual_intime" value="<?php echo $actual_intime; ?>" />
				<input type="hidden" name="hidden_actual_outtime" id="hidden_actual_outtime" value="<?php echo $actual_outtime; ?>" />
			  </td>
			</tr>
			<tr style="display: none;">
			  <td><span class="required">*</span> <?php echo 'First Half Status'; ?></td>
			  <td>
				<select name="firsthalf_status" id="firsthalf_status">
				  <?php foreach($statuses as $skey => $svalue) { ?>
					<?php if($firsthalf_status === $svalue['stat_id']) { ?>
					  <option value="<?php echo $svalue['stat_id'] ?>" selected="selected"><?php echo $svalue['stat_name']; ?></option>
					<?php } else { ?>
					  <option value="<?php echo $svalue['stat_id'] ?>"><?php echo $svalue['stat_name']; ?></option>
					<?php } ?>
				  <?php } ?>
				</select>
			  </td>
			</tr>
			<tr style="display: none;">
			  <td><span class="required">*</span> <?php echo 'Second Half Status'; ?></td>
			  <td>
				<select name="secondhalf_status" id="secondhalf_status">
				  <?php foreach($statuses as $skey => $svalue) { ?>
					<?php if($secondhalf_status === $svalue['stat_id']) { ?>
					  <option value="<?php echo $svalue['stat_id'] ?>" selected="selected"><?php echo $svalue['stat_name']; ?></option>
					<?php } else { ?>
					  <option value="<?php echo $svalue['stat_id'] ?>"><?php echo $svalue['stat_name']; ?></option>
					<?php } ?>
				  <?php } ?>
				</select>
			  </td>
			</tr>
		  </table>
		</div>
	  </form>
	</div>
  </div>
</div>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--
$('#tabs a').tabs();
$('.date').datepicker({
	dateFormat: 'yy-mm-dd',
	onSelect: function(date, instance) {
	  filter_name_id = $('#e_name_id').val();
	  filter_project_id = $('#project_id').val();
	  if(filter_name_id != '' && filter_project_id != '' && filter_project_id != '0'){
		$.ajax({
		  url: 'index.php?route=transaction/manualpunch/autocomplete&token=<?php echo $token; ?>&filter_name_id=' +  encodeURIComponent(filter_name_id) + '&filter_date=' + date + '&filter_project_id=' + filter_project_id,
		  dataType: 'json',
		  success: function(json) {   
			if(json['month_close'] == 1){
			  $('#close_warn').css('display', '');
			  $('.save').hide();
			  return false;
			} else {
			  $('.save').show();
			  $('#close_warn').css('display', 'none;');
			}
			$('#od').attr('checked', false);
			$('input[name=\'actual_intime\']').val(json['actual_intime']);
			$('input[name=\'actual_outtime\']').val(json['actual_outtime']);
			$('input[name=\'shift_intime\']').val(json['shift_intime']);
			$('input[name=\'shift_outtime\']').val(json['shift_outtime']);
			$('input[name=\'insert\']').val(json['insert_stat']);
			$('input[name=\'transaction_id\']').val(json['transaction_id']);
			$('input[name=\'date_from\']').val(json['date_from']);
			$('input[name=\'date_to\']').val(json['date_to']);

			//if(ui.item.actual_intime != '' && ui.item.actual_intime != '00:00:00'){
			  //$('#actual_intime').timepicker('disable');
			  //$('#actual_outtime').timepicker('disable');
			//} else {
			  //$('#actual_intime').timepicker('enable');
			  //$('#actual_outtime').timepicker('enable');
			//}
			/*
			on_duty = json[0]['on_duty'];
			if(on_duty == 1){
			  $('#od').attr('checked', true);
			} else {
			  $('#od').attr('checked', false);
			}
			*/
			firsthalf_status = json['firsthalf_status'];
			if(firsthalf_status == '1'){
			  $('#firsthalf_status option[value="1"]').attr("selected", "selected");
			} else if(firsthalf_status == '0') {
			  $('#firsthalf_status option[value="0"]').attr("selected", "selected");
			} else if(firsthalf_status == 'WO'){
			  $('#firsthalf_status option[value="WO"]').attr("selected", "selected");
			} else if(firsthalf_status == 'HLD'){
			  $('#firsthalf_status option[value="HLD"]').attr("selected", "selected");
			} else if(firsthalf_status == 'HD'){
			  $('#firsthalf_status option[value="HD"]').attr("selected", "selected");
			} else if(firsthalf_status == 'TOUR'){
			  $('#firsthalf_status option[value="TOUR"]').attr("selected", "selected");
			} else if(firsthalf_status == 'COF'){
			  $('#firsthalf_status option[value="COF"]').attr("selected", "selected");
			} else if(firsthalf_status == 'PL'){
			  $('#firsthalf_status option[value="PL"]').attr("selected", "selected");
			} else if(firsthalf_status == 'CL'){
			  $('#firsthalf_status option[value="CL"]').attr("selected", "selected");
			} else if(firsthalf_status == 'SL'){
			  $('#firsthalf_status option[value="SL"]').attr("selected", "selected");
			} else if(firsthalf_status == 'OD'){
			  $('#firsthalf_status option[value="OD"]').attr("selected", "selected");
			}

			secondhalf_status = json['secondhalf_status'];
			if(secondhalf_status == '1'){
			  $('#secondhalf_status option[value="1"]').attr("selected", "selected");
			} else if(secondhalf_status == '0') {
			  $('#secondhalf_status option[value="0"]').attr("selected", "selected");
			} else if(secondhalf_status == 'WO'){
			  $('#secondhalf_status option[value="WO"]').attr("selected", "selected");
			} else if(secondhalf_status == 'HLD'){
			  $('#secondhalf_status option[value="HLD"]').attr("selected", "selected");
			} else if(secondhalf_status == 'HD'){
			  $('#secondhalf_status option[value="HD"]').attr("selected", "selected");
			} else if(secondhalf_status == 'TOUR'){
			  $('#secondhalf_status option[value="TOUR"]').attr("selected", "selected");
			} else if(secondhalf_status == 'COF'){
			  $('#secondhalf_status option[value="COF"]').attr("selected", "selected");
			} else if(secondhalf_status == 'PL'){
			  $('#secondhalf_status option[value="PL"]').attr("selected", "selected");
			} else if(secondhalf_status == 'CL'){
			  $('#secondhalf_status option[value="CL"]').attr("selected", "selected");
			} else if(secondhalf_status == 'SL'){
			  $('#secondhalf_status option[value="SL"]').attr("selected", "selected");
			} else if(secondhalf_status == 'OD'){
			  $('#secondhalf_status option[value="OD"]').attr("selected", "selected");
			}

			shift_id = json['shift_id'];
			//$('#shift_id option[value="'+ shift_id +'"]').attr("selected", "selected").change();
			$('#shift_id option[value="'+ shift_id +'"]').attr("selected", "selected");

			$('#shift1_id').val(json['shift_id']);
			$('#hidden_actual_intime').val(json['actual_intime']);
			$('#hidden_actual_outtime').val(json['actual_outtime']);
			$('#hidden_shift_id').val(json['shift_id']);

			return false;  
		  }
		});
	  }
	}
});

$('.date1').datepicker({
	dateFormat: 'yy-mm-dd'
});

$('.time').timepicker({timeFormat: 'hh:mm:ss'});

$.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
	var self = this, currentCategory = '';
	$.each(items, function(index, item) {
	  if (item.category != currentCategory) {
		//ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
		currentCategory = item.category;
	  }
	  self._renderItem(ul, item);
	});
  }
});

$('input[name=\'e_name\']').autocomplete({	
	delay: 500,
  	source: function(request, response) {
		date = $('#dot').val();
		$.ajax({
		  	url: 'index.php?route=transaction/manualpunch/autocomplete_name&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term) + '&filter_date=' + date,
		  	dataType: 'json',
		  	success: function(json) {   
				response($.map(json, function(item) {
			  		return {
						label: item.name,
						value1: item.emp_id,
						project_datas: item.project_datas
			  		}
				}));
		  	}
		});
	}, 
  	select: function(event, ui) {
  		console.log(ui.item.label);
  		$('#e_name').val(ui.item.label);	
  		$('#e_name_id').val(ui.item.value1);	
  		$('#emp_code').val(ui.item.value1);	
  		$('#project_id').find('option').remove();
  		$.each(ui.item.project_datas, function (i, item) {
	    	$('#project_id').append($('<option>', { 
	        	value: item.project_id,
	        	text : item.project_name 
	   	 	}));
		});
  	}
});


//--></script>
<script type="text/javascript"><!--
$('#project_id').change(function(){
 	date = $('#dot').val();
 	emp_id = $('#emp_code').val();
 	project_id = $('#project_id').val();
	$.ajax({
	  	url: 'index.php?route=transaction/manualpunch/autocomplete&token=<?php echo $token; ?>&filter_name_id=' + emp_id + '&filter_date=' + date + '&filter_project_id=' + project_id,
	  	dataType: 'json',
	  	success: function(json) {   
			$('#od').attr('checked', false);
			
			if(json.month_close == 1){
			  $('#close_warn').css('display', '');
			  return false;
			} else {
			  $('#close_warn').css('display', 'none;');
			}
			$('input[name=\'actual_intime\']').val(json.actual_intime);
			$('input[name=\'actual_outtime\']').val(json.actual_outtime);
			$('input[name=\'shift_intime\']').val(json.shift_intime);
			$('input[name=\'shift_outtime\']').val(json.shift_outtime);
			$('input[name=\'insert\']').val(json.insert_stat);
			$('input[name=\'transaction_id\']').val(json.transaction_id);
			$('input[name=\'date_from\']').val(json.date_from);
			$('input[name=\'date_to\']').val(json.date_to);

			$('#hidden_actual_intime').val(json.actual_intime);
			$('#hidden_actual_outtime').val(json.actual_outtime);
			$('#shift1_id').val(json.shift_id);
			$('#hidden_shift_id').val(json.shift_id);

			if(json.actual_intime != '' && json.actual_intime != '00:00:00'){
			  //$('#actual_intime').timepicker('disable');
			  //$('#actual_outtime').timepicker('disable');
			} else {
			  //$('#actual_intime').timepicker('enable');
			  //$('#actual_outtime').timepicker('enable');
			}

			/*
			on_duty = ui.item.on_duty;
			if(on_duty == 1){
			  $('#od').attr('checked', true);
			} else {
			  $('#od').attr('checked', false);
			}
			*/

			firsthalf_status = json.firsthalf_status;
			if(firsthalf_status == '1'){
			  $('#firsthalf_status option[value="1"]').attr("selected", "selected");
			} else if(firsthalf_status == '0') {
			  $('#firsthalf_status option[value="0"]').attr("selected", "selected");
			} else if(firsthalf_status == 'WO'){
			  $('#firsthalf_status option[value="WO"]').attr("selected", "selected");
			} else if(firsthalf_status == 'HLD'){
			  $('#firsthalf_status option[value="HLD"]').attr("selected", "selected");
			} else if(firsthalf_status == 'HD'){
			  $('#firsthalf_status option[value="HD"]').attr("selected", "selected");
			} else if(firsthalf_status == 'COF'){
			  $('#firsthalf_status option[value="COF"]').attr("selected", "selected");
			} else if(firsthalf_status == 'TOUR'){
			  $('#firsthalf_status option[value="TOUR"]').attr("selected", "selected");
			} else if(firsthalf_status == 'PL'){
			  $('#firsthalf_status option[value="PL"]').attr("selected", "selected");
			} else if(firsthalf_status == 'CL'){
			  $('#firsthalf_status option[value="CL"]').attr("selected", "selected");
			} else if(firsthalf_status == 'SL'){
			  $('#firsthalf_status option[value="SL"]').attr("selected", "selected");
			} else if(firsthalf_status == 'OD'){
			  $('#firsthalf_status option[value="OD"]').attr("selected", "selected");
			}

			secondhalf_status = json.secondhalf_status;
			if(secondhalf_status == '1'){
			  $('#secondhalf_status option[value="1"]').attr("selected", "selected");
			} else if(secondhalf_status == '0') {
			  $('#secondhalf_status option[value="0"]').attr("selected", "selected");
			} else if(secondhalf_status == 'WO'){
			  $('#secondhalf_status option[value="WO"]').attr("selected", "selected");
			} else if(secondhalf_status == 'HLD'){
			  $('#secondhalf_status option[value="HLD"]').attr("selected", "selected");
			} else if(secondhalf_status == 'HD'){
			  $('#secondhalf_status option[value="HD"]').attr("selected", "selected");
			} else if(secondhalf_status == 'TOUR'){
			  $('#secondhalf_status option[value="TOUR"]').attr("selected", "selected");
			} else if(secondhalf_status == 'COF'){
			  $('#secondhalf_status option[value="COF"]').attr("selected", "selected");
			} else if(secondhalf_status == 'PL'){
			  $('#secondhalf_status option[value="PL"]').attr("selected", "selected");
			} else if(secondhalf_status == 'CL'){
			  $('#secondhalf_status option[value="CL"]').attr("selected", "selected");
			} else if(secondhalf_status == 'SL'){
			  $('#secondhalf_status option[value="SL"]').attr("selected", "selected");
			} else if(secondhalf_status == 'SHL'){
			  $('#secondhalf_status option[value="SHL"]').attr("selected", "selected");
			} else if(secondhalf_status == 'OD'){
			  $('#secondhalf_status option[value="OD"]').attr("selected", "selected");
			}
			//alert(ui.item.shift_id);
			shift_id = json.shift_id;
			//$('#shift_id option[value="'+ shift_id +'"]').attr("selected", "selected").change();
			$('#shift_id option[value="'+ shift_id +'"]').attr("selected", "selected");
			//$('$shift_id').trigger
			return false;
		}
	});
});

$('#shift_id').change(function(){
  shift_id = $(this).val();
  dot = $('#dot').val();
  emp_code = $('#emp_code').val();
  $.ajax({
	url: 'index.php?route=transaction/manualpunch/getshiftdata&token=<?php echo $token; ?>&shift_id=' + shift_id+'&transaction_id=' + transaction_id+'&dot=' + dot+'&emp_code=' + emp_code,
	dataType: 'json',
	success: function(json) {  
	  if(json.status == 1){
		$('#shift_intime').val(json.shift_data['in_time']);
		$('#shift_outtime').val(json.shift_data['out_time']);
		dot = $('#dot').val();
		$('#date_from').val(dot);
		$('#date_to').val(dot);
	  } else {
		$('#shift_intime').val('00:00:00');
		$('#shift_outtime').val('00:00:00');
		dot = $('#dot').val();
		$('#date_from').val(dot);
		$('#date_to').val(dot);
	  } 
	}
  });
});
//--></script>
<?php echo $footer; ?>