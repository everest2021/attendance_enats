<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
  <table class="product" style="width:100% !important;">
  <?php 
              if($exceptional) {
            ?>
              <tr>
                  <td colspan="4">
                      Before closing the day you will need to manually settle the abnormal entries for date : <?php echo $filter_date; ?>
                  </td>
              </tr>
              <tr>
                <td>
                    Sr No
                </td>
                <td>
                    Name
                </td>
                <td>
                    In time
                </td>
                <td>
                    Out time
                </td>
              </tr>
              <?php 
                $i = 1;
                foreach($exceptional as $data) {
              ?>
                <tr>
                  <td>
                    <?php echo $i; ?>
                  </td>
                  <td>
                    <?php echo $data['emp_name']; ?>
                  </td>
                  <td>
                    <?php echo $data['act_intime']; ?>
                  </td>
                  <td>
                    <?php echo $data['act_outtime']; ?>
                  </td> 
                </tr>
              <?php
                }
              ?>
            <?php 
              } 
            ?>
  </table>
</body>
</html>