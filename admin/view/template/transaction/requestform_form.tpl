<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div style="display:none;" class="ajax_warning warning">Month Already Closed</div>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" id="save" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <div id="tabs" class="htabs"><a href="#tab-general"><?php echo $tab_general; ?></a></div>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general">
          <table class="form">
            <tr>
              <td><span class="required">*</span> <?php echo 'Employee Name'; ?></td>
              <td colspan="2">
                <input readonly="readonly" type="text" name="e_name" id="e_name" value="<?php echo $e_name; ?>" size="100" />
                <input type="hidden" name="batch_id" id="batch_id" value="<?php echo $batch_id; ?>" />
              </td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo 'Emp Code'; ?></td>
              <td colspan="2">
                <input readonly="readonly" type="text" name="e_name_id" id="e_name_id" value="<?php echo $e_name_id; ?>" size="10" />
              </td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo 'Request Type'; ?></td>
              <td>
                <select name="request_type" id="request_type">
                  <?php foreach($request_types as $lkey => $lvalue) { ?>
                    <?php if($lkey == $request_type){ ?>
                      <option value="<?php echo $lkey; ?>" selected="selected"><?php echo $lvalue; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $lkey; ?>"><?php echo $lvalue; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
                <?php if ($error_request_type) { ?>
                  <span class="error"><?php echo $error_request_type; ?></span>
                <?php } ?>
              </td>
            </tr>
            <tr id="leave_data_div" style="display:none;">
              <td><span class="required">*</span> <?php echo 'Leave Data'; ?></td>
              <td>
                <input readonly="readonly" type="text" name="leave_data" id="leave_data" value="<?php echo $leave_data ?>" size="30" />
              </td>
            </tr>
            <tr id="dot_div" style="display:none;">
              <td><span class="required">*</span> <?php echo 'Date'; ?></td>
              <td>
                <input class="date" type="text" name="dot" id="dot" value="<?php echo $dot ?>" size="30" />
              </td>
            </tr>
            <tr>
              <td><span class="required"></span> <?php echo 'Description'; ?></td>
              <td colspan="2">
                <textarea name="description" cols="40" rows="8"><?php echo $description ?></textarea>
              </td>
            </tr>
          </table>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--
$('#tabs a').tabs();

$(document).ready(function() {
  is_leave = '<?php echo $is_leave ?>';
  if(is_leave == '1'){
    $('#leave_data_div').show();
    $('#dot_div').hide();
  } else {
    $('#dot_div').show();
    $('#leave_data_div').hide();
  }
});

$('#request_type').change(function(){
  type = $(this).val();
  if(type == '1'){
    $('#leave_data_div').show();
    $('#dot_div').hide();
  } else {
    $('#dot_div').show();
    $('#leave_data_div').hide();
  }  
});
//--></script>
<script type="text/javascript"><!--
$('.date').datepicker({
    dateFormat: 'yy-mm-dd'
});
//--></script>
<?php echo $footer; ?>