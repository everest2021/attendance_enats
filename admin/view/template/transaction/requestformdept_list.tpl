<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo 'Request Approval List'; ?></h1>
      <div class="buttons">
        <a onclick="('#form').submit();" class="button" style=""><?php echo 'Approve'; ?></a>
        <a onclick="reject_fun()" class="button delete" style=""><?php echo 'Reject'; ?></a>
      </div>
    </div>
    <?php if(isset($this->session->data['is_dept'])){ ?>
    <div style="margin-left: 40%;">
      <div style="text-align: left;">
        <span style="font-size: 16px;">Department</span> : <b style="font-size: 16px;"><?php echo $this->session->data['dept_name']; ?></b>
      </div>
    </div>
    <?php } ?>
    <div class="content">
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left" style="width:16%;">
                <?php echo 'Name'; ?>
              </td>
              <td class="left" style="width:7%;">
                <?php echo 'Dte of Input'; ?>
              </td>
               <td class="left" style="width:13%;">
                <?php echo 'Dte of Proc'; ?>
              </td>
              <td class="left" style="width:9%;">
                <?php echo 'Request Type'; ?>
              </td>
              <td class="left" style="width:12%;display: none;">
                <?php echo 'Leave Data'; ?>
              </td>
              <td class="left">
                <?php echo 'Description'; ?>
              </td>
              <td style="width:6%;">
                <a><?php echo "Unit"; ?></a>
              </td>
              <td style="width:10%;">
                <a><?php echo "Approval"; ?></a>
              </td>
              <td class="right" style="width:11%;"><?php echo 'Action'; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr class="filter">
              <td>&nbsp;</td>
              <td>
                <input type="text" id="filter_name" name="filter_name" value="<?php echo $filter_name; ?>" style="width:170px;" />
                <input type="hidden" id="filter_name_id" name="filter_name_id" value="<?php echo $filter_name_id; ?>" />
              </td>
              <td><input type="text" id="filter_date" name="filter_date" value="<?php echo $filter_date; ?>" class="date"  style="width:90px;" /></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td style="display: none;">&nbsp;</td>
              <td>&nbsp;</td>
              <td style="width:7%">
                <select name="filter_unit" style="width:75px;">
                  <?php foreach($unit_data as $key => $ud) { ?>
                    <?php if($key == $filter_unit) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td>
                <select name="filter_approval_1">
                  <?php foreach($approves as $key => $ud) { ?>
                    <?php if($key == $filter_approval_1) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td align="right">
                <a onclick="filter();" class="button"><?php echo $button_filter; ?></a>
              </td>
            </tr>
            <form action="<?php echo $approve; ?>" method="post" enctype="multipart/form-data" id="form">
              <?php if ($requests) { ?>
              <?php foreach ($requests as $employee) { ?>
              <tr>
                <td style="text-align: center;"><?php if ($employee['selected']) { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $employee['id']; ?>" checked="checked" />
                  <?php } else { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $employee['id']; ?>" />
                  <?php } ?></td>
                <td class="left"><?php echo $employee['name']; ?></td>
                <td class="left"><?php echo $employee['doi']; ?></td>
                <td class="left"><?php echo $employee['dot']; ?></td>
                <td class="left"><?php echo $employee['request_type']; ?></td>
                <td class="left" style="display: none;"><?php echo $employee['leave_data']; ?></td>
                <td class="left"><?php echo $employee['description']; ?></td>
                <td class="left"><?php echo $employee['unit']; ?></td>
                <td class="left"><?php echo $employee['approval_1']; ?></td>
                <td class="right"><?php foreach ($employee['action'] as $action) { ?>
                  <?php if($action['href'] == '') { ?>
                    <p style="cursor:default;"><?php echo $action['text']; ?></p>
                  <?php } else { ?>
                    [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                  <?php } ?>
                  <?php } ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="center" colspan="9"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </form>
          </tbody>
        </table>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=transaction/requestformdept&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  if (filter_name) {
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name=' + encodeURIComponent(filter_name);  
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    }
  }

  var filter_date = $('input[name=\'filter_date\']').attr('value');
  if (filter_date) {
    url += '&filter_date=' + encodeURIComponent(filter_date);
  }

  var filter_approval_1 = $('select[name=\'filter_approval_1\']').attr('value');
  if (filter_approval_1) {
    url += '&filter_approval_1=' + encodeURIComponent(filter_approval_1);
  }

  var filter_dept = $('select[name=\'filter_dept\']').attr('value');
  if (filter_dept) {
    url += '&filter_dept=' + encodeURIComponent(filter_dept);
  }

  var filter_unit = $('select[name=\'filter_unit\']').attr('value');
  if (filter_unit) {
    url += '&filter_unit=' + encodeURIComponent(filter_unit);
  }

  location = url;
  return false;
}
//--></script>
<script type="text/javascript"><!--
$('#filter_name, #filter_date').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});

$.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
    var self = this, currentCategory = '';
    $.each(items, function(index, item) {
      if (item.category != currentCategory) {
        //ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
        currentCategory = item.category;
      }
      self._renderItem(ul, item);
    });
  }
});

$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=transaction/leave_ess_dept/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.employee_id,
            emp_code: item.emp_code
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    $('input[name=\'filter_name_id\']').val(ui.item.emp_code);
            
    return false;
  },
  focus: function(event, ui) {
        return false;
    }
});

function reject_fun(){
  reject_var = '<?php echo $reject; ?>';
  reject_var = reject_var.replace('&amp;', '&');
  reject_var = reject_var.replace('&amp;', '&');
  reject_var = reject_var.replace('&amp;', '&');
  reject_var = reject_var.replace('&amp;', '&');
  reject_var = reject_var.replace('&amp;', '&');
  reject_var = reject_var.replace('&amp;', '&');
  reject_var = reject_var.replace('&amp;', '&');
  $('#form').attr('action', reject_var);
  $('#form').submit();
}

//--></script> 
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--
$('.date').datepicker({
    dateFormat: 'yy-mm-dd'
});
//--></script>
<?php echo $footer; ?>