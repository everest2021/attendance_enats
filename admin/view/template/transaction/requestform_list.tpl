<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo 'Self Request List'; ?></h1>
      <div class="buttons">
        <a href="<?php echo $insert; ?>" class="button" style=""><?php echo $button_insert; ?></a>
        <a onclick="$('#form').submit();" class="button" style=""><?php echo 'Delete'; ?></a>
      </div>
    </div>
    <div style="margin-left: 10%;">
      <div style="text-align: left;">
        <span style="font-size: 16px;">Name</span> : <b style="font-size: 16px;"><?php echo $filter_name; ?></b> &nbsp;&nbsp; | &nbsp;&nbsp;
        <span style="font-size: 16px;">Id</span> : <b style="font-size: 16px;"><?php echo $filter_name_id; ?></b> &nbsp;&nbsp; | &nbsp;&nbsp;
        <span style="font-size: 16px;">Department</span> : <b style="font-size: 16px;"><?php echo $filter_dept; ?></b> &nbsp;&nbsp; | &nbsp;&nbsp;
        <span style="font-size: 16px;">Unit</span> : <b style="font-size: 16px;"><?php echo $filter_unit; ?></b>
      </div>
    </div>
    <div class="content">
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left" style="width:10%;">
                <?php echo 'Dte of input'; ?>
              </td>
              <td class="left" style="width:13%;">
                <?php echo 'Dte of Proc'; ?>
              </td>
              <td class="left" style="width:11%;">
                <?php echo 'Request Type'; ?>
              </td>
              <td class="left" style="width:15%;display: none;">
                <?php echo 'Leave Data'; ?>
              </td>
              <td class="left">
                <?php echo 'Description'; ?>
              </td>
              <td style="width:10%;">
                <a><?php echo "Approval"; ?></a>
              </td>
              <td class="right" style="width:8%;"><?php echo 'Action'; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr class="filter">
              <td>&nbsp;</td>
              <td><input type="text" id="filter_date" name="filter_date" value="<?php echo $filter_date; ?>" class="date"  style="width:90px;" /></td>
              <td>
                <input type="text" id="filter_date_proc" name="filter_date_proc" value="<?php echo $filter_date_proc; ?>" class="date"  style="width:90px;display: none;" />
              </td>
              <td>&nbsp;</td>
              <td style="display: none;">&nbsp;</td>
              <td>&nbsp;</td>
              <td>
                <select name="filter_approval_1">
                  <?php foreach($approves as $key => $ud) { ?>
                    <?php if($key == $filter_approval_1) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td align="right">
                <a onclick="filter();" class="button"><?php echo $button_filter; ?></a>
              </td>
            </tr>
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
              <?php if ($requests) { ?>
              <?php foreach ($requests as $employee) { ?>
              <tr>
                <td style="text-align: center;"><?php if ($employee['selected']) { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $employee['id']; ?>" checked="checked" />
                  <?php } else { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $employee['id']; ?>" />
                  <?php } ?></td>
                <td class="left"><?php echo $employee['doi']; ?></td>
                <td class="left"><?php echo $employee['dot']; ?></td>
                <td class="left"><?php echo $employee['request_type']; ?></td>
                <td class="left" style="display: none;"><?php echo $employee['leave_data']; ?></td>
                <td class="left"><?php echo $employee['description']; ?></td>
                <td class="left"><?php echo $employee['approval_1']; ?></td>
                <td class="right"><?php foreach ($employee['action'] as $action) { ?>
                  <?php if($action['href'] == '') { ?>
                    <p style="cursor:default;"><?php echo $action['text']; ?></p>
                  <?php } else { ?>
                    [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                  <?php } ?>
                  <?php } ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="center" colspan="7"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </form>
          </tbody>
        </table>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=transaction/requestform&token=<?php echo $token; ?>';
  
  var filter_date = $('input[name=\'filter_date\']').attr('value');
  if (filter_date) {
    url += '&filter_date=' + encodeURIComponent(filter_date);
  }

  var filter_date_proc = $('input[name=\'filter_date_proc\']').attr('value');
  if (filter_date_proc) {
    url += '&filter_date_proc=' + encodeURIComponent(filter_date_proc);
  }

  var filter_approval_1 = $('select[name=\'filter_approval_1\']').attr('value');
  if (filter_approval_1) {
    url += '&filter_approval_1=' + encodeURIComponent(filter_approval_1);
  }

  location = url;
  return false;
}
//--></script>
<script type="text/javascript"><!--
$('#filter_name, #filter_date').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});

$.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
    var self = this, currentCategory = '';
    $.each(items, function(index, item) {
      if (item.category != currentCategory) {
        //ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
        currentCategory = item.category;
      }
      self._renderItem(ul, item);
    });
  }
});
//--></script> 
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--
$('.date').datepicker({
    dateFormat: 'yy-mm-dd'
});
//--></script>
<?php echo $footer; ?>