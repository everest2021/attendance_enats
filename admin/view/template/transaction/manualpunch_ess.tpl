<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="warning" id="close_warn" style="display:none;"><?php echo 'Month already Closed'; ?></div>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
        <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
        <a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a>
      </div>
    </div>
    <div class="content">
      <div id="tabs" class="htabs"><a href="#tab-general"><?php echo $tab_general; ?></a></div>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general">
          <table class="form">
            <tr class="px_record_class">
              <td><span class="required">*</span> <?php echo 'Date'; ?></td>
              <td><input readonly="readony" style="cursor:default;background: transparent;" type="text" id="dot" name="dot" value="<?php echo $dot; ?>" size="10" class="date"/>
                <?php if ($error_dot) { ?>
                <span class="error"><?php echo $error_dot; ?></span>
                <?php } ?>
              </td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo 'Employee Name'; ?></td>
              <td>
                <input style="background: transparent;" readonly="readonly" type="text" name="e_name" id="e_name" value="<?php echo $e_name; ?>" size="100" />
                <input type="hidden" name="e_name_id" id="e_name_id" value="<?php echo $e_name_id; ?>" size="100" />
                <input type="hidden" name="insert" id="insert" value="<?php echo $insert; ?>" />
                <input type="hidden" name="transaction_id" id="transaction_id" value="<?php echo $transaction_id; ?>" />
                <input type="hidden" name="id" id="id" value="<?php echo $id; ?>" />
                <?php if ($error_employee) { ?>
                <span class="error"><?php echo $error_employee; ?></span>
                <?php } ?></td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo 'Emp Code'; ?></td>
              <td>
                <input style="background: transparent;" readonly="readonly" type="text" id="emp_code" name="emp_code" value="<?php echo $emp_code; ?>" size="10" />
              </td>
            </tr>
            <tr>
              <td><span class="required">*</span><?php echo 'Type'; ?></td>
              <td>
                <select name="type" id="type">
                  <?php foreach($types as $skey => $svalue) { ?>
                    <?php if($skey == $type){ ?>
                      <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
                <?php if ($error_type) { ?>
                <span class="error"><?php echo $error_type; ?></span>
                <?php } ?>
              </td>
            </tr>
            <tr class="px_record_class">
              <td><span class="required">*</span><?php echo 'PX Record Type'; ?></td>
              <td>
                <select name="px_record_type" id="px_record_type">
                  <option value="0" selected="selected">Please Select</option>
                  <?php foreach($px_record_types as $skey => $svalue) { ?>
                    <?php if($skey == $px_record_type){ ?>
                      <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
                <?php if ($error_px_record_type) { ?>
                <span class="error"><?php echo $error_px_record_type; ?></span>
                <?php } ?>
              </td>
            </tr>
            <tr style="display: none;">
              <td><span class="required">*</span><?php echo 'Shift'; ?></td>
              <td>
                <select name="shift_id" id="shift_id">
                  <option value="0">Please Select</option>
                  <?php foreach($shift_data as $skey => $svalue) { ?>
                    <?php if($svalue['shift_id'] === $shift_id){ ?>
                      <option value="<?php echo $svalue['shift_id']; ?>" selected="selected"><?php echo $svalue['shift_name']; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $svalue['shift_id']; ?>"><?php echo $svalue['shift_name']; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
                <?php if ($error_shift_id) { ?>
                <span class="error"><?php echo $error_shift_id; ?></span>
                <?php } ?>
                <input type="hidden" name="hidden_shift_id" id="hidden_shift_id" value="<?php echo $shift_id; ?>" />
              </td>
            </tr>
            <tr class="px_record_class">
              <td><span class="required">*</span> <?php echo 'Shift Time'; ?></td>
              <td>
                <input style="background: transparent;" type="text" name="shift_intime" id="shift_intime" value="<?php echo $shift_intime ?>" size="10" readonly="readonly" />
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input style="background: transparent;" type="text" name="shift_outtime" id="shift_outtime" value="<?php echo $shift_outtime ?>" size="10" readonly="readonly" />
              </td>
            </tr>
            <tr class="px_record_class">
              <td><span class="required">*</span> <?php echo 'Actual Time'; ?></td>
              <td>
                <input style="display: none;" type="text" name="date_from" id="date_from" value="<?php echo $date_from; ?>" size="10" class="date1" />
                
                <input type="text" name="actual_intime" id="actual_intime" value="<?php echo $actual_intime; ?>" size="10" class="time" />
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input style="display: none;" type="text" name="date_to" id="date_to" value="<?php echo $date_to; ?>" size="10" class="date1" />
                
                <input type="text" name="actual_outtime" id="actual_outtime" value="<?php echo $actual_outtime; ?>" size="10" class="time" />
                
                <input type="hidden" name="hidden_actual_intime" id="hidden_actual_intime" value="<?php echo $actual_intime; ?>" />
                <input type="hidden" name="hidden_actual_outtime" id="hidden_actual_outtime" value="<?php echo $actual_outtime; ?>" />
              </td>
            </tr>
            <tr class="tour_class">
              <td><span class="required">*</span> <?php echo 'Date'; ?></td>
              <td>
                <input type="text" name="date_from1" id="date_from1" value="<?php echo $date_from1; ?>" size="10" class="date1" />
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="text" name="date_to1" id="date_to1" value="<?php echo $date_to1; ?>" size="10" class="date1" />
              </td>
            </tr>
            <tr style="display: none;">
              <td><span class="required">*</span> <?php echo 'First Half Status'; ?></td>
              <td>
                <select name="firsthalf_status" id="firsthalf_status">
                  <?php foreach($statuses as $skey => $svalue) { ?>
                    <?php if($firsthalf_status === $svalue['stat_id']) { ?>
                      <option value="<?php echo $svalue['stat_id'] ?>" selected="selected"><?php echo $svalue['stat_name']; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $svalue['stat_id'] ?>"><?php echo $svalue['stat_name']; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
            </tr>
            <tr style="display: none;">
              <td><span class="required">*</span> <?php echo 'Second Half Status'; ?></td>
              <td>
                <select name="secondhalf_status" id="secondhalf_status">
                  <?php foreach($statuses as $skey => $svalue) { ?>
                    <?php if($secondhalf_status === $svalue['stat_id']) { ?>
                      <option value="<?php echo $svalue['stat_id'] ?>" selected="selected"><?php echo $svalue['stat_name']; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $svalue['stat_id'] ?>"><?php echo $svalue['stat_name']; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo 'Reason'; ?></td>
              <td>
                <input autocomplete="false" type="text" name="punch_reason" id="punch_reason" value="<?php echo $punch_reason ?>" size="50" />
                <?php if ($error_punch_reason) { ?>
                <span class="error"><?php echo $error_punch_reason; ?></span>
                <?php } ?>
              </td>
            </tr>
          </table>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--
$('#tabs a').tabs();
$('.date').datepicker({
    dateFormat: 'yy-mm-dd',
    onSelect: function(date, instance) {
      filter_name = $('#e_name').val();
      filter_name_id = $('#e_name_id').val();
      if(filter_name != ''){
        $.ajax({
          url: 'index.php?route=transaction/manualpunch_ess/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(filter_name) + '&filter_date=' + date + '&filter_name_id=' + filter_name_id,
          dataType: 'json',
          success: function(json) {   
            if(json[0]['month_close'] == 1){
              $('#close_warn').css('display', '');
              $('.save').hide();
              return false;
            } else {
              $('.save').show();
              $('#close_warn').css('display', 'none;');
            }
            $('#od').attr('checked', false);
            $('input[name=\'e_name\']').val(json[0]['name']);
            $('input[name=\'e_name_id\']').val(json[0]['emp_id']);
            $('input[name=\'emp_code\']').val(json[0]['emp_id']);
            $('input[name=\'actual_intime\']').val(json[0]['actual_intime']);
            $('input[name=\'actual_outtime\']').val(json[0]['actual_outtime']);
            $('input[name=\'shift_intime\']').val(json[0]['shift_intime']);
            $('input[name=\'shift_outtime\']').val(json[0]['shift_outtime']);
            $('input[name=\'insert\']').val(json[0]['insert_stat']);
            $('input[name=\'transaction_id\']').val(json[0]['transaction_id']);
            $('input[name=\'id\']').val(json[0]['id']);
            $('input[name=\'date_from\']').val(json[0]['date_from']);
            $('input[name=\'date_to\']').val(json[0]['date_to']);

            //if(ui.item.actual_intime != '' && ui.item.actual_intime != '00:00:00'){
              //$('#actual_intime').timepicker('disable');
              //$('#actual_outtime').timepicker('disable');
            //} else {
              //$('#actual_intime').timepicker('enable');
              //$('#actual_outtime').timepicker('enable');
            //}
            /*
            on_duty = json[0]['on_duty'];
            if(on_duty == 1){
              $('#od').attr('checked', true);
            } else {
              $('#od').attr('checked', false);
            }
            */
            firsthalf_status = json[0]['firsthalf_status'];
            if(firsthalf_status == '1'){
              $('#firsthalf_status option[value="1"]').attr("selected", "selected");
            } else if(firsthalf_status == '0') {
              $('#firsthalf_status option[value="0"]').attr("selected", "selected");
            } else if(firsthalf_status == 'WO'){
              $('#firsthalf_status option[value="WO"]').attr("selected", "selected");
            } else if(firsthalf_status == 'HLD'){
              $('#firsthalf_status option[value="HLD"]').attr("selected", "selected");
            } else if(firsthalf_status == 'HD'){
              $('#firsthalf_status option[value="HD"]').attr("selected", "selected");
            } else if(firsthalf_status == 'TOUR'){
              $('#firsthalf_status option[value="TOUR"]').attr("selected", "selected");
            } else if(firsthalf_status == 'COF'){
              $('#firsthalf_status option[value="COF"]').attr("selected", "selected");
            } else if(firsthalf_status == 'PL'){
              $('#firsthalf_status option[value="PL"]').attr("selected", "selected");
            } else if(firsthalf_status == 'CL'){
              $('#firsthalf_status option[value="CL"]').attr("selected", "selected");
            } else if(firsthalf_status == 'SL'){
              $('#firsthalf_status option[value="SL"]').attr("selected", "selected");
            } else if(firsthalf_status == 'OD'){
              $('#firsthalf_status option[value="OD"]').attr("selected", "selected");
            }

            secondhalf_status = json[0]['secondhalf_status'];
            if(secondhalf_status == '1'){
              $('#secondhalf_status option[value="1"]').attr("selected", "selected");
            } else if(secondhalf_status == '0') {
              $('#secondhalf_status option[value="0"]').attr("selected", "selected");
            } else if(secondhalf_status == 'WO'){
              $('#secondhalf_status option[value="WO"]').attr("selected", "selected");
            } else if(secondhalf_status == 'HLD'){
              $('#secondhalf_status option[value="HLD"]').attr("selected", "selected");
            } else if(secondhalf_status == 'HD'){
              $('#secondhalf_status option[value="HD"]').attr("selected", "selected");
            } else if(secondhalf_status == 'TOUR'){
              $('#secondhalf_status option[value="TOUR"]').attr("selected", "selected");
            } else if(secondhalf_status == 'COF'){
              $('#secondhalf_status option[value="COF"]').attr("selected", "selected");
            } else if(secondhalf_status == 'PL'){
              $('#secondhalf_status option[value="PL"]').attr("selected", "selected");
            } else if(secondhalf_status == 'CL'){
              $('#secondhalf_status option[value="CL"]').attr("selected", "selected");
            } else if(secondhalf_status == 'SL'){
              $('#secondhalf_status option[value="SL"]').attr("selected", "selected");
            } else if(secondhalf_status == 'OD'){
              $('#secondhalf_status option[value="OD"]').attr("selected", "selected");
            }

            shift_id = json[0]['shift_id'];
            //$('#shift_id option[value="'+ shift_id +'"]').attr("selected", "selected").change();
            $('#shift_id option[value="'+ shift_id +'"]').attr("selected", "selected");

            type = json[0]['type'];
            $('#type option[value="'+ type +'"]').attr("selected", "selected");

            px_record_type = json[0]['px_record_type'];
            $('#px_record_type option[value="'+ px_record_type +'"]').attr("selected", "selected");

            $('#shift1_id').val(json[0]['shift_id']);
            $('#hidden_actual_intime').val(json[0]['actual_intime']);
            $('#hidden_actual_outtime').val(json[0]['actual_outtime']);
            $('#hidden_shift_id').val(json[0]['shift_id']);

            return false;  
          }
        });
      }
    }
});

$('.date1').datepicker({
    dateFormat: 'yy-mm-dd'
});

$('.time').timepicker({timeFormat: 'hh:mm:ss'});

$.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
    var self = this, currentCategory = '';
    $.each(items, function(index, item) {
      if (item.category != currentCategory) {
        //ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
        currentCategory = item.category;
      }
      self._renderItem(ul, item);
    });
  }
});

//--></script>
<script type="text/javascript"><!--
$('input[name=\'e_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    date = $('#dot').val();
    $.ajax({
      url: 'index.php?route=transaction/manualpunch_ess/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term) + '&filter_date=' + date,
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.emp_id,
            actual_intime: item.actual_intime,
            shift_id: item.shift_id,
            actual_intime: item.actual_intime,
            actual_outtime: item.actual_outtime,
            shift_intime: item.shift_intime,
            shift_outtime: item.shift_outtime,
            firsthalf_status: item.firsthalf_status,
            secondhalf_status: item.secondhalf_status,
            insert_stat: item.insert_stat,
            date_from: item.date_from,
            date_to: item.date_to,
            month_close: item.month_close,
            transaction_id: item.transaction_id,
            id: item.id,
            on_duty: item.on_duty
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('#od').attr('checked', false);
    if(ui.item.month_close == 1){
      $('#close_warn').css('display', '');
      return false;
    } else {
      $('#close_warn').css('display', 'none;');
    }

    $('input[name=\'e_name\']').val(ui.item.label);
    $('input[name=\'e_name_id\']').val(ui.item.value);
    $('input[name=\'emp_code\']').val(ui.item.value);
    $('input[name=\'actual_intime\']').val(ui.item.actual_intime);
    $('input[name=\'actual_outtime\']').val(ui.item.actual_outtime);
    $('input[name=\'shift_intime\']').val(ui.item.shift_intime);
    $('input[name=\'shift_outtime\']').val(ui.item.shift_outtime);
    $('input[name=\'insert\']').val(ui.item.insert_stat);
    $('input[name=\'transaction_id\']').val(ui.item.transaction_id);
    $('input[name=\'id\']').val(ui.item.id);
    $('input[name=\'date_from\']').val(ui.item.date_from);
    $('input[name=\'date_to\']').val(ui.item.date_to);

    $('#hidden_actual_intime').val(ui.item.actual_intime);
    $('#hidden_actual_outtime').val(ui.item.actual_outtime);
    $('#hidden_shift_id').val(ui.item.shift_id);
    $('#shift1_id').val(ui.item.shift_id);

    if(ui.item.actual_intime != '' && ui.item.actual_intime != '00:00:00'){
      //$('#actual_intime').timepicker('disable');
      //$('#actual_outtime').timepicker('disable');
    } else {
      //$('#actual_intime').timepicker('enable');
      //$('#actual_outtime').timepicker('enable');
    }

    /*
    on_duty = ui.item.on_duty;
    if(on_duty == 1){
      $('#od').attr('checked', true);
    } else {
      $('#od').attr('checked', false);
    }
    */

    firsthalf_status = ui.item.firsthalf_status;
    if(firsthalf_status == '1'){
      $('#firsthalf_status option[value="1"]').attr("selected", "selected");
    } else if(firsthalf_status == '0') {
      $('#firsthalf_status option[value="0"]').attr("selected", "selected");
    } else if(firsthalf_status == 'WO'){
      $('#firsthalf_status option[value="WO"]').attr("selected", "selected");
    } else if(firsthalf_status == 'HLD'){
      $('#firsthalf_status option[value="HLD"]').attr("selected", "selected");
    } else if(firsthalf_status == 'HD'){
      $('#firsthalf_status option[value="HD"]').attr("selected", "selected");
    } else if(firsthalf_status == 'TOUR'){
      $('#firsthalf_status option[value="TOUR"]').attr("selected", "selected");
    } else if(firsthalf_status == 'COF'){
      $('#firsthalf_status option[value="COF"]').attr("selected", "selected");
    } else if(firsthalf_status == 'PL'){
      $('#firsthalf_status option[value="PL"]').attr("selected", "selected");
    } else if(firsthalf_status == 'CL'){
      $('#firsthalf_status option[value="CL"]').attr("selected", "selected");
    } else if(firsthalf_status == 'SL'){
      $('#firsthalf_status option[value="SL"]').attr("selected", "selected");
    } else if(firsthalf_status == 'OD'){
      $('#firsthalf_status option[value="OD"]').attr("selected", "selected");
    }

    secondhalf_status = ui.item.secondhalf_status;
    if(secondhalf_status == '1'){
      $('#secondhalf_status option[value="1"]').attr("selected", "selected");
    } else if(secondhalf_status == '0') {
      $('#secondhalf_status option[value="0"]').attr("selected", "selected");
    } else if(secondhalf_status == 'WO'){
      $('#secondhalf_status option[value="WO"]').attr("selected", "selected");
    } else if(secondhalf_status == 'HLD'){
      $('#secondhalf_status option[value="HLD"]').attr("selected", "selected");
    } else if(secondhalf_status == 'HD'){
      $('#secondhalf_status option[value="HD"]').attr("selected", "selected");
    } else if(secondhalf_status == 'TOUR'){
      $('#secondhalf_status option[value="TOUR"]').attr("selected", "selected");
    } else if(secondhalf_status == 'COF'){
      $('#secondhalf_status option[value="COF"]').attr("selected", "selected");
    } else if(secondhalf_status == 'PL'){
      $('#secondhalf_status option[value="PL"]').attr("selected", "selected");
    } else if(secondhalf_status == 'CL'){
      $('#secondhalf_status option[value="CL"]').attr("selected", "selected");
    } else if(secondhalf_status == 'SL'){
      $('#secondhalf_status option[value="SL"]').attr("selected", "selected");
    } else if(secondhalf_status == 'SHL'){
      $('#secondhalf_status option[value="SHL"]').attr("selected", "selected");
    } else if(secondhalf_status == 'OD'){
      $('#secondhalf_status option[value="OD"]').attr("selected", "selected");
    }
    //alert(ui.item.shift_id);
    shift_id = ui.item.shift_id;
    //$('#shift_id option[value="'+ shift_id +'"]').attr("selected", "selected").change();
    $('#shift_id option[value="'+ shift_id +'"]').attr("selected", "selected");
    //$('$shift_id').trigger
    
    return false;
  },
  focus: function(event, ui) {
    return false;
  }
});

$('input[name=\'emp_code\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    date = $('#dot').val();
    $.ajax({
      url: 'index.php?route=transaction/manualpunch_ess/autocomplete&token=<?php echo $token; ?>&filter_name_id=' +  encodeURIComponent(request.term) + '&filter_date=' + date,
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.emp_id,
            value: item.emp_id,
            name: item.name,
            actual_intime: item.actual_intime,
            shift_id: item.shift_id,
            actual_outtime: item.actual_outtime,
            actual_intime: item.actual_intime,
            shift_outtime: item.shift_outtime,
            shift_intime: item.shift_intime,
            firsthalf_status: item.firsthalf_status,
            secondhalf_status: item.secondhalf_status,
            insert_stat: item.insert_stat,
            date_from: item.date_from,
            date_to: item.date_to,
            month_close: item.month_close,
            transaction_id: item.transaction_id,
            id: item.id,
            on_duty: item.on_duty
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('#od').attr('checked', false);
    if(ui.item.month_close == 1){
      $('#close_warn').css('display', '');
      return false;
    } else {
      $('#close_warn').css('display', 'none;');
    }

    $('input[name=\'e_name\']').val(ui.item.name);
    $('input[name=\'e_name_id\']').val(ui.item.value);
    $('input[name=\'emp_code\']').val(ui.item.value);
    $('input[name=\'actual_intime\']').val(ui.item.actual_intime);
    $('input[name=\'actual_outtime\']').val(ui.item.actual_outtime);
    $('input[name=\'shift_intime\']').val(ui.item.shift_intime);
    $('input[name=\'shift_outtime\']').val(ui.item.shift_outtime);
    $('input[name=\'insert\']').val(ui.item.insert_stat);
    $('input[name=\'transaction_id\']').val(ui.item.transaction_id);
    $('input[name=\'id\']').val(ui.item.id);
    $('input[name=\'date_from\']').val(ui.item.date_from);
    $('input[name=\'date_to\']').val(ui.item.date_to);
    if(ui.item.actual_intime != '' && ui.item.actual_intime != '00:00:00'){
      //$('#actual_intime').timepicker('disable');
      //$('#actual_outtime').timepicker('disable');
    } else {
      //$('#actual_intime').timepicker('enable');
      //$('#actual_outtime').timepicker('enable');
    }

    $('#hidden_actual_intime').val(ui.item.actual_intime);
    $('#hidden_actual_outtime').val(ui.item.actual_outtime);
    $('#hidden_shift_id').val(ui.item.shift_id);
    $('#shift1_id').val(ui.item.shift_id);

    /*
    on_duty = ui.item.on_duty;
    if(on_duty == 1){
      $('#od').attr('checked', true);
    } else {
      $('#od').attr('checked', false);
    }
    */

    firsthalf_status = ui.item.firsthalf_status;
    if(firsthalf_status == '1'){
      $('#firsthalf_status option[value="1"]').attr("selected", "selected");
    } else if(firsthalf_status == '0') {
      $('#firsthalf_status option[value="0"]').attr("selected", "selected");
    } else if(firsthalf_status == 'WO'){
      $('#firsthalf_status option[value="WO"]').attr("selected", "selected");
    } else if(firsthalf_status == 'HLD'){
      $('#firsthalf_status option[value="HLD"]').attr("selected", "selected");
    } else if(firsthalf_status == 'HD'){
      $('#firsthalf_status option[value="HD"]').attr("selected", "selected");
    } else if(firsthalf_status == 'TOUR'){
      $('#firsthalf_status option[value="TOUR"]').attr("selected", "selected");
    } else if(firsthalf_status == 'COF'){
      $('#firsthalf_status option[value="COF"]').attr("selected", "selected");
    } else if(firsthalf_status == 'PL'){
      $('#firsthalf_status option[value="PL"]').attr("selected", "selected");
    } else if(firsthalf_status == 'CL'){
      $('#firsthalf_status option[value="CL"]').attr("selected", "selected");
    } else if(firsthalf_status == 'SL'){
      $('#firsthalf_status option[value="SL"]').attr("selected", "selected");
    } else if(firsthalf_status == 'OD'){
      $('#firsthalf_status option[value="OD"]').attr("selected", "selected");
    }

    secondhalf_status = ui.item.secondhalf_status;
    if(secondhalf_status == '1'){
      $('#secondhalf_status option[value="1"]').attr("selected", "selected");
    } else if(secondhalf_status == '0') {
      $('#secondhalf_status option[value="0"]').attr("selected", "selected");
    } else if(secondhalf_status == 'WO'){
      $('#secondhalf_status option[value="WO"]').attr("selected", "selected");
    } else if(secondhalf_status == 'HLD'){
      $('#secondhalf_status option[value="HLD"]').attr("selected", "selected");
    } else if(secondhalf_status == 'HD'){
      $('#secondhalf_status option[value="HD"]').attr("selected", "selected");
    } else if(secondhalf_status == 'TOUR'){
      $('#secondhalf_status option[value="TOUR"]').attr("selected", "selected");
    } else if(secondhalf_status == 'COF'){
      $('#secondhalf_status option[value="COF"]').attr("selected", "selected");
    } else if(secondhalf_status == 'PL'){
      $('#secondhalf_status option[value="PL"]').attr("selected", "selected");
    } else if(secondhalf_status == 'CL'){
      $('#secondhalf_status option[value="CL"]').attr("selected", "selected");
    } else if(secondhalf_status == 'SL'){
      $('#secondhalf_status option[value="SL"]').attr("selected", "selected");
    } else if(secondhalf_status == 'OD'){
      $('#secondhalf_status option[value="OD"]').attr("selected", "selected");
    }
    //alert(ui.item.shift_id);
    shift_id = ui.item.shift_id;
    //$('#shift_id option[value="'+ shift_id +'"]').attr("selected", "selected").change();
    $('#shift_id option[value="'+ shift_id +'"]').attr("selected", "selected");
    //$('$shift_id').trigger
    
    return false;
  },
  focus: function(event, ui) {
    return false;
  }
});

$('#type').change(function(){
  type = $(this).val();
  if(type == 1){
    $('.px_record_class').show();
    $('.tour_class').hide();
  } else {
    $('.px_record_class').hide();
    $('.tour_class').show();
  }
});

$('#shift_id').change(function(){
  shift_id = $(this).val();
  dot = $('#dot').val();
  emp_code = $('#emp_code').val();
  $.ajax({
    url: 'index.php?route=transaction/manualpunch/getshiftdata&token=<?php echo $token; ?>&shift_id=' + shift_id+'&transaction_id=' + transaction_id+'&dot=' + dot+'&emp_code=' + emp_code,
    dataType: 'json',
    success: function(json) {  
      if(json.status == 1){
        $('#shift_intime').val(json.shift_data['in_time']);
        $('#shift_outtime').val(json.shift_data['out_time']);
        dot = $('#dot').val();
        $('#date_from').val(dot);
        $('#date_to').val(dot);
      } else {
        $('#shift_intime').val('00:00:00');
        $('#shift_outtime').val('00:00:00');
        dot = $('#dot').val();
        $('#date_from').val(dot);
        $('#date_to').val(dot);
      } 
    }
  });
});

$('#px_record_type').change(function(){
  px_record_type = $(this).val();
  if(px_record_type == 1){
    $('#date_from').attr("readonly", "readonly");
    $('#date_from').timepicker("destroy");
    $('#date_to').removeAttr("readonly");
    $('#date_to').timepicker({timeFormat: 'hh:mm:ss'});

    $('#actual_intime').attr("readonly", "readonly");
    $('#actual_intime').timepicker("destroy");
    $('#actual_outtime').removeAttr("readonly");
    $('#actual_outtime').timepicker({timeFormat: 'hh:mm:ss'});  
  } else if(px_record_type == 2){
    $('#date_to').attr("readonly", "readonly");
    $('#date_to').timepicker("destroy");
    $('#date_from').removeAttr("readonly");
    $('#date_from').timepicker({timeFormat: 'hh:mm:ss'});

    $('#actual_outtime').attr("readonly", "readonly");
    $('#actual_outtime').timepicker("destroy");
    $('#actual_intime').removeAttr("readonly");
    $('#actual_intime').timepicker({timeFormat: 'hh:mm:ss'});
  } else {
    $('#date_from').removeAttr("readonly");
    $('#date_from').timepicker();
    $('#date_to').removeAttr("readonly");
    $('#date_to').timepicker({timeFormat: 'hh:mm:ss'});

    $('#actual_intime').removeAttr("readonly");
    $('#actual_intime').timepicker({timeFormat: 'hh:mm:ss'});
    $('#actual_outtime').removeAttr("readonly");
    $('#actual_outtime').timepicker({timeFormat: 'hh:mm:ss'});
  }
});

$(document).ready(function() {
  px_record_type = $('#px_record_type').val();
  if(px_record_type == 1){
    $('#date_from').attr("readonly", "readonly");
    $('#date_from').timepicker("destroy");
    $('#date_to').removeAttr("readonly");
    $('#date_to').timepicker({timeFormat: 'hh:mm:ss'});

    $('#actual_intime').attr("readonly", "readonly");
    $('#actual_intime').timepicker("destroy");
    $('#actual_outtime').removeAttr("readonly");
    $('#actual_outtime').timepicker({timeFormat: 'hh:mm:ss'});  
  } else if(px_record_type == 2){
    $('#date_to').attr("readonly", "readonly");
    $('#date_to').timepicker("destroy");
    $('#date_from').removeAttr("readonly");
    $('#date_from').timepicker({timeFormat: 'hh:mm:ss'});

    $('#actual_outtime').attr("readonly", "readonly");
    $('#actual_outtime').timepicker("destroy");
    $('#actual_intime').removeAttr("readonly");
    $('#actual_intime').timepicker({timeFormat: 'hh:mm:ss'});
  } else {
    $('#date_from').removeAttr("readonly");
    $('#date_from').timepicker();
    $('#date_to').removeAttr("readonly");
    $('#date_to').timepicker({timeFormat: 'hh:mm:ss'});

    $('#actual_intime').removeAttr("readonly");
    $('#actual_intime').timepicker({timeFormat: 'hh:mm:ss'});
    $('#actual_outtime').removeAttr("readonly");
    $('#actual_outtime').timepicker({timeFormat: 'hh:mm:ss'});
  }

  type = $('#type').val();
  if(type == 1){
    $('.px_record_class').show();
    $('.tour_class').hide();
  } else {
    $('.px_record_class').hide();
    $('.tour_class').show();
  }
});

//--></script>
<?php echo $footer; ?>