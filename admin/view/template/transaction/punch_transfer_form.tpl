<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
    </div>
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
        <div class="box">
        <div class="heading">
            <h1><img src="view/image/user.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
        </div>
        <div class="content">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <table class="form">
                <tr>
                    <td>
                        <span class="required">*</span> <?php echo $column_name; ?>
                    </td>
                    <td>
                        <select name="from_device" id="input-select_cmd">
                            <option value="0">None</option>
                            <!-- echo"<pre>";print_r($devices);exit; -->
                            <?php foreach($devices as $key => $cvalue) {//echo"<pre>";print_r($cvalue);exit; ?>
                                <?php if ($cvalue['device_serial_no'] == $selected_cmd) { ?>
                                    <option value="<?php echo $cvalue['id']; ?>" selected="selected"><?php echo $cvalue['device_serial_no']; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $cvalue['id']; ?>"><?php echo $cvalue['device_serial_no']; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="required">*</span> <?php echo 'Employee Name'; ?>
                    </td>
                    <td>
                        <input  type="text" name="emp_name" id = "emp_id" class = "emp_name" value="<?php echo $emp_name; ?>" />
                        <input type="hidden" name="emp_code" id = "emp_code" value="<?php echo $emp_code; ?>" />
                        
                        <?php if ($error_name) { ?>
                            <span class="error"><?php echo $error_name; ?></span>
                        <?php } ?>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="required">*</span> <?php echo $entry_name; ?>
                    </td>
                    <td>
                        <select name="to_device" id="input-select_cmd">
                            <option value="0">None</option>
                            <?php foreach($devices as $ckey => $pvalue) { //echo"<pre>";print_r($pvalue);exit; ?>
                                <?php if ($pvalue['device_serial_no'] == $select_cmd) { ?>
                                    <option value="<?php echo $pvalue['id']; ?>" selected="selected"><?php echo $pvalue['device_serial_no']; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $pvalue['id']; ?>"><?php echo $pvalue['device_serial_no']; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                </table>
            </form>
        </div>
        </div>
</div> 
<script type="text/javascript"><!--
$('input[name=\'emp_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=transaction/punch_transfer/getemp_name&token=<?php echo $token; ?>&emp_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.emp_name,
            value: item.emp_id,
            device_emp_code: item.device_emp_code
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'emp_name\']').val(ui.item.label);
    // $('input[name=\'emp_code\']').val(ui.item.label.device_emp_code);
    $('#emp_code').val(ui.item.device_emp_code);      
    return false;
  },
  focus: function(event, ui) {
        return false;
    }
});
//--></script>

<?php echo $footer; ?> 