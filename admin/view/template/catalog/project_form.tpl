<?php echo $header; ?>
<div id="content">
  	<div class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<?php echo $breadcrumb['separator']; ?>
		<a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
	<?php } ?>
  	</div>
  	<?php if ($error_warning) { ?>
  		<div class="warning"><?php echo $error_warning; ?></div>
  	<?php } ?>
  	<div class="box">
	<div class="heading">
	  	<h1><img src="view/image/user.png" alt="" /> <?php echo $heading_title; ?></h1>
	  	<div class="buttons">
	  		<a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a>
	  	</div>
	</div>
	<div class="content">
	  	<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
			<table class="form">
		  	<tr>
			<td><span class="required">*</span> <?php echo $entry_name; ?></td>
			<td><input type="text" name="name" style="width:500px;"value="<?php echo $name; ?>" />
				<?php if ($error_name) { ?>
			  		<span class="error"><?php echo $error_name; ?></span>
			  	<?php } ?></td>
		  	</tr>
		  	<tr >
				<td><span class="required">*</span> <?php echo $entry_description; ?></td>
				<td>
					<input type="text" name="description" style="width:500px;" value="<?php echo $description; ?>" />
			  	<?php if ($error_description) { ?>
			  		<span class="error"><?php echo $error_description; ?></span>
			  	<?php } ?></td>
		  	</tr>
		  	<tr>
			<td>
				<span class="required">*</span> <?php echo $entry_address; ?> 
   	 		</td>
			<td>
				<input type="text" name="address" style="width:500px;" value="<?php echo $address; ?>" />
			  	<?php if ($error_address) { ?>
			  		<span class="error"><?php echo $error_address; ?></span>
			  	<?php } ?><a href="https://www.google.com/maps"> Maps</a>
			</td>
		  	</tr>
		   	<tr>
			<td>
				<span class="required">*</span> <?php echo $entry_latitude; ?></td>
			<td>
				<input type="text" name="latitude" style="width:500px;" value="<?php echo $latitude; ?>" />
			  	<?php if ($error_latitude) { ?>
			 		<span class="error"><?php echo $error_latitude; ?></span>
			  	<?php } ?></td>
		  	</tr>
		  	<tr>
			<td>
				<span class="required">*</span> <?php echo $entry_longitude; ?></td>
			<td>
				<input type="text" name="longitude" style="width:500px;" value="<?php echo $longitude; ?>" />
			  	<?php if ($error_longitude) { ?>
			 		<span class="error"><?php echo $error_longitude; ?></span>
			  	<?php } ?></td>
		  	</tr>
		  	</table>
	 	</form>
	</div>
</div>
</div>
<?php echo $footer; ?> 