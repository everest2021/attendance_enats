<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
    </div>
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
        <div class="box">
        <div class="heading">
            <h1><img src="view/image/user.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
        </div>
        <div class="content">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <table class="form">
                <tr>
                <td>
                    <span class="required">*</span> <?php echo $entry_device_serial_no; ?>
                </td>
                <td>
                    <input  type="text" name="device_serial_no" value="<?php echo $device_serial_no; ?>" />
                    <?php if ($error_device_serial_no) { ?>
                        <span class="error"><?php echo $error_device_serial_no; ?></span>
                    <?php } ?>
                    
                </td>
                </tr>
                <tr>
                    <td><span class=""></span> <?php echo "$entry_location"; ?></td>
                    <td><input type="text" name="location" value="<?php echo $location; ?>" size="50" />
                    </td>
                </tr>
                <tr>
                    <td><span class=""></span> <?php echo "$entry_type"; ?></td>
                    <td><input type="text" name="type" value="<?php echo $type; ?>" size="50" />
                    </td>
                </tr>
                <tr>
                    <td><span class=""></span> <?php echo "$entry_time_zone"; ?></td>
                    <td><input type="text" name="time_zone" placeholder="time zone" value="<?php echo $time_zone; ?>"  />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Last Ping : </td>
                        <td><?php echo $punch_date ?><?php echo $punch_time ?></label>
                    
                    </td>
                </tr>
                <tr>
                    <td><?php echo $entry_status; ?></td>
                    <td><label for="status"><?php echo $status ?></label>  

                </td>
                </tr>
                </table>
            </form>
        </div>
        </div>
    </div>
<?php echo $footer; ?> 