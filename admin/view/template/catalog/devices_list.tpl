<?php echo $header; ?>
<div id="content">
  	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
  	</div>
  		<?php if ($error_warning) { ?>
  			<div class="warning"><?php echo $error_warning; ?></div>
  		<?php } ?>
  		<?php if ($success) { ?>
  			<div class="success"><?php echo $success; ?></div>
  		<?php } ?>
  			<div class="box">
			<div class="heading">
	  			<h1><img src="view/image/user.png" alt="" /> <?php echo $heading_title; ?></h1>
	  			<div class="buttons"><a href="<?php echo $insert; ?>" class="button"><?php echo $button_insert; ?></a><a onclick="$('form').submit();" class="button"><?php echo $button_delete; ?></a></div>
			</div>
			<div class="content">
	  			<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
					<table class="list">
		  			<thead>
					<tr>
			  		<td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[device_serial_no*=\'selected\']').attr('checked', this.checked);" /></td>
			  		<td class="left"><?php if ($sort == 'device_serial_no') { ?>
						<a href="<?php echo $sort_device_serial_no; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_device_serial_no; ?></a>
					<?php } else { ?>
						<a href="<?php echo $sort_device_serial_no; ?>"><?php echo $column_device_serial_no; ?></a>
					<?php } ?></td>
			  		<td class="left"><?php if ($sort == 'location') { ?>
						<a href="<?php echo $sort_location; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_location; ?></a>
					<?php } else { ?>
						<a href="<?php echo $sort_location; ?>"><?php echo $column_location; ?></a>
					<?php } ?></td>
			  		<td class="right"><?php if ($sort == 'type') { ?>
			  			<a href="<?php echo $sort_type; ?>" class="<?php echo strtolower($order); ?>"><?php echo $type; ?></a>
			  			<?php } else { ?>
						<a href="<?php echo $sort_type; ?>"><?php echo $column_type; ?></a>
			  		<?php } ?></td>

			  		<td class="right"><?php if ($sort == 'time_zone') { ?>
			  			<a href="<?php echo $sort_time_zone; ?>" class="<?php echo strtolower($order); ?>"><?php echo $time_zone; ?></a>
			  			<?php } else { ?>
						<a href="<?php echo $sort_time_zone; ?>"><?php echo $column_time_zone; ?></a>
			  		<?php } ?></td>
			  		
			  		<td class="left"><?php if ($sort == 'status') { ?>
						<a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
					<?php } else { ?>
						<a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
					<?php } ?></td>
			  		<td class="right"><?php echo $column_action; ?></td>
					</tr>
		  			</thead>
			  			<tbody>
			  			<tr style="display: none;" class="filter">
              			<td></td>

              			<td>
              				<input type="text" id="filter_device_serial_no" name="filter_device_serial_no" value="<?php echo $filter_device_serial_no; ?>" style="width:390px;" />
              			</td>
              			<td></td>
              			<td></td>
              			<td align="right">
              				<a onclick="filter();" class="button"><?php echo 'Filter'; ?></a>
              			</td>
            			</tr>
						<?php if ($devices) { ?>
						<?php foreach ($devices as $devices) { ?>
						<tr>
				  		<td style="text-align: center;"><?php if ($devices['selected']) { ?>
								<input type="checkbox" name="selected[]" value="<?php echo $devices['id']; ?>" checked="checked" />
							<?php } else { ?>
								<input type="checkbox" name="selected[]" value="<?php echo $devices['id']; ?>" />
							<?php } ?>
						</td>
				  		<td class="left"><?php echo $devices['device_serial_no']; ?></td>
				  		<td class="left"><?php echo $devices['location']; ?></td>
				  		<td class="right"><?php echo $devices['type']; ?></td>
				  		<td class="right"><?php echo $devices['time_zone']; ?></td>
				  		<td class="left"><?php echo $devices['status'];?></td>
				  		<td class="right"><?php foreach ($devices['action'] as $action) { ?>
							[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
						<?php } ?></td>
						</tr>
						<?php } ?>
						<?php } else { ?>
						<tr>
				 		 <td class="center" colspan="5"><?php echo $text_no_results; ?></td>
						</tr>
						<?php } ?>
			  			</tbody>
					</table>
	  			</form>
	  				<div class="pagination"><?php echo $pagination; ?></div>
			</div>
  		</div>
	</div>
	<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=catalog/devices&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
  }

  location = url;
  return false;
}
//--></script>
<script type="text/javascript"><!--
$('#filter_name').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
//--></script> 
<script type="text/javascript"><!--
$('input[device_serial_not=\'filter_device_serial_no\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/devices/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[device_serial_no=\'filter_device_serial_no\']').val(ui.item.label);
            
    return false;
  },
  focus: function(event, ui) {
        return false;
    }
});
//--></script>
<?php echo $footer; ?> 