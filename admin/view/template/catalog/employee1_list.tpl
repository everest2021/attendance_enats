<?php echo $header; ?>
<div id="content">
  	<div class="breadcrumb" style="display: none;">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
  	</div>
  	<?php if ($error_warning) { ?>
  		<div class="warning"><?php echo $error_warning; ?></div>
  	<?php } ?>
  	<?php if ($success) { ?>
  		<div class="success"><?php echo $success; ?></div>
  	<?php } ?>
  	<div class="box">
		<div class="heading">
	  		<h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
	  	</div>
		<div class="content" style="overflow-x: hidden !important;">
	  		<div class="dashboard-top">
				<div class="dashboard-bottom">
		  			<div class="dashboard-heading"><?php echo 'Dashboard'; ?></div>
		  			<div class="dashboard-content">
						<div class="dashboard-overview-top clearfix">
			  				<div class="sales-value-graph" style="width: 44%;overflow-y: hidden;display: inline-block;">
								<table border="1" style="border-color: #c3d8f1;width: 95%;text-align: center;margin-left: 5%;">
				  					<tbody>
										<tr>
					  						<td style="padding:10px;font-size:14px;text-align: left;font-weight: bold;">
												Employee Name
					  						</td>
					  						<td style="text-align: left;padding:10px;font-size:14px;font-weight: bold;">
												<?php echo $employee1s['name']; ?>
											</td>	
										</tr>
										<tr>
					  						<td style="padding:10px;font-size:14px;text-align: left;font-weight: bold;">
												Employee Code
					  						</td>
					  						<td style="text-align: left;padding:10px;font-size:14px;font-weight: bold;">
												<?php echo $employee1s['code']; ?>
											</td>	
										</tr>
										<tr>
					  						<td style="padding:10px;font-size:14px;text-align: left;font-weight: bold;">
												Shift
					  						</td>
					  						<td style="text-align: left;padding:10px;font-size:14px;font-weight: bold;">
												<?php echo $employee1s['shift']; ?>
											</td>	
										</tr>
										<tr>
					  						<td style="padding:10px;font-size:14px;text-align: left;font-weight: bold;">
												Total Working Hours
					  						</td>
					  						<td style="text-align: left;padding:10px;font-size:14px;font-weight: bold;">
												<?php echo $employee1s['total_working_hours']; ?>
											</td>	
										</tr>
										<tr>
					  						<td style="padding:10px;font-size:14px;text-align: left;font-weight: bold;">
												Actual Working Hours
					  						</td>
					  						<td style="text-align: left;padding:10px;font-size:14px;font-weight: bold;">
												<?php echo $employee1s['total_actual_working_hours']; ?>
											</td>	
										</tr>
										<tr>
					  						<td style="padding:10px;font-size:14px;text-align: left;font-weight: bold;">
												Pending Hours
					  						</td>
					  						<td style="text-align: left;padding:10px;font-size:14px;font-weight: bold;">
												<?php echo $employee1s['difference_hours']; ?>
											</td>	
										</tr>
										<tr>
					  						<td style="padding:10px;font-size:14px;text-align: left;font-weight: bold;">
												Extra Hours
					  						</td>
					  						<td style="text-align: left;padding:10px;font-size:14px;font-weight: bold;">
												<?php echo $employee1s['extra_hours']; ?>
											</td>	
										</tr>
										<tr>
					  						<td style="padding:10px;font-size:14px;text-align: left;font-weight: bold;">
												Leave Balance
					  						</td>
					  						<td style="text-align: left;padding:10px;font-size:14px;font-weight: bold;">
												<?php echo $employee1s['total_bal_pl']; ?>
											</td>	
										</tr>
										<tr>
					  						<td style="padding:10px;font-size:14px;text-align: left;font-weight: bold;">
												Total Late Count
					  						</td>
					  						<td style="text-align: left;padding:10px;font-size:14px;font-weight: bold;">
												<?php echo $employee1s['total_late_count']; ?>
											</td>	
										</tr>
										<tr>
					  						<td style="padding:10px;font-size:14px;text-align: left;font-weight: bold;">
												Total Early Count
					  						</td>
					  						<td style="text-align: left;padding:10px;font-size:14px;font-weight: bold;">
												<?php echo $employee1s['total_early_count']; ?>
											</td>	
										</tr>
										<tr>
					  						<td style="padding:10px;font-size:14px;text-align: left;font-weight: bold;">
												Total Absent Count
					  						</td>
					  						<td style="text-align: left;padding:10px;font-size:14px;font-weight: bold;">
												<?php echo $employee1s['total_absent_count']; ?>
											</td>	
										</tr>
										<tr>
					  						<td style="padding:10px;font-size:14px;text-align: left;font-weight: bold;">
												In Time
					  						</td>
					  						<td style="text-align: left;padding:10px;font-size:14px;font-weight: bold;">
												<?php echo $employee1s['todays_in_time']; ?>
											</td>	
										</tr>
										<tr>
					  						<td style="padding:10px;font-size:14px;text-align: left;font-weight: bold;">
												Today's Working Hours
					  						</td>
					  						<td style="text-align: left;padding:10px;font-size:14px;font-weight: bold;">
												<?php echo $employee1s['todays_working_hours']; ?>
											</td>	
										</tr>
										<tr>
					  						<td style="padding:10px;font-size:14px;text-align: left;font-weight: bold;">
												Today's Pending Hours
					  						</td>
					  						<td style="text-align: left;padding:10px;font-size:14px;font-weight: bold;">
												<?php echo $employee1s['todays_pending_hours']; ?>
											</td>	
										</tr>
									</tbody>
								</table>
							</div>
							<div class="sales-value-graph" style="width: 55%;overflow-y: hidden;display: inline-block;vertical-align: top;">
								<table border="1" style="border-color: #c3d8f1;width: 95%;text-align: center;margin-left: 5%;">
				  					<tbody>
										<tr>
					  						<td style="padding:10px;font-size:16px;text-align: center;font-weight: bold;" colspan="4">
												Status
					  						</td>
										</tr>
										<tr>
					  						<td style="padding:10px;font-size:14px;text-align: center;font-weight: bold;" colspan="2">
												Team
					  						</td>
					  						<td style="padding:10px;font-size:14px;text-align: center;font-weight: bold;" colspan="2">
												Self
					  						</td>
										</tr>
										<tr>
					  						<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;width: 30%;">
												Leave Approval
											</td>
											<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;">
												<?php echo $employee1s['leave_approval_count']; ?>
											</td>
											<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;width: 30%;">
												Leave Approval
											</td>
											<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;">
												<?php echo $employee1s['self_leave_approval_count']; ?>
											</td>
										</tr>
										<tr>
					  						<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;width: 30%;">
												Early Modification Approval
											</td>
											<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;">
												<?php echo $employee1s['manual_early_count']; ?>
											</td>
											<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;width: 30%;">
												Early Modification Approval
											</td>
											<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;">
												<?php echo $employee1s['self_manual_early_count']; ?>
											</td>
										</tr>
										<tr>
					  						<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;width: 30%;">
												Late Modification Approval
											</td>
											<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;">
												<?php echo $employee1s['manual_late_count']; ?>
											</td>

											<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;width: 30%;">
												Late Modification Approval
											</td>
											<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;">
												<?php echo $employee1s['self_manual_late_count']; ?>
											</td>
										</tr>
										<tr>
					  						<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;width: 30%;">
												Other Modification Approval
											</td>
											<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;">
												<?php echo $employee1s['manual_other_count']; ?>
											</td>

											<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;width: 30%;">
												Other Modification Approval
											</td>
											<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;">
												<?php echo $employee1s['self_manual_other_count']; ?>
											</td>
										</tr>
										<tr>
					  						<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;width: 30%;">
												Tour Approval
											</td>
											<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;">
												<?php echo $employee1s['tour_count']; ?>
											</td>

											<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;width: 30%;">
												Tour Approval
											</td>
											<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;">
												<?php echo $employee1s['self_tour_count']; ?>
											</td>
										</tr>
				  					</tbody>
								</table>

								<table border="1" style="border-color: #c3d8f1;width: 95%;text-align: center;margin-left: 5%;margin-top: 5%;">
				  					<tbody>
										<tr>
					  						<td style="padding:10px;font-size:16px;text-align: center;font-weight: bold;" colspan="4">
												Team Leave Details
					  						</td>
										</tr>
										<tr>
					  						<td style="padding:10px;font-size:12px;text-align: left;font-weight: bold;">
												Employee Name
					  						</td>
					  						<td style="padding:10px;font-size:12px;text-align: left;font-weight: bold;display: none;">
												Leave Type
					  						</td>
					  						<td style="padding:10px;font-size:12px;text-align: left;font-weight: bold;">
												Date From
					  						</td>
					  						<td style="padding:10px;font-size:12px;text-align: left;font-weight: bold;">
												Date To
					  						</td>
					  						<td style="padding:10px;font-size:12px;text-align: left;font-weight: bold;">
												Leave Days
					  						</td>
										</tr>
										<?php foreach($employee1s['team_leave_data'] as $lkey => $lvalue){ ?>
											<tr>
						  						<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;">
													<?php echo $lvalue['emp_name']; ?>
												</td>
												<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;display: none;">
													<?php echo $lvalue['leave_type']; ?>
												</td>
												<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;">
													<?php echo $lvalue['leave_from']; ?>
												</td>
												<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;">
													<?php echo $lvalue['leave_to']; ?>
												</td>
												<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;">
													<?php echo $lvalue['total_leave_days']; ?>
												</td>	
											</tr>
										<?php } ?>
				  					</tbody>
								</table>
							</div>
							<div class="sales-value-graph" style="width: 30%;overflow-y: hidden;vertical-align: top;margin-top: 1%;">
								<table border="1" style="border-color: #c3d8f1;width: 95%;text-align: center;margin-left: 5%;">
				  					<tbody>
										<tr>
					  						<td style="padding:10px;font-size:16px;text-align: center;font-weight: bold;">
												<a href="<?php echo $hr_code_book; ?>" target="_blank">HR Code Book</a>
					  						</td>
										</tr>
									</tbody>
								</table>
							</div>
							<?php if($show == 1){ ?>
								<div class="sales-value-graph" style="width: 30%;overflow-y: hidden;margin-top: 1%;">
									<table border="1" style="border-color: #c3d8f1;width: 95%;text-align: center;margin-left: 5%;">
					  					<tbody>
											<tr>
						  						<td style="padding:10px;font-size:16px;text-align: center;font-weight: bold;" colspan="2">
													<?php echo $employee1s['month_name']; ?>
												</td>
											</tr>
											<tr>
						  						<td style="padding:10px;font-size:12px;text-align: left;font-weight: bold;">
													Employee Name
						  						</td>
						  						<td style="padding:10px;font-size:12px;text-align: left;font-weight: bold;">
													Date
						  						</td>
											</tr>
											<?php foreach($employee1s['birthday_data'] as $lkey => $lvalue){ ?>
												<tr>
							  						<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;">
														<?php echo $lvalue['emp_name']; ?>
													</td>
													<?php if($lvalue['today_birthday'] == '1'){ ?>
														<td class="blink_me" style="text-align: left;padding:10px;font-size:12px;font-weight: bold;color: green;">
															<?php echo $lvalue['dob']; ?>
														</td>
													<?php } else { ?>
														<td style="text-align: left;padding:10px;font-size:12px;font-weight: bold;">
															<?php echo $lvalue['dob']; ?>
														</td>
													<?php } ?>
												</tr>
											<?php } ?>
											<tr>
												<td style="text-align: right;padding:10px;font-size:12px;font-weight: bold;" colspan="2">
													<a href="<?php echo $employee1s['prev_date']; ?>" class="button">Previous</a> 
													&nbsp;&nbsp; | &nbsp;&nbsp;
													<a href="<?php echo $employee1s['next_date']; ?>" class="button">Next</a>
												</td>
											</tr>
					  					</tbody>
									</table>
								</div>
							<?php } ?>
						</div>
		  			</div>
				</div>
	  		</div>
	  		<div class="clear"></div>
		</div>
  	</div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=catalog/employee1&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
	url += '&filter_name=' + encodeURIComponent(filter_name);
  }

  
  location = url;
  return false;
}
//--></script>
<script type="text/javascript"><!--
$('#filter_name, #filter_trainer').keydown(function(e) {
  if (e.keyCode == 13) {
	filter();
  }
});
//--></script> 
<script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
	$.ajax({
	  url: 'index.php?route=catalog/employee1/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
	  dataType: 'json',
	  success: function(json) {   
		response($.map(json, function(item) {
		  return {
			label: item.name,
			value: item.employee_id
		  }
		}));
	  }
	});
  }, 
  select: function(event, ui) {
	$('input[name=\'filter_name\']').val(ui.item.label);
			
	return false;
  },
  focus: function(event, ui) {
		return false;
	}
});
//--></script>
<?php echo $footer; ?>