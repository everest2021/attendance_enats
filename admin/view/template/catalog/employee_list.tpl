<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><?php echo $heading_title; ?></h1>
      <div class="buttons">
          <a href="<?php echo $insert; ?>" class="button" style=""><?php echo $button_insert; ?></a>
          <!-- <a  href="<?php echo $export_depthead; ?>" class="button"><?php echo 'Export'; ?></a> -->
          <a onclick="$('#form').submit();" class="button" ><?php echo $button_delete; ?></a>
      </div>
    </div>
    <div class="content">
        <?php if($reporting_to_name != ''){ ?>
          <h4>Reporting To : <?php echo $reporting_to_name; ?></h4>
        <?php } ?>
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left">
                <?php if ($sort == 'name') { ?>
                  <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                <?php } else { ?>
                  <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                <?php } ?>
              </td>
              <td>
                <?php if ($sort == 'code') { ?>
                  <a href="<?php echo $sort_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Code'; ?></a>
                <?php } else { ?>
                  <a href="<?php echo $sort_code; ?>"><?php echo 'Code'; ?></a>
                <?php } ?>
              </td>
              <td style="display:none;">
                <?php if ($sort == 'department') { ?>
                  <a href="<?php echo $sort_department; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Department'; ?></a>
                <?php } else { ?>
                  <a href="<?php echo $sort_department; ?>"><?php echo 'Department'; ?></a>
                <?php } ?>
              </td>
              <td style="display:none;">
                <?php if ($sort == 'unit') { ?>
                  <a href="<?php echo $sort_unit; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Location'; ?></a>
                <?php } else { ?>
                  <a href="<?php echo $sort_unit; ?>"><?php echo 'Location'; ?></a>
                <?php } ?>
              </td>
              
              <td>
                <?php if ($sort == 'status') { ?>
                  <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Status'; ?></a>
                <?php } else { ?>
                  <a href="<?php echo $sort_status; ?>"><?php echo 'Status'; ?></a>
                <?php } ?>
              </td>
              <td class="right"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr style="display: none;"  class="filter">
              <td>&nbsp;</td>
              <td>
                <input type="text" id="filter_name" name="filter_name" value="<?php echo $filter_name; ?>"  style="width:210px;" />
              </td>
              <td>
                <input type="text" id="filter_code" name="filter_code" value="<?php echo $filter_code; ?>"  style="width:210px;" />
              </td>
              <td style="display:none;" >
                <select name="filter_department">
                  <option value="0">All</option>
                  <?php foreach($departments as $gkey => $gvalue) { ?>
                    <?php if ($gkey == $filter_department) { ?>
                      <option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td style="display:none;">
                <select name="filter_unit">
                  <option value="0">All</option>
                  <?php foreach($units as $gkey => $gvalue) { ?>
                    <?php if ($gkey == $filter_unit) { ?>
                      <option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
             
              <td>
                <select name="filter_status">
                  <option value="0">All</option>
                  <?php foreach($statuses as $skey => $svalue) { ?>
                    <?php if ($skey == $filter_status) { ?>
                      <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td align="right"><a onclick="filter();" class="button"><?php echo $button_filter; ?></a></td>
            </tr>
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
              <?php if ($employees) { ?>
              <?php foreach ($employees as $employee) { ?>
              <tr>
                <td style="text-align: center;"><?php if ($employee['selected']) { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $employee['employee_id']; ?>" checked="checked" />
                  <?php } else { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $employee['employee_id']; ?>" />
                  <?php } ?></td>
                <td class="left"><?php echo $employee['name']; ?></td>
                <td class="left"><?php echo $employee['code']; ?></td>
               
                <td class="left"><?php echo $employee['status']; ?></td>
                
                <td class="right"><?php foreach ($employee['action'] as $action) { ?>
                  [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                  <?php } ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="center" colspan="11"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </form>
          </tbody>
        </table>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=catalog/employee&token=<?php echo $token; ?>';
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
  }

  var filter_code = $('input[name=\'filter_code\']').attr('value');
  if (filter_code) {
    url += '&filter_code=' + encodeURIComponent(filter_code);
  }

  var filter_department = $('select[name=\'filter_department\']').attr('value');
  if (filter_department) {
    url += '&filter_department=' + encodeURIComponent(filter_department);
  }

  var filter_unit = $('select[name=\'filter_unit\']').attr('value');
  if (filter_unit) {
    url += '&filter_unit=' + encodeURIComponent(filter_unit);
  }

  // var filter_shift = $('select[name=\'filter_shift\']').attr('value');
  // if (filter_shift) {
  //   url += '&filter_shift=' + encodeURIComponent(filter_shift);
  // }

  var filter_status = $('select[name=\'filter_status\']').attr('value');
  if (filter_status) {
    url += '&filter_status=' + encodeURIComponent(filter_status);
  }

  location = url;
  return false;
}
//--></script>
<script type="text/javascript"><!--
$('#filter_name, #filter_trainer').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
//--></script> 
<script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/employee/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.employee_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
            
    return false;
  },
  focus: function(event, ui) {
        return false;
    }
});
//--></script>
<script type="text/javascript"><!--
$('input[name=\'filter_trainer\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/employee/autocomplete_trainer&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.trainer_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_trainer\']').val(ui.item.label);
    $('input[name=\'filter_trainer_id\']').val(ui.item.value);
            
    return false;
  },
  focus: function(event, ui) {
        return false;
    }
});
//--></script>
<?php echo $footer; ?>