<?php echo $header; ?>
<div id="content">
  	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?>
			<a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
  	</div>
  	<?php if ($error_warning) { ?>
  		<div class="warning"><?php echo $error_warning; ?></div>
  	<?php } ?>
  	<div class="box">
		<div class="heading">
	  		<h1><img src="" alt="" /> <?php echo $heading_title; ?></h1>
	  	<div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
		</div>
		<div class="content">
	  		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
				<table class="form">
		  			<tr>
						<td>
							<span class="required">*</span> <?php echo "Name"; ?>
						</td>
	            		<td class="left">
	            			<input type="text" name="emp_name" value="<?php echo $emp_name; ?>"  />
	            			<input type="hidden" name="emp_code" value="<?php echo $emp_code; ?>"  />
		            		<?php if ($error_name) { ?>
								<span class="error"><?php echo $error_name; ?></span>
							<?php } ?>				
	            		</td>
					</tr>
		   			<tr>
						<td>
							<span class="required">*</span> <?php echo "Date"; ?>
						</td>
						<td>
							<input type="text" name="date" value="<?php echo $date; ?>" class="form-control date" />
							<?php if ($error_date) { ?>
								<span class="error"><?php echo $error_date; ?></span>
							<?php } ?>			  			
						</td>
		  			</tr>
		   			<tr>
						<td>
							<span class="required">*</span> <?php echo "Amount"; ?>
						</td>
						<td>
							<input type="text" name="amount" value="<?php echo $amount; ?>" placeholder="<?php echo $amount ; ?>"  />
							<?php if ($error_amount) { ?>
								<span class="error"><?php echo $error_amount; ?></span>
							<?php } ?>
			  			</td>
		  			</tr>
				</table>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
$(document).ready(function() {
  $(function() {
           $('.date').datepicker({
            dateFormat: 'dd-mm-yy',
      });    
  });
  
});
//--></script>
<script type="text/javascript"><!--
$.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
    var self = this, currentCategory = '';
    $.each(items, function(index, item) {
      if (item.category != currentCategory) {
        //ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
        currentCategory = item.category;
      }
      self._renderItem(ul, item);
    });
  }
});

$('input[name=\'emp_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
    $.ajax({
      url: 'index.php?route=catalog/employee/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term)+'&filter_date_start='+filter_date_start,
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.emp_code
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'emp_name\']').val(ui.item.label);
    $('input[name=\'emp_code\']').val(ui.item.value);
    return false;
  },
  focus: function(event, ui) {
    return false;
  }
});
//--></script>
<?php echo $footer; ?> 