<?php echo $header; ?>
<div id="content">
  	<div class="breadcrumb">
    	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
   	 		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    	<?php } ?>
  	</div>
  	<?php if ($error_warning) { ?>
  	<div class="warning"><?php echo $error_warning; ?></div>
  		<?php } ?>
  			<?php if ($success) { ?>
  			<div class="success"><?php echo $success; ?></div>
  		<?php } ?>
  		<div class="box">
    	<div class="heading">
      		<h1><img src="view/image/user.png" alt="" /> <?php echo $heading_title; ?></h1>
      		<div class="buttons"><a href="<?php echo $insert; ?>" class="button"><?php echo $button_insert; ?></a><a onclick="$('form').submit();" class="button"><?php echo $button_delete; ?></a></div>
    	</div>
    	<div class="content">
      		<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        		<table class="list">
          			<thead>
            			<tr>
              				<td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              				<td width="500" class="left"><?php if ($sort == 'name') { ?>
                				<a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                			<?php } else { ?>
                				<a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                			<?php } ?></td>
            				<td class="left"><?php echo $column_latitude; ?></td>
                            <td class="left"><?php echo $column_longitude; ?></td>
              				<td class="right"><?php echo $column_action; ?></td>
           		 		</tr>
          			</thead>
          			<tbody>
                        <tr class="filter">
                        <td>&nbsp;</td>
                        <td>
                            <input type="text" id="filter_name" name="filter_name" value="<?php echo $filter_name; ?>"  style="width:210px;" />
                        </td><td></td><td></td>
                         <td align="right"><a onclick="filter();" class="button"><?php echo $button_filter; ?></a></td>
                        </tr>
            		    <?php if ($projects) { ?>
            			<?php foreach ($projects as $project) { ?>
            			<tr>
              			<td style="text-align: center;"><?php if ($project['selected']) { ?>
               				<input type="checkbox" name="selected[]" value="<?php echo $project['id']; ?>" checked="checked" />
                		<?php } else { ?>
                			<input type="checkbox" name="selected[]" value="<?php echo $project['id']; ?>" />
                		<?php } ?></td>
              			<td class="left"><?php echo $project['name']; ?></td>
		               	<td class="left"><?php echo $project['latitude']; ?></td>
                        <td class="left"><?php echo $project['longitude']; ?></td>          
		              	<td class="right"><?php foreach ($project['action'] as $action) { ?>
		                	[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
		                <?php } ?></td>
		            	</tr>
		           	 	<?php } ?>
		            	<?php } else { ?>
		            	<tr>
		              		<td class="center" colspan="5"><?php echo $text_no_results; ?></td>
		            	</tr>
		            	<?php } ?>
		          	</tbody>
		       		</table>
		      	</form>
      			<div class="pagination"><?php echo $pagination; ?>
      			</div>
    		</div>
  		</div>
	</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=catalog/project&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
  }

  location = url;
  return false;
}
//--></script>
<script type="text/javascript"><!--
$('#filter_name').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
//--></script> 
<script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/project/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
            
    return false;
  },
  focus: function(event, ui) {
        return false;
    }
});
//--></script>
<?php echo $footer; ?> 