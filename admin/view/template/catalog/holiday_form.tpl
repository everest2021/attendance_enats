<?php echo $header; ?>
<div id="content">
	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
	</div>
	<?php if ($error_warning) { ?>
		<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
	<?php if ($success) { ?>
		<div class="success"><?php echo $success; ?></div>
	<?php } ?>
	<div class="box">
		<div class="heading">
			<h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
			<div class="buttons">
				<a onclick="$('#form').submit();" class="button"><?php echo 'Save'; ?></a>
				<?php if($process_holiday != ''){ ?>
					<a href="<?php echo $process_holiday; ?>" class="button"><?php echo 'Process Holiday'; ?></a>
				<?php } ?>
				<a href="<?php echo $cancel; ?>" class="button"><?php echo 'Cancel'; ?></a>
			</div>
		</div>
		<div class="content">
			<div id="tabs" class="htabs"><a href="#tab-general"><?php echo 'General'; ?></a></div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
				<div id="tab-general">
					<table class="form">
						<tr>	
							<td><span class="required">*</span> <?php echo 'Name'; ?></td>
							<td>
								<input type="text" name="name" value="<?php echo $name; ?>" size="100" />
								<?php if ($error_name) { ?>
									<span class="error"><?php echo $error_name; ?></span>
								<?php } ?>
							</td>	
						</tr>
						<tr>
							<td><span class="required">*</span> <?php echo 'Date'; ?></td>
							<td>
								<input type="text" id="date" name="date" value="<?php echo $date; ?>" size="100" readonly="readonly" style="cursor:default;background: transparent;" />
								<?php if ($error_date) { ?>
									<span class="error"><?php echo $error_date; ?></span>
								<?php } ?>
							</td>
						</tr>
						<tr>
						  	<td colspan="2" style="text-align: right;">
						   	<a onclick="$('.checkboxclass').attr('checked', true);"><?php echo 'Select All'; ?></a> / <a onclick="$('.checkboxclass').attr('checked', false);"><?php echo 'UnSelect All'; ?></a>
						  	</td>
						</tr>
						<?php if ($unit_data) { ?>
							<?php foreach ($unit_data as $ukey => $unit) { ?>
								<tr class="unit_tr" style="display: none;">
									<td>
										<?php echo $unit['name']; ?> : 
									</td>
									<td> 
									<div class="scrollbox" style="position:absolute;left:-999px;">
										<?php $class = 'even'; ?>
											<?php foreach ($department_data as $dkey => $dvalue) { ?>
											<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
											<div class="<?php echo $class; ?>">
												<?php if (array_key_exists($dkey, $holi_datas[$ukey])) { ?>
													<input class="checkboxclass" type="checkbox" name="holi_datas[<?php echo $ukey; ?>][<?php echo $dkey; ?>]" value="<?php echo $dvalue; ?>" checked="checked" />
												<?php echo $dvalue; ?>
												<?php } else { ?>
													<input class="checkboxclass" type="checkbox" name="holi_datas[<?php echo $ukey; ?>][<?php echo $dkey; ?>]" value="<?php echo $dvalue; ?>" />
												<?php echo $dvalue; ?>
												<?php } ?>
											</div>
										<?php } ?>
									</div>	
										<?php if($unit['selected'] == '1'){ ?>
										  	<input onclick="return false;" readonly="readonly" class="checkboxclass" type="checkbox" value="1" checked="checked" />
										<?php } else { ?>
										  	<input onclick="return false;" readonly="readonly" class="checkboxclass" type="checkbox" value="1" />
										<?php } ?>
											<a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo 'Select'; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo 'UnSelect'; ?></a>
									</td>
								</tr>
							<?php } ?>
						<?php } ?>
					</table>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript">
$('#tabs a').tabs(); 

$('.time').timepicker({timeFormat: 'h:m'});
</script> 
<script type="text/javascript">
$(document).ready(function() {
  $('#date').datepicker({dateFormat: 'yy-mm-dd'});
  $('.checkboxclass').attr('checked', true);
});
</script>
<?php echo $footer; ?>