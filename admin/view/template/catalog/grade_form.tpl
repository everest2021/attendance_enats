<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <div id="tabs" class="htabs">
        <a href="#tab-general"><?php echo $tab_general; ?></a>
      </div>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general">
          <table class="form">
            <tr>
              <td><?php echo 'Name'; ?></td>
              <td><input tabindex="1" type="text" name="g_name" value="<?php echo $g_name; ?>" /></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td><?php echo 'Number of Days'; ?></td>
              <td><input tabindex="2" type="text" name="no_days" id="no_days" value="<?php echo $no_days; ?>" /></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" style="font-weight: bold;"><?php echo 'Earning :'; ?></td>
              <td colspan="2" style="font-weight: bold;"><?php echo 'Deduction :'; ?></td>
            </tr>
            <tr>
              <td><?php echo 'Basic'; ?></td>
              <td><input tabindex="3" type="text" name="basic" id="basic" value="<?php echo $basic; ?>" /></td>
              <td><?php echo 'Provident Fund (P.F)'; ?></td>
              <td><input tabindex="10" type="text" name="pf" id="pf" value="<?php echo $pf; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo 'Dearness Allowance (D.A)'; ?></td>
              <td><input tabindex="4" type="text" name="da" id="da" value="<?php echo $da; ?>" /></td>
              <td><?php echo 'Employees State Insurance (E.S.I.C)'; ?></td>
              <td><input tabindex="11" type="text" name="esi" id="esi" value="<?php echo $esi; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo 'H.R.A'; ?></td>
              <td><input tabindex="5" type="text" name="hra" id="hra" value="<?php echo $hra; ?>" /></td>
              <td><?php echo 'Professional Tax (P.T)'; ?></td>
              <td><input tabindex="12" type="text" name="pt" id="pt" value="<?php echo $pt; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo 'Transport/Conveyance Allowance'; ?></td>
              <td><input tabindex="6" type="text" name="ta" id="ta" value="<?php echo $ta; ?>" /></td>
              <td><?php echo 'Income Tax (I.T)'; ?></td>
              <td><input tabindex="13" type="text" name="income_tax" id="income_tax" value="<?php echo $income_tax; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo 'Education Allowance'; ?></td>
              <td><input tabindex="7" type="text" name="ea" id="ea" value="<?php echo $ea; ?>" /></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td><?php echo 'Medical Allowance'; ?></td>
              <td><input tabindex="8" type="text" name="medical_allowance" id="medical_allowance" value="<?php echo $medical_allowance; ?>" /></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td><?php echo 'Other Allowance'; ?></td>
              <td><input tabindex="9" type="text" name="other_allowance" id="other_allowance" value="<?php echo $other_allowance; ?>" /></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td><?php echo 'Earning Total'; ?></td>
              <td><input readonly="readonly" type="text" name="earning_total" id="earning_total" value="<?php echo $earning_total; ?>" /></td>
              <td><?php echo 'Total Deduction'; ?></td>
              <td><input readonly="readonly" type="text" name="deduction_total" id="deduction_total" value="<?php echo $deduction_total; ?>" /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td><?php echo 'Net Total'; ?></td>
              <td><input readonly="readonly" type="text" name="net_total" id="net_total" value="<?php echo $net_total; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_status; ?></td>
              <td><select tabindex="14" name="status" id="status">
                  <?php if ($status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
          </table>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
$("#basic, #hra, #da, #ta, #ea, #pf, #esi, #pt, #status, #deduction_total, #net_total, #earning_total, #income_tax, #other_allowance, #medical_allowance").focusout(function(){
  basic = parseFloat($('#basic').val());
  hra = parseFloat($('#hra').val());
  da = parseFloat($('#da').val());
  ta = parseFloat($('#ta').val());
  ea = parseFloat($('#ea').val());
  ma = parseFloat($('#medical_allowance').val());
  oa = parseFloat($('#other_allowance').val());

  pf = parseFloat($('#pf').val());
  esi = parseFloat($('#esi').val());
  pt = parseFloat($('#pt').val());
  it = parseFloat($('#income_tax').val());

  earning_total = basic + hra + da + ta + ea + ma + oa;
  deduction_total = pf + esi + pt + it;

  console.log(pf);
  console.log(esi);
  console.log(pt);
  console.log(it);

  $('#earning_total').val(earning_total);
  $('#deduction_total').val(deduction_total);

  $('#net_total').val(earning_total - deduction_total);
});
//--></script> 
<?php echo $footer; ?>