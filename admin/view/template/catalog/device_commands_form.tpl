<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
    </div>
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
        <div class="box">
        <div class="heading">
            <h1><img src="view/image/user.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
        </div>
        <div class="content">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <table class="form">
                <tr>
                <td>
                    <span class="required">*</span> <?php echo $entry_device_serial_no; ?>
                </td>
                <td>
                    <input  type="text" name="device_serial_no" value="<?php echo $device_serial_no; ?>" />
                    <?php if ($error_device_serial_no) { ?>
                        <span class="error"><?php echo $error_device_serial_no; ?></span>
                    <?php } ?>
                </td>
                </tr>
                <tr>
                    <td><span class="required">*</span> <?php echo "Employee Code"; ?></td>
                    <td>
                        <input type="text" name="emp_code" value="<?php echo $emp_code; ?>" />
                    </td>
                     <?php if ($emp_code_error) { ?>
                        <span class="error"><?php echo $emp_code_error; ?></span>
                    <?php } ?>
                </tr>

                <tr>
                    <td><span class="required">*</span> <?php echo "Select Command"; ?></td>
                    <td>
                        <select name="select_cmd" id="input-select_cmd">
                            <option value="0">None</option>
                            <?php foreach($cmds as $ckey => $cvalue) { ?>
                                <?php if ($ckey == $select_cmd) { ?>
                                    <option value="<?php echo $ckey; ?>" selected="selected"><?php echo $cvalue; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $ckey; ?>"><?php echo $cvalue; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        
                    </td>
               <!-- <tr>
                    <td><span class=""></span> <?php echo "command"; ?></td>
                    <td><input type="text" name="command" value="<?php echo $command; ?>" size="50" />
                    </td>
                </tr> -->
                <tr style="display: none">
                <td><?php echo $entry_status; ?></td>
                <td><select name="status">
                    <?php if ($status) { ?>
                        <option value="0"><?php echo $text_disabled; ?></option>
                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <?php } else { ?>
                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                        <option value="1"><?php echo $text_enabled; ?></option>
                    <?php } ?>
                    </select>
                </td>
                </tr>
                </table>
            </form>
        </div>
        </div>
    </div>
<?php echo $footer; ?> 