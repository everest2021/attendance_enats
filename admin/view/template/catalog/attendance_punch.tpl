<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><?php echo $heading_title; ?></h1>
      <div class="buttons">
          <!-- <a href="<?php echo $insert; ?>" class="button" style=""><?php echo $button_insert; ?></a> -->
          <!-- <a  href="<?php echo $export_depthead; ?>" class="button"><?php echo 'Export'; ?></a> -->
          <!-- <a onclick="$('#form').submit();" class="button" ><?php echo $button_delete; ?></a> -->
      </div>
    </div>
    <div class="content">
        <?php if($reporting_to_name != ''){ ?>
          <h4>Reporting To : <?php echo $reporting_to_name; ?></h4>
        <?php } ?>
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left">
                
                  <?php echo "Employee ID " ?>
               
              </td>
              <td class="left">
                
                  <?php echo "Device Name" ?>
               
              </td>
              <td>
                
                  <?php echo 'Punch Date'; ?>
              </td>
              
              <td>
                  <?php echo 'Punch Time'; ?>
                
              </td>
             
            </tr>
          </thead>
          <tbody>
            
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
              <?php if ($employees) { ?>
              <?php foreach ($employees as $employee) { ?>
              <tr>
                <td style="text-align: center;"><?php if ($employee['selected']) { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $employee['employee_id']; ?>" checked="checked" />
                  <?php } else { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $employee['employee_id']; ?>" />
                  <?php } ?></td>
                <td class="left"><?php echo $employee['employee_id']; ?></td>

                <td class="left"><?php echo $employee['device_id']; ?></td>
                <td class="left"><?php echo $employee['punch_date']; ?></td>
               
                <td class="left"><?php echo $employee['punch_time']; ?></td>
                
                
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="center" colspan="11"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </form>
          </tbody>
        </table>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=catalog/employee&token=<?php echo $token; ?>';
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
  }

  var filter_code = $('input[name=\'filter_code\']').attr('value');
  if (filter_code) {
    url += '&filter_code=' + encodeURIComponent(filter_code);
  }

  var filter_department = $('select[name=\'filter_department\']').attr('value');
  if (filter_department) {
    url += '&filter_department=' + encodeURIComponent(filter_department);
  }

  var filter_unit = $('select[name=\'filter_unit\']').attr('value');
  if (filter_unit) {
    url += '&filter_unit=' + encodeURIComponent(filter_unit);
  }

  // var filter_shift = $('select[name=\'filter_shift\']').attr('value');
  // if (filter_shift) {
  //   url += '&filter_shift=' + encodeURIComponent(filter_shift);
  // }

  var filter_status = $('select[name=\'filter_status\']').attr('value');
  if (filter_status) {
    url += '&filter_status=' + encodeURIComponent(filter_status);
  }

  location = url;
  return false;
}
//--></script>
<script type="text/javascript"><!--
$('#filter_name, #filter_trainer').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
//--></script> 
<script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/employee/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.employee_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
            
    return false;
  },
  focus: function(event, ui) {
        return false;
    }
});
//--></script>
<script type="text/javascript"><!--
$('input[name=\'filter_trainer\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/employee/autocomplete_trainer&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.trainer_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_trainer\']').val(ui.item.label);
    $('input[name=\'filter_trainer_id\']').val(ui.item.value);
            
    return false;
  },
  focus: function(event, ui) {
        return false;
    }
});
//--></script>
<?php echo $footer; ?>