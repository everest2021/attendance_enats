<?php echo $header; ?>
<div id="content">
  	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
  	</div>
  	<?php if ($error_warning) { ?>
  		<div class="warning"><?php echo $error_warning; ?></div>
  	<?php } ?>
  	<div class="box">
		<div class="heading">
	  		<h1><img src="" alt="" /> <?php echo $heading_title; ?></h1>
	  		<div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
		</div>
		<div class="content">
	  		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
				<table class="form">
		  			<tr>
						<td>
						<span class=""></span> <?php echo "Project Name"; ?></td>
            			<td>
                			<select name="project_id" id="input-project_id">
                  				<option value="0">Please Select </option>
                  				<?php foreach($project_datas as $skey => $svalue) { ?>
                    			<?php if ($skey == $project_id) { ?>
                      				<option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                    			<?php } else { ?>
                      				<option value="<?php echo $skey; ?>" ><?php echo $svalue; ?></option>
                    			<?php } ?>
                			<?php } ?>
               				</select>
               				<input type="hidden" name="project_transaction_id" value="0" id="input-project_transaction_id" />
            			</td>
					</tr>
		   			<tr>
						<td>
						<span class="required">*</span> <?php echo $entry_start_date; ?></td>
						<td>
							<input type="text" name="start_date" value="<?php echo $start_date; ?>" placeholder="<?php echo $entry_start_date; ?>" id="input-start_date" class="form-control date" />
			  				<input type="hidden" name="hidden_start_date" value="<?php echo $hidden_start_date; ?>" id="input-hidden_start_date" class="form-control" />
			  				<?php if ($error_start_date) { ?>
			  					<span class="error"><?php echo $error_start_date; ?></span>
			  				<?php  } ?>
						</td>
		  			</tr>
		   			<tr>
						<td>
							<span class="required">*</span> <?php echo $entry_end_date; ?></td>
						<td>
							<input type="text" name="end_date" value="<?php echo $end_date; ?>" placeholder="<?php echo $entry_end_date; ?>" id="input-end_date" class="form-control date" />
							<input type="hidden" name="hidden_end_date" value="<?php echo $hidden_end_date; ?>" id="input-hidden_end_date" class="form-control" />
			  				<?php if ($error_end_date) { ?>
			  					<span class="error"><?php echo $error_end_date; ?></span>
			  				<?php  } ?>
			  			</td>
		  			</tr>
		  			<tr>
            			<td><?php echo 'Employee List'; ?></td>
            			<td>
            				<div class="scrollbox">
            				<?php $class = 'even'; ?>
            				<?php foreach ($employee_data as $ekey => $evalue) { ?>
            				<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
            				<div class="<?php echo $class; ?>">
            				<?php if (in_array($ekey, $employee_list)) { ?>
                				<input type="checkbox" name="employee_list[]" value="<?php echo $ekey; ?>"  checked="checked" />
            				<?php echo $evalue; ?>
            				<?php } else { ?>
                				<input type="checkbox" name="employee_list[]" value="<?php echo $ekey; ?>"  />
            				<?php echo $evalue; ?>
            				<?php } ?>
            				</div>
            				<?php } ?>
            				</div>
   			  				<a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / 
			  				<a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a>
			  			</td>
		  			</tr>
				</table>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
$(document).ready(function() {
  	$('.date').datepicker({dateFormat: 'dd-mm-yy',
		onSelect: function(date, instance) {
			d_id = $(this).attr('id');
			if(d_id == 'input-start_date'){
				start_date = $('#input-start_date').val();
		  		$('#input-hidden_start_date').val(start_date);
			} else {
				end_date = $('#input-end_date').val();
		  		$('#input-hidden_end_date').val(end_date);
			}
		}
	});
});

$('#input-project_id').on('change', function() {
	project_name = $('#input-project_id option:selected').text();
	project_id = $('#input-project_id').val();
	$.ajax({
		url: 'index.php?route=catalog/project_assignment/getproject_data&token=<?php echo $token; ?>&filter_project_id=' +  encodeURIComponent(project_id),
		dataType: 'json',
		success: function(json) {   
			if(json['exist'] == 1){
				$("#input-start_date").val(json['start_date']);
				$("#input-hidden_start_date").val(json['start_date']);
				$('#input-start_date').datepicker("disable");
				$("#input-end_date").val(json['end_date']);
				$("#input-hidden_end_date").val(json['end_date']);
				$('#input-end_date').datepicker("disable");
				$("#input-project_transaction_id").val(json['project_transaction_id']);
			} else {
				$('#input-start_date').datepicker("enable");
				$('#input-end_date').datepicker("enable");
			}
		}
	});
});

//--></script>
<?php echo $footer; ?>