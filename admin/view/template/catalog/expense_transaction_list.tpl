<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?>
        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
</div>
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>
    <div class="box">
    <div class="heading">
        <h1>
            <img src="" alt="" /> <?php echo $heading_title; ?>
        </h1>
    <div class="buttons">
        <a href="<?php echo $insert; ?>" class="button"><?php echo $button_insert; ?></a>
        <a onclick="$('form').submit();" class="button"><?php echo $button_delete; ?></a>
    </div>
    </div>
    <div class="content">
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
            <table class="list">
                <thead>
                    <tr>
                        <td width="1" style="text-align: center;">
                            <input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
                        <td class="left">
                        <?php if ($sort == 'name') { ?>
                            <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                        <?php } ?>
                        </td>
                        <td class="left">
                            <?php if ($sort == 'date') { ?>
                                <a href="<?php echo $sort_date; ?>" class="<?php echo strtolower($order); ?>"><?php echo "Date"; ?></a>
                            <?php } else { ?>
                                <a href="<?php echo $sort_date; ?>"><?php echo "Date"; ?></a>
                            <?php } ?>
                        </td>
                        <td class="left"><?php echo "Amount"; ?></td>
                        <td class="right"><?php echo $column_action; ?></td>
                    </tr>
                </thead>
            <tbody>
            <tr>
            <td></td>
            <td class="left"><input type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" /></td>
                <input type="hidden" name="filter_id" value="<?php echo $filter_id; ?>" id="filter_id" size="20" /></td>
            <td style="">
                <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start"/>
                <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" id="date-end" />
            </td>
            <td></td>
            <td class="right"><a style="" onclick="filter();" id="filter" class="button"><?php echo "Filter"; ?></a>
            </td>
            </tr>
                <?php if ($expense_transactions) { ?>
                <?php foreach ($expense_transactions as $expense_transaction) { ?>
            <tr>
                <td style="text-align: center;">
                <?php if ($expense_transaction['selected']) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $expense_transaction['id']; ?>" checked="checked" />
                <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $expense_transaction['id']; ?>" />
                <?php } ?>
                </td>
                <td class="left"><?php echo $expense_transaction['emp_name']; ?></td>
                <td class="left"><?php echo $expense_transaction['date']; ?></td>
                <td class="left"><?php echo $expense_transaction['amount']; ?></td>
                <td class="right"><?php foreach ($expense_transaction['action'] as $action) { ?>
                    [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                <?php } ?>
                </td>
            </tr>
                <?php } ?>
                <?php } else { ?>
            <tr>
                <td class="center" colspan="5"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
            </tbody>
            </table>
        </form>
        <div class="pagination"><?php echo $pagination; ?>
        </div>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=catalog/expense_transaction&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_id = $('input[name=\'filter_id\']').attr('value');
    if (filter_id) {
      url += '&filter_id=' + encodeURIComponent(filter_id);
    } else {
      alert('Please Enter Correct  Name');
      return false;
    }
  }

  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  
  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  location = url;
  return false;
}
//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
  $(function() {
           $('#date-start').datepicker({
            dateFormat: 'dd-mm-yy',
      });
      
      $('#date-end').datepicker({
            dateFormat: 'dd-mm-yy',
          
      });
      
  });
  
});
//--></script>
<script type="text/javascript"><!--

$.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
    var self = this, currentCategory = '';
    $.each(items, function(index, item) {
      if (item.category != currentCategory) {
        //ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
        currentCategory = item.category;
      }
      self._renderItem(ul, item);
    });
  }
});

$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
    $.ajax({
      url: 'index.php?route=catalog/employee/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term)+'&filter_date_start='+filter_date_start,
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.emp_code
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    $('input[name=\'filter_id\']').val(ui.item.value);
    return false;
  },
  focus: function(event, ui) {
    return false;
  }
});
//--></script>
<?php echo $footer; ?> 