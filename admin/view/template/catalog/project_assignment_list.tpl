<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
</div>
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>
    <div class="box">
    <div class="heading">
        <h1><img src="" alt="" /> <?php echo $heading_title; ?></h1>
        <div class="buttons"><a href="<?php echo $insert; ?>" class="button"><?php echo $button_insert; ?></a><a onclick="$('form').submit();" class="button"><?php echo $button_delete; ?></a></div>
    </div>
    <div class="content">
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
            <table class="list">
                <thead>
                    <tr>
                        <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
                        <td class="left"><?php if ($sort == 'project_name') { ?>
                            <a href="<?php echo $sort_project_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_project_name; ?>"><?php echo $column_name; ?></a>
                        <?php } ?></td>
                        <td class="left"><?php echo $column_start_date; ?></td>
                        <td class="left"><?php echo $column_end_date; ?></td>
                        <td class="right"><?php echo $column_action; ?></td>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($project_assignments) { ?>
                    <?php foreach ($project_assignments as $project_assignment) { ?>
                        <tr>
                            <td style="text-align: center;">
                                <?php if(!empty($project_assignment['action'])){ ?>
                                    <?php if ($project_assignment['selected']) { ?>
                                        <input type="checkbox" name="selected[]" value="<?php echo $project_assignment['id']; ?>" checked="checked" />
                                    <?php } else { ?>
                                        <input type="checkbox" name="selected[]" value="<?php echo $project_assignment['id']; ?>" />
                                    <?php } ?>
                                <?php } ?>
                            </td>
                            <td class="left"><?php echo $project_assignment['project_name']; ?></td>
                            <td class="left"><?php echo $project_assignment['start_date']; ?></td>
                            <td class="left"><?php echo $project_assignment['end_date']; ?></td>
                            <td class="right"><?php foreach ($project_assignment['action'] as $action) { ?>
                                [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                            <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                <tr>
                    <td class="center" colspan="5"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
        </form>
            <div class="pagination"><?php echo $pagination; ?></div>
        </div>
    </div>
</div>
<?php echo $footer; ?> 