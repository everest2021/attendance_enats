<?php echo $header; ?>
<div id="content">
  	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
  	</div>
  		<?php if ($error_warning) { ?>
  			<div class="warning"><?php echo $error_warning; ?></div>
  		<?php } ?>
  		<?php if ($success) { ?>
  			<div class="success"><?php echo $success; ?></div>
  		<?php } ?>
  			<div class="box">
			<div class="heading">
	  			<h1><img src="view/image/user.png" alt="" /> <?php echo $heading_title; ?></h1>
	  			<div class="buttons"><a href="<?php echo $insert; ?>" class="button"><?php echo $button_insert; ?></a><a onclick="$('form').submit();" class="button"><?php echo $button_delete; ?></a></div>
			</div>
			<div class="content">
	  			<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
					<table class="list">
		  			<thead>
					<tr>
			  		<td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[id*=\'selected\']').attr('checked', this.checked);" /></td>
			  		<!-- <td class="left"><?php if ($sort == 'device_serial_no') { ?>
						<a href="<?php echo $sort_device_serial_no; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_device_serial_no; ?></a>
					<?php } else { ?>
						<a href="<?php echo $sort_device_serial_no; ?>"><?php echo $column_device_serial_no; ?></a>
					<?php } ?></td>
					<td class="left"><?php if ($sort == 'command') { ?>
						<a href="<?php echo $sort_command; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_command; ?></a>
					<?php } else { ?>
						<a href="<?php echo $sort_command; ?>"><?php echo $column_command; ?></a>
					<?php } ?></td>
			  		<td class="left"><?php if ($sort == 'status') { ?>
						<a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
					<?php } else { ?>
						<a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
					<?php } ?></td>
			  		<td class="right"><?php echo $column_action; ?></td> -->
			  		<td class="left"><?php echo $column_device_serial_no; ?></td>
			  		<td class="left"><?php echo 'Block /Uunblock '; ?></td>
			  		<td class="left"><?php echo $column_command; ?></td>
			  		<td class="left"><?php echo $column_status; ?></td>
			  		<td class="left"><?php echo $column_action; ?></td>
					</tr>
		  			</thead>
			  			<tbody>
			  			<tr style="display: none;" class="filter">
              			<td></td>
              			<td><input type="text" id="filter_device_serial_no" device_serial_no="filter_device_serial_no" value="<?php echo $filter_device_serial_no; ?>" style="width:390px;" /></td>
              			<td></td>
              			<td></td><td align="right"><a onclick="filter();" class="button"><?php echo 'Filter'; ?></a></td>
            			</tr>
						<?php if ($device_commander) { ?>
						<?php foreach ($device_commander as $device_commands) { ?>
						<tr>
				  		<td style="text-align: center;"><?php if ($device_commands['selected']) { ?>
							<input type="checkbox" name="selected[]" value="<?php echo $device_commands['id']; ?>" checked="checked" id="selected"/>
						<?php } else { ?>
							<input type="checkbox" name="selected[]" value="<?php echo $device_commands['id']; ?>" id="selected"/>
						<?php } ?></td>
				  		<td class="left"><?php echo $device_commands['device_serial_no']; ?></td>
				  		<td class="left"><?php echo $device_commands['command_name']; ?></td>
				  		<td class="left"><?php echo $device_commands['command']; ?></td>
				  		<td class="left"><?php echo $device_commands['status']; ?></td>
				  		<td class="right"><?php foreach ($device_commands['action'] as $action) { ?>
							[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
						<?php } ?></td>
						</tr>
						<?php } ?>
						<?php } else { ?>
						<tr>
				 		 <td class="center" colspan="5"><?php echo $text_no_results; ?></td>
						</tr>
						<?php } ?>
			  			</tbody>
					</table>
	  			</form>
	  				<div class="pagination"><?php echo $pagination; ?></div>
			</div>
  		</div>
	</div>
	<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=catalog/device_commands&token=<?php echo $token; ?>';
  
  var filter_device_serial_no = $('input[device_serial_no=\'filter_device_serial_no\']').attr('value');
  
  if (filter_device_serial_no) {
    url += '&filter_device_serial_no=' + encodeURIComponent(filter_device_serial_no);
  }

  command = url;
  return false;
}
//--></script>
<script type="text/javascript"><!--
$('#filter_device_serial_no').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
//--></script> 
<script type="text/javascript"><!--
$('input[device_serial_no=\'filter_device_serial_no\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/device_commands/autocomplete&token=<?php echo $token; ?>&filter_device_serial_no=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.device_serial_no,
            value: item.id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[device_serial_no=\'filter_device_serial_no\']').val(ui.item.label);
            
    return false;
  },
  focus: function(event, ui) {
        return false;
    }
});
//--></script>
<?php echo $footer; ?> 