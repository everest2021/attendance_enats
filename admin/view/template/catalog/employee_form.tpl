<?php echo $header; ?>
<div id="content">
	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
	</div>
	<?php if ($error_warning) { ?>
	<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
	<div class="box">
		<div class="heading">
			<h1><?php echo $heading_title; ?></h1>
			<div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
		</div>
		<div class="content">
			<div id="tabs" class="htabs"><a href="#tab-general"><?php echo $tab_general; ?></a></div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
				<div id="tab-general">
					<table class="form">
						<tr style="display: none;">
							<td><span class="required">*</span> <?php echo $entry_name; ?></td>
							<td><input type="text" name="name" value="<?php echo $name; ?>" size="50" />
								<?php if ($error_name) { ?>
								<span class="error"><?php echo $error_name; ?></span>
								<?php } ?></td>
							<td><span class="required">*</span> <?php echo "Emp Code"; ?></td>
							<td><input  type="text" name="emp_code" value="<?php echo $emp_code; ?>" size="50" /></td>
						</tr>
						 <tr>
							<td><span class="required">*</span> <?php echo "Employee Name"; ?></td>
							<td><input type="text" name="emp_name" value="<?php echo $emp_name; ?>" size="30" />
								<?php if ($error_name) { ?>
								<span class="error"><?php echo $error_name; ?></span>
								<?php } ?></td>
							<td style="display: none;"><span class="required">*</span> <?php echo "Middle Name"; ?></td>
							<td style="display: none;"><input type="text" name="middile_name" value="<?php echo $middile_name; ?>" size="30" />
								<?php if ($error_name) { ?>
								<span class="error"><?php echo $error_name; ?></span>
								<?php } ?></td>
						</tr>
						<tr style="display: none;">
							<td><span class="required">*</span> <?php echo "Last Name"; ?></td>
							<td><input type="text" name="last_name" value="<?php echo $last_name; ?>" size="30" />
								<?php if ($error_name) { ?>
								<span class="error"><?php echo $error_name; ?></span>
								<?php } ?></td>
							<td><span class="required" >*</span> <?php echo "Employee Code"; ?></td>
							<td>
								<input  type="text" name="employee_code" id="employee_code" value="<?php echo $employee_code; ?>" size="50" />
								<input type="hidden" name="employee_code" id="employee_code" value="<?php echo $employee_code; ?>" />
							</td>

							<td style="display:none;"><span class="required">*</span> <?php echo "Shift"; ?></td>
							<td style="display:none;">
								<select name="shift_id" id="shift_id">
									<?php foreach($shift_data as $gkey => $gvalue) { ?>
										<?php if ($gkey == $shift_id) { ?>
											<option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
										<?php } else { ?>
											<option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
										<?php } ?>
									<?php } ?>
								</select>
								<input type="hidden" name="hidden_shift_id" value="<?php echo $hidden_shift_id; ?>" />
							</td>
						</tr>
						<tr style="display:none;">
							<td style="display:none;"><span class="required" >*</span> <?php echo "Designation"; ?></td>
							<td>
								<input style="display:none;" type="text" name="designation" id="designation" value="<?php echo $designation; ?>" size="50" />
								<input style="display:none;" type="hidden" name="designation_id" id="designation_id" value="<?php echo $designation_id; ?>" size="50" />
							</td>
							
						</tr>
						<tr style="display: none;">
							<td><span class="required">*</span> <?php echo "Grade"; ?></td>
							<td>
								<input type="text" name="grade" id="grade" value="<?php echo $grade; ?>" size="100" />
								<input type="hidden" name="grade_id" id="grade_id" value="<?php echo $grade_id; ?>" size="100" />
							</td>
						</tr>
						 <tr style="display:none;">
							<td><span class="required">*</span> <?php echo "Unit"; ?></td>
							<td>
								<select name="unit_id" id="input-unit_id">
									<option value="0">None</option>
									<?php foreach($units as $gkey => $gvalue) { ?>
										<?php if ($gvalue == $unit) { ?>
											<option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
										<?php } else { ?>
											<option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
										<?php } ?>
									<?php } ?>
								</select>
								<input type="hidden" name="unit" id="unit"  value="<?php echo $unit; ?>"  class="form-control" />
								<input type="hidden" name="hidden_unit" id="hidden_unit"  value="<?php echo $hidden_unit; ?>"  class="form-control" />
								<input type="hidden" name="hidden_unit_id" id="hidden_unit_id"  value="<?php echo $hidden_unit_id; ?>"  class="form-control" />
							</td>
							<td><span class="required">*</span> <?php echo "Holiday Unit"; ?></td>
							<td style="display:none;">
								<select name="holiday_unit_id" id="input-holiday_unit_id">
									<option value="0">None</option>
									<?php foreach($units as $gkey => $gvalue) { ?>
										<?php if ($gvalue == $holiday_unit) { ?>
											<option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
										<?php } else { ?>
											<option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
										<?php } ?>
									<?php } ?>
								</select>
								<input type="hidden" name="holiday_unit" id="holiday_unit"  value="<?php echo $holiday_unit; ?>"  class="form-control" />
								<input type="hidden" name="hidden_holiday_unit" id="hidden_holiday_unit"  value="<?php echo $hidden_holiday_unit; ?>"  class="form-control" />
								<input type="hidden" name="hidden_holiday_unit_id" id="hidden_holiday_unit_id"  value="<?php echo $hidden_holiday_unit_id; ?>"  class="form-control" />
							</td>
						</tr>
						<tr style="display: none;">
							<td><?php echo "Martial Status"; ?></td>
							<td>
								<select name="martial_status">
									<option value="0">None</option>
									<?php foreach($martial_statuss as $gkey => $gvalue) { ?>
										<?php if ($gkey == $martial_status) { ?>
											<option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
										<?php } else { ?>
											<option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
										<?php } ?>
									<?php } ?>
								</select>
							</td>
							<td><?php echo "Blood Group"; ?></td>
							<td>
								<select name="blood_group">
									<option value="0">None</option>
									<?php foreach($blood_groups as $gkey => $gvalue) { ?>
										<?php if ($gkey == $blood_group) { ?>
											<option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
										<?php } else { ?>
											<option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
										<?php } ?>
									<?php } ?>
								</select>
							</td>
						</tr>
						<tr style="display: none;">
							<td><span class="required">*</span> <?php echo "Reporting Head"; ?></td>
							<td>
								<input type="text" name="reporting_to_name" id="reporting_to_name" value="<?php echo $reporting_to_name; ?>" size="50" />
								<input type="hidden" name="reporting_to" id="reporting_to" value="<?php echo $reporting_to; ?>" />
							</td>
							<td><span class="required">*</span> <?php echo "Emp Code"; ?></td>
							<td><input readonly="readonly" type="text" name="emp_code" value="<?php echo $employee_code; ?>" size="50" /></td>
						</tr>
						<tr style="display: none;">
							<td><?php echo "Date Of Birth"; ?></td>
							<td><input type="text" name="dob" value="<?php echo $dob; ?>" size="50" class="date" />
							</td>
							 <td><?php echo "Date Of Joining"; ?></td>
							<td><input type="text" name="doj" value="<?php echo $doj; ?>" size="50" class="date" />
							</td>
						</tr>
						<tr style="display: none;">
							<td><?php echo "Date Of Confirmation"; ?></td>
							<td><input type="text" name="doc" value="<?php echo $doc; ?>" size="50" class="date" />
							</td>
							<td><?php echo "Date Of Left"; ?></td>
							<td><input type="text" name="dol" value="<?php echo $dol; ?>" size="50" class="date" />
							</td>
						</tr>
						<tr style="display: none;">
							<td><?php echo "Gender"; ?></td>
							<td>
								<select name="gender">
									<option value="0">None</option>
									<?php foreach($genders as $gkey => $gvalue) { ?>
										<?php if ($gkey == $gender) { ?>
											<option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
										<?php } else { ?>
											<option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
										<?php } ?>
									<?php } ?>
								</select>
							</td>
							<td><span class=""></span> <?php echo "Pervious Company UAN No."; ?></td>
							<td><input type="text" name="pre_com_uan_no" value="<?php echo $pre_com_uan_no; ?>" size="50" /></td>
						</tr>
						<tr>
							
							<td><span class=""></span> <?php echo "Card No"; ?></td>
							<td><input type="text" name="card_no" value="<?php echo $card_no; ?>" size="50" />
							</td>
							<td><?php echo 'Status'; ?></td>
							<td>
								<select name="status">
									<?php if($status == 1){ ?>
										<option value = "1" selected="selected"><?php echo 'Active'; ?></option>
										<option value = "0"><?php echo 'Inactive'; ?></option>
									<?php } else { ?>
										<option value = "0" selected="selected"><?php echo 'Inactive'; ?></option>
										<option value = "1"><?php echo 'Active'; ?></option>
									<?php } ?>
								</select>
								<input type="hidden" name="hidden_status" id="hidden_status" value="<?php echo $hidden_status; ?>" size="50" />
								<?php if ($error_status) { ?>
								<span class="error"><?php echo $error_status; ?></span>
								<?php } ?>
							</td>
						</tr>
						<tr>
							<td><span class=""></span> <?php echo "Device Serial No"; ?></td>
							<td><input type="text" name="device_serial_no" value="<?php echo $device_serial_no; ?>" size="50" /></td>
							
						</tr>
						<tr style="display: none;">
							<td><span class=""></span> <?php echo "IFSC Code"; ?></td>
							<td><input type="text" name="ifsc_code" value="<?php echo $ifsc_code; ?>" size="50" /></td>
							<td><span class=""></span> <?php echo "Branch Name"; ?></td>
							<td><input type="text" name="branch_name" value="<?php echo $branch_name; ?>" size="50" /></td>
						</tr>
						<tr style="display: none;">
							<td><span class=""></span> <?php echo "Aadhar Card No"; ?></td>
							<td><input type="text" name="aadhar_no" value="<?php echo $aadhar_no; ?>" size="50" /></td>  
							<td><span class=""></span> <?php echo "Pan Card No"; ?></td>
							<td><input type="text" name="pan_card_no" value="<?php echo $pan_card_no; ?>" size="50" /></td>
						</tr>
						<tr style="display: none;">
							<td><span class=""></span> <?php echo "Email ID"; ?></td>
							<td><input type="text" name="email" value="<?php echo $email; ?>" size="50" /></td>
							<td><span class=""></span> <?php echo "Personal Number"; ?></td>
							<td><input type="text" name="personal_no" value="<?php echo $personal_no; ?>" size="50" /></td>
						</tr>
						<tr style="display: none;">
							<td><span class=""></span> <?php echo "Emergency number"; ?></td>
							<td><input type="text" name="emg_no" value="<?php echo $emg_no; ?>" size="50" /></td>
							<td><span class=""></span> <?php echo "Emergency contact person"; ?></td>
							<td><input type="text" name="emg_per" value="<?php echo $emg_per; ?>" size="50" /></td>
						</tr>
						<tr style="display: none;">
							<td><span class=""></span> <?php echo "Address"; ?></td>
							<td><input type="text" name="address" value="<?php echo $address; ?>" size="50" /></td>
							<td><span class=""></span> <?php echo "Education"; ?></td>
							<td><input type="text" name="education" value="<?php echo $education; ?>" size="50" /></td>
						</tr>
						<tr style="display: none;">
							<td><span class=""></span> <?php echo "Year of Experience"; ?></td>
							<td><input type="text" name="year_experience" value="<?php echo $year_experience; ?>" size="50" /></td>
						</tr>
						<tr style="display: none;">
							<td>
								<span class="required">*</span> <?php echo 'Week Off 1'; ?>
							</td>
							<td>
									<select name="week_1" id="input-week_1">
										<?php foreach($week_datas as $key => $ud) { ?>
											<?php if ($key == $week_1) { ?>
												<option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
											<?php } else { ?>
												<option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
											<?php } ?>
										<?php } ?>
									</select>
									<input type="hidden" name="hidden_week_1" value="<?php echo $hidden_week_1; ?>" />
							</td>
							<td>
								<span class="required">*</span> <?php echo 'Week Off 2'; ?>
							</td>
							<td>
								<select name="week_2" id="input-week_2">
									<?php foreach($week_datas as $key => $ud) { ?>
										<?php if ($key == $week_2) { ?>
											<option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
										<?php } else { ?>
											<option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
										<?php } ?>
									<?php } ?>
								</select>
								<input type="hidden" name="hidden_week_2" value="<?php echo $hidden_week_2; ?>" />

								<select name="full_day" id="input-full_day">
									<?php foreach($full_days as $fkey => $fvalue) { ?>
										<?php if ($fkey == $full_day) { ?>
											<option value='<?php echo $fkey; ?>' selected="selected"><?php echo $fvalue; ?></option> 
										<?php } else { ?>
											<option value='<?php echo $fkey; ?>'><?php echo $fvalue; ?></option>
										<?php } ?>
									<?php } ?>
								</select>
								<input type="hidden" name="hidden_full_day" value="<?php echo $hidden_full_day; ?>" />
							</td>
						</tr>
						<tr style="display: none;">
							<td><span class=""></span> <?php echo "Currency"; ?></td>
							<td>
								<select name="currency" id="input-currency">
									<option value="0">Please Select</option>
									<?php foreach($currency_data as $ckey => $cvalue) { ?>
										<?php if ($ckey == $currency) { ?>
											<option value="<?php echo $ckey; ?>" selected="selected"><?php echo $cvalue; ?></option>
										<?php } else { ?>
											<option value="<?php echo $ckey; ?>"><?php echo $cvalue; ?></option>
										<?php } ?>
									<?php } ?>
								</select>
							</td>
							<td><span class=""></span> <?php echo "Gross Salary"; ?></td>
							<td><input type="text" name="Gross_salary" value="<?php echo $Gross_salary; ?>" size="50" /></td>
						</tr>
						<tr style="display: none;">
							<td>
								<?php echo "Academics Certificate"; ?>
							</td>
							<td>
								<input readonly="readonly" type="text" name="academics_certificate" value="<?php echo $academics_certificate; ?>" placeholder="<?php echo 'Academics Certificate'; ?>" id="input-academics_certificate" class="form-control" />
								<input type="hidden" name="academics_certificate_source" value="<?php echo $academics_certificate_source; ?>" id="input-medical_checkup_source" class="form-control" />
								<span class="input-group-btn">
									<button type="button" id="button-academics_certificate" data-loading-text="<?php echo 'Please Wait'; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload Document'; ?></button>
								</span>
								<?php if($academics_certificate_source != ''){ ?>
									<br/ ><a target="_blank" style="cursor: pointer;" id="academics_certificate_source" class="thumbnail" href="<?php echo $academics_certificate_source; ?>">View Document</a>
								<?php } ?>
							</td>
						 <!--  <td>
								&nbsp;
							</td> -->
							<td>
								<?php echo "Pan Card"; ?>
							</td>
							<td>
								<input readonly="readonly" type="text" name="pan_card" value="<?php echo $pan_card; ?>" placeholder="<?php echo 'Pan Card'; ?>" id="input-pan_card" class="form-control" />
								<input type="hidden" name="pan_card_source" value="<?php echo $pan_card_source; ?>" id="input-pan_card_source" class="form-control" />
								<span class="input-group-btn">
									<button type="button" id="button-pan_card" data-loading-text="<?php echo 'Please Wait'; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload Document'; ?></button>
								</span>
								<?php if($pan_card_source != ''){ ?>
									<br/ ><a target="_blank" style="cursor: pointer;" id="pan_card_source" class="thumbnail" href="<?php echo $pan_card_source; ?>">View Document</a>
								<?php } ?>
							</td>
						</tr>
						<tr style="display: none;">
							<td>
								<?php echo "Aadhar Card"; ?>
							</td>
							<td>
								<input readonly="readonly" type="text" name="aadhar_card" value="<?php echo $aadhar_card; ?>" placeholder="<?php echo 'Aadhar Card'; ?>" id="input-aadhar_card" class="form-control" />
								<input type="hidden" name="aadhar_card_source" value="<?php echo $aadhar_card_source; ?>" id="input-medical_checkup_source" class="form-control" />
								<span class="input-group-btn">
									<button type="button" id="button-aadhar_card" data-loading-text="<?php echo 'Please Wait'; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload Document'; ?></button>
								</span>
								<?php if($aadhar_card_source != ''){ ?>
									<br/ ><a target="_blank" style="cursor: pointer;" id="aadhar_card_source" class="thumbnail" href="<?php echo $aadhar_card_source; ?>">View Document</a>
								<?php } ?>
							</td>
						 <!--  <td>
								&nbsp;
							</td> -->
							<td>
								<?php echo "Cancelled Cheque"; ?>
							</td>
							<td>
								<input readonly="readonly" type="text" name="cancelled_cheque" value="<?php echo $cancelled_cheque; ?>" placeholder="<?php echo 'Cancelled Cheque'; ?>" id="input-cancelled_cheque" class="form-control" />
								<input type="hidden" name="cancelled_cheque_source" value="<?php echo $cancelled_cheque_source; ?>" id="input-cancelled_cheque_source" class="form-control" />
								<span class="input-group-btn">
									<button type="button" id="button-cancelled_cheque" data-loading-text="<?php echo 'Please Wait'; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload Document'; ?></button>
								</span>
								<?php if($cancelled_cheque_source != ''){ ?>
									<br/ ><a target="_blank" style="cursor: pointer;" id="cancelled_cheque_source" class="thumbnail" href="<?php echo $cancelled_cheque_source; ?>">View Document</a>
								<?php } ?>
							</td>
						</tr>
						<tr style="display: none;">
							<td>
								<?php echo "Photo"; ?>
							</td>
							<td>
								<input readonly="readonly" type="text" name="emp_photo" value="<?php echo $emp_photo; ?>" placeholder="<?php echo 'Photo'; ?>" id="input-emp_photo" class="form-control" />
								<input type="hidden" name="emp_photo_source" value="<?php echo $emp_photo_source; ?>" id="input-emp_photo_source" class="form-control" />
								<span class="input-group-btn">
									<button type="button" id="button-emp_photo" data-loading-text="<?php echo 'Please Wait'; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload Document'; ?></button>
								</span>
								<?php if($emp_photo_source != ''){ ?>
									<br/ ><a target="_blank" style="cursor: pointer;" id="emp_photo_source" class="thumbnail" href="<?php echo $emp_photo_source; ?>">View Document</a>
								<?php } ?>
							</td>
							<td>
								<?php echo "Resume"; ?>
							</td>
							<td>
								<input readonly="readonly" type="text" name="resume" value="<?php echo $resume; ?>" placeholder="<?php echo 'resume'; ?>" id="input-resume" class="form-control" />
								<input type="hidden" name="resume_source" value="<?php echo $resume_source; ?>" id="input-resume_source" class="form-control" />
								<span class="input-group-btn">
									<button type="button" id="button-resume" data-loading-text="<?php echo 'Please Wait'; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload Document'; ?></button>
								</span>
								<?php if($resume_source != ''){ ?>
									<br/ ><a target="_blank" style="cursor: pointer;" id="resume_source" class="thumbnail" href="<?php echo $resume_source; ?>">View Document</a>
								<?php } ?>
							</td>
						</tr>
						<tr style="display:none;">
							<td><?php echo 'Department head'; ?></td>
							<td>
								<select name="is_dept">
									<?php if($is_dept == '1'){ ?>
										<option value = "1" selected="selected"><?php echo 'Yes'; ?></option>
										<option value = "0"><?php echo 'No'; ?></option>
									<?php } else { ?>
										<option value = "0" selected="selected"><?php echo 'No'; ?></option>
										<option value = "1"><?php echo 'Yes'; ?></option>
									<?php } ?>
								</select>
							</td>
						</tr>
						<tr style="display:none;">
							<td><?php echo 'Department List'; ?></td>
							<td><div class="scrollbox">
									<?php $class = 'even'; ?>
									<?php foreach ($department_data as $dkey => $dvalue) { ?>
									<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
									<div class="<?php echo $class; ?>">
										<?php if (in_array($dkey, $dept_head_list)) { ?>
										<input type="checkbox" name="dept_head_list[]" value="<?php echo $dkey; ?>" checked="checked" />
										<?php echo $dvalue; ?>
										<?php } else { ?>
										<input type="checkbox" name="dept_head_list[]" value="<?php echo $dkey; ?>" />
										<?php echo $dvalue; ?>
										<?php } ?>
									</div>
									<?php } ?>
								</div>
							</td>
						</tr>
						<tr style="display: none;">
							<td><span class="required">*</span> <?php echo 'Card Number'; ?></td>
							<td><input type="text" name="card_number" value="<?php echo $card_number; ?>" size="100" />
								<?php if ($error_card_number) { ?>
								<span class="error"><?php echo $error_card_number; ?></span>
								<?php } ?></td>
						</tr>
						<tr style="display: none;">
							<td><span class="required">*</span> <?php echo 'Weekly Off'; ?></td>
							<td>
								<select name="weekly_off">
									<option value="0">None</option>
									<?php foreach($weeks as $wkey => $wvalue) { ?>
										<?php if ($wkey == $weekly_off) { ?>
											<option value="<?php echo $wkey; ?>" selected="selected"><?php echo $wvalue; ?></option>
										<?php } else { ?>
											<option value="<?php echo $wkey; ?>"><?php echo $wvalue; ?></option>
										<?php } ?>
									<?php } ?>
								</select>
								<?php if ($error_weekly_off) { ?>
								<span class="error"><?php echo $error_weekly_off; ?></span>
								<?php } ?>
							</td>
						</tr>
						<tr style="display: none;">
							<td><span class="required">*</span> <?php echo 'Shift Type'; ?></td>
							<td>
								<select name="shift_type">
									<?php if($shift_type == 'F' || $shift_type == ''){ ?>
										<option value = "F" selected="selected"><?php echo 'Fixed'; ?></option>
										<option value = "R"><?php echo 'Rotate'; ?></option>
									<?php } else { ?>
										<option value = "R" selected="selected"><?php echo 'Rotate'; ?></option>
										<option value = "F"><?php echo 'Fixed'; ?></option>
									<?php } ?>
								</select>
							</td>
						</tr>
						<tr style="display: none;">
							<td><?php echo 'Super User'; ?></td>
							<td>
								<select name="is_super">
									<?php if($is_super == '1'){ ?>
										<option value = "1" selected="selected"><?php echo 'Yes'; ?></option>
										<option value = "0"><?php echo 'No'; ?></option>
									<?php } else { ?>
										<option value = "0" selected="selected"><?php echo 'No'; ?></option>
										<option value = "1"><?php echo 'Yes'; ?></option>
									<?php } ?>
								</select>
							</td>
					</table>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">

$('#button-resume').on('click', function() {
	$('#form-resume').remove();
	$('body').prepend('<form enctype="multipart/form-data" id="form-resume" style="display: none;"><input type="file" name="file" /></form>');
	$('#form-resume input[name=\'file\']').trigger('click');
	if (typeof timer != 'undefined') {
			clearInterval(timer);
	}
	timer = setInterval(function() {
		if ($('#form-resume input[name=\'file\']').val() != '') {
			clearInterval(timer); 
			image_name = 'resume';  
			$.ajax({
				url: 'index.php?route=catalog/employee/upload&token=<?php echo $token; ?>'+'&image_name='+image_name,
				type: 'post',   
				dataType: 'json',
				data: new FormData($('#form-resume')[0]),
				cache: false,
				contentType: false,
				processData: false,   
				beforeSend: function() {
					$('#button-upload').button('loading');
				},
				complete: function() {
					$('#button-upload').button('reset');
				},  
				success: function(json) {
					if (json['error']) {
						alert(json['error']);
					}
					if (json['success']) {
						alert(json['success']);
						console.log(json);
						$('input[name=\'resume\']').attr('value', json['filename']);
						$('input[name=\'resume_source\']').attr('value', json['link_href']);
						d = new Date();
						var previewHtml = '<br/><a target="_blank" style="cursor: pointer;" id="resume_source" href="'+json['link_href']+'">View Document</a>';
						$('#resume_source').remove();
						$('#button-resume').after(previewHtml);
					}
				},      
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});


$('#button-emp_photo').on('click', function() {
	$('#form-emp_photo').remove();
	$('body').prepend('<form enctype="multipart/form-data" id="form-emp_photo" style="display: none;"><input type="file" name="file" /></form>');
	$('#form-emp_photo input[name=\'file\']').trigger('click');
	if (typeof timer != 'undefined') {
			clearInterval(timer);
	}
	timer = setInterval(function() {
		if ($('#form-emp_photo input[name=\'file\']').val() != '') {
			clearInterval(timer); 
			image_name = 'Emp-Photo';  
			$.ajax({
				url: 'index.php?route=catalog/employee/upload&token=<?php echo $token; ?>'+'&image_name='+image_name,
				type: 'post',   
				dataType: 'json',
				data: new FormData($('#form-emp_photo')[0]),
				cache: false,
				contentType: false,
				processData: false,   
				beforeSend: function() {
					$('#button-upload').button('loading');
				},
				complete: function() {
					$('#button-upload').button('reset');
				},  
				success: function(json) {
					if (json['error']) {
						alert(json['error']);
					}
					if (json['success']) {
						alert(json['success']);
						console.log(json);
						$('input[name=\'emp_photo\']').attr('value', json['filename']);
						$('input[name=\'emp_photo_source\']').attr('value', json['link_href']);
						d = new Date();
						var previewHtml = '<br/><a target="_blank" style="cursor: pointer;" id="emp_photo_source" href="'+json['link_href']+'">View Document</a>';
						$('#emp_photo_source').remove();
						$('#button-emp_photo').after(previewHtml);
					}
				},      
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});

$('#button-cancelled_cheque').on('click', function() {
	$('#form-cancelled_cheque').remove();
	$('body').prepend('<form enctype="multipart/form-data" id="form-cancelled_cheque" style="display: none;"><input type="file" name="file" /></form>');
	$('#form-cancelled_cheque input[name=\'file\']').trigger('click');
	if (typeof timer != 'undefined') {
			clearInterval(timer);
	}
	timer = setInterval(function() {
		if ($('#form-cancelled_cheque input[name=\'file\']').val() != '') {
			clearInterval(timer); 
			image_name = 'Cancelled-cheque';  
			$.ajax({
				url: 'index.php?route=catalog/employee/upload&token=<?php echo $token; ?>'+'&image_name='+image_name,
				type: 'post',   
				dataType: 'json',
				data: new FormData($('#form-cancelled_cheque')[0]),
				cache: false,
				contentType: false,
				processData: false,   
				beforeSend: function() {
					$('#button-upload').button('loading');
				},
				complete: function() {
					$('#button-upload').button('reset');
				},  
				success: function(json) {
					if (json['error']) {
						alert(json['error']);
					}
					if (json['success']) {
						alert(json['success']);
						console.log(json);
						$('input[name=\'cancelled_cheque\']').attr('value', json['filename']);
						$('input[name=\'cancelled_cheque_source\']').attr('value', json['link_href']);
						d = new Date();
						var previewHtml = '<br/><a target="_blank" style="cursor: pointer;" id="cancelled_cheque_source" href="'+json['link_href']+'">View Document</a>';
						$('#cancelled_cheque_source').remove();
						$('#button-cancelled_cheque').after(previewHtml);
					}
				},      
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});

$('#button-aadhar_card').on('click', function() {
	$('#form-aadhar_card').remove();
	$('body').prepend('<form enctype="multipart/form-data" id="form-aadhar_card" style="display: none;"><input type="file" name="file" /></form>');
	$('#form-aadhar_card input[name=\'file\']').trigger('click');
	if (typeof timer != 'undefined') {
			clearInterval(timer);
	}
	timer = setInterval(function() {
		if ($('#form-aadhar_card input[name=\'file\']').val() != '') {
			clearInterval(timer); 
			image_name = 'aadharcard';  
			$.ajax({
				url: 'index.php?route=catalog/employee/upload&token=<?php echo $token; ?>'+'&image_name='+image_name,
				type: 'post',   
				dataType: 'json',
				data: new FormData($('#form-aadhar_card')[0]),
				cache: false,
				contentType: false,
				processData: false,   
				beforeSend: function() {
					$('#button-upload').button('loading');
				},
				complete: function() {
					$('#button-upload').button('reset');
				},  
				success: function(json) {
					if (json['error']) {
						alert(json['error']);
					}
					if (json['success']) {
						alert(json['success']);
						console.log(json);
						$('input[name=\'aadhar_card\']').attr('value', json['filename']);
						$('input[name=\'aadhar_card_source\']').attr('value', json['link_href']);
						d = new Date();
						var previewHtml = '<br/><a target="_blank" style="cursor: pointer;" id="aadhar_card_source" href="'+json['link_href']+'">View Document</a>';
						$('#aadhar_card_source').remove();
						$('#button-aadhar_card').after(previewHtml);
					}
				},      
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});


$('#button-pan_card').on('click', function() {
	$('#form-pan_card').remove();
	$('body').prepend('<form enctype="multipart/form-data" id="form-pan_card" style="display: none;"><input type="file" name="file" /></form>');
	$('#form-pan_card input[name=\'file\']').trigger('click');
	if (typeof timer != 'undefined') {
			clearInterval(timer);
	}
	timer = setInterval(function() {
		if ($('#form-pan_card input[name=\'file\']').val() != '') {
			clearInterval(timer); 
			image_name = 'Pancard';  
			$.ajax({
				url: 'index.php?route=catalog/employee/upload&token=<?php echo $token; ?>'+'&image_name='+image_name,
				type: 'post',   
				dataType: 'json',
				data: new FormData($('#form-pan_card')[0]),
				cache: false,
				contentType: false,
				processData: false,   
				beforeSend: function() {
					$('#button-upload').button('loading');
				},
				complete: function() {
					$('#button-upload').button('reset');
				},  
				success: function(json) {
					if (json['error']) {
						alert(json['error']);
					}
					if (json['success']) {
						alert(json['success']);
						console.log(json);
						$('input[name=\'pan_card\']').attr('value', json['filename']);
						$('input[name=\'pan_card_source\']').attr('value', json['link_href']);
						d = new Date();
						var previewHtml = '<br/><a target="_blank" style="cursor: pointer;" id="pan_card_source" href="'+json['link_href']+'">View Document</a>';
						$('#pan_card_source').remove();
						$('#button-pan_card').after(previewHtml);
					}
				},      
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});

$('#button-academics_certificate').on('click', function() {
	$('#form-academics_certificate').remove();
	$('body').prepend('<form enctype="multipart/form-data" id="form-academics_certificate" style="display: none;"><input type="file" name="file" /></form>');
	$('#form-academics_certificate input[name=\'file\']').trigger('click');
	if (typeof timer != 'undefined') {
			clearInterval(timer);
	}
	timer = setInterval(function() {
		if ($('#form-academics_certificate input[name=\'file\']').val() != '') {
			clearInterval(timer); 
			image_name = 'acadamics-certificate';  
			$.ajax({
				url: 'index.php?route=catalog/employee/upload&token=<?php echo $token; ?>'+'&image_name='+image_name,
				type: 'post',   
				dataType: 'json',
				data: new FormData($('#form-academics_certificate')[0]),
				cache: false,
				contentType: false,
				processData: false,   
				beforeSend: function() {
					$('#button-upload').button('loading');
				},
				complete: function() {
					$('#button-upload').button('reset');
				},  
				success: function(json) {
					if (json['error']) {
						alert(json['error']);
					}
					if (json['success']) {
						alert(json['success']);
						console.log(json);
						$('input[name=\'academics_certificate\']').attr('value', json['filename']);
						$('input[name=\'academics_certificate_source\']').attr('value', json['link_href']);
						d = new Date();
						var previewHtml = '<br/><a target="_blank" style="cursor: pointer;" id="academics_certificate_source" href="'+json['link_href']+'">View Document</a>';
						$('#academics_certificate_source').remove();
						$('#button-academics_certificate').after(previewHtml);
					}
				},      
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
</script>
<script type="text/javascript">
$('input[name=\'designation\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/designation/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {   
				response($.map(json, function(item) {
					return {
						label: item.d_name,
						value: item.designation_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'designation\']').val(ui.item.label);
		$('input[name=\'designation_id\']').val(ui.item.value);
		return false;
	},
	focus: function(event, ui) {
		return false;
	}
});

$('input[name=\'grade\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/grade/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {   
				response($.map(json, function(item) {
					return {
						label: item.g_name,
						value: item.grade_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'grade\']').val(ui.item.label);
		$('input[name=\'grade_id\']').val(ui.item.value);
		return false;
	},
	focus: function(event, ui) {
		return false;
	}
});

$('input[name=\'department\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/department/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {   
				response($.map(json, function(item) {
					return {
						label: item.d_name,
						value: item.department_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'department\']').val(ui.item.label);
		$('input[name=\'department_id\']').val(ui.item.value);
		return false;
	},
	focus: function(event, ui) {
		return false;
	}
});

$('input[name=\'reporting_to_name\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/employee/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {   
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.emp_code
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'reporting_to_name\']').val(ui.item.label);
		$('input[name=\'reporting_to\']').val(ui.item.value);
		return false;
	},
	focus: function(event, ui) {
		return false;
	}
});
//--></script>
<script type="text/javascript"><!--
// jQuery.browser = {};
// (function () {
//     jQuery.browser.msie = false;
//     jQuery.browser.version = 0;
//     if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
//         jQuery.browser.msie = true;
//         jQuery.browser.version = RegExp.$1;
//     }
// })();
$(document).ready(function() {
	$('.date').datepicker({dateFormat: 'yy-mm-dd'});

	// $('#input-week').multiselect({
	//   includeSelectAllOption: true,
	//   enableFiltering: true,
	//   maxHeight: 235,
	// });
});

$('#input-unit_id').on('change', function() {
	u_id = $('#input-unit_id option:selected').text();
	unit_id = $('#input-unit_id').val();
	$('#unit').val(u_id);
	/*
	$.ajax({
		url: 'index.php?route=catalog/employee/getshift_id&token=<?php echo $token; ?>&filter_unit_id=' +  encodeURIComponent(unit_id),
		dataType: 'json',
		success: function(json) {   
			$("#shift_id").val(json['shift_id']);
			$("#shift_id option[value='"+json['shift_id']+"']").attr('selected','selected');
		}
	});
	*/
});

$('#input-holiday_unit_id').on('change', function() {
	u_id = $('#input-holiday_unit_id option:selected').text();
	holiday_unit_id = $('#input-holiday_unit_id').val();
	$('#holiday_unit').val(u_id);
});
//--></script>
<?php echo $footer; ?>