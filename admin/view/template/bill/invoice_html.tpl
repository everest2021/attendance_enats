<?php if(!isset($html_show)) { ?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<?php } ?>
<?php foreach ($final_datas as $fkey => $final_data) { ?>
  <?php foreach ($final_data as $key => $data) { ?>
  <div style="page-break-after: always;">
    <h1 style="text-align:center;"><?php echo $text_invoice; ?></h1>
    <table class="store" style="margin-bottom:0px;">
      <tr>
        <td>
          <table>
            <tr>
              <td>
                <b style="font-size:18px;"><?php echo $data['doctor_name']; ?></b>
              </td>
            </tr>
            <tr>
              <td>
                <?php echo '1202 Forum, Uday Baug'; ?>
              </td>
            </tr>
            <tr>
              <td>
                <?php echo 'Pune, Maharashtra 4110313'; ?>
              </td>
            </tr>
            <tr>
              <td style="padding-top:40px;">
                <b style="font-size:15px;"><?php echo $data['owner_name']; ?></b>
              </td>
            </tr>
          </table>
        </td>
        <td align="right" valign="top">
          <table>
            <tr>
              <td>
                <?php echo 'Tel: '. $telephone; ?>
              </td>
            </tr>
            <tr>
              <td>
                <?php echo $email; ?>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <table class="address">
      <tr class="heading">
        <td style="width:100%"><?php echo 'Invoice No: '; ?><b><?php echo  $data['bill_id'] . '  ' . $data['month'] .' '. $data['year']; ?></b></td>
      </tr>
      <tr>
        <td style="width:100%">
          <b style="font-size:15px;"><?php echo $data['horse_name'] . ' (' . $data['owner_share'] . '%)'; ?></b>
      </tr>
    </table>
  
    <table class="product">
      <tr class="heading">
        <td><b><?php echo 'Date'; ?></b></td>
        <td><b><?php echo 'Description'; ?></b></td>
        <td align="right"><b><?php echo 'Qty'; ?></b></td>
        <td align="right"><b><?php echo 'Fees'; ?></b></td>
      </tr>
      <?php $medicine_total = 0; ?>
      <?php foreach ($data['transaction_data'] as $medicine) { ?>
      <tr>
        <td><?php echo $medicine['dot']; ?></td>
        <td><?php echo $medicine['medicine_name']; ?></td>
        <td align="right"><?php echo $medicine['medicine_quantity']; ?></td>
        <td align="right"><?php echo $medicine['medicine_total']; ?></td>
      </tr>
      <?php $medicine_total = $medicine_total + $medicine['medicine_total']; ?>
      <?php } ?>
      <tr>
        <td align="right" colspan="3"><b><?php echo 'Total Amount'; ?>:</b></td>
        <td align="right"><?php echo 'Rs. ' . round($medicine_total, 2); ?></td>
      </tr>
    </table>
    <table class="store" style="margin-bottom:0px;">
      <tr>
        <td>
          <table>
            <tr>
              <td>
                <?php echo 'Trainer :' ?><b style="font-size:15px;"><?php echo $data['trainer_name']; ?></b>
              </td>
            </tr>
          </table>
        </td>
        <td align="right" valign="top">
          <table>
            <tr>
              <td>
                <?php echo 'Your Faithfully'; ?>
              </td>
            </tr>
            <tr>
              <td style="padding-top :30px;">
                <?php echo '(Authorized Signatory)'; ?>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>
  <?php } ?>
<?php } ?>
<?php if(!isset($html_show)) { ?>
</body>
</html>
<?php } ?>
