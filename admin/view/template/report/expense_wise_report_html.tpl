<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
  	<h1 style="text-align:center;">
	<?php echo ''; ?><br />
	<?php echo $title; ?><br />
		<p style="font-size:15px;"><?php echo 'Period : '. date('d-m-Y', strtotime($date_start)) . ' - ' . date('d-m-Y', strtotime($date_end)); ?></p>
		<p style="display:inline;font-size:15px;float: right"><?php echo 'Generated On : '. Date('d-m-Y'); ?></p>
  	</h1>
  	<?php if ($finaldata) {?>
  	<table class="product" style="width:100% !important;" style="text-align: right">
		<thead>
			<tr>
	        	<td class="left"><?php echo 'Employee Name'; ?></td>
	        	<td class="left"><?php echo 'Expense Name'; ?></td>
	        	<td class="right"><?php echo 'Amount'; ?></td>
	        	<td class="left"><?php echo 'Date'; ?></td>
	      	</tr>
	    </thead>
		<tbody>
			<?php foreach($finaldata as $fkey =>$fvalue) { ?>
			<?php foreach($fvalue['tran_data'] as $fkey =>$result ) { ?>
			 <tr>
            	 <td class="left"><?php echo $fvalue['name']; ?>
                </td>
                 <td class="left"><?php echo $result['narration']; ?>
                </td>
               
          	 	<td class="right"><?php echo $result['debit']; ?>
                </td>
               
                <td class="left"><?php echo $result['date']; ?>
                </td>
            </tr>
           <?php } ?>
			<tr style="border-top:2px solid #000000;padding-top:#000000;">
				<td  colspan='4'; style="border-bottom:2px solid #000000;padding-bottom:#000000;" ></td>
			</tr>
			<?php }  ?>
		</tbody>
	</table>
	<?php }  ?>
</div>
</body>
</html>