<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
    </div>
    <div class="content sales-report">
      <table class="form">
        <tr>
          <td style="width:13%;"><?php echo "Name"; ?>
            <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" size="15" />
            <input type="hidden" name="filter_name_id" value="<?php echo $filter_name_id; ?>" id="filter_name_id" />
          </td>
          <td style="width:10%;"><?php echo "Date Start"; ?>
            <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start" size="6" />
          </td>
          <td style="width:10%;"><?php echo "Date End"; ?>
            <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" id="date-end" size="6" />
          </td>
          <td style="width:10%">Location
            <select name="unit">
              <?php foreach($unit_data as $key => $ud) { ?>
                <?php if($key === $unit) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>

          <td style="width:8%">Department
            <select name="department" style="width:100%;">
              <?php foreach($department_data as $key => $dd) { ?>
                <?php if($key === $department) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $dd; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $dd; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>

          <td style="width:6%;display: none;">Group
            <select name="group" style="width:85px;">
              <?php foreach($group_data as $key => $gd) { ?>
                <?php if($key == $group) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $gd; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $gd; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td style="width:8%">Type
            <select name="filter_type" style="width:75px;">
              <?php foreach($type_data as $tkey => $tvalue) { ?>
                <?php if($tkey == $filter_type) { ?>
                  <option value='<?php echo $tkey; ?>' selected="selected"><?php echo $tvalue; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $tkey; ?>'><?php echo $tvalue; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
    
          <td style="text-align: right;">
            <a style="padding: 5px 5px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
      <?php if($count_mismatch == '0') { ?>
            <a style="padding: 5px 5px;" onclick="filter_export();" id="filter" class="button"><?php echo 'Export'; ?></a>
            <a style="padding: 5px 5px;" onclick="filter_export_summary();" id="filter" class="button"><?php echo 'Export Summary'; ?></a>
      <?php if($this->user->getId() == '1') { ?>
            <a style="padding: 5px 5px;" onclick="filter_export_insert();" id="filter" class="button"><?php echo 'Update Records Spine'; ?></a>
            <?php } ?>
      <?php } ?>
    </td>
        </tr>
      </table>
      <table class="list">
        <tbody>
          <?php if($final_datas) { ?>
            <?php $i = 1; ?>
            <?php foreach($final_datas as $final_data) { ?>
              <tr>
                <td style="padding: 0px 9px;"></td>
                <?php foreach($days as $dkey => $dvalue) { ?>
                  <td class="left" style="padding: 0px 9px;font-size:11px;"><?php echo $dvalue['day']; ?></td>
                <?php } ?>
              </tr>
              <tr style="font-weight:bold;font-size:11px;">
                <td colspan = "<?php echo count($final_data['tran_data']['action']) + 1; ?> ">
                  <?php echo 'Name : ' . $final_data['basic_data']['name']; ?> &nbsp;&nbsp;&nbsp; <?php echo 'Emp Code : '.$final_data['basic_data']['emp_code']; ?> &nbsp;&nbsp;&nbsp; <?php echo 'Department : '.$final_data['basic_data']['department']; ?> &nbsp;&nbsp;&nbsp;
                  <?php echo 'P : ' . $final_data['basic_data']['present_count'] ?> &nbsp;&nbsp;&nbsp; <?php echo 'WO : ' . $final_data['basic_data']['week_count'] ?> &nbsp;&nbsp;&nbsp; <?php echo 'HLD : ' . $final_data['basic_data']['holiday_count'] ?> &nbsp;&nbsp;&nbsp; <?php echo 'L : ' . $final_data['basic_data']['leave_count'] ?> &nbsp;&nbsp;&nbsp; <?php echo 'ABS : ' . $final_data['basic_data']['absent_count'] ?> &nbsp;&nbsp;&nbsp; <?php echo 'T : ' . $final_data['basic_data']['total_cnt'] ?>
                </td>
              </tr>
              <tr>
                <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                  <?php echo '1HLF'; ?>
                </td>
                <?php foreach($final_data['tran_data']['action'] as $tkey => $tvalue) { ?>
                  <td style="padding: 0px 9px;font-size:11px;">
                    <?php echo $tvalue['firsthalf_status']; ?>
                  </td>
                <?php } ?>
              </tr>
              <tr>
                <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                  <?php echo '2HLF'; ?>
                </td>
                <?php foreach($final_data['tran_data']['action'] as $tkey => $tvalue) { ?>
                  <td style="padding: 0px 9px;font-size:11px;">
                    <?php echo $tvalue['secondhalf_status']; ?>
                  </td>
                <?php } ?>
              </tr>
              <tr style="border-bottom:2px solid black;">
                <td>
                </td>
                <td colspan = "<?php echo count($final_data['tran_data']['action']) + 1; ?>" >
                </td>
              </tr>
              <?php $i++; ?>
            <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan = "1"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=report/muster&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }

  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  
  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  /*
  var filter_month = $('select[name=\'filter_month\']').attr('value');
  
  if (filter_month) {
    url += '&filter_month=' + encodeURIComponent(filter_month);
  }

  var filter_year = $('select[name=\'filter_year\']').attr('value');
  
  if (filter_year) {
    url += '&filter_year=' + encodeURIComponent(filter_year);
  }
  */

  var unit = $('select[name=\'unit\']').attr('value');
  
  if (unit) {
    url += '&unit=' + encodeURIComponent(unit);
  }

  var department = $('select[name=\'department\']').attr('value');
  
  if (department) {
    url += '&department=' + encodeURIComponent(department);
  }

  var group = $('select[name=\'group\']').attr('value');
  
  if (group) {
    url += '&group=' + encodeURIComponent(group);
  }

  var filter_type = $('select[name=\'filter_type\']').attr('value');
  
  if (filter_type) {
    url += '&filter_type=' + encodeURIComponent(filter_type);
  }

  url += '&once=1';
  
  location = url;
  return false;
}

function filter_export() {
  url = 'index.php?route=report/muster/export&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }

  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  
  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  /*
  var filter_month = $('select[name=\'filter_month\']').attr('value');
  
  if (filter_month) {
    url += '&filter_month=' + encodeURIComponent(filter_month);
  }

  var filter_year = $('select[name=\'filter_year\']').attr('value');
  
  if (filter_year) {
    url += '&filter_year=' + encodeURIComponent(filter_year);
  }
  */

  var unit = $('select[name=\'unit\']').attr('value');
  
  if (unit) {
    url += '&unit=' + encodeURIComponent(unit);
  }

  var department = $('select[name=\'department\']').attr('value');
  
  if (department) {
    url += '&department=' + encodeURIComponent(department);
  }

  var group = $('select[name=\'group\']').attr('value');
  
  if (group) {
    url += '&group=' + encodeURIComponent(group);
  }

  var filter_type = $('select[name=\'filter_type\']').attr('value');
  
  if (filter_type) {
    url += '&filter_type=' + encodeURIComponent(filter_type);
  }

  location = url;
  return false;
}

function filter_export_summary() {
  url = 'index.php?route=report/muster/export_summary&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }

  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  
  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  /*
  var filter_month = $('select[name=\'filter_month\']').attr('value');
  
  if (filter_month) {
    url += '&filter_month=' + encodeURIComponent(filter_month);
  }

  var filter_year = $('select[name=\'filter_year\']').attr('value');
  
  if (filter_year) {
    url += '&filter_year=' + encodeURIComponent(filter_year);
  }
  */

  var unit = $('select[name=\'unit\']').attr('value');
  
  if (unit) {
    url += '&unit=' + encodeURIComponent(unit);
  }

  var department = $('select[name=\'department\']').attr('value');
  
  if (department) {
    url += '&department=' + encodeURIComponent(department);
  }

  var group = $('select[name=\'group\']').attr('value');
  
  if (group) {
    url += '&group=' + encodeURIComponent(group);
  }

  var filter_type = $('select[name=\'filter_type\']').attr('value');
  
  if (filter_type) {
    url += '&filter_type=' + encodeURIComponent(filter_type);
  }

  location = url;
  return false;
}

function filter_export_insert() {
  url = 'index.php?route=report/muster/export_insert&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }

  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  
  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  /*
  var filter_month = $('select[name=\'filter_month\']').attr('value');
  
  if (filter_month) {
    url += '&filter_month=' + encodeURIComponent(filter_month);
  }

  var filter_year = $('select[name=\'filter_year\']').attr('value');
  
  if (filter_year) {
    url += '&filter_year=' + encodeURIComponent(filter_year);
  }
  */

  var unit = $('select[name=\'unit\']').attr('value');
  
  if (unit) {
    url += '&unit=' + encodeURIComponent(unit);
  }

  var department = $('select[name=\'department\']').attr('value');
  
  if (department) {
    url += '&department=' + encodeURIComponent(department);
  }

  var group = $('select[name=\'group\']').attr('value');
  
  if (group) {
    url += '&group=' + encodeURIComponent(group);
  }

  var filter_type = $('select[name=\'filter_type\']').attr('value');
  
  if (filter_type) {
    url += '&filter_type=' + encodeURIComponent(filter_type);
  }

  location = url;
  return false;
}

//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
  
  //$('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  //$('#date-end').datepicker({dateFormat: 'yy-mm-dd'});

  $(function() {
      //$.datepicker.setDefaults($.datepicker.regional['en']);
      $('#date-start').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(selectedDate) {
              var date = $(this).datepicker('getDate');
              $('#date-end').datepicker('option', 'maxDate', date); // Reset minimum date
              date.setDate(date.getDate() + 30); // Add 30 days
              $('#date-end').datepicker('setDate', date); // Set as default
            }
      });
      
      $('#date-end').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(selectedDate) {
              $('#date-start').datepicker('option', 'maxDate', $(this).datepicker('getDate')); // Reset maximum date
            }
      });
      
  });
  
});
//--></script>
<script type="text/javascript"><!--

$.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
    var self = this, currentCategory = '';
    $.each(items, function(index, item) {
      if (item.category != currentCategory) {
        //ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
        currentCategory = item.category;
      }
      self._renderItem(ul, item);
    });
  }
});

$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/employee/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.emp_code
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    $('input[name=\'filter_name_id\']').val(ui.item.value);
    return false;
  },
  focus: function(event, ui) {
    return false;
  }
});
//--></script>
<?php echo $footer; ?>