<?php echo $header; ?>
<div id="content">
  	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
  	</div>
  	<?php if ($error_warning) { ?>
  	<div class="warning"><?php echo $error_warning; ?></div>
  	<?php } ?>
  	<?php if ($success) { ?>
  	<div class="success"><?php echo $success; ?></div>
  	<?php } ?>
  	<div class="box">
		<div class="heading">
	  		<h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
		</div>
	<div class="content sales-report">
	  	<table class="form">
			<tr>
				<td style="width:13%;"><?php echo "Employee Name"; ?>
				<?php if(isset($this->session->data['emp_code']) && !$this->user->isAdmin() && $reporting_head == 0) { ?>
					<input readonly="readonly" type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" size="25" />
				<?php } else { ?>
					<input type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" size="25" />
				<?php } ?>
					<input type="hidden" name="filter_name_id" value="<?php echo $filter_name_id; ?>" id="filter_name_id" />
				</td>
			  	<td style="width:9%;"><?php echo "Start Date"; ?>
					<input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start" size="12" /></td>
				<td style="width:9%;"><?php echo "End Date"; ?>
					<input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" id="date-end" size="12" /></td>
		  <?php /* ?>
		  	<?php if($filter_reporting_head_1 == '0'){ ?>
				<td style="display: none;" style="width:9%">Location
			  	<select name="filter_unit">
					<?php foreach($unit_data as $key => $ud) { ?>
				  	<?php if($key == $filter_unit) { ?>
						<option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
				  	<?php } else { ?>
						<option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
				  	<?php } ?>
					<?php } ?>
			  	</select>
				</td>
		  	<?php } ?>

		  	<?php if($filter_reporting_head_1 == '0'){ ?>
				<td  style="display: none;" style="width:13%">Department
			  		<select name="filter_department">
						<?php foreach($department_data as $key => $dd) { ?>
				  		<?php if($key == $filter_department) { ?>
							<option value='<?php echo $key; ?>' selected="selected"><?php echo $dd; ?></option> 
				  		<?php } else { ?>
							<option value='<?php echo $key; ?>'><?php echo $dd; ?></option>
				  		<?php } ?>
						<?php } ?>
			  		</select>
				</td>
		  	<?php } ?>
		  
		  	<td style="display: none;" style="width:9%">Status
				<select  name="filter_status">
			  		<?php foreach($statuses as $skey => $svalue) { ?>
					<?php if($skey == $filter_status) { ?>
				  		<option value='<?php echo $skey; ?>' selected="selected"><?php echo $svalue; ?></option> 
					<?php } else { ?>
				  		<option value='<?php echo $skey; ?>'><?php echo $svalue; ?></option>
					<?php } ?>
			  		<?php } ?>
				</select>
		  	</td>
		  <?php */ ?>
		  	<td style="text-align: right;">
				<a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
				<a style="padding: 13px 25px;" onclick="filter_export();" id="filter" class="button"><?php echo 'Export'; ?></a>
			</td>
		</tr>
	  	</table>
	  		<?php if ($final_datas) { ?>
	  			<table class="list">
				<thead>
			  		<tr>
						<td style="text-align: left;"><?php echo ' Project Name'; ?></td>
						<td style="text-align: left;"><?php echo 'Date'; ?></td>
						<td style="text-align: center;" colspan='2'><?php echo 'Shift Attended'; ?></td>
						<?php if($filter_status == 0 || $filter_status == 1) { ?>
						<td style="text-align: left;"><?php echo 'Working Hours'; ?></td>
						<td style="display:none;" class="left"><?php echo 'Late time'; ?></td>
						<td style="display:none;" class="left"><?php echo 'Early time'; ?></td>
						<?php } ?>
						<td style="text-align: left;"><?php echo 'Status'; ?></td>
			  		</tr>
			  		 	<td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td style="text-align: center;" style="width:8%;">In</td>
                        <td style="text-align: center;" style="width:8%;">Out</td>
                        <?php if($filter_status == 0 || $filter_status == 1) { ?>
                        <td style="display:none;" >&nbsp;</td>
                        <td style="display:none;" >&nbsp;</td>
                        <td>&nbsp;</td>
                        <?php } ?>
                        <td>&nbsp;</td>
                    </tr>
				</thead>
				<tbody>
					<?php foreach($final_datas as $fkey =>$fvalue) { ?>
					<tr>
						<td class="left" colspan="6"><strong>Employee Name:<?php echo $fvalue['emp_name']; ?></strong></td>
					</tr>
						<?php foreach($fvalue['trans_data'] as $fkey =>$result ) { ?>
				  	<tr>
						<td><?php echo $result['project_name']; ?></td>
						<td><?php echo $result['date']; ?></td>
						<td><?php echo $result['in']; ?></td>
						<td><?php echo $result['out']; ?></td>
						<?php if($filter_status == 0 || $filter_status == 1) { ?>
						<td><?php echo $result['working_time']; ?></td>
						<?php /*?>
					  	<td><?php if($result['late_time'] != '00:00:00') { ?>
						  	<span style="background-color:red;">
							<?php echo $result['late_time']; ?>
						  	</span>
							<?php } else { ?>
						  	<?php echo $result['late_time']; ?>
						<?php } ?>
					  	</td>

					  	<td>
						<?php if($result['early_time'] != '00:00:00') { ?>
						  	<span style="background-color:red;">
							<?php echo $result['early_time']; ?>
						  	</span>
						<?php } else { ?>
						  	<?php echo $result['early_time']; ?>
						<?php } ?>
					  	</td>
					  	<?php */?>
						<?php } ?>
						<td>
					  	<?php echo $result['status']; ?>
						</td>
				  	</tr>  
			  		<?php } ?>
			  		<tr colspan='8'; style="border-top:2px solid #000000;padding-top:#000000;">
						<td colspan='8'; style="border-bottom:2px solid #000000;padding-bottom:#000000;" ></td>
					</tr>
			  	<?php } ?>
			</tbody>
	  	</table>
	   <?php } ?>
	</div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=report/attendance&token=<?php echo $token; ?>';

   var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
	url += '&filter_name=' + encodeURIComponent(filter_name);
	var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
	if (filter_name_id) {
	  url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
	} else {
	  alert('Please Enter Name');
	  return false;
	}
  }
  
  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
	url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }
  
  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  
  if (filter_date_end) {
	url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var filter_unit = $('select[name=\'filter_unit\']').attr('value');
  
  if (filter_unit) {
	url += '&filter_unit=' + encodeURIComponent(filter_unit);
  }

  var filter_department = $('select[name=\'filter_department\']').attr('value');
  
  if (filter_department) {
	url += '&filter_department=' + encodeURIComponent(filter_department);
  }
  
  var filter_status = $('select[name=\'filter_status\']').attr('value');
  
  if (filter_status) {
	url += '&filter_status=' + encodeURIComponent(filter_status);
  }  
  
  location = url;
  return false;
}


function filter_export() {
  url = 'index.php?route=report/attendance/export&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
	url += '&filter_name=' + encodeURIComponent(filter_name);
	var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
	if (filter_name_id) {
	  url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
	} else {
	  alert('Please Enter Correct Employee Name');
	  return false;
	}
  }

  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
	url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  
  if (filter_date_end) {
	url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  
  location = url;
  return false;
}

//--></script> 
<script type="text/javascript"><!--

$.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
	var self = this, currentCategory = '';
	$.each(items, function(index, item) {
	  if (item.category != currentCategory) {
		//ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
		currentCategory = item.category;
	  }
	  self._renderItem(ul, item);
	});
  }
});

$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
	filter_name = $('input[name=\'filter_name\']').attr('value');
	$.ajax({
	  url: 'index.php?route=report/attendance/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
	  dataType: 'json',
	  success: function(json) {   
		response($.map(json, function(item) {
		  return {
			label: item.name,
			value: item.emp_code
		  }
		}));
	  }
	});
  }, 
  select: function(event, ui) {
	$('input[name=\'filter_name\']').val(ui.item.label);
	$('input[name=\'filter_name_id\']').val(ui.item.value);
	return false;
  },
  focus: function(event, ui) {
	return false;
  }
});
//--></script>

<script type="text/javascript"><!--
$(document).ready(function() {
  $(function() {	  
	  $('#date-start').datepicker({
			dateFormat: 'dd-mm-yy',			
	  });
	  
	  $('#date-end').datepicker({
			dateFormat: 'dd-mm-yy',
			
	  });
	  
  });
  
});
//--></script>
<?php echo $footer; ?>