<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Live Tracking</title>

	</head>
	<body style="height: 100%;margin: 0;padding: 0;">
		<div id="map" style="height: 100%;"></div>
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBaRGDx1pT3TmF3RPXtSSS1XpmZeIr6_oQ&callback=initMap"></script>
	</body>
	<script type="text/javascript">
		<?php 
		$js_array = json_encode($tran_datas_punch);
		echo "var locations = ". $js_array . ";\n";		
		?>
		function initMap() {
			console.log(locations);
			var map = new google.maps.Map(document.getElementById('map'), {
				zoom: 13,
				center: new google.maps.LatLng(locations[0]['lat'], locations[0]['lon']),
				mapTypeId: google.maps.MapTypeId.ROADMAP
			});
			var infowindow = new google.maps.InfoWindow({});
			var marker, i;
			for (i = 0; i < locations.length; i++) {
				marker = new google.maps.Marker({
					position: new google.maps.LatLng(locations[i]['lat'], locations[i]['lon']),
					map: map
				});

				google.maps.event.addListener(marker, 'click', (function (marker, i) {
					return function () {
						infowindow.setContent(locations[i]['date_time']);
						infowindow.open(map, marker);
					}
				})(marker, i));
			}
		}
	</script>
</html>