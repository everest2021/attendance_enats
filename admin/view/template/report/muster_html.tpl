<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<?php //foreach($final_datass as $fkeyss => $final_datas) { ?>
<div style="page-break-after: always;">
  <h1 style="text-align:center;">
    <?php echo $title; ?><br />
    <p style="display:inline;font-size:15px;"><?php echo 'Period : '. $filter_date_start . ' - ' . $filter_date_end; ?></p><br />
    <p style="display:inline;font-size:15px;"><?php echo 'Date : '. Date('d-m-Y'); ?></p>
  </h1>
  <table class="product" style="width:100% !important;">
    <tbody>
      <?php if($final_datas) { ?>
        <?php $i = 1; ?>
        <?php foreach($final_datas as $final_data) { ?>
          <tr>
            <td style="padding: 0px 9px;"></td>
            <?php foreach($days as $dkey => $dvalue) { ?>
              <td class="left" style="padding: 0px 9px;font-size:11px;"><?php echo $dvalue['day']; ?></td>
            <?php } ?>
          </tr>
          <tr style="font-weight:bold;font-size:11px;">
            <td colspan = "<?php echo count($final_data['tran_data']['action']) + 1; ?> ">
              <?php echo 'Name : ' . $final_data['basic_data']['name']; ?> &nbsp;&nbsp;&nbsp; <?php echo 'Emp Code : '.$final_data['basic_data']['emp_code']; ?> &nbsp;&nbsp;&nbsp; <?php echo 'Department : '.$final_data['basic_data']['department']; ?> &nbsp;&nbsp;&nbsp;
              <?php echo 'P : ' . $final_data['basic_data']['present_count'] ?> &nbsp;&nbsp;&nbsp; <?php echo 'WO : ' . $final_data['basic_data']['week_count'] ?> &nbsp;&nbsp;&nbsp; <?php echo 'HLD : ' . $final_data['basic_data']['holiday_count'] ?> &nbsp;&nbsp;&nbsp; <?php echo 'L : ' . $final_data['basic_data']['leave_count'] ?> &nbsp;&nbsp;&nbsp; <?php echo 'ABS : ' . $final_data['basic_data']['absent_count'] ?> &nbsp;&nbsp;&nbsp; <?php echo 'T : ' . $final_data['basic_data']['total_cnt'] ?>
            </td>
          </tr>
          <tr>
            <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo '1HLF'; ?>
            </td>
            <?php foreach($final_data['tran_data']['action'] as $tkey => $tvalue) { ?>
              <td style="padding: 0px 9px;font-size:11px;">
                <?php echo $tvalue['firsthalf_status']; ?>
              </td>
            <?php } ?>
          </tr>
          <tr>
            <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo '2HLF'; ?>
            </td>
            <?php foreach($final_data['tran_data']['action'] as $tkey => $tvalue) { ?>
              <td style="padding: 0px 9px;font-size:11px;">
                <?php echo $tvalue['secondhalf_status']; ?>
              </td>
            <?php } ?>
          </tr>
          <tr style="border-bottom:2px solid black;">
            <td>
            </td>
            <td colspan = "<?php echo count($final_data['tran_data']['action']) + 1; ?>" >
            </td>
          </tr>
          <?php $i++; ?>
        <?php } ?>
      <?php } ?>
    </tbody>
  </table>
</div>
<?php //} ?>
</body>
</html>