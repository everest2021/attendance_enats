<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
  <h1 style="text-align:center;">
    <?php echo $title; ?><br />
    <p style="display:inline;font-size:15px;"><?php echo 'Period : '. $filter_date_start . ' : ' . $filter_date_end; ?></p><br />
    <p style="display:inline;font-size:15px;"><?php echo 'Date : '. Date('d-m-Y'); ?></p>
  </h1>
  <table class="product" style="width:100% !important;">
    <tbody>
      <tr>
        <td>
          Name
        </td>
        <td>
          Emp Code 
        </td>
        <td>
          Department
        </td>
        <td>
          Present
        </td>
        <td>
          Weekly Off
        </td>
        <td>
          Holiday
        </td>
        <?php /* ?>
        <td>
          Half day
        </td>
        <?php */ ?>
        <td>
          Leave
        </td>
        <td>
          Absent
        </td>
        <td>
          Total
        </td>
        <td>
          Total Paid Days
        </td>
        <td>
          PL
        </td>
        <td>
          CL
        </td>
        <td>
          SL
        </td>
        <td>
          PLE
        </td>
      </tr>
      <?php if($final_datas) { ?>
        <?php $i = 1; ?>
        <?php foreach($final_datas as $final_data) { ?>
          <tr style="font-weight:bold;font-size:11px;">
            <td>
              <?php echo $final_data['basic_data']['name']; ?>
            </td>
            <td>
              <?php echo $final_data['basic_data']['emp_code']; ?>
            </td>
            <td>
              <?php echo $final_data['basic_data']['department']; ?>
            </td>
            <td>
              <?php echo $final_data['basic_data']['present_count']; ?>
            </td>
            <td>
              <?php echo $final_data['basic_data']['week_count']; ?>
            </td>
            <td>
              <?php echo $final_data['basic_data']['holiday_count']; ?>
            </td>
            <?php /* ?>
            <td>
              <?php echo $final_data['basic_data']['halfday_count']; ?>
            </td>
            <?php */ ?>
            <td>
              <?php echo $final_data['basic_data']['leave_count']; ?>
            </td>
            <td>
              <?php echo $final_data['basic_data']['absent_count']; ?>
            </td>
            <td>
              <?php echo $final_data['basic_data']['total_cnt']; ?>
            </td>
            <td>
              <?php
                $a = $final_data['basic_data']['total_cnt'];
                $b = $final_data['basic_data']['absent_count'];
                $c = $a - $b;
              ?>
              <?php echo $c; ?>
            </td>
            <td>
              <?php echo $final_data['basic_data']['pl_cnt']; ?>
            </td>
            <td>
              <?php echo $final_data['basic_data']['cl_cnt']; ?>
            </td>
            <td>
              <?php echo $final_data['basic_data']['sl_cnt']; ?>
            </td>
            <td>
              <?php echo $final_data['basic_data']['encash']; ?>
            </td>
          </tr>
          <?php $i++; ?>
        <?php } ?>
      <?php } ?>
    </tbody>
  </table>
</div>
</body>
</html>