<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
    </div>
    <div class="content sales-report">
      <table class="form">
        <tr>
          <td style="width:13%;"><?php echo "Name"; ?>
            <?php if(isset($this->session->data['emp_code']) && !$this->user->isAdmin() && $reporting_head == 0) { ?>
              <input readonly="readonly" type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" size="25" />
            <?php } else { ?>
              <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" size="25" />
            <?php } ?>
            <input type="hidden" name="filter_name_id" value="<?php echo $filter_name_id; ?>" id="filter_name_id" />
          </td>
          <td style="width:8%;"><?php echo "Date Start"; ?>
            <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start" size="10" />
          </td>
          <td style="width:8%;"><?php echo "Date End"; ?>
            <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" id="date-end" size="10" />
          </td>

          <?php if($user_dept == '0' || $is_dept == '1') { ?>
          <td style="width:8%">Location
            <select name="filter_unit">
              <?php foreach($unit_data as $key => $ud) { ?>
                <?php if($key == $filter_unit) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <?php } ?>

          <?php if($user_dept == '0') { ?>
          <td style="width:8%">Department
            <select name="filter_department" style="width:100%;">
              <?php foreach($department_data as $key => $dd) { ?>
                <?php if($key == $filter_department) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $dd; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $dd; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <?php } ?>

          <td style="text-align: right;">
            <a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
            <a style="padding: 13px 25px;" onclick="filter_export();" id="filter" class="button"><?php echo 'Export'; ?></a>
          </td>
        </tr>
      </table>
      <table class="list">
        <thead>
          <tr>
            <td class="center"><?php echo 'Sr.No'; ?></td>
            <td class="center"><?php echo 'Name of Employee'; ?></td>
            <td class="center"><?php echo 'Total Working Hours'; ?></td>
            <td class="center"><?php echo 'Actual Working Hours'; ?></td>
            <td class="center"><?php echo 'Difference'; ?></td>
            <td class="center"><?php echo 'Extra Hours'; ?></td>
            <td class="center"><?php echo 'Deduction A <br/ > Working Hours'; ?></td>
            <td class="center"><?php echo 'Late Mark & <br /> Early Exit'; ?></td>
            <td class="center"><?php echo 'Deduction B <br /> Late & Early Count'; ?></td>
            <td class="center"><?php echo 'Leaves C'; ?></td>
            <td class="center"><?php echo 'Utilized Leave <br /> Total Deduction A + B+ C'; ?></td>
            <td class="center"><?php echo 'Absent Days'; ?></td>
            <td class="center"><?php echo 'Total'; ?></td>
            <td class="center"><?php echo 'Opening Leave'; ?></td>
            <td class="center"><?php echo 'Closing Leave'; ?></td>
            <td class="center"><?php echo 'Absenteeism <br /> Salary Deduction'; ?></td>
            <td class="center"><?php echo 'Payable Days'; ?></td>
          </tr>
        </thead>
        <tbody>
        <tbody>
          <?php if($final_datas) { ?>
            <?php $i = 1; ?>
            <?php foreach($final_datas as $final_data) { ?>
              <tr>
                <td>
                  <?php echo $i; ?>
                </td>
                <td>
                  <?php echo $final_data['name']; ?>
                </td>
                <td>
                  <?php echo $final_data['total_working_hours']; ?>
                </td>
                <td>
                  <?php echo $final_data['total_actual_working_hours']; ?>
                </td>
                <td>
                  <?php echo $final_data['total_pending_hours']; ?>
                </td>
                <td>
                  <?php echo $final_data['total_extra_hours']; ?>
                </td>
                <td>
                  <?php echo $final_data['total_deduction_days']; ?>
                </td>
                <td>
                  <?php echo $final_data['total_late_early_count']; ?>
                </td>
                <td>
                  <?php echo $final_data['total_late_early_days']; ?>
                </td>
                <td>
                  <?php echo $final_data['total_leaves_count']; ?>
                </td>
                <td>
                  <?php echo $final_data['total_deduction']; ?>
                </td>
                <td>
                  <?php echo $final_data['total_absent_raw_count']; ?>
                </td>
                <td>
                  <?php echo $final_data['total_utitilized_plus_absent']; ?>
                </td>
                <td>
                  <?php echo $final_data['total_leave_opening']; ?>
                </td>
                <td>
                  <?php echo $final_data['total_leave_balance']; ?>
                </td>
                <td>
                  <?php echo $final_data['total_absent_days']; ?>
                </td>
                <td>
                  <?php echo $final_data['total_month_days']; ?>
                </td>
              </tr>         
              <?php $i++; ?>
            <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan="15"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=report/attendance1&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }

  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  
  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var filter_unit = $('select[name=\'filter_unit\']').attr('value');
  
  if (filter_unit) {
    url += '&filter_unit=' + encodeURIComponent(filter_unit);
  }

  var filter_department = $('select[name=\'filter_department\']').attr('value');
  
  if (filter_department) {
    url += '&filter_department=' + encodeURIComponent(filter_department);
  }

  var filter_type = $('select[name=\'filter_type\']').attr('value');
  
  if (filter_type) {
    url += '&filter_type=' + encodeURIComponent(filter_type);
  }  

  url += '&once=1';
  
  location = url;
  return false;
}

function filter_export() {
  url = 'index.php?route=report/attendance1/export&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }

  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  
  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var filter_unit = $('select[name=\'filter_unit\']').attr('value');
  
  if (filter_unit) {
    url += '&filter_unit=' + encodeURIComponent(filter_unit);
  }

  var filter_department = $('select[name=\'filter_department\']').attr('value');
  
  if (filter_department) {
    url += '&filter_department=' + encodeURIComponent(filter_department);
  }

  var filter_type = $('select[name=\'filter_type\']').attr('value');
  
  if (filter_type) {
    url += '&filter_type=' + encodeURIComponent(filter_type);
  }  
  
  location = url;
  return false;
}

//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
  
  //$('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  //$('#date-end').datepicker({dateFormat: 'yy-mm-dd'});

  $(function() {
      //$.datepicker.setDefaults($.datepicker.regional['en']);
      $('#date-start').datepicker({
            dateFormat: 'yy-mm-dd',
            /*
            onSelect: function(selectedDate) {
              var date = $(this).datepicker('getDate');
              $('#date-end').datepicker('option', 'maxDate', date); // Reset minimum date
              date.setDate(date.getDate() + 30); // Add 30 days
              $('#date-end').datepicker('setDate', date); // Set as default
            }
            */
      });
      
      $('#date-end').datepicker({
            dateFormat: 'yy-mm-dd',
            /*
            onSelect: function(selectedDate) {
              $('#date-start').datepicker('option', 'maxDate', $(this).datepicker('getDate')); // Reset maximum date
            }
            */
      });
      
  });
  
});
//--></script>
<script type="text/javascript"><!--

$.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
    var self = this, currentCategory = '';
    $.each(items, function(index, item) {
      if (item.category != currentCategory) {
        //ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
        currentCategory = item.category;
      }
      self._renderItem(ul, item);
    });
  }
});

$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
    $.ajax({
      url: 'index.php?route=report/attendance1/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term)+'&filter_date_start='+filter_date_start,
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.emp_code
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    $('input[name=\'filter_name_id\']').val(ui.item.value);
    return false;
  },
  focus: function(event, ui) {
    return false;
  }
});
//--></script>
<?php echo $footer; ?>