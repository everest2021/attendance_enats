<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<?php $i = 1; ?>
<?php foreach($final_datass as $fkeyss => $final_datas) { ?>
<div style="page-break-after: always;">
  <h1 style="text-align:center;">
    <?php echo $title; ?><br />
    <p style="display:inline;font-size:15px;"><?php echo 'Period : '. $date_start . ' - ' . $date_end; ?></p><br />
    <p style="display:inline;font-size:15px;"><?php echo 'Date : '. Date('d-m-Y'); ?></p>
  </h1>
  <table class="product" style="width:100% !important;">
    <tbody>
      <tr class="">
        <td>
          Sr. No
        </td>
        <td>
          Employee Code
        </td>
        <td>
          Employee Name
        </td>
        <td>
          Designation
        </td>
        <td>
          Department
        </td>
        <td>
          Deficit Minutes (0-1 hrs)
        </td>
        <td>
          Deficit Minutes (1-2 hrs)
        </td>
        <td>
          Deficit Minutes (2-3 hrs)
        </td>
        <td>
          Deficit Minutes (3-4 hrs)
        </td>
        <td>
          Deficit Minutes (4-5 hrs)
        </td>
        <td>
          Deficit Minutes (>5 hrs)
        </td>
        <td>
          Occassion
        </td>
        <td>
          Leave Reduction
        </td>
      </tr>
      <?php if($final_datas) { ?>
        <?php foreach($final_datas as $final_data) { ?>
          <tr>
            <td class="left" style="font-size:11px;"><?php echo $i; ?></td>
            <td class="left" style="font-size:11px;"><?php echo $final_data['emp_code']; ?></td>
            <td class="left" style="font-size:11px;"><?php echo $final_data['name']; ?></td>
            <td class="left" style="font-size:11px;"><?php echo $final_data['designation']; ?></td>
            <td class="left" style="font-size:11px;"><?php echo $final_data['department']; ?></td>
            <td class="right" style="font-size:11px;"><?php echo $final_data['onehourcnt']; ?></td>
            <td class="right" style="font-size:11px;"><?php echo $final_data['twohourcnt']; ?></td>
            <td class="right" style="font-size:11px;"><?php echo $final_data['threehourcnt']; ?></td>
            <td class="right" style="font-size:11px;"><?php echo $final_data['fourhourcnt']; ?></td>
            <td class="right" style="font-size:11px;"><?php echo $final_data['fivehourcnt']; ?></td>
            <td class="right" style="font-size:11px;"><?php echo $final_data['greatfivehourcnt']; ?></td>
            <td class="right" style="font-size:11px;"><?php echo $final_data['totalcnt']; ?></td>
            <td class="right" style="font-size:11px;"><?php echo $final_data['leave']; ?></td>
          </tr>
          <?php $i++; ?>
        <?php } ?>
      <?php } ?>
    </tbody>
  </table>
</div>
<?php } ?>
</body>
</html>