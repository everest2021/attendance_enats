<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body style="width:100%;">
<?php //foreach($final_datass as $fkeyss => $final_datas) { ?>
<div style="page-break-after: always;">
  <h1 style="text-align:center;">
    <?php echo 'ECONSHIP MARINE PVT. LTD'; ?><br /><?php echo 'LEAVE REGISTER'; ?><br />
    <p style="display:inline;font-size:15px;"><?php echo 'Period : '. $filter_date_start . ' - ' . $filter_date_end; ?></p>
  </h1>
  <table class="product" style="width:100% !important;">
    <tbody>
      <?php if($final_datas) { ?>
        <?php $i = 1; ?>
        <?php foreach($final_datas as $final_data) { ?>
          <tr>
            <td class="left" style="width:9%;font-weight:bold;font-size:12px;">From Date</td>
            <td class="left" style="width:9%;font-weight:bold;font-size:12px;">To Date</td>
            <td class="left" style="width:9%;font-weight:bold;font-size:12px;">Break</td>
            <?php if($filter_leave == '0'){ ?>
              <td class="left" colspan="4" style="text-align:center;font-weight:bold;font-size:12px;">Leave Detail</td>
            <?php } else { ?>
              <td class="left" colspan="1" style="text-align:center;font-weight:bold;font-size:12px;">Leave Detail</td>
            <?php } ?>
          </tr>
          <tr style="font-weight:bold;font-size:11px;">
            <?php if($filter_leave == '0'){ ?>
              <td colspan = "7">
                <?php echo 'Employee Code & Name : ' . $final_data['basic_data']['emp_code']; ?> &nbsp;&nbsp;&nbsp; <?php echo $final_data['basic_data']['name']; ?> 
              </td>
            <?php } else { ?>
              <td colspan = "4">
                <?php echo 'Employee Code & Name : ' . $final_data['basic_data']['emp_code']; ?> &nbsp;&nbsp;&nbsp; <?php echo $final_data['basic_data']['name']; ?> 
              </td>
            <?php } ?>
          </tr>
          <tr>
            <td colspan="3">
            </td>
            <?php if($filter_leave == 'PL' || $filter_leave == '0'){ ?>
            <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
              PL
            </td>
            <?php } ?>
            <?php if($filter_leave == 'CL' || $filter_leave == '0'){ ?>
            <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
              CL
            </td>
            <?php } ?>
            <?php if($filter_leave == 'SL' || $filter_leave == '0'){ ?>
            <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
              SL
            </td>
            <?php } ?>
            <?php if($filter_leave == 'LWP' || $filter_leave == '0'){ ?>
            <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
              LWP
            </td>
            <?php } ?>
          </tr>
          <tr>
            <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo 'opening Bal'; ?>
            </td>
            <td style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['open_date']; ?>
            </td>
            <td>
            </td>
            <?php if($filter_leave == 'PL' || $filter_leave == '0'){ ?>
            <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['pl_open']; ?>
            </td>

            <?php if($filter_leave == 'CL' || $filter_leave == '0'){ ?>
            <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['cl_open']; ?>
            </td>
            <?php } ?>

            <?php if($filter_leave == 'SL' || $filter_leave == '0'){ ?>
            <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['sl_open']; ?>
            </td>
            <?php } ?>

            <?php if($filter_leave == 'LWP' || $filter_leave == '0'){ ?>
            <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['lwp_open']; ?>
            </td>
            <?php } ?>
          </tr>
          <tr>
            <td colspan="3" class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo 'Consumed Leave'; ?>
            </td>
            <?php if($filter_leave == 'PL' || $filter_leave == '0'){ ?>
            <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['total_days_pl']; ?>
            </td>
            <?php } ?>

            <?php if($filter_leave == 'CL' || $filter_leave == '0'){ ?>
            <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['total_days_cl']; ?>
            </td>
            <?php } ?>

            <?php if($filter_leave == 'SL' || $filter_leave == '0'){ ?>
            <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['total_days_sl']; ?>
            </td>
            <?php } ?>

            <?php if($filter_leave == 'LWP' || $filter_leave == '0'){ ?>
            <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['total_days_lwp']; ?>
            </td>
            <?php } ?>
          </tr>
          <tr>
            <td colspan="3" class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo 'Closing Balance'; ?>
            </td>
            <?php if($filter_leave == 'PL' || $filter_leave == '0'){ ?>
            <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['closing_pl']; ?>
            </td>
            <?php } ?>

            <?php if($filter_leave == 'CL' || $filter_leave == '0'){ ?>
            <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['closing_cl']; ?>
            </td>
            <?php } ?>

            <?php if($filter_leave == 'SL' || $filter_leave == '0'){ ?>
            <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['closing_sl']; ?>
            </td>
            <?php } ?>

            <?php if($filter_leave == 'LWP' || $filter_leave == '0'){ ?>
            <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['closing_lwp']; ?>
            </td>
            <?php } ?>
          </tr>
          <?php if($filter_year == date('Y')) { ?>
            <tr>
              <td colspan="3" class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo 'UnProcessed Leave'; ?>
              </td>
              <?php if($filter_leave == 'PL' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['un_total_days_pl']; ?>
              </td>
              <?php } ?>

              <?php if($filter_leave == 'CL' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['un_total_days_cl']; ?>
              </td>
              <?php } ?>

              <?php if($filter_leave == 'SL' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['un_total_days_sl']; ?>
              </td>
              <?php } ?>

              <?php if($filter_leave == 'LWP' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['un_total_days_lwp']; ?>
              </td>
              <?php } ?>
            </tr>
            <tr>
              <td colspan="3" class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo 'UnProcessed Balance'; ?>
              </td>
              <?php if($filter_leave == 'PL' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['un_total_bal_pl']; ?>
              </td>
              <?php } ?>

              <?php if($filter_leave == 'CL' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['un_total_bal_cl']; ?>
              </td>
              <?php } ?>

              <?php if($filter_leave == 'SL' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['un_total_bal_sl']; ?>
              </td>
              <?php } ?>

              <?php if($filter_leave == 'LWP' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['un_total_bal_lwp']; ?>
              </td>
              <?php } ?>
            </tr>
          <?php } ?>
          <tr style="border-bottom:2px solid black;">
            <?php if($filter_leave == '0'){ ?>  
              <td colspan = "7">
              </td>
            <?php } else { ?>
              <td colspan = "4">
              </td>
            <?php } ?>
          
          </tr>
          <?php $i++; ?>
        <?php } ?>
      <?php } ?>
    </tbody>
  </table>
</div>
<?php //} ?>
</body>
</html>