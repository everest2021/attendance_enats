<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
    </div>
    <div class="content sales-report">
      <table class="form">
        <tr>
          <td style="width:13%;"><?php echo "Name"; ?>
            <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" size="15" />
            <input type="hidden" name="filter_name_id" value="<?php echo $filter_name_id; ?>" id="filter_name_id" />
          </td>
          <td style="width:10%;">
            <?php echo 'Month'; ?>
            <select name="filter_month" id="filter_month">
              <?php foreach($months as $key => $value) { ?>
                <?php if ($filter_month == $key) { ?>
                  <option value="<?php echo $key ?>" selected="selected"><?php echo $value; ?></option>
                <?php } else { ?>
                  <option value="<?php echo $key ?>"><?php echo $value; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td style="width:7%;">
            <?php echo 'Year'; ?>
            <select name="filter_year" id="filter_year">
              <?php for($i=2009; $i<=2020; $i++) { ?>
                <?php if($filter_year == $i) { ?>
                  <option value="<?php echo $i ?>" selected="selected"><?php echo $i; ?></option>
                <?php } else { ?>
                  <option value="<?php echo $i ?>"><?php echo $i; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <?php /* ?>
          <td style="width:10%;"><?php echo "Date Start"; ?>
            <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start" size="6" />
          </td>
          <td style="width:10%;"><?php echo "Date End"; ?>
            <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" id="date-end" size="6" />
          </td>
          <?php */ ?>
          <td style="width:10%">Location
            <select name="unit">
              <?php foreach($unit_data as $key => $ud) { ?>
                <?php if($key === $unit) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>

          <td style="width:8%">Department
            <select name="department" style="width:100%;">
              <?php foreach($department_data as $key => $dd) { ?>
                <?php if($key === $department) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $dd; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $dd; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>

          <td style="width:10%;display: none;">Group
            <select name="group">
              <?php foreach($group_data as $key => $gd) { ?>
                <?php if($key == $group) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $gd; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $gd; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td style="text-align: right;">
            <a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
            <a style="padding: 13px 25px;" onclick="filter_export();" id="filter" class="button"><?php echo 'Export'; ?></a>
          </td>
        </tr>
      </table>
      <table class="list">
        <tbody>
          <tr class="">
            <td>
              Sr. No
            </td>
            <td>
              Emp Code
            </td>
            <td>
              Emp Name
            </td>
            <td>
              Def Min (0-1 hrs)
            </td>
            <td>
              Def Min (1-2 hrs)
            </td>
            <td>
              Def Min (2-3 hrs)
            </td>
            <td>
              Def Min (3-4 hrs)
            </td>
            <td>
              Def Min (4-5 hrs)
            </td>
            <td>
              Def Min (>5 hrs)
            </td>
            <td>
              Occ
            </td>
            <td>
              Leave Red
            </td>
          </tr>
          <?php if($final_datas) { ?>
            <?php $i = 1; ?>
            <?php foreach($final_datas as $final_data) { ?>
              <tr>
                <td class="left">
                  <?php echo $i; ?>
                </td>
                <td class="left">
                  <?php echo $final_data['emp_code']; ?>
                </td>
                <td class="left">
                  <?php echo $final_data['name']; ?>
                </td>
                <td class="right">
                  <?php echo $final_data['onehourcnt']; ?>
                </td>
                <td class="right">
                  <?php echo $final_data['twohourcnt']; ?>
                </td>
                <td class="right">
                  <?php echo $final_data['threehourcnt']; ?>
                </td>
                <td class="right">
                  <?php echo $final_data['fourhourcnt']; ?>
                </td>
                <td class="right">
                  <?php echo $final_data['fivehourcnt']; ?>
                </td>
                <td class="right">
                  <?php echo $final_data['greatfivehourcnt']; ?>
                </td>
                <td class="right">
                  <?php echo $final_data['totalcnt']; ?>
                </td>
                <td class="right">
                  <?php echo $final_data['leave']; ?>
                </td>
              </tr>
              <?php $i++; ?>
            <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan = "11"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=report/deficit&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }

  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  
  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var filter_month = $('select[name=\'filter_month\']').attr('value');
  
  if (filter_month) {
    url += '&filter_month=' + encodeURIComponent(filter_month);
  }

  var filter_year = $('select[name=\'filter_year\']').attr('value');
  
  if (filter_year) {
    url += '&filter_year=' + encodeURIComponent(filter_year);
  }

  var unit = $('select[name=\'unit\']').attr('value');
  
  if (unit) {
    url += '&unit=' + encodeURIComponent(unit);
  }

  var department = $('select[name=\'department\']').attr('value');
  
  if (department) {
    url += '&department=' + encodeURIComponent(department);
  }

  var group = $('select[name=\'group\']').attr('value');
  
  if (group) {
    url += '&group=' + encodeURIComponent(group);
  }

  var filter_type = $('select[name=\'filter_type\']').attr('value');
  
  if (filter_type) {
    url += '&filter_type=' + encodeURIComponent(filter_type);
  }  

  url += '&once=1';
  
  location = url;
  return false;
}

function filter_export() {
  url = 'index.php?route=report/deficit/export&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }

  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  
  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var filter_month = $('select[name=\'filter_month\']').attr('value');
  
  if (filter_month) {
    url += '&filter_month=' + encodeURIComponent(filter_month);
  }

  var filter_year = $('select[name=\'filter_year\']').attr('value');
  
  if (filter_year) {
    url += '&filter_year=' + encodeURIComponent(filter_year);
  }

  var unit = $('select[name=\'unit\']').attr('value');
  
  if (unit) {
    url += '&unit=' + encodeURIComponent(unit);
  }

  var department = $('select[name=\'department\']').attr('value');
  
  if (department) {
    url += '&department=' + encodeURIComponent(department);
  }

  var group = $('select[name=\'group\']').attr('value');
  
  if (group) {
    url += '&group=' + encodeURIComponent(group);
  }

  var filter_type = $('select[name=\'filter_type\']').attr('value');
  
  if (filter_type) {
    url += '&filter_type=' + encodeURIComponent(filter_type);
  }  
  
  location = url;
  return false;
}

//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
  
  //$('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  //$('#date-end').datepicker({dateFormat: 'yy-mm-dd'});

  $(function() {
      //$.datepicker.setDefaults($.datepicker.regional['en']);
      $('#date-start').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(selectedDate) {
              var date = $(this).datepicker('getDate');
              $('#date-end').datepicker('option', 'maxDate', date); // Reset minimum date
              date.setDate(date.getDate() + 30); // Add 30 days
              $('#date-end').datepicker('setDate', date); // Set as default
            }
      });
      
      $('#date-end').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(selectedDate) {
              $('#date-start').datepicker('option', 'maxDate', $(this).datepicker('getDate')); // Reset maximum date
            }
      });
      
  });
  
});
//--></script>
<script type="text/javascript"><!--

$.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
    var self = this, currentCategory = '';
    $.each(items, function(index, item) {
      if (item.category != currentCategory) {
        //ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
        currentCategory = item.category;
      }
      self._renderItem(ul, item);
    });
  }
});

$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/employee/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.emp_code
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    $('input[name=\'filter_name_id\']').val(ui.item.value);
    return false;
  },
  focus: function(event, ui) {
    return false;
  }
});
//--></script>
<?php echo $footer; ?>