<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
    <div style="page-break-after: always;">
        <h1 style="text-align:center;">
        <?php echo ''; ?><br />
        <?php echo $title; ?><br />
            <p style="display:inline;font-size:15px;"><?php echo 'Strat Date : '. $filter_date_start . ' &nbsp;&nbsp;&nbsp;&nbsp; End Date : '. $filter_date_end . ' &nbsp;&nbsp;&nbsp;&nbsp; ' ?></p>
            <p style="display:inline;font-size:15px;float: right"><?php echo 'Generated On : '. Date('d-m-Y'); ?></p>
        </h1>
    <?php if ($final_datas) { ?>
    <table class="product" style="width:100% !important;">
        <thead>
            <tr>
                <td style="text-align: left;"><?php echo ' Project Name'; ?></td>
                <td style="text-align: left;"><?php echo 'Date'; ?></td>
                <td class="center" colspan='2'><?php echo 'Shift Attended'; ?></td>
                <?php if($filter_status == 0 || $filter_status == 1) { ?>
                <td style="text-align: left;"><?php echo 'Working Hours'; ?></td>
                <?php } ?>
                <td style="text-align: left;"><?php echo 'Status'; ?></td>   
            </tr>           
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td class="center" style="width:8%;">In</td>
                <td class="center" style="width:8%;">Out</td>
                <?php if($filter_status == 0 || $filter_status == 1) { ?>
                <td>&nbsp;</td>
                <?php } ?>
                <td>&nbsp;</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach($final_datas as $fkey =>$fvalue) { ?>
            <tr>
                <td class="left" colspan="6"><strong>Employee Name:<?php echo $fvalue['emp_name']; ?></strong></td>
            </tr>
                <?php foreach($fvalue['trans_data'] as $fkey =>$result ) { ?>
            <tr>
                <td><?php echo $result['project_name']; ?></td>
                <td><?php echo $result['date']; ?></td>
                <td><?php echo $result['in']; ?></td>
                <td><?php echo $result['out']; ?></td>
                <?php if($filter_status == 0 || $filter_status == 1) { ?>
                <td><?php echo $result['working_time']; ?></td>
                <?php /* ?>
                <td><?php if($result['late_time'] != '00:00:00') { ?>
                    <span style="background-color:red;">
                    <?php echo $result['late_time']; ?>
                    </span>
                    <?php } else { ?>
                    <?php echo $result['late_time']; ?>
                <?php } ?>
                </td>
                <td>
                    <?php if($result['early_time'] != '00:00:00') { ?>
                    <span style="background-color:red;">
                    <?php echo $result['early_time']; ?>
                    </span>
                    <?php } else { ?>
                    <?php echo $result['early_time']; ?>
                    <?php } ?>
                </td>
                <?php */ ?>
                    <?php } ?>
                <td>
                    <?php echo $result['status']; ?>
                </td>
            </tr>  
            <?php } ?>
        <?php } ?>
    </tbody>
</table>
<?php } ?>
  </div>
</body>
</html>