<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div  style="page-break-after: always;">
  	<?php if ($final_datas) { ?>
		<table style="border: 1px solid black;" class="product text-align: right">
			<tr style="border: 1px solid black;">
				<td style="border: 1px solid black;" colspan='21'>&nbsp;</td>
				<td style="border: 1px solid black; text-align: left" colspan=''><?php echo 'Dept.:'; ?></td>
			  	<td style="border: 1px solid black; text-align: left"><?php echo $final_datas['dept']; ?></td>
			</tr>
			<tr style="border: 1px solid black;">
				<td style="border: 1px solid black;" colspan='21'>&nbsp;</td>
				<td style="border: 1px solid black; font-weight:bold text-align: left"  colspan=''><?php echo 'Doc No.:'; ?></td>
			  	<td style="border: 1px solid black; text-align: left"><?php echo $final_datas['doc_no']; ?></td>
			</tr>
			<tr style="border: 1px solid black;">
				<td style="border: 1px solid black;" colspan='21'>&nbsp;</td>
				<td style="border: 1px solid black; font-weight:bold text-align: left" colspan=''><?php echo 'Rev No.:'; ?></td>
			  	<td style="border: 1px solid black; text-align: left"><?php echo $final_datas['rev_no']; ?></td>
			</tr>
			<tr style="border: 1px solid black;">
				<td style="border: 1px solid black;" colspan='21'>&nbsp;</td>
				<td style="border: 1px solid black; font-weight:bold text-align: left" colspan='' ><?php echo 'Eff.Date:'; ?></td>
			  	<td style="border: 1px solid black; text-align: left"><?php echo date('d-m-Y', strtotime($final_datas['date'])); ?></td>
			</tr>
			<tr style="border: 1px solid black;">
				<td style="border: 1px solid black;" colspan='21'>&nbsp;</td>
				<td style="border: 1px solid black; font-weight:bold text-align: left" colspan=''><?php echo 'Page:'; ?></td>
			  	<td style="border: 1px solid black; text-align: left"><?php echo "1 of 1"; ?></td>
			</tr>
		</table>
		<table style="border: 1px solid black;" class="product width:100% !important; text-align: right">
			<thead style="border: 1px solid black;">
				<tr style="border: 1px solid black;">
					<td style="border: 1px solid black; font-weight:bold; text-align: right;float: right" colspan='16' ><?php echo 'Region:'; ?></td>
					<td style="border: 1px solid black;"></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: right;float: right" colspan='2' ><?php echo 'Circle:'; ?></td>
					<td style="border: 1px solid black;"></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: right;float: right" colspan='2' ><?php echo  'Measurment Book No:'; ?></td>
					<td style="border: 1px solid black;"></td>
				</tr>
				<tr style="border: 1px solid black; text-align: center">
					<td style="border: 1px solid black;" colspan='8'>&nbsp;</td>
					<td  style="border: 1px solid black; font-weight:bold" colspan=''><?php echo "BHARTI-AIRTEL OFC Route" ?></td>
					<td style="border: 1px solid black;"colspan='14'>&nbsp;</td>
				</tr>
				<tr style="border: 1px solid black; text-align: center">
					<td style="border: 1px solid black;" colspan='8'>&nbsp;</td>
					<td style="border: 1px solid black; font-weight:bold" colspan=''><?php echo "MEASURMENT SHEET (CIVIL WORKS)" ?></td>
					<td style="border: 1px solid black;" colspan='14'>&nbsp;</td>
				</tr>
				<tr style="border: 1px solid black;">
					<td style="border: 1px solid black; font-weight:bold; text-align: left"><?php echo 'Fiber Factory'; ?></td>
				</tr>
				<tr style="border: 1px solid black;">
					<td style="border: 1px solid black; font-weight:bold; text-align: left" colspan='2'><?php echo 'Contractor Name:'; ?></td>
				  	<td style="border: 1px solid black;" colspan='4'><?php echo $final_datas['contractor_name']; ?>
				  	</td>
				</tr>
				<tr style="border: 1px solid black;">
					<td style="border: 1px solid black; font-weight:bold; text-align: left" colspan='2'><?php echo 'Route Name:'; ?></td>
				  	<td style="border: 1px solid black;" colspan='4'><?php echo $final_datas['route_name']; ?>
				  	</td>
				</tr>
				<tr style="border: 1px solid black;">
					<td style="border: 1px solid black; font-weight:bold; text-align: left" colspan='2'><?php echo 'Link/Section:'; ?></td>
				  	<td style="border: 1px solid black;" colspan='4'><?php echo $final_datas['link_section']; ?>
				  	</td>
				</tr>
				<tr style="border: 1px solid black;">
					<td style="border: 1px solid black; font-weight:bold; text-align: left" colspan='2'><?php echo 'Work Order No.:'; ?></td>
				  	<td style="border: 1px solid black;" colspan='4'><?php echo $final_datas['work_order_no']; ?>
				  	</td>
				</tr>
				<tr style="border: 1px solid black;">
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'><?php echo 'Land Mark'; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'><?php echo 'Location/MS'; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" colspan='2'><?php echo 'Chainage'; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'><?php echo 'Length(mtrs)'; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'><?php echo 'Depth Of Trench(mtrs)'; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'><?php echo 'Strata(Hard/Normal)'; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'><?php echo 'Open Technching/Moiling/HDD(mtrs)'; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" colspan='2'><?php echo 'Offset From(mtrs)'; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" colspan='2' rowspan='2'><?php echo 'Sample ABD'; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'><?php echo 'Culvert/Bridge/Road/Railway Length(mtrs)'; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'><?php echo 'HDPE Duct (mtrs)'; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" colspan='5'><?php echo 'Protection in Length(mtrs)'; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" colspan='3'><?php echo 'Accessories(Nos.)'; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'><?php echo 'Remarks'; ?></td>
				</tr>
                    <td style="border: 1px solid black; font-weight:bold; text-align: center;">From</td>
                    <td style="border: 1px solid black; font-weight:bold; text-align: center;">To</td>
                   	<td style="border: 1px solid black; font-weight:bold; text-align: center;" >Edge Of Road</td>
                    <td style="border: 1px solid black; font-weight:bold; text-align: center;" >Center Of Road</td>
                    <td style="border: 1px solid black; font-weight:bold; text-align: center;" >GL Class</td>
                    <td style="border: 1px solid black; font-weight:bold; text-align: center;" >DWC</td>
                    <td style="border: 1px solid black; font-weight:bold; text-align: center;" >RCC</td>
                    <td style="border: 1px solid black; font-weight:bold; text-align: center;" >Stone</td>
                    <td style="border: 1px solid black; font-weight:bold; text-align: center;" >PCC</td>
                    <td style="border: 1px solid black; font-weight:bold; text-align: center;" >Couplers</td>
                    <td style="border: 1px solid black; font-weight:bold; text-align: center;" >GC/PTC/HH</td>
                    <td style="border: 1px solid black; font-weight:bold; text-align: center;" >RM</td>
                </tr>
			</thead>
			<tbody>
				<?php foreach($final_datas['point_datas'] as $fkey =>$result ) { ?>
					<tr style="border: 1px solid black;">                
						<td style="border: 1px solid black; text-align: center"><?php echo "Land Mark"; ?>
						</td>
						<td style="border: 1px solid black; text-align: center"><?php echo $result['lat_lon']; ?>
						</td>
						<td style="border: 1px solid black; text-align: center;"><?php echo $result['chainage_from']; ?>
						</td>
						<td style="border: 1px solid black; text-align: center;"><?php echo $result['chainage_to']; ?>
						</td>
						<td style="border: 1px solid black; text-align: center;"><?php echo $result['length']; ?>
						</td>
						<td style="border: 1px solid black; text-align: center;"><?php echo $result['depth']; ?>
						</td>
						<td style="border: 1px solid black; text-align: center;"><?php echo $result['strata']; ?>
						</td>
						<td style="border: 1px solid black; text-align: center;"><?php echo $result['open_trenching_moiling_hdd']; ?>
						</td>
						<td style="border: 1px solid black; text-align: center;"><?php echo $result['edge_of_road']; ?>
						</td>
						<td style="border: 1px solid black; text-align: center;"><?php echo $result['center_of_road']; ?>
						</td>
						<td style="border: 1px solid black; text-align: center;"  colspan='2'><?php echo $result['sample_abd']; ?>
						</td>
						<td style="border: 1px solid black; text-align: center;"><?php echo $result['culvert_bridge_road_railway_length']; ?>
						</td>
						<td style="border: 1px solid black; text-align: center;"><?php echo $result['hdpe_duct']; ?>
						</td>
						<td style="border: 1px solid black; text-align: center;"><?php echo $result['gi_glass_b']; ?>
						</td>
						<td style="border: 1px solid black; text-align: center;"><?php echo $result['dwc']; ?>
						</td>
						<td style="border: 1px solid black; text-align: center;"><?php echo $result['rcc']; ?>
						</td>
						<td style="border: 1px solid black; text-align: center;"><?php echo $result['stone']; ?>
						</td>
						<td style="border: 1px solid black; text-align: center;"><?php echo $result['pcc']; ?>
						</td>
						<td style="border: 1px solid black; text-align: center;"><?php echo $result['couplers']; ?>
						</td>
						<td style="border: 1px solid black; text-align: center;"><?php echo $result['jc_ptc_hh']; ?>
						</td>
						<td style="border: 1px solid black; text-align: center;"><?php echo $result['rm']; ?>
						</td>
						<td style="border: 1px solid black; text-align: center;"><?php echo $result['remarks']; ?>
						</td>
					</tr>
				<?php }  ?>
				<tr style="border: 1px solid black;" >
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'colspan='4'><?php echo 'Total'; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'><?php echo $final_datas['total_l']; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2' colspan='2'><?php echo 'OFC BLowing Details Fiber Count ' . $final_datas['ofc_blowing']; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'><?php echo 'Start Meter Reading'; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'><?php echo $final_datas['start_meter_reading']; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'><?php echo 'End Meter Reading'; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2' colspan='2' ><?php echo $final_datas['end_meter_reading']; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'><?php echo $final_datas['total_cbrrl']; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'><?php echo $final_datas['total_hdpe']; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'><?php echo $final_datas['total_gi']; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'><?php echo $final_datas['total_dwc']; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'><?php echo $final_datas['total_rcc']; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'><?php echo $final_datas['total_stone']; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'><?php echo $final_datas['total_pcc']; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'><?php echo $final_datas['total_cup']; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'><?php echo $final_datas['total_jc']; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center" rowspan='2'><?php echo $final_datas['total_rm']; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: center"	rowspan='2'><?php echo ''; ?></td>
				</tr>
				<tr></tr>
				<tr style="border: 1px solid black;">
					<td style="border: 1px solid black; text-align: left" colspan='3'><?php echo 'Contractor'; ?></td>
					<td style="border: 1px solid black; text-align: left" colspan='3'><?php echo $final_datas['contractor_name']; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo 'Engineer'; ?></td>
					<td style="border: 1px solid black; text-align: left" colspan='3'><?php echo ''; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo "" ?></td>
					<td style="border: 1px solid black; text-align: left" colspan='3'><?php echo 'Circle Fiber Deployment Head'; ?></td>
					<td style="border: 1px solid black; text-align: left" colspan='3'><?php echo ''; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo ""; ?></td>
					<td style="border: 1px solid black; text-align: left" colspan='3'><?php echo 'Regional Fiber Deployment Head'; ?></td>
					<td style="border: 1px solid black; text-align: left" colspan='2'><?php echo ''; ?></td>
				</tr>
				<tr style="border: 1px solid black;">
					<td style="border: 1px solid black; text-align: left" colspan='3'><?php echo 'Signature'; ?></td>
					<td style="border: 1px solid black; text-align: left" colspan='3'>
						<img src="<?php echo $final_datas['signature_path']; ?>" style="width: 50px; height: 50px;" />
					</td>
					<td style="border: 1px solid black; text-align: left" ><?php echo 'Signature'; ?></td>
					<td style="border: 1px solid black; text-align: left" colspan='3'><?php echo ''; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo "" ?></td>
					<td style="border: 1px solid black; text-align: left" colspan='3'><?php echo 'Signature'; ?></td>
					<td style="border: 1px solid black; text-align: left" colspan='3'><?php echo ''; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo ""; ?></td>
					<td style="border: 1px solid black; text-align: left" colspan='3'><?php echo 'Signature'; ?></td>
					<td style="border: 1px solid black; text-align: left" colspan='2'><?php echo ''; ?></td>
				</tr>
				<tr style="border: 1px solid black;">
					<td style="border: 1px solid black; text-align: left" colspan='3'><?php echo 'Date'; ?></td>
					<td style="border: 1px solid black; text-align: left" colspan='3'><?php echo date('d-m-Y', strtotime($final_datas['date'])); ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo 'Date'; ?></td>
					<td style="border: 1px solid black; text-align: left" colspan='3'><?php echo ''; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo "" ?></td>
					<td style="border: 1px solid black; text-align: left" colspan='3'><?php echo 'Date'; ?></td>
					<td style="border: 1px solid black; text-align: left" colspan='3'><?php echo ''; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo ""; ?></td>
					<td style="border: 1px solid black; text-align: left" colspan='3'><?php echo 'Date'; ?></td>
					<td style="border: 1px solid black; text-align: left" colspan='2'><?php echo ''; ?></td>
				</tr>
				<tr style="border: 1px solid black;">
					<td style="border: 1px solid black; text-align: left" colspan='8'><?php echo 'Note 1: CFDH will sign the Measurment sheet during inspection of route.'; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo ''; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo ''; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo ''; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo ''; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo ''; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo ''; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo ''; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo ''; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo ''; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo ''; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo ''; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo ''; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo ''; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo ''; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo ''; ?></td>
				</tr>
				<tr style="border: 1px solid black;">
					<td style="border: 1px solid black; text-align: left" colspan='20'><?php echo 'Note 2: Read the intruction for filling Measurment sheet and important construction guidelines for your backside.'; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo ''; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo ''; ?></td>
					<td style="border: 1px solid black; text-align: left" ><?php echo ''; ?></td>
				</tr>
				<tr style="border: 1px solid black;">
					<td style="border: 1px solid black; font-weight:bold; text-align: left" rowspan='2' colspan='3'><?php echo 'OFC BLowing Details Fiber Count 2F'; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: left" rowspan='2' colspan='2'><?php echo 'Start Reading(M)'; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: left" rowspan='2' colspan='2'><?php echo $final_datas['start_meter_reading']; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: left" rowspan='2'><?php echo 'End Meter Reading(M)'; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: left" rowspan='2' colspan='2'><?php echo $final_datas['end_meter_reading']; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: left" rowspan='2' colspan='2'><?php echo 'Total OFC Blown(M)'; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: left" rowspan='2' ><?php echo $final_datas['end_meter_reading']; ?></td>
					<td style="border: 1px solid black; font-weight:bold; text-align: left" rowspan='2' colspan='10'><?php echo 'Remarks:' . $final_datas['ofc_blowing'] . 'F'; ?></td>
				</tr>
			</tbody>
		</table>
	<?php }  ?>
</div>
</body>
<?php //echo exit;?>
</html>