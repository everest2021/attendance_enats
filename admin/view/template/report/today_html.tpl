<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
  	<h1 style="text-align:center;">
	<?php echo ''; ?><br />
	<?php echo $title; ?><br />
		<p style="display:inline;font-size:15px;"><?php echo 'Date : '. $date_start . ' &nbsp;&nbsp;&nbsp;&nbsp;  ' ?></p>
  	</h1>
  	<?php if($final_datass) { ?>
  	<?php $i = 1; ?>
  	<table class="product" style="width:100% !important;">
		<thead>
		 <tr class="">
			<td style="text-align:left;"><?php echo 'Project Name'; ?></td>
			<td style="text-align:center;"><?php echo 'Shift Attended'; ?></td>
			<td style="display:none;" class="center"><?php echo 'Late time'; ?></td>	
		</tr>
	  	<tr>
			<td>&nbsp;</td>
			<td class="center">In</td>	
	  	</tr>
	  	</thead>
	  	<tbody>
		  	<?php foreach($final_datass as $fkey => $fvalue) { ?>
		  		<tr>
			 		<td style="text-align:left;" colspan="6"><strong><?php echo $fvalue['name'].'-'.$fvalue['emp_id']; ?></strong></td>
				</tr>
		  			<?php foreach($fvalue['employee'] as $ekey => $evalue) { ?>
					<tr>
						<td><?php echo $evalue['project_name']; ?></td>
						<td style="text-align:center;"><?php echo $evalue['act_intime']; ?></td>
						<?php /*?>
						<td>
							<?php if($evalue['late_time'] != '00:00:00') { ?>
								<span style="background-color:red;">
									<?php echo $evalue['late_time']; ?>
								</span>
							<?php } else { ?>
								<?php echo $evalue['late_time']; ?>
							<?php } ?>
						</td>
						<?php */?>
					</tr>
	  				<?php } ?>
					</tr>
					<tr colspan='4'; style="border-top:2px solid #000000;padding-top:#000000;">
					<td colspan='4'; style="border-bottom:2px solid #000000;padding-bottom:#000000;" ></td>
					</tr>
				<?php } ?>
			<?php $i++; ?>	
	  		<?php } ?>
		</tbody>
	</table>
</div>
</body>
</html>