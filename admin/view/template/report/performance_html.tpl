<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<?php foreach($final_datass as $fkeyss => $final_datas) { ?>
<div style="page-break-after: always;">
  <h1 style="text-align:center;">
    <?php echo 'Econ Shipping PVT LTD.'; ?><br />
    <?php echo $title; ?><br />
    <p style="display:inline;font-size:15px;"><?php echo 'Period : '. $date_start . ' - ' . $date_end; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo 'Report Type : '. $report_type; ?></p>
    <p style="display:inline;font-size:15px;float: right"><?php echo 'Generated On : '. Date('d-m-Y'); ?></p>
  </h1>
  <table class="product" style="width:100% !important;">
    <tbody>
      <?php if($final_datas) { ?>
        <?php $i = 1; ?>
        <?php foreach($final_datas as $final_data) { ?>
          <tr>
            <td style="padding: 0px 9px;"></td>
            <?php foreach($days as $dkey => $dvalue) { ?>
              <td class="left" style="padding: 0px 9px;font-size:11px;"><?php echo $dvalue['day'].'<br />'.$dvalue['day_name']; ?></td>
            <?php } ?>
          </tr>
          <tr>
            <td colspan = "<?php echo count($final_data['tran_data']) + 1; ?> " style="font-weight:bold;font-size:11px;">
              <?php echo $final_data['basic_data']['name']; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo 'Emp Code : '.$final_data['basic_data']['emp_code']; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo 'Department : '.$final_data['basic_data']['department']; ?>
            </td>
          </tr>
          <tr>
            <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo 'Sft In'; ?>
            </td>
            <?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
              <td style="padding: 0px 9px;font-size:11px;">
                <?php echo $tvalue['shift_intime']; ?>
              </td>
            <?php } ?>
          </tr>
          <tr>
            <td class="left" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo 'Sft Out'; ?>
            </td>
            <?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
              <td style="padding: 0px 9px;font-size:11px;">
                <?php echo $tvalue['shift_outtime']; ?>
              </td>
            <?php } ?>
          </tr>
          <tr>
            <td class="left" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo 'Atd In'; ?>
            </td>
            <?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
              <td style="padding: 0px 9px;font-size:11px;">
                <?php echo $tvalue['act_intime']; ?>
              </td>
            <?php } ?>
          </tr>
          <tr>
            <td class="left" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo 'Atd Out'; ?>
            </td>
            <?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
              <td style="padding: 0px 9px;font-size:11px;">
                <?php echo $tvalue['act_outtime']; ?>
              </td>
            <?php } ?>
          </tr>
          <tr style="">
            <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo 'Stat'; ?>
            </td>
            <?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
              <td align="left" style="padding: 0px 9px;font-size:11px;">
                <?php if($report_type == 'Absent'){ ?>
                  <?php echo $tvalue['firsthalf_status']; ?>
                <?php } else { ?>
                  <?php echo $tvalue['firsthalf_status']; ?>
                <?php } ?>
              </td>
            <?php } ?>
          </tr>
          <?php /* ?>
          <tr style="">
            <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo 'SH Stat'; ?>
            </td>
            <?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
              <td align="left" style="padding: 0px 9px;font-size:11px;">
                <?php if($report_type == 'Absent'){ ?>
                  <?php echo $tvalue['secondhalf_status']; ?>
                <?php } else { ?>
                  <?php echo $tvalue['secondhalf_status']; ?>
                <?php } ?>
              </td>
            <?php } ?>
          </tr>
          <?php */ ?>
          <tr style="">
            <td class="left" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo 'Late'; ?>
            </td>
            <?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
              <td style="padding: 0px 9px;font-size:11px;">
                <?php echo $tvalue['late_time']; ?>
              </td>
            <?php } ?>
          </tr>
          <tr style="">
            <td class="left" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo 'Early'; ?>
            </td>
            <?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
              <td style="padding: 0px 9px;font-size:11px;">
                <?php echo $tvalue['early_time']; ?>
              </td>
            <?php } ?>
          </tr>
          <tr style="border-bottom:2px solid black;">
            <td class="left" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo 'Wrk hrs'; ?>
            </td>
            <?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
              <td style="padding: 0px 9px;font-size:11px;">
                <?php echo $tvalue['working_time']; ?>
              </td>
            <?php } ?>
          </tr>
          <?php if($filter_type == '3'){ ?>
            <tr style="border-bottom:2px solid black;">
              <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo 'Loss hours'; ?>
              </td>
              <?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $tvalue['loss_hours']; ?>
                </td>
              <?php } ?>
            </tr>
          <?php } ?>
          <?php if($filter_type == '5'){ ?>
            <tr style="border-bottom:2px solid black;">
              <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo 'Exces hours'; ?>
              </td>
              <?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $tvalue['excess_hours']; ?>
                </td>
              <?php } ?>
            </tr>
          <?php } ?>
          <tr style="border-bottom:2px solid black;">
            <td>
            </td>
            <td colspan = "<?php echo count($final_data['tran_data']); ?>" >
            </td>
          </tr>
          <?php $i++; ?>
        <?php } ?>
      <?php } ?>
    </tbody>
  </table>
</div>
<?php } ?>
</body>
</html>