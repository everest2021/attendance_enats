<?php echo $header; ?>
<div id="content">
  	<div class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<?php echo $breadcrumb['separator']; ?>
		<a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
	<?php } ?>
  	</div>
  	<?php if ($error_warning) { ?>
  	<div class="warning"><?php echo $error_warning; ?>
  	</div>
  	<?php } ?>
  	<?php if ($success) { ?>
  	<div class="success"><?php echo $success; ?>
  	</div>
  	<?php } ?>
  	<div class="box">
	<div class="heading">
	  	<h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
	</div>
	<div class="content sales-report">
	  	<table class="form">
			<tr>
		  		<td style="width:13%;"><?php echo "Name"; ?>
				<?php if(isset($this->session->data['emp_code']) && !$this->user->isAdmin() && $reporting_head == 0) { ?>
			  		<input readonly="readonly" type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" size="25" />
				<?php } else { ?>
			  		<input type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" size="25" />
				<?php } ?>
					<input type="hidden" name="filter_name_id" value="<?php echo $filter_name_id; ?>" id="filter_name_id" />
		  		</td>
		  		<td style="width:8%;"><?php echo "Date Start"; ?>
					<input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start" size="10" />
		  		</td>
		  		<td style="width:8%;"><?php echo "Date End"; ?>
					<input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" id="date-end" size="10" />
		  		</td>
		  		<td style="text-align: right;">
					<a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
					<a style="padding: 13px 25px;" onclick="filter_export();" id="filter" class="button"><?php echo 'Export'; ?></a>
		  		</td>
			</tr>
	  	</table>
	  	<?php if ($finaldata) {?>
	  		<table class="list">
		 		<thead>
		          	<tr>
		            	<td class="left"><?php echo 'Date'; ?></td>
		            	<td class="left"><?php echo 'Location'; ?></td>
		          	</tr>
		        </thead>
		        <tbody>
			        <?php foreach($finaldata as $fkey =>$fvalue) { ?>
				 		<tr>
				 			<td class="left" colspan="5"><strong><?php echo $fvalue['name']; ?></strong></td>
				 		</tr>
			          	<?php foreach($fvalue['tran_data'] as $fkey =>$result ) { ?>
			            <tr>			          
			                <td class="left"><?php echo $result['date']; ?>
			                </td>
			                <td>
			               <a href=" <?php echo $result['link']; ?>" target="_blank"><?php echo "Map Location"; ?></a>
			           		</td>
			            </tr>
			            <?php } ?>
			            <tr colspan='5'; style="border-top:2px solid #000000;padding-top:#000000;">
			            	<td colspan='5'; style="border-bottom:2px solid #000000;padding-bottom:#000000;" ></td>
			            </tr>
	        		<?php }  ?>
		        </tbody>
	        </table>
        <?php }  ?>
    </div>
  	</div>
</div>
<script type="text/javascript"><!--
$(document).ready(function() {
  $(function() {	  
	  $('#date-start').datepicker({
			dateFormat: 'dd-mm-yy',			
	  });
	  
	  $('#date-end').datepicker({
			dateFormat: 'dd-mm-yy',
			
	  });
	  
  });
  
});
//--></script>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=report/employee_location&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
	url += '&filter_name=' + encodeURIComponent(filter_name);
	var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
	if (filter_name_id) {
	  url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
	} else {
	  alert('Please Enter Name');
	  return false;
	}
  }

  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
	url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  
  if (filter_date_end) {
	url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  

  url += '&once=1';
  
  location = url;
  return false;
}

function filter_export() {
  url = 'index.php?route=report/expense_ledger/export&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }

  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  
  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  
  location = url;
  return false;
}

//--></script> 

<script type="text/javascript"><!--

$.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
	var self = this, currentCategory = '';
	$.each(items, function(index, item) {
	  if (item.category != currentCategory) {
		//ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
		currentCategory = item.category;
	  }
	  self._renderItem(ul, item);
	});
  }
});

$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
	filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
	$.ajax({
	  url: 'index.php?route=report/expense_ledger/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term)+'&filter_date_start='+filter_date_start,
	  dataType: 'json',
	  success: function(json) {   
		response($.map(json, function(item) {
		  return {
			label: item.name,
			value: item.emp_code
		  }
		}));
	  }
	});
  }, 
  select: function(event, ui) {
	$('input[name=\'filter_name\']').val(ui.item.label);
	$('input[name=\'filter_name_id\']').val(ui.item.value);
	return false;
  },
  focus: function(event, ui) {
	return false;
  }
});
//--></script>
<?php echo $footer; ?>