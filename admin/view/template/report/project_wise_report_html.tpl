<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
	<h1 style="text-align:center;">
	<?php echo ''; ?><br />
	<?php echo $title; ?><br />
		
		<p style="display:inline;font-size:15px;float: right"><?php echo 'Generated On : '. Date('d-m-Y'); ?></p>
	</h1>
	<?php if ($final_datas) {?>
			<table class="product" style="width:100% !important;" style="text-align: right">
				<thead>
					<tr>
						<td class="left"><?php echo 'Employee Name'; ?></td>
						<td class="left"><?php echo 'Start Date'; ?></td>
						<td class="left"><?php echo 'End Date'; ?></td>
						<td class="left"><?php echo 'Present'; ?></td>
						<td class="left"><?php echo 'Holiday'; ?></td>
						<td class="left"><?php echo 'Leave'; ?></td>		            
						<td class="left"><?php echo 'Absent'; ?></td>
					</tr>
				</thead>
				<tbody>
					<?php foreach($final_datas as $fkey =>$fvalue) { ?>
						<tr>
							<td class="left" colspan="7"><strong>Project Name:<?php echo $fvalue['project_name']; ?></strong></td>
						</tr>
						<?php foreach($fvalue['emp_trans'] as $fkey =>$result ) { ?>
						<tr>			          
							<td class="left"><?php echo $result['emp_name']; ?>
							</td>
							<td class="left"><?php echo $result['start_date']; ?>                 
							</td>
							<td class="left"><?php echo $result['end_date']; ?>
							</td>
							<td class="left"><?php echo $result['present']; ?>
							</td>
							<td class="left"><?php echo $result['holiday']; ?>
							</td>
							<td class="left"><?php echo $result['leave']; ?>
							</td>
							<td class="left"><?php echo $result['absent']; ?>
							</td>
						</tr>
						<?php } ?>			            
						<tr colspan='7'; style="border-top:2px solid #000000;padding-top:#000000;">
							<td colspan='7'; style="border-bottom:2px solid #000000;padding-bottom:#000000;" ></td>
						</tr>
					<?php }  ?>
				</tbody>
			</table>
		<?php }  ?>
	</div>
</body>
</html>