<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<?php foreach($final_datass as $fkeyss => $final_datas) { ?>
<div style="page-break-after: always;">
  <h1 style="text-align:center;">
    <?php echo 'Econ Shipping PVT LTD.'; ?><br />
    <?php echo $title; ?><br />
    <p style="font-size:15px;"><?php echo 'Period : '. date('d-m-Y', strtotime($date_start)) . ' - ' . date('d-m-Y', strtotime($date_end)); ?></p>
    <p style="display:inline;font-size:15px;float: right"><?php echo 'Generated On : '. Date('d-m-Y'); ?></p>
  </h1>
  <table class="product" style="width:100% !important;">
    <thead>
      <tr>
        <td class="center"><?php echo 'Sr.No'; ?></td>
        <td class="center"><?php echo 'Name of Employee'; ?></td>
        <td class="center"><?php echo 'Total Working Hours'; ?></td>
        <td class="center"><?php echo 'Actual Working Hours'; ?></td>
        <td class="center"><?php echo 'Difference'; ?></td>
        <td class="center"><?php echo 'Extra Hours'; ?></td>
        <td class="center"><?php echo 'Deduction A <br/ > Working Hours'; ?></td>
        <td class="center"><?php echo 'Late Mark & <br /> Early Exit'; ?></td>
        <td class="center"><?php echo 'Deduction B <br /> Late & Early Count'; ?></td>
        <td class="center"><?php echo 'Leaves C'; ?></td>
        <td class="center"><?php echo 'Utilized Leave <br /> Total Deduction A + B+ C'; ?></td>
        <td class="center"><?php echo 'Absent Days'; ?></td>
        <td class="center"><?php echo 'Total'; ?></td>
        <td class="center"><?php echo 'Opening Leave'; ?></td>
        <td class="center"><?php echo 'Closing Leave'; ?></td>
        <td class="center"><?php echo 'Absenteeism <br /> Salary Deduction'; ?></td>
        <td class="center"><?php echo 'Payable Days'; ?></td>
      </tr>
    </thead>
    <tbody>
    <tbody>
      <?php if($final_datas) { ?>
        <?php $i = 1; ?>
        <?php foreach($final_datas as $final_data) { ?>
          <tr>
            <td>
              <?php echo $i; ?>
            </td>
            <td>
              <?php echo $final_data['name']; ?>
            </td>
            <td>
              <?php echo $final_data['total_working_hours']; ?>
            </td>
            <td>
              <?php echo $final_data['total_actual_working_hours']; ?>
            </td>
            <td>
              <?php echo $final_data['total_pending_hours']; ?>
            </td>
            <td>
              <?php echo $final_data['total_extra_hours']; ?>
            </td>
            <td>
              <?php echo $final_data['total_deduction_days']; ?>
            </td>
            <td>
              <?php echo $final_data['total_late_early_count']; ?>
            </td>
            <td>
              <?php echo $final_data['total_late_early_days']; ?>
            </td>
            <td>
              <?php echo $final_data['total_leaves_count']; ?>
            </td>
            <td>
              <?php echo $final_data['total_deduction']; ?>
            </td>
            <td>
              <?php echo $final_data['total_absent_raw_count']; ?>
            </td>
            <td>
              <?php echo $final_data['total_utitilized_plus_absent']; ?>
            </td>
            <td>
              <?php echo $final_data['total_leave_opening']; ?>
            </td>
            <td>
              <?php echo $final_data['total_leave_balance']; ?>
            </td>
            <td>
              <?php echo $final_data['total_absent_days']; ?>
            </td>
            <td>
              <?php echo $final_data['total_month_days']; ?>
            </td>
          </tr>         
          <?php $i++; ?>
        <?php } ?>
      <?php } else { ?>
      <tr>
        <td class="center" colspan="15"><?php echo $text_no_results; ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<?php } ?>
</body>
</html>