<?php echo $header; ?>
<div id="content">
	<div class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
	<?php } ?>
	</div>
  	<?php if ($error_warning) { ?>
	<div class="warning"><?php echo $error_warning; ?></div>
  	<?php } ?>
  	<?php if ($success) { ?>
  	<div class="success"><?php echo $success; ?></div>
  	<?php } ?>
  	<div class="box">
	<div class="heading">
	  	<h1><img src="view/image/report.png" alt="" /> <?php echo "Live Map Attendance Report"; ?></h1>
	</div>
	<div class="content sales-report">
	  	<table class="form">
	  		
		<tr>
			<td style="width:13%;"><?php echo "Employee Name"; ?>
				<?php if(isset($this->session->data['emp_code']) && !$this->user->isAdmin() && $reporting_head == 0) { ?>
			  		<input readonly="readonly" type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" size="25" />
				<?php } else { ?>
			  		<input type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" size="25" />
				<?php } ?>
					<input type="hidden" name="filter_name_id" value="<?php echo $filter_name_id; ?>" id="filter_name_id" />
		  	</td>
		  	<td style="width:13%;"><?php echo " Project Name"; ?>
				<?php if(isset($this->session->data['id']) && !$this->user->isAdmin() && $reporting_head == 0) { ?>
					<input readonly="readonly" type="text" name="filter_project_name" value="<?php echo $filter_project_name; ?>" id="filter_project_name" size="25" />
				<?php } else { ?>
					<input type="text" name="filter_project_name" value="<?php echo $filter_project_name; ?>" id="filter_project_name" size="25" />
				<?php } ?>
					<input type="hidden" name="filter_project_id" value="<?php echo $filter_project_id; ?>" id="filter_project_id" />
			</td>
		
			<td style="text-align: right;">
				<a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
				<a style="display:none;" target="_blank" style="padding: 13px 25px;" href="<?php echo $generate_today; ?>" class="button"><?php echo "Refresh"; ?></a>
				<a style="padding: 13px 25px;" href="<?php echo $export; ?>" class="button"><?php echo "Export"; ?></a>
		  	</td>
		</tr>
	  	</table>
		  	<?php if($final_datas) { ?>
				<?php $i = 1; ?>
			  	<table class="list">
					<thead>
					  	<tr>
							<td style="text-align: left;"><?php echo 'Project Name'; ?></td>
							<td style="text-align: center;"><?php echo 'Shift Attended'; ?></td>
							<td style="display:none" style="text-align: left;"><?php echo 'Late time'; ?></td>
							<td style="text-align: right;"><?php echo 'Action'; ?></td>
					  	</tr>
					  	<tr>
							<td>&nbsp;</td>
							<td class="center">In</td>
							<td>&nbsp;</td>
						</tr>
					</thead>
					<tbody>
				  		<?php foreach($final_datas as $fkey =>$fvalue) { ?>
					 		<tr>
					 			<td style="text-align: left;" colspan="4"><strong><?php echo $fvalue['name'].'-'.$fvalue['emp_id']; ?></strong></td>
					 		</tr>
				  			<?php foreach($fvalue['employee'] as $ekey => $evalue) { ?>
							<tr>
								<td><?php echo $evalue['project_name']; ?></td>
								<td><?php echo $evalue['act_intime']; ?></td>
								<?php /* ?>
								<td style="dispaly:none">
									<?php if($evalue['late_time'] != '00:00:00') { ?>
										<span style="background-color:red;">
											<?php echo $evalue['late_time']; ?>
										</span>
									<?php } else { ?>
										<?php echo $evalue['late_time']; ?>
									<?php } ?>
								</td>
								<?php */ ?>
								<td style="text-align: right;">
									<?php $photo = ''; ?>
									<?php if($evalue['is_head'] == 1 && $evalue['photo_src'] != ''){ ?>
										<?php  $photo = $evalue['photo_src']; ?>
										<b><font size="3" class="buttons"><a href="<?php echo $photo; ?>" class="button"><?php echo 'View on Map'; ?></a></b>
									<?php } else { ?>
										<?php  $photo = $evalue['photo_source']; ?>
										<b><font size="3" class="buttons"><a href="<?php echo $photo; ?>" class="button"><?php echo ''; ?></a></b>
									<?php } ?>
								</td>
							<?php } ?>
							</tr>
							<tr colspan='4'; style="border-top:2px solid #000000;padding-top:#000000;">
								<td colspan='4'; style="border-bottom:2px solid #000000;padding-bottom:#000000;" ></td>
							<?php $i++; ?>	
						<?php } ?>
							</tr>
		  			</tbody>
		  		</table>
	  		<?php } ?>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
function filter() {
url = 'index.php?route=report/today&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
	url += '&filter_name=' + encodeURIComponent(filter_name);
	var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
	if (filter_name_id) {
	  url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
	} else {
	  alert('Please Enter Name');
	  return false;
	}
}

 var filter_project_name = $('input[name=\'filter_project_name\']').attr('value');
  
  if (filter_project_name) {
	url += '&filter_project_name=' + encodeURIComponent(filter_project_name);
	var filter_project_id = $('input[name=\'filter_project_id\']').attr('value');
	if (filter_project_id) {
	  url += '&filter_project_id=' + encodeURIComponent(filter_project_id);
	} else {
	  alert('Please Enter Name');
	  return false;
	}
  }

  url += '&once=1';
  
  location = url;
  return false;
}
function filter_export() {
  url = 'index.php?route=report/today/export&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }

  var filter_project_name = $('input[name=\'filter_project_name\']').attr('value');
  
  if (filter_project_name) {
	url += '&filter_project_name=' + encodeURIComponent(filter_project_name);
	var filter_project_id = $('input[name=\'filter_project_id\']').attr('value');
	if (filter_project_id) {
	  url += '&filter_project_id=' + encodeURIComponent(filter_project_id);
	} else {
	  alert('Please Enter Name');
	  return false;
	}
  }

//   url = 'index.php?route=report/today&token=<?php echo $token; ?>';
//   var filter_unit = $('select[name=\'filter_unit\']').attr('value');
//   if (filter_unit) {
// 	url += '&filter_unit=' + encodeURIComponent(filter_unit);
//   }
//   var filter_department = $('select[name=\'filter_department\']').attr('value');
//   if (filter_department) {
// 	url += '&filter_department=' + encodeURIComponent(filter_department);
//   }
  location = url;
   return false;
 }
//--></script> 
<script type="text/javascript"><!--

$.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
	var self = this, currentCategory = '';
	$.each(items, function(index, item) {
	  if (item.category != currentCategory) {
		//ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
		currentCategory = item.category;
	  }
	  self._renderItem(ul, item);
	});
  }
});

$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
	filter_name = $('input[name=\'filter_name\']').attr('value');
	$.ajax({
	  url: 'index.php?route=report/today/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
	  dataType: 'json',
	  success: function(json) {   
		response($.map(json, function(item) {
		  return {
			label: item.name,
			value: item.emp_code
		  }
		}));
	  }
	});
  }, 
  select: function(event, ui) {
	$('input[name=\'filter_name\']').val(ui.item.label);
	$('input[name=\'filter_name_id\']').val(ui.item.value);
	return false;
  },
  focus: function(event, ui) {
	return false;
  }
});
//--></script>
<script type="text/javascript"><!--

$.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
	var self = this, currentCategory = '';
	$.each(items, function(index, item) {
	  if (item.category != currentCategory) {
		//ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
		currentCategory = item.category;
	  }
	  self._renderItem(ul, item);
	});
  }
});

$('input[name=\'filter_project_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
	filter_project_name = $('input[name=\'filter_project_name\']').attr('value');
	$.ajax({
	  url: 'index.php?route=report/today/autocomplete_project&token=<?php echo $token; ?>&filter_project_name=' +  encodeURIComponent(request.term),
	  dataType: 'json',
	  success: function(json) {   
		response($.map(json, function(item) {
		  return {
			label: item.name,
			value: item.id
		  }
		}));
	  }
	});
  }, 
  select: function(event, ui) {
	$('input[name=\'filter_project_name\']').val(ui.item.label);
	$('input[name=\'filter_project_id\']').val(ui.item.value);
	return false;
  },
  focus: function(event, ui) {
	return false;
  }
});
//--></script>
<?php echo $footer; ?>