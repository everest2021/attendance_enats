<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo 'Self PX Record Entry'; ?></h1>
      <div class="buttons">
        <?php if($filter_name_id != '') { ?>
          <a href="<?php echo $insert; ?>" class="button" style=""><?php echo $button_insert; ?></a>
        <?php } ?>
        <a style="display: none;" onclick="$('#form').submit();" class="button" style=""><?php echo 'Delete'; ?></a>
      </div>
    </div>
    <div class="content">
      <?php if($filter_type == 1){ ?>
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;display: none;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left" style="width: 105px;">
                <?php echo 'Name'; ?>
              </td>
              <td>
                <a><?php echo "Department"; ?></a>
              </td>
              <td>
                <a><?php echo "Location"; ?></a>
              </td>
              <td class="left" style="width:80px;">
                <?php echo 'DOI'; ?>
              </td>
              <td class="left" style="width: 105px;">
                <?php echo 'Date'; ?>
              </td>
              <td>
                <a><?php echo "In Time"; ?></a>
              </td>
              <td>
                <a><?php echo "Out Time"; ?></a>
              </td>
              <td>
                <a><?php echo "Modification"; ?></a>
              </td>
              <td>
                <a><?php echo "Reason"; ?></a>
              </td>
              <td>
                <a><?php echo "Type"; ?></a>
              </td>
              <td>
                <a><?php echo "PX Record Type"; ?></a>
              </td>
              <td style="width:70px;">
                <a><?php echo "Approval Status"; ?></a>
              </td>
              <td>
                <a><?php echo "Process"; ?></a>
              </td>
              <td class="right"><?php echo 'Action'; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr class="filter">
              <td style="display: none;">&nbsp;</td>
              <td>
                <input type="text" id="filter_name" name="filter_name" value="<?php echo $filter_name; ?>"  style="width:150px;" />
                <input type="hidden" id="filter_name_id" name="filter_name_id" value="<?php echo $filter_name_id; ?>" />
              </td>
              <td>
                <select name="filter_dept" style="width:60px;">
                  <?php foreach($dept_data as $key => $ud) { ?>
                    <?php if($key == $filter_dept) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td>
                <select name="filter_unit" style="width:60px;">
                  <?php foreach($unit_data as $key => $ud) { ?>
                    <?php if($key == $filter_unit) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td>
                <input type="text" id="filter_date" name="filter_date" value="<?php echo $filter_date; ?>" class="date"  style="width:67px;" />&nbsp;
                <input type="text" id="filter_date_to" name="filter_date_to" value="<?php echo $filter_date_to; ?>" class="date"  style="width:67px;" />
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>
                <select name="filter_type">
                  <?php foreach($types as $key => $ud) { ?>
                    <?php if($key == $filter_type) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td>
                <select name="filter_px_record_type">
                  <?php foreach($px_record_types as $key => $ud) { ?>
                    <?php if($key == $filter_px_record_type) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td>
                <select name="filter_approval_1">
                  <?php foreach($approves as $key => $ud) { ?>
                    <?php if($key == $filter_approval_1) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td>
                <select name="filter_proc">
                  <?php foreach($process as $key => $ud) { ?>
                    <?php if($key == $filter_proc) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td align="right">
                <a onclick="filter();" class="button"><?php echo $button_filter; ?></a>
              </td>
            </tr>
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
              <?php if ($manual_list) { ?>
              <?php foreach ($manual_list as $employee) { ?>
              <tr>
                <td style="text-align: center;display: none;"><?php if ($employee['selected']) { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $employee['id']; ?>" checked="checked" />
                  <?php } else { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $employee['id']; ?>" />
                  <?php } ?></td>
                <td class="left"><?php echo $employee['name']; ?></td>
                <td class="left"><?php echo $employee['dept_name']; ?></td>
                <td class="left"><?php echo $employee['unit']; ?></td>
                <td class="left"><?php echo $employee['dot']; ?></td>
                <td class="left"><?php echo $employee['date']; ?></td>
                <td class="left"><?php echo $employee['in_time']; ?></td>
                <td class="left"><?php echo $employee['out_time']; ?></td>
                <td class="left"><?php echo $employee['modification']; ?></td>
                <td class="left"><?php echo $employee['punch_reason']; ?></td>
                <td class="left"><?php echo $employee['type']; ?></td>
                <td class="left"><?php echo $employee['px_record_type']; ?></td>
                <td class="left"><?php echo $employee['approval_1']; ?></td>
                <td class="left"><?php echo $employee['proc_stat']; ?></td>
                <td class="right"><?php foreach ($employee['action'] as $action) { ?>
                  <?php if($action['href'] == '') { ?>
                    <p style="cursor:default;"><?php echo $action['text']; ?></p>
                  <?php } else { ?>
                    [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                  <?php } ?>
                  <?php } ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="center" colspan="15"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </form>
          </tbody>
        </table>
      <?php } else { ?>
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;display: none;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left" style="width: 105px;">
                <?php echo 'Name'; ?>
              </td>
              <td>
                <a><?php echo "Department"; ?></a>
              </td>
              <td>
                <a><?php echo "Location"; ?></a>
              </td>
              <td class="left" style="width:80px;">
                <?php echo 'DOI'; ?>
              </td>
              <td class="left" style="width: 105px;">
                <?php echo 'Date From'; ?>
              </td>
              <td style="width: 105px;">
                <a><?php echo "Date To"; ?></a>
              </td>
              <td>
                <a><?php echo "Reason"; ?></a>
              </td>
              <td>
                <a><?php echo "Type"; ?></a>
              </td>
              <td style="width:70px;">
                <a><?php echo "Approval Status"; ?></a>
              </td>
              <td>
                <a><?php echo "Process"; ?></a>
              </td>
              <td class="right"><?php echo 'Action'; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr class="filter">
              <td style="display: none;">&nbsp;</td>
              <td>
                <input type="text" id="filter_name" name="filter_name" value="<?php echo $filter_name; ?>"  style="width:150px;" />
                <input type="hidden" id="filter_name_id" name="filter_name_id" value="<?php echo $filter_name_id; ?>" />
              </td>
              <td>
                <select name="filter_dept" style="width:60px;">
                  <?php foreach($dept_data as $key => $ud) { ?>
                    <?php if($key == $filter_dept) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td>
                <select name="filter_unit" style="width:60px;">
                  <?php foreach($unit_data as $key => $ud) { ?>
                    <?php if($key == $filter_unit) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td>
                <input type="text" id="filter_date" name="filter_date" value="<?php echo $filter_date; ?>" class="date"  style="width:67px;" />&nbsp;
                <input type="text" id="filter_date_to" name="filter_date_to" value="<?php echo $filter_date_to; ?>" class="date"  style="width:67px;" />
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>
                <select name="filter_type">
                  <?php foreach($types as $key => $ud) { ?>
                    <?php if($key == $filter_type) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td>
                <select name="filter_approval_1">
                  <?php foreach($approves as $key => $ud) { ?>
                    <?php if($key == $filter_approval_1) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td>
                <select name="filter_proc">
                  <?php foreach($process as $key => $ud) { ?>
                    <?php if($key == $filter_proc) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td align="right">
                <a onclick="filter();" class="button"><?php echo $button_filter; ?></a>
              </td>
            </tr>
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
              <?php if ($manual_list) { ?>
              <?php foreach ($manual_list as $employee) { ?>
              <tr>
                <td style="text-align: center;display: none;"><?php if ($employee['selected']) { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $employee['batch_id']; ?>" checked="checked" />
                  <?php } else { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $employee['batch_id']; ?>" />
                  <?php } ?></td>
                <td class="left"><?php echo $employee['name']; ?></td>
                <td class="left"><?php echo $employee['dept_name']; ?></td>
                <td class="left"><?php echo $employee['unit']; ?></td>
                <td class="left"><?php echo $employee['dot']; ?></td>
                <td class="left"><?php echo $employee['date_from']; ?></td>
                <td class="left"><?php echo $employee['date_to']; ?></td>
                <td class="left"><?php echo $employee['punch_reason']; ?></td>
                <td class="left"><?php echo $employee['type']; ?></td>
                <td class="left"><?php echo $employee['approval_1']; ?></td>
                <td class="left"><?php echo $employee['proc_stat']; ?></td>
                <td class="right"><?php foreach ($employee['action'] as $action) { ?>
                  <?php if($action['href'] == '') { ?>
                    <p style="cursor:default;"><?php echo $action['text']; ?></p>
                  <?php } else { ?>
                    [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                  <?php } ?>
                  <?php } ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="center" colspan="12"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </form>
          </tbody>
        </table>    
      <?php } ?>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=transaction/manualpunch_ess_admin&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  if (filter_name) {
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    }
  }

  var filter_date = $('input[name=\'filter_date\']').attr('value');
  if (filter_date) {
    url += '&filter_date=' + encodeURIComponent(filter_date);
  }

  var filter_date_to = $('input[name=\'filter_date_to\']').attr('value');
  if (filter_date_to) {
    url += '&filter_date_to=' + encodeURIComponent(filter_date_to);
  }

  var filter_approval_1 = $('select[name=\'filter_approval_1\']').attr('value');
  if (filter_approval_1) {
    url += '&filter_approval_1=' + encodeURIComponent(filter_approval_1);
  }

  var filter_proc = $('select[name=\'filter_proc\']').attr('value');
  if (filter_proc) {
    url += '&filter_proc=' + encodeURIComponent(filter_proc);
  }

  var filter_dept = $('select[name=\'filter_dept\']').attr('value');
  if (filter_dept) {
    url += '&filter_dept=' + encodeURIComponent(filter_dept);
  }

  var filter_unit = $('select[name=\'filter_unit\']').attr('value');
  if (filter_unit) {
    url += '&filter_unit=' + encodeURIComponent(filter_unit);
  }

  var filter_px_record_type = $('select[name=\'filter_px_record_type\']').attr('value');
  if (filter_px_record_type) {
    url += '&filter_px_record_type=' + encodeURIComponent(filter_px_record_type);
  }

  var filter_type = $('select[name=\'filter_type\']').attr('value');
  if (filter_type) {
    url += '&filter_type=' + encodeURIComponent(filter_type);
  }

  location = url;
  return false;
}
//--></script>
<script type="text/javascript"><!--
$('#filter_name, #filter_date, #filter_date_to').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});

$.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
    var self = this, currentCategory = '';
    $.each(items, function(index, item) {
      if (item.category != currentCategory) {
        //ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
        currentCategory = item.category;
      }
      self._renderItem(ul, item);
    });
  }
});
//--></script> 
<script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=transaction/leave_ess_dept/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.employee_id,
            emp_code: item.emp_code
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    $('input[name=\'filter_name_id\']').val(ui.item.emp_code);
            
    return false;
  },
  focus: function(event, ui) {
        return false;
    }
});
//--></script>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--
$('.date').datepicker({
    dateFormat: 'yy-mm-dd'
});
//--></script>
<?php echo $footer; ?>