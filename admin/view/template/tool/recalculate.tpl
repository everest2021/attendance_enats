<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/recalculate.png" alt="" /> <?php echo $heading_title; ?></h1>
    </div>
    <div class="content sales-report">
      <table class="form" style="width: 100%;">
        <tr>
          <td style="width:1%;"><?php echo "Date Start"; ?>
            <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start" size="8" />
          </td>
          <td style="width:1%;"><?php echo "Date End"; ?>
            <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" id="date-end" size="8" />
          </td>
          <td style="width:1%;"><?php echo "Name"; ?>
            <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" size="22" />
            <input type="hidden" name="filter_name_id" value="<?php echo $filter_name_id; ?>" id="filter_name_id" />
          </td>
          <td style="width:1%;display: none;">Department
            <select name="filter_department" id="filter_department" style="width: 100px;">
              <?php foreach($department_data as $key => $ud) { ?>
                <?php if($key == $filter_department) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td style="width:1%;display: none;">Unit
            <select name="filter_unit" id="filter_unit" style="width: 100px;">
              <?php foreach($unit_data as $key => $ud) { ?>
                <?php if($key == $filter_unit) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td style="text-align: right;">
            <a style="padding: 5px 10px;" onclick="filter();" id="filter" class="button"><?php echo 'Recalculate'; ?></a>
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=tool/recalculate/recalculate_data&token=<?php echo $token; ?>';
  var filter_name = $('#filter_name').val();
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('#filter_name_id').val();
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }

  var filter_date_start = $('#date-start').val();
  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('#date-end').val();
  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var filter_department = $('#filter_department').val();
  if (filter_department && filter_department != '0') {
    url += '&filter_department=' + encodeURIComponent(filter_department);
  }

  var filter_unit = $('#filter_unit').val();
  if (filter_unit && filter_unit != '0') {
    url += '&filter_unit=' + encodeURIComponent(filter_unit);
  }
  url += '&once=1';
  //alert(url);
  //return false;
  location = url;
  return false;
}

$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/employee/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.emp_code
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    $('input[name=\'filter_name_id\']').val(ui.item.value);
    return false;
  },
  focus: function(event, ui) {
    return false;
  }
});
$('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
$('#date-end').datepicker({dateFormat: 'yy-mm-dd'});

//--></script>
<?php echo $footer; ?>