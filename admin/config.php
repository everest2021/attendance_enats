<?php
session_start();
date_default_timezone_set("Asia/Kolkata");
// HTTP
define('HTTP_SERVER', 'http://79.143.181.160/attendance_punch/admin/');
define('HTTP_CATALOG', 'http://79.143.181.160/attendance_punch/');

// HTTPS
define('HTTPS_SERVER', 'http://79.143.181.160/attendance_punch/admin/');
define('HTTPS_CATALOG', 'http://79.143.181.160/attendance_punch/');

// DIR
define('DIR_APPLICATION', '/var/www/html/attendance_punch/admin/');
define('DIR_SYSTEM', '/var/www/html/attendance_punch/system/');
define('DIR_DATABASE', '/var/www/html/attendance_punch/system/database/');
define('DIR_LANGUAGE', '/var/www/html/attendance_punch/admin/language/');
define('DIR_TEMPLATE', '/var/www/html/attendance_punch/admin/view/template/');
define('DIR_CONFIG', '/var/www/html/attendance_punch/system/config/');
define('DIR_IMAGE', '/var/www/html/attendance_punch/image/');
define('DIR_CACHE', '/var/www/html/attendance_punch/system/cache/');
define('DIR_DOWNLOAD', '/var/www/html/attendance_punch/download/');
define('DIR_LOGS', '/var/www/html/attendance_punch/system/logs/');
define('DIR_CATALOG', '/var/www/html/attendance_punch/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'P!h@r@21');
define('DB_DATABASE', 'db_attendance');
define('DB_PREFIX', 'oc_');
//}
?>
