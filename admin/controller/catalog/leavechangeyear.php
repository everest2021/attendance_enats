<?php    
class ControllerCatalogLeavechangeyear extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('catalog/leave');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/leave');
		$this->load->model('catalog/employee');

		$this->getList();
	}

	public function insert() {
		$this->language->load('catalog/leave');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/leave');
		$this->load->model('catalog/employee');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			//$emp_data = $this->model_transaction_transaction->getEmployees_dat($data['e_name_id']);
			$this->session->data['success'] = 'You have Inserted the Transaction';	
			//echo 'out';exit;
			$this->redirect($this->url->link('transaction/manualpunch', 'token=' . $this->session->data['token'].'&transaction_id='.$transaction_id, 'SSL'));
		}

		$this->getForm();
	}

	protected function getList() {
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/leave', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/leave/insert', 'token=' . $this->session->data['token'] , 'SSL');
		$this->data['delete'] = $this->url->link('catalog/leave/delete', 'token=' . $this->session->data['token'] , 'SSL');	

		$this->data['leaves'] = array();

		$current_year = $this->model_catalog_leave->getEmployeeLeaveYearData();	
		if($current_year == '2016') {
			$this->session->data['warning'] = 'Year already closed';	

			$this->redirect($this->url->link('catalog/leave', 'token=' . $this->session->data['token'], 'SSL'));			
		}
		$new_year = $current_year + 1;
		$active_employee = $this->model_catalog_employee->getActiveEmployee();

		foreach($active_employee as $data) {
			// echo '<pre>';
			// print_r($data);
			$leave_employee_data = $this->model_catalog_leave->getEmployeeLeaveData($data['emp_code']);	
			// echo '<pre>';
			// print_r($leave_employee_data);
			
			if($leave_employee_data) {
				$leave_employee_data['current_year'] = $current_year;
				$leave_employee_data['new_year'] = $new_year;
				
				if($data['group'] == 'OFFICIALS'){
					$leave_employee_data['pl_acc'] = $leave_employee_data['pl_bal'] + 30; 
					$leave_employee_data['pl_bal'] = $leave_employee_data['pl_acc']; 
					
					$leave_employee_data['cl_acc'] = 7; 
					$leave_employee_data['cl_bal'] = $leave_employee_data['cl_acc']; 
					
					$leave_employee_data['sl_acc'] = $leave_employee_data['sl_bal'] + 10; 
					$leave_employee_data['sl_bal'] = $leave_employee_data['sl_acc']; 
				} else {
					$leave_employee_data['pl_acc'] = $leave_employee_data['pl_bal'] + 30; 
					$leave_employee_data['pl_bal'] = $leave_employee_data['pl_acc']; 
					
					$leave_employee_data['cl_acc'] = 15; 
					$leave_employee_data['cl_bal'] = $leave_employee_data['cl_acc']; 
					
					$leave_employee_data['sl_acc'] = $leave_employee_data['sl_bal'] + 4; 
					$leave_employee_data['sl_bal'] = $leave_employee_data['sl_acc']; 
				}

				// echo '<pre>';
				// print_r($leave_employee_data);
				$this->model_catalog_leave->copyLeaveEmpData($leave_employee_data);
			} else {
				$data['current_year'] = $current_year;
				$data['new_year'] = $new_year;
				$this->model_catalog_leave->insertBlankEmpData($data);
			}
			
		}

		$this->model_catalog_leave->changeStatus($current_year);	

		$this->session->data['success'] = 'Year changed to '.$new_year;	

		$this->redirect($this->url->link('catalog/leave', 'token=' . $this->session->data['token'], 'SSL'));
		
	}

}
?>