<?php    
class ControllerCatalogHoliday extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('catalog/holiday');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/holiday');

		$this->getList();
	}

	public function insert() {
		
		$this->language->load('catalog/holiday');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/holiday');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// echo '<pre>';
			// print_r($this->request->post);

		
			$this->model_catalog_holiday->addholiday($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/holiday', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {

		$this->language->load('catalog/holiday');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/holiday');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			// echo "<pre>";
			// print_r($this->request->post);
			// exit();
			$this->model_catalog_holiday->editholiday($this->request->get['holiday_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/holiday', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('catalog/holiday');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/holiday');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $holiday_id) {
				$this->model_catalog_holiday->deleteholiday($holiday_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/holiday', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		} elseif(isset($this->request->get['holiday_id']) && $this->validateDelete()){
			$this->model_catalog_holiday->deleteholiday($this->request->get['holiday_id']);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/holiday', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'date';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/holiday', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/holiday/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/holiday/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	

		$this->data['holidays'] = array();

		$data = array(
			'filter_name' => $filter_name,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$holiday_total = $this->model_catalog_holiday->getTotalholidays($data);

		$results = $this->model_catalog_holiday->getholidays($data);

		// echo '<pre>';
		// print_r($results);
		// exit;

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/holiday/update', 'token=' . $this->session->data['token'] . '&holiday_id=' . $result['holiday_id'] . $url, 'SSL')
			);

			// $action[] = array(
			// 	'text' => 'Delete',
			// 	'href' => $this->url->link('catalog/holiday/delete', 'token=' . $this->session->data['token'] . '&holiday_id=' . $result['holiday_id'] . $url, 'SSL')
			// );

			$this->data['holidays'][] = array(
				'holiday_id' => $result['holiday_id'],
				'name' => $result['name'],
				'date'            => $result['date'],
				'selected'        => isset($this->request->post['selected']) && in_array($result['holiday_id'], $this->request->post['selected']),
				'action'          => $action
			);
		}

		$this->data['token'] = $this->session->data['token'];	

		$this->data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		$this->data['sort_name'] = $this->url->link('catalog/holiday', 'token=' . $this->session->data['token'] . '&sort=date' . $url, 'SSL');
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		$pagination = new Pagination();
		$pagination->total = $holiday_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/holiday', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['filter_name'] = $filter_name;
		
		$this->template = 'catalog/holiday_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} elseif(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}

		if (isset($this->error['date'])) {
			$this->data['error_date'] = $this->error['date'];
		} else {
			$this->data['error_date'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/holiday', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['holiday_id'])) {
			$this->data['action'] = $this->url->link('catalog/holiday/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
			$this->data['process_holiday'] = '';
		} else {
			$this->data['action'] = $this->url->link('catalog/holiday/update', 'token=' . $this->session->data['token'] . '&holiday_id=' . $this->request->get['holiday_id'] . $url, 'SSL');
			$this->data['process_holiday'] = $this->url->link('catalog/holiday/process_holiday', 'token=' . $this->session->data['token'] . $url . '&holiday_id=' . $this->request->get['holiday_id'], 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/holiday', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['holiday_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$holiday_info = $this->model_catalog_holiday->getholiday($this->request->get['holiday_id']);
		}
		// echo "<pre>";
		// print_r($holiday_info);
		// exit();

		$this->data['token'] = $this->session->data['token'];
		$this->load->model('report/attendance');

		if (isset($this->request->post['holi_datas'])) {
			$unitss = $this->db->query("SELECT * FROM `oc_unit`")->rows;
			foreach ($this->request->post['holi_datas'] as $key => $value) {
				foreach ($unitss as $ukey => $uvalue) {
			   	    if ($key == $uvalue['unit_id']) {
			   	        $this->data['holi_datas'][$uvalue['unit_id']] = $value;
			   	   	} else {
						$this->data['holi_datas'][$uvalue['unit_id']]= array();
					}
				} 
			}
		} elseif (!empty($holiday_info['holiday_id'])) {
			$units = $this->db->query("SELECT * FROM `oc_unit`")->rows;
			foreach ($units as $key => $unit) {
				$unit_values = $this->db->query("SELECT * FROM `oc_holiday_loc` where holiday_id='".$this->request->get['holiday_id']."' AND location_id = '".$unit['unit_id']."' ");
				if ($unit_values->num_rows > 0) { 
					if($unit_values->row['value'] != ''){
						// echo '<pre>';
						// print_r(unserialize($unit_values->row['value']));
						// exit;
						$this->data['holi_datas'][$unit['unit_id']] = unserialize($unit_values->row['value']);
						foreach($this->data['holi_datas'][$unit['unit_id']] as $key => $value){
							$this->data['holi_datas'][$unit['unit_id']][$key] = html_entity_decode(strtolower(trim($value)));
						}
					} else {
						$this->data['holi_datas'][$unit['unit_id']] = array();	
					}
				} else {
					$this->data['holi_datas'][$unit['unit_id']] = array();
				}
			}
		} else {
			$units = $this->db->query("SELECT * FROM `oc_unit`")->rows;
			foreach ($units as $key => $unit) {
				$this->data['holi_datas'][$unit['unit_id']] = array();
			}
		}

		$this->load->model('catalog/department');
		$department_datas = $this->model_catalog_department->getDepartments();
		$department_data = array();
		foreach ($department_datas as $dkey => $dvalue) {
			$dvalue['d_name'] = html_entity_decode($dvalue['d_name']);
			$department_data[$dvalue['department_id']] = $dvalue['d_name'];
		}
		$this->data['department_data'] = $department_data;
		
		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($holiday_info)) {
			$this->data['name'] = $holiday_info['name'];
		} else {	
			$this->data['name'] = '';
		}

		if (isset($this->request->post['date'])) {
			$this->data['date'] = $this->request->post['date'];
		} elseif (!empty($holiday_info)) {
			$this->data['date'] = $holiday_info['date'];
		} else {	
			$this->data['date'] = '';
		}

		$this->load->model('catalog/employee');

	   	$results = $this->model_catalog_employee->getUnit();
	   	// echo '<pre>';
	   	// print_r($results);
	   	// exit();
	    $unit_data = array();
		foreach ($results as $result) {
			$unit_data[$result['unit_id']]['name']= $result['unit'];
			$unit_data[$result['unit_id']]['id']= $result['unit_id'];
			
			if(isset($this->data['holi_datas'][$result['unit_id']]) && !empty($this->data['holi_datas'][$result['unit_id']]) ){
				$unit_data[$result['unit_id']]['selected'] = 1;	
			} else {
				$unit_data[$result['unit_id']]['selected'] = 0;	
			}
		}
		$this->data['unit_data'] = $unit_data;



		$this->template = 'catalog/holiday_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}  

	protected function validateForm() {
		$this->load->model('catalog/halfday');

		if (!$this->user->hasPermission('modify', 'catalog/holiday')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = 'Please Enter Valid Holiday Name';
		}

		if($this->request->post['date'] != '' && !isset($this->request->get['holiday_id'])){
			$is_exist = $this->model_catalog_halfday->getholiday_exist($this->request->post['date']);
			if($is_exist == 1){
				$this->error['warning'] = 'Holiday With Same Date Present';
			}
		}

		if ($this->request->post['date'] != '' && isset($this->request->post['holiday_id'])) {
			$is_exist_edit = $this->model_catalog_halfday->getholiday_existedit($this->request->post['date'],$this->request->post['holiday_id']);
			if($is_exist_edit == 1){
				$this->error['warning'] = 'Holiday With Same Date Present';
			} 
		}

		$date = $this->request->post['date'];

		

		
		

		// if ((utf8_strlen($this->request->post['holiday_code']) < 1) || (utf8_strlen($this->request->post['holiday_code']) > 64)) {
		// 	$this->error['holiday_code'] = 'holiday Code is Required';
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}



	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/holiday')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// $this->load->model('catalog/horse');

		// if(isset($this->request->post['selected'])){
		// 	foreach ($this->request->post['selected'] as $holiday_id) {
		// 		$horse_total = $this->model_catalog_horse->getTotalhorsesByholidayId($holiday_id);

		// 		if ($horse_total) {
		// 			$this->error['warning'] = sprintf($this->language->get('error_horse'), $horse_total);
		// 		}	
		// 	}
		// } elseif(isset($this->request->get['holiday_id'])){
		// 	$horse_total = $this->model_catalog_horse->getTotalhorsesByholidayId($this->request->get['holiday_id']);

		// 	if ($horse_total) {
		// 		$this->error['warning'] = sprintf($this->language->get('error_horse'), $horse_total);
		// 	}
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}  
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/holiday');

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_holiday->getholidays($data);

			foreach ($results as $result) {
				$json[] = array(
					'holiday_id' => $result['holiday_id'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}

	public function process_holiday() {
		$this->language->load('catalog/holiday');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/holiday');

		// echo '<pre>';
		// print_r($this->request->get);
		// exit;

		$in = 0;
		if(isset($this->request->get['holiday_id'])){
			$holiday_id = $this->request->get['holiday_id'];
			$holiday_dates = $this->db->query("SELECT `date` FROM `oc_holiday` WHERE `holiday_id` = '".$holiday_id."' ");
			if($holiday_dates->num_rows > 0){
				$date = $holiday_dates->row['date'];
				if($date < date('Y-m-d')){
					$in = 1;
					$month = date('n', strtotime($date));
					$year = date('Y', strtotime($date));
					$day = date('j', strtotime($date));
					$sql = "SELECT `transaction_id`, `emp_id`, `emp_name` FROM `oc_transaction` WHERE `date` = '".$date."' ";
					$resultss = $this->db->query($sql)->rows;
					// echo '<pre>';
					// print_r($resultss);
					// exit;
					foreach($resultss as $rkeys => $rvalues){
						$shift_sql = "SELECT `".$day."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalues['emp_id']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
						$shift_datas = $this->db->query($shift_sql);
						if($shift_datas->num_rows > 0){
							$shift_data = $shift_datas->row[$day];
							$shift_exp = explode('_', $shift_data);
							if($shift_exp[0] == 'H'){
								$tran_datas = $this->db->query("SELECT `leave_status`, `firsthalf_status`, `secondhalf_status` FROM `oc_transaction` WHERE `transaction_id` = '".$rvalues['transaction_id']."' ");
								if($tran_datas->num_rows > 0){
									$tran_data = $tran_datas->row;
									if($tran_data['leave_status'] != '1'){
										$update_sql = "UPDATE `oc_transaction` SET `holiday_id` = '1', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `absent_status` = '0' WHERE `transaction_id` = '".$rvalues['transaction_id']."' AND `emp_id` = '".$rvalues['emp_id']."' ";
										// echo $update_sql;
										// echo '<br />';
										// exit;
										$this->db->query($update_sql);
									}
								}
							}
						}	
					}
				} else {
					$this->session->data['warning'] = 'Post Dated Holiday Cannot be Processed';
				}
			}
		}
		//echo 'out';exit;
		if($in == 1){
			$this->session->data['success'] = 'Holiday Processed Successfully';
		}
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->redirect($this->url->link('catalog/holiday/update', 'token=' . $this->session->data['token'] . $url.'&holiday_id=' . $this->request->get['holiday_id'], 'SSL'));

	}	
}
?>