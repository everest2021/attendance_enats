<?php  
class ControllerCatalogDevicecommands extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('catalog/device_commands');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/device_commands');

		$this->getList();
	}

	public function insert() {
		$this->language->load('catalog/device_commands');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/device_commands');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_device_commands->adddevice_commands($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/device_commands', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/device_commands');

		$this->document->setTitle('device_commands');

		$this->load->model('catalog/device_commands');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_device_commands->editdevice_commands($this->request->get['id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/device_commands', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('catalog/device_commands');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/device_commands');

		if (isset($this->request->post['selected']) ) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_device_commands->deletedevice_commands($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/device_commands', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_device_serial_no'])) {
			$filter_device_serial_no = $this->request->get['filter_device_serial_no'];
		} else {
			$filter_device_serial_no = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_device_serial_no'])) {
			$url .= '&filter_device_serial_no=' . $this->request->get['filter_device_serial_no'];
		}


		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/device_commands', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/device_commands/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/device_commands/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['device_commander'] = array();

		$data = array(
			'filter_device_serial_no' => $filter_device_serial_no,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$device_commands_total = $this->model_catalog_device_commands->getTotaldevice_commander();

		$results = $this->model_catalog_device_commands->getdevice_commander($data);

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/device_commands/update', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, 'SSL')
			);

			$this->data['device_commander'][] = array(
				'id'    => $result['id'],
				'device_serial_no'   => $result['device_serial_no'],
				'command'   => $result['command'],
				'command_name'   => $result['command_name'],
				'status'     =>($result['status'] == 1 ? 'Done' : 'Pending'),
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['added_date'])),
				'selected'   => isset($this->request->post['selected']) && in_array($result['id'], $this->request->post['selected']),
				'action'     => $action
			);
		}

		$this->data['token'] = $this->session->data['token'];

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_device_serial_no'] = $this->language->get('column_device_serial_no');
		$this->data['column_command'] = $this->language->get('column_command');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_date_added'] = $this->language->get('column_date_added');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_device_serial_no'])) {
			$url .= '&filter_device_serial_no=' . $this->request->get['filter_device_serial_no'];
		}

		$this->data['sort_name'] = $this->url->link('catalog/device_commands', 'token=' . $this->session->data['token'] . '&sort=device_serial_no' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('catalog/device_commands', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');
		$this->data['sort_command'] = $this->url->link('catalog/device_commands', 'token=' . $this->session->data['token'] . '&sort=command' . $url, 'SSL');
		$this->data['sort_date_added'] = $this->url->link('catalog/device_commands', 'token=' . $this->session->data['token'] . '&sort=date_added' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $device_commands_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/device_commands', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['filter_device_serial_no'] = $filter_device_serial_no;

		$this->template = 'catalog/device_commands_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = 'Device Commands';

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');

		$this->data['entry_device_serial_no'] = $this->language->get('entry_device_serial_no');
		$this->data['entry_command'] = $this->language->get('entry_command');
		// $this->data['entry_confirm'] = $this->language->get('entry_confirm');
		// $this->data['entry_firstname'] = $this->language->get('entry_firstname');
		// $this->data['entry_lastname'] = $this->language->get('entry_lastname');
		// $this->data['entry_email'] = $this->language->get('entry_email');
		// $this->data['entry_user_group'] = $this->language->get('entry_user_group');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_captcha'] = $this->language->get('entry_captcha');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['device_serial_no'])) {
			$this->data['error_device_serial_no'] = $this->error['device_serial_no'];
		} else {
			$this->data['error_device_serial_no'] = '';
		}

		if (isset($this->error['emp_code_error'])) {
			$this->data['emp_code_error'] = $this->error['emp_code_error'];
		} else {
			$this->data['emp_code_error'] = '';
		}

		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Device Commands',
			'href'      => $this->url->link('catalog/device_commands/update', 'token=' . $this->session->data['token'] , 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['id'])) {
			$this->data['action'] = $this->url->link('catalog/device_commands/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/device_commands/update', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/device_commands', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$device_commands_info = $this->model_catalog_device_commands->getdevice_commands($this->request->get['id']);
		}

		$this->data['cmds'] = array(
			'BUFD' => 'Block Users From Device',
			'UBUFD' => 'Un-Block Users From Device',

		);

		if (isset($this->request->post['device_serial_no'])) {
			$this->data['device_serial_no'] = $this->request->post['device_serial_no'];
		} elseif (!empty($device_commands_info)) {
			$this->data['device_serial_no'] = $device_commands_info['device_serial_no'];
		} else {
			$this->data['device_serial_no'] = '';
		}

		if (isset($this->request->post['select_cmd'])) {
			$this->data['select_cmd'] = $this->request->post['select_cmd'];
		} elseif (!empty($device_commands_info)) {
			$this->data['select_cmd'] = $device_commands_info['short_cmd_name'];
		} else {
			$this->data['select_cmd'] = '';
		}

		if (isset($this->request->post['emp_code'])) {
			$this->data['emp_code'] = $this->request->post['emp_code'];
		} elseif (!empty($device_commands_info)) {
			$this->data['emp_code'] = $device_commands_info['emp_code'];
		} else {
			$this->data['emp_code'] = '';
		}

		if (isset($this->request->post['added_date'])) {
			$this->data['added_date'] = $this->request->post['added_date'];
		} elseif (!empty($device_commands_info)) {
			$this->data['added_date'] = $device_commands_info['added_date'];
		} else {
			$this->data['added_date'] = '';
		}
		
		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($device_commands_info)) {
			$this->data['status'] = $device_commands_info['status'];
		} else {
			$this->data['status'] = '';
		}

		$this->template = 'catalog/device_commands_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/device_commands')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		

		if ((utf8_strlen($this->request->post['device_serial_no']) < 1) || (utf8_strlen($this->request->post['device_serial_no']) > 32)) {
			$this->error['device_serial_no'] = "Device Enter Serial No";
		}

		if ((utf8_strlen($this->request->post['emp_code']) < 1) || (utf8_strlen($this->request->post['emp_code']) > 32)) {
			$this->error['emp_code_error'] ="Please Enter Employee Code";
		}

		
		

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/device_commands')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// foreach ($this->request->post['selected'] as $id) {
		// 	if ($this->user->getId() == $id) {
		// 		$this->error['warning'] = $this->language->get('error_account');
		// 	}
		// }

		if (!$this->error) {
			return true;
		} else { 
			return false;
		}
	}
public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/device_commands');

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_device_commands->getdevice_commander($data);

			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'], 
					'device_serial_no'            => strip_tags(html_entity_decode($result['device_serial_no'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['device_serial_no'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>