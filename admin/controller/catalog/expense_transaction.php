<?php
class ControllerCatalogExpensetransaction extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('catalog/expense_transaction');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/expense_transaction');

		$this->getList();
		
	}

	public function insert() {
		$this->language->load('catalog/expense_transaction');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/expense_transaction');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_expense_transaction->addexpensetransaction($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/expense_transaction', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/expense_transaction');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/expense_transaction');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_expense_transaction->editexpensetransaction($this->request->get['id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/expense_transaction', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() { 
		$this->language->load('catalog/expense_transaction');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/expense_transaction');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_expense_transaction->deleteexpensetransaction($id);	
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/expense_transaction', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';//date('d-m-Y');
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';//date('d-m-Y');
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}
		if (isset($this->request->get['filter_id'])) {
			$filter_id = $this->request->get['filter_id'];
		} else {
			$filter_id = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';
		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_id'])) {
			$url .= '&filter_id=' . $this->request->get['filter_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}	

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/expense_transaction', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/expense_transaction/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/expense_transaction/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	
			
		$this->data['expense_transactions'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start, 
			'filter_date_end'	     => $filter_date_end, 
			'filter_name'            => $filter_name,
			'filter_id'            => $filter_id,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$expense_transaction_total = $this->model_catalog_expense_transaction->getTotalexpensetransactions($data);

		$results = $this->model_catalog_expense_transaction->getexpensetransactions($data);

		foreach ($results as $result) {

		//echo '<pre>';print_r($results);exit();
							
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/expense_transaction/update', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, 'SSL')
			);	
			
			$this->data['expense_transactions'][] = array(
				'id'				=>$result['id'],
				'emp_id'			=>$result['emp_code'],
				'emp_name'			=>	$result['emp_name'],
				'date'   			=> date('d-m-Y', strtotime($result['date'])),
				'amount'			=>$result['amount'],
				'selected'      	=> isset($this->request->post['selected']) && in_array($result['id'], $this->request->post['selected']),
				'action'        	=> $action
			);
		}

		$this->data['token'] = $this->session->data['token'];

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_date'] = $this->language->get('column_date');
		//$this->data['column_end_date'] = $this->language->get('column_end_date');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';
		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}


		if (isset($this->request->get['filter_id'])) {
			$url .= '&filter_id=' . $this->request->get['filter_id'];
		} 
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_name'] = $this->url->link('catalog/expense_transaction', 'token=' . $this->session->data['token'] . '&sort=emp_name' . $url, 'SSL');
		$this->data['sort_date'] = $this->url->link('catalog/expense_transaction', 'token=' . $this->session->data['token'] . '&sort=date' . $url, 'SSL');
		

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $expense_transaction_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/expense_transaction', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();				

		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;		
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_id'] = $filter_id;
		$this->data['sort'] = $sort; 
		$this->data['order'] = $order;

		$this->template = 'catalog/expense_transaction_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_select_all'] = $this->language->get('text_select_all');
		$this->data['text_unselect_all'] = $this->language->get('text_unselect_all');

		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_date'] = $this->language->get('entry_date');
		$this->data['entry_end_date'] = $this->language->get('entry_end_date');
		$this->data['entry_access'] = $this->language->get('entry_access');
		$this->data['entry_modify'] = $this->language->get('entry_modify');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->error['filter_name'])) {
			$this->data['error_name'] = $this->error['filter_name'];
		} else {
			$this->data['error_name'] = '';
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}


		if (isset($this->error['date'])) {
			$this->data['error_date'] = $this->error['date'];
		} else {
			$this->data['error_date'] = '';
		}

		if (isset($this->error['amount'])) {
			$this->data['error_amount'] = $this->error['amount'];
		} else {
			$this->data['error_amount'] = '';
		}

		$url = '';
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}



		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/expense_transaction', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);
		$this->data['token'] = $this->session->data['token'];
	
		if (!isset($this->request->get['id'])) {
			$this->data['action'] = $this->url->link('catalog/expense_transaction/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/expense_transaction/update', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/expense_transaction', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['id']) && $this->request->server['REQUEST_METHOD'] != 'POST') {
			$expense_transaction_info = $this->model_catalog_expense_transaction->getexpensetransaction($this->request->get['id']);
		}
		$this->data['token'] = $this->session->data['token'];

		if (isset($this->request->post['emp_name'])) {
			$this->data['emp_name'] = $this->request->post['emp_name'];
		} elseif (!empty($expense_transaction_info)) {
			$this->data['emp_name'] = $expense_transaction_info['emp_name'];
		} else {
			$this->data['emp_name'] = '';
		}

		if (isset($this->request->post['emp_code'])) {
			$this->data['emp_code'] = $this->request->post['emp_code'];
		} elseif (!empty($expense_transaction_info)) {
			$this->data['emp_code'] = $expense_transaction_info['emp_code'];
		} else {
			$this->data['emp_code'] = '';
		}
		
		if (isset($this->request->post['date'])) {
			$this->data['date'] = $this->request->post['date'];
		} elseif (!empty($expense_transaction_info)) {
			$this->data['date'] = date('d-m-Y', strtotime($expense_transaction_info['date']));
		} else {
			$this->data['date'] = '';
		}

		if (isset($this->request->post['amount'])) {
			$this->data['amount'] = $this->request->post['amount'];
		} elseif (!empty($expense_transaction_info)) {
			$this->data['amount'] = $expense_transaction_info['amount'] ;
		} else {
			$this->data['amount'] = '';
		}

		if (isset($this->request->post['narration'])) {
			$this->data['narration'] = $this->request->post['narration'];
		} elseif (!empty($expense_transaction_info)) {
			$this->data['narration'] = $expense_transaction_info['narration'] ;
		} else {
			$this->data['narration'] = '';
		}
		$this->data['filter_name'] = $filter_name;
		$this->template = 'catalog/expense_transaction_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/expense_transaction')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if ((utf8_strlen($this->request->post['emp_name']) < 1) || (utf8_strlen($this->request->post['emp_name']) > 64)) {
			$this->error['name'] = 'Please Enter Valid Name';
		} else {
			if($this->request->post['emp_code'] == '' || $this->request->post['emp_code'] == 0){
				$this->error['name'] = 'Please Enter Valid Name';
			}
		}
		if ((utf8_strlen($this->request->post['date']) < 1) || (utf8_strlen($this->request->post['date']) > 64)) {
			$this->error['date'] = 'Please Enter Valid Date';
		}
		if ((utf8_strlen($this->request->post['amount']) < 1) || (utf8_strlen($this->request->post['amount']) > 20)) {
			$this->error['amount'] = 'Please Enter Valid Amount';
		}

		// if ((utf8_strlen($this->request->post['project_name']) < 3) || (utf8_strlen($this->request->post['project_name']) > 64)) {
		// 	$this->error['project_name'] = $this->language->get('error_project_name');
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/expense_transaction')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/expense_transaction');

		// foreach ($this->request->post['selected'] as $id) {
		// 	$project_assignment_total = $this->model_catalog_project_assignment->getTotalprojectassignments($id);

		// 	if ($project_assignment_total) {
		// 		$this->error['warning'] = sprintf($this->language->get('error_project_assignment'), $project_assignment_total);
		// 	}
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['name'])) {
			$this->load->model('catalog/expense_transaction');

			if (isset($this->session->data['id']) && !$this->user->isAdmin()) {
				$id = $this->session->data['id'];
			} else {
				$id = '';
			}

			$data = array(
				'name' => $this->request->get['name'],
				'id' => $id,
				'start'       => 0,
				'limit'       => 10
			);

			$results = $this->model_catalog_expense_transaction->getexpensetransactions($data);
//			echo '<pre>';print_r($results);exit();
			foreach ($results as $result) {
				$json[] = array(
					'name' => $result['name'],
					'id' => $result['id'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}

}
?>