<?php    
class ControllerCatalogAttendancePunch extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('catalog/attendance_punch');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/attendance_punch');

		$this->getList();
	}

	

	public function update() {
		$this->language->load('catalog/attendance_punch');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/attendance_punch');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			$this->model_catalog_attendance_punch->editemployee($this->request->get['employee_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_code'])) {
				$url .= '&filter_code=' . $this->request->get['filter_code'];
			}

			// if (isset($this->request->get['filter_department'])) {
			// 	$url .= '&filter_department=' . $this->request->get['filter_department'];
			// }

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['filter_shift'])) {
				$url .= '&filter_shift=' . $this->request->get['filter_shift'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if(isset($this->request->get['return'])){
				$this->redirect($this->url->link('report/employee_data', 'token=' . $this->session->data['token'].'&h_name='.$this->request->post['name'].'&h_name_id='.$this->request->get['employee_id'], 'SSL'));
			} else {
				$this->redirect($this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . $url , 'SSL'));
			}
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('catalog/attendance_punch');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/attendance_punch');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $employee_id) {
				$this->model_catalog_attendance_punch->deleteemployee($employee_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_code'])) {
				$url .= '&filter_code=' . $this->request->get['filter_code'];
			}

			// if (isset($this->request->get['filter_department'])) {
			// 	$url .= '&filter_department=' . $this->request->get['filter_department'];
			// }

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['filter_shift'])) {
				$url .= '&filter_shift=' . $this->request->get['filter_shift'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/attendance_punch', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		} elseif(isset($this->request->get['employee_id']) && $this->validateDelete()){
			$this->model_catalog_attendance_punch->deleteemployee($this->request->get['employee_id']);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_code'])) {
				$url .= '&filter_code=' . $this->request->get['filter_code'];
			}

			// if (isset($this->request->get['filter_department'])) {
			// 	$url .= '&filter_department=' . $this->request->get['filter_department'];
			// }

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['filter_shift'])) {
				$url .= '&filter_shift=' . $this->request->get['filter_shift'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/attendance_punch', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	

	protected function getList() {
		$this->data['reporting_to_name'] = '';
		
		
		// echo '<pre>';
		// print_r($this->data['reporting_to_name']);
		// exit;

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_code'])) {
			$filter_code = $this->request->get['filter_code'];
		} else {
			$filter_code = '';
		}

		

		

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = '1';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'emp_code';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		
		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/attendance_punch', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/attendance_punch/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/attendance_punch/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	
		$this->data['export_depthead'] = $this->url->link('catalog/attendance_punch/export_depthead', 'token=' . $this->session->data['token'] . $url, 'SSL');	

		$this->data['employees'] = array();

		$data = array(
			'filter_name' => $filter_name,
			'filter_code' => $filter_code,
			
			'filter_status' => $filter_status,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$employee_total = $this->model_catalog_attendance_punch->getTotalemployees($data);

		$results = $this->model_catalog_attendance_punch->getemployees($data);
  

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/attendance_punch/update', 'token=' . $this->session->data['token'] . '&employee_id=' . $result['id'] . $url, 'SSL')
			);

 			
 			if($result['status'] == '1'){
 				$status = 'Active';
 			} else {
 				$status = 'In Active';
 			}

			$this->data['employees'][] = array(
				'employee_id' => $result['emp_id'],
				'punch_time'  => $result['punch_time'],
				'punch_date'  => date('d-M-Y', strtotime( $result['punch_date'])),
				'device_id'   => $result['device_id'],
				'selected'        => isset($this->request->post['selected']) && in_array($result['id'], $this->request->post['selected']),
				'action'          => $action
			);
		}

		$statuses[1] = 'Active';
		$statuses[2] = 'In active';
		$this->data['statuses'] = $statuses;

		$this->data['token'] = $this->session->data['token'];	

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_delete'] = $this->language->get('text_delete');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_trainer'] = $this->language->get('column_trainer');
		$this->data['column_action'] = $this->language->get('column_action');		

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		$this->data['button_filter'] = $this->language->get('button_filter');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		
		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['filter_trainer'])) {
			$url .= '&filter_trainer=' . $this->request->get['filter_trainer'];
		}

		if (isset($this->request->get['filter_trainer_id'])) {
			$url .= '&filter_trainer_id=' . $this->request->get['filter_trainer_id'];
		}

		$this->data['sort_name'] = $this->url->link('catalog/attendance_punch', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_code'] = $this->url->link('catalog/attendance_punch', 'token=' . $this->session->data['token'] . '&sort=emp_code' . $url, 'SSL');
		// $this->data['sort_department'] = $this->url->link('catalog/attendance_punch', 'token=' . $this->session->data['token'] . '&sort=department' . $url, 'SSL');
		$this->data['sort_unit'] = $this->url->link('catalog/attendance_punch', 'token=' . $this->session->data['token'] . '&sort=unit' . $url, 'SSL');
		$this->data['sort_shift'] = $this->url->link('catalog/attendance_punch', 'token=' . $this->session->data['token'] . '&sort=shift_id' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('catalog/attendance_punch', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		// if (isset($this->request->get['filter_department'])) {
		// 	$url .= '&filter_department=' . $this->request->get['filter_department'];
		// }

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['filter_shift'])) {
			$url .= '&filter_shift=' . $this->request->get['filter_shift'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['filter_trainer'])) {
			$url .= '&filter_trainer=' . $this->request->get['filter_trainer'];
		}

		if (isset($this->request->get['filter_trainer_id'])) {
			$url .= '&filter_trainer_id=' . $this->request->get['filter_trainer_id'];
		}

		$pagination = new Pagination();
		$pagination->total = $employee_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/attendance_punch', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_code'] = $filter_code;
	 
		$this->data['filter_status'] = $filter_status;

		$this->template = 'catalog/attendance_punch.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_employee_code'] = $this->language->get('entry_employee_code');
		$this->data['entry_card_no'] = $this->language->get('entry_card_no');
		$this->data['entry_device_serial_no'] = $this->language->get('entry_device_serial_no');
		$this->data['entry_device_employee_code'] = $this->language->get('entry_device_employee_code');
		$this->data['entry_remove'] = $this->language->get('entry_remove');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['tab_general'] = $this->language->get('tab_general');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}

		if (isset($this->error['card_number'])) {
			$this->data['error_card_number'] = $this->error['card_number'];
		} else {
			$this->data['error_card_number'] = '';
		}

		

		if (isset($this->error['status'])) {
			$this->data['error_status'] = $this->error['status'];
		} else {
			$this->data['error_status'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}


		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}


		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/attendance_punch', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (isset($this->request->get['employee_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$employee_info = $this->model_catalog_attendance_punch->getemployee($this->request->get['employee_id']);

			// echo "<pre>";
			// print_r($employee_info);
			// exit();
		}

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($employee_info)) {
			$this->data['name'] = $employee_info['emp_name'];
		} else {	
			$this->data['name'] = '';
		}

		if (isset($this->request->post['emp_code'])) {
			$this->data['employee_code'] = $this->request->post['emp_code'];
		} elseif (!empty($employee_info)) {
			$this->data['employee_code'] = $employee_info['device_emp_code'];
		} else {	
			$emp_codes = $this->db->query("SELECT `device_emp_code` FROM `oc_employee` ORDER BY `device_emp_code` DESC LIMIT 1");
			if($emp_codes->num_rows > 0){
				$emp_code = $emp_codes->row['device_emp_code'] + 1;
			} else {
				$emp_code = '1000';
			}
			$this->data['employee_code'] = $emp_code;
		}


		// if (isset($this->request->post['employee_code'])) {
		// 	$this->data['employee_code'] = $this->request->post['employee_code'];
		// } elseif (!empty($employee_info)) {
		// 	$this->data['employee_code'] = $employee_info['device_emp_code'];
		// } else {	
		// 	$this->data['employee_code'] = '';
		// }

		if (isset($this->request->post['card_no'])) {
			$this->data['card_no'] = $this->request->post['card_no'];
		} elseif (!empty($employee_info)) {
			$this->data['card_no'] = $employee_info['card'];
		} else {	
			$this->data['card_no'] = '';
		}
		// echo'<pre>';
		// print_r($employee_info);
		// exit;
		if (isset($this->request->post['emp_name'])) {
			$this->data['emp_name'] = $this->request->post['emp_name'];
		} elseif (!empty($employee_info)) {
			$this->data['emp_name'] = $employee_info['emp_name'];
		} else {	
			$this->data['emp_name'] = '';
		}

		if (isset($this->request->post['middle_name'])) {
			$this->data['middle_name'] = $this->request->post['middle_name'];
		} elseif (!empty($employee_info)) {
			$this->data['middle_name'] = $employee_info['emp_name'];
		} else {	
			$this->data['middle_name'] = '';
		}


		if (isset($this->request->post['last_name'])) {
			$this->data['last_name'] = $this->request->post['last_name'];
		} elseif (!empty($employee_info)) {
			$this->data['last_name'] = $employee_info['emp_name'];
		} else {	
			$this->data['last_name'] = '';
		}
		
		if (isset($this->request->post['device_serial_no'])) {
			$this->data['device_serial_no'] = $this->request->post['device_serial_no'];
		} elseif (!empty($employee_info)) {
			$this->data['device_serial_no'] = $employee_info['device_serial_no'];
		} else {	
			$this->data['device_serial_no'] = '';
		}

		
		
			
			

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
			$this->data['hidden_status'] = $this->request->post['hidden_status'];
		} elseif (!empty($employee_info)) {
			$this->data['status'] = $employee_info['status'];
			$this->data['hidden_status'] = $employee_info['status'];
		} else {	
			$this->data['status'] = 1;
			$this->data['hidden_status'] = 1;
		}

		

		// }

		

		if (!isset($this->request->get['employee_id'])) {
			$this->data['action'] = $this->url->link('catalog/employee/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			if(isset($this->request->get['return'])){
				$this->data['action'] = $this->url->link('catalog/employee/update', 'token=' . $this->session->data['token'] . '&employee_id=' . $this->request->get['employee_id'] . '&return=1', 'SSL');
			} else {
				$this->data['action'] = $this->url->link('catalog/employee/update', 'token=' . $this->session->data['token'] . '&employee_id=' . $this->request->get['employee_id'] . $url, 'SSL');
			}
		}

		if(isset($this->request->get['return'])){
			$this->data['cancel'] = $this->url->link('report/employee_data', 'token=' . $this->session->data['token'] . '&h_name=' . $this->data['name'] . '&h_name_id=' . $this->request->get['employee_id'], 'SSL');
		} else {
			$this->data['cancel'] = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}

		$this->data['weeks'] = array(
			'1' => 'Sunday',
			'2' => 'Monday',
			'3' => 'Tuesday',
			'4' => 'Wednesday',
			'5' => 'Thursday',
			'6' => 'Friday',
			'7' => 'Saturday',
		);
		
		$this->template = 'catalog/attendance_punch.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}  

	
	protected function validateForm() {
		$this->load->model('catalog/attendance_punch');

		if (!$this->user->hasPermission('modify', 'catalog/attendance_punch')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	
	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/attendance_punch');

			if (isset($this->session->data['emp_code']) && !$this->user->isAdmin()) {
				$filter_device_emp_code = $this->session->data['emp_code'];
			} else {
				$filter_emp_code = '';
			}

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_device_emp_code' => $filter_emp_code,
				'start'       => 0,
				'limit'       => 10
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'device_emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}

	
	
}
?>