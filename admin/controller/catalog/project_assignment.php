<?php
class ControllerCatalogProjectAssignment extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('catalog/project_assignment');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/project_assignment');

		$this->getList();
		
	}

	public function insert() {
		$this->language->load('catalog/project_assignment');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/project_assignment');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$this->model_catalog_project_assignment->addprojectassignment($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			//if(strtotime($this->request->post['hidden_end_date']) <= strtotime(date('Y-m-d'))){
				$employee_list_string = implode(',', $this->request->post['employee_list']);
				$url = HTTP_CATALOG.'service/dataprocess_empty.php?project_assignment=1&start_date='.$this->request->post['hidden_start_date'].'&end_date='.$this->request->post['hidden_end_date'].'&employee_list_string='.$employee_list_string;
				header('Location: ' . $url);
			//} else {
				//$this->redirect($this->url->link('catalog/project_assignment', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			//}
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/project_assignment');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/project_assignment');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_project_assignment->editprojectassignment($this->request->get['id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$url = HTTP_CATALOG.'service/dataprocess_empty.php?project_assignment=1';
			header('Location: ' . $url);
			//$this->redirect($this->url->link('catalog/project_assignment', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() { 
		$this->language->load('catalog/project_assignment');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/project_assignment');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_project_assignment->deleteprojectassignment($id);	
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/project_assignment', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		} elseif(isset($this->request->get['id']) && $this->validateDelete()){
			$id = $this->request->get['id'];
			$this->model_catalog_project_assignment->deleteprojectassignment($id);	
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/project_assignment', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'project_id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}	

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/project_assignment', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/project_assignment/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/project_assignment/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	

		$this->data['project_assignments'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$project_assignment_total = $this->model_catalog_project_assignment->getTotalprojectassignments($data);
		$results = $this->model_catalog_project_assignment->getprojectassignments($data);
		foreach ($results as $result) {
			$project_ds = $this->db->query("SELECT * FROM `oc_project` WHERE `id` = '".$result['project_id']."' ")->row;
			$pro_d = $project_ds['name'];
			//echo '<pre>';print_r($pro_d);exit();
			$action = array();
			if(strtotime($result['start_date']) > strtotime(date('Y-m-d'))){
				// $action[] = array(
				// 	'text' => $this->language->get('text_edit'),
				// 	'href' => $this->url->link('catalog/project_assignment/update', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, 'SSL')
				// );
				$action[] = array(
					'text' => 'Delete',
					'href' => $this->url->link('catalog/project_assignment/delete', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, 'SSL')
				);
			}		
			$this->data['project_assignments'][] = array(
				'id'				=>$result['id'],
				'project_id'		=>$result['project_id'],
				'project_name'     => $pro_d,		
				'start_date'   	=> date('d-m-Y', strtotime($result['start_date'])),
				'end_date'   	=> date('d-m-Y', strtotime($result['end_date'])),
				'selected'      => isset($this->request->post['selected']) && in_array($result['id'], $this->request->post['selected']),
				'action'        => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_start_date'] = $this->language->get('column_start_date');
		$this->data['column_end_date'] = $this->language->get('column_end_date');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_project_name'] = $this->url->link('catalog/project_assignment', 'token=' . $this->session->data['token'] . '&sort=project_name' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $project_assignment_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/project_assignment', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();				

		$this->data['sort'] = $sort; 
		$this->data['order'] = $order;

		$this->template = 'catalog/project_assignment_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_select_all'] = $this->language->get('text_select_all');
		$this->data['text_unselect_all'] = $this->language->get('text_unselect_all');

		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_start_date'] = $this->language->get('entry_start_date');
		$this->data['entry_end_date'] = $this->language->get('entry_end_date');
		$this->data['entry_access'] = $this->language->get('entry_access');
		$this->data['entry_modify'] = $this->language->get('entry_modify');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['project_id'])) {
			$this->data['error_project_id'] = $this->error['project_id'];
		} else {
			$this->data['error_project_id'] = '';
		}

		if (isset($this->error['project_name'])) {
			$this->data['error_project_name'] = $this->error['project_name'];
		} else {
			$this->data['error_project_name'] = '';
		}


		if (isset($this->error['start_date'])) {
			$this->data['error_start_date'] = $this->error['start_date'];
		} else {
			$this->data['error_start_date'] = '';
		}

		if (isset($this->error['end_date'])) {
			$this->data['error_end_date'] = $this->error['end_date'];
		} else {
			$this->data['error_end_date'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['token'] = $this->session->data['token'];

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/project_assignment', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);
	
		if (!isset($this->request->get['id'])) {
			$this->data['action'] = $this->url->link('catalog/project_assignment/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/project_assignment/update', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/project_assignment', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['id']) && $this->request->server['REQUEST_METHOD'] != 'POST') {
			$project_assignment_info = $this->model_catalog_project_assignment->getprojectassignment($this->request->get['id']);
		}

		if (isset($this->request->post['project_id'])) {
			$this->data['project_id'] = $this->request->post['project_id'];
		} elseif (!empty($project_assignment_info)) {
			$this->data['project_id'] = $project_assignment_info['project_id'];
		} else {
			$this->data['project_id'] = '';
		}
			
		$this->load->model('catalog/project');
		$project_datas = $this->model_catalog_project->getprojects();
		$project_data = array();
		//$department_data['0'] = 'All';
		foreach ($project_datas as $skey => $svalue) {
			$project_data[strtolower(trim($svalue['id']))] = (trim($svalue['name']));
		}
		$this->data['project_datas'] = $project_data;
		
		
		if (isset($this->request->post['start_date'])) {
			$this->data['start_date'] = $this->request->post['start_date'];
			$this->data['hidden_start_date'] = $this->request->post['hidden_start_date'];
		} elseif (!empty($project_assignment_info)) {
			$this->data['start_date'] = date('d-m-Y', strtotime($project_assignment_info['start_date']));
			$this->data['hidden_start_date'] = date('d-m-Y', strtotime($project_assignment_info['start_date']));
		} else {
			$this->data['start_date'] = '';
			$this->data['hidden_start_date'] = '';
		}

		if (isset($this->request->post['end_date'])) {
			$this->data['end_date'] = $this->request->post['end_date'];
			$this->data['hidden_end_date'] = $this->request->post['hidden_end_date'];
		} elseif (!empty($project_assignment_info)) {
			$this->data['end_date'] = date('d-m-Y', strtotime($project_assignment_info['end_date']));
			$this->data['hidden_end_date'] = date('d-m-Y', strtotime($project_assignment_info['end_date']));
		} else {
			$this->data['end_date'] = '';
			$this->data['hidden_end_date'] = '';
		}


		if (isset($this->request->post['employee_list'])) {
			$this->data['employee_list'] = $this->request->post['employee_list'];
		} elseif (!empty($project_assignment_info)) {
			$employee_list = $this->db->query("SELECT * FROM " . DB_PREFIX . "project_employee WHERE project_id = '" . $this->request->get['id'] . "'")->rows;
		foreach($employee_list as $ekey => $evalue){
			$employee_list[html_entity_decode(strtolower(trim($evalue['employee_id'])))] = html_entity_decode(strtolower(trim($evalue['employee_id'])));			
		}
			$this->data['employee_list'] = $employee_list;
		} else {	
			$this->data['employee_list'] = array();
		}

		$this->load->model('catalog/employee');
		$employee_datas = $this->model_catalog_employee->getemployees();
		$employee_data = array();
		//$department_data['0'] = 'All';
		foreach ($employee_datas as $ekey => $evalue) {
			$employee_data[strtolower(trim($evalue['emp_code']))] = (trim($evalue['name']));
		}
		$this->data['employee_data'] = $employee_data;
		$this->template = 'catalog/project_assignment_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/project_assignment')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// if ((utf8_strlen($this->request->post['project_name']) < 3) || (utf8_strlen($this->request->post['project_name']) > 64)) {
		// 	$this->error['project_name'] = $this->language->get('error_project_name');
		// }

		// $all_data = $this->db->query("SELECT * FROM `oc_project_transection` WHERE `project_id` = '".$this->request->post['project_id']."' ");
		// if($all_data->num_rows > 0){
		// 	$this->error['warning'] = "This Project is Already Assigned to Employees";
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/project_assignment')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/project_assignment');

		// foreach ($this->request->post['selected'] as $id) {
		// 	$project_assignment_total = $this->model_catalog_project_assignment->getTotalprojectassignments($id);

		// 	if ($project_assignment_total) {
		// 		$this->error['warning'] = sprintf($this->language->get('error_project_assignment'), $project_assignment_total);
		// 	}
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	public function getproject_data() {
		$json = array();
		$json['exist'] = 0;
		if (isset($this->request->get['filter_project_id'])) {
			$filter_project_id = $this->request->get['filter_project_id'];
			$all_datas = $this->db->query("SELECT * FROM `oc_project_transection` WHERE `project_id` = '".$this->request->get['filter_project_id']."' ");
			if($all_datas->num_rows > 0){
				$all_data = $all_datas->row;
				$json['start_date'] = $all_data['start_date'];
				$json['end_date'] = $all_data['end_date'];
				$json['project_transaction_id'] = $all_data['id'];
				$json['exist'] = 1; 
			}
		}
		$this->response->setOutput(json_encode($json));
	}
}
?>