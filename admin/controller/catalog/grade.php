<?php
class ControllerCatalogGrade extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('catalog/grade');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/grade');

		$this->getList();
	}

	public function insert() {
		$this->language->load('catalog/grade');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/grade');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_grade->addGrade($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/grade', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/grade');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/grade');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_grade->editGrade($this->request->get['grade_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/grade', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('catalog/grade');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/grade');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $grade_id) {
				$this->model_catalog_grade->deleteGrade($grade_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/grade', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'g_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/grade', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/grade/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/grade/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	

		$this->data['grades'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$grade_total = $this->model_catalog_grade->getTotalGrades();

		$results = $this->model_catalog_grade->getGrades($data);

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/grade/update', 'token=' . $this->session->data['token'] . '&grade_id=' . $result['grade_id'] . $url, 'SSL')
			);

			$this->data['grades'][] = array(
				'grade_id' => $result['grade_id'],
				'g_name'          => $result['g_name'],
				'selected'       => isset($this->request->post['selected']) && in_array($result['grade_id'], $this->request->post['selected']),
				'action'         => $action
			);
		}	

		$this->data['token'] = $this->session->data['token'];

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_title'] = $this->language->get('column_title');
		$this->data['column_sort_order'] = $this->language->get('column_sort_order');
		$this->data['column_action'] = $this->language->get('column_action');		

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_title'] = $this->url->link('catalog/grade', 'token=' . $this->session->data['token'] . '&sort=g_name' . $url, 'SSL');
		$this->data['sort_sort_order'] = $this->url->link('catalog/grade', 'token=' . $this->session->data['token'] . '&sort=i.sort_order' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $grade_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/grade', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'catalog/grade_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');

		$this->data['entry_title'] = $this->language->get('entry_title');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_store'] = $this->language->get('entry_store');
		$this->data['entry_keyword'] = $this->language->get('entry_keyword');
		$this->data['entry_bottom'] = $this->language->get('entry_bottom');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_layout'] = $this->language->get('entry_layout');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['tab_general'] = $this->language->get('tab_general');
		$this->data['tab_data'] = $this->language->get('tab_data');
		$this->data['tab_design'] = $this->language->get('tab_design');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['title'])) {
			$this->data['error_title'] = $this->error['title'];
		} else {
			$this->data['error_title'] = array();
		}

		if (isset($this->error['description'])) {
			$this->data['error_description'] = $this->error['description'];
		} else {
			$this->data['error_description'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),     		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/grade', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['grade_id'])) {
			$this->data['action'] = $this->url->link('catalog/grade/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/grade/update', 'token=' . $this->session->data['token'] . '&grade_id=' . $this->request->get['grade_id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/grade', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['grade_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$grade_info = $this->model_catalog_grade->getGrade($this->request->get['grade_id']);
		}

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->request->post['g_name'])) {
			$this->data['g_name'] = $this->request->post['g_name'];
		} elseif (!empty($grade_info)) {
			$this->data['g_name'] = $grade_info['g_name'];
		} else {
			$this->data['g_name'] = '';
		}

		if (isset($this->request->post['grade_params'])) {
			$this->data['grade_params'] = $this->request->post['grade_params'];
		} elseif(!empty($grade_info)){
			$grade_earnings = $this->db->query("SELECT * FROM `oc_designationparam` WHERE `status` = '1' AND `type` = 'Earning' ")->rows;
			$grade_deduction = $this->db->query("SELECT * FROM `oc_designationparam` WHERE `status` = '1' AND `type` = 'Deduction' ")->rows;
			// echo '<pre>';
			// print_r($grade_earnings);
			// echo '<pre>';
			// print_r($grade_deduction);
			
			$gradeparam_value = '';
			$i = 0;
			foreach($grade_earnings as $gkey => $gvalue){
				$grade_params[$i]['gradeparam_name'] = $gvalue['name'];
				$grade_params[$i]['gradeparam_value'] = $gradeparam_value;
				$i ++;

				if(isset($grade_deduction[$gkey])){
					$grade_params[$i]['gradeparam_name'] = $grade_deduction[$gkey]['name'];
					$grade_params[$i]['gradeparam_value'] = $gradeparam_value;
					$i++;
				}
			}
			// echo '<pre>';
			// print_r($grade_params);
			// exit;

		} else{
			$this->data['grade_params'] = array();
		}	

		if (isset($this->request->post['basic'])) {
			$this->data['basic'] = $this->request->post['basic'];
		} elseif (!empty($grade_info)) {
			$this->data['basic'] = $grade_info['basic'];
		} else {
			$this->data['basic'] = '0';
		}

		if (isset($this->request->post['da'])) {
			$this->data['da'] = $this->request->post['da'];
		} elseif (!empty($grade_info)) {
			$this->data['da'] = $grade_info['da'];
		} else {
			$this->data['da'] = '0';
		}

		if (isset($this->request->post['hra'])) {
			$this->data['hra'] = $this->request->post['hra'];
		} elseif (!empty($grade_info)) {
			$this->data['hra'] = $grade_info['hra'];
		} else {
			$this->data['hra'] = '0';
		}

		if (isset($this->request->post['ta'])) {
			$this->data['ta'] = $this->request->post['ta'];
		} elseif (!empty($grade_info)) {
			$this->data['ta'] = $grade_info['ta'];
		} else {
			$this->data['ta'] = '0';
		}

		if (isset($this->request->post['ea'])) {
			$this->data['ea'] = $this->request->post['ea'];
		} elseif (!empty($grade_info)) {
			$this->data['ea'] = $grade_info['ea'];
		} else {
			$this->data['ea'] = '0';
		}

		if (isset($this->request->post['medical_allowance'])) {
			$this->data['medical_allowance'] = $this->request->post['medical_allowance'];
		} elseif (!empty($grade_info)) {
			$this->data['medical_allowance'] = $grade_info['medical_allowance'];
		} else {
			$this->data['medical_allowance'] = '0';
		}

		if (isset($this->request->post['other_allowance'])) {
			$this->data['other_allowance'] = $this->request->post['other_allowance'];
		} elseif (!empty($grade_info)) {
			$this->data['other_allowance'] = $grade_info['other_allowance'];
		} else {
			$this->data['other_allowance'] = '0';
		}

		if (isset($this->request->post['earning_total'])) {
			$this->data['earning_total'] = $this->request->post['earning_total'];
		} elseif (!empty($grade_info)) {
			$this->data['earning_total'] = $grade_info['earning_total'];
		} else {
			$this->data['earning_total'] = '0';
		}

		if (isset($this->request->post['pf'])) {
			$this->data['pf'] = $this->request->post['pf'];
		} elseif (!empty($grade_info)) {
			$this->data['pf'] = $grade_info['pf'];
		} else {
			$this->data['pf'] = '0';
		}

		if (isset($this->request->post['esi'])) {
			$this->data['esi'] = $this->request->post['esi'];
		} elseif (!empty($grade_info)) {
			$this->data['esi'] = $grade_info['esi'];
		} else {
			$this->data['esi'] = '0';
		}

		if (isset($this->request->post['pt'])) {
			$this->data['pt'] = $this->request->post['pt'];
		} elseif (!empty($grade_info)) {
			$this->data['pt'] = $grade_info['pt'];
		} else {
			$this->data['pt'] = '0';
		}

		if (isset($this->request->post['income_tax'])) {
			$this->data['income_tax'] = $this->request->post['income_tax'];
		} elseif (!empty($grade_info)) {
			$this->data['income_tax'] = $grade_info['income_tax'];
		} else {
			$this->data['income_tax'] = '0';
		}

		if (isset($this->request->post['deduction_total'])) {
			$this->data['deduction_total'] = $this->request->post['deduction_total'];
		} elseif (!empty($grade_info)) {
			$this->data['deduction_total'] = $grade_info['deduction_total'];
		} else {
			$this->data['deduction_total'] = '0';
		}

		if (isset($this->request->post['net_total'])) {
			$this->data['net_total'] = $this->request->post['net_total'];
		} elseif (!empty($grade_info)) {
			$this->data['net_total'] = $grade_info['net_total'];
		} else {
			$this->data['net_total'] = '0';
		}

		if (isset($this->request->post['no_days'])) {
			$this->data['no_days'] = $this->request->post['no_days'];
		} elseif (!empty($grade_info)) {
			$this->data['no_days'] = $grade_info['no_days'];
		} else {
			$this->data['no_days'] = '';
		}

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($grade_info)) {
			$this->data['status'] = $grade_info['status'];
		} else {
			$this->data['status'] = 1;
		}

		$this->template = 'catalog/grade_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/grade')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['g_name']) < 1) || (utf8_strlen($this->request->post['g_name']) > 64)) {
			$this->error['g_name'] = 'Plese Enter Grade Name';
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/grade')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	public function autocomplete() {
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/grade');
			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);
			$results = $this->model_catalog_grade->getGrades($data);
			foreach ($results as $result) {
				$json[] = array(
					'grade_id' => $result['grade_id'],
					'g_name'            => strip_tags(html_entity_decode($result['g_name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['g_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}
}
?>