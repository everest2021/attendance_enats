<?php  
class ControllerCatalogdevices extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('catalog/devices');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/devices');

		$this->getList();
	}

	public function insert() {
		$this->language->load('catalog/devices');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/devices');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_devices->adddevices($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/devices', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/devices');

		$this->document->setTitle('devices');

		$this->load->model('catalog/devices');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_devices->editdevices($this->request->get['id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/devices', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('catalog/devices');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/devices');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_devices->deletedevices($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/devices', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_device_serial_no'])) {
			$filter_device_serial_no = $this->request->get['filter_device_serial_no'];
		} else {
			$filter_device_serial_no = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'device_serial_no';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_device_serial_no'])) {
			$url .= '&filter_device_serial_no=' . $this->request->get['filter_device_serial_no'];
		}


		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/devices', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/devices/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/devices/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['devices'] = array();

		$data = array(
			'filter_device_serial_no' => $filter_device_serial_no,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$devices_total = $this->model_catalog_devices->getTotaldevices();

		$results = $this->model_catalog_devices->getdevices($data);

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/devices/update', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, 'SSL')
			);

			$sql = "SELECT * FROM `oc_attendance` WHERE device_id='" .$result['device_serial_no'] ."' ORDER BY id DESC LIMIT 1";
				 
			$query = $this->db->query($sql);

			if($query->num_rows>0){
				$punch_date= $query->row['punch_date'];
				$punch_time= $query->row['punch_time'];
			 	$current_date = date('Y-m-d');
				$current_time = date('H:i:s');

				$current_date = date( $current_date . ' ' . $current_time);
				$punch_dates  =  $punch_date . ' ' . $punch_time;
				$start_date = new DateTime($current_date);
				$since_start = $start_date->diff(new DateTime($punch_dates));
				$full_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
				$minute = sprintf(sprintf("%02d", $since_start->i));
				// echo'<pre>';
				// print_r($minute);
				// exit;
				if($minute > 5){
					$status = 'Offline';

				}else{
					$status = 'Online';
				}

			}else{
			 	$punch_date = "";
			 	$punch_time = "";
			 	$status = 'Offline';
			 }
			
			
			$this->data['devices'][] = array(
				'id'    => $result['id'],
				'device_serial_no'   => $result['device_serial_no'],
				'location'   => $result['location'],
				'time_zone'   => $result['time_zone'],
				'type'   => $result['type'],
				'status' => $status,
				'selected'   => isset($this->request->post['selected']) && in_array($result['id'], $this->request->post['selected']),
				// 'status'     => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'action'     => $action
			);
		}

		$this->data['token'] = $this->session->data['token'];

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_device_serial_no'] = $this->language->get('column_device_serial_no');
		$this->data['column_location'] = $this->language->get('column_location');
		$this->data['column_type'] = $this->language->get('column_type');
		$this->data['column_time_zone'] = $this->language->get('column_time_zone');
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_device_serial_no'])) {
			$url .= '&filter_device_serial_no=' . $this->request->get['filter_device_serial_no'];
		}

		$this->data['sort_device_serial_no'] = $this->url->link('catalog/devices', 'token=' . $this->session->data['token'] . '&sort=device_serial_no' . $url, 'SSL');
		$this->data['sort_location'] = $this->url->link('catalog/devices', 'token=' . $this->session->data['token'] . '&sort=location' . $url, 'SSL');
		$this->data['sort_time_zone'] = $this->url->link('catalog/devices', 'token=' . $this->session->data['token'] . '&sort=time_zone' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('catalog/devices', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');
		$this->data['sort_type'] = $this->url->link('catalog/devices', 'token=' . $this->session->data['token'] . '&sort=type' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $devices_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/devices', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['filter_device_serial_no'] = $filter_device_serial_no;

		$this->template = 'catalog/devices_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = 'Devices';

		$this->data['Online'] = $this->language->get('Online');
		$this->data['Offline'] = $this->language->get('Offline');

		$this->data['entry_device_serial_no'] = $this->language->get('entry_device_serial_no');
		$this->data['entry_location'] = $this->language->get('entry_location');
		// $this->data['entry_confirm'] = $this->language->get('entry_confirm');
		// $this->data['entry_firstdevice_serial_no'] = $this->language->get('entry_firstdevice_serial_no');
		// $this->data['entry_lastdevice_serial_no'] = $this->language->get('entry_lastdevice_serial_no');
		// $this->data['entry_email'] = $this->language->get('entry_email');
		// $this->data['entry_user_group'] = $this->language->get('entry_user_group');
		$this->data['entry_type'] = $this->language->get('entry_type');
		$this->data['entry_time_zone'] = $this->language->get('entry_time_zone');
		$this->data['entry_captcha'] = $this->language->get('entry_captcha');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

			

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['device_serial_no'])) {
			$this->data['error_device_serial_no'] = $this->error['device_serial_no'];
		} else {
			$this->data['error_device_serial_no'] = '';
		}

		
		$url = '';




		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Devices',
			'href'      => $this->url->link('catalog/devices/update', 'token=' . $this->session->data['token'] , 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['id'])) {
			$this->data['action'] = $this->url->link('catalog/devices/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/devices/update', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('common/home', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$devices_info = $this->model_catalog_devices->getdevice($this->request->get['id']);
			// echo"<pre>";
			// print_r($devices_info);
			// exit
		}

		if (isset($this->request->post['device_serial_no'])) {

				$sql = "SELECT * FROM `oc_attendance` WHERE device_id='".$this->request->post['device_serial_no'] ."' ORDER BY id DESC LIMIT 1";

		$query = $this->db->query($sql);

		if($query->num_rows>0){
				$punch_date= $query->row['punch_date'];
				$punch_time= $query->row['punch_time'];
			 	$current_date = date('Y-m-d');
				$current_time = date('H:i:s');

				$current_date = date( $current_date . ' ' . $current_time);
				$punch_dates  =  $punch_date . ' ' . $punch_time;
				$start_date = new DateTime($current_date);
				$since_start = $start_date->diff(new DateTime($punch_dates));
				$full_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
				$minute = sprintf(sprintf("%02d", $since_start->i));
				
				if($minute > 5){
					$status = 'Offline';

				}else{
					$status = 'Online';
				}

			}else{
			 	$punch_date = "";
			 	$punch_time = "";
			 	$status = 'Offline';
			 }
		
			$this->data['device_serial_no'] = $this->request->post['device_serial_no'];
		} elseif (!empty($devices_info)) {
			$sql = "SELECT * FROM `oc_attendance` WHERE device_id='". $devices_info['device_serial_no'] ."' ORDER BY id DESC LIMIT 1";

		$query = $this->db->query($sql);

		if($query->num_rows>0){
				$punch_date= $query->row['punch_date'];
				$punch_time= $query->row['punch_time'];
			 	$current_date = date('Y-m-d');
				$current_time = date('H:i:s');

				$current_date = date( $current_date . ' ' . $current_time);
				$punch_dates  =  $punch_date . ' ' . $punch_time;
				$start_date = new DateTime($current_date);
				$since_start = $start_date->diff(new DateTime($punch_dates));
				$full_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
				$minute = sprintf(sprintf("%02d", $since_start->i));
				
				if($minute > 5){
					$status = 'Offline';

				}else{
					$status = 'Online';
				}

			}else{
			 	$punch_date = "";
			 	$punch_time = "";
			 	$status = 'Offline';
			 }
		
			$this->data['device_serial_no'] = $devices_info['device_serial_no'];
		} else {
			$punch_date = "";
			 $punch_time = "";
			 $status = 'Offline';
			$this->data['device_serial_no'] = '';
		}

		// echo'<pre>';
		// print_r($status);
		// exit;
			 $this->data['status'] =$status;
			 $this->data['punch_date'] =$punch_date;
			 $this->data['punch_time'] =$punch_time;
		if (isset($this->request->post['location'])) {
			$this->data['location'] = $this->request->post['location'];
		} elseif (!empty($devices_info)) {
			$this->data['location'] = $devices_info['location'];
		} else {
			$this->data['location'] = '';
		}

		if (isset($this->request->post['time_zone'])) {
			$this->data['time_zone'] = $this->request->post['time_zone'];
		} elseif (!empty($devices_info)) {
			$this->data['time_zone'] = $devices_info['time_zone'];
		} else {
			$this->data['time_zone'] = '330';
		}

		if (isset($this->request->post['type'])) {
			$this->data['type'] = $this->request->post['type'];
		} elseif (!empty($devices_info)) {
			$this->data['type'] = $devices_info['type'];
		} else {
			$this->data['type'] = '';
		}

		// if (isset($this->request->post['status'])) {
		// 	$this->data['status'] = $this->request->post['status'];
		// } elseif (!empty($devices_info)) {
		// 	$this->data['status'] = $devices_info['status'];
		// } else {
		// 	$this->data['status'] = '';
		// }

		$this->template = 'catalog/devices_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/devices')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		

		if ((utf8_strlen($this->request->post['device_serial_no']) < 1) || (utf8_strlen($this->request->post['device_serial_no']) > 32)) {
			$this->error['device_serial_no'] = $this->language->get('error_device_serial_no');
		}

		
		

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/devices')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['selected'] as $id) {
			if ($this->user->getId() == $id) {
				$this->error['warning'] = $this->language->get('error_account');
			}
		}

		if (!$this->error) {
			return true;
		} else { 
			return false;
		}
	}
public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_device_serial_no'])) {
			$this->load->model('catalog/devices');

			$data = array(
				'filter_device_serial_no' => $this->request->get['filter_device_serial_no'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_devices->getdevices($data);

			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'], 
					'device_serial_no'            => strip_tags(html_entity_decode($result['device_serial_no'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['device_serial_no'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>