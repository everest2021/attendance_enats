<?php    
class ControllerCatalogEmployee1 extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('catalog/employee1');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/employee');
		$this->load->model('transaction/transaction');
		$this->getList();
	}

	protected function getList() {
		date_default_timezone_set("Asia/Kolkata");
		$this->data['reporting_to_name'] = '';
		if (isset($this->session->data['emp_code'])) {
			$filter_emp_code = $this->session->data['emp_code'];
			$reporting_to_datas = $this->db->query("SELECT `reporting_to`, `reporting_to_name` FROM `oc_employee` WHERE `emp_code` = '".$filter_emp_code."' "); 
			if($reporting_to_datas->num_rows > 0){
				$reporting_to_name = $reporting_to_datas->row['reporting_to_name'];
				$this->data['reporting_to_name'] = $reporting_to_name;
			}
		} else {
			$filter_emp_code = '';
			$this->data['reporting_to_name'] = '';
		}

		// echo '<pre>';
		// print_r($this->data['reporting_to_name']);
		// exit;

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/employee1', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/employee1/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/employee1/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	
		$this->data['export_depthead'] = $this->url->link('catalog/employee1/export_depthead', 'token=' . $this->session->data['token'] . $url, 'SSL');	

		$this->data['employee1s'] = array();

		$data = array(
			'filter_name' => $filter_name,
			'filter_emp_code' => $filter_emp_code,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		$this->load->model('report/common_report');
		$this->load->model('transaction/transaction');

		$employee1_total = $this->model_catalog_employee->getTotalemployees($data);
        $results = $this->model_catalog_employee->getemployees($data);
		foreach ($results as $result) {
			$today_date = date('j');
			$month = date('n');
			$year = date('Y');
			$shift_data = $this->model_catalog_employee->getshift_id($result['emp_code'], $today_date, $month, $year);
			$shift_ids = explode('_', $shift_data);
			if(isset($shift_ids[1])){
				if($shift_ids[0] == 'S'){
					$shift_id = $shift_ids['1'];
					$shift_name = $this->model_catalog_employee->getshiftname($shift_id);
				} elseif($shift_ids[0] == 'W') {
					$shift_id = $shift_ids['1'];
					$shift_name = 'Weekly Off';
				} elseif($shift_ids[0] == 'H'){
					$shift_id = $shift_ids['1'];
					$shift_name = 'Holiday';
				} elseif($shift_ids[0] == 'HD'){
					$shift_id = $shift_ids['1'];
					$shift_name = 'Half Day';
				} elseif($shift_ids[0] == 'C'){
					$shift_id = $shift_ids['1'];
					$shift_name = 'Complimentary Off';
				}
			} else {
				$shift_id = 0;
				$shift_name = '';
			}
			$year = date('Y');
			$month = date('n');
			$day = date('j');
			if($day >= 26 && $day <= 31){
				$prev_month = $month;
				$prev_year = $year;
			} else {
				if($month == 1){
					$prev_month = 12;
					$prev_year = $year - 1;
				} else {
					$prev_month = $month - 1;
					$prev_year = $year;
				}
			}
			$comp_date = sprintf("%04d-%02d-%02d", $prev_year, $prev_month, '26');
			$current_date = date('Y-m-d');
			$comp_date_end = Date('Y-m-d', strtotime($current_date .' -1 day'));
			//$comp_date_end = sprintf("%04d-%02d-%02d", date('Y'), date('n'), date('d'));
			// $comp_date = '2017-10-26';
			// $comp_date_end = '2017-11-25';
			$filter_date_start = $comp_date;
			$filter_date_end = $comp_date_end;

			$data1['filter_date_start'] = $comp_date;
			$data1['filter_date_end'] = $comp_date_end;
			if($result['doj'] != '0000-00-00' && strtotime($result['doj']) > strtotime($data1['filter_date_start'])){
				$data1['filter_date_start'] = $result['doj'];
			}
			if($result['dol'] != '0000-00-00' && strtotime($result['dol']) < strtotime($data1['filter_date_end'])){
				$data1['filter_date_end'] = $result['dol'];
			}
			// echo '<pre>';
			// print_r($data1);
			// exit;
			$transaction_datas = $this->model_report_common_report->gettransaction_data($result['emp_code'], $data1);
			$transaction_inn = '0';
			$working_hours_array = array();
			$actual_working_hours_array = array();
			$actual_working_hours = '00:00:00';
			$late_early_count = 0;
			$total_late_count = 0;
			$total_early_count = 0;
			$total_absent_count = 0;
			$total_absent_count1 = 0;
			// echo '<pre>';
			// print_r($transaction_datas);
			// exit;
			foreach($transaction_datas as $tkey => $tvalue){
				if($tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0' && $tvalue['working_time'] != '00:00:00'){
					if($tvalue['after_shift'] == 1){
						$tvalue['act_outtime'] = $tvalue['shift_outtime_flexi'];
					}
					$shift_intime_minus_one = Date('H:i:s', strtotime($tvalue['shift_intime'] .' -0 minutes'));
					$start_date = new DateTime($tvalue['date'].' '.$shift_intime_minus_one);
					$since_start = $start_date->diff(new DateTime($tvalue['date'].' '.$tvalue['act_intime']));
					if($since_start->h > 12){
						$start_date = new DateTime($tvalue['date'].' '.$shift_intime_minus_one);
						$since_start = $start_date->diff(new DateTime($tvalue['date_out'].' '.$tvalue['act_intime']));
					}
					$shift_early = 0;
					if($since_start->invert == 1){
						$shift_early = 1;
					}
					if($shift_early == 1){
						$shift_intime_minus_one = Date('H:i:s', strtotime($tvalue['shift_intime'] .' -0 minutes'));
						if(strtotime($tvalue['act_outtime']) < strtotime($shift_intime_minus_one)){
							$start_date = new DateTime($tvalue['date'].' '.$tvalue['act_intime']);
						} else {
							$start_date = new DateTime($tvalue['date'].' '.$shift_intime_minus_one);
						}
					} else {
						$start_date = new DateTime($tvalue['date'].' '.$tvalue['act_intime']);
					}
					$since_start = $start_date->diff(new DateTime($tvalue['date'].' '.$tvalue['act_outtime']));
					$working_hour = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
					$tvalue['working_time'] = $working_hour;
					$working_hours_array[$tvalue['date']] = $tvalue['working_time'];				
				} else {
					if($tvalue['date'] == date('Y-m-d') && $tvalue['act_intime'] != '00:00:00'){
						$current_time = date('H:i:s');
						$start_date = new DateTime($tvalue['date'].' '.$tvalue['act_intime']);
						$since_start = $start_date->diff(new DateTime($tvalue['date_out'].' '.$current_time));
						$working_hours_array[$tvalue['date']] = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);	
					}
				}

				if($tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0' && $tvalue['act_intime'] <> '00:00:00' && $tvalue['act_outtime'] <> '00:00:00'){
					$start_date = new DateTime($tvalue['date'].' '.$tvalue['shift_intime']);
					$since_start = $start_date->diff(new DateTime($tvalue['date_out'].' '.$tvalue['shift_outtime']));
					$shift_working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
					$actual_working_hours_array[$tvalue['date']] = $shift_working_time;	
					if($tkey == 0){
						$actual_working_hours = $shift_working_time;
					}
				}

				if($tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0'){
					$late_early_count = $late_early_count + $tvalue['late_mark'];
					$late_early_count = $late_early_count + $tvalue['early_mark'];
					
					$total_late_count = $total_late_count + $tvalue['late_mark'];
					$total_early_count = $total_early_count + $tvalue['early_mark'];
				}
				
				if($tvalue['date'] < $current_date){
					if($tvalue['absent_status'] == 1 && $tvalue['working_time'] == '00:00:00' && $tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0'){
						$total_absent_count = $total_absent_count + 1;
					} elseif($tvalue['absent_status'] == 0.5){
						//$total_absent_count = $total_absent_count + 0.5;
					}
				}
			}
			// echo '<pre>';
			// print_r($actual_working_hours_array);
			// echo '<pre>';
			// print_r($working_hours_array);
			// exit;
			$time = 0;
			$hours = 0;
			$minutes = 0;
			foreach ($working_hours_array as $time_val) {
				$times = explode(':', $time_val);
				$hours += $times[0];
				$minutes += $times[1];
			}
			$min_min = 0;
			$min_hours = 0;
			if($minutes > 0){
				$min_hours = floor($minutes / 60);
    			$min_min = ($minutes % 60);
    			$min_min = sprintf('%02d', $min_min);
    		}
    		$total_working_hours = sprintf('%02d', ($hours + $min_hours)).'.'.sprintf('%02d', $min_min);
			$time = 0;
			$hours = 0;
			$minutes = 0;
			foreach ($actual_working_hours_array as $time_val) {
				$times = explode(':', $time_val);
				$hours += $times[0];
				$minutes += $times[1];
			}
			$min_min = 0;
			$min_hours = 0;
			if($minutes > 0){
				$min_hours = floor($minutes / 60);
    			$min_min = ($minutes % 60);
    			$min_min = sprintf('%02d', $min_min);
    		}
    		$total_actual_working_hours = sprintf('%02d', ($hours + $min_hours)).'.'.sprintf('%02d', $min_min);
			if($total_actual_working_hours > $total_working_hours){
				$extra_hours = 0;
				//$difference_hours = $total_actual_working_hours - $total_working_hours;
				$total_actual_working_hours_min = $this->hoursToMinutes($total_actual_working_hours);
				$total_working_hours_min = $this->hoursToMinutes($total_working_hours);
				$difference_minutes = $total_actual_working_hours_min - $total_working_hours_min;
				$min_hours = floor($difference_minutes / 60);
				$min_min = ($difference_minutes % 60);
				$difference_hours = $min_hours.'.'.$min_min; 
			} else {
				$difference_hours = 0;
				//$extra_hours = $total_working_hours - $total_actual_working_hours;
				$total_actual_working_hours_min = $this->hoursToMinutes($total_actual_working_hours);
				$total_working_hours_min = $this->hoursToMinutes($total_working_hours);
				$difference_minutes = $total_working_hours_min - $total_actual_working_hours_min;
				$min_hours = floor($difference_minutes / 60);
				$min_min = ($difference_minutes % 60);
				$extra_hours = $min_hours.'.'.$min_min; 
			}
			$total_bal = $this->model_transaction_transaction->gettotal_bal_ess($result['emp_code']);
			$total_previous_bal = $this->model_transaction_transaction->getprevious_balances($result['emp_code'], $filter_date_start, $filter_date_end, $result, 'PL');
			$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro_ess_date($result['emp_code'], 'PL', $filter_date_start, $filter_date_end);
			$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1_ess_date($result['emp_code'], 'PL', $filter_date_start, $filter_date_end);
			
			//$difference_hours = '112.47';
			$deduction_days_rounded = floor($difference_hours / 9.00);
			$deduction_days = round($difference_hours / 9.00, 2);
			$deduction_days = sprintf("%.2f", $deduction_days);
			$deduction_days_exp = explode('.', $deduction_days);
			if(isset($deduction_days_exp[1])){
				if($deduction_days_exp[1] == '00' || $deduction_days_exp[1] == 00){
					$deduction_days_exp[1] = 0;
				}
				if($deduction_days_exp[1] > 0 && $deduction_days_exp[1] <= 50){
					$deduction_days_rounded = $deduction_days_rounded + 0.5; 
				} else {
					if($deduction_days_exp[1] != 0){
						$deduction_days_rounded = $deduction_days_rounded + 1;
					}
				}
			}
			// echo $difference_hours;
			// echo '<br />';
			// echo $deduction_days;
			// echo '<br />';
			// echo '<pre>';
			// print_r($deduction_days_exp);
			// echo '<br />';
			// echo $deduction_days_rounded;
			// echo '<br />';
			// exit;
			//$late_early_count = 9;
			$late_early_days_rounded = floor($late_early_count / 6);
			$late_early_days = round($late_early_count / 6, 2);
			$late_early_days = sprintf("%.2f", $late_early_days);
			$late_early_days_exp = explode('.', $late_early_days);
			if(isset($late_early_days_exp[1])){
				if($late_early_days_exp[1] == '00' || $late_early_days_exp[1] == 00){
					$late_early_days_exp[1] = 0;
				}
				if($late_early_days_exp[1] >= 50){
					$late_early_days_rounded = $late_early_days_rounded + 0.5; 
				}
			}
			// echo $late_early_count;
			// echo '<br />';
			// echo $late_early_days;
			// echo '<br />';
			// echo '<pre>';
			// print_r($late_early_days_exp);
			// echo '<br />';
			// echo $late_early_days_rounded;
			// echo '<br />';
			// exit;
			$leave_minus_amount = 0;
			$total_absent_days = 0;
			if(isset($total_bal['pl_acc'])){
				if(!isset($total_bal_pro['bal_p'])){
					$total_bal_pro['bal_p'] = 0;
				}
				if(!isset($total_bal_pro['encash'])){
					$total_bal_pro['encash'] = 0;
				}
				if(!isset($total_bal_pro1['leave_amount'])){
					$total_bal_pro1['leave_amount'] = 0;
				}	
				$total_leave_opening = $total_previous_bal;
				$total_bal_pl = $total_leave_opening - ($total_bal_pro['bal_p'] + $total_bal_pro['encash'] + $total_bal_pro1['leave_amount']) - ($late_early_days_rounded + $deduction_days_rounded);
				if($total_bal_pl < 0){
					$total_absent_days += abs($total_bal_pl);
					$total_bal_pl = 0;
				}
			}
			$total_absent_days += $total_absent_count;
			//echo "SELECT `punch_time` FROM `oc_attendance_new` WHERE `emp_id` = '".$result['emp_code']."' AND `punch_date` = '".date('Y-m-d')."' ";exit;
			//$todays_intime_datas = $this->db->query("SELECT `punch_time` FROM `oc_attendance` WHERE `emp_id` = '".$result['emp_code']."' AND `punch_date` = '".date('Y-m-d')."' ");
			$todays_intime_datas = $this->db->query("SELECT `act_intime`, `act_outtime`, `act_shift_intime`, `act_shift_outtime` FROM `oc_transaction` WHERE `emp_id` = '".$result['emp_code']."' AND `date` = '".date('Y-m-d')."' AND `act_shift_id` <> '0' ");
			$todays_in_time = '00:00:00';
			$todays_working_hours = '00:00:00';
			$todays_pending_hours = '00:00:00';
			// echo '<pre>';
			// print_r($todays_intime_datas);
			// exit;
			if($todays_intime_datas->num_rows > 0){
				$todays_intime_data = $todays_intime_datas->row;
				$todays_in_time = $todays_intime_data['act_intime'];
				if($todays_intime_data['act_outtime'] != '00:00:00'){
					$todays_out_time = date('H:i:s');//$todays_intime_data['act_outtime'];
				} else {
					$todays_out_time = date('H:i:s');
				}
				$current_date = date('Y-m-d');
				$start_date = new DateTime($current_date.' '.$todays_in_time);
				$since_start = $start_date->diff(new DateTime($current_date.' '.$todays_out_time));
				$todays_working_hours = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
				
				$start_date = new DateTime($current_date.' '.$todays_intime_data['act_shift_intime']);
				$since_start = $start_date->diff(new DateTime($current_date.' '.$todays_intime_data['act_shift_outtime']));
				$shift_working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);

				$start_date = new DateTime($current_date.' '.$todays_working_hours);
				$since_start = $start_date->diff(new DateTime($current_date.' '.$shift_working_time));
				
				if($since_start->invert == 0){
					$todays_pending_hours = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
				} else {
					$todays_pending_hours = '00:00:00';
				}
				// echo $todays_working_hours;
				// echo '<br />';
				// echo $shift_working_time;
				// echo '<br />';
				// echo '<pre>';
				// print_r($since_start);
				// echo '<br />';
				// echo $todays_pending_hours;
				// echo '<br />';
				// exit;
			}
			// echo $total_actual_working_hours;
			// echo '<br />';
			// echo $total_working_hours;
			// echo '<br />';
			// echo $difference_hours;
			// echo '<br />';
			// echo $extra_hours;
			// echo '<br />';
			// echo $total_bal['pl_acc'];
			// echo '<br />';
			// echo $total_bal_pro['bal_p'];
			// echo '<br />';
			// echo $total_bal_pro['encash'];
			// echo '<br />';
			// echo $total_bal_pro1['leave_amount'];
			// echo '<br />';
			// echo $late_early_days_rounded;
			// echo '<br />';
			// echo $deduction_days_rounded;
			// echo '<br />';
			// echo $total_bal_pl;
			// echo '<br />';
			// echo $todays_in_time;
			// echo '<br />';
			// echo $todays_working_hours;
			// echo '<br />';
			// exit;
			$today_date = date('Y-m-d');
			//$leave_datas_sql = "SELECT `leave_type`, `batch_id`, `emp_id` FROM `oc_leave_transaction` WHERE `date` >= '".$today_date."' AND `dept_id` = '".$result['department_id']."' AND `unit_id` = '".$result['unit_id']."' GROUP BY `batch_id` ";
			if($this->user->isAdmin()){
				$leave_datas_sql = "SELECT `leave_type`, `batch_id`, `emp_id` FROM `oc_leave_transaction_temp` WHERE `approval_1` = '0' AND `a_status` = '1' AND `p_status` = '0' GROUP BY `batch_id` ";
			} else {
				$leave_datas_sql = "SELECT `leave_type`, `batch_id`, `emp_id` FROM `oc_leave_transaction_temp` WHERE `reporting_to` = '".$result['emp_code']."' AND `approval_1` = '0' AND `a_status` = '1' AND `p_status` = '0' GROUP BY `batch_id` ";
			}
			$leave_datas = $this->db->query($leave_datas_sql);
			$leave_data = array();
			$leave_approval_count = 0;
			foreach($leave_datas->rows as $lkey => $lvalue){
				$leave_approval_count ++;
				/*
				$emp_datas = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$lvalue['emp_id']."' ");
				$emp_name = '';
				if($emp_datas->num_rows > 0){
					$emp_data = $emp_datas->row;
					$emp_name = $emp_data['name'];
				}
				$total_leave_dayss = $this->model_transaction_transaction->gettotal_leave_days_ess($lvalue['batch_id']);
				if($total_leave_dayss['days'] == ''){
					$total_leave_days = $total_leave_dayss['leave_amount'];
				} else {
					$total_leave_days = $total_leave_dayss['days'];
				}
				$leave_from = $this->model_transaction_transaction->getleave_from_ess($lvalue['batch_id']);
				$leave_to = $this->model_transaction_transaction->getleave_to_ess($lvalue['batch_id']);

				$leave_data[] = array(
					'emp_name' => $emp_name,
					'leave_type' => $lvalue['leave_type'],
					'leave_from' => date('d-m-Y', strtotime($leave_from)),
					'leave_to' => date('d-m-Y', strtotime($leave_to)),
					'total_leave_days' => $total_leave_days,
				);
				*/
			}
			
			if($this->user->isAdmin()){
				$manual_early_datas_sql = "SELECT `emp_id` FROM `oc_manual_punch` WHERE `approval_1` = '0' AND `p_status` = '0' AND `px_record_type` = '1' AND `type` = '1'  ";
			} else {
				$manual_early_datas_sql = "SELECT `emp_id` FROM `oc_manual_punch` WHERE `reporting_to` = '".$result['emp_code']."' AND `approval_1` = '0' AND `p_status` = '0' AND `px_record_type` = '1' AND `type` = '1' ";
			}
			$manual_early_count = $this->db->query($manual_early_datas_sql)->num_rows;

			if($this->user->isAdmin()){
				$manual_late_datas_sql = "SELECT `emp_id` FROM `oc_manual_punch` WHERE `approval_1` = '0' AND `p_status` = '0' AND `px_record_type` = '2' AND `type` = '1'  ";
			} else {
				$manual_late_datas_sql = "SELECT `emp_id` FROM `oc_manual_punch` WHERE `reporting_to` = '".$result['emp_code']."' AND `approval_1` = '0' AND `p_status` = '0' AND `px_record_type` = '2' AND `type` = '1' ";
			}
			
			$manual_late_count = $this->db->query($manual_late_datas_sql)->num_rows;

			if($this->user->isAdmin()){
				$manual_other_datas_sql = "SELECT `emp_id` FROM `oc_manual_punch` WHERE `approval_1` = '0' AND `p_status` = '0' AND `px_record_type` = '3' AND `type` = '1'  ";
			} else {
				$manual_other_datas_sql = "SELECT `emp_id` FROM `oc_manual_punch` WHERE `reporting_to` = '".$result['emp_code']."' AND `approval_1` = '0' AND `p_status` = '0' AND `px_record_type` = '3' AND `type` = '1' ";
			}
			$manual_other_count = $this->db->query($manual_other_datas_sql)->num_rows;

			if($this->user->isAdmin()){
				$tour_datas_sql = "SELECT `emp_id` FROM `oc_manual_punch` WHERE `approval_1` = '0' AND `p_status` = '0' AND `type` = '2' GROUP BY `batch_id` ";
			} else {
				$tour_datas_sql = "SELECT `emp_id` FROM `oc_manual_punch` WHERE `reporting_to` = '".$result['emp_code']."' AND `approval_1` = '0' AND `p_status` = '0' AND `type` = '2' GROUP BY `batch_id` ";
			}
			$tour_count = $this->db->query($tour_datas_sql)->num_rows;

			
			$leave_datas_sql = "SELECT `leave_type`, `batch_id`, `emp_id` FROM `oc_leave_transaction_temp` WHERE `emp_id` = '".$result['emp_code']."' AND `approval_1` = '0' AND `a_status` = '1' AND `p_status` = '0' GROUP BY `batch_id` ";
			$leave_datas = $this->db->query($leave_datas_sql);
			$self_leave_approval_count = 0;
			foreach($leave_datas->rows as $lkey => $lvalue){
				$self_leave_approval_count ++;
			}

			$self_manual_early_datas_sql = "SELECT `emp_id` FROM `oc_manual_punch` WHERE `emp_id` = '".$result['emp_code']."' AND `approval_1` = '0' AND `p_status` = '0' AND `px_record_type` = '1' AND `type` = '1' ";
			$self_manual_early_count = $this->db->query($self_manual_early_datas_sql)->num_rows;

			$self_manual_late_datas_sql = "SELECT `emp_id` FROM `oc_manual_punch` WHERE `emp_id` = '".$result['emp_code']."' AND `approval_1` = '0' AND `p_status` = '0' AND `px_record_type` = '2' AND `type` = '1'  ";
			$self_manual_late_count = $this->db->query($self_manual_late_datas_sql)->num_rows;

			$self_manual_other_datas_sql = "SELECT `emp_id` FROM `oc_manual_punch` WHERE `emp_id` = '".$result['emp_code']."' AND `approval_1` = '0' AND `p_status` = '0' AND `px_record_type` = '3' AND `type` = '1'  ";
			$self_manual_other_count = $this->db->query($self_manual_other_datas_sql)->num_rows;
			
			$self_tour_datas_sql = "SELECT `emp_id` FROM `oc_manual_punch` WHERE `emp_id` = '".$result['emp_code']."' AND `approval_1` = '0' AND `p_status` = '0' AND `type` = '2' GROUP BY `batch_id` ";
			$self_tour_count = $this->db->query($self_tour_datas_sql)->num_rows;
			
			if($this->user->isAdmin()){
				$leave_datas_sql = "SELECT `leave_type`, `batch_id`, `emp_id` FROM `oc_leave_transaction` WHERE `a_status` = '1' AND `p_status` = '0' GROUP BY `batch_id` ";
			} else {
				$leave_datas_sql = "SELECT `leave_type`, `batch_id`, `emp_id` FROM `oc_leave_transaction` WHERE `dept_id` = '".$result['department_id']."' AND `unit_id` = '".$result['unit_id']."' AND `a_status` = '1' AND `p_status` = '0' GROUP BY `batch_id` ";
			}
			
			$leave_datas = $this->db->query($leave_datas_sql);
			$team_leave_data = array();
			foreach($leave_datas->rows as $lkey => $lvalue){
				$emp_datas = $this->db->query("SELECT CONCAT_WS(' ',first_name,last_name) AS emp_name FROM `oc_employee` WHERE `emp_code` = '".$lvalue['emp_id']."' ");
				$emp_name = '';
				if($emp_datas->num_rows > 0){
					$emp_data = $emp_datas->row;
					$emp_name = $emp_data['emp_name'];
				}
				$total_leave_dayss = $this->model_transaction_transaction->gettotal_leave_days($lvalue['batch_id']);
				if($total_leave_dayss['days'] == ''){
					$total_leave_days = $total_leave_dayss['leave_amount'];
				} else {
					$total_leave_days = $total_leave_dayss['days'];
				}
				$leave_from = $this->model_transaction_transaction->getleave_from($lvalue['batch_id']);
				$leave_to = $this->model_transaction_transaction->getleave_to($lvalue['batch_id']);

				$team_leave_data[] = array(
					'emp_name' => $emp_name,
					'leave_type' => $lvalue['leave_type'],
					'leave_from' => date('d-m-Y', strtotime($leave_from)),
					'leave_to' => date('d-m-Y', strtotime($leave_to)),
					'total_leave_days' => $total_leave_days,
				);
			}

			if(isset($this->request->get['date'])){
				$current_date = $this->request->get['date'];
				$current_date_exp = explode('-', $current_date);

				$month = $current_date_exp[1];
				$year = $current_date_exp[0];
				$number_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);

				$date_start = $current_date;
				$date_end = $current_date_exp[0].'-'.$current_date_exp[1].'-'.$number_days;
				
				$month_name = 'Birthday List For ' . date('M, Y', strtotime($date_start));

			} else {
				$month = date('n');
				$year = date('Y');
				$number_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
				
				$date_start = date('Y').'-'.date('n').'-01';
				$date_end = date('Y').'-'.date('n').'-'.$number_days;
				
				$month_name = 'Birthday List For ' . date('M, Y', strtotime($date_start));
			}
			
			if($month == 12){
				$next_month = 1;
				$next_year = $year + 1;
			} else {
				$next_month = $month + 1;
				$next_year = $year;
			}
			$next_date = sprintf("%04d-%02d-%02d", $next_year, $next_month, '01');
			$next_date_href = $this->url->link('catalog/employee1', 'token=' . $this->session->data['token'] . '&date=' . $next_date, 'SSL');
			if($month == 1){
				$prev_month = 12;
				$prev_year = $year - 1;
			} else {
				$prev_month = $month - 1;
				$prev_year = $year;
			}
			$prev_date = sprintf("%04d-%02d-%02d", $prev_year, $prev_month, '01');
			$prev_date_href = $this->url->link('catalog/employee1', 'token=' . $this->session->data['token'] . '&date=' . $prev_date, 'SSL');

			//$birthday_datas_sql = "SELECT `name`, `dob` FROM `oc_employee` WHERE `dob` >= '".$date_start."' AND `dob` <=  '".$date_end."' ORDER BY `dob` ";
			$date_start_month = date('m', strtotime($date_start));
			$date_start_day = date('d', strtotime($date_start));

			$date_end_month = date('m', strtotime($date_end));
			$date_end_day = date('d', strtotime($date_end));

			$birthday_datas_sql = "SELECT `name`, `dob`, `first_name`, `last_name` FROM `oc_employee` WHERE (MONTH(`dob`) = '".$date_start_month."' AND DAY(`dob`) >= '".$date_start_day."' AND DAY(`dob`) <= '".$date_end_day."') AND `status` = '1' ORDER BY DAY(`dob`) ";
			//echo $birthday_datas_sql;exit;
			$birthday_datas = $this->db->query($birthday_datas_sql);
			$birthday_data = array();
			foreach($birthday_datas->rows as $bkey => $bvalue){
				$current_month = date('n');
				$current_day = date('d');

				$birth_month = date('n', strtotime($bvalue['dob']));
				$birth_day = date('d', strtotime($bvalue['dob']));

				if($current_month == $birth_month && $current_day == $birth_day){
				//if($bvalue['dob'] == date('Y-m-d')){
					$today_birthday = 1;
				} else {
					$today_birthday = 0;
				}
				$birthday_data[] = array(
					'emp_name' => $bvalue['first_name'].' '.$bvalue['last_name'],
					'dob' => date('d-m-Y', strtotime($bvalue['dob'])),
					'today_birthday' => $today_birthday,
				);
			}			

			$this->data['employee1s'] = array(
				'employee_id' => $result['employee_id'],
				'name'        => $result['first_name'].' '.$result['last_name'],
				'code' 	      => $result['emp_code'],
				'total_actual_working_hours' 	  => $total_working_hours,
				'total_working_hours'   => $total_actual_working_hours,
				'difference_hours'   => $difference_hours,
				'extra_hours'   => $extra_hours,
				'total_bal_pl'   => $total_bal_pl,
				'total_late_count' => $total_late_count,
				'total_early_count' => $total_early_count,
				'total_absent_count' => $total_absent_count,
				
				'todays_in_time'   => $todays_in_time,
				'todays_working_hours'   => $todays_working_hours,
				'todays_pending_hours' => $todays_pending_hours,
				'shift'   => $shift_name,
				'leave_data'   => $leave_data,
				'team_leave_data'   => $team_leave_data,
				'birthday_data'   => $birthday_data,
				'next_date'   => $next_date_href,
				'prev_date'   => $prev_date_href,
				'month_name'   => $month_name,
				'leave_approval_count' => $leave_approval_count,
				'manual_early_count' => $manual_early_count,
				'manual_late_count' => $manual_late_count,
				'manual_other_count' => $manual_other_count,

				'self_leave_approval_count' => $self_leave_approval_count,
				'self_manual_early_count' => $self_manual_early_count,
				'self_manual_late_count' => $self_manual_late_count,
				'self_manual_other_count' => $self_manual_other_count,

				'tour_count' => $tour_count,
				'self_tour_count' => $self_tour_count,
			);
		}

		if($this->user->isAdmin()){
			$this->data['show'] = 1;
		} else {
			$this->data['show'] = 0;
		}

		$this->data['hr_code_book'] = 'https://docs.google.com/document/d/1YJmj9uU4q6ktkgCaSmcg6aZ-9VW0LnZeKzwz9RgR0H8/edit?ts=5a85163e';//HTTP_CATALOG.'SophiaCV_doc.docx';

		$this->data['token'] = $this->session->data['token'];	

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_delete'] = $this->language->get('text_delete');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_trainer'] = $this->language->get('column_trainer');
		$this->data['column_action'] = $this->language->get('column_action');		

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		$this->data['button_filter'] = $this->language->get('button_filter');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_trainer'])) {
			$url .= '&filter_trainer=' . $this->request->get['filter_trainer'];
		}

		if (isset($this->request->get['filter_trainer_id'])) {
			$url .= '&filter_trainer_id=' . $this->request->get['filter_trainer_id'];
		}

		$this->data['sort_name'] = $this->url->link('catalog/employee1', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_trainer'] = $this->url->link('catalog/employee1', 'token=' . $this->session->data['token'] . '&sort=trainer' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_trainer'])) {
			$url .= '&filter_trainer=' . $this->request->get['filter_trainer'];
		}

		if (isset($this->request->get['filter_trainer_id'])) {
			$url .= '&filter_trainer_id=' . $this->request->get['filter_trainer_id'];
		}

		$pagination = new Pagination();
		$pagination->total = $employee1_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/employee1', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_emp_code'] = $filter_emp_code;

		$this->template = 'catalog/employee1_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function hoursToMinutes($hours) {
		if (strstr($hours, '.')){
			# Split hours and minutes.
			$separatedData = explode('.', $hours);
			$minutesInHours    = $separatedData[0] * 60;
			$minutesInDecimals = $separatedData[1];
			$totalMinutes = $minutesInHours + $minutesInDecimals;
		} else {
			$totalMinutes = $hours * 60;
		}
		//echo $totalMinutes;exit;
		return $totalMinutes;
	}
	
	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			if (isset($this->session->data['emp_code'])) {
				$filter_emp_code = $this->session->data['emp_code'];
			} else {
				$filter_emp_code = '';
			}

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_emp_code' => $filter_emp_code,
				'start'       => 0,
				'limit'       => 10
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>