<?php    
class ControllerSnippetsYearprocess extends Controller { 
	private $error = array();

	public function index() {
		$filter_year = '2018';
		//echo $filter_year;exit;
		$months_array = array(
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5',
			'6' => '6',
			'7' => '7',
			'8' => '8',
			'9' => '9',
			'10' => '10',
			'11' => '11',
			'12' => '12',
		);
		$employee_datas = $this->db->query("SELECT `emp_code`, `unit`, `unit_id`, `doj`, `shift_id`, `name` FROM `oc_employee` WHERE `status` = '1' ")->rows;
		// echo '<pre>';
		// print_r($employee_datas);
		// exit;
		foreach($employee_datas as $ekey => $evalue){
			$shift_id = 'S_'.$evalue['shift_id'];
			foreach($months_array as $mkey => $mvalue){
				$is_exist = $this->db->query("SELECT * FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$mvalue."' AND `year` = '".$filter_year."' ");
				if($is_exist->num_rows > 0){
					$sql = "UPDATE `oc_shift_schedule` SET 
							`1` = '".$shift_id."',
							`2` = '".$shift_id."',
							`3` = '".$shift_id."',
							`4` = '".$shift_id."',
							`5` = '".$shift_id."',
							`6` = '".$shift_id."',
							`7` = '".$shift_id."',
							`8` = '".$shift_id."',
							`9` = '".$shift_id."',
							`10` = '".$shift_id."',
							`11` = '".$shift_id."',
							`12` = '".$shift_id."',
							`13` = '".$shift_id."',
							`14` = '".$shift_id."',
							`15` = '".$shift_id."',
							`16` = '".$shift_id."',
							`17` = '".$shift_id."',
							`18` = '".$shift_id."',
							`19` = '".$shift_id."',
							`20` = '".$shift_id."',
							`21` = '".$shift_id."',
							`22` = '".$shift_id."',
							`23` = '".$shift_id."',
							`24` = '".$shift_id."',
							`25` = '".$shift_id."',
							`26` = '".$shift_id."',
							`27` = '".$shift_id."',
							`28` = '".$shift_id."',
							`29` = '".$shift_id."',
							`30` = '".$shift_id."',
							`31` = '".$shift_id."',
							`month` = '".$mvalue."',
							`year` = '".$filter_year."',
							`emp_code` = '".$evalue['emp_code']."',
							`unit` = '".$this->db->escape($evalue['unit'])."', 
							`unit_id` = '".$this->db->escape($evalue['unit_id'])."' 
							WHERE `id` = '".$is_exist->row['id']."' ";
					// echo $sql;
					// echo '<br />';
					// exit;
					$this->db->query($sql);
				} else {
					$sql = "INSERT INTO `oc_shift_schedule` SET 
							`1` = '".$shift_id."',
							`2` = '".$shift_id."',
							`3` = '".$shift_id."',
							`4` = '".$shift_id."',
							`5` = '".$shift_id."',
							`6` = '".$shift_id."',
							`7` = '".$shift_id."',
							`8` = '".$shift_id."',
							`9` = '".$shift_id."',
							`10` = '".$shift_id."',
							`11` = '".$shift_id."',
							`12` = '".$shift_id."',
							`13` = '".$shift_id."',
							`14` = '".$shift_id."',
							`15` = '".$shift_id."',
							`16` = '".$shift_id."',
							`17` = '".$shift_id."',
							`18` = '".$shift_id."',
							`19` = '".$shift_id."',
							`20` = '".$shift_id."',
							`21` = '".$shift_id."',
							`22` = '".$shift_id."',
							`23` = '".$shift_id."',
							`24` = '".$shift_id."',
							`25` = '".$shift_id."',
							`26` = '".$shift_id."',
							`27` = '".$shift_id."',
							`28` = '".$shift_id."',
							`29` = '".$shift_id."',
							`30` = '".$shift_id."',
							`31` = '".$shift_id."',
							`month` = '".$mvalue."',
							`year` = '".$filter_year."',
							`emp_code` = '".$evalue['emp_code']."',
							`unit` = '".$this->db->escape($evalue['unit'])."', 
							`unit_id` = '".$this->db->escape($evalue['unit_id'])."' ";
					// echo $sql;
					// echo '<br />';
					//exit;
					$this->db->query($sql);
				}
			}
			
			$current_year = $filter_year;
			$next_year = $filter_year + 1;
			$start = new DateTime($current_year.'-01-01');
			$end   = new DateTime($next_year.'-01-01');
			$interval = DateInterval::createFromDateString('1 day');
			$period = new DatePeriod($start, $interval, $end);
			$week_array = array();
			foreach ($period as $dt) {
			    if ($dt->format('N') == 5 || $dt->format('N') == 6 || $dt->format('N') == 7) {
			    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
			    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
			    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
			    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
			    	$week_array[$dt->format('Y-m-d')]['compare_day'] = $dt->format('N');
			    }
			}
			// echo '<pre>';
			// print_r($week_array);
			// exit;
			
			$weekly_off = 'W_1_'.$evalue['shift_id'];
			$half_day = 'HD_1_'.$evalue['shift_id'];
			foreach ($week_array as $wkey => $wvalue) {
				$is_week_off = $this->db->query("SELECT `week_day` FROM `oc_shift_weekoff` WHERE `shift_id` = '".$evalue['shift_id']."' AND `week_day` = '".$wvalue['compare_day']."' AND `unit_id` = '".$evalue['unit_id']."' ");
				if($is_week_off->num_rows > 0){
					$sql = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$weekly_off."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$evalue['emp_code']."' ";	
	        		// echo $sql;
	        		// echo '<br />';
	        		$this->db->query($sql);
				}

				$is_half_day = $this->db->query("SELECT `half_day` FROM `oc_shift_weekoff` WHERE `shift_id` = '".$evalue['shift_id']."' AND `half_day` = '".$wvalue['compare_day']."' AND `unit_id` = '".$evalue['unit_id']."' ");
				if($is_half_day->num_rows > 0){
					$sql = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$half_day."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$evalue['emp_code']."' ";	
	        		// echo $sql;
	        		// echo '<br />';
	        		$this->db->query($sql);
				}
			}

			$year_start_date = $filter_year.'-01-01';
			
			if($evalue['doj'] == '0000-00-00'){
				$doj_compare = $year_start_date;
			} else {
				$doj_compare = $evalue['doj'];
			}

			$holidayloc_sql = "SELECT `holiday_id`, `location`, `location_id` FROM `oc_holiday_loc` WHERE `location_id` = '".$evalue['unit_id']."' ";
			$holidayloc_datas = $this->db->query($holidayloc_sql);
			if($holidayloc_datas->num_rows > 0){
				$holidayloc_data = $holidayloc_datas->rows;
				foreach($holidayloc_data as $hkey => $hvalue){
					$holiday_sql = "SELECT `date` FROM `oc_holiday` WHERE `holiday_id` = '".$hvalue['holiday_id']."' AND `date` >= '".$doj_compare."' AND `date` >= '".$year_start_date."' ";
					$holiday_datas = $this->db->query($holiday_sql);
					if($holiday_datas->num_rows > 0){
						$holiday_data = $holiday_datas->row;
						$date = $holiday_data['date'];
						$month = date('n', strtotime($date));
						$year = date('Y', strtotime($date));
						$day = date('j', strtotime($date));
						$holiday_string = 'H_'.$hvalue['holiday_id'].'_'.$evalue['shift_id'];
						$update_shift_schedule = "UPDATE `oc_shift_schedule` SET `".$day."` = '".$holiday_string."' WHERE `month` = '".$month."' AND `year` = '".$year."' AND `emp_code` = '".$evalue['emp_code']."' ";
						// echo $update_shift_schedule;
						// echo '<br />';
						// //exit;
						$this->db->query($update_shift_schedule);
					}
				}
				//echo 'out';exit;
			}
			$is_exist = $this->db->query("SELECT `emp_id`, `leave_id` FROM `oc_leave` WHERE `emp_id` = '".$evalue['emp_code']."' AND `year` = '".$filter_year."' ");
			if($is_exist->num_rows == 0){
				$update_sql = "UPDATE `oc_leave` SET `close_status` = '1' WHERE `emp_id` = '".$evalue['emp_code']."' ";
				$this->db->query($update_sql);

				$insert2 = "INSERT INTO `oc_leave` SET 
							`emp_id` = '".$evalue['emp_code']."', 
							`emp_name` = '".$evalue['name']."', 
							`emp_doj` = '".$evalue['doj']."', 
							`year` = '".$filter_year."',
							`close_status` = '0' "; 
				$this->db->query($insert2);
			}
			echo 'Out';exit;	
		}
		echo 'Done';exit;
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function array_sort($array, $on, $order=SORT_ASC){

	    $new_array = array();
	    $sortable_array = array();

	    if (count($array) > 0) {
	        foreach ($array as $k => $v) {
	            if (is_array($v)) {
	                foreach ($v as $k2 => $v2) {
	                    if ($k2 == $on) {
	                        $sortable_array[$k] = $v2;
	                    }
	                }
	            } else {
	                $sortable_array[$k] = $v;
	            }
	        }

	        switch ($order) {
	            case SORT_ASC:
	                asort($sortable_array);
	                break;
	            case SORT_DESC:
	                arsort($sortable_array);
	                break;
	        }

	        foreach ($sortable_array as $k => $v) {
	            $new_array[$k] = $array[$k];
	        }
	    }

	    return $new_array;
	}
}
?>