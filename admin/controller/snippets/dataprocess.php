<?php    
class ControllerSnippetsDataprocess extends Controller { 
	private $error = array();

	public function index() {

		//$current_time = date('h:i:s');
		//echo $current_time;

		$this->load->model('transaction/transaction');
		$this->load->model('report/common_report');
		$this->load->model('catalog/employee');		
		
		$batch_ids = $this->db->query("SELECT `batch_id` FROM `oc_transaction` ORDER BY `batch_id` DESC LIMIT 1");
		if($batch_ids->num_rows > 0){
			$batch_id = $batch_ids->row['batch_id'] + 1;
		} else {
			$batch_id = 1;
		}

		if(isset($_GET['date']) && isset($_GET['new'])){
			//echo 'aaaa';exit;
			$today_date = $_GET['date'];
			
			$this->db->query("DELETE FROM `oc_attendance` WHERE `punch_date` >= '".$today_date."' ");
			//$this->log->write("DELETE FROM `oc_attendance` WHERE `punch_date` >= '".$today_date."' ");
			$this->db->query("DELETE FROM `oc_transaction` WHERE `date` >= '".$today_date."' ");
			//$this->log->write("DELETE FROM `oc_transaction` WHERE `date` >= '".$today_date."' ");
			$this->db->query("UPDATE `oc_leave_transaction_temp` SET `p_status` = '0' WHERE `date` >= '".$today_date."' ");
			//$this->log->write("UPDATE `oc_leave_transaction_temp` SET `p_status` = '0' WHERE `date` >= '".$today_date."' ");
			$this->db->query("UPDATE `oc_leave_transaction` SET `p_status` = '0' WHERE `date` >= '".$today_date."' ");
			//$this->log->write("UPDATE `oc_leave_transaction` SET `p_status` = '0' WHERE `date` >= '".$today_date."' ");
			$today_date = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'));
		} else {
			//echo 'bbbbb';exit;
			$today_date = date('Y-m-d');
		}
			
		$device_last_dates = $this->db->query("SELECT `date` FROM `oc_transaction` ORDER BY `date` DESC LIMIT 1");
		if($device_last_dates->num_rows > 0){
			$device_last_date = $device_last_dates->row['date'];
		} else {
			$device_last_date = '2016-10-01';
		}
		$next_date = date('Y-m-d', strtotime($device_last_date . ' +1 day'));

		$day = array();
		$days = $this->GetDays($next_date, $today_date);
        foreach ($days as $dkey => $dvalue) {
        	$dates = explode('-', $dvalue);
        	$day[$dkey]['day'] = $dates[2];
        	$day[$dkey]['date'] = $dvalue;
        }
        //$day = array();
        // echo '<pre>';
        // print_r($day);
        // exit;
        foreach($day as $dkeys => $dvalues){
			$filter_date_start = $dvalues['date'];
			//echo $filter_date_start;exit;
			$exp_datas = array();
			$exp_datas_2 = array();
			$a = glob(DIR_DOWNLOAD."*.Txt");
			$b = glob(DIR_DOWNLOAD."*.txt");
			$c = glob(DIR_DOWNLOAD."*.TXT");
			$all_files = array_merge($a, $b, $c);
			$next_date = date('Y-m-d', strtotime($filter_date_start .' +1 day'));

			$in1 = 0;
			foreach($all_files as $file_name) {
				$exp_datas_temp = array();
				$texp = explode('/', $file_name);
				$comp_date = date('d-m-Y', strtotime($filter_date_start));
				$comp_text = $comp_date.'.txt';
				$dir_file_name = strtolower(end($texp));
				if($comp_text == $dir_file_name){
					$in1 = 1;
					$data = file_get_contents($file_name, FILE_USE_INCLUDE_PATH, null);
					$exp_datas_temp = explode(PHP_EOL, $data);
					$exp_datas = array_merge($exp_datas, $exp_datas_temp);
				}
			}

			$this->data['employees'] = array();
			$employees = array();
			$this->load->model('catalog/employee');		
			if(isset($exp_datas[0]) && $exp_datas[0] == ''){
				unset($exp_datas[0]);
				$rev_exp_datas = $exp_datas;
			} else {
				$rev_exp_datas = $exp_datas;			
			}
			

			$employees = array();
			$employees_final = array();

			foreach($rev_exp_datas as $data) {
				$tdata = trim($data);
				$emp_id  = substr($tdata, 0, 8);
				$in_date_raw = substr($tdata, 9, 11);
				$in_date = date('Y-m-d', strtotime($in_date_raw));
				$in_time_hour = substr($tdata, 21, 2);
				$in_time_min = substr($tdata, 24, 2);
				$in_time = $in_time_hour.':'.$in_time_min;

				if($emp_id != '') {
					$result = $this->model_transaction_transaction->getEmployees_dat($emp_id);
					if(isset($result['emp_code'])){
						$employees[] = array(
							'employee_id' => $result['emp_code'],
							'card_id' => $result['card_number'],
							'in_time' => $in_time,
							'punch_date' => $in_date,
							'fdate' => date('Y-m-d H:i:s', strtotime($in_date.' '.$in_time))
						);
					}
				}
			}

			// echo '<pre>';
			// print_r($employees);
			// exit;

			usort($employees, array($this, "sortByOrder"));
			
			$o_emp = array();
			foreach ($employees as $ekey => $evalue) {
				$employees_final[] = $evalue;
			}
			
			if (isset($this->request->get['unit'])) {
				$filter_unit = $this->request->get['unit'];
			} else {
				$filter_unit = 0;
			}

			if(isset($employees_final) && count($employees_final) > 0){
				//$this->model_transaction_transaction->deleteorderhistory($filter_date_start);
				//$this->model_transaction_transaction->deleteorderhistory($next_date);
				foreach ($employees_final as $fkey => $employee) {
					$exist = $this->model_transaction_transaction->getorderhistory($employee);
					if($exist == 0){
						$mystring = $employee['in_time'];
						$findme   = ':';
						$pos = strpos($mystring, $findme);
						if($pos !== false){
							$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 3, 2);
						} else {
							$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 2, 2);
						}
						if($in_times != ':'){
							$in_time = date("h:i", strtotime($in_times));
						} else {
							$in_time = '';
						}
						$employee['in_time'] = $in_times;
						// echo '<pre>';
						// print_r($employee);
						// exit;
						$this->model_transaction_transaction->insert_attendance_data($employee);
					}
				}
			}
			$response = array();
			//$is_exist = $this->model_report_common_report->getattendance_exist($filter_date_start);
			//if($is_exist == 1){
				$data = array();
				$data['unit'] = $filter_unit;
				$results = $this->model_report_common_report->getemployees($data);
				foreach ($results as $rkey => $rvalue) {
					$rvaluess = $this->model_transaction_transaction->getrawattendance_group_date_custom($rvalue['emp_code'], $filter_date_start);
					// echo 'Emp Id : ' . $rvalue['emp_code'];
					// echo '<br />';

					//echo "<pre>"; print_r($rvaluess); exit;
					if($rvaluess) {
						//$raw_attendance_datass = $this->model_transaction_transaction->getrawattendance_group();
						//if($raw_attendance_datass) {
						//foreach ($raw_attendance_datass as $rkeyss => $rvaluess) {
						$emp_data = $this->model_transaction_transaction->getempdata($rvaluess['emp_id']);
						$device_id = $rvaluess['device_id'];
						if(isset($emp_data['name']) && $emp_data['name'] != ''){
							
							$emp_name = $emp_data['name'];
							$department = $emp_data['department'];
							$unit = $emp_data['unit'];
							$group = $emp_data['group'];

							$day_date = date('j', strtotime($filter_date_start));
							$day = date('j', strtotime($filter_date_start));
							$month = date('n', strtotime($filter_date_start));
							$year = date('Y', strtotime($filter_date_start));

							$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvaluess['emp_id']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
							//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvaluess['emp_id']."' ";
							$shift_schedule = $this->db->query($update3)->row;
							$schedule_raw = explode('_', $shift_schedule[$day_date]);
							if(!isset($schedule_raw[2])){
								$schedule_raw[2] = 1;
							}
							if($schedule_raw[0] == 'S'){
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[1]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								if(isset($shift_data['shift_id'])){
									$abnormal_status = 0;
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];

									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$act_in_punch_date = $filter_date_start;
									$act_out_punch_date = $filter_date_start;
									
									$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows == 0){ 
										$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
										if(isset($act_intimes['punch_time'])) {
											$act_intime = $act_intimes['punch_time'];
											$act_in_punch_date = $act_intimes['punch_date'];
										}
										$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
										if(isset($act_outtimes['punch_time'])) {
											$act_outtime = $act_outtimes['punch_time'];
											$act_out_punch_date = $act_outtimes['punch_date'];
										}
									} else {
										if($trans_exist->row['act_intime'] == '00:00:00'){
											$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
											$new_data_exist = $this->db->query($new_data_sql);
											if($new_data_exist->num_rows > 0){	
												$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
												if(isset($act_intimes['punch_time'])) {
													$act_intime = $act_intimes['punch_time'];
													$act_in_punch_date = $act_intimes['punch_date'];
												}
											}	
										} else {
											$act_intime = $trans_exist->row['act_intime'];
											$act_in_punch_date = $trans_exist->row['date'];	
										}
									
										$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
										$new_data_exist = $this->db->query($new_data_sql);
										if($new_data_exist->num_rows > 0){
											$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
											if(isset($act_outtimes['punch_time'])) {
												$act_outtime = $act_outtimes['punch_time'];
												$act_out_punch_date = $act_outtimes['punch_date'];
											}
										} else {
											$act_outtime = $trans_exist->row['act_outtime'];
											$act_out_punch_date = $trans_exist->row['date_out'];
										}
									}

									if($act_intime == $act_outtime){
										$act_outtime = '00:00:00';
									}
									
									$first_half = 0;
									$second_half = 0;
									$late_time = '00:00:00';
									
									if($act_intime != '00:00:00'){
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
										$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
										if($since_start->h > 12){
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
										}
										$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
										if($since_start->invert == 1){
											$late_time = '00:00:00';
										}
										if($act_outtime != '00:00:00'){
											$first_half = 1;
											$second_half = 1;
											
										} else {
											$first_half = 0;
											$second_half = 0;
										}
									} else {
										$first_half = 0;
										$second_half = 0;
									}

									$working_time = '00:00:00';
									$early_time = '00:00:00';
									if($abnormal_status == 0){ //if abnormal status is zero calculate further 
										$early_time = '00:00:00';
										if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
											$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
												$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
											}
											$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
											if($since_start->invert == 0){
												$early_time = '00:00:00';
											}
										}					
										
										$working_time = '00:00:00';
										if($act_outtime != '00:00:00'){//for getting working time
											$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
										}
									} else {
										$first_half = 0;
										$second_half = 0;
									}
									
									$abs_stat = 0;
									if($first_half == 1 && $second_half == 1){
										$present_status = 1;
										$absent_status = 0;
									} elseif($first_half == 1 && $second_half == 0){
										$present_status = 0.5;
										$absent_status = 0.5;
									} elseif($first_half == 0 && $second_half == 1){
										$present_status = 0.5;
										$absent_status = 0.5;
									} else {
										$present_status = 0;
										$absent_status = 1;
									}

									$day = date('j', strtotime($rvaluess['punch_date']));
									$month = date('n', strtotime($rvaluess['punch_date']));
									$year = date('Y', strtotime($rvaluess['punch_date']));
									//echo 'out';exit;
									if($trans_exist->num_rows == 0){
										if($abs_stat == 1){
											$act_intime = '00:00:00';
											$act_outtime = '00:00:00';
											$abnormal_status = 0;
											$act_out_punch_date = $filter_date_start;
											$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."' ";
										} else {
											$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."' ";
										}
										$this->db->query($sql);
										$transaction_id = $this->db->getLastId();
									} else {
										if($abs_stat == 1){
											$act_intime = '00:00:00';
											$act_outtime = '00:00:00';
											$abnormal_status = 0;
											$act_out_punch_date = $filter_date_start;
											$sql = "UPDATE `oc_transaction` SET `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `holiday_id` = '0', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
										} else {
											$sql = "UPDATE `oc_transaction` SET `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `holiday_id` = '0', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
										}
										$this->db->query($sql);
										$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
									}
									// echo $sql.';';
									// echo '<br />';
									// exit;
									
								}
								//echo 'out1';exit;
							} elseif ($schedule_raw[0] == 'W') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								$act_in_punch_date = $filter_date_start;
								$act_out_punch_date = $filter_date_start;

								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){ 
									$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
									if(isset($act_intimes['punch_time'])) {
										$act_intime = $act_intimes['punch_time'];
										$act_in_punch_date = $act_intimes['punch_date'];
									}
									$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
								} else {
									if($trans_exist->row['act_intime'] == '00:00:00'){
										$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
										$new_data_exist = $this->db->query($new_data_sql);
										if($new_data_exist->num_rows > 0){	
											$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
											if(isset($act_intimes['punch_time'])) {
												$act_intime = $act_intimes['punch_time'];
												$act_in_punch_date = $act_intimes['punch_date'];
											}
										}	
									} else {
										$act_intime = $trans_exist->row['act_intime'];
										$act_in_punch_date = $trans_exist->row['date'];	
									}
									
									$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
									$new_data_exist = $this->db->query($new_data_sql);
									if($new_data_exist->num_rows > 0){
										$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
										if(isset($act_outtimes['punch_time'])) {
											$act_outtime = $act_outtimes['punch_time'];
											$act_out_punch_date = $act_outtimes['punch_date'];
										}
									} else {
										$act_outtime = $trans_exist->row['act_outtime'];
										$act_out_punch_date = $trans_exist->row['date_out'];
									}
								}
								$abnormal_status = 0;

								$working_time = '00:00:00';
								if($act_outtime != '00:00:00'){//for getting working time
									$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
									$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
									$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
								}

								$abs_stat = 0;
								if($filter_date_start != $act_in_punch_date){
									$abs_stat = 1;
									$act_in_punch_date = $filter_date_start;
								}

								// if($act_intime == '00:00:00' || $act_outtime == '00:00:00'){
								// 	$abnormal_status = 1;
								// }
								
								if($act_intime == $act_outtime){
									$act_outtime = '00:00:00';
								}

								if($trans_exist->num_rows == 0){
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `working_time` = '".$working_time."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '".$abnormal_status."', halfday_status = '0', compli_status = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."' ";
									} else {
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `working_time` = '".$working_time."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '".$abnormal_status."', halfday_status = '0', compli_status = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->getLastId();
								} else {
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', abnormal_status = '".$abnormal_status."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', compli_status = '0', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', halfday_status = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									} else {
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', abnormal_status = '".$abnormal_status."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', compli_status = '0', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', halfday_status = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
								}
								//echo $sql.';';
								//echo '<br />';
								
							} elseif ($schedule_raw[0] == 'H') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								$act_in_punch_date = $filter_date_start;
								$act_out_punch_date = $filter_date_start;
								
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){ 
									$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
									if(isset($act_intimes['punch_time'])) {
										$act_intime = $act_intimes['punch_time'];
										$act_in_punch_date = $act_intimes['punch_date'];
									}
									$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
								} else {
									if($trans_exist->row['act_intime'] == '00:00:00'){
										$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
										$new_data_exist = $this->db->query($new_data_sql);
										if($new_data_exist->num_rows > 0){	
											$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
											if(isset($act_intimes['punch_time'])) {
												$act_intime = $act_intimes['punch_time'];
												$act_in_punch_date = $act_intimes['punch_date'];
											}
										}	
									} else {
										$act_intime = $trans_exist->row['act_intime'];
										$act_in_punch_date = $trans_exist->row['date'];	
									}


									$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
									$new_data_exist = $this->db->query($new_data_sql);
									if($new_data_exist->num_rows > 0){
										$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
										if(isset($act_outtimes['punch_time'])) {
											$act_outtime = $act_outtimes['punch_time'];
											$act_out_punch_date = $act_outtimes['punch_date'];
										}
									} else {
										$act_outtime = $trans_exist->row['act_outtime'];
										$act_out_punch_date = $trans_exist->row['date_out'];
									}
								}

								$abnormal_status = 0;

								$working_time = '00:00:00';
								if($act_outtime != '00:00:00'){//for getting working time
									$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
									$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
									$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
								}
								
								$abs_stat = 0;
								if($filter_date_start != $act_in_punch_date){
									$abs_stat = 1;
									$act_in_punch_date = $filter_date_start;
								}

								// if($act_intime == '00:00:00' || $act_outtime == '00:00:00'){
								// 	$abnormal_status = 1;
								// }
								
								if($act_intime == $act_outtime){
									$act_outtime = '00:00:00';
								}

								if($trans_exist->num_rows == 0){
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `working_time` = '".$working_time."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', `abnormal_status` = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."' ";
									} else {
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `working_time` = '".$working_time."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', `abnormal_status` = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->getLastId();
								} else {
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `present_status` = '0', `absent_status` = '0', `halfday_status` = '0', `compli_status` = '0', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `holiday_id` = '".$schedule_raw[1]."', `device_id` = '".$device_id."', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									} else {
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `present_status` = '0', `absent_status` = '0', `halfday_status` = '0', `compli_status` = '0', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `holiday_id` = '".$schedule_raw[1]."', `device_id` = '".$device_id."', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
								}
								//echo $sql.';';
								//echo '<br />';
							} elseif ($schedule_raw[0] == 'HD') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
								//$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								$act_in_punch_date = $filter_date_start;
								$act_out_punch_date = $filter_date_start;
								
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){ 
									$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
									if(isset($act_intimes['punch_time'])) {
										$act_intime = $act_intimes['punch_time'];
										$act_in_punch_date = $act_intimes['punch_date'];
									}
									$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
								} else {
									if($trans_exist->row['act_intime'] == '00:00:00'){
										$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
										$new_data_exist = $this->db->query($new_data_sql);
										if($new_data_exist->num_rows > 0){	
											$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
											if(isset($act_intimes['punch_time'])) {
												$act_intime = $act_intimes['punch_time'];
												$act_in_punch_date = $act_intimes['punch_date'];
											}
										}	
									} else {
										$act_intime = $trans_exist->row['act_intime'];
										$act_in_punch_date = $trans_exist->row['date'];	
									}
									
									
									$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
									$new_data_exist = $this->db->query($new_data_sql);
									if($new_data_exist->num_rows > 0){
										$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
										if(isset($act_outtimes['punch_time'])) {
											$act_outtime = $act_outtimes['punch_time'];
											$act_out_punch_date = $act_outtimes['punch_date'];
										}
									} else {
										$act_outtime = $trans_exist->row['act_outtime'];
										$act_out_punch_date = $trans_exist->row['date_out'];
									}
								}

								$abnormal_status = 0;
								$abs_stat = 0;
								if($act_intime == $act_outtime){
									$act_outtime = '00:00:00';
								}

								
								$first_half = 0;
								$second_half = 0;
								$late_time = '00:00:00';
								
								// $act_intime = '12:05:00';
								// $act_outtime = '17:55:00';

								if($act_intime != '00:00:00'){
									$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
									$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
									if($since_start->h > 12){
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
										$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
									}
									$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
									if($since_start->invert == 1){
										$late_time = '00:00:00';
									}
									
									if($act_outtime != '00:00:00'){
										$first_half = 1;
										$second_half = 'HD';
									} else {
										$first_half = 0;
										$second_half = 0;
									}
								} else {
									$first_half = 0;
									$second_half = 0;
								}

								$working_time = '00:00:00';
								$early_time = '00:00:00';
								if($abnormal_status == 0){ //if abnormal status is zero calculate further 
									$early_time = '00:00:00';
									if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
										$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
										$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
										if($since_start->h > 12){
											$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
											$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
										}
										$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
										if($since_start->invert == 0){
											$early_time = '00:00:00';
										}
									}					
									
									$working_time = '00:00:00';
									if($act_outtime != '00:00:00'){//for getting working time
										$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
										$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
										$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
									}
								} else {
									$first_half = 0;
									$second_half = 0;
								}
								

								$abs_stat = 0;
								if($first_half == 1 && $second_half == 'HD'){
									$present_status = 1;
									$absent_status = 0;
								} else {
									$present_status = 0;
									$absent_status = 1;
								}

								if($trans_exist->num_rows == 0){
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `halfday_status` = '".$schedule_raw[1]."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `device_id` = '".$device_id."', batch_id = '".$batch_id."' ";
									} else {
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `halfday_status` = '".$schedule_raw[1]."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `device_id` = '".$device_id."', batch_id = '".$batch_id."' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->getLastId();
								} else {
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `halfday_status` = '".$schedule_raw[1]."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `device_id` = '".$device_id."', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									} else {
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `halfday_status` = '".$schedule_raw[1]."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `device_id` = '".$device_id."', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
								}
								//echo $sql.';';
								//echo '<br />';
							}  elseif ($schedule_raw[0] == 'C') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								$act_in_punch_date = $filter_date_start;
								$act_out_punch_date = $filter_date_start;
								
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){ 
									$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
									if(isset($act_intimes['punch_time'])) {
										$act_intime = $act_intimes['punch_time'];
										$act_in_punch_date = $act_intimes['punch_date'];
									}
									$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
								} else {
									if($trans_exist->row['act_intime'] == '00:00:00'){
										$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
										$new_data_exist = $this->db->query($new_data_sql);
										if($new_data_exist->num_rows > 0){	
											$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
											if(isset($act_intimes['punch_time'])) {
												$act_intime = $act_intimes['punch_time'];
												$act_in_punch_date = $act_intimes['punch_date'];
											}
										}	
									} else {
										$act_intime = $trans_exist->row['act_intime'];
										$act_in_punch_date = $trans_exist->row['date'];	
									}
									
									$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
									$new_data_exist = $this->db->query($new_data_sql);
									if($new_data_exist->num_rows > 0){
										$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
										if(isset($act_outtimes['punch_time'])) {
											$act_outtime = $act_outtimes['punch_time'];
											$act_out_punch_date = $act_outtimes['punch_date'];
										}
									} else {
										$act_outtime = $trans_exist->row['act_outtime'];
										$act_out_punch_date = $trans_exist->row['date_out'];
									}
								}

								$abnormal_status = 0;

								$working_time = '00:00:00';
								if($act_outtime != '00:00:00'){//for getting working time
									$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
									$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
									$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
								}
								
								$abs_stat = 0;
								if($filter_date_start != $act_in_punch_date){
									$abs_stat = 1;
									$act_in_punch_date = $filter_date_start;
								}

								// if($act_intime == '00:00:00' || $act_outtime == '00:00:00'){
								// 	$abnormal_status = 1;
								// }
								
								if($act_intime == $act_outtime){
									$act_outtime = '00:00:00';
								}

								if($trans_exist->num_rows == 0){
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `working_time` = '".$working_time."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'COF', `secondhalf_status` = 'COF', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '".$schedule_raw[1]."', `device_id` = '".$device_id."', batch_id = '".$batch_id."' ";
									} else {
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `working_time` = '".$working_time."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'COF', `secondhalf_status` = 'COF', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '".$schedule_raw[1]."', `device_id` = '".$device_id."', batch_id = '".$batch_id."' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->getLastId();
								} else {
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', `firsthalf_status` = 'COF', `secondhalf_status` = 'COF', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '".$schedule_raw[1]."', `device_id` = '".$device_id."', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									} else {
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', `firsthalf_status` = 'COF', `secondhalf_status` = 'COF', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '".$schedule_raw[1]."', `device_id` = '".$device_id."', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";	
									}
									$this->db->query($sql);
									$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
								}
								//echo $sql.';';
								//echo '<br />';
							}
							if($act_in_punch_date == $act_out_punch_date) {
								if($act_intime != '00:00:00' && $act_outtime != '00:00:00'){
									$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` >= '".$act_intime."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
									//echo $update;exit;
									$this->db->query($update);
									$this->log->write($update);
								} elseif($act_intime != '00:00:00' && $act_outtime == '00:00:00') {
									$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` = '".$act_intime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
									//echo $update;exit;
									$this->db->query($update);
									$this->log->write($update);
								} elseif($act_intime == '00:00:00' && $act_outtime != '00:00:00') {
									$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_out_punch_date."' AND `punch_time` = '".$act_outtime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
									//echo $update;exit;
									$this->db->query($update);
									$this->log->write($update);
								}
							} else {
								$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` >= '".$act_intime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
								$this->db->query($update);
								$this->log->write($update);

								$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_out_punch_date."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
								$this->db->query($update);
								$this->log->write($update);
							}
						}
					} else {
						$emp_data = $this->model_transaction_transaction->getempdata($rvalue['emp_code']);
						if(isset($emp_data['name']) && $emp_data['name'] != ''){
							$emp_name = $emp_data['name'];
							$department = $emp_data['department'];
							$unit = $emp_data['unit'];
							$group = $emp_data['group'];

							$day_date = date('j', strtotime($filter_date_start));
							$month = date('n', strtotime($filter_date_start));
							$year = date('Y', strtotime($filter_date_start));

							$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
							//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvalue['emp_code']."' ";
							$shift_schedule = $this->db->query($update3)->row;
							$schedule_raw = explode('_', $shift_schedule[$day_date]);
							if(!isset($schedule_raw[2])){
								$schedule_raw[2]= 1;
							}
							if($schedule_raw[0] == 'S'){
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[1]);
								if(isset($shift_data['shift_id'])){
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];
									
									$day = date('j', strtotime($filter_date_start));
									$month = date('n', strtotime($filter_date_start));
									$year = date('Y', strtotime($filter_date_start));

									$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows == 0){
										$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."' ";
										//echo $sql.';';
										//echo '<br />';
										$this->db->query($sql);
									} else {
										$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."'";
										//echo $sql.';';
										//echo '<br />';
										//$this->db->query($sql);
									}
								} else {
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];
									
									$day = date('j', strtotime($filter_date_start));
									$month = date('n', strtotime($filter_date_start));
									$year = date('Y', strtotime($filter_date_start));

									$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows == 0){
										$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."' ";
										//echo $sql.';';
										//echo '<br />';
										$this->db->query($sql);
									} else {
										$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
										//echo $sql.';';
										//echo '<br />';
										//$this->db->query($sql);
									}
								}
							} elseif ($schedule_raw[0] == 'W') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								
								if($trans_exist->num_rows == 0){
									$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."' ";
									//echo $sql.';';
									//echo '<br />';
									$this->db->query($sql);
								} else {
									$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									//echo $sql.';';
									//echo '<br />';
									//$this->db->query($sql);
								}
							} elseif ($schedule_raw[0] == 'H') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){
									$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."'  ";
									//echo $sql.';';
									//echo '<br />';
									$this->db->query($sql);
								} else {
									$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									//echo $sql.';';
									//echo '<br />';
									//$this->db->query($sql);
								}
							} elseif ($schedule_raw[0] == 'HD') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
								//$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){
									$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '".$schedule_raw[1]."', batch_id = '".$batch_id."' ";
									//echo $sql.';';
									//echo '<br />';
									$this->db->query($sql);
								} else {
									$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '".$schedule_raw[1]."', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									//echo $sql.';';
									//echo '<br />';
									//$this->db->query($sql);
								}
							}  elseif ($schedule_raw[0] == 'C') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){
									$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'COF', `secondhalf_status` = 'COF', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', compli_status = '".$schedule_raw[1]."', batch_id = '".$batch_id."' ";
									//echo $sql.';';
									//echo '<br />';
									$this->db->query($sql);
								} else {
									$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'COF', `secondhalf_status` = 'COF', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', compli_status = '".$schedule_raw[1]."', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									//echo $sql.';';
									//echo '<br />';
									//$this->db->query($sql);
								}
							}
							// $update = "UPDATE `oc_attendance` SET `status` = '1' WHERE `punch_date` = '".$filter_date_start."' AND `punch_time` >= '".$act_intime."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
							// $this->db->query($update);
							// $this->log->write($update);
						}	
					}
				}

				$this->load->model('transaction/dayprocess');

				//$filter_date = $this->model_transaction_dayprocess->getNextDate($filter_unit);
				//$this->data['filter_date'] = $filter_date;

				$absent = $this->model_transaction_dayprocess->getAbsent($filter_date_start,$filter_unit);
				// echo '<pre>';
				// print_r($absent);
				// exit;
				foreach($absent as $data) {
					$check_leave = $this->model_transaction_dayprocess->checkLeave($data['date'],$data['emp_id']);
					//$this->model_transaction_dayprocess->checkLeave_1($data['date'],$data['emp_id']);
				}
				
				$this->model_transaction_dayprocess->update_close_day($filter_date_start, $filter_unit);

				//$this->db->query("commit;");
				//$response['success'] = 'Transaction Generated';
			//} else {
				//$this->db->query("commit;");
				//$response['warning'] = 'No Data available';
			//}
		}

		$this->load->model('transaction/leaveprocess');
		$this->load->model('transaction/dayprocess');

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = '';
		}

		$unprocessed = $this->model_transaction_leaveprocess->getUnprocessedLeaveTillDate_2($unit);
		
		foreach($unprocessed as $data) {
			$is_closed = $this->model_transaction_dayprocess->is_closed_stat($data['date'], $data['emp_id']); 
			if($is_closed == 1){
				$this->model_transaction_dayprocess->checkLeave($data['date'], $data['emp_id']);
			}
		}
		//echo 'Done';exit;

		// $transaction_datas = $this->db->query("SELECT * FROM `oc_transaction` WHERE `batch_id` = '".$batch_id."' ")->rows;
		// $custom_data = array();
		// foreach($transaction_datas as $tkeys => $tvalues){
		// 	$emp_datas = $this->db->query("SELECT `emp_code`, `name`, `department`, `unit`, `division`, `region`, `doj` FROM `oc_employee` WHERE `emp_code` = '".$tvalues['emp_id']."' ");
		// 	if($emp_datas->num_rows > 0){
		// 		$emp_data = $emp_datas->row;
		// 		$emp_code = $emp_data['emp_code'];
		// 		$emp_name = $emp_data['name'];
		// 		$department = $emp_data['department'];
		// 		$unit = $emp_data['unit'];
		// 		$division = $emp_data['division'];
		// 		$region = $emp_data['region'];
		// 		$doj = $emp_data['doj'];
		// 	}

		// 	$date_to_compare = $filter_date_start;
		// 	if(strtotime($doj) > strtotime($date_to_compare)){
		// 		$status = 'x';
		// 	} elseif($tvalues['present_status'] == '1'){
		// 		$status = 'p';
		// 	} elseif($tvalues['weekly_off'] != '0'){
		// 		$working_times = explode(':', $tvalues['working_time']);
		// 		$working_hours = $working_times[0];
		// 		if($working_hours >= '06'){
		// 			$status = 'PW';
		// 		} else {
		// 			$status = 'W';	
		// 		}
		// 	} elseif($tvalues['holiday_id'] != '0'){
		// 		$working_times = explode(':', $tvalues['working_time']);
		// 		$working_hours = $working_times[0];
		// 		if($working_hours >= '06'){
		// 			$status = 'PPH';
		// 		} else {
		// 			$status = 'PH';
		// 		}
		// 	} elseif($tvalues['act_intime'] != '00:00:00' && $tvalues['act_outtime'] == '00:00:00'){
		// 		$status = 'E';
		// 	} elseif($tvalues['leave_status'] != '0'){
		// 		if($tvalues['leave_status'] == '0.5'){
		// 			$status = 'PL';
		// 		} else {
		// 			$status = 'PL';
		// 		}
		// 	} elseif($tvalues['absent_status'] == '1'){
		// 		$status = 'A';
		// 	}

		// 	$custom_data[] = array(
		// 		'emp_id' => $emp_code,
		// 		'emp_name' => $emp_name,
		// 		'punch_id' => $emp_code,
		// 		'department' => $department,
		// 		'site' => $unit,
		// 		'region' => $region,
		// 		'division' => $division,
		// 		'attend_date' => $filter_date_start,
		// 		'status' => $status,
		// 		'in_time' => $tvalues['act_intime'],
		// 		'out_time' => $tvalues['act_outtime'],
		// 		'total_hour' => $tvalues['working_time'],
		// 	);
		// }
		// echo '<pre>';
		// print_r($custom_data);
		// exit;
		
		//$this->session->data['success'] = 'Employess Updated Sucessfully';
		//$this->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'));
		$this->template = 'catalog/dataprocess.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function sortByOrder($a, $b) {
		$v1 = strtotime($a['fdate']);
	   	$v2 = strtotime($b['fdate']);
	   	return $v1 - $v2; // $v2 - $v1 to reverse direction
		// if ($a['punch_date'] == $b['punch_date']) {
	 		//return ($a['in_time'] > $b['in_time']) ? -1 : 1;;
	 	//}
	 	//return $a['punch_date'] - $b['punch_date'];
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function array_sort($array, $on, $order=SORT_ASC){

	    $new_array = array();
	    $sortable_array = array();

	    if (count($array) > 0) {
	        foreach ($array as $k => $v) {
	            if (is_array($v)) {
	                foreach ($v as $k2 => $v2) {
	                    if ($k2 == $on) {
	                        $sortable_array[$k] = $v2;
	                    }
	                }
	            } else {
	                $sortable_array[$k] = $v;
	            }
	        }

	        switch ($order) {
	            case SORT_ASC:
	                asort($sortable_array);
	                break;
	            case SORT_DESC:
	                arsort($sortable_array);
	                break;
	        }

	        foreach ($sortable_array as $k => $v) {
	            $new_array[$k] = $array[$k];
	        }
	    }

	    return $new_array;
	}
}
?>