<?php    
class ControllerSnippetsSnippet extends Controller { 
	private $error = array();

	public function index() {
		$months_array = array(
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5',
			'6' => '6',
			'7' => '7',
			'8' => '8',
			'9' => '9',
			'10' => '10',
			'11' => '11',
			'12' => '12',
		);

		$db_unit_datas = $this->db->query("SELECT * FROM `oc_unit` WHERE 1=1 ")->rows;
		$db_unit_data = array();
		foreach($db_unit_datas as $ukey => $uvalue){
			$db_unit_data[$uvalue['unit_id']] = html_entity_decode(strtolower($uvalue['unit']));
		}

		// echo '<pre>';
		// print_r($db_unit_data);
		// exit;

		$t = DIR_DOWNLOAD."employee_list_csv.csv";
		//echo $t;exit;
		$file=fopen($t,"r");
		$i=1;
		$department_data = array();
		$designtion_data = array();
		$unit_data = array();
		// $this->db->query("TRUNCATE TABLE `oc_employee`");
		// $this->db->query("TRUNCATE TABLE `oc_department`");
		// $this->db->query("TRUNCATE TABLE `oc_designation`");
		// $this->db->query("TRUNCATE TABLE `oc_shift_schedule`");
		// $this->db->query("TRUNCATE TABLE `oc_leave`");
		while(($var=fgetcsv($file,1000,","))!==FALSE){
			if($i != 1) {
				if($var[4] != '' && $var[4] != '-'){
					if(!isset($designtion_data[trim($var[4])])){
						$designtion_data[trim(ucwords($var[4]))] = html_entity_decode(strtolower(trim($var[4])));
					}	
				}
				if($var[5] != '' && $var[5] != '-'){
					if(!isset($department_data[trim($var[5])])){
						$department_data[trim(ucwords($var[5]))] = html_entity_decode(strtolower(trim($var[5])));
					}
				}
				if($var[6] != '' && $var[6] != '-'){
					$var6 = html_entity_decode(strtolower(trim($var[6])));
					if(!in_array($var6, $db_unit_data)){
						$var[6] = 'Rest India';
					}
					if(!isset($unit_data[trim($var[6])])){
						$unit_data[trim(ucwords($var[6]))] = html_entity_decode(strtolower(trim($var[6])));
					}
				}
				
			}
			$i ++;
		}
		fclose($file);
		// echo '<pre>';
		// print_r($designtion_data);
		// echo '<pre>';
		// print_r($department_data);
		// echo '<pre>';
		// print_r($unit_data);
		// exit;
		$department_data_linked = array();
		foreach($department_data as $dkey => $dvalue){
			$is_exist = $this->db->query("SELECT `department_id` FROM `oc_department` WHERE `d_name` = '".$this->db->escape($dkey)."' ");
			if($is_exist->num_rows == 0){
				echo "SELECT `department_id` FROM `oc_department` WHERE `d_name` = '".$this->db->escape($dkey)."' ";
				echo '<br />';
				echo $dkey;
				echo '<br />';
				echo $dvalue;
				echo '<br />';
				echo 'in';exit;
				$sql = "INSERT INTO `oc_department` SET `d_name` = '".$dkey."', `status` = '1' ";
				$this->db->query($sql);
				$department_id = $this->db->getLastId();
				$department_data_linked[$dvalue] = $department_id;
			} else {
				$department_id = $is_exist->row['department_id'];
				$department_data_linked[$dvalue] = $department_id;
			}
		}
		$designation_data_linked = array();
		foreach($designtion_data as $dkey => $dvalue){
			$is_exist = $this->db->query("SELECT `designation_id` FROM `oc_designation` WHERE `d_name` = '".$this->db->escape($dkey)."' ");
			if($is_exist->num_rows == 0){
				echo $dkey;
				echo '<br />';
				echo $dvalue;
				echo '<br />';
				echo 'in 1';exit;
				$sql = "INSERT INTO `oc_designation` SET `d_name` = '".$dkey."', `status` = '1' ";
				$this->db->query($sql);
				$designation_id = $this->db->getLastId();
				$designation_data_linked[$dvalue] = $designation_id;
			} else {
				$designation_id = $is_exist->row['designation_id'];
				$designation_data_linked[$dvalue] = $designation_id;
			}
		}

		$unit_data_linked = array();
		foreach($unit_data as $dkey => $dvalue){
			$is_exist = $this->db->query("SELECT `unit_id` FROM `oc_unit` WHERE `unit` = '".$this->db->escape($dkey)."' ");
			if($is_exist->num_rows == 0){
				echo $dkey;
				echo '<br />';
				echo $dvalue;
				echo '<br />';
				echo 'in 2';exit;
				$sql = "INSERT INTO `oc_unit` SET `unit` = '".$dkey."', `status` = '1' ";
				$this->db->query($sql);
				$unit_id = $this->db->getLastId();
				$unit_data_linked[$dvalue] = $unit_id;
			} else {
				$unit_id = $is_exist->row['unit_id'];
				$unit_data_linked[$dvalue] = $unit_id;
			}
		}

		// echo '<pre>';
		// print_r($department_data_linked);
		// echo '<pre>';
		// print_r($designation_data_linked);
		// echo '<pre>';
		// print_r($unit_data_linked);
		// exit;
		//echo 'out';exit;

		$i=1;
		$file=fopen($t,"r");
		while(($var=fgetcsv($file,1000,","))!==FALSE){
			if($i != 1) {
				$emp_code=addslashes($var[11]);//emp_code
				if($emp_code != ''){
					$first_name=addslashes($var[0]);//first_name
					$middile_name=addslashes($var[1]);//middile_name
					$last_name=addslashes($var[2]);//last_name
					$name = $first_name.' '.$middile_name.' '.$last_name;
					$shift_id=addslashes($var[3]);//shift_id
					$designation=addslashes(ucwords($var[4]));//designation
					$designations=html_entity_decode(strtolower(trim($var[4])));//designation
					if($designations != '' && $designations != '-'){
						$designation_id=$designation_data_linked[$designations];//designation_id
					} else {
						$designation_id=0;
					}
					$department=addslashes(ucwords($var[5]));//department
					$departments=html_entity_decode(strtolower(trim($var[5])));//department
					if($departments != '' && $departments != '-'){
						$department_id=$department_data_linked[$departments];//department_id
					} else {
						$department_id=0;
					}
					$unit=addslashes(ucwords($var[6]));//unit
					$unitss=html_entity_decode(strtolower(trim($var[6])));//unit
					if(!in_array($unitss, $db_unit_data)){
						$unit = 'Rest India';
					}
					$units=html_entity_decode(strtolower(trim($var[6])));//unit
					if($units != '' && $units != '-'){
						if(!in_array($units, $db_unit_data)){
							$units = 'rest india';
						}
						$unit_id=$unit_data_linked[$units];//unit_id
					} else {
						$unit_id = 0;
					}
					$gender=addslashes(ucwords($var[7]));//gender
					$martial_status=addslashes(ucwords($var[8]));//martial_status
					$blood_group=addslashes($var[9]);//blood_group
					$reporting_to_name=addslashes($var[10]);//reporting_to_name
					$reporting_to = '';
					if($reporting_to_name != ''){
						$reporting_to_name_exp = explode(' ', $reporting_to_name);
						if(!isset($reporting_to_name_exp[1])){
							$reporting_to_name_exp[1] = '';
						}
						$emp_exist = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE `first_name` = '".$this->db->escape($reporting_to_name_exp[0])."' AND `last_name` = '".$this->db->escape($reporting_to_name_exp[1])."' ");
						if($emp_exist->num_rows > 0){
							$reporting_to = $emp_exist->row['emp_code'];
						}
					}
					$emp_code=addslashes($var[11]);//emp_code
					
					$dob=addslashes($var[12]);//dob
					if($dob != ''){
						$dob_exp = explode('.', $dob);
						if(isset($dob_exp[2])){
							$dob_string = $dob_exp[2].'-'.$dob_exp[1].'-'.$dob_exp[0];
							$dob = Date('Y-m-d', strtotime($dob_string));//dob
						} else {
							$dob = '0000-00-00';
						}
					} else {
						$dob = '0000-00-00';
					}

					$doj=addslashes($var[13]);//doj
					if($doj != ''){
						$doj_exp = explode('.', $doj);
						if(isset($doj_exp[2])){
							$doj_string = $doj_exp[2].'-'.$doj_exp[1].'-'.$doj_exp[0];
							$doj = Date('Y-m-d', strtotime($doj_string));//doj
						} else {
							$doj = '0000-00-00';
						}
					} else {
						$doj = '0000-00-00';
					}

					$doc=addslashes($var[14]);//doc
					if($doc != ''){
						$doc_exp = explode('.', $doc);
						if(isset($doc_exp[2])){
							$doc_string = $doc_exp[2].'-'.$doc_exp[1].'-'.$doc_exp[0];
							$doc = Date('Y-m-d', strtotime($doc_string));//doc
						} else {
							$doc = '0000-00-00';
						}
					} else {
						$doc = '0000-00-00';
					}

					$dol=addslashes($var[15]);//dol
					if($dol != ''){
						$dol_exp = explode('.', $dol);
						if(isset($dol_exp[2])){
							$dol_string = $dol_exp[2].'-'.$dol_exp[1].'-'.$dol_exp[0];
							$dol = Date('Y-m-d', strtotime($dol_string));//dol
						} else {
							$dol = '0000-00-00';
						}
					} else {
						$dol = '0000-00-00';
					}

					$pre_com_uan_no=addslashes($var[16]);//pre_com_uan_no
					$Gross_salary=addslashes($var[17].' '.$var[18]);//Currency + Gross_salary
					$status=addslashes(ucwords($var[19]));//status
					if($status == 'Active' || $status == ''){
						$status = 1;
					}
					$findme = '~';
					$bank_name=addslashes($var[20]);//bank_name
					
					$ac_no=addslashes($var[21]);//ac_no
					$pos_ac_no = strpos($ac_no, $findme);
					if($pos_ac_no !== false){
						$ac_no = substr($ac_no, 1);
					}
					
					$branch_name=addslashes($var[22]);//branch_name
					$ifsc_code=addslashes($var[23]);//ifsc_code
					$pan_card=addslashes($var[24]);//pan_card
					
					$aadhar_no=addslashes($var[25]);//aadhar_no
					$pos_aadhar_no = strpos($aadhar_no, $findme);
					if($pos_aadhar_no !== false){
						$aadhar_no = substr($aadhar_no, 1);
					}
					
					$email=addslashes($var[26]);//email
					
					$personal_no=addslashes($var[27]);//personal_no
					$pos_personal_no = strpos($personal_no, $findme);
					if($pos_personal_no !== false){
						$personal_no = substr($personal_no, 1);
					}

					$emg_no=addslashes($var[28]);//emg_no
					$pos_emg_no = strpos($emg_no, $findme);
					if($pos_emg_no !== false){
						$emg_no = substr($emg_no, 1);
					}

					$emg_per=addslashes($var[29]);//emg_per
					$address=addslashes($var[30]);//address
					$education=addslashes($var[31]);//education
					$year_experience=addslashes($var[32]);//year_experience
					$user_name = $emp_code;
					$salt = substr(md5(uniqid(rand(), true)), 0, 9);
					$password = sha1($salt . sha1($salt . sha1($emp_code)));

					$shift_id = 1;
					$shift_datas = $this->db->query("SELECT `shift_id` FROM `oc_shift_weekoff` WHERE `unit_id` = '".$unit_id."' ");
					if($shift_datas->num_rows > 0){
						$shift_id = $shift_datas->row['shift_id'];
					}
					$is_exist = $this->db->query("SELECT * FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ");
					// echo '<pre>';
					// print_r($is_exist);
					// exit;
					if($is_exist->num_rows == 0){
						$insert = "INSERT INTO `oc_employee` SET 
									`emp_code` = '".$emp_code."', 
									`first_name` = '".$first_name."', 
									`middile_name` = '".$middile_name."', 
									`last_name` = '".$last_name."', 
									`name` = '".$name."', 
									`shift_id` = '".$shift_id."', 
									`designation` = '".$designation."', 
									`designation_id` = '".$designation_id."', 
									`department` = '".$department."',
									`department_id` = '".$department_id."',
									`unit` = '".$unit."', 
									`unit_id` = '".$unit_id."', 
									`gender` = '".$gender."', 
									`martial_status` = '".$martial_status."', 
									`blood_group` = '".$blood_group."', 
									`dob` = '".$dob."', 
									`doj` = '".$doj."', 
									`doc` = '".$doc."', 
									`dol` = '".$dol."', 
									`reporting_to` = '".$reporting_to."', 
									`reporting_to_name` = '".$reporting_to_name."', 
									`pre_com_uan_no` = '".$pre_com_uan_no."', 
									`Gross_salary` = '".$Gross_salary."', 
									`status` = '".$status."', 
									`bank_name` = '".$bank_name."', 
									`ac_no` = '".$ac_no."', 
									`branch_name` = '".$branch_name."',
									`ifsc_code` = '".$ifsc_code."', 
									`pan_card_no` = '".$pan_card."',
									`aadhar_no` = '".$aadhar_no."', 
									`email` = '".$email."', 
									`personal_no` = '".$personal_no."', 
									`emg_no` = '".$emg_no."', 
									`emg_per` = '".$emg_per."', 
									`address` = '".$address."', 
									`education` = '".$education."', 
									`year_experience` = '".$year_experience."', 
									`shift_type` = 'F',
									`user_group_id` = '11',
									`username` = '".$user_name."', 
									`password` = '".$password."', 
									`salt` = '".$salt."' "; 
						// echo $insert;
						// echo '<br />';
						// echo '<br />';
						//exit;
						$this->db->query($insert);
					} else {
						$update = "UPDATE `oc_employee` SET 
									`emp_code` = '".$emp_code."', 
									`first_name` = '".$first_name."', 
									`middile_name` = '".$middile_name."', 
									`last_name` = '".$last_name."', 
									`name` = '".$name."', 
									`shift_id` = '".$shift_id."', 
									`designation` = '".$designation."', 
									`designation_id` = '".$designation_id."', 
									`department` = '".$department."',
									`department_id` = '".$department_id."',
									`unit` = '".$unit."', 
									`unit_id` = '".$unit_id."', 
									`gender` = '".$gender."', 
									`martial_status` = '".$martial_status."', 
									`blood_group` = '".$blood_group."', 
									`dob` = '".$dob."', 
									`doj` = '".$doj."', 
									`doc` = '".$doc."', 
									`dol` = '".$dol."', 
									`reporting_to` = '".$reporting_to."', 
									`reporting_to_name` = '".$reporting_to_name."', 
									`pre_com_uan_no` = '".$pre_com_uan_no."', 
									`Gross_salary` = '".$Gross_salary."', 
									`status` = '".$status."', 
									`bank_name` = '".$bank_name."', 
									`ac_no` = '".$ac_no."', 
									`branch_name` = '".$branch_name."',
									`ifsc_code` = '".$ifsc_code."', 
									`pan_card_no` = '".$pan_card."',
									`aadhar_no` = '".$aadhar_no."', 
									`email` = '".$email."', 
									`personal_no` = '".$personal_no."', 
									`emg_no` = '".$emg_no."', 
									`emg_per` = '".$emg_per."', 
									`address` = '".$address."', 
									`education` = '".$education."', 
									`year_experience` = '".$year_experience."', 
									`shift_type` = 'F',
									`user_group_id` = '11',
									`username` = '".$user_name."'
									WHERE `emp_code` = '".$is_exist->row['emp_code']."' ";
						// echo $update;
						// echo '<br />';
						// echo '<br />';
						//exit; 
						$this->db->query($update);
					}

					$shift_id = 'S_'.$shift_id;
					foreach($months_array as $mkey => $mvalue){
						/*
						$is_exist = $this->db->query("SELECT * FROM `oc_shift_schedule` WHERE `emp_code` = '".$emp_code."' AND `month` = '".$mvalue."' AND `year` = '2017' ");
						if($is_exist->num_rows == 0){
							$sql = "INSERT INTO `oc_shift_schedule` SET 
									`1` = '".$shift_id."',
									`2` = '".$shift_id."',
									`3` = '".$shift_id."',
									`4` = '".$shift_id."',
									`5` = '".$shift_id."',
									`6` = '".$shift_id."',
									`7` = '".$shift_id."',
									`8` = '".$shift_id."',
									`9` = '".$shift_id."',
									`10` = '".$shift_id."',
									`11` = '".$shift_id."',
									`12` = '".$shift_id."',
									`13` = '".$shift_id."',
									`14` = '".$shift_id."',
									`15` = '".$shift_id."',
									`16` = '".$shift_id."',
									`17` = '".$shift_id."',
									`18` = '".$shift_id."',
									`19` = '".$shift_id."',
									`20` = '".$shift_id."',
									`21` = '".$shift_id."',
									`22` = '".$shift_id."',
									`23` = '".$shift_id."',
									`24` = '".$shift_id."',
									`25` = '".$shift_id."',
									`26` = '".$shift_id."',
									`27` = '".$shift_id."',
									`28` = '".$shift_id."',
									`29` = '".$shift_id."',
									`30` = '".$shift_id."',
									`31` = '".$shift_id."',
									`month` = '".$mvalue."',
									`year` = '2017',
									`emp_code` = '".$emp_code."',
									`unit` = '".$this->db->escape($unit)."', 
									`unit_id` = '".$this->db->escape($unit_id)."' ";
							// echo $sql;
							// echo '<br />';
							//exit;
							$this->db->query($sql);
						}
						*/
						$is_exist = $this->db->query("SELECT * FROM `oc_shift_schedule` WHERE `emp_code` = '".$emp_code."' AND `month` = '".$mvalue."' AND `year` = '".date('Y')."' ");
						if($is_exist->num_rows > 0){
							$sql = "UPDATE `oc_shift_schedule` SET 
									`1` = '".$shift_id."',
									`2` = '".$shift_id."',
									`3` = '".$shift_id."',
									`4` = '".$shift_id."',
									`5` = '".$shift_id."',
									`6` = '".$shift_id."',
									`7` = '".$shift_id."',
									`8` = '".$shift_id."',
									`9` = '".$shift_id."',
									`10` = '".$shift_id."',
									`11` = '".$shift_id."',
									`12` = '".$shift_id."',
									`13` = '".$shift_id."',
									`14` = '".$shift_id."',
									`15` = '".$shift_id."',
									`16` = '".$shift_id."',
									`17` = '".$shift_id."',
									`18` = '".$shift_id."',
									`19` = '".$shift_id."',
									`20` = '".$shift_id."',
									`21` = '".$shift_id."',
									`22` = '".$shift_id."',
									`23` = '".$shift_id."',
									`24` = '".$shift_id."',
									`25` = '".$shift_id."',
									`26` = '".$shift_id."',
									`27` = '".$shift_id."',
									`28` = '".$shift_id."',
									`29` = '".$shift_id."',
									`30` = '".$shift_id."',
									`31` = '".$shift_id."',
									`month` = '".$mvalue."',
									`year` = '".date('Y')."',
									`emp_code` = '".$emp_code."',
									`unit` = '".$this->db->escape($unit)."', 
									`unit_id` = '".$this->db->escape($unit_id)."' 
									WHERE `id` = '".$is_exist->row['id']."' ";
							// echo $sql;
							// echo '<br />';
							// exit;
							$this->db->query($sql);
						} else {
							$sql = "INSERT INTO `oc_shift_schedule` SET 
									`1` = '".$shift_id."',
									`2` = '".$shift_id."',
									`3` = '".$shift_id."',
									`4` = '".$shift_id."',
									`5` = '".$shift_id."',
									`6` = '".$shift_id."',
									`7` = '".$shift_id."',
									`8` = '".$shift_id."',
									`9` = '".$shift_id."',
									`10` = '".$shift_id."',
									`11` = '".$shift_id."',
									`12` = '".$shift_id."',
									`13` = '".$shift_id."',
									`14` = '".$shift_id."',
									`15` = '".$shift_id."',
									`16` = '".$shift_id."',
									`17` = '".$shift_id."',
									`18` = '".$shift_id."',
									`19` = '".$shift_id."',
									`20` = '".$shift_id."',
									`21` = '".$shift_id."',
									`22` = '".$shift_id."',
									`23` = '".$shift_id."',
									`24` = '".$shift_id."',
									`25` = '".$shift_id."',
									`26` = '".$shift_id."',
									`27` = '".$shift_id."',
									`28` = '".$shift_id."',
									`29` = '".$shift_id."',
									`30` = '".$shift_id."',
									`31` = '".$shift_id."',
									`month` = '".$mvalue."',
									`year` = '".date('Y')."',
									`emp_code` = '".$emp_code."',
									`unit` = '".$this->db->escape($unit)."', 
									`unit_id` = '".$this->db->escape($unit_id)."' ";
							// echo $sql;
							// echo '<br />';
							//exit;
							$this->db->query($sql);
						}
					}

					$current_year = '2018';
					$next_year = date('Y') + 1;
					$start = new DateTime($current_year.'-01-01');
					$end   = new DateTime($next_year.'-01-01');
					$interval = DateInterval::createFromDateString('1 day');
					$period = new DatePeriod($start, $interval, $end);
					$week_array = array();
					foreach ($period as $dt) {
					    if ($dt->format('N') == 5 || $dt->format('N') == 6 || $dt->format('N') == 7) {
					    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
					    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
					    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
					    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
					    	$week_array[$dt->format('Y-m-d')]['compare_day'] = $dt->format('N');
					    }
					}
					// echo '<pre>';
					// print_r($week_array);
					// exit;
					$employee_datas = $this->db->query("SELECT `emp_code`, `unit`, `unit_id`, `shift_id` FROM `oc_employee` WHERE `status` = '1' AND `emp_code` = '".$emp_code."' ")->rows;
					foreach($employee_datas as $ekey => $evalue){
						$weekly_off = 'W_1_'.$evalue['shift_id'];
						$half_day = 'HD_1_'.$evalue['shift_id'];
						foreach ($week_array as $wkey => $wvalue) {
							$is_week_off = $this->db->query("SELECT `week_day` FROM `oc_shift_weekoff` WHERE `shift_id` = '".$evalue['shift_id']."' AND `week_day` = '".$wvalue['compare_day']."' AND `unit_id` = '".$evalue['unit_id']."' ");
							if($is_week_off->num_rows > 0){
								$sql = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$weekly_off."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$evalue['emp_code']."' ";	
				        		// echo $sql;
				        		// echo '<br />';
				        		$this->db->query($sql);
							}

							$is_half_day = $this->db->query("SELECT `half_day` FROM `oc_shift_weekoff` WHERE `shift_id` = '".$evalue['shift_id']."' AND `half_day` = '".$wvalue['compare_day']."' AND `unit_id` = '".$evalue['unit_id']."' ");
							if($is_half_day->num_rows > 0){
								$sql = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$half_day."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$evalue['emp_code']."' ";	
				        		// echo $sql;
				        		// echo '<br />';
				        		$this->db->query($sql);
							}
						}
					}

					$holidayloc_sql = "SELECT `holiday_id`, `location`, `location_id` FROM `oc_holiday_loc` WHERE `location_id` = '".$unit_id."' ";
					$holidayloc_datas = $this->db->query($holidayloc_sql);
					if($holidayloc_datas->num_rows > 0){
						$holidayloc_data = $holidayloc_datas->rows;
						if($doj == '' || $doj == '0000-00-00'){
							$dojj = '1970-01-01';
						} else {
							$dojj = $doj;
						}
						foreach($holidayloc_data as $hkey => $hvalue){
							$holiday_sql = "SELECT `date` FROM `oc_holiday` WHERE `holiday_id` = '".$hvalue['holiday_id']."' AND `date` >= '".$dojj."' ";
							$holiday_datas = $this->db->query($holiday_sql);
							if($holiday_datas->num_rows > 0){
								$holiday_data = $holiday_datas->row;
								$date = $holiday_data['date'];
								$month = date('n', strtotime($date));
								$year = date('Y', strtotime($date));
								$day = date('j', strtotime($date));
								$holiday_string = 'H_'.$hvalue['holiday_id'].'_'.$shift_id;
								$update_shift_schedule = "UPDATE `oc_shift_schedule` SET `".$day."` = '".$holiday_string."' WHERE `month` = '".$month."' AND `year` = '".$year."' AND `emp_code` = '".$emp_code."' ";
								// echo $update_shift_schedule;
								// echo '<br />';
								// //exit;
								$this->db->query($update_shift_schedule);
							}
						}
						//echo 'out';exit;
					}
					// $is_exist = $this->db->query("SELECT `emp_id`, `leave_id` FROM `oc_leave` WHERE `emp_id` = '".$emp_code."' AND `year` = '2017' ");
					// if($is_exist->num_rows == 0){
					// 	$insert2 = "INSERT INTO `oc_leave` SET 
					// 				`emp_id` = '".$emp_code."', 
					// 				`emp_name` = '".$name."', 
					// 				`emp_doj` = '".$doj."', 
					// 				`year` = '2017',
					// 				`close_status` = '1' "; 
					// 	$this->db->query($insert2);
					// }
					$is_exist = $this->db->query("SELECT `emp_id`, `leave_id` FROM `oc_leave` WHERE `emp_id` = '".$emp_code."' AND `year` = '".date('Y')."' ");
					if($is_exist->num_rows == 0){
						$insert2 = "INSERT INTO `oc_leave` SET 
									`emp_id` = '".$emp_code."', 
									`emp_name` = '".$name."', 
									`emp_doj` = '".$doj."', 
									`year` = '".date('Y')."',
									`close_status` = '0' "; 
						$this->db->query($insert2);
					}
				}
				//echo 'Done';exit;
			}
			$i ++;	
		}
		fclose($file);
		echo 'out';exit;
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function array_sort($array, $on, $order=SORT_ASC){

	    $new_array = array();
	    $sortable_array = array();

	    if (count($array) > 0) {
	        foreach ($array as $k => $v) {
	            if (is_array($v)) {
	                foreach ($v as $k2 => $v2) {
	                    if ($k2 == $on) {
	                        $sortable_array[$k] = $v2;
	                    }
	                }
	            } else {
	                $sortable_array[$k] = $v;
	            }
	        }

	        switch ($order) {
	            case SORT_ASC:
	                asort($sortable_array);
	                break;
	            case SORT_DESC:
	                arsort($sortable_array);
	                break;
	        }

	        foreach ($sortable_array as $k => $v) {
	            $new_array[$k] = $array[$k];
	        }
	    }

	    return $new_array;
	}
}
?>