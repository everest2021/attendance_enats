<?php    
class ControllerSnippetsSnippet1 extends Controller { 
	private $error = array();

	public function index() {
		$employee_datas = $this->db->query("SELECT `emp_code`, `unit`, `unit_id`, `shift_id` FROM `oc_employee` WHERE 1=1 ")->rows;
		// echo '<pre>';
		// print_r($employee_datas);
		// exit;
		foreach($employee_datas as $ekey => $evalue){
			if($evalue['unit_id'] == 1 || $evalue['unit_id'] == 2 || $evalue['unit_id'] == 3 || $evalue['unit_id'] == 4 || $evalue['unit_id'] == 5){
				$unit_id = $evalue['unit_id'];
			} else {
				$unit_id = 2;
			}
			$week_offs = $this->db->query("SELECT `week_day`, `half_day` FROM `oc_shift_weekoff` WHERE `shift_id` = '".$evalue['shift_id']."' AND `unit_id` = '".$unit_id."' ORDER BY `id` ")->rows;
			$week_1 = 0;
			$week_2 = 0;
			$full_day = 1;
			// echo '<pre>';
			// print_r($week_offs);
			// exit;
			foreach($week_offs as $wkey => $wvalue){
				if($wkey == 1){
					if($wvalue['week_day'] != '0'){
						$week_1 = $wvalue['week_day'];
					}
				}
				if($wkey == 0){
					if($wvalue['week_day'] != '0'){
						$week_2 = $wvalue['week_day'];
					}
					if($wvalue['half_day'] != '0'){
						$week_2 = $wvalue['half_day'];
						$full_day = 0;
					}
				}
			}
			$sql = "UPDATE `oc_employee` SET `week_1` = '".$week_1."', `week_2` = '".$week_2."', `full_day` = '".$full_day."' WHERE `emp_code` = '".$evalue['emp_code']."' ";
			// echo $sql;
			// echo '<br />';
			// echo '<br />';
			//exit;
			$this->db->query($sql);
		}
		echo 'Done';exit;

		$leave_credit_datas = $this->db->query("SELECT `emp_code` FROM `oc_leave_credit_transaction` WHERE 1=1 ")->rows;
		// echo '<pre>';
		// print_r($leave_credit_datas);
		// exit;
		foreach($leave_credit_datas as $lkey => $lvalue){
			$leave_datas = $this->db->query("SELECT `leave_id`, `pl_acc` FROM `oc_leave` WHERE `emp_id` = '".$lvalue['emp_code']."' AND `close_status` = '0' ");
			if($leave_datas->num_rows > 0){
				$leave_data = $leave_datas->row;
				$sub_leave = $leave_data['pl_acc'] - 2; 
				$sql = "UPDATE `oc_leave` SET `pl_acc` = '".$sub_leave."' WHERE `leave_id` = '".$leave_data['leave_id']."' ";
				// echo $sql;
				// echo '<br />';
				//exit;
				$this->db->query($sql);
			}
		}
		echo 'Done';exit;

		$months_array = array(
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5',
			'6' => '6',
			'7' => '7',
			'8' => '8',
			'9' => '9',
			'10' => '10',
			'11' => '11',
			'12' => '12',
		);
		$current_year = '2018';
		$next_year = date('Y') + 1;
		$start = new DateTime($current_year.'-01-01');
		$end   = new DateTime($next_year.'-01-01');
		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($start, $interval, $end);
		$week_array = array();
		foreach ($period as $dt) {
		    if ($dt->format('N') == 5 || $dt->format('N') == 6 || $dt->format('N') == 7) {
		    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
		    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
		    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
		    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
		    	$week_array[$dt->format('Y-m-d')]['compare_day'] = $dt->format('N');
		    }
		}
		// echo '<pre>';
		// print_r($week_array);
		// exit;
		$employee_datas = $this->db->query("SELECT `emp_code`, `unit`, `unit_id`, `shift_id` FROM `oc_employee` WHERE 1=1 ORDER BY `employee_id` ")->rows;
		// echo '<pre>';
		// print_r($employee_datas);
		// exit;
		foreach($employee_datas as $ekey => $evalue){
			// if($evalue['unit_id'] == 1 || $evalue['unit_id'] == 3 || $evalue['unit_id'] == 4){
			// 	$shift_id = 1;
			// } elseif($evalue['unit_id'] == 2) {
			// 	$shift_id = 2;
			// } elseif($evalue['unit_id'] == 5) {
			// 	$shift_id = 3;
			// } elseif($evalue['unit_id'] == 6) {
			// 	$shift_id = 4;
			// } elseif($evalue['unit_id'] == 7) {
			// 	$shift_id = 5;
			// } else {
			// 	$shift_id = 2;
			// }
			// echo '<pre>';
			// print_r($shift_id);
			// exit;
			// $update_sql = "UPDATE `oc_employee` SET `shift_id` = '".$shift_id."' WHERE `emp_code` = '".$evalue['emp_code']."' ";  
			// $this->db->query($update_sql);
			// echo $update_sql;
			// echo '<br />';
			//exit;
			
			$shift_id = 'S_'.$evalue['shift_id'];
			$emp_code = $evalue['emp_code'];
			$unit = $evalue['unit'];
			$unit_id = $evalue['unit_id'];
			foreach($months_array as $mkey => $mvalue){
				$is_exist = $this->db->query("SELECT * FROM `oc_shift_schedule` WHERE `emp_code` = '".$emp_code."' AND `month` = '".$mvalue."' AND `year` = '2018' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_shift_schedule` SET 
							`1` = '".$shift_id."',
							`2` = '".$shift_id."',
							`3` = '".$shift_id."',
							`4` = '".$shift_id."',
							`5` = '".$shift_id."',
							`6` = '".$shift_id."',
							`7` = '".$shift_id."',
							`8` = '".$shift_id."',
							`9` = '".$shift_id."',
							`10` = '".$shift_id."',
							`11` = '".$shift_id."',
							`12` = '".$shift_id."',
							`13` = '".$shift_id."',
							`14` = '".$shift_id."',
							`15` = '".$shift_id."',
							`16` = '".$shift_id."',
							`17` = '".$shift_id."',
							`18` = '".$shift_id."',
							`19` = '".$shift_id."',
							`20` = '".$shift_id."',
							`21` = '".$shift_id."',
							`22` = '".$shift_id."',
							`23` = '".$shift_id."',
							`24` = '".$shift_id."',
							`25` = '".$shift_id."',
							`26` = '".$shift_id."',
							`27` = '".$shift_id."',
							`28` = '".$shift_id."',
							`29` = '".$shift_id."',
							`30` = '".$shift_id."',
							`31` = '".$shift_id."',
							`month` = '".$mvalue."',
							`year` = '2018',
							`emp_code` = '".$emp_code."',
							`unit` = '".$this->db->escape($unit)."', 
							`unit_id` = '".$this->db->escape($unit_id)."' ";
					// echo $sql;
					// echo '<br />';
					//exit;
					$this->db->query($sql);
				} else {
					$sql = "UPDATE `oc_shift_schedule` SET 
							`1` = '".$shift_id."',
							`2` = '".$shift_id."',
							`3` = '".$shift_id."',
							`4` = '".$shift_id."',
							`5` = '".$shift_id."',
							`6` = '".$shift_id."',
							`7` = '".$shift_id."',
							`8` = '".$shift_id."',
							`9` = '".$shift_id."',
							`10` = '".$shift_id."',
							`11` = '".$shift_id."',
							`12` = '".$shift_id."',
							`13` = '".$shift_id."',
							`14` = '".$shift_id."',
							`15` = '".$shift_id."',
							`16` = '".$shift_id."',
							`17` = '".$shift_id."',
							`18` = '".$shift_id."',
							`19` = '".$shift_id."',
							`20` = '".$shift_id."',
							`21` = '".$shift_id."',
							`22` = '".$shift_id."',
							`23` = '".$shift_id."',
							`24` = '".$shift_id."',
							`25` = '".$shift_id."',
							`26` = '".$shift_id."',
							`27` = '".$shift_id."',
							`28` = '".$shift_id."',
							`29` = '".$shift_id."',
							`30` = '".$shift_id."',
							`31` = '".$shift_id."',
							`month` = '".$mvalue."',
							`year` = '2018',
							`emp_code` = '".$emp_code."',
							`unit` = '".$this->db->escape($unit)."', 
							`unit_id` = '".$this->db->escape($unit_id)."' 
							WHERE `id` = '".$is_exist->row['id']."' ";
					// echo $sql;
					// echo '<br />';
					// exit;
					$this->db->query($sql);
				}
			}

			if($evalue['unit_id'] == 1 || $evalue['unit_id'] == 2 || $evalue['unit_id'] == 3 || $evalue['unit_id'] == 4 || $evalue['unit_id'] == 5){
				$unit_id = $evalue['unit_id'];
			} else {
				$unit_id = 2;
			}

			$shift_id = $evalue['shift_id'];
			$weekly_off = 'W_1_'.$shift_id;
			$half_day = 'HD_1_'.$shift_id;
			foreach ($week_array as $wkey => $wvalue) {
				$is_week_off = $this->db->query("SELECT `week_day` FROM `oc_shift_weekoff` WHERE `shift_id` = '".$shift_id."' AND `week_day` = '".$wvalue['compare_day']."' AND `unit_id` = '".$unit_id."' ");
				if($is_week_off->num_rows > 0){
					$sql = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$weekly_off."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$evalue['emp_code']."' ";	
	        		// echo $sql;
	        		// echo '<br />';
	        		$this->db->query($sql);
				}

				$is_half_day = $this->db->query("SELECT `half_day` FROM `oc_shift_weekoff` WHERE `shift_id` = '".$shift_id."' AND `half_day` = '".$wvalue['compare_day']."' AND `unit_id` = '".$unit_id."' ");
				if($is_half_day->num_rows > 0){
					$sql = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$half_day."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$evalue['emp_code']."' ";	
	        		// echo $sql;
	        		// echo '<br />';
	        		$this->db->query($sql);
				}
			}
			//echo '<br />';
			//echo 'out';exit;
		}
		echo 'Done';exit;

		// $employee_datas = $this->db->query("SELECT `emp_code`, `unit`, `unit_id` FROM `oc_employee` WHERE `status` = '1' ")->rows;
		// foreach($employee_datas as $ekey => $evalue){
		// 	$shift_datas = $this->db->query("SELECT `shift_id` FROM `oc_shift_weekoff` WHERE `unit_id` = '".$evalue['unit_id']."' ");
		// 	if($shift_datas->num_rows > 0){
		// 		$shift_id = $shift_datas->row['shift_id'];
		// 		$sql = "UPDATE `oc_employee` SET 
		// 				`shift_id` = '".$shift_id."'
		// 				WHERE `emp_code` = '".$evalue['emp_code']."' ";
		// 		// echo $sql;
		// 		// echo '<br />';
		// 		// exit;
		// 		$this->db->query($sql);
		// 	}
		// }
		// echo 'Done';exit;

		/*
		$employee_datas = $this->db->query("SELECT `emp_code`, `unit`, `unit_id`, `shift_id` FROM `oc_employee` WHERE `status` = '1' ")->rows;
		$months_array = array(
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5',
			'6' => '6',
			'7' => '7',
			'8' => '8',
			'9' => '9',
			'10' => '10',
			'11' => '11',
			'12' => '12',
		);
		// echo '<pre>';
		// print_r($months_array);
		// exit;
		foreach($employee_datas as $ekey => $evalue){
			$shift_id = 'S_'.$evalue['shift_id'];
			foreach($months_array as $mkey => $mvalue){
				$is_exist = $this->db->query("SELECT * FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$mvalue."' AND `year` = '".date('Y')."' ");
				if($is_exist->num_rows > 0){
					$sql = "UPDATE `oc_shift_schedule` SET 
							`1` = '".$shift_id."',
							`2` = '".$shift_id."',
							`3` = '".$shift_id."',
							`4` = '".$shift_id."',
							`5` = '".$shift_id."',
							`6` = '".$shift_id."',
							`7` = '".$shift_id."',
							`8` = '".$shift_id."',
							`9` = '".$shift_id."',
							`10` = '".$shift_id."',
							`11` = '".$shift_id."',
							`12` = '".$shift_id."',
							`13` = '".$shift_id."',
							`14` = '".$shift_id."',
							`15` = '".$shift_id."',
							`16` = '".$shift_id."',
							`17` = '".$shift_id."',
							`18` = '".$shift_id."',
							`19` = '".$shift_id."',
							`20` = '".$shift_id."',
							`21` = '".$shift_id."',
							`22` = '".$shift_id."',
							`23` = '".$shift_id."',
							`24` = '".$shift_id."',
							`25` = '".$shift_id."',
							`26` = '".$shift_id."',
							`27` = '".$shift_id."',
							`28` = '".$shift_id."',
							`29` = '".$shift_id."',
							`30` = '".$shift_id."',
							`31` = '".$shift_id."',
							`month` = '".$mvalue."',
							`year` = '".date('Y')."',
							`emp_code` = '".$evalue['emp_code']."',
							`unit` = '".$this->db->escape($evalue['unit'])."', 
							`unit_id` = '".$this->db->escape($evalue['unit_id'])."' 
							WHERE `id` = '".$is_exist->row['id']."' ";
					// echo $sql;
					// echo '<br />';
					// exit;
					$this->db->query($sql);
				} else {
					$sql = "INSERT INTO `oc_shift_schedule` SET 
							`1` = '".$shift_id."',
							`2` = '".$shift_id."',
							`3` = '".$shift_id."',
							`4` = '".$shift_id."',
							`5` = '".$shift_id."',
							`6` = '".$shift_id."',
							`7` = '".$shift_id."',
							`8` = '".$shift_id."',
							`9` = '".$shift_id."',
							`10` = '".$shift_id."',
							`11` = '".$shift_id."',
							`12` = '".$shift_id."',
							`13` = '".$shift_id."',
							`14` = '".$shift_id."',
							`15` = '".$shift_id."',
							`16` = '".$shift_id."',
							`17` = '".$shift_id."',
							`18` = '".$shift_id."',
							`19` = '".$shift_id."',
							`20` = '".$shift_id."',
							`21` = '".$shift_id."',
							`22` = '".$shift_id."',
							`23` = '".$shift_id."',
							`24` = '".$shift_id."',
							`25` = '".$shift_id."',
							`26` = '".$shift_id."',
							`27` = '".$shift_id."',
							`28` = '".$shift_id."',
							`29` = '".$shift_id."',
							`30` = '".$shift_id."',
							`31` = '".$shift_id."',
							`month` = '".$mvalue."',
							`year` = '".date('Y')."',
							`emp_code` = '".$evalue['emp_code']."',
							`unit` = '".$this->db->escape($evalue['unit'])."', 
							`unit_id` = '".$this->db->escape($evalue['unit_id'])."' ";
					// echo $sql;
					// echo '<br />';
					//exit;
					$this->db->query($sql);
				}
			}
			//echo '<br />';
			//echo '<br />';
		}
		echo 'Done';exit;
		*/

		
		$no = 0;
		$start = new DateTime('2017-01-01');
		$end   = new DateTime('2018-01-01');
		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($start, $interval, $end);
		$week_array = array();
		foreach ($period as $dt) {
		    if ($dt->format('N') == 5 || $dt->format('N') == 6 || $dt->format('N') == 7) {
		    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
		    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
		    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
		    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
		    	$week_array[$dt->format('Y-m-d')]['compare_day'] = $dt->format('N');
		        $no++;
		    }
		}
		// echo '<pre>';
		// print_r($week_array);
		// exit;
		$employee_datas = $this->db->query("SELECT `emp_code`, `unit`, `unit_id`, `shift_id` FROM `oc_employee` WHERE `status` = '1' ")->rows;
		foreach($employee_datas as $ekey => $evalue){
			$weekly_off = 'W_1_'.$evalue['shift_id'];
			$half_day = 'HD_1_'.$evalue['shift_id'];
			foreach ($week_array as $wkey => $wvalue) {
				$is_week_off = $this->db->query("SELECT `week_day` FROM `oc_shift_weekoff` WHERE `shift_id` = '".$evalue['shift_id']."' AND `week_day` = '".$wvalue['compare_day']."' AND `unit_id` = '".$evalue['unit_id']."' ");
				if($is_week_off->num_rows > 0){
					$sql = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$weekly_off."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$evalue['emp_code']."' ";	
	        		// echo $sql;
	        		// echo '<br />';
	        		//exit;	
	        		//$this->db->query($sql);
				}

				$is_half_day = $this->db->query("SELECT `half_day` FROM `oc_shift_weekoff` WHERE `shift_id` = '".$evalue['shift_id']."' AND `half_day` = '".$wvalue['compare_day']."' AND `unit_id` = '".$evalue['unit_id']."' ");
				if($is_half_day->num_rows > 0){
					$sql = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$half_day."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$evalue['emp_code']."' ";	
	        		// echo $sql;
	        		// echo '<br />';
	        		//exit;	
	        		//$this->db->query($sql);
				}
				//exit;
				//echo '<br />';
				//echo '<br />';
			}
			//exit;
			// echo '<br />';
			// echo '<br />';
			// echo '<br />';
			// echo '<br />';
		}
		echo 'Done';exit;
		

		
		$leave_temp_datas = $this->db->query("SELECT `emp_id`, `id` FROM `oc_leave_transaction_temp` WHERE `reporting_to` = '0' ")->rows;
		// echo '<pre>';
		// print_r($leave_temp_datas);
		// exit;
		foreach($leave_temp_datas as $lkeys => $lvalues){
			$emp_datas = $this->db->query("SELECT `reporting_to` FROM `oc_employee` WHERE `emp_code` = '".$lvalues['emp_id']."' ");
			if($emp_datas->num_rows > 0){
				$reporting_to = $emp_datas->row['reporting_to'];
				$update_sql = "UPDATE `oc_leave_transaction_temp` SET `reporting_to` = '".$reporting_to."' WHERE `id` = '".$lvalues['id']."' ";
				//$this->db->query($update_sql);
				//echo '<pre>';
				//print_r($update_sql);
				//exit;
			}
		}
		echo 'out';
		exit;


		// $leave_temp_datas = $this->db->query("SELECT * FROM `oc_leave_transaction_temp` GROUP BY `batch_id` ")->rows;
		// foreach($leave_temp_datas as $lkeys => $lvalues){
		// 	$leave_datas = $this->db->query("SELECT * FROM `oc_leave_transaction` WHERE `linked_batch_id` = '".$lvalues['batch_id']."' ");
		// 	if($leave_datas->num_rows == 0){
		// 		$update_sql = "UPDATE `oc_leave_transaction_temp` SET `a_status` = '0' WHERE `batch_id` = '".$lvalues['batch_id']."' ";
		// 		$this->db->query($update_sql);
		// 		echo '<pre>';
		// 		print_r($update_sql);
		// 	}
		// }
		// echo 'out';
		// exit;

		$employee_datas = $this->db->query("SELECT `emp_code`, `unit` FROM `oc_employee` WHERE `status` = '1' ")->rows;
		$months_array = array(
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5',
			'6' => '6',
			'7' => '7',
			'8' => '8',
			'9' => '9',
			'10' => '10',
			'11' => '11',
			'12' => '12',
		);
		// echo '<pre>';
		// print_r($months_array);
		// exit;
		foreach($employee_datas as $ekey => $evalue){
			$is_exist = $this->db->query("SELECT * FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' ");
			//echo '<pre>';
			//print_r($is_exist->num_rows);
			//exit;
			if($is_exist->num_rows == 0){
				$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE `unit` = '".$evalue['unit']."' AND `emp_code` <> '".$evalue['emp_code']."' LIMIT 1");
				if($emp_codes->num_rows > 0){
					$emp_codes = $emp_codes->row;
					$emp_code = $emp_codes['emp_code'];
				
					$exist_schedules = $this->db->query("SELECT * FROM `oc_shift_schedule` WHERE `emp_code` = '".$emp_code."' ");
					if($exist_schedules->num_rows > 0){
						$exist_schedule = $exist_schedules->rows;
						foreach($exist_schedule as $ekeys => $evalues){
							$sql = "INSERT INTO `oc_shift_schedule` SET `1` = '".$evalues['1']."', `2` = '".$evalues['2']."', `3` = '".$evalues['3']."', `4` = '".$evalues['4']."', `5` = '".$evalues['5']."', `6` = '".$evalues['6']."', `7` = '".$evalues['7']."', `8` = '".$evalues['8']."', `9` = '".$evalues['9']."', `10` = '".$evalues['10']."', `11` = '".$evalues['11']."', `12` = '".$evalues['12']."', `13` = '".$evalues['13']."', `14` = '".$evalues['14']."', `15` = '".$evalues['15']."', `16` = '".$evalues['16']."', `17` = '".$evalues['17']."', `18` = '".$evalues['18']."', `19` = '".$evalues['19']."', `20` = '".$evalues['20']."', `21` = '".$evalues['21']."', `22` = '".$evalues['22']."', `23` = '".$evalues['23']."', `24` = '".$evalues['24']."', `25` = '".$evalues['25']."', `26` = '".$evalues['26']."', `27` = '".$evalues['27']."', `28` = '".$evalues['28']."', `29` = '".$evalues['29']."', `30` = '".$evalues['30']."', `31` = '".$evalues['31']."', `emp_code` = '".$evalue['emp_code']."', `status` = '1', `month` = '".$evalues['month']."', `year` = '".$evalues['year']."', `unit` = '".$evalues['unit']."' "; 
							//$this->db->query($sql);
							echo $sql;
							echo '<br />';
							//exit;
						}
					} else {
						foreach ($months_array as $key => $value) {
							$insert1 = "INSERT INTO `oc_shift_schedule` SET 
								`emp_code` = '".$evalue['emp_code']."',
								`1` = 'S_1',
								`2` = 'S_1',
								`3` = 'S_1',
								`4` = 'S_1',
								`5` = 'S_1',
								`6` = 'S_1',
								`7` = 'S_1',
								`8` = 'S_1',
								`9` = 'S_1',
								`10` = 'S_1',
								`11` = 'S_1',
								`12` = 'S_1', 
								`13` = 'S_1', 
								`14` = 'S_1', 
								`15` = 'S_1', 
								`16` = 'S_1', 
								`17` = 'S_1', 
								`18` = 'S_1', 
								`19` = 'S_1', 
								`20` = 'S_1', 
								`21` = 'S_1', 
								`22` = 'S_1', 
								`23` = 'S_1', 
								`24` = 'S_1', 
								`25` = 'S_1', 
								`26` = 'S_1', 
								`27` = 'S_1', 
								`28` = 'S_1', 
								`29` = 'S_1', 
								`30` = 'S_1', 
								`31` = 'S_1', 
								`month` = '".$key."',
								`year` = '".date('Y')."',
								`status` = '1' ,
								`unit` = '".$evalue['unit']."' "; 
								// echo "<pre>";
								echo $insert1;
								echo '<br />';
								//$this->db->query($insert1);
						}						
					}
				} else {
					foreach ($months_array as $key => $value) {
						$insert1 = "INSERT INTO `oc_shift_schedule` SET 
							`emp_code` = '".$evalue['emp_code']."',
							`1` = 'S_1',
							`2` = 'S_1',
							`3` = 'S_1',
							`4` = 'S_1',
							`5` = 'S_1',
							`6` = 'S_1',
							`7` = 'S_1',
							`8` = 'S_1',
							`9` = 'S_1',
							`10` = 'S_1',
							`11` = 'S_1',
							`12` = 'S_1', 
							`13` = 'S_1', 
							`14` = 'S_1', 
							`15` = 'S_1', 
							`16` = 'S_1', 
							`17` = 'S_1', 
							`18` = 'S_1', 
							`19` = 'S_1', 
							`20` = 'S_1', 
							`21` = 'S_1', 
							`22` = 'S_1', 
							`23` = 'S_1', 
							`24` = 'S_1', 
							`25` = 'S_1', 
							`26` = 'S_1', 
							`27` = 'S_1', 
							`28` = 'S_1', 
							`29` = 'S_1', 
							`30` = 'S_1', 
							`31` = 'S_1', 
							`month` = '".$key."',
							`year` = '".date('Y')."',
							`status` = '1' ,
							`unit` = '".$evalue['unit']."' "; 
							// echo "<pre>";
							echo $insert1;
							echo '<br />';
							//$this->db->query($insert1);
					}
				}
			}
		}
		echo 'Done';exit;

		


		// $start_date = '2017-02-01';
		// $end_date = '2017-12-31';
		// $day = array();
		// $days = $this->GetDays($start_date, $end_date);
  //       foreach ($days as $dkey => $dvalue) {
  //       	$dates = explode('-', $dvalue);
  //       	$day[$dkey]['day'] = (int)$dates[2];
  //       	$day[$dkey]['month'] = $dates[1];
  //       	$day[$dkey]['year'] = $dates[0];
  //       	$day[$dkey]['date'] = $dvalue;
  //       }
  //       foreach($day as $dkey => $dvalue){
  //       	$sql = "UPDATE `oc_shift_schedule` SET `".$dvalue['day']."` = 'S_1' WHERE 1=1 ";	
  //       	$this->db->query($sql);
  //       	//echo $sql;exit;
  //       }
  //       echo 'Done';exit;
		// echo '<pre>';
		// print_r($day);
		// exit;

		
		





		

		//$sql = "SELECT `id`, `emp_id` FROM `oc_leave_transaction_temp` WHERE 1=1 GROUP BY emp_id";
		//$resultss = $this->db->query($sql)->rows;
		//foreach($resultss as $rkeys => $rvalues){
			//$groups = $this->db->query("SELECT `group` FROM `oc_employee` WHERE `emp_code` = '".$rvalues['emp_id']."' ");
			//if($groups->num_rows > 0){
				//$group = $groups->row['group'];
			//} else {
				//$group = '';
			//}
			//$sql = "UPDATE `oc_leave_transaction_temp` SET `group` = '".$this->db->escape($group)."' WHERE `emp_id` = '".$rvalues['emp_id']."' ";
			//echo $sql;exit;
			//$this->db->query($sql);
		//}
		//echo 'out';exit;

		$sql = "SELECT `id`, `emp_id` FROM `oc_leave_transaction` WHERE 1=1 GROUP BY emp_id";
		$resultss = $this->db->query($sql)->rows;
		foreach($resultss as $rkeys => $rvalues){
			$groups = $this->db->query("SELECT `group` FROM `oc_employee` WHERE `emp_code` = '".$rvalues['emp_id']."' ");
			if($groups->num_rows > 0){
				$group = $groups->row['group'];
			} else {
				$group = '';
			}
			$sql = "UPDATE `oc_leave_transaction` SET `group` = '".$this->db->escape($group)."' WHERE `emp_id` = '".$rvalues['emp_id']."' ";
			//echo $sql;exit;
			$this->db->query($sql);
		}
		echo 'out';exit;

		
		// $sql = "SELECT `department` FROM `oc_employee` GROUP BY `department` ";
		// $query = $this->db->query($sql)->rows;
		// foreach($query as $key => $value){
		// 	$dept = $value['department'];
		// 	$dept = str_replace(array('.', '&', ' ', '(', ')'), '_', $dept);
		// 	$dept = rtrim($dept, '_');
		// 	$user_name = $dept;
		// 	$salt = substr(md5(uniqid(rand(), true)), 0, 9);
		// 	$password = sha1($salt . sha1($salt . sha1($user_name)));
		// 	$sql = "INSERT INTO `oc_user` set `user_group_id` = '10', `username` = '".$this->db->escape($user_name)."', `password` = '".$this->db->escape($password)."', `salt` = '".$this->db->escape($salt)."', `firstname` = '".$this->db->escape($user_name)."', `dept_name` = '".$this->db->escape($value['department'])."', `status` = '1', `is_dept` = '1', `date_added` = '".date('Y-m-d')."' ";
		// 	$this->db->query($sql);
		// 	echo $sql;
		// 	echo '<br />';
		// 	//exit;
		// }
		// echo 'Done';exit;
		
		$sql = "SELECT * FROM `oc_leave_transaction` WHERE `linked_batch_id` = '0' GROUP BY `batch_id` ";
		$resultss = $this->db->query($sql)->rows;
		foreach($resultss as $rkeys => $rvalues){
			$sql = "SELECT * FROM `oc_leave_transaction` WHERE `batch_id` = '".$rvalues['batch_id']."' ";
			$results = $this->db->query($sql)->rows;
			
			$batch_id_sql = "SELECT `batch_id` FROM `oc_leave_transaction_temp` ORDER BY `batch_id` DESC LIMIT 1";
			$obatch_ids = $this->db->query($batch_id_sql);
			if($obatch_ids->num_rows == 0){
				$batch_id = 1;
			} else{
				$obatch_id = $obatch_ids->row['batch_id'];
				$batch_id = $obatch_id + 1;
			}

			foreach($results as $rkey => $rvalue){
				if($rvalue['p_status'] == '1'){
					$approval_1 = 1;
					$approval_2 = 1;
				} else {
					$approval_1 = 1;
					$approval_2 = 1;
				}

				$dept_name = $this->db->query("SELECT `department` FROM `oc_employee` WHERE `emp_code` = '".$rvalue['emp_id']."' ")->row['department'];

				$sql = "INSERT INTO `oc_leave_transaction_temp` set 
										`emp_id` = '".$rvalue['emp_id']."', 
										`leave_type` = '".$rvalue['leave_type']."', 
										`date` = '".$rvalue['date']."', 
										`leave_amount` = '".$rvalue['leave_amount']."', 
										`type` = '".$rvalue['type']."', 
										`days` = '".$rvalue['days']."', 
										`encash` = '".$rvalue['encash']."', 
										`p_status` = '".$rvalue['p_status']."', 
										`approval_1` = '".$this->db->escape($approval_1)."', 
										`approval_2` = '".$this->db->escape($approval_2)."', 
										`batch_id` = '".$this->db->escape($batch_id)."', 
										`a_status` = '".$rvalue['a_status']."',
										`unit` = '".$rvalue['unit']."',
										`year` = '".$rvalue['year']."',
										`dot` = '".$rvalue['dot']."',
										`date_cof` = '".$rvalue['date_cof']."',
										`dept_name` = '".$dept_name."' ";
				$this->db->query($sql);
				//$linked_batch_id = $this->db->getLastId();
				$update_sql = "UPDATE `oc_leave_transaction` SET `linked_batch_id` = '".$batch_id."' WHERE `id` = '".$rvalue['id']."' ";
				$this->db->query($update_sql);
				//echo $sql;
				//echo '<br />';
			}
			//exit;
		}
		echo 'Done';exit;


		$sql = "SELECT * FROM `oc_employee` WHERE 1=1";
		$results = $this->db->query($sql)->rows;
		foreach($results as $rkey => $rvalue){
			$emp_code = $rvalue['emp_code'];
			$user_name = $emp_code;
			$salt = substr(md5(uniqid(rand(), true)), 0, 9);
			$password = sha1($salt . sha1($salt . sha1($emp_code)));
			$sql = "UPDATE `oc_employee` set `user_group_id` = '11', `username` = '".$this->db->escape($emp_code)."', `password` = '".$this->db->escape($password)."', `salt` = '".$this->db->escape($salt)."' WHERE `employee_id` = '".$rvalue['employee_id']."' ";
			$this->db->query($sql);
			//echo $sql;exit;
		}
		echo 'Done';exit;



		//UPDATE `oc_transaction` SET `firsthalf_status` = '0', `secondhalf_status` = '0', `absent_status` = '1' WHERE `halfday_status` <> '0' AND `secondhalf_status` = 'HLD' AND `firsthalf_status` = 'HLD';

		
		// $trans_data = $this->db->query("SELECT * FROM `oc_transaction` WHERE `date` = '2016-08-05' AND `unit` = 'MUMBAI' ")->rows;
		// // echo '<pre>';
		// // print_r($trans_data);
		// // exit;
		// foreach($trans_data as $tkey => $tvalue){
		// 	$transaction_id = $tvalue['transaction_id'];
		// 	$this->db->query("DELETE FROM `oc_attendance` WHERE `transaction_id` = '".$transaction_id."' ");
		// 	$this->db->query("DELETE FROM `oc_transaction` WHERE `transaction_id` = '".$transaction_id."' ");
		// }
		// echo 'Done';exit;


		// $sql = "SELECT * FROM `transaction` WHERE `date` = '2016-02-23' AND `unit` = 'MUMBAI'";
		// $results = $this->db->query($sql)->rows;
		// foreach($results as $rkey => $rvalue){
		// 	$employee[] = array(
		// 		'EmpID' => $rvalue['emp_id'],
		// 		'EmpName' => $rvalue['emp_name'],
		// 		'ShiftIntime' => $rvalue['shift_intime'],
		// 		'ShiftOuttime' => $rvalue['shift_outtime'],
		// 		'ActInDate' => $rvalue['date'],
		// 		'ActInTime' => $rvalue['act_intime'],
		// 		'ActOutDate' => $rvalue['date_out'],
		// 		'ActOutTime' => $rvalue['act_outtime'],
		// 	);
		// }
		// echo '<pre>';
		// print_r($employee);
		// exit;


		$leave = 'OD';
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '".$leave."' AND `secondhalf_status` = '".$leave."' AND `absent_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '1 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '".$leave."' AND `secondhalf_status` = '".$leave."' AND `present_status` = '1' ";
		if($leave == 'OD'){
			$sql .= "AND `on_duty` = '0' ";
		}
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			//UPDATE `oc_transaction` SET `present_status` = '0' WHERE `firsthalf_status` = 'PL' AND `secondhalf_status` = 'PL' AND `present_status` = '1';
			//UPDATE `oc_transaction` SET `present_status` = '0' WHERE `firsthalf_status` = 'COF' AND `secondhalf_status` = 'COF' AND `present_status` = '1'
			echo '2 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '".$leave."' AND `secondhalf_status` = '".$leave."' AND `leave_status` = '0' ";
		if($leave == 'COF'){
			$sql .= "AND `compli_status` = '0' ";
		} elseif($leave == 'OD'){
			$sql .= "AND `on_duty` = '0' ";
		}
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '3 problem';
			echo '<br />';
		}

		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '".$leave."' AND `secondhalf_status` = '1' AND `absent_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '4 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '".$leave."' AND `secondhalf_status` = '1' AND `absent_status` = '0.5' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '5 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '".$leave."' AND `secondhalf_status` = '1' AND `present_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '6 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '".$leave."' AND `secondhalf_status` = '1' AND `present_status` = '0' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '7 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '".$leave."' AND `secondhalf_status` = '1' AND `leave_status` = '0' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '8 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '".$leave."' AND `secondhalf_status` = '1' AND `leave_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '9 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '1' AND `secondhalf_status` = '".$leave."' AND `absent_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '10 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '1' AND `secondhalf_status` = '".$leave."' AND `absent_status` = '0.5' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '11 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '1' AND `secondhalf_status` = '".$leave."' AND `present_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '12 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '1' AND `secondhalf_status` = '".$leave."' AND `present_status` = '0' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '13 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '1' AND `secondhalf_status` = '".$leave."' AND `leave_status` = '0' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '14 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '1' AND `secondhalf_status` = '".$leave."' AND `leave_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '15 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '".$leave."' AND `secondhalf_status` = '0' AND `absent_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '16 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '".$leave."' AND `secondhalf_status` = '0' AND `absent_status` = '0' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '17 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '".$leave."' AND `secondhalf_status` = '0' AND `present_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '18 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '".$leave."' AND `secondhalf_status` = '0' AND `present_status` = '0.5' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '19 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '".$leave."' AND `secondhalf_status` = '0' AND `leave_status` = '0' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '20 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '".$leave."' AND `secondhalf_status` = '0' AND `leave_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '21 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '0' AND `secondhalf_status` = '".$leave."' AND `absent_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '22 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '0' AND `secondhalf_status` = '".$leave."' AND `absent_status` = '0' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '23 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '0' AND `secondhalf_status` = '".$leave."' AND `present_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '24 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '0' AND `secondhalf_status` = '".$leave."' AND `present_status` = '0.5' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '25 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '0' AND `secondhalf_status` = '".$leave."' AND `leave_status` = '0' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '26 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '0' AND `secondhalf_status` = '".$leave."' AND `leave_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '27 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '".$leave."' AND `secondhalf_status` = 'HD' AND `absent_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '28 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '".$leave."' AND `secondhalf_status` = 'HD' AND `absent_status` = '0.5' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '29 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '".$leave."' AND `secondhalf_status` = 'HD' AND `present_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '30 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '".$leave."' AND `secondhalf_status` = 'HD' AND `present_status` = '0.5' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '31 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '".$leave."' AND `secondhalf_status` = 'HD' AND `leave_status` = '0' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '32 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '".$leave."' AND `secondhalf_status` = 'HD' AND `leave_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '33 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = 'HD' AND `secondhalf_status` = '".$leave."' AND `absent_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '34 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = 'HD' AND `secondhalf_status` = '".$leave."' AND `absent_status` = '0.5' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '35 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = 'HD' AND `secondhalf_status` = '".$leave."' AND `present_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '36 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = 'HD' AND `secondhalf_status` = '".$leave."' AND `present_status` = '0.5' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '37 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = 'HD' AND `secondhalf_status` = '".$leave."' AND `leave_status` = '0' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '38 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = 'HD' AND `secondhalf_status` = '".$leave."' AND `leave_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '39 problem';
			echo '<br />';
		}

		
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = 'HD' AND `secondhalf_status` = '1' AND `absent_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '41 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = 'HD' AND `secondhalf_status` = '1' AND `absent_status` = '0.5' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '42 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = 'HD' AND `secondhalf_status` = '1' AND `present_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '43 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = 'HD' AND `secondhalf_status` = '1' AND `absent_status` = '0.5' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '44 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = 'HD' AND `secondhalf_status` = '1' AND `leave_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '45 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = 'HD' AND `secondhalf_status` = '1' AND `leave_status` = '0.5' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '46 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '1' AND `secondhalf_status` = 'HD' AND `absent_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '47 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '1' AND `secondhalf_status` = 'HD' AND `absent_status` = '0.5' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '48 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '1' AND `secondhalf_status` = 'HD' AND `present_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '49 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '1' AND `secondhalf_status` = 'HD' AND `absent_status` = '0.5' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '50 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '1' AND `secondhalf_status` = 'HD' AND `leave_status` = '1' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '51 problem';
			echo '<br />';
		}
		$sql = "SELECT * FROM `oc_transaction` WHERE `firsthalf_status` = '1' AND `secondhalf_status` = 'HD' AND `leave_status` = '0.5' ";
		$is_exist = $this->db->query($sql);
		if($is_exist->num_rows > 0){
			echo '52 problem';
			echo '<br />';
		}
		
		echo 'Done';exit;
		// $rvaluess['punch_date'] = '2015-11-15';
		// $shift_intime = '09:00:00';
		// $shift_outtime = '05:30:00';

		// $act_intime = '09:15:00';
		// $act_outtime = '16:00:00';
		
		// $first_half = 0;
		// $second_half = 0;
		// $abnormal_status = 0;
		// $late_time = '00:00:00';
		// if($act_intime != '00:00:00'){
		// 	$start_date = new DateTime($rvaluess['punch_date'].' '.$shift_intime);
		// 	$since_start = $start_date->diff(new DateTime($rvaluess['punch_date'].' '.$act_intime));
		// 	$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
		// 	if($since_start->invert == 1 && $since_start->h >= 1){ //abnormal status condition
		// 		$abnormal_status = 1;
		// 	} else {
		// 		if($since_start->h >= 3 && $since_start->invert == 0){ //if shiftintime is greater than act intme by 3h condition
		// 			$first_half = 0;
		// 			if($act_outtime != '00:00:00'){ //to get second half actintime and actouttime difference should be greater then or equal to 3 hrs
		// 				$start_date = new DateTime($rvaluess['punch_date'].' '.$act_intime);
		// 				$since_start = $start_date->diff(new DateTime($rvaluess['punch_date'].' '.$act_outtime));
		// 				if($since_start->h >= 3 && $since_start->invert == 0){
		// 					$second_half = 1;
		// 				} else {
		// 					$second_half = 0;
		// 				}
		// 			} else {
		// 				$second_half = 0;
		// 			}
		// 		} else {
		// 			if($act_outtime != '00:00:00'){
		// 				$start_date = new DateTime($rvaluess['punch_date'].' '.$shift_intime);
		// 				$since_start = $start_date->diff(new DateTime($rvaluess['punch_date'].' '.$act_outtime));
		// 				if($since_start->h >= 6 && $since_start->invert == 0){
		// 					$first_half = 1;
		// 					$second_half = 1;
		// 				} elseif($since_start->h >= 3 && $since_start->invert == 0){
		// 					$first_half = 1;
		// 					$second_half = 0;
		// 				} else {
		// 					$first_half = 0;
		// 					$second_half = 0;
		// 				}
		// 			} else {
		// 				$first_half = 0;
		// 				$second_half = 0;
		// 			}
		// 		}
		// 	}
		// } else {
		// 	$first_half = 0;
		// 	$second_half = 0;
		// }

		// echo '<pre>';
		// print_r($abnormal_status);
		// echo '<pre>';
		// print_r($first_half);
		// echo '<pre>';
		// print_r($second_half);
		// exit;
		
		$this->load->model('transaction/transaction');
		$this->load->model('common/report');

		$days = $this->GetDays('2015-05-01', '2015-05-31');//change the month
		$day = array();
		foreach ($days as $dkey => $dvalue) {
        	$dates = explode('-', $dvalue);
        	$day[$dkey]['day'] = $dates[2];
        	$day[$dkey]['date'] = $dvalue;
        }
        // echo '<pre>';
        // print_r($day);
        // exit;
        $results = $this->model_report_common_report->getemployees($data);
        foreach($day as $dkey => $dvalue){
	        foreach ($results as $rkey => $rvalue) {
				$rvaluess = $this->model_transaction_transaction->getrawattendance_group_date_custom($rvalue['emp_code'], $dvalue['date']);
				if($rvaluess) {
					$emp_data = $this->model_transaction_transaction->getempdata($rvaluess['emp_id']);
					if(isset($emp_data['name']) && $emp_data['name'] != '' && $emp_data['shift_type'] == 'F'){
						$emp_name = $emp_data['name'];
						$department = $emp_data['department'];
						$unit = $emp_data['unit'];
						$group = $emp_data['group'];

						$day_date = date('j', strtotime($dvalue['date']));
						$month = date('n', strtotime($dvalue['date']));
						$year = date('Y', strtotime($dvalue['date']));

						$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvaluess['emp_id']."' ";
						$shift_schedule = $this->db->query($update3)->row;
						$schedule_raw = explode('_', $shift_schedule[$day_date]);
						
						if($rvaluess['holiday'] != 0){
							$act_intime = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
							$act_outtime = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date']);
							if($act_intime == $act_outtime){
								$act_outtime = '00:00:00';
							}

							$day = date('j', strtotime($rvaluess['punch_date']));
							$month = date('n', strtotime($rvaluess['punch_date']));
							$year = date('Y', strtotime($rvaluess['punch_date']));

							$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `weekly_off` = '0', `holiday_id` = '1', `leave` = '0', `present_status` = '1', `absent_status` = '0' `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$rvaluess['punch_date']."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `manual_status` = '".$rvaluess['manual_status']."' ";
							$this->db->query($sql);
							$sql1 = "UPDATE `oc_shift_schedule` SET `".$day_date."` = 'H_1' WHERE emp_code = '".$rvaluess['emp_id']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
							$this->db->query($sql1);
						} elseif($rvaluess['weekly_off'] != 0){

							$act_intime = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
							$act_outtime = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date']);
							if($act_intime == $act_outtime){
								$act_outtime = '00:00:00';
							}

							$day = date('j', strtotime($rvaluess['punch_date']));
							$month = date('n', strtotime($rvaluess['punch_date']));
							$year = date('Y', strtotime($rvaluess['punch_date']));

							$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `holiday_id` = '0', `weekly_off` = '1', `leave` = '0', `present_status` = '1', `absent_status` = '0', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$rvaluess['punch_date']."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `manual_status` = '".$rvaluess['manual_status']."' ";
							$this->db->query($sql);

							$sql1 = "UPDATE `oc_shift_schedule` SET `".$day_date."` = 'W_1' WHERE emp_code = '".$rvaluess['emp_id']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
							$this->db->query($sql1);
						} elseif($rvaluess['leave'] != 0){

							$act_intime = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
							$act_outtime = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date']);
							if($act_intime == $act_outtime){
								$act_outtime = '00:00:00';
							}

							$day = date('j', strtotime($rvaluess['punch_date']));
							$month = date('n', strtotime($rvaluess['punch_date']));
							$year = date('Y', strtotime($rvaluess['punch_date']));

							$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `holiday_id` = '0', `weekly_off` = '0', `leave` = '1', `present_status` = '1', `absent_status` = '0', `firsthalf_status` = 'L', `secondhalf_status` = 'L', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$rvaluess['punch_date']."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `manual_status` = '".$rvaluess['manual_status']."' ";
							$this->db->query($sql);

							$sql1 = "UPDATE `oc_shift_schedule` SET `".$day_date."` = 'L_1' WHERE emp_code = '".$rvaluess['emp_id']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
							$this->db->query($sql1);
						} else {
							//if($schedule_raw[0] == 'S'){
							$update4 = "SELECT  `1` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvaluess['emp_id']."' ";
							$shift_schedule = $this->db->query($update4)->row;
							$schedule_raw = explode('_', $shift_schedule['1']);
							$shift_id = $schedule_raw[1];
							$shift_data = $this->model_transaction_transaction->getshiftdata($shift_id);
							if(isset($shift_data['shift_id'])){
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];

								$act_intime = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
								$act_outtime = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date']);
								
								if($act_intime == $act_outtime){
									$act_outtime = '00:00:00';
								}
								
								$first_half = 0;
								$second_half = 0;
								$abnormal_status = 0;
								$late_time = '00:00:00';
								if($act_intime != '00:00:00'){
									$start_date = new DateTime($rvaluess['punch_date'].' '.$shift_intime);
									$since_start = $start_date->diff(new DateTime($rvaluess['punch_date'].' '.$act_intime));
									$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
									if($since_start->invert == 1 && $since_start->h >= 1){ //abnormal status condition
										$abnormal_status = 1;
									} else {
										if($since_start->h >= 3 && $since_start->invert == 0){ //if shiftintime is greater than act intme by 3h condition
											$first_half = 0;
											if($act_outtime != '00:00:00'){ //to get second half actintime and actouttime difference should be greater then or equal to 3 hrs
												$start_date = new DateTime($rvaluess['punch_date'].' '.$act_intime);
												$since_start = $start_date->diff(new DateTime($rvaluess['punch_date'].' '.$act_outtime));
												if($since_start->h >= 3 && $since_start->invert == 0){
													$second_half = 1;
												} else {
													$second_half = 0;
												}
											} else {
												$second_half = 0;
											}
										} else {
											if($act_outtime != '00:00:00'){
												$start_date = new DateTime($rvaluess['punch_date'].' '.$shift_intime);
												$since_start = $start_date->diff(new DateTime($rvaluess['punch_date'].' '.$act_outtime));
												if($since_start->h >= 6 && $since_start->invert == 0){
													$first_half = 1;
													$second_half = 1;
												} elseif($since_start->h >= 3 && $since_start->invert == 0){
													$first_half = 1;
													$second_half = 0;
												} else {
													$first_half = 0;
													$second_half = 0;
												}
											} else {
												$first_half = 0;
												$second_half = 0;
											}
										}
									}
								} else {
									$first_half = 0;
									$second_half = 0;
								}

								if($abnormal_status == 0){ //if abnormal status is zero calculate further 
									$early_time = '00:00:00';
									if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
										$start_date = new DateTime($rvaluess['punch_date'].' '.$shift_outtime);
										$since_start = $start_date->diff(new DateTime($rvaluess['punch_date'].' '.$act_outtime));
										$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
										if($since_start->invert == 0){
											$early_time = '00:00:00';
										}
									}					
									
									$working_time = '00:00:00';
									if($act_outtime != '00:00:00'){//for getting working time
										$start_date = new DateTime($rvaluess['punch_date'].' '.$act_intime);
										$since_start = $start_date->diff(new DateTime($rvaluess['punch_date'].' '.$act_outtime));
										$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
									}
								} else {
									$first_half = 0;
									$second_half = 0;
								} 

								// if($first_half == 1 && $second_half == 1){
								// 	$present_status = 1;
								// 	$absent_status = 0;
								// } elseif($first_half == 1 && $second_half == 0){
								// 	$present_status = 0.5;
								// 	$absent_status = 0.5;
								// } elseif($first_half == 0 && $second_half == 1){
								// 	$present_status = 0.5;
								// 	$absent_status = 0.5;
								// } else {
								// 	$present_status = 0;
								// 	$absent_status = 1;
								// }

								if($rvaluess['present'] == 1){
									$present_status = 1;
									$absent_status = 0;
								} elseif($rvaluess['absent'] == 1){
									$present_status = 0;
									$absent_status = 1;
								} else {
									$present_status = 0;
									$absent_status = 0;
								}

								$day = date('j', strtotime($rvaluess['punch_date']));
								$month = date('n', strtotime($rvaluess['punch_date']));
								$year = date('Y', strtotime($rvaluess['punch_date']));

								$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$rvaluess['punch_date']."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `manual_status` = '".$rvaluess['manual_status']."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0' ";
								//echo $sql;exit;
								$this->db->query($sql);
							}
						}
					}
				} else {
					
				}
			}
		}
		echo 'Done';exit;
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function array_sort($array, $on, $order=SORT_ASC){

	    $new_array = array();
	    $sortable_array = array();

	    if (count($array) > 0) {
	        foreach ($array as $k => $v) {
	            if (is_array($v)) {
	                foreach ($v as $k2 => $v2) {
	                    if ($k2 == $on) {
	                        $sortable_array[$k] = $v2;
	                    }
	                }
	            } else {
	                $sortable_array[$k] = $v;
	            }
	        }

	        switch ($order) {
	            case SORT_ASC:
	                asort($sortable_array);
	                break;
	            case SORT_DESC:
	                arsort($sortable_array);
	                break;
	        }

	        foreach ($sortable_array as $k => $v) {
	            $new_array[$k] = $array[$k];
	        }
	    }

	    return $new_array;
	}
}
?>