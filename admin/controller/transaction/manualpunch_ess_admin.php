<?php    
class ControllerTransactionManualpunchessadmin extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('transaction/manualpunch_ess_dept');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		$this->getList();
	}

	public function approve() {
		$this->language->load('transaction/manualpunch_ess_dept');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		if (isset($this->request->post['selected'])) {
			if($this->request->get['type'] == 1){
				foreach ($this->request->post['selected'] as $id) {
					$manual_data_sql = "SELECT * FROM `oc_manual_punch` WHERE `id` = '".$id."' ";
					$manual_data = $this->db->query($manual_data_sql)->row;
					if($manual_data['approval_1'] == '0' || $manual_data['approval_1'] == '2'){
						$trans_data_sql = "SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$manual_data['emp_id']."' AND `date` = '".$manual_data['date']."' ";
						$trans_datas = $this->db->query($trans_data_sql);
						if($trans_datas->num_rows > 0){
							$manual_data['transaction_id'] = $trans_datas->row['transaction_id'];
						}
						if($manual_data['transaction_id'] == 0){
							$sql = "INSERT INTO `oc_transaction` SET 
								`emp_id` = '".$manual_data['emp_id']."', 
								`emp_name` = '".$manual_data['emp_name']."', 
								`shift_id` = '".$manual_data['shift_id']."',
								`shift_code` = '".$manual_data['shift_code']."',
								`shift_intime` = '".$manual_data['shift_intime']."',
								`shift_outtime` = '".$manual_data['shift_outtime']."',
								`shift_outtime_flexi` = '".$manual_data['shift_outtime_flexi']."',
								`shift_allowance` = '".$manual_data['shift_allowance']."',
								`act_shift_id` = '".$manual_data['act_shift_id']."',
								`act_shift_code` = '".$manual_data['act_shift_code']."',
								`act_shift_intime` = '".$manual_data['act_shift_intime']."',
								`act_shift_outtime` = '".$manual_data['act_shift_outtime']."',
								`act_shift_outtime_flexi` = '".$manual_data['act_shift_outtime_flexi']."',
								`act_shift_allowance` = '".$manual_data['act_shift_allowance']."',
								`act_intime` = '".$manual_data['act_intime']."',
								`act_outtime` = '".$manual_data['act_outtime']."',
								`weekly_off` = '".$manual_data['weekly_off']."',
								`holiday_id` = '".$manual_data['holiday_id']."',
								`late_time` = '".$manual_data['late_time']."',
								`early_time` = '".$manual_data['early_time']."',
								`working_time` = '".$manual_data['working_time']."',
								`over_time` = '".$manual_data['over_time']."',
								`over_time_amount` = '".$manual_data['over_time_amount']."',
								`over_time_esic` = '".$manual_data['over_time_esic']."',
								`after_shift` = '".$manual_data['after_shift']."',
								`day` = '".$manual_data['day']."',
								`month` = '".$manual_data['month']."',
								`year` = '".$manual_data['year']."',
								`date` = '".$manual_data['date']."',
								`department` = '".$manual_data['department']."',
								`department_id` = '".$manual_data['department_id']."',
								`unit` = '".$manual_data['unit']."',
								`unit_id` = '".$manual_data['unit_id']."',
								`firsthalf_status` = '".$manual_data['firsthalf_status']."',
								`secondhalf_status` = '".$manual_data['secondhalf_status']."',
								`manual_status` = '".$manual_data['manual_status']."',
								`leave_status` = '".$manual_data['leave_status']."',
								`present_status` = '".$manual_data['present_status']."',
								`absent_status` = '".$manual_data['absent_status']."',
								`date_out` = '".$manual_data['date_out']."',
								`late_mark` = '".$manual_data['late_mark']."',
								`early_mark` = '".$manual_data['early_mark']."',
								`shift_change` = '".$manual_data['shift_change']."',
								`shift_free_change` = '".$manual_data['shift_free_change']."',
								`act_intime_change` = '".$manual_data['act_intime_change']."',
								`act_outtime_change` = '".$manual_data['act_outtime_change']."',
								`tour_id` = '".$manual_data['tour_id']."' ";
								//echo $sql;
								//echo '<br />';
								$this->db->query($sql);
								$this->log->write($sql);
								$transaction_id = $this->db->getLastId();
								$this->db->query("UPDATE `oc_manual_punch` SET `transaction_id` = '".$transaction_id."' WHERE `id` = '".$manual_data['id']."' ");
						} else {
							$sql = "UPDATE `oc_transaction` SET 
								`emp_id` = '".$manual_data['emp_id']."', 
								`emp_name` = '".$manual_data['emp_name']."', 
								`shift_id` = '".$manual_data['shift_id']."',
								`shift_code` = '".$manual_data['shift_code']."',
								`shift_intime` = '".$manual_data['shift_intime']."',
								`shift_outtime` = '".$manual_data['shift_outtime']."',
								`shift_outtime_flexi` = '".$manual_data['shift_outtime_flexi']."',
								`shift_allowance` = '".$manual_data['shift_allowance']."',
								`act_shift_id` = '".$manual_data['act_shift_id']."',
								`act_shift_code` = '".$manual_data['act_shift_code']."',
								`act_shift_intime` = '".$manual_data['act_shift_intime']."',
								`act_shift_outtime` = '".$manual_data['act_shift_outtime']."',
								`act_shift_outtime_flexi` = '".$manual_data['act_shift_outtime_flexi']."',
								`act_shift_allowance` = '".$manual_data['act_shift_allowance']."',
								`act_intime` = '".$manual_data['act_intime']."',
								`act_outtime` = '".$manual_data['act_outtime']."',
								`weekly_off` = '".$manual_data['weekly_off']."',
								`holiday_id` = '".$manual_data['holiday_id']."',
								`late_time` = '".$manual_data['late_time']."',
								`early_time` = '".$manual_data['early_time']."',
								`working_time` = '".$manual_data['working_time']."',
								`over_time` = '".$manual_data['over_time']."',
								`over_time_amount` = '".$manual_data['over_time_amount']."',
								`over_time_esic` = '".$manual_data['over_time_esic']."',
								`after_shift` = '".$manual_data['after_shift']."',
								`day` = '".$manual_data['day']."',
								`month` = '".$manual_data['month']."',
								`year` = '".$manual_data['year']."',
								`date` = '".$manual_data['date']."',
								`department` = '".$manual_data['department']."',
								`department_id` = '".$manual_data['department_id']."',
								`unit` = '".$manual_data['unit']."',
								`unit_id` = '".$manual_data['unit_id']."',
								`firsthalf_status` = '".$manual_data['firsthalf_status']."',
								`secondhalf_status` = '".$manual_data['secondhalf_status']."',
								`manual_status` = '".$manual_data['manual_status']."',
								`leave_status` = '".$manual_data['leave_status']."',
								`present_status` = '".$manual_data['present_status']."',
								`absent_status` = '".$manual_data['absent_status']."',
								`date_out` = '".$manual_data['date_out']."',
								`late_mark` = '".$manual_data['late_mark']."',
								`early_mark` = '".$manual_data['early_mark']."',
								`shift_change` = '".$manual_data['shift_change']."',
								`shift_free_change` = '".$manual_data['shift_free_change']."',
								`act_intime_change` = '".$manual_data['act_intime_change']."',
								`act_outtime_change` = '".$manual_data['act_outtime_change']."',
								`tour_id` = '".$manual_data['tour_id']."' 
								WHERE `transaction_id` = '".$manual_data['transaction_id']."' ";
								//echo $sql;
								//echo '<br />';
								$this->db->query($sql);
								$this->log->write($sql);
								$this->db->query("UPDATE `oc_manual_punch` SET `transaction_id` = '".$manual_data['transaction_id']."' WHERE `id` = '".$manual_data['id']."' ");
						}
					}
					date_default_timezone_set("Asia/Kolkata");
					//if(isset($this->session->data['is_super']) || isset($this->session->data['is_super1'])){
						//$approve_sql = "UPDATE `oc_leave_transaction_temp` SET `a_status` = '1', `approval_2` = '1', `approval_date_2` = '".date('Y-m-d h:i:s')."', `reject_date_2` = '0000-00-00 00:00:00' WHERE `batch_id` = '".$batch_id."' ";
					//} elseif(isset($this->session->data['is_dept'])){
						$approve_sql = "UPDATE `oc_manual_punch` SET `p_status` = '1', `approval_1` = '1', `approval_date` = '".date('Y-m-d h:i:s')."', `approval_1_by` = '".$this->session->data['emp_code']."', `reject_date` = '0000-00-00 00:00:00' WHERE `id` = '".$id."' ";
					//}
					$this->db->query($approve_sql);
					$this->log->write($approve_sql);
				}
				$this->session->data['success'] = 'PX Record Entry Approved Successfully';
			} else {
				foreach ($this->request->post['selected'] as $batch_id) {
					$manual_data_sql = "SELECT * FROM `oc_manual_punch` WHERE `batch_id` = '".$batch_id."' ";
					$manual_datas = $this->db->query($manual_data_sql)->rows;
					foreach($manual_datas as $mkey => $manual_data){
						if($manual_data['approval_1'] == '0' || $manual_data['approval_1'] == '2'){
							$trans_data_sql = "SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$manual_data['emp_id']."' AND `date` = '".$manual_data['date']."' ";
							$trans_datas = $this->db->query($trans_data_sql);
							if($trans_datas->num_rows > 0){
								$manual_data['transaction_id'] = $trans_datas->row['transaction_id'];
							}
							if($manual_data['transaction_id'] == 0){
								$sql = "INSERT INTO `oc_transaction` SET 
									`emp_id` = '".$manual_data['emp_id']."', 
									`emp_name` = '".$manual_data['emp_name']."', 
									`shift_id` = '".$manual_data['shift_id']."',
									`shift_code` = '".$manual_data['shift_code']."',
									`shift_intime` = '".$manual_data['shift_intime']."',
									`shift_outtime` = '".$manual_data['shift_outtime']."',
									`shift_outtime_flexi` = '".$manual_data['shift_outtime_flexi']."',
									`shift_allowance` = '".$manual_data['shift_allowance']."',
									`act_shift_id` = '".$manual_data['act_shift_id']."',
									`act_shift_code` = '".$manual_data['act_shift_code']."',
									`act_shift_intime` = '".$manual_data['act_shift_intime']."',
									`act_shift_outtime` = '".$manual_data['act_shift_outtime']."',
									`act_shift_outtime_flexi` = '".$manual_data['act_shift_outtime_flexi']."',
									`act_shift_allowance` = '".$manual_data['act_shift_allowance']."',
									`act_intime` = '".$manual_data['act_intime']."',
									`act_outtime` = '".$manual_data['act_outtime']."',
									`weekly_off` = '".$manual_data['weekly_off']."',
									`holiday_id` = '".$manual_data['holiday_id']."',
									`late_time` = '".$manual_data['late_time']."',
									`early_time` = '".$manual_data['early_time']."',
									`working_time` = '".$manual_data['working_time']."',
									`over_time` = '".$manual_data['over_time']."',
									`over_time_amount` = '".$manual_data['over_time_amount']."',
									`over_time_esic` = '".$manual_data['over_time_esic']."',
									`after_shift` = '".$manual_data['after_shift']."',
									`day` = '".$manual_data['day']."',
									`month` = '".$manual_data['month']."',
									`year` = '".$manual_data['year']."',
									`date` = '".$manual_data['date']."',
									`department` = '".$manual_data['department']."',
									`department_id` = '".$manual_data['department_id']."',
									`unit` = '".$manual_data['unit']."',
									`unit_id` = '".$manual_data['unit_id']."',
									`firsthalf_status` = '".$manual_data['firsthalf_status']."',
									`secondhalf_status` = '".$manual_data['secondhalf_status']."',
									`manual_status` = '".$manual_data['manual_status']."',
									`leave_status` = '".$manual_data['leave_status']."',
									`present_status` = '".$manual_data['present_status']."',
									`absent_status` = '".$manual_data['absent_status']."',
									`date_out` = '".$manual_data['date_out']."',
									`late_mark` = '".$manual_data['late_mark']."',
									`early_mark` = '".$manual_data['early_mark']."',
									`shift_change` = '".$manual_data['shift_change']."',
									`shift_free_change` = '".$manual_data['shift_free_change']."',
									`act_intime_change` = '".$manual_data['act_intime_change']."',
									`act_outtime_change` = '".$manual_data['act_outtime_change']."',
									`tour_id` = '".$manual_data['tour_id']."' ";
									//echo $sql;
									//echo '<br />';
								$this->db->query($sql);
								$this->log->write($sql);
								$transaction_id = $this->db->getLastId();
								$this->db->query("UPDATE `oc_manual_punch` SET `transaction_id` = '".$transaction_id."' WHERE `id` = '".$manual_data['id']."' ");
							} else {
								$sql = "UPDATE `oc_transaction` SET 
									`emp_id` = '".$manual_data['emp_id']."', 
									`emp_name` = '".$manual_data['emp_name']."', 
									`shift_id` = '".$manual_data['shift_id']."',
									`shift_code` = '".$manual_data['shift_code']."',
									`shift_intime` = '".$manual_data['shift_intime']."',
									`shift_outtime` = '".$manual_data['shift_outtime']."',
									`shift_outtime_flexi` = '".$manual_data['shift_outtime_flexi']."',
									`shift_allowance` = '".$manual_data['shift_allowance']."',
									`act_shift_id` = '".$manual_data['act_shift_id']."',
									`act_shift_code` = '".$manual_data['act_shift_code']."',
									`act_shift_intime` = '".$manual_data['act_shift_intime']."',
									`act_shift_outtime` = '".$manual_data['act_shift_outtime']."',
									`act_shift_outtime_flexi` = '".$manual_data['act_shift_outtime_flexi']."',
									`act_shift_allowance` = '".$manual_data['act_shift_allowance']."',
									`act_intime` = '".$manual_data['act_intime']."',
									`act_outtime` = '".$manual_data['act_outtime']."',
									`weekly_off` = '".$manual_data['weekly_off']."',
									`holiday_id` = '".$manual_data['holiday_id']."',
									`late_time` = '".$manual_data['late_time']."',
									`early_time` = '".$manual_data['early_time']."',
									`working_time` = '".$manual_data['working_time']."',
									`over_time` = '".$manual_data['over_time']."',
									`over_time_amount` = '".$manual_data['over_time_amount']."',
									`over_time_esic` = '".$manual_data['over_time_esic']."',
									`after_shift` = '".$manual_data['after_shift']."',
									`day` = '".$manual_data['day']."',
									`month` = '".$manual_data['month']."',
									`year` = '".$manual_data['year']."',
									`date` = '".$manual_data['date']."',
									`department` = '".$manual_data['department']."',
									`department_id` = '".$manual_data['department_id']."',
									`unit` = '".$manual_data['unit']."',
									`unit_id` = '".$manual_data['unit_id']."',
									`firsthalf_status` = '".$manual_data['firsthalf_status']."',
									`secondhalf_status` = '".$manual_data['secondhalf_status']."',
									`manual_status` = '".$manual_data['manual_status']."',
									`leave_status` = '".$manual_data['leave_status']."',
									`present_status` = '".$manual_data['present_status']."',
									`absent_status` = '".$manual_data['absent_status']."',
									`date_out` = '".$manual_data['date_out']."',
									`late_mark` = '".$manual_data['late_mark']."',
									`early_mark` = '".$manual_data['early_mark']."',
									`shift_change` = '".$manual_data['shift_change']."',
									`shift_free_change` = '".$manual_data['shift_free_change']."',
									`act_intime_change` = '".$manual_data['act_intime_change']."',
									`act_outtime_change` = '".$manual_data['act_outtime_change']."',
									`tour_id` = '".$manual_data['tour_id']."' 
									WHERE `transaction_id` = '".$manual_data['transaction_id']."' ";
									//echo $sql;
									//echo '<br />';
								$this->db->query($sql);
								$this->log->write($sql);
								$this->db->query("UPDATE `oc_manual_punch` SET `transaction_id` = '".$manual_data['transaction_id']."' WHERE `id` = '".$manual_data['id']."' ");
							}
						}
					}
					date_default_timezone_set("Asia/Kolkata");
					//if(isset($this->session->data['is_super']) || isset($this->session->data['is_super1'])){
						//$approve_sql = "UPDATE `oc_leave_transaction_temp` SET `a_status` = '1', `approval_2` = '1', `approval_date_2` = '".date('Y-m-d h:i:s')."', `reject_date_2` = '0000-00-00 00:00:00' WHERE `batch_id` = '".$batch_id."' ";
					//} elseif(isset($this->session->data['is_dept'])){
						$approve_sql = "UPDATE `oc_manual_punch` SET `p_status` = '1', `approval_1` = '1', `approval_date` = '".date('Y-m-d h:i:s')."', `approval_1_by` = '".$this->session->data['emp_code']."', `reject_date` = '0000-00-00 00:00:00' WHERE `batch_id` = '".$batch_id."' ";
					//}
					$this->db->query($approve_sql);
					$this->log->write($approve_sql);
				}
				$this->session->data['success'] = 'Tour Entry Approved Successfully';
			}


			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['filter_date_to'])) {
				$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
			}

			if (isset($this->request->get['filter_leave_from'])) {
				$url .= '&filter_leave_from=' . $this->request->get['filter_leave_from'];
			}

			if (isset($this->request->get['filter_leave_to'])) {
				$url .= '&filter_leave_to=' . $this->request->get['filter_leave_to'];
			}

			if (isset($this->request->get['filter_dept'])) {
				$url .= '&filter_dept=' . $this->request->get['filter_dept'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_proc'])) {
				$url .= '&filter_proc=' . $this->request->get['filter_proc'];
			}

			if (isset($this->request->get['filter_px_record_type'])) {
				$url .= '&filter_px_record_type=' . $this->request->get['filter_px_record_type'];
			}

			if (isset($this->request->get['filter_type'])) {
				$url .= '&filter_type=' . $this->request->get['filter_type'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('transaction/manualpunch_ess_admin', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		} elseif(isset($this->request->get['id'])){
			$id = $this->request->get['id'];
			$manual_data_sql = "SELECT * FROM `oc_manual_punch` WHERE `id` = '".$id."' ";
			$manual_data = $this->db->query($manual_data_sql)->row;
			if($manual_data['approval_1'] == '0' || $manual_data['approval_1'] == '2'){
				$trans_data_sql = "SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$manual_data['emp_id']."' AND `date` = '".$manual_data['date']."' ";
				$trans_datas = $this->db->query($trans_data_sql);
				if($trans_datas->num_rows > 0){
					$manual_data['transaction_id'] = $trans_datas->row['transaction_id'];
				}
				if($manual_data['transaction_id'] == 0){
					$sql = "INSERT INTO `oc_transaction` SET 
						`emp_id` = '".$manual_data['emp_id']."', 
						`emp_name` = '".$manual_data['emp_name']."', 
						`shift_id` = '".$manual_data['shift_id']."',
						`shift_code` = '".$manual_data['shift_code']."',
						`shift_intime` = '".$manual_data['shift_intime']."',
						`shift_outtime` = '".$manual_data['shift_outtime']."',
						`shift_outtime_flexi` = '".$manual_data['shift_outtime_flexi']."',
						`shift_allowance` = '".$manual_data['shift_allowance']."',
						`act_shift_id` = '".$manual_data['act_shift_id']."',
						`act_shift_code` = '".$manual_data['act_shift_code']."',
						`act_shift_intime` = '".$manual_data['act_shift_intime']."',
						`act_shift_outtime` = '".$manual_data['act_shift_outtime']."',
						`act_shift_outtime_flexi` = '".$manual_data['act_shift_outtime_flexi']."',
						`act_shift_allowance` = '".$manual_data['act_shift_allowance']."',
						`act_intime` = '".$manual_data['act_intime']."',
						`act_outtime` = '".$manual_data['act_outtime']."',
						`weekly_off` = '".$manual_data['weekly_off']."',
						`holiday_id` = '".$manual_data['holiday_id']."',
						`late_time` = '".$manual_data['late_time']."',
						`early_time` = '".$manual_data['early_time']."',
						`working_time` = '".$manual_data['working_time']."',
						`over_time` = '".$manual_data['over_time']."',
						`over_time_amount` = '".$manual_data['over_time_amount']."',
						`over_time_esic` = '".$manual_data['over_time_esic']."',
						`after_shift` = '".$manual_data['after_shift']."',
						`day` = '".$manual_data['day']."',
						`month` = '".$manual_data['month']."',
						`year` = '".$manual_data['year']."',
						`date` = '".$manual_data['date']."',
						`department` = '".$manual_data['department']."',
						`department_id` = '".$manual_data['department_id']."',
						`unit` = '".$manual_data['unit']."',
						`unit_id` = '".$manual_data['unit_id']."',
						`firsthalf_status` = '".$manual_data['firsthalf_status']."',
						`secondhalf_status` = '".$manual_data['secondhalf_status']."',
						`manual_status` = '".$manual_data['manual_status']."',
						`leave_status` = '".$manual_data['leave_status']."',
						`present_status` = '".$manual_data['present_status']."',
						`absent_status` = '".$manual_data['absent_status']."',
						`date_out` = '".$manual_data['date_out']."',
						`late_mark` = '".$manual_data['late_mark']."',
						`early_mark` = '".$manual_data['early_mark']."',
						`shift_change` = '".$manual_data['shift_change']."',
						`shift_free_change` = '".$manual_data['shift_free_change']."',
						`act_intime_change` = '".$manual_data['act_intime_change']."',
						`act_outtime_change` = '".$manual_data['act_outtime_change']."',
						`tour_id` = '".$manual_data['tour_id']."' ";
						//echo $sql;
						//echo '<br />';
						$this->db->query($sql);
						$this->log->write($sql);
						$transaction_id = $this->db->getLastId();
						$this->db->query("UPDATE `oc_manual_punch` SET `transaction_id` = '".$transaction_id."' WHERE `id` = '".$manual_data['id']."' ");
				} else {
					$sql = "UPDATE `oc_transaction` SET 
						`emp_id` = '".$manual_data['emp_id']."', 
						`emp_name` = '".$manual_data['emp_name']."', 
						`shift_id` = '".$manual_data['shift_id']."',
						`shift_code` = '".$manual_data['shift_code']."',
						`shift_intime` = '".$manual_data['shift_intime']."',
						`shift_outtime` = '".$manual_data['shift_outtime']."',
						`shift_outtime_flexi` = '".$manual_data['shift_outtime_flexi']."',
						`shift_allowance` = '".$manual_data['shift_allowance']."',
						`act_shift_id` = '".$manual_data['act_shift_id']."',
						`act_shift_code` = '".$manual_data['act_shift_code']."',
						`act_shift_intime` = '".$manual_data['act_shift_intime']."',
						`act_shift_outtime` = '".$manual_data['act_shift_outtime']."',
						`act_shift_outtime_flexi` = '".$manual_data['act_shift_outtime_flexi']."',
						`act_shift_allowance` = '".$manual_data['act_shift_allowance']."',
						`act_intime` = '".$manual_data['act_intime']."',
						`act_outtime` = '".$manual_data['act_outtime']."',
						`weekly_off` = '".$manual_data['weekly_off']."',
						`holiday_id` = '".$manual_data['holiday_id']."',
						`late_time` = '".$manual_data['late_time']."',
						`early_time` = '".$manual_data['early_time']."',
						`working_time` = '".$manual_data['working_time']."',
						`over_time` = '".$manual_data['over_time']."',
						`over_time_amount` = '".$manual_data['over_time_amount']."',
						`over_time_esic` = '".$manual_data['over_time_esic']."',
						`after_shift` = '".$manual_data['after_shift']."',
						`day` = '".$manual_data['day']."',
						`month` = '".$manual_data['month']."',
						`year` = '".$manual_data['year']."',
						`date` = '".$manual_data['date']."',
						`department` = '".$manual_data['department']."',
						`department_id` = '".$manual_data['department_id']."',
						`unit` = '".$manual_data['unit']."',
						`unit_id` = '".$manual_data['unit_id']."',
						`firsthalf_status` = '".$manual_data['firsthalf_status']."',
						`secondhalf_status` = '".$manual_data['secondhalf_status']."',
						`manual_status` = '".$manual_data['manual_status']."',
						`leave_status` = '".$manual_data['leave_status']."',
						`present_status` = '".$manual_data['present_status']."',
						`absent_status` = '".$manual_data['absent_status']."',
						`date_out` = '".$manual_data['date_out']."',
						`late_mark` = '".$manual_data['late_mark']."',
						`early_mark` = '".$manual_data['early_mark']."',
						`shift_change` = '".$manual_data['shift_change']."',
						`shift_free_change` = '".$manual_data['shift_free_change']."',
						`act_intime_change` = '".$manual_data['act_intime_change']."',
						`act_outtime_change` = '".$manual_data['act_outtime_change']."',
						`tour_id` = '".$manual_data['tour_id']."' 
						WHERE `transaction_id` = '".$manual_data['transaction_id']."' ";
						// echo $sql;
						// echo '<br />';
						// exit;
						$this->db->query($sql);
						$this->log->write($sql);
						$this->db->query("UPDATE `oc_manual_punch` SET `transaction_id` = '".$manual_data['transaction_id']."' WHERE `id` = '".$manual_data['id']."' ");
				}
			}
			//echo 'out';exit;
			date_default_timezone_set("Asia/Kolkata");
			//if(isset($this->session->data['is_super']) || isset($this->session->data['is_super1'])){
				//$approve_sql = "UPDATE `oc_leave_transaction_temp` SET `a_status` = '1', `approval_2` = '1', `approval_date_2` = '".date('Y-m-d h:i:s')."', `reject_date_2` = '0000-00-00 00:00:00' WHERE `batch_id` = '".$batch_id."' ";
			//} elseif(isset($this->session->data['is_dept'])){
				$approve_sql = "UPDATE `oc_manual_punch` SET `p_status` = '1', `approval_1` = '1', `approval_date` = '".date('Y-m-d h:i:s')."', `approval_1_by` = '".$this->session->data['emp_code']."', `reject_date` = '0000-00-00 00:00:00' WHERE `id` = '".$id."' ";
			//}
			$this->db->query($approve_sql);
			$this->log->write($approve_sql);
			
			$this->session->data['success'] = 'PX Record Entry Approved Successfully';

			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['filter_date_to'])) {
				$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
			}

			if (isset($this->request->get['filter_leave_from'])) {
				$url .= '&filter_leave_from=' . $this->request->get['filter_leave_from'];
			}

			if (isset($this->request->get['filter_leave_to'])) {
				$url .= '&filter_leave_to=' . $this->request->get['filter_leave_to'];
			}

			if (isset($this->request->get['filter_dept'])) {
				$url .= '&filter_dept=' . $this->request->get['filter_dept'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_proc'])) {
				$url .= '&filter_proc=' . $this->request->get['filter_proc'];
			}

			if (isset($this->request->get['filter_px_record_type'])) {
				$url .= '&filter_px_record_type=' . $this->request->get['filter_px_record_type'];
			}

			if (isset($this->request->get['filter_type'])) {
				$url .= '&filter_type=' . $this->request->get['filter_type'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('transaction/manualpunch_ess_admin', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		} elseif(isset($this->request->get['batch_id'])){
			$batch_id = $this->request->get['batch_id'];
			$manual_data_sql = "SELECT * FROM `oc_manual_punch` WHERE `batch_id` = '".$batch_id."' ";
			$manual_datas = $this->db->query($manual_data_sql)->rows;
			foreach($manual_datas as $mkey => $manual_data){
				if($manual_data['approval_1'] == '0' || $manual_data['approval_1'] == '2'){
					$trans_data_sql = "SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$manual_data['emp_id']."' AND `date` = '".$manual_data['date']."' ";
					$trans_datas = $this->db->query($trans_data_sql);
					if($trans_datas->num_rows > 0){
						$manual_data['transaction_id'] = $trans_datas->row['transaction_id'];
					}
					if($manual_data['transaction_id'] == 0){
						$sql = "INSERT INTO `oc_transaction` SET 
							`emp_id` = '".$manual_data['emp_id']."', 
							`emp_name` = '".$manual_data['emp_name']."', 
							`shift_id` = '".$manual_data['shift_id']."',
							`shift_code` = '".$manual_data['shift_code']."',
							`shift_intime` = '".$manual_data['shift_intime']."',
							`shift_outtime` = '".$manual_data['shift_outtime']."',
							`shift_outtime_flexi` = '".$manual_data['shift_outtime_flexi']."',
							`shift_allowance` = '".$manual_data['shift_allowance']."',
							`act_shift_id` = '".$manual_data['act_shift_id']."',
							`act_shift_code` = '".$manual_data['act_shift_code']."',
							`act_shift_intime` = '".$manual_data['act_shift_intime']."',
							`act_shift_outtime` = '".$manual_data['act_shift_outtime']."',
							`act_shift_outtime_flexi` = '".$manual_data['act_shift_outtime_flexi']."',
							`act_shift_allowance` = '".$manual_data['act_shift_allowance']."',
							`act_intime` = '".$manual_data['act_intime']."',
							`act_outtime` = '".$manual_data['act_outtime']."',
							`weekly_off` = '".$manual_data['weekly_off']."',
							`holiday_id` = '".$manual_data['holiday_id']."',
							`late_time` = '".$manual_data['late_time']."',
							`early_time` = '".$manual_data['early_time']."',
							`working_time` = '".$manual_data['working_time']."',
							`over_time` = '".$manual_data['over_time']."',
							`over_time_amount` = '".$manual_data['over_time_amount']."',
							`over_time_esic` = '".$manual_data['over_time_esic']."',
							`after_shift` = '".$manual_data['after_shift']."',
							`day` = '".$manual_data['day']."',
							`month` = '".$manual_data['month']."',
							`year` = '".$manual_data['year']."',
							`date` = '".$manual_data['date']."',
							`department` = '".$manual_data['department']."',
							`department_id` = '".$manual_data['department_id']."',
							`unit` = '".$manual_data['unit']."',
							`unit_id` = '".$manual_data['unit_id']."',
							`firsthalf_status` = '".$manual_data['firsthalf_status']."',
							`secondhalf_status` = '".$manual_data['secondhalf_status']."',
							`manual_status` = '".$manual_data['manual_status']."',
							`leave_status` = '".$manual_data['leave_status']."',
							`present_status` = '".$manual_data['present_status']."',
							`absent_status` = '".$manual_data['absent_status']."',
							`date_out` = '".$manual_data['date_out']."',
							`late_mark` = '".$manual_data['late_mark']."',
							`early_mark` = '".$manual_data['early_mark']."',
							`shift_change` = '".$manual_data['shift_change']."',
							`shift_free_change` = '".$manual_data['shift_free_change']."',
							`act_intime_change` = '".$manual_data['act_intime_change']."',
							`act_outtime_change` = '".$manual_data['act_outtime_change']."',
							`tour_id` = '".$manual_data['tour_id']."' ";
							//echo $sql;
							//echo '<br />';
							$this->db->query($sql);
							$this->log->write($sql);
							$transaction_id = $this->db->getLastId();
							$this->db->query("UPDATE `oc_manual_punch` SET `transaction_id` = '".$transaction_id."' WHERE `id` = '".$manual_data['id']."' ");

					} else {
						$sql = "UPDATE `oc_transaction` SET 
							`emp_id` = '".$manual_data['emp_id']."', 
							`emp_name` = '".$manual_data['emp_name']."', 
							`shift_id` = '".$manual_data['shift_id']."',
							`shift_code` = '".$manual_data['shift_code']."',
							`shift_intime` = '".$manual_data['shift_intime']."',
							`shift_outtime` = '".$manual_data['shift_outtime']."',
							`shift_outtime_flexi` = '".$manual_data['shift_outtime_flexi']."',
							`shift_allowance` = '".$manual_data['shift_allowance']."',
							`act_shift_id` = '".$manual_data['act_shift_id']."',
							`act_shift_code` = '".$manual_data['act_shift_code']."',
							`act_shift_intime` = '".$manual_data['act_shift_intime']."',
							`act_shift_outtime` = '".$manual_data['act_shift_outtime']."',
							`act_shift_outtime_flexi` = '".$manual_data['act_shift_outtime_flexi']."',
							`act_shift_allowance` = '".$manual_data['act_shift_allowance']."',
							`act_intime` = '".$manual_data['act_intime']."',
							`act_outtime` = '".$manual_data['act_outtime']."',
							`weekly_off` = '".$manual_data['weekly_off']."',
							`holiday_id` = '".$manual_data['holiday_id']."',
							`late_time` = '".$manual_data['late_time']."',
							`early_time` = '".$manual_data['early_time']."',
							`working_time` = '".$manual_data['working_time']."',
							`over_time` = '".$manual_data['over_time']."',
							`over_time_amount` = '".$manual_data['over_time_amount']."',
							`over_time_esic` = '".$manual_data['over_time_esic']."',
							`after_shift` = '".$manual_data['after_shift']."',
							`day` = '".$manual_data['day']."',
							`month` = '".$manual_data['month']."',
							`year` = '".$manual_data['year']."',
							`date` = '".$manual_data['date']."',
							`department` = '".$manual_data['department']."',
							`department_id` = '".$manual_data['department_id']."',
							`unit` = '".$manual_data['unit']."',
							`unit_id` = '".$manual_data['unit_id']."',
							`firsthalf_status` = '".$manual_data['firsthalf_status']."',
							`secondhalf_status` = '".$manual_data['secondhalf_status']."',
							`manual_status` = '".$manual_data['manual_status']."',
							`leave_status` = '".$manual_data['leave_status']."',
							`present_status` = '".$manual_data['present_status']."',
							`absent_status` = '".$manual_data['absent_status']."',
							`date_out` = '".$manual_data['date_out']."',
							`late_mark` = '".$manual_data['late_mark']."',
							`early_mark` = '".$manual_data['early_mark']."',
							`shift_change` = '".$manual_data['shift_change']."',
							`shift_free_change` = '".$manual_data['shift_free_change']."',
							`act_intime_change` = '".$manual_data['act_intime_change']."',
							`act_outtime_change` = '".$manual_data['act_outtime_change']."',
							`tour_id` = '".$manual_data['tour_id']."' 
							WHERE `transaction_id` = '".$manual_data['transaction_id']."' ";
							// echo $sql;
							// echo '<br />';
							// exit;
							$this->db->query($sql);
							$this->log->write($sql);
							$this->db->query("UPDATE `oc_manual_punch` SET `transaction_id` = '".$manual_data['transaction_id']."' WHERE `id` = '".$manual_data['id']."' ");
					}
				}
			}
			//echo 'out';exit;
			date_default_timezone_set("Asia/Kolkata");
			//if(isset($this->session->data['is_super']) || isset($this->session->data['is_super1'])){
				//$approve_sql = "UPDATE `oc_leave_transaction_temp` SET `a_status` = '1', `approval_2` = '1', `approval_date_2` = '".date('Y-m-d h:i:s')."', `reject_date_2` = '0000-00-00 00:00:00' WHERE `batch_id` = '".$batch_id."' ";
			//} elseif(isset($this->session->data['is_dept'])){
				$approve_sql = "UPDATE `oc_manual_punch` SET `p_status` = '1', `approval_1` = '1', `approval_date` = '".date('Y-m-d h:i:s')."', `approval_1_by` = '".$this->session->data['emp_code']."', `reject_date` = '0000-00-00 00:00:00' WHERE `batch_id` = '".$batch_id."' ";
			//}
			$this->db->query($approve_sql);
			$this->log->write($approve_sql);

			$this->session->data['success'] = 'Tour Entry Approved Successfully';

			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['filter_date_to'])) {
				$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
			}

			if (isset($this->request->get['filter_leave_from'])) {
				$url .= '&filter_leave_from=' . $this->request->get['filter_leave_from'];
			}

			if (isset($this->request->get['filter_leave_to'])) {
				$url .= '&filter_leave_to=' . $this->request->get['filter_leave_to'];
			}

			if (isset($this->request->get['filter_dept'])) {
				$url .= '&filter_dept=' . $this->request->get['filter_dept'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_proc'])) {
				$url .= '&filter_proc=' . $this->request->get['filter_proc'];
			}

			if (isset($this->request->get['filter_px_record_type'])) {
				$url .= '&filter_px_record_type=' . $this->request->get['filter_px_record_type'];
			}

			if (isset($this->request->get['filter_type'])) {
				$url .= '&filter_type=' . $this->request->get['filter_type'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('transaction/manualpunch_ess_admin', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	public function cancel() {
		date_default_timezone_set("Asia/Kolkata");
		$this->language->load('transaction/manualpunch_ess_dept');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		if (isset($this->request->post['selected'])) {
			if($this->request->get['type'] == 1){
				foreach ($this->request->post['selected'] as $id) {
					$approve_sql = "UPDATE `oc_manual_punch` SET `p_status` = '0', `approval_1` = '2', `approval_date` = '0000-00-00 00:00:00', `approval_1_by` = '0', `reject_date` = '".date('Y-m-d h:i:s')."' WHERE `id` = '".$id."' ";
					$this->db->query($approve_sql);
					$this->log->write($approve_sql);
				}
				$this->session->data['success'] = 'PX Record Entry Rejected Successfully';
			} else {
				foreach ($this->request->post['selected'] as $batch_id) {
					$approve_sql = "UPDATE `oc_manual_punch` SET `p_status` = '0', `approval_1` = '2', `approval_date` = '0000-00-00 00:00:00', `approval_1_by` = '0', `reject_date` = '".date('Y-m-d h:i:s')."' WHERE `batch_id` = '".$batch_id."' ";
					$this->db->query($approve_sql);
					$this->log->write($approve_sql);
				}
				$this->session->data['success'] = 'Tour Entry Rejected Successfully';
			}

			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['filter_date_to'])) {
				$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
			}

			if (isset($this->request->get['filter_leave_from'])) {
				$url .= '&filter_leave_from=' . $this->request->get['filter_leave_from'];
			}

			if (isset($this->request->get['filter_leave_to'])) {
				$url .= '&filter_leave_to=' . $this->request->get['filter_leave_to'];
			}

			if (isset($this->request->get['filter_dept'])) {
				$url .= '&filter_dept=' . $this->request->get['filter_dept'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_proc'])) {
				$url .= '&filter_proc=' . $this->request->get['filter_proc'];
			}

			if (isset($this->request->get['filter_px_record_type'])) {
				$url .= '&filter_px_record_type=' . $this->request->get['filter_px_record_type'];
			}

			if (isset($this->request->get['filter_type'])) {
				$url .= '&filter_type=' . $this->request->get['filter_type'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('transaction/manualpunch_ess_admin', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		} elseif (isset($this->request->post['reject_reason_1']) || isset($this->request->post['reject_reason_2'])) {
			$id = $this->request->get['id'];
			$reject_reason_1 = $this->request->post['reject_reason_1'];
			$approve_sql = "UPDATE `oc_manual_punch` SET `p_status` = '0', `approval_1` = '0', `approval_date` = '0000-00-00 00:00:00', `approval_1_by` = '', `reject_reason` = '".$reject_reason_1."', `reject_date` = '".date('Y-m-d h:i:s')."' WHERE `id` = '".$id."' ";
			$this->db->query($approve_sql);
			$this->log->write($approve_sql);
			$this->session->data['success'] = 'PX Record Entry Rejected Successfully';

			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['filter_date_to'])) {
				$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
			}

			if (isset($this->request->get['filter_leave_from'])) {
				$url .= '&filter_leave_from=' . $this->request->get['filter_leave_from'];
			}

			if (isset($this->request->get['filter_leave_to'])) {
				$url .= '&filter_leave_to=' . $this->request->get['filter_leave_to'];
			}

			if (isset($this->request->get['filter_dept'])) {
				$url .= '&filter_dept=' . $this->request->get['filter_dept'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_proc'])) {
				$url .= '&filter_proc=' . $this->request->get['filter_proc'];
			}

			if (isset($this->request->get['filter_px_record_type'])) {
				$url .= '&filter_px_record_type=' . $this->request->get['filter_px_record_type'];
			}

			if (isset($this->request->get['filter_type'])) {
				$url .= '&filter_type=' . $this->request->get['filter_type'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('transaction/manualpunch_ess_admin', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		} elseif(isset($this->request->get['id']) && $this->validateDelete()){
			$id = $this->request->get['id'];
			$approve_sql_1 = "UPDATE `oc_manual_punch` SET `p_status` = '0', `approval_1` = '2', `approval_date` = '0000-00-00 00:00:00', `approval_1_by` = '', `reject_date` = '".date('Y-m-d h:i:s')."' WHERE `id` = '".$id."' ";
			//echo $approve_sql_1;exit;
			$this->db->query($approve_sql_1);
			$this->log->write($approve_sql_1);

			$this->session->data['success'] = 'PX Record Entry Rejected Successfully';

			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['filter_date_to'])) {
				$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
			}

			if (isset($this->request->get['filter_leave_from'])) {
				$url .= '&filter_leave_from=' . $this->request->get['filter_leave_from'];
			}

			if (isset($this->request->get['filter_leave_to'])) {
				$url .= '&filter_leave_to=' . $this->request->get['filter_leave_to'];
			}

			if (isset($this->request->get['filter_dept'])) {
				$url .= '&filter_dept=' . $this->request->get['filter_dept'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_proc'])) {
				$url .= '&filter_proc=' . $this->request->get['filter_proc'];
			}

			if (isset($this->request->get['filter_px_record_type'])) {
				$url .= '&filter_px_record_type=' . $this->request->get['filter_px_record_type'];
			}

			if (isset($this->request->get['filter_type'])) {
				$url .= '&filter_type=' . $this->request->get['filter_type'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('transaction/manualpunch_ess_admin', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		} elseif(isset($this->request->get['batch_id']) && $this->validateDelete()){
			$batch_id = $this->request->get['batch_id'];
			$approve_sql_1 = "UPDATE `oc_manual_punch` SET `p_status` = '0', `approval_1` = '2', `approval_date` = '0000-00-00 00:00:00', `approval_1_by` = '', `reject_date` = '".date('Y-m-d h:i:s')."' WHERE `batch_id` = '".$batch_id."' ";
			//echo $approve_sql_1;exit;
			$this->db->query($approve_sql_1);
			$this->log->write($approve_sql_1);

			$this->session->data['success'] = 'Tour Entry Rejected Successfully';

			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['filter_date_to'])) {
				$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
			}

			if (isset($this->request->get['filter_leave_from'])) {
				$url .= '&filter_leave_from=' . $this->request->get['filter_leave_from'];
			}

			if (isset($this->request->get['filter_leave_to'])) {
				$url .= '&filter_leave_to=' . $this->request->get['filter_leave_to'];
			}

			if (isset($this->request->get['filter_dept'])) {
				$url .= '&filter_dept=' . $this->request->get['filter_dept'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_proc'])) {
				$url .= '&filter_proc=' . $this->request->get['filter_proc'];
			}

			if (isset($this->request->get['filter_px_record_type'])) {
				$url .= '&filter_px_record_type=' . $this->request->get['filter_px_record_type'];
			}

			if (isset($this->request->get['filter_type'])) {
				$url .= '&filter_type=' . $this->request->get['filter_type'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('transaction/manualpunch_ess_admin', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		}  else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_dept'])) {
			$filter_dept = html_entity_decode($this->request->get['filter_dept']);
		} else {
			$filter_dept = '0';
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = $this->request->get['filter_unit'];
		} else {
			$filter_unit = '0';
		}

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$year = date('Y');
			$month = date('n');
			if($month == 1){
				$prev_month = 12;
				$prev_year = $year - 1;
			} else {
				$prev_month = $month - 1;
				$prev_year = $year;
			}
			//$filter_date = sprintf("%04d-%02d-%02d", $prev_year, $prev_month, '26');
			$filter_date = '';
		}

		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		} else {
			$filter_date_to = '';
		}

		if (isset($this->request->get['filter_leave_from'])) {
			$filter_leave_from = $this->request->get['filter_leave_from'];
		} else {
			$year = date('Y');
			$month = date('n');
			if($month == 1){
				$prev_month = 12;
				$prev_year = $year - 1;
			} else {
				$prev_month = $month - 1;
				$prev_year = $year;
			}
			$filter_leave_from = sprintf("%04d-%02d-%02d", $prev_year, $prev_month, '26');
		}

		if (isset($this->request->get['filter_leave_to'])) {
			$filter_leave_to = $this->request->get['filter_leave_to'];
		} else {
			$filter_leave_to = '';
		}

		if (isset($this->request->get['filter_approval_1'])) {
			$filter_approval_1 = $this->request->get['filter_approval_1'];
		} else {
			$filter_approval_1 = '1';
		}

		if (isset($this->request->get['filter_proc'])) {
			$filter_proc = $this->request->get['filter_proc'];
		} else {
			$filter_proc = '0';
		}

		if (isset($this->request->get['filter_px_record_type'])) {
			$filter_px_record_type = $this->request->get['filter_px_record_type'];
		} else {
			$filter_px_record_type = '';
		}

		if (isset($this->request->get['filter_type'])) {
			$filter_type = $this->request->get['filter_type'];
		} else {
			$filter_type = '1';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		if (isset($this->request->get['filter_date_to'])) {
			$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
		}

		if (isset($this->request->get['filter_leave_from'])) {
			$url .= '&filter_leave_from=' . $this->request->get['filter_leave_from'];
		}

		if (isset($this->request->get['filter_leave_to'])) {
			$url .= '&filter_leave_to=' . $this->request->get['filter_leave_to'];
		}

		if (isset($this->request->get['filter_dept'])) {
			$url .= '&filter_dept=' . $this->request->get['filter_dept'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['filter_approval_1'])) {
			$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
		}

		if (isset($this->request->get['filter_proc'])) {
			$url .= '&filter_proc=' . $this->request->get['filter_proc'];
		}

		if (isset($this->request->get['filter_px_record_type'])) {
			$url .= '&filter_px_record_type=' . $this->request->get['filter_px_record_type'];
		}

		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('transaction/manualpunch_ess_admin', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$url_insert = '&filter_name_id='.$filter_name_id.'&filter_name_id_1='.$filter_name_id.'&filter_name='.$filter_name;

		$this->data['insert'] = $this->url->link('transaction/manualpunch_ess_admin/update', 'token=' . $this->session->data['token'] . $url . $url_insert, 'SSL');
		$this->data['delete'] = $this->url->link('transaction/manualpunch_ess_admin/delete', 'token=' . $this->session->data['token'] . $url . $url_insert, 'SSL');
		
		$this->data['manual_list'] = array();
		$employee_total = 0;

		$data = array(
			'filter_name' => $filter_name,
			'filter_name_id' => $filter_name_id,
			'filter_date' => $filter_date,
			'filter_date_to' => $filter_date_to,
			'filter_leave_from' => $filter_leave_from,
			'filter_leave_to' => $filter_leave_to,
			'filter_approval_1' => $filter_approval_1,
			'filter_proc' => $filter_proc,
			'filter_dept' => $filter_dept,
			'filter_unit' => $filter_unit,
			'filter_px_record_type' => $filter_px_record_type,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		if($filter_type == '1'){		
			$employee_total = $this->model_transaction_transaction->getTotalmanualTransaction_ess($data);
			$results = $this->model_transaction_transaction->getmanualTransaction_ess($data);
			foreach ($results as $result) {
				$action = array();
				if($result['p_status'] == 1){
					
				} else {
					if($result['reject_date'] == '0000-00-00'){
						$action[] = array(
							'text' => 'Accept',
							'href' => $this->url->link('transaction/manualpunch_ess_admin/approve', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . '&transaction_id=' . $result['transaction_id'] . $url, 'SSL')
						);
						$action[] = array(
							'text' => 'Reject',
							'href' => $this->url->link('transaction/manualpunch_ess_admin/cancel', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, 'SSL')
						);
					} else {
						// $action[] = array(
						// 	'text' => 'Accept',
						// 	'href' => $this->url->link('transaction/manualpunch_ess_admin/approve', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . '&transaction_id=' . $result['transaction_id'] . $url, 'SSL')
						// );
					}	
				}
				if($result['approval_1'] == '1'){
					if($result['approval_date'] != '0000-00-00 00:00:00'){
						$approval_1 = 'Approved on ' . date('d-m-y h:i:s', strtotime($result['approval_date']));
					} else {
						$approval_1 = 'Approved';
					}
				} else if($result['approval_1'] == '2'){
					$approval_1 = 'Rejected';
				} else {
					$approval_1 = 'Pending';
				}

				
				if($result['p_status'] == '1'){
					$proc_stat = 'Processed';
				} else {
					$proc_stat = 'UnProcessed';
				}

				if($result['reject_date'] != '0000-00-00'){
					if($result['reject_reason'] != ''){
						$reject_reason = $result['reject_reason'].' - as on '.date('d-m-y', strtotime($result['reject_date']));
					} else {
						$reject_reason = 'as on '.date('d-m-y', strtotime($result['reject_date']));
					}
				} else {
					$reject_reason = '';
				}

				$px_record_type = '';
				$modification = '';
				if($result['px_record_type'] == 1){
					$modification = $result['act_outtime_old'];
					$px_record_type = 'Early Modification';
				} elseif($result['px_record_type'] == 2){
					$modification = $result['act_intime_old'];
					$px_record_type = 'Late Modification';
				} elseif($result['px_record_type'] == 3){
					$modification = $result['act_intime_old'].' - '.$result['act_outtime_old'];
					$px_record_type = 'Other Modification';
				}

				if($result['type'] == '1'){
					$type = 'PX Record';
				} else {
					$type = 'TOUR';
				}
				$emp_data = $this->model_transaction_transaction->getempdata($result['emp_id']);

				$this->data['manual_list'][] = array(
					'id' => $result['id'],
					'emp_id' => $result['emp_id'],
					'name' => $emp_data['emp_name'],
					'dot' => date('d-m-y', strtotime($result['dot'])),
					'date' => date('d-m-y', strtotime($result['date'])),
					'in_time' => $result['act_intime'],
					'out_time' => $result['act_outtime'],
					'modification' => $modification,
					'approval_1' => $approval_1,
					'reject_reason' => $reject_reason,
					'proc_stat' => $proc_stat,
					'px_record_type' => $px_record_type,
					'type' => $type,
					'unit' => $result['unit'],
					'dept_name' => $result['department'],
					'punch_reason' => $result['punch_reason'],
					'selected'        => isset($this->request->post['selected']) && in_array($result['id'], $this->request->post['selected']),
					'action'          => $action
				);
			}
		} else {
			$data['group_by'] = 1;
			// echo '<pre>';
			// print_r($data);
			// exit;
			$employee_total = $this->model_transaction_transaction->getTotalmanualTransaction_ess($data);
			$results = $this->model_transaction_transaction->getmanualTransaction_ess($data);
			foreach ($results as $result) {
				$action = array();
				if($result['p_status'] == 1){
					
				} else {
					if($result['reject_date'] == '0000-00-00'){
						$action[] = array(
							'text' => 'Accept',
							'href' => $this->url->link('transaction/manualpunch_ess_admin/approve', 'token=' . $this->session->data['token'] . '&batch_id=' . $result['batch_id'] . '&transaction_id=' . $result['transaction_id'] . $url, 'SSL')
						);
						$action[] = array(
							'text' => 'Reject',
							'href' => $this->url->link('transaction/manualpunch_ess_admin/cancel', 'token=' . $this->session->data['token'] . '&batch_id=' . $result['batch_id'] . $url, 'SSL')
						);
					} else {
						// $action[] = array(
						// 	'text' => 'Accept',
						// 	'href' => $this->url->link('transaction/manualpunch_ess_admin/approve', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . '&transaction_id=' . $result['transaction_id'] . $url, 'SSL')
						// );
					}	
				}
				if($result['approval_1'] == '1'){
					if($result['approval_date'] != '0000-00-00 00:00:00'){
						$approval_1 = 'Approved on ' . date('d-m-y h:i:s', strtotime($result['approval_date']));
					} else {
						$approval_1 = 'Approved';
					}
				} else if($result['approval_1'] == '2'){
					$approval_1 = 'Rejected';
				} else {
					$approval_1 = 'Pending';
				}

				
				if($result['p_status'] == '1'){
					$proc_stat = 'Processed';
				} else {
					$proc_stat = 'UnProcessed';
				}

				if($result['reject_date'] != '0000-00-00'){
					if($result['reject_reason'] != ''){
						$reject_reason = $result['reject_reason'].' - as on '.date('d-m-y', strtotime($result['reject_date']));
					} else {
						$reject_reason = 'as on '.date('d-m-y', strtotime($result['reject_date']));
					}
				} else {
					$reject_reason = '';
				}

				$px_record_type = '';
				if($result['px_record_type'] == 1){
					$px_record_type = 'Early Modification';
				} elseif($result['px_record_type'] == 2){
					$px_record_type = 'Late Modification';
				} elseif($result['px_record_type'] == 3){
					$px_record_type = 'Other Modification';
				}

				if($result['type'] == '1'){
					$type = 'PX Record';
				} else {
					$type = 'TOUR';
				}

				$date_from = $this->model_transaction_transaction->gettour_from_ess($result['batch_id']);
				$date_to = $this->model_transaction_transaction->gettour_to_ess($result['batch_id']);

				$this->data['manual_list'][] = array(
					'batch_id' => $result['batch_id'],
					'id' => $result['id'],
					'emp_id' => $result['emp_id'],
					'name' => $result['emp_name'],
					'dot' => date('d-m-y', strtotime($result['dot'])),
					'date_from' => date('d-m-y', strtotime($date_from)),
					'date_to' => date('d-m-y', strtotime($date_to)),
					'approval_1' => $approval_1,
					'reject_reason' => $reject_reason,
					'proc_stat' => $proc_stat,
					'px_record_type' => $px_record_type,
					'type' => $type,
					'unit' => $result['unit'],
					'dept_name' => $result['department'],
					'punch_reason' => $result['punch_reason'],
					'selected'        => isset($this->request->post['selected']) && in_array($result['id'], $this->request->post['selected']),
					'action'          => $action
				);
			}
		}

		// echo '<pre>';
		// print_r($this->data['leaves']);
		// exit;

		$locations = $this->db->query("SELECT * FROM `oc_unit` ")->rows;
		$unit_data['0'] = 'All'; 
		foreach($locations as $lkey => $lvalue){
			$unit_data[$lvalue['unit_id']] = $lvalue['unit'];
		}
		$this->data['unit_data'] = $unit_data;

		$approves = array(
			'0' => 'All',
			'1' => 'Pending',
			'2' => 'Approved',
			'3' => 'Rejected'
		);
		$this->data['approves'] = $approves;

		$process = array(
			'0' => 'All',
			'1' => 'UnProcessed',
			'2' => 'Processed',
		);
		$this->data['process'] = $process;

		$this->data['px_record_types'] = array(
			'0' => 'All',
			'1' => 'Early Modification',
			'2' => 'Late Modification',
			'3' => 'Other Modification',
		);

		$this->data['types'] = array(
			'1' => 'PX Record',
			'2' => 'TOUR',
		);

		$department_datas = $this->db->query("SELECT * FROM `oc_department` ")->rows;
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['d_name'];
		}
		$this->data['dept_data'] = $department_data;

		$this->data['token'] = $this->session->data['token'];	

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_delete'] = $this->language->get('text_delete');

		$this->data['button_insert'] = $this->language->get('button_insert');

		$this->data['button_filter'] = $this->language->get('button_filter');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		if (isset($this->request->get['filter_date_to'])) {
			$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
		}

		if (isset($this->request->get['filter_leave_from'])) {
			$url .= '&filter_leave_from=' . $this->request->get['filter_leave_from'];
		}

		if (isset($this->request->get['filter_leave_to'])) {
			$url .= '&filter_leave_to=' . $this->request->get['filter_leave_to'];
		}

		if (isset($this->request->get['filter_dept'])) {
			$url .= '&filter_dept=' . $this->request->get['filter_dept'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['filter_approval_1'])) {
			$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
		}

		if (isset($this->request->get['filter_proc'])) {
			$url .= '&filter_proc=' . $this->request->get['filter_proc'];
		}

		if (isset($this->request->get['filter_px_record_type'])) {
			$url .= '&filter_px_record_type=' . $this->request->get['filter_px_record_type'];
		}

		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$pagination = new Pagination();
		$pagination->total = $employee_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/manualpunch_ess_admin', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_date'] = $filter_date;
		$this->data['filter_date_to'] = $filter_date_to;
		$this->data['filter_leave_from'] = $filter_leave_from;
		$this->data['filter_leave_to'] = $filter_leave_to;
		$this->data['filter_approval_1'] = $filter_approval_1;
		$this->data['filter_proc'] = $filter_proc;
		$this->data['filter_dept'] = $filter_dept;
		$this->data['filter_unit'] = $filter_unit;
		$this->data['filter_px_record_type'] = $filter_px_record_type;
		$this->data['filter_type'] = $filter_type;
		
		$this->template = 'transaction/manualpunch_ess_admin.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validateDelete(){
		// $this->load->model('transaction/transaction');
		// if(isset($this->request->post['selected'])){
		// 	foreach ($this->request->post['selected'] as $batch_id) {
		// 		$leave_sql = "SELECT `id` FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' AND `p_status` = '1' ";
		// 		$date_res = $this->db->query($leave_sql);
		// 		if ($date_res->num_rows > 0) {
		// 			$this->error['warning'] = 'Leave With batch ' . $batch_id . ' already processed';
		// 		}
		// 	}	
		// } elseif(isset($this->request->get['batch_id'])){
		// 	$batch_id = $this->request->get['batch_id'];
		// 	$leave_sql = "SELECT `id` FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' AND `p_status` = '1' ";
		// 	$date_res = $this->db->query($leave_sql);
		// 	if ($date_res->num_rows > 0) {
		// 		$this->error['warning'] = 'Leave With batch ' . $batch_id . ' already processed';
		// 	}
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}		
	}

	protected function validateForm() {
		$this->load->model('transaction/transaction');

		if (!$this->user->hasPermission('modify', 'transaction/horse_wise')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if(strlen(utf8_decode(trim($this->request->post['e_name']))) < 1 || strlen(utf8_decode(trim($this->request->post['e_name']))) > 255){
			$this->error['e_name'] = 'Please Select Employee Name';
		} else {
			if($this->request->post['e_name_id'] == ''){
				$emp_id = $this->model_transaction_transaction->getempexist($this->request->post['e_name_id']);				
				if($emp_id == 0){
					$this->error['e_name'] = 'Employee Does Not Exist';
				}
			}
		}

		if(strlen(utf8_decode(trim($this->request->post['dot']))) < 1 || strlen(utf8_decode(trim($this->request->post['dot']))) > 255){
			$this->error['dot'] = $this->language->get('error_dot');
		}

		if($this->request->post['shift_id'] === 0){
			$this->error['shift_id'] = 'Please Select Value From Shift Data';
		}

		// echo '<pre>';
		// print_r($this->error);
		// exit;

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>