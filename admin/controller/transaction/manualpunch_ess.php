<?php    
class ControllerTransactionManualpunchess extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('transaction/manualpunch_ess');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		$this->getList();
	}

	public function update() {
		$this->language->load('transaction/manualpunch_ess');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			if($this->request->post['type'] == '1'){
				if($this->request->post['actual_intime'] == '' || $this->request->post['actual_intime'] == '0') {
					$this->request->post['actual_intime'] = '00:00:00';
				}
				if($this->request->post['actual_outtime'] == '' || $this->request->post['actual_outtime'] == '0') {
					$this->request->post['actual_outtime'] = '00:00:00';
				}
				if($this->request->post['dot'] != ''){
					$this->request->post['dot'] = date('Y-m-d', strtotime($this->request->post['dot']));
					$this->request->post['day'] = date('j', strtotime($this->request->post['dot']));
					$this->request->post['month'] = date('n', strtotime($this->request->post['dot']));
					$this->request->post['year'] = date('Y', strtotime($this->request->post['dot']));
				} else {
					$this->request->post['dot'] = date('Y-m-d');
					$this->request->post['day'] = date('j');
					$this->request->post['month'] = date('n');
					$this->request->post['year'] = date('Y');
				}
				$day = $this->request->post['day'];
				$month = $this->request->post['month'];
				$year = $this->request->post['year'];

				$act_intime = $this->request->post['actual_intime'];
				$act_outtime = $this->request->post['actual_outtime'];
				$shift_intime = $this->request->post['shift_intime'];
				$shift_outtime = $this->request->post['shift_outtime'];
				$shift_outtime_flexi = Date('H:i:s', strtotime($this->request->post['shift_outtime'] .' +60 minutes'));
				$act_shift_intime = $shift_intime;
				$act_shift_outtime = $shift_outtime;
				$act_shift_outtime_flexi = $shift_outtime_flexi;
				$punch_date = $this->request->post['dot'];
				$act_in_punch_date = date('Y-m-d', strtotime($this->request->post['date_from']));
				$act_out_punch_date = date('Y-m-d', strtotime($this->request->post['date_to']));
				$data = $this->request->post;

				$abnormal_status = 0;
				$leave_status = 0;
				$first_half = 0;
				$second_half = 0;
				$late_time = '00:00:00';
				//$act_intime = '09:59:00';
				//$act_outtime = '19:45:00';
				$late_mark = 0;
				$early_mark = 0;
				$shift_early = 0;
				$working_time = '00:00:00';
				$shift_working_time = '00:00:00';
				$emp_data = $this->model_transaction_transaction->getEmployees_dat($this->request->post['e_name_id']);

				$shift_data = $this->db->query("SELECT * FROM `oc_shift` WHERE `in_time` = '".$shift_intime."' ")->row;
				if($act_intime != '00:00:00'){
					$shift_intime_plus_one = Date('H:i:s', strtotime($act_shift_intime .' +60 minutes'));
					$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
					$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
					if($since_start->h > 12){
						$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
					}
					if($since_start->invert == 1 || ($since_start->h == 0 && $since_start->i == 0)){
						$late_time = '00:00:00';
						$late_mark = 0;
					} else {
						if($since_start->h > 0 || ($since_start->h == 0 && $since_start->i > 0)){
							$late_hour = $since_start->h;
							$late_min = $since_start->i;
							$late_sec = $since_start->s;
							$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
							$late_mark = 1;
						} else {
							$late_time = '00:00:00';
							$late_mark = 0;
						}
					}
					$shift_intime_minus_one = Date('H:i:s', strtotime($act_shift_intime .' -0 minutes'));
					$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
					$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
					if($since_start->h > 12){
						$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
					}
					if($since_start->invert == 1){
						$shift_early = 1;
					}

					$start_date = new DateTime($act_in_punch_date.' '.$act_shift_intime);
					$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_shift_outtime));
					$shift_working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
					
					if($act_outtime != '00:00:00'){
						if($shift_early == 1){
							$shift_intime_minus_one = Date('H:i:s', strtotime($act_shift_intime .' -0 minutes'));
							if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
								$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
							} else {
								$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
							}
						} else {
							$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
						}
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
						if($data['shift_id'] == 'HD'){
							if( ($since_start->h == 4 && $since_start->i >= 00) || $since_start->h > 4){
								$first_half = 1;
								$second_half = 1;
							} elseif( ($since_start->h == 2 && $since_start->i >= 00) || $since_start->h > 2){
								$first_half = 1;
								$second_half = 0;
							} else {
								$first_half = 0;
								$second_half = 0;
							}
						} else {
							if(strtotime($shift_working_time) == strtotime('09:30:00')){
								if( ($since_start->h == 9 && $since_start->i >= 30) || $since_start->h > 9){
									$first_half = 1;
									$second_half = 1;
								} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
									$first_half = 1;
									$second_half = 0;
								} else {
									$first_half = 0;
									$second_half = 0;
								}
							} elseif (strtotime($shift_working_time) == strtotime('09:00:00')) {
								if( ($since_start->h == 9 && $since_start->i >= 0) || $since_start->h > 9){
									$first_half = 1;
									$second_half = 1;
								} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
									$first_half = 1;
									$second_half = 0;
								} else {
									$first_half = 0;
									$second_half = 0;
								}
							} elseif (strtotime($shift_working_time) == strtotime('08:30:00')) {
								if( ($since_start->h == 8 && $since_start->i >= 30) || $since_start->h > 8){
									$first_half = 1;
									$second_half = 1;
								} elseif( ($since_start->h == 4 && $since_start->i >= 0) || $since_start->h > 4){
									$first_half = 1;
									$second_half = 0;
								} else {
									$first_half = 0;
									$second_half = 0;
								}
							}
						}
						$working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
					} else {
						$first_half = 0;
						$second_half = 0;
					}
				} else {
					$first_half = 0;
					$second_half = 0;
				}

				$early_time = '00:00:00';
				$over_time = '00:00:00';
				$over_time_amount = '0';
				$over_time_esic = '0';
				$after_shift = '0';
				if($abnormal_status == 0){ //if abnormal status is zero calculate further 
					if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and actouttime 
						$start_date = new DateTime($act_in_punch_date.' '.$act_shift_outtime_flexi);
						$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
						if($since_start->invert == 0 && ($since_start->h > 0 || $since_start->i > 0)){
							$after_shift = 1;
						} else {
							$after_shift = 0;
						}
						$shift_outtime_minus_one = Date('H:i:s', strtotime($act_shift_outtime .' -0 minutes'));
						$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
						if($since_start->h > 12){
							$shift_outtime_minus_one = Date('H:i:s', strtotime($act_shift_outtime .' -0 minutes'));
							$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
						}
						$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
						if($since_start->invert == 0){
							$early_time = '00:00:00';
						} else {
							$early_mark = 1;
						}
						$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
						if($since_start->invert == 1){
							$over_time = '00:00:00';
						} else {
							if($data['shift_id'] == 'HD'){
								$shift_working_hours = '04:00:00';
								$start_date = new DateTime($act_in_punch_date.' '.$shift_working_hours);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$working_time));
								if($since_start->invert == 0){
									$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
								} else {
									$over_time = '00:00:00';
								}
							} else {
								if(strtotime($shift_working_time) == strtotime('09:30:00')){
									$shift_working_hours = '09:30:00';
									$start_date = new DateTime($act_in_punch_date.' '.$shift_working_hours);
									$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$working_time));
									if($since_start->invert == 0){
										$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
									} else {
										$over_time = '00:00:00';
									}
								} elseif(strtotime($shift_working_time) == strtotime('09:00:00')){
									$shift_working_hours = '09:00:00';
									$start_date = new DateTime($act_in_punch_date.' '.$shift_working_hours);
									$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$working_time));
									if($since_start->invert == 0){
										$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
									} else {
										$over_time = '00:00:00';
									}
								} elseif(strtotime($shift_working_time) == strtotime('08:30:00')){
									$shift_working_hours = '08:30:00';
									$start_date = new DateTime($act_in_punch_date.' '.$shift_working_hours);
									$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$working_time));
									if($since_start->invert == 0){
										$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
									} else {
										$over_time = '00:00:00';
									}
								}
							}
							/*
							if($late_time != '00:00:00'){
								$late_time_exp = explode(':', $late_time);
								$hour = $late_time_exp[0];
								$min = $late_time_exp[1];
								$hour_min = $hour.':'.$min;
								$minutes_compare = hoursToMinutes($hour_min);
								$over_time1 = $over_time;
								$over_time_exp = explode(':', $over_time);
								$hour = $over_time_exp[0];
								$min = $over_time_exp[1];
								$hour_min = $hour.':'.$min;
								$over_time_minutes_compare = hoursToMinutes($hour_min);
								if($over_time_minutes_compare > $minutes_compare){
									$over_time = Date('H:i:s', strtotime($over_time1 .' -'. $minutes_compare .' minutes'));
								} else {
									$over_time = '00:00:00';
								}
							} else {
								$over_time = $over_time;
							}
							*/
							/*
							$overtime_exp = explode(':', $over_time);
							$hour = $overtime_exp[0];
							$min = $overtime_exp[1];
							$hour_min = $hour.':'.$min;
							$minutes_compare = hoursToMinutes($hour_min);
							$over_time_hours = '';
							if($minutes_compare > 0 && $minutes_compare <= 44){
								$over_time_hours = '00';
							} elseif($minutes_compare >= 45 && $minutes_compare <= 104){
								$over_time_hours = '01';
							} elseif($minutes_compare >= 105 && $minutes_compare <= 164){
								$over_time_hours = '02';
							} elseif($minutes_compare >= 165 && $minutes_compare <= 224){
								$over_time_hours = '03';
							} elseif($minutes_compare >= 225 && $minutes_compare <= 284){
								$over_time_hours = '04';
							} elseif($minutes_compare >= 285 && $minutes_compare <= 344){
								$over_time_hours = '05';
							} elseif($minutes_compare >= 345 && $minutes_compare <= 405){
								$over_time_hours = '06';
							} elseif($minutes_compare >= 405 && $minutes_compare <= 464){
								$over_time_hours = '07';
							} elseif($minutes_compare >= 465 && $minutes_compare <= 524){
								$over_time_hours = '08';
							} elseif($minutes_compare >= 525 && $minutes_compare <= 584){
								$over_time_hours = '09';
							} elseif($minutes_compare >= 585 && $minutes_compare <= 644){
								$over_time_hours = '10';
							} elseif($minutes_compare >= 645 && $minutes_compare <= 704){
								$over_time_hours = '11';
							} elseif($minutes_compare >= 705 && $minutes_compare <= 764){
								$over_time_hours = '12';
							} elseif($minutes_compare >= 765 && $minutes_compare <= 824){
								$over_time_hours = '13';
							} elseif($minutes_compare >= 825 && $minutes_compare <= 884){
								$over_time_hours = '14';
							} elseif($minutes_compare >= 885 && $minutes_compare <= 944){
								$over_time_hours = '15';
							} elseif($minutes_compare >= 945 && $minutes_compare <= 1004){
								$over_time_hours = '16';
							}
							// if($emp_salary['basic'] > 0 && $emp_salary['vda'] > 0){
							// 	$over_time_amount_1 = (($emp_salary['basic'] + $emp_salary['vda']) / 8);
							// 	$over_time_amount = $over_time_amount_1 * 2 * $over_time_hours;
							// }
							$over_time_amount = '0.00';
							//$over_time = $over_time_hours.':00';
							$over_time_esic = round(($over_time_amount * 1.75) / 100);
							*/
						}
					}					
				} else {
					$first_half = 0;
					$second_half = 0;
				}

				$leave_data_sql = "SELECT `leave_type`, `type` FROM `oc_leave_transaction` WHERE `emp_id` = '".$this->request->post['e_name_id']."' AND `date` = '".$act_in_punch_date."' AND `p_status` = '1' AND `a_status` = '1' ";
				$leave_data_res = $this->db->query($leave_data_sql);
				$leave_in = 0;
				if($leave_data_res->num_rows > 0){
					//$shift_inn = '1';
					if($leave_data_res->row['type'] == ''){
						$leave_in = 1;
						$first_half = $leave_data_res->row['leave_type'];
						$second_half = $leave_data_res->row['leave_type'];
					} else {
						if($leave_data_res->row['type'] == 'F'){
							$leave_in = 1;
							$first_half = $leave_data_res->row['leave_type'];
							$second_half = $leave_data_res->row['leave_type'];
						} elseif($leave_data_res->row['type'] == '1'){
							$leave_in = 2;
							$first_half = $leave_data_res->row['leave_type'];
						} elseif($leave_data_res->row['type'] == '2'){
							$leave_in = 3;
							$second_half = $leave_data_res->row['leave_type'];
						}
					}
				}
				

				$shift_change = 0;
				$shift_free_change = 0;
				if($data['shift_id'] != $data['hidden_shift_id']){
					if($data['shift_id'] == 'WO' || $data['shift_id'] == 'HLD' || $data['shift_id'] == 'OD'){
						$shift_free_change = 1;
					} else {
						$shift_change = 1;
					}
				}

				$act_intime_change = 0;
				if($data['actual_intime'] != $data['hidden_actual_intime']){
					$act_intime_change = 1;
				}

				$act_outtime_change = 0;
				if($data['actual_outtime'] != $data['hidden_actual_outtime']){
					$act_outtime_change = 1;
				}

				if($first_half == '1' && $second_half == '1'){
					$present_status = 1;
					$absent_status = 0;
				} elseif($first_half == '1' && $second_half == '0'){
					$present_status = 0.5;
					$absent_status = 0.5;
				} elseif($first_half == '0' && $second_half == '1'){
					$present_status = 0.5;
					$absent_status = 0.5;
				} elseif($leave_in == '1'){
					$present_status = 0;
					$absent_status = 0;
					$leave_status = 1;
				} elseif($leave_in == '2'){
					$present_status = 0.5;
					if($second_half == '0'){
						$absent_status = 0.5;
					} else {
						$absent_status = 0;
					}
					$leave_status = 0.5;
				} elseif($leave_in == '3'){
					$present_status = 0.5;
					if($first_half == '0'){
						$absent_status = 0.5;
					} else {
						$absent_status = 0;
					}
					$leave_status = 0.5;
				} else {
					$present_status = 0;
					$absent_status = 1;
				}
				
				$day_date1 = date('j', strtotime($act_in_punch_date));
				$day1 = date('j', strtotime($act_in_punch_date));
				$month1 = date('n', strtotime($act_in_punch_date));
				$year1 = date('Y', strtotime($act_in_punch_date));

				$update3 = "SELECT  `".$day_date1."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$data['e_name_id']."' AND `month` = '".$month1."' AND `year` = '".$year1."' ";
				$shift_schedule = $this->db->query($update3)->row;
				$schedule_raw = explode('_', $shift_schedule[$day_date1]);
				if(!isset($schedule_raw[2])){
					$schedule_raw[2] = 1;
				}

				if($data['shift_id'] == 'WO'){
					$absent_status = 0;
					$weekly_off = 1;
					$holiday = 0;
					$half_day = 0;
					$tour_id = 0;
					$first_half = 'WO';
					$second_half = 'WO';
					$day_info_id = 'W_1';
					$shift_id = $schedule_raw[2];
					$act_shift_id = $schedule_raw[2];
					$shift_data = $this->model_transaction_transaction->getshiftdata($shift_id);
					$shift_code = $shift_data['shift_code'];
					$act_shift_code = $shift_code;
				} elseif($data['shift_id'] == 'HLD'){
					$absent_status = 0;
					$weekly_off = 0;
					$holiday = 1;
					$half_day = 0;
					$tour_id = 0;
					$first_half = 'HLD';
					$second_half = 'HLD';
					$day_info_id = 'H_1';
					$shift_id = $schedule_raw[2];
					$act_shift_id = $schedule_raw[2];
					$shift_data = $this->model_transaction_transaction->getshiftdata($shift_id);
					$shift_code = $shift_data['shift_code'];
					$act_shift_code = $shift_code;
				} elseif($data['shift_id'] == 'HD'){
					$weekly_off = 0;
					$holiday = 0;
					$half_day = 1;
					$tour_id = 0;
					$first_half = 'HD';
					$second_half = 'HD';
					$day_info_id = 'HD_1';
					$shift_id = $schedule_raw[2];
					$act_shift_id = $schedule_raw[2];
					$shift_data = $this->model_transaction_transaction->getshiftdata($shift_id);
					$shift_code = $shift_data['shift_code'];
					$act_shift_code = $shift_code;
				} elseif($data['shift_id'] == 'TOUR'){
					$absent_status = 0;
					$weekly_off = 0;
					$holiday = 0;
					$half_day = 0;
					$tour_id = 1;
					$first_half = 'TOUR';
					$second_half = 'TOUR';
					$day_info_id = 'TOUR_1';
					$shift_id = $schedule_raw[1];
					$act_shift_id = $schedule_raw[1];
					$shift_data = $this->model_transaction_transaction->getshiftdata($shift_id);
					$shift_code = $shift_data['shift_code'];
					$act_shift_code = $shift_code;
				} else {
					$weekly_off = 0;
					$holiday = 0;
					$half_day = 0;
					$tour_id = 0;
					$day_info_id = 'S_'.$data['shift_id'];
					$shift_id = $data['shift_id'];
					$act_shift_id = $data['shift_id'];
					$shift_data = $this->model_transaction_transaction->getshiftdata($shift_id);
					if(isset($shift_data['shift_code'])){
						$shift_code = $shift_data['shift_code'];
					} else {
						$shift_code = '';
					}
					$act_shift_code = $shift_code;
				}

				// echo '<pre>';
				// print_r($data['insert']);
				// exit;
				
				if(isset($emp_data['emp_code'])){
					$department = $emp_data['department'];
					$department_id = $emp_data['department_id'];
					$unit = $emp_data['unit'];
					$unit_id = $emp_data['unit_id'];
					$reporting_to = $emp_data['reporting_to'];
				} else {
					$department = '';
					$department_id = '';
					$unit = '';
					$unit_id = '';
					$reporting_to = '';
				}
				if($data['insert'] == '0'){
					$sql = "UPDATE `oc_manual_punch` SET `type` = '".$data['type']."', `px_record_type` = '".$data['px_record_type']."', `reporting_to` = '".$reporting_to."', `punch_reason` = '".$data['punch_reason']."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `act_shift_id` = '".$act_shift_id."', `act_shift_code` = '".$act_shift_code."', `act_shift_intime` = '".$shift_intime."', `act_shift_outtime` = '".$shift_outtime."', `act_shift_outtime_flexi` = '".$act_shift_outtime_flexi."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `act_intime_old` = '".$data['hidden_actual_intime']."', `act_outtime_old` = '".$data['hidden_actual_outtime']."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', `working_time` = '".$working_time."', `over_time` = '".$over_time."', `over_time_amount` = '".$over_time_amount."', `over_time_esic` = '".$over_time_esic."', `after_shift` = '".$after_shift."', `weekly_off` = '".$weekly_off."', `holiday_id` = '".$holiday."', `halfday_status` = '".$half_day."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `abnormal_status` = '".$abnormal_status."', `present_status` = '".$present_status."', `leave_status` = '".$leave_status."', `absent_status` = '".$absent_status."', `manual_status` = '1', `shift_change` = '".$shift_change."', `shift_free_change` = '".$shift_free_change."', `act_intime_change` = '".$act_intime_change."', `act_outtime_change` = '".$act_outtime_change."' WHERE `id` = '".$data['id']."' ";
					// echo $sql;
					// echo '<br />';
					// exit;
					$this->db->query($sql);	
					$this->session->data['success'] = 'You have Modified the PX Record Entry';
					$id = $data['id'];
				} else {
					if($leave_in == 1){
						$leave_status = 1;
					} elseif($leave_in == 2 || $leave_in == 3){
						$leave_status = 0.5;
					} else {
						$leave_status = 0;
					}
					$sql = "INSERT INTO `oc_manual_punch` SET `type` = '".$data['type']."', `px_record_type` = '".$data['px_record_type']."', `reporting_to` = '".$reporting_to."', `dot` = '".date('Y-m-d')."', `punch_reason` = '".$data['punch_reason']."', `transaction_id` = '".$data['transaction_id']."', `emp_id` = '".$data['e_name_id']."', `emp_name` = '".$data['e_name']."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `act_shift_id` = '".$act_shift_id."', `act_shift_code` = '".$act_shift_code."', `act_shift_intime` = '".$shift_intime."', `act_shift_outtime` = '".$shift_outtime."', `act_shift_outtime_flexi` = '".$act_shift_outtime_flexi."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `act_intime_old` = '".$data['hidden_actual_intime']."', `act_outtime_old` = '".$data['hidden_actual_outtime']."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', `working_time` = '".$working_time."', `over_time` = '".$over_time."', `over_time_amount` = '".$over_time_amount."', `over_time_esic` = '".$over_time_esic."', `after_shift` = '".$after_shift."', `day` = '".$data['day']."', `month` = '".$data['month']."', `year` = '".$data['year']."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `weekly_off` = '".$weekly_off."', `holiday_id` = '".$holiday."', `halfday_status` = '".$half_day."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `abnormal_status` = '".$abnormal_status."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', `leave_status` = '".$leave_status."', `manual_status` = '1', `department` = '".$this->db->escape($department)."', department_id = '".$this->db->escape($department_id)."', `unit` = '".$this->db->escape($unit)."', unit_id = '".$this->db->escape($unit_id)."' ";
					// echo $sql;
					// echo '<br />';
					// exit;
					$this->db->query($sql);
					$id = $this->db->getLastId();
					$this->session->data['success'] = 'You have Inserted the PX Record Entry';	
				}
			} else {
				$data = $this->request->post;
				$batch_id_sql = "SELECT `batch_id` FROM `oc_manual_punch` ORDER BY `batch_id` DESC LIMIT 1";
				$obatch_ids = $this->db->query($batch_id_sql);
				if($obatch_ids->num_rows == 0){
					$batch_id = 1;
				} else{
					$obatch_id = $obatch_ids->row['batch_id'];
					$batch_id = $obatch_id + 1;
				}
				//echo $batch_id;exit;
				$data['date_from1'] = date('Y-m-d', strtotime($data['date_from1']));
				$data['date_to1'] = date('Y-m-d', strtotime($data['date_to1']));	
				$days = $this->GetDays($data['date_from1'], $data['date_to1']);
				$day = array();
				foreach ($days as $dkeys => $dvalues) {
		        	$dates = explode('-', $dvalues);
		        	$day[$dkeys]['day'] = $dates[2];
		        	$day[$dkeys]['date'] = $dvalues;
		        }
		        $emp_data = $this->model_transaction_transaction->getEmployees_dat($data['e_name_id']);
				if(isset($emp_data['emp_code'])){
					$department = $emp_data['department'];
					$department_id = $emp_data['department_id'];
					$unit = $emp_data['unit'];
					$unit_id = $emp_data['unit_id'];
					$reporting_to = $emp_data['reporting_to'];
				} else {
					$department = '';
					$department_id = '';
					$unit = '';
					$unit_id = '';
					$reporting_to = '';
				}
		        foreach($day as $dkey => $dvalue){
					$filter_date = $dvalue['date'];
					$day_date = date('j', strtotime($filter_date));
					$day = date('j', strtotime($filter_date));
					$month = date('n', strtotime($filter_date));
					$year = date('Y', strtotime($filter_date));

					$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$data['e_name_id']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
					$shift_schedule = $this->db->query($update3)->row;
					$schedule_raw = explode('_', $shift_schedule[$day_date]);
					if(!isset($schedule_raw[2])){
						$schedule_raw[2] = 1;
					}
					$weekly_off = 0;
					$holiday = 0;
					$tour_id = 1;
					$first_half = 'TOUR';
					$second_half = 'TOUR';
					$abnormal_status = 0;
					$present_status = 0;
					$absent_status = 0;
					$leave_status = 0;
					$shift_id = $schedule_raw[1];
					$act_shift_id = $schedule_raw[1];
					$shift_data = $this->model_transaction_transaction->getshiftdata($shift_id);
					$shift_code = $shift_data['shift_code'];
					$shift_intime = $shift_data['in_time'];
					$shift_outtime = $shift_data['out_time'];
					$shift_outtime_flexi = Date('H:i:s', strtotime($shift_data['out_time'] .' +60 minutes'));
					$act_shift_code = $shift_code;
					$act_shift_intime = $shift_intime;
					$act_shift_outtime = $shift_outtime;
					$act_shift_outtime_flexi = $shift_outtime_flexi;
					$act_intime = '00:00:00';
					$act_outtime = '00:00:00';
					$early_time = '00:00:00';
					$late_time = '00:00:00';
					$working_time = '00:00:00';
					$late_mark = '0';
					$early_mark = '0';
					$over_time = '00:00:00';
					$over_time_amount = '0';
					$over_time_esic = '0';
					$after_shift = '0';
					$transaction_id = '0';
					$shift_change = 0;
					$shift_free_change = 0;
					$act_intime_change = 0;
					$act_outtime_change = 0;
					$tran_datas = $this->db->query("SELECT * FROM `oc_manual_punch` WHERE `emp_id` = '".$data['e_name_id']."' AND `date` = '".$filter_date."' ");
					if($tran_datas->num_rows > 0){
						$tran_data = $tran_datas->row;
						$transaction_id = $tran_data['transaction_id'];
						$shift_id = $tran_data['shift_id'];
						$shift_code = $tran_data['shift_code'];
						$shift_intime = $tran_data['shift_intime'];
						$shift_outtime = $tran_data['shift_outtime'];
						$shift_outtime_flexi = Date('H:i:s', strtotime($tran_data['shift_outtime'] .' +60 minutes'));
						$act_shift_id = $shift_id;
						$act_shift_code = $tran_data['act_shift_code'];
						$act_shift_intime = $shift_intime;
						$act_shift_outtime = $shift_outtime;
						$act_shift_outtime_flexi = $shift_outtime_flexi;
						$act_intime = $tran_data['act_intime'];
						$act_outtime = $tran_data['act_outtime'];
						$early_time = $tran_data['early_time'];
						$late_time = $tran_data['late_time'];
						$working_time = $tran_data['working_time'];
						$late_mark = $tran_data['late_mark'];
						$early_mark = $tran_data['early_mark'];
						$over_time = $tran_data['over_time'];
						$over_time_amount = $tran_data['over_time_amount'];
						$over_time_esic = $tran_data['over_time_esic'];
						$after_shift = $tran_data['after_shift'];
						$shift_change = $tran_data['shift_change'];
						$shift_free_change = $tran_data['shift_free_change'];
						$act_intime_change = $tran_data['act_intime_change'];
						$act_outtime_change = $tran_data['act_outtime_change'];
						$sql = "UPDATE `oc_manual_punch` SET `type` = '".$data['type']."', `px_record_type` = '".$data['px_record_type']."', `reporting_to` = '".$reporting_to."', `punch_reason` = '".$data['punch_reason']."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `act_shift_id` = '".$act_shift_id."', `act_shift_code` = '".$act_shift_code."', `act_shift_intime` = '".$shift_intime."', `act_shift_outtime` = '".$shift_outtime."', `act_shift_outtime_flexi` = '".$act_shift_outtime_flexi."', `date` = '".$filter_date."', `date_out` = '".$filter_date."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', `working_time` = '".$working_time."', `over_time` = '".$over_time."', `over_time_amount` = '".$over_time_amount."', `over_time_esic` = '".$over_time_esic."', `after_shift` = '".$after_shift."', `weekly_off` = '".$weekly_off."', `holiday_id` = '".$holiday."', `tour_id` = '".$tour_id."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `abnormal_status` = '".$abnormal_status."', `present_status` = '".$present_status."', `leave_status` = '".$leave_status."', `absent_status` = '".$absent_status."', `manual_status` = '1', `shift_change` = '".$shift_change."', `shift_free_change` = '".$shift_free_change."', `act_intime_change` = '".$act_intime_change."', `act_outtime_change` = '".$act_outtime_change."' WHERE `id` = '".$tran_data['id']."' ";
						// echo $sql;
						// echo '<br />';
						// exit;
						$this->db->query($sql);	
						$this->log->write($sql);	
						$this->session->data['success'] = 'You have Modified the TOUR Entry';
						$id = $data['id'];
					} else {
						$sql = "INSERT INTO `oc_manual_punch` SET `type` = '".$data['type']."', `batch_id` = '".$batch_id."', `px_record_type` = '".$data['px_record_type']."', `reporting_to` = '".$reporting_to."', `dot` = '".date('Y-m-d')."', `punch_reason` = '".$data['punch_reason']."', `transaction_id` = '".$transaction_id."', `emp_id` = '".$data['e_name_id']."', `emp_name` = '".$data['e_name']."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `act_shift_id` = '".$act_shift_id."', `act_shift_code` = '".$act_shift_code."', `act_shift_intime` = '".$shift_intime."', `act_shift_outtime` = '".$shift_outtime."', `act_shift_outtime_flexi` = '".$act_shift_outtime_flexi."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', `working_time` = '".$working_time."', `over_time` = '".$over_time."', `over_time_amount` = '".$over_time_amount."', `over_time_esic` = '".$over_time_esic."', `after_shift` = '".$after_shift."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date."', `date_out` = '".$filter_date."', `weekly_off` = '".$weekly_off."', `holiday_id` = '".$holiday."', `tour_id` = '".$tour_id."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `abnormal_status` = '".$abnormal_status."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', `leave_status` = '".$leave_status."', `manual_status` = '1', `department` = '".$this->db->escape($department)."', department_id = '".$this->db->escape($department_id)."', `unit` = '".$this->db->escape($unit)."', unit_id = '".$this->db->escape($unit_id)."' ";
						// echo $sql;
						// echo '<br />';
						// exit;
						$this->db->query($sql);
						$this->log->write($sql);	
						$id = $this->db->getLastId();
						$this->session->data['success'] = 'You have Inserted the TOUR Entry';	
					}
				}
			}

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['filter_date_to'])) {
				$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
			}

			if (isset($this->request->get['filter_leave_from'])) {
				$url .= '&filter_leave_from=' . $this->request->get['filter_leave_from'];
			}

			if (isset($this->request->get['filter_leave_to'])) {
				$url .= '&filter_leave_to=' . $this->request->get['filter_leave_to'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_proc'])) {
				$url .= '&filter_proc=' . $this->request->get['filter_proc'];
			}

			if (isset($this->request->post['type'])) {
				$url .= '&filter_type=' . $this->request->post['type'];
			}

			if (isset($this->request->get['return_dept'])) {
				$url .= '&return_dept=' . $this->request->get['return_dept'];
			}

			if (isset($this->request->get['filter_px_record_type'])) {
				$url .= '&filter_px_record_type=' . $this->request->get['filter_px_record_type'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			//echo 'out';exit;
			if (isset($this->request->get['return_dept'])) {
				$r_url = $this->url->link('transaction/manualpunch_dept', 'token=' . $this->session->data['token'].$url, 'SSL');
				$this->redirect($r_url);
			} else {
				$r_url = $this->url->link('transaction/manualpunch_ess', 'token=' . $this->session->data['token'].$url, 'SSL');
				$this->redirect($r_url);
			}
		}
		$this->getForm();
	}

	public function delete() {
		$this->language->load('transaction/leave');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			if(isset($this->request->get['type']) && $this->request->get['type'] == 1){
				foreach ($this->request->post['selected'] as $id) {
					$can_leave_sql = "DELETE FROM `oc_manual_punch` WHERE `id` = '".$id."' ";
					$this->db->query($can_leave_sql);
				}
			} else {
				foreach ($this->request->post['selected'] as $batch_id) {
					$can_leave_sql = "DELETE FROM `oc_manual_punch` WHERE `batch_id` = '".$batch_id."' ";
					$this->db->query($can_leave_sql);
				}
			}
			$this->session->data['success'] = 'PX Record Entry Deleted Successfully';

			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['filter_date_to'])) {
				$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
			}

			if (isset($this->request->get['filter_leave_from'])) {
				$url .= '&filter_leave_from=' . $this->request->get['filter_leave_from'];
			}

			if (isset($this->request->get['filter_leave_to'])) {
				$url .= '&filter_leave_to=' . $this->request->get['filter_leave_to'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_proc'])) {
				$url .= '&filter_proc=' . $this->request->get['filter_proc'];
			}

			if (isset($this->request->get['filter_px_record_type'])) {
				$url .= '&filter_px_record_type=' . $this->request->get['filter_px_record_type'];
			}

			if (isset($this->request->get['filter_type'])) {
				$url .= '&filter_type=' . $this->request->get['filter_type'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('transaction/manualpunch_ess', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		} elseif(isset($this->request->get['id']) && $this->validateDelete()){
			$can_leave_sql = "DELETE FROM `oc_manual_punch` WHERE `id` = '".$this->request->get['id']."' ";
			$this->db->query($can_leave_sql);
			$this->session->data['success'] = 'PX Record Entry Deleted Successfully';

			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['filter_date_to'])) {
				$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
			}

			if (isset($this->request->get['filter_leave_from'])) {
				$url .= '&filter_leave_from=' . $this->request->get['filter_leave_from'];
			}

			if (isset($this->request->get['filter_leave_to'])) {
				$url .= '&filter_leave_to=' . $this->request->get['filter_leave_to'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_proc'])) {
				$url .= '&filter_proc=' . $this->request->get['filter_proc'];
			}

			if (isset($this->request->get['filter_px_record_type'])) {
				$url .= '&filter_px_record_type=' . $this->request->get['filter_px_record_type'];
			}

			if (isset($this->request->get['filter_type'])) {
				$url .= '&filter_type=' . $this->request->get['filter_type'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('transaction/manualpunch_ess', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		} elseif(isset($this->request->get['batch_id']) && $this->validateDelete()){
			$can_leave_sql = "DELETE FROM `oc_manual_punch` WHERE `batch_id` = '".$this->request->get['batch_id']."' ";
			$this->db->query($can_leave_sql);
			$this->session->data['success'] = 'TOUR Entry Deleted Successfully';

			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['filter_date_to'])) {
				$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
			}

			if (isset($this->request->get['filter_leave_from'])) {
				$url .= '&filter_leave_from=' . $this->request->get['filter_leave_from'];
			}

			if (isset($this->request->get['filter_leave_to'])) {
				$url .= '&filter_leave_to=' . $this->request->get['filter_leave_to'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_proc'])) {
				$url .= '&filter_proc=' . $this->request->get['filter_proc'];
			}

			if (isset($this->request->get['filter_px_record_type'])) {
				$url .= '&filter_px_record_type=' . $this->request->get['filter_px_record_type'];
			}

			if (isset($this->request->get['filter_type'])) {
				$url .= '&filter_type=' . $this->request->get['filter_type'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('transaction/manualpunch_ess', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code'])){
			$emp_name = $this->db->query("SELECT CONCAT_WS(' ',first_name,last_name) AS name FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} elseif(isset($this->session->data['d_emp_id'])){
			$emp_name = $this->db->query("SELECT CONCAT_WS(' ',first_name,last_name) AS name FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['d_emp_id']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code'])){
			$filter_name_id = $this->session->data['emp_code'];
		} elseif(isset($this->session->data['d_emp_id'])){
			$filter_name_id = $this->session->data['d_emp_id'];
		}  else {
			$filter_name_id = '';
		}

		if($filter_name_id != ''){
			$filter_dept = $this->db->query("SELECT `department_id` FROM `oc_employee` WHERE `emp_code` = '".$filter_name_id."' ")->row['department_id'];
			$filter_dept_name = $this->db->query("SELECT `department` FROM `oc_employee` WHERE `emp_code` = '".$filter_name_id."' ")->row['department'];
		} else {
			$filter_dept = '';
			$filter_dept_name = '';
		}

		if($filter_name_id != ''){
			$filter_unit = $this->db->query("SELECT `unit_id` FROM `oc_employee` WHERE `emp_code` = '".$filter_name_id."' ")->row['unit_id'];
			$filter_unit_name = $this->db->query("SELECT `unit` FROM `oc_employee` WHERE `emp_code` = '".$filter_name_id."' ")->row['unit'];
		} else {
			$filter_unit = '';
			$filter_unit_name = '';
		}

		//echo $filter_unit;exit;

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$year = date('Y');
			$month = date('n');
			if($month == 1){
				$prev_month = 12;
				$prev_year = $year - 1;
			} else {
				$prev_month = $month - 1;
				$prev_year = $year;
			}
			//$filter_date = sprintf("%04d-%02d-%02d", $prev_year, $prev_month, '26');
			$filter_date = '';
		}

		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		} else {
			$filter_date_to = '';
		}

		if (isset($this->request->get['filter_leave_from'])) {
			$filter_leave_from = $this->request->get['filter_leave_from'];
		} else {
			$year = date('Y');
			$month = date('n');
			if($month == 1){
				$prev_month = 12;
				$prev_year = $year - 1;
			} else {
				$prev_month = $month - 1;
				$prev_year = $year;
			}
			$filter_leave_from = sprintf("%04d-%02d-%02d", $prev_year, $prev_month, '26');
		}

		if (isset($this->request->get['filter_leave_to'])) {
			$filter_leave_to = $this->request->get['filter_leave_to'];
		} else {
			$filter_leave_to = '';
		}

		if (isset($this->request->get['filter_approval_1'])) {
			$filter_approval_1 = $this->request->get['filter_approval_1'];
		} else {
			$filter_approval_1 = '0';
		}

		if (isset($this->request->get['filter_proc'])) {
			$filter_proc = $this->request->get['filter_proc'];
		} else {
			$filter_proc = '0';
		}

		if (isset($this->request->get['filter_type'])) {
			$filter_type = $this->request->get['filter_type'];
		} else {
			$filter_type = '1';
		}

		if (isset($this->request->get['filter_px_record_type'])) {
			$filter_px_record_type = $this->request->get['filter_px_record_type'];
		} else {
			$filter_px_record_type = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		if (isset($this->request->get['filter_date_to'])) {
			$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
		}

		if (isset($this->request->get['filter_leave_from'])) {
			$url .= '&filter_leave_from=' . $this->request->get['filter_leave_from'];
		}

		if (isset($this->request->get['filter_leave_to'])) {
			$url .= '&filter_leave_to=' . $this->request->get['filter_leave_to'];
		}

		if (isset($this->request->get['filter_approval_1'])) {
			$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
		}

		if (isset($this->request->get['filter_proc'])) {
			$url .= '&filter_proc=' . $this->request->get['filter_proc'];
		}

		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}

		if (isset($this->request->get['filter_px_record_type'])) {
			$url .= '&filter_px_record_type=' . $this->request->get['filter_px_record_type'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('transaction/manualpunch_ess', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$url_insert = '&filter_name_id='.$filter_name_id.'&filter_name_id_1='.$filter_name_id.'&filter_name='.$filter_name;

		$this->data['insert'] = $this->url->link('transaction/manualpunch_ess/update', 'token=' . $this->session->data['token'] . $url . $url_insert, 'SSL');
		$this->data['delete'] = $this->url->link('transaction/manualpunch_ess/delete', 'token=' . $this->session->data['token'] . $url . $url_insert, 'SSL');
		
		$this->data['manual_list'] = array();
		$employee_total = 0;

		$data = array(
			'filter_name' => $filter_name,
			'filter_name_id' => $filter_name_id,
			'filter_date' => $filter_date,
			'filter_date_to' => $filter_date_to,
			'filter_leave_from' => $filter_leave_from,
			'filter_leave_to' => $filter_leave_to,
			'filter_unit' => $filter_unit,
			'filter_approval_1' => $filter_approval_1,
			'filter_proc' => $filter_proc,
			'filter_px_record_type' => $filter_px_record_type,
			'filter_type' => $filter_type,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		
		if($filter_type == '1'){
			$employee_total = $this->model_transaction_transaction->getTotalmanualtransaction_ess($data);
			$results = $this->model_transaction_transaction->getmanualTransaction_ess($data);
			foreach ($results as $result) {
				$action = array();
				if($result['reject_date'] == '0000-00-00'){
					if($result['p_status'] == 1){
						
					} else {
						$action[] = array(
							'text' => 'Edit',
							'href' => $this->url->link('transaction/manualpunch_ess/update', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . '&transaction_id=' . $result['transaction_id'] . $url, 'SSL')
						);
						$action[] = array(
							'text' => 'Delete',
							'href' => $this->url->link('transaction/manualpunch_ess/delete', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, 'SSL')
						);	
					}
				} else {
					$action[] = array(
						'text' => 'Delete',
						'href' => $this->url->link('transaction/manualpunch_ess/delete', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, 'SSL')
					);
				}

				

				$emp_data = $this->model_transaction_transaction->getempdata($result['emp_id']);

				if($result['approval_1'] == '1'){
					if($result['approval_date'] != '0000-00-00 00:00:00'){
						$approval_1 = 'Approved on ' . date('d-m-y h:i:s', strtotime($result['approval_date']));
					} else {
						$approval_1 = 'Approved';
					}
				} else if($result['approval_1'] == '2'){
					$approval_1 = 'Rejected';
				} else {
					$approval_1 = 'Pending';
				}

				
				if($result['p_status'] == '1'){
					$proc_stat = 'Processed';
				} else {
					$proc_stat = 'UnProcessed';
				}

				if($result['reject_date'] != '0000-00-00'){
					if($result['reject_reason'] != ''){
						$reject_reason = $result['reject_reason'].' - as on '.date('d-m-y', strtotime($result['reject_date']));
					} else {
						$reject_reason = 'as on '.date('d-m-y', strtotime($result['reject_date']));
					}
				} else {
					$reject_reason = '';
				}

				$px_record_type = '';
				$modification = '';
				if($result['px_record_type'] == 1){
					$modification = $result['act_outtime_old'];
					$px_record_type = 'Early Modification';
				} elseif($result['px_record_type'] == 2){
					$modification = $result['act_intime_old'];
					$px_record_type = 'Late Modification';
				} elseif($result['px_record_type'] == 3){
					$modification = $result['act_intime_old'].' - '.$result['act_outtime_old'];
					$px_record_type = 'Other Modification';
				}

				if($result['type'] == '1'){
					$type = 'PX Record';
				} else {
					$type = 'TOUR';
				}

				$this->data['manual_list'][] = array(
					'id' => $result['id'],
					'emp_id' => $result['emp_id'],
					'name' => $emp_data['emp_name'],
					'dot' => date('d-m-y', strtotime($result['dot'])),
					'date' => date('d-m-y', strtotime($result['date'])),
					'in_time' => $result['act_intime'],
					'out_time' => $result['act_outtime'],
					'modification' => $modification,
					'approval_1' => $approval_1,
					'reject_reason' => $reject_reason,
					'proc_stat' => $proc_stat,
					'px_record_type' => $px_record_type,
					'type' => $type,
					'unit' => $result['unit'],
					'dept_name' => $result['department'],
					'punch_reason' => $result['punch_reason'],
					'selected'        => isset($this->request->post['selected']) && in_array($result['id'], $this->request->post['selected']),
					'action'          => $action
				);
			}
		} else {
			$data['group_by'] = 1;
			$employee_total = $this->model_transaction_transaction->getTotalmanualtransaction_ess($data);
			$results = $this->model_transaction_transaction->getmanualTransaction_ess($data);
			foreach ($results as $result) {
				$action = array();
				if($result['reject_date'] == '0000-00-00'){
					if($result['p_status'] == 1){
						
					} else {
						$action[] = array(
							'text' => 'Edit',
							'href' => $this->url->link('transaction/manualpunch_ess/update', 'token=' . $this->session->data['token'] . '&batch_id=' . $result['batch_id'] . '&transaction_id=' . $result['transaction_id'] . $url, 'SSL')
						);
						$action[] = array(
							'text' => 'Delete',
							'href' => $this->url->link('transaction/manualpunch_ess/delete', 'token=' . $this->session->data['token'] . '&batch_id=' . $result['batch_id'] . $url, 'SSL')
						);	
					}
				} else {
					$action[] = array(
						'text' => 'Delete',
						'href' => $this->url->link('transaction/manualpunch_ess/delete', 'token=' . $this->session->data['token'] . '&batch_id=' . $result['batch_id'] . $url, 'SSL')
					);
				}

				

				$emp_data = $this->model_transaction_transaction->getempdata($result['emp_id']);

				if($result['approval_1'] == '1'){
					if($result['approval_date'] != '0000-00-00 00:00:00'){
						$approval_1 = 'Approved on ' . date('d-m-y h:i:s', strtotime($result['approval_date']));
					} else {
						$approval_1 = 'Approved';
					}
				} else if($result['approval_1'] == '2'){
					$approval_1 = 'Rejected';
				} else {
					$approval_1 = 'Pending';
				}

				
				if($result['p_status'] == '1'){
					$proc_stat = 'Processed';
				} else {
					$proc_stat = 'UnProcessed';
				}

				if($result['reject_date'] != '0000-00-00'){
					if($result['reject_reason'] != ''){
						$reject_reason = $result['reject_reason'].' - as on '.date('d-m-y', strtotime($result['reject_date']));
					} else {
						$reject_reason = 'as on '.date('d-m-y', strtotime($result['reject_date']));
					}
				} else {
					$reject_reason = '';
				}

				if($result['type'] == '1'){
					$type = 'PX Record';
				} else {
					$type = 'TOUR';
				}

				$date_from = $this->model_transaction_transaction->gettour_from_ess($result['batch_id']);
				$date_to = $this->model_transaction_transaction->gettour_to_ess($result['batch_id']);

				$this->data['manual_list'][] = array(
					'batch_id' => $result['batch_id'],
					'id' => $result['id'],
					'emp_id' => $result['emp_id'],
					'name' => $result['emp_name'],
					'dot' => date('d-m-y', strtotime($result['dot'])),
					'date_from' => date('d-m-y', strtotime($date_from)),
					'date_to' => date('d-m-y', strtotime($date_to)),
					'approval_1' => $approval_1,
					'reject_reason' => $reject_reason,
					'proc_stat' => $proc_stat,
					'type' => $type,
					'unit' => $result['unit'],
					'dept_name' => $result['department'],
					'punch_reason' => $result['punch_reason'],
					'selected'        => isset($this->request->post['selected']) && in_array($result['id'], $this->request->post['selected']),
					'action'          => $action
				);
			}
		}

		// echo '<pre>';
		// print_r($this->data['leaves']);
		// exit;

		$locations = $this->db->query("SELECT * FROM `oc_unit` ")->rows;
		$unit_data['0'] = 'All'; 
		foreach($locations as $lkey => $lvalue){
			$unit_data[$lvalue['unit_id']] = $lvalue['unit'];
		}
		$this->data['unit_data'] = $unit_data;

		$approves = array(
			'0' => 'All',
			'1' => 'Pending',
			'2' => 'Approved',
			'3' => 'Rejected'
		);
		$this->data['approves'] = $approves;

		$process = array(
			'0' => 'All',
			'1' => 'UnProcessed',
			'2' => 'Processed',
		);
		$this->data['process'] = $process;

		$this->data['px_record_types'] = array(
			'0' => 'All',
			'2' => 'Late Modification',
			'1' => 'Early Modification',
			'3' => 'Other Modification',
		);

		$this->data['types'] = array(
			'1' => 'PX Record',
			'2' => 'TOUR',
		);

		$this->data['token'] = $this->session->data['token'];	

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_delete'] = $this->language->get('text_delete');

		$this->data['button_insert'] = $this->language->get('button_insert');

		$this->data['button_filter'] = $this->language->get('button_filter');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		// if (isset($this->request->get['page'])) {
		// 	$url .= '&page=' . $this->request->get['page'];
		// }

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		if (isset($this->request->get['filter_date_to'])) {
			$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
		}

		if (isset($this->request->get['filter_leave_from'])) {
			$url .= '&filter_leave_from=' . $this->request->get['filter_leave_from'];
		}

		if (isset($this->request->get['filter_leave_to'])) {
			$url .= '&filter_leave_to=' . $this->request->get['filter_leave_to'];
		}

		if (isset($this->request->get['filter_approval_1'])) {
			$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
		}

		if (isset($this->request->get['filter_proc'])) {
			$url .= '&filter_proc=' . $this->request->get['filter_proc'];
		}

		if (isset($this->request->get['filter_px_record_type'])) {
			$url .= '&filter_px_record_type=' . $this->request->get['filter_px_record_type'];
		}

		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}

		$pagination = new Pagination();
		$pagination->total = $employee_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/manualpunch_ess', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_date'] = $filter_date;
		$this->data['filter_date_to'] = $filter_date_to;
		$this->data['filter_leave_from'] = $filter_leave_from;
		$this->data['filter_leave_to'] = $filter_leave_to;
		$this->data['filter_approval_1'] = $filter_approval_1;
		$this->data['filter_proc'] = $filter_proc;
		$this->data['filter_dept'] = $filter_dept;
		$this->data['filter_dept_name'] = $filter_dept_name;
		$this->data['filter_unit'] = $filter_unit;
		$this->data['filter_unit_name'] = $filter_unit_name;
		$this->data['filter_px_record_type'] = $filter_px_record_type;
		$this->data['filter_type'] = $filter_type;
		
		$this->template = 'transaction/manualpunch_ess_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['tab_general'] = $this->language->get('tab_general');

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {	
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['e_name'])) {
			$this->data['error_employee'] = $this->error['e_name'];
		} else {
			$this->data['error_employee'] = '';
		}

		if (isset($this->error['dot'])) {
			$this->data['error_dot'] = $this->error['dot'];
		} else {
			$this->data['error_dot'] = '';
		}

		if (isset($this->error['shift_id'])) {
			$this->data['error_shift_id'] = $this->error['shift_id'];
		} else {
			$this->data['error_shift_id'] = '';
		}

		if (isset($this->error['px_record_type'])) {
			$this->data['error_px_record_type'] = $this->error['px_record_type'];
		} else {
			$this->data['error_px_record_type'] = '';
		}

		if (isset($this->error['type'])) {
			$this->data['error_type'] = $this->error['type'];
		} else {
			$this->data['error_type'] = '';
		}

		if (isset($this->error['punch_reason'])) {
			$this->data['error_punch_reason'] = $this->error['punch_reason'];
		} else {
			$this->data['error_punch_reason'] = '';
		}

		

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('transaction/manualpunch_ess', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		if (isset($this->request->get['filter_date_to'])) {
			$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
		}

		if (isset($this->request->get['filter_leave_from'])) {
			$url .= '&filter_leave_from=' . $this->request->get['filter_leave_from'];
		}

		if (isset($this->request->get['filter_leave_to'])) {
			$url .= '&filter_leave_to=' . $this->request->get['filter_leave_to'];
		}

		if (isset($this->request->get['filter_approval_1'])) {
			$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
		}

		if (isset($this->request->get['filter_proc'])) {
			$url .= '&filter_proc=' . $this->request->get['filter_proc'];
		}

		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['return_dept'])) {
			$url .= '&return_dept=' . $this->request->get['return_dept'];
		}

		if (isset($this->request->get['filter_px_record_type'])) {
			$url .= '&filter_px_record_type=' . $this->request->get['filter_px_record_type'];
		}

		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		
		$this->data['action'] = $this->url->link('transaction/manualpunch_ess/update', 'token=' . $this->session->data['token'].$url, 'SSL');
		if (isset($this->request->get['return_dept'])) {
			$this->data['cancel'] = $this->url->link('transaction/manualpunch_dept', 'token=' . $this->session->data['token'].$url, 'SSL');
		} else {
			$this->data['cancel'] = $this->url->link('transaction/manualpunch_ess', 'token=' . $this->session->data['token'].$url, 'SSL');
		}
		
		$transaction_data = array();
		$manual_data = array();
		if (isset($this->request->get['transaction_id']) && isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$transaction_data = $this->model_transaction_transaction->gettransaction_data($this->request->get['transaction_id']);
			$manual_datas = $this->db->query("SELECT * FROM `oc_manual_punch` WHERE `id` = '".$this->request->get['id']."' ");
			if($manual_datas->num_rows > 0){
				$manual_data = $manual_datas->rows;
			} else {
				$manual_data = array();
			}
		} elseif (isset($this->request->get['transaction_id']) && isset($this->request->get['batch_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$transaction_data = $this->model_transaction_transaction->gettransaction_data($this->request->get['transaction_id']);
			$manual_datas = $this->db->query("SELECT * FROM `oc_manual_punch` WHERE `batch_id` = '".$this->request->get['batch_id']."' ORDER BY `date` ASC LIMIT 1");
			if($manual_datas->num_rows > 0){
				$manual_data = $manual_datas->rows;
			} else {
				$manual_data = array();
			}
		} elseif (isset($this->request->get['transaction_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$transaction_data = $this->model_transaction_transaction->gettransaction_data($this->request->get['transaction_id']);
			$manual_data = array();
		} elseif (isset($this->request->get['filter_name_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$manual_datas = $this->db->query("SELECT * FROM `oc_manual_punch` WHERE `date` = '".date('Y-m-d')."' AND `emp_id` = '".$this->request->get['filter_name_id']."' ");
			if($manual_datas->num_rows > 0){
				$manual_data = $manual_datas->rows;
			} else {
				$transaction_datas = $this->db->query("SELECT * FROM `oc_transaction` WHERE `date` = '".date('Y-m-d')."' AND `emp_id` = '".$this->request->get['filter_name_id']."' ");
				if($transaction_datas->num_rows > 0){
					$transaction_data = $transaction_datas->rows;
				}
			}
			// echo '<pre>';
			// print_r($transaction_data);
			// exit;
		}
		$this->data['token'] = $this->session->data['token'];

		$statuses[1]['stat_id'] = '0';
		$statuses[1]['stat_name'] = 'Absent';
		$statuses[2]['stat_id'] = '1';
		$statuses[2]['stat_name'] = 'Present';
		$statuses[3]['stat_id'] = 'WO';
		$statuses[3]['stat_name'] = 'Weekly Off';
		$statuses[4]['stat_id'] = 'HD';
		$statuses[4]['stat_name'] = 'Half Day';
		$statuses[5]['stat_id'] = 'HLD';
		$statuses[5]['stat_name'] = 'Holiday';
		$statuses[6]['stat_id'] = 'TOUR';
		$statuses[6]['stat_name'] = 'TOUR';
		$statuses[7]['stat_id'] = 'OD';
		$statuses[7]['stat_name'] = 'On Duty';
		$statuses[8]['stat_id'] = 'PL';
		$statuses[8]['stat_name'] = 'PL';
		$statuses[9]['stat_id'] = 'CL';
		$statuses[9]['stat_name'] = 'CL';
		$statuses[10]['stat_id'] = 'SL';
		$statuses[10]['stat_name'] = 'SL';
		$statuses[11]['stat_id'] = 'CL';
		$statuses[11]['stat_name'] = 'CL';
		$this->data['statuses'] = $statuses;

		$datas['filter_name'] = '';

		if (isset($this->request->post['e_name'])) {
			$this->data['e_name'] = $this->request->post['e_name'];
			$this->data['e_name_id'] = $this->request->post['e_name_id'];
			$this->data['emp_code'] = $this->request->post['e_name_id'];
		} elseif (isset($manual_data[0]['emp_name'])) {
			$this->data['e_name'] = $manual_data[0]['emp_name'];
			$this->data['e_name_id'] = $manual_data[0]['emp_id'];
			$this->data['emp_code'] = $manual_data[0]['emp_id'];
		} elseif (isset($transaction_data[0]['emp_name'])) {
			$emp_names = $this->db->query("SELECT CONCAT_WS(' ',first_name,last_name) AS emp_name FROM `oc_employee` WHERE `emp_code` = '".$transaction_data[0]['emp_id']."' ");
			$emp_name = '';
			if($emp_names->num_rows > 0){
				$emp_name = $emp_names->row['emp_name'];
			}

			$this->data['e_name'] = $emp_name;
			$this->data['e_name_id'] = $transaction_data[0]['emp_id'];
			$this->data['emp_code'] = $transaction_data[0]['emp_id'];
		
		} else {	
			if(isset($this->request->get['filter_name_id'])){
				$this->data['e_name'] = $this->request->get['filter_name'];
				$this->data['e_name_id'] = $this->request->get['filter_name_id'];
				$this->data['emp_code'] = $this->request->get['filter_name_id'];	
			} else {
				$this->data['e_name'] = '';
				$this->data['e_name_id'] = '';
				$this->data['emp_code'] = '';	
			}
			
		}

		$day = date('j');
		$month = date('n');
		$year = date('Y');
		$assi_shift_datas = $this->db->query("SELECT `".$day."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$this->data['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
		$assi_shift_id = 0;
		if($assi_shift_datas->num_rows > 0){
			$assi_shift_ids = $assi_shift_datas->row[$day];
			$assi_shift_id_exp = explode('_', $assi_shift_ids);
			if(isset($assi_shift_id_exp[2])){
				$assi_shift_id = $assi_shift_id_exp[2];
			} else {
				$assi_shift_id = $assi_shift_id_exp[1];
			}
		}

		$shift_data = array();
		$shift_datas = $this->model_catalog_shift->getshifts($datas);
		$shift_data[1]['shift_id'] = 'WO';
		$shift_data[1]['shift_name'] = 'Weekly Off';

		$shift_data[2]['shift_id'] = 'HLD';
		$shift_data[2]['shift_name'] = 'Holiday';

		$shift_data[3]['shift_id'] = 'HD';
		$shift_data[3]['shift_name'] = 'Half Day';

		$shift_data[4]['shift_id'] = 'TOUR';
		$shift_data[4]['shift_name'] = 'TOUR';
		$ckey = 6;
		//echo $assi_shift_id;exit;
		$shift_intime = '00:00:00';
		$shift_outtime = '00:00:00';
		foreach ($shift_datas as $skey => $svalue) {
			if($assi_shift_id == $svalue['shift_id']){
				$shift_data[$ckey]['shift_id'] = intval($svalue['shift_id']);
				$shift_data[$ckey]['shift_name'] = $svalue['name'];
				$shift_intime = $svalue['in_time'];
				$shift_outtime = $svalue['out_time'];
				$ckey ++;
			}
		}
		$this->data['shift_data'] = $shift_data;
		// echo '<pre>';
		// print_r($shift_data);
		// exit;

		if (isset($this->request->post['insert'])) {
			$this->data['insert'] = $this->request->post['insert'];
		} elseif(isset($manual_data[0]['id'])) {
			$this->data['insert'] = 0;
		} else {
			$this->data['insert'] = '';
		}

		if (isset($this->request->post['transaction_id'])) {
			$this->data['transaction_id'] = $this->request->post['transaction_id'];
		} elseif(isset($manual_data[0]['transaction_id'])) {
			$this->data['transaction_id'] = $manual_data[0]['transaction_id'];
		} elseif(isset($transaction_data[0]['transaction_id'])) {
			$this->data['transaction_id'] = $transaction_data[0]['transaction_id'];
		} else {	
			$this->data['transaction_id'] = 0;
		}

		if (isset($this->request->post['punch_reason'])) {
			$this->data['punch_reason'] = $this->request->post['punch_reason'];
		} elseif(isset($manual_data[0]['punch_reason'])) {
			$this->data['punch_reason'] = $manual_data[0]['punch_reason'];
		} else {	
			$this->data['punch_reason'] = '';
		}

		if (isset($this->request->post['type'])) {
			$this->data['type'] = $this->request->post['type'];
		} elseif(isset($manual_data[0]['type'])) {
			$this->data['type'] = $manual_data[0]['type'];
		} else {	
			$this->data['type'] = '';
		}
		//echo $this->data['type'];exit;

		$this->data['types'] = array(
			'1' => 'PX Record',
			'2' => 'TOUR',
		);

		if (isset($this->request->post['px_record_type'])) {
			$this->data['px_record_type'] = $this->request->post['px_record_type'];
		} elseif(isset($manual_data[0]['px_record_type'])) {
			$this->data['px_record_type'] = $manual_data[0]['px_record_type'];
		} else {	
			$this->data['px_record_type'] = '';
		}
		//echo $this->data['px_record_type'];exit;

		$this->data['px_record_types'] = array(
			'2' => 'Late Modification',
			'1' => 'Early Modification',
			'3' => 'Other Modification',
		);

		if (isset($this->request->post['id'])) {
			$this->data['id'] = $this->request->post['id'];
		} elseif(isset($manual_data[0]['id'])) {
			$this->data['id'] = $manual_data[0]['id'];
		} else {	
			$this->data['id'] = 0;
		}

		if (isset($this->request->post['dot'])) {
			$this->data['dot'] = $this->request->post['dot'];
		} elseif (isset($manual_data[0]['date'])) {
			$this->data['dot'] = date('Y-m-d', strtotime($manual_data[0]['date']));
		} elseif (isset($transaction_data[0]['date'])) {
			$this->data['dot'] = date('Y-m-d', strtotime($transaction_data[0]['date']));
		} else {	
			$this->data['dot'] = date('Y-m-d');
		}
		$this->data['date_from'] = $this->data['dot'];

		if (isset($this->request->post['date_to'])) {
			$this->data['date_to'] = $this->request->post['date_to'];
		} elseif (isset($manual_data[0]['date_out'])) {
			$this->data['date_to'] = date('Y-m-d', strtotime($manual_data[0]['date_out']));
		} elseif (isset($transaction_data[0]['date_out'])) {
			$this->data['date_to'] = date('Y-m-d', strtotime($transaction_data[0]['date_out']));
		} else {	
			$this->data['date_to'] = date('Y-m-d');
		}

		if (isset($this->request->post['date_from1'])) {
			$this->data['date_from1'] = $this->request->post['date_from1'];
		} elseif (isset($manual_data[0]['date'])) {
			$this->data['date_from1'] = date('Y-m-d', strtotime($manual_data[0]['date']));
		} else {	
			$this->data['date_from1'] = date('Y-m-d');
		}

		if (isset($this->request->post['date_to1'])) {
			$this->data['date_to1'] = $this->request->post['date_to1'];
		} elseif (isset($manual_data[0]['date_out'])) {
			$date_to = $this->model_transaction_transaction->gettour_to_ess($manual_data[0]['batch_id']);
			$this->data['date_to1'] = date('Y-m-d', strtotime($date_to));
		} else {	
			$this->data['date_to1'] = date('Y-m-d');
		}

		if (isset($this->request->post['shift_id'])) {
			$this->data['shift_id'] = intval($this->request->post['shift_id']);
			$this->data['shift_intime'] = $this->request->post['shift_intime']; 
			$this->data['shift_outtime'] = $this->request->post['shift_outtime']; 
			$this->data['actual_intime'] = $this->request->post['actual_intime']; 
			$this->data['actual_outtime'] = $this->request->post['actual_outtime']; 
			$this->data['firsthalf_status'] = $this->request->post['firsthalf_status']; 
			$this->data['secondhalf_status'] = $this->request->post['secondhalf_status']; 
		} elseif (isset($manual_data[0]['emp_name'])) {
			$today_date = date('j', strtotime($manual_data[0]['date']));
			$month = date('n', strtotime($manual_data[0]['date']));
			$year = date('Y', strtotime($manual_data[0]['date']));
			$shift_data = $this->model_catalog_employee->getshift_id($manual_data[0]['emp_id'], $today_date, $month, $year);
			$shift_ids = explode('_', $shift_data);
			$shift_intime = '';
			$shift_outtime = '';
			$act_intime = '';
			$act_outtime = '';
			$shift_id = 0;

			if($manual_data[0]['weekly_off'] != '0'){
				$shift_id = 'WO';
				$shift_intime = $manual_data[0]['shift_intime'];
				$shift_outtime = $manual_data[0]['shift_outtime'];
				$act_intime = $manual_data[0]['act_intime'];
				$act_outtime = $manual_data[0]['act_outtime'];
			} elseif($manual_data[0]['holiday_id'] != '0'){
				$shift_id = 'HLD';
				$shift_intime = $manual_data[0]['shift_intime'];
				$shift_outtime = $manual_data[0]['shift_outtime'];
				$act_intime = $manual_data[0]['act_intime'];
				$act_outtime = $manual_data[0]['act_outtime'];
			} elseif($manual_data[0]['halfday_status'] != '0'){
				$shift_id = 'HD';
				$shift_intime = $manual_data[0]['shift_intime'];
				$shift_outtime = $manual_data[0]['shift_outtime'];
				$act_intime = $manual_data[0]['act_intime'];
				$act_outtime = $manual_data[0]['act_outtime'];
			} elseif($manual_data[0]['tour_id'] != '0'){
				$shift_id = 'TOUR';
				$shift_intime = $manual_data[0]['shift_intime'];
				$shift_outtime = $manual_data[0]['shift_outtime'];
				$act_intime = $manual_data[0]['act_intime'];
				$act_outtime = $manual_data[0]['act_outtime'];
			} elseif($manual_data[0]['compli_status'] != '0'){
				$shift_id = 'COF';
				$shift_intime = $manual_data[0]['shift_intime'];
				$shift_outtime = $manual_data[0]['shift_outtime'];
				$act_intime = $manual_data[0]['act_intime'];
				$act_outtime = $manual_data[0]['act_outtime'];
			} elseif($manual_data[0]['on_duty'] != '0'){
				$shift_id = 'OD';
				$shift_intime = $manual_data[0]['shift_intime'];
				$shift_outtime = $manual_data[0]['shift_outtime'];
				$act_intime = $manual_data[0]['act_intime'];
				$act_outtime = $manual_data[0]['act_outtime'];
			} else {
				$shift_datas_sql = "SELECT * FROM `oc_shift` WHERE `in_time` = '".$manual_data[0]['shift_intime']."' AND `out_time` = '".$manual_data[0]['shift_outtime']."' ";
				$shift_data_datas = $this->db->query($shift_datas_sql);
				$shift_id = 0;
				if($shift_data_datas->num_rows > 0){
					$shift_id = intval($shift_data_datas->row['shift_id']);
				}
				$shift_intime = $manual_data[0]['shift_intime'];
				$shift_outtime = $manual_data[0]['shift_outtime'];
				$act_intime = $manual_data[0]['act_intime'];
				$act_outtime = $manual_data[0]['act_outtime'];
			}
			$this->data['shift_id'] = $shift_id;
			$this->data['shift_intime'] = $shift_intime;
			$this->data['shift_outtime'] = $shift_outtime;
			$this->data['actual_intime'] = $act_intime;
			$this->data['actual_outtime'] = $act_outtime;
			$this->data['firsthalf_status'] = $manual_data[0]['firsthalf_status'];
			$this->data['secondhalf_status'] = $manual_data[0]['secondhalf_status'];
			
			if($shift_id == 'WO'){
				$this->data['firsthalf_status'] = 'WO';
				$this->data['secondhalf_status'] = 'WO';
			} elseif($shift_id == 'HLD'){
				$this->data['firsthalf_status'] = 'HLD';
				$this->data['secondhalf_status'] = 'HLD';
			} elseif($shift_id == 'HD'){
				$this->data['firsthalf_status'] = 'HD';
				$this->data['secondhalf_status'] = 'HD';
			} elseif($shift_id == 'TOUR'){
				$this->data['firsthalf_status'] = 'TOUR';
				$this->data['secondhalf_status'] = 'TOUR';
			} elseif($shift_id == 'COF'){
				$this->data['firsthalf_status'] = 'COF';
				$this->data['secondhalf_status'] = 'COF';
			} elseif($shift_id == 'OD'){
				$this->data['firsthalf_status'] = 'OD';
				$this->data['secondhalf_status'] = 'OD';
			}

			$leave_data_sql = "SELECT `leave_type`, `type` FROM `oc_leave_transaction` WHERE `emp_id` = '".$this->data['e_name_id']."' AND `date` = '".$this->data['dot']."' AND `p_status` = '1' AND `a_status` = '1' ";
			$leave_data_res = $this->db->query($leave_data_sql);
			$leave_in = 0;
			if($leave_data_res->num_rows > 0){
				//$shift_inn = '1';
				if($leave_data_res->row['type'] == ''){
					$leave_in = 1;
					$first_half = $leave_data_res->row['leave_type'];
					$second_half = $leave_data_res->row['leave_type'];
				} else {
					if($leave_data_res->row['type'] == 'F'){
						$leave_in = 1;
						$this->data['firsthalf_status'] = $leave_data_res->row['leave_type'];
						$this->data['secondhalf_status'] = $leave_data_res->row['leave_type'];
					} elseif($leave_data_res->row['type'] == '1'){
						$leave_in = 2;
						$this->data['firsthalf_status'] = $leave_data_res->row['leave_type'];
					} elseif($leave_data_res->row['type'] == '2'){
						$leave_in = 3;
						$this->data['secondhalf_status'] = $leave_data_res->row['leave_type'];
					}
				}
			}
		} elseif (isset($transaction_data[0]['emp_name'])) {
			$today_date = date('j', strtotime($transaction_data[0]['date']));
			$month = date('n', strtotime($transaction_data[0]['date']));
			$year = date('Y', strtotime($transaction_data[0]['date']));
			$shift_data = $this->model_catalog_employee->getshift_id($transaction_data[0]['emp_id'], $today_date, $month, $year);
			$shift_ids = explode('_', $shift_data);
			$shift_intime = '';
			$shift_outtime = '';
			$act_intime = '';
			$act_outtime = '';
			$shift_id = 0;

			if($transaction_data[0]['weekly_off'] != '0'){
				$shift_id = 'WO';
				$shift_intime = $transaction_data[0]['shift_intime'];
				$shift_outtime = $transaction_data[0]['shift_outtime'];
				$act_intime = $transaction_data[0]['act_intime'];
				$act_outtime = $transaction_data[0]['act_outtime'];
			} elseif($transaction_data[0]['holiday_id'] != '0'){
				$shift_id = 'HLD';
				$shift_intime = $transaction_data[0]['shift_intime'];
				$shift_outtime = $transaction_data[0]['shift_outtime'];
				$act_intime = $transaction_data[0]['act_intime'];
				$act_outtime = $transaction_data[0]['act_outtime'];
			} elseif($transaction_data[0]['halfday_status'] != '0'){
				$shift_id = 'HD';
				$shift_intime = $transaction_data[0]['shift_intime'];
				$shift_outtime = $transaction_data[0]['shift_outtime'];
				$act_intime = $transaction_data[0]['act_intime'];
				$act_outtime = $transaction_data[0]['act_outtime'];
			} elseif($transaction_data[0]['tour_id'] != '0'){
				$shift_id = 'TOUR';
				$shift_intime = $transaction_data[0]['shift_intime'];
				$shift_outtime = $transaction_data[0]['shift_outtime'];
				$act_intime = $transaction_data[0]['act_intime'];
				$act_outtime = $transaction_data[0]['act_outtime'];
			} elseif($transaction_data[0]['compli_status'] != '0'){
				$shift_id = 'COF';
				$shift_intime = $transaction_data[0]['shift_intime'];
				$shift_outtime = $transaction_data[0]['shift_outtime'];
				$act_intime = $transaction_data[0]['act_intime'];
				$act_outtime = $transaction_data[0]['act_outtime'];
			} elseif($transaction_data[0]['on_duty'] != '0'){
				$shift_id = 'OD';
				$shift_intime = $transaction_data[0]['shift_intime'];
				$shift_outtime = $transaction_data[0]['shift_outtime'];
				$act_intime = $transaction_data[0]['act_intime'];
				$act_outtime = $transaction_data[0]['act_outtime'];
			} else {
				$shift_datas_sql = "SELECT * FROM `oc_shift` WHERE `in_time` = '".$transaction_data[0]['shift_intime']."' AND `out_time` = '".$transaction_data[0]['shift_outtime']."' ";
				$shift_data_datas = $this->db->query($shift_datas_sql);
				$shift_id = 0;
				if($shift_data_datas->num_rows > 0){
					$shift_id = intval($shift_data_datas->row['shift_id']);
				}
				$shift_intime = $transaction_data[0]['shift_intime'];
				$shift_outtime = $transaction_data[0]['shift_outtime'];
				$act_intime = $transaction_data[0]['act_intime'];
				$act_outtime = $transaction_data[0]['act_outtime'];
			}

			//echo $shift_id;exit;

			$this->data['shift_id'] = $shift_id;
			$this->data['shift_intime'] = $shift_intime;
			$this->data['shift_outtime'] = $shift_outtime;
			$this->data['actual_intime'] = $act_intime;
			$this->data['actual_outtime'] = $act_outtime;
			$this->data['firsthalf_status'] = $transaction_data[0]['firsthalf_status'];
			$this->data['secondhalf_status'] = $transaction_data[0]['secondhalf_status'];
			
			if($shift_id == 'WO'){
				$this->data['firsthalf_status'] = 'WO';
				$this->data['secondhalf_status'] = 'WO';
			} elseif($shift_id == 'HLD'){
				$this->data['firsthalf_status'] = 'HLD';
				$this->data['secondhalf_status'] = 'HLD';
			} elseif($shift_id == 'HD'){
				$this->data['firsthalf_status'] = 'HD';
				$this->data['secondhalf_status'] = 'HD';
			} elseif($shift_id == 'TOUR'){
				$this->data['firsthalf_status'] = 'TOUR';
				$this->data['secondhalf_status'] = 'TOUR';
			} elseif($shift_id == 'COF'){
				$this->data['firsthalf_status'] = 'COF';
				$this->data['secondhalf_status'] = 'COF';
			} elseif($shift_id == 'OD'){
				$this->data['firsthalf_status'] = 'OD';
				$this->data['secondhalf_status'] = 'OD';
			}

			$leave_data_sql = "SELECT `leave_type`, `type` FROM `oc_leave_transaction` WHERE `emp_id` = '".$this->data['e_name_id']."' AND `date` = '".$this->data['dot']."' AND `p_status` = '1' AND `a_status` = '1' ";
			$leave_data_res = $this->db->query($leave_data_sql);
			$leave_in = 0;
			if($leave_data_res->num_rows > 0){
				//$shift_inn = '1';
				if($leave_data_res->row['type'] == ''){
					$leave_in = 1;
					$first_half = $leave_data_res->row['leave_type'];
					$second_half = $leave_data_res->row['leave_type'];
				} else {
					if($leave_data_res->row['type'] == 'F'){
						$leave_in = 1;
						$this->data['firsthalf_status'] = $leave_data_res->row['leave_type'];
						$this->data['secondhalf_status'] = $leave_data_res->row['leave_type'];
					} elseif($leave_data_res->row['type'] == '1'){
						$leave_in = 2;
						$this->data['firsthalf_status'] = $leave_data_res->row['leave_type'];
					} elseif($leave_data_res->row['type'] == '2'){
						$leave_in = 3;
						$this->data['secondhalf_status'] = $leave_data_res->row['leave_type'];
					}
				}
			}
		} else {	
			$this->data['shift_id'] = intval($assi_shift_id);
			$this->data['shift_intime'] = $shift_intime;
			$this->data['shift_outtime'] = $shift_outtime;
			$this->data['actual_intime'] = '';
			$this->data['actual_outtime'] = '';
			$this->data['firsthalf_status'] = '0';
			$this->data['secondhalf_status'] = '0';
		}

		// echo '<pre>';
		// print_r($shift_data);
		// echo '<pre>';
		// print_r($this->data['shift_id']);
		// exit;

		$this->template = 'transaction/manualpunch_ess.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validateDelete() {
		$this->load->model('transaction/transaction');

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}  

	protected function validateForm() {
		$this->load->model('transaction/transaction');

		if (!$this->user->hasPermission('modify', 'transaction/manualpunch_ess')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if(strlen(utf8_decode(trim($this->request->post['e_name']))) < 1 || strlen(utf8_decode(trim($this->request->post['e_name']))) > 255){
			$this->error['e_name'] = 'Please Select Employee Name';
		} else {
			if($this->request->post['e_name_id'] == ''){
				$emp_id = $this->model_transaction_transaction->getempexist($this->request->post['e_name_id']);				
				if($emp_id == 0){
					$this->error['e_name'] = 'Employee Does Not Exist';
				}
			}
		}

		if(strlen(utf8_decode(trim($this->request->post['dot']))) < 1 || strlen(utf8_decode(trim($this->request->post['dot']))) > 255){
			$this->error['dot'] = $this->language->get('error_dot');
		}

		if($this->request->post['actual_intime'] != '00:00:00' && $this->request->post['actual_outtime'] != '00:00:00'){
			$date_from = $this->request->post['date_from'];
			$date_to = $this->request->post['date_to'];
			$actual_intime = $this->request->post['actual_intime'];
			$actual_outtime = $this->request->post['actual_outtime'];

			$start_date = new DateTime($date_from.' '.$actual_intime);
			$since_start = $start_date->diff(new DateTime($date_to.' '.$actual_outtime));
			if($since_start->invert == 1){
				$this->error['warning'] = 'Actual Outime cannot be less than Actual Intime';		
			}
		}

		if($this->request->post['type'] == 1){
			if($this->request->post['shift_id'] == '0'){
				$this->error['shift_id'] = 'Please Select Value From Shift Data';
			}
		}

		if($this->request->post['type'] == 1){
			if($this->request->post['px_record_type'] == 0){
				$this->error['px_record_type'] = 'Please Select Value From PX Record Type';
			}
		}

		if(strlen(utf8_decode(trim($this->request->post['punch_reason']))) < 1 || strlen(utf8_decode(trim($this->request->post['punch_reason']))) > 255){
			$this->error['punch_reason'] = 'Please Enter PX Record Reason';
		}

		$from = $this->request->post['date_from1'];
		$to = $this->request->post['date_to1'];

		$date_sql = "SELECT `date` FROM `oc_manual_punch` WHERE `date` BETWEEN '".$from."' AND '".$to."' AND (`approval_1` = '0' OR `approval_1` = '1') AND `emp_id`= '".$this->request->post['e_name_id']."' ";
		//echo $date_sql;exit;
		$date_res = $this->db->query($date_sql);
		if($date_res->num_rows > 0){
			$this->error['warning'] = 'PX Record For Dates Exist';	
		}

		$date_sql = "SELECT `date`, `leave_amount`, `type` FROM `oc_leave_transaction_temp` WHERE `date` BETWEEN '".$from."' AND '".$to."' AND `a_status` = '1' AND `emp_id`= '".$this->request->post['e_name_id']."' ";
		$date_res = $this->db->query($date_sql);
		if($date_res->num_rows > 0){
			if($date_res->row['type'] != ''){
				if($date_res->row['leave_amount'] == 0.5 && $this->request->post['leave_amount'] == 0.5){
					if($date_res->row['type'] == $this->request->post['type']){
						$this->error['warning'] = 'Leave For Dates Exist';
					}
				} else {
					$this->error['warning'] = 'Leave For Dates Exist';
				}
			} else {
				$this->error['warning'] = 'Leave For Dates Exist';	
			}
		}

		

		// echo '<pre>';
		// print_r($this->request->post);
		// echo '<pre>';
		// print_r($this->error);
		// exit;

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	public function autocomplete() {
		$json = array();

		if ((isset($this->request->get['filter_name']) || isset($this->request->get['filter_name_id']) ) && isset($this->request->get['filter_date'])) {
			$this->load->model('catalog/employee');
			$this->load->model('report/common_report');

			if(isset($this->request->get['filter_name'])){
				$filter_name = $this->request->get['filter_name'];
			} else {
				$filter_name = '';
			}

			if(isset($this->request->get['filter_name_id'])){
				$filter_name_id = $this->request->get['filter_name_id'];
			} else {
				$filter_name_id = '';
			}

			if($filter_name_id != ''){
				$filter_name = '';
			}

			$data = array(
				'filter_name' => $filter_name,
				'filter_name_id' => $filter_name_id,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$filter_date = $this->request->get['filter_date'];
				$datas = array(
					'filter_date' => date('Y-m-d', strtotime($this->request->get['filter_date'])),
					'filter_limit' => 1,
					//'month_close' => 1,
					'filter_date_start' => '',
					'filter_date_end' => '',
					'filter_unit' => '',
					'filter_department' => '',
				);

				$actual_intime = '00:00:00';
				$actual_outtime = '00:00:00';
				$shift_intime = '00:00:00';
				$shift_outtime = '00:00:00';
				$secondhalf_status = 0;
				$firsthalf_status = 0;
				$shift_id = 0;
				$transaction_id = 0;
				$id = 0;
				$date_from = '0000-00-00';
				$date_to = '0000-00-00';
				$month_close = 0;
				$shift_id = 0; 
				$on_duty = 0;
				$type = 1;
				$px_record_type = 0;
				$manual_datas = $this->db->query("SELECT * FROM `oc_manual_punch` WHERE `date` = '".$datas['filter_date']."' AND `emp_id` = '".$result['emp_code']."' ");
				if($manual_datas->num_rows > 0){
					$transaction_data = $manual_datas->rows;
					if(isset($transaction_data[0]['id'])){
						$actual_intime = $transaction_data[0]['act_intime'];
						$actual_outtime = $transaction_data[0]['act_outtime'];
						$shift_intime = $transaction_data[0]['shift_intime'];
						$shift_outtime = $transaction_data[0]['shift_outtime'];
						$firsthalf_status = $transaction_data[0]['firsthalf_status'];
						$secondhalf_status = $transaction_data[0]['secondhalf_status'];
						$transaction_id = $transaction_data[0]['transaction_id'];
						$id = $transaction_data[0]['id'];
						$date_from = $transaction_data[0]['date'];
						$date_to = $transaction_data[0]['date_out'];
						$month_close = $transaction_data[0]['month_close_status'];
						$type = $transaction_data[0]['type'];
						$px_record_type = $transaction_data[0]['px_record_type'];
						$insert_stat = 0;
						
						if($transaction_data[0]['weekly_off'] != '0'){
							$shift_id = 'WO';
						} elseif($transaction_data[0]['holiday_id'] != '0'){
							$shift_id = 'HLD';
						} elseif($transaction_data[0]['halfday_status'] != '0'){
							$shift_id = 'HD';
						} elseif($transaction_data[0]['tour_id'] != '0'){
							$shift_id = 'TOUR';
						} else {
							$shift_datas_sql = "SELECT * FROM `oc_shift` WHERE `in_time` = '".$transaction_data[0]['shift_intime']."' AND `out_time` = '".$transaction_data[0]['shift_outtime']."' ";
							$shift_data_datas = $this->db->query($shift_datas_sql);
							$shift_id = 0;
							if($shift_data_datas->num_rows > 0){
								$shift_id = $shift_data_datas->row['shift_id'];
							}
						}
					} else {
						$insert_stat = 1;
					}
					if(isset($transaction_data[0]['on_duty'])){
						$on_duty = $transaction_data[0]['on_duty'];
					} else {
						$on_duty = 0;
					}
				} else {
					$transaction_data = $this->model_report_common_report->gettransaction_data($result['emp_code'], $datas);
					$actual_intime = '00:00:00';
					$actual_outtime = '00:00:00';
					$shift_intime = '00:00:00';
					$shift_outtime = '00:00:00';
					$secondhalf_status = 0;
					$firsthalf_status = 0;
					$shift_id = 0;
					$transaction_id = 0;
					$id = 0;
					$date_from = '0000-00-00';
					$date_to = '0000-00-00';
					$month_close = 0;
					$shift_id = 0; 
					if(isset($transaction_data[0]['transaction_id'])){
						$actual_intime = $transaction_data[0]['act_intime'];
						$actual_outtime = $transaction_data[0]['act_outtime'];
						$shift_intime = $transaction_data[0]['shift_intime'];
						$shift_outtime = $transaction_data[0]['shift_outtime'];
						$firsthalf_status = $transaction_data[0]['firsthalf_status'];
						$secondhalf_status = $transaction_data[0]['secondhalf_status'];
						$transaction_id = $transaction_data[0]['transaction_id'];
						$id = 0;//$transaction_data[0]['id'];
						$date_from = $transaction_data[0]['date'];
						$date_to = $transaction_data[0]['date_out'];
						$month_close = $transaction_data[0]['month_close_status'];
						$insert_stat = 1;
						
						if($transaction_data[0]['weekly_off'] != '0'){
							$shift_id = 'WO';
						} elseif($transaction_data[0]['holiday_id'] != '0'){
							$shift_id = 'HLD';
						} elseif($transaction_data[0]['halfday_status'] != '0'){
							$shift_id = 'HD';
						} elseif($transaction_data[0]['tour_id'] != '0'){
							$shift_id = 'TOUR';
						} else {
							$shift_datas_sql = "SELECT * FROM `oc_shift` WHERE `in_time` = '".$transaction_data[0]['shift_intime']."' AND `out_time` = '".$transaction_data[0]['shift_outtime']."' ";
							$shift_data_datas = $this->db->query($shift_datas_sql);
							$shift_id = 0;
							if($shift_data_datas->num_rows > 0){
								$shift_id = $shift_data_datas->row['shift_id'];
							}
						}
					} else {
						$sel_date = $datas['filter_date'];
						$day = date('j', strtotime($sel_date));
						$month = date('n', strtotime($sel_date));
						$year = date('Y', strtotime($sel_date));
						$assi_shift_datas = $this->db->query("SELECT `".$day."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$result['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
						$assi_shift_id = 0;
						if($assi_shift_datas->num_rows > 0){
							$assi_shift_ids = $assi_shift_datas->row[$day];
							$assi_shift_id_exp = explode('_', $assi_shift_ids);
							if(isset($assi_shift_id_exp[2])){
								$assi_shift_id = $assi_shift_id_exp[2];
							} else {
								$assi_shift_id = $assi_shift_id_exp[1];
							}
						}
						$shift_id = $assi_shift_id;
					
						$insert_stat = 1;
					}
					if(isset($transaction_data[0]['on_duty'])){
						$on_duty = $transaction_data[0]['on_duty'];
					} else {
						$on_duty = 0;
					}
				}
				$json[] = array(
					'emp_id' => $result['emp_code'],
					'on_duty' => $on_duty, 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
					'shift_id' => $shift_id,
					'actual_intime' => $actual_intime,
					'actual_outtime' => $actual_outtime,
					'shift_intime' => $shift_intime,
					'shift_outtime' => $shift_outtime,
					'firsthalf_status' => $firsthalf_status,
					'secondhalf_status' => $secondhalf_status,
					'insert_stat' => $insert_stat,
					'date_from' => $date_from,
					'date_to' => $date_to,
					'month_close' => $month_close,
					'transaction_id' => $transaction_id,
					'type' => $type,
					'px_record_type' => $px_record_type,
					'id' => $id
				);
			}		
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}
}
?>