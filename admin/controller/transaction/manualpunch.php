<?php    
class ControllerTransactionManualpunch extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('transaction/manualpunch');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		$this->getForm();
	}

	public function update() {
		$this->language->load('transaction/manualpunch');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			if($this->request->post['actual_intime'] == '' || $this->request->post['actual_intime'] == '0') {
				$this->request->post['actual_intime'] = '00:00:00';
			}
			if($this->request->post['actual_outtime'] == '' || $this->request->post['actual_outtime'] == '0') {
				$this->request->post['actual_outtime'] = '00:00:00';
			}
			if($this->request->post['dot'] != ''){
				$this->request->post['dot'] = date('Y-m-d', strtotime($this->request->post['dot']));
				$this->request->post['day'] = date('j', strtotime($this->request->post['dot']));
				$this->request->post['month'] = date('n', strtotime($this->request->post['dot']));
				$this->request->post['year'] = date('Y', strtotime($this->request->post['dot']));
			} else {
				$this->request->post['dot'] = date('Y-m-d');
				$this->request->post['day'] = date('j');
				$this->request->post['month'] = date('n');
				$this->request->post['year'] = date('Y');
			}
			$day = $this->request->post['day'];
			$month = $this->request->post['month'];
			$year = $this->request->post['year'];

			$act_intime = $this->request->post['actual_intime'];
			$act_outtime = $this->request->post['actual_outtime'];
			$shift_intime = $this->request->post['shift_intime'];
			$shift_outtime = $this->request->post['shift_outtime'];
			$shift_outtime_flexi = Date('H:i:s', strtotime($this->request->post['shift_outtime'] .' +60 minutes'));
			$act_shift_intime = $shift_intime;
			$act_shift_outtime = $shift_outtime;
			$act_shift_outtime_flexi = $shift_outtime_flexi;
			$punch_date = $this->request->post['dot'];
			$act_in_punch_date = date('Y-m-d', strtotime($this->request->post['date_from']));
			$act_out_punch_date = date('Y-m-d', strtotime($this->request->post['date_to']));
			$data = $this->request->post;
			
			$abnormal_status = 0;
			$leave_status = 0;
			$first_half = 0;
			$second_half = 0;
			$late_time = '00:00:00';
			//$act_intime = '09:59:00';
			//$act_outtime = '19:45:00';
			$late_mark = 0;
			$early_mark = 0;
			$shift_early = 0;
			$working_time = '00:00:00';
			$shift_working_time = '00:00:00';

			$shift_data = $this->db->query("SELECT * FROM `oc_shift` WHERE `in_time` = '".$shift_intime."' ")->row;
			if($act_intime != '00:00:00'){
				$shift_intime_plus_one = Date('H:i:s', strtotime($act_shift_intime .' +60 minutes'));
				$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
				$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
				if($since_start->h > 12){
					$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
					$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
				}
				if($since_start->invert == 1 || ($since_start->h == 0 && $since_start->i == 0)){
					$late_time = '00:00:00';
					$late_mark = 0;
				} else {
					if($since_start->h > 0 || ($since_start->h == 0 && $since_start->i > 0)){
						$late_hour = $since_start->h;
						$late_min = $since_start->i;
						$late_sec = $since_start->s;
						$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
						$late_mark = 1;
						$late_mark = 0;
						$late_time = '00:00:00';
					} else {
						$late_time = '00:00:00';
						$late_mark = 0;
					}
				}
				$shift_intime_minus_one = Date('H:i:s', strtotime($act_shift_intime .' -0 minutes'));
				$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
				$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
				if($since_start->h > 12){
					$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
					$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
				}
				if($since_start->invert == 1){
					$shift_early = 1;
				}

				$start_date = new DateTime($act_in_punch_date.' '.$act_shift_intime);
				$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_shift_outtime));
				$shift_working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
				
				if($act_outtime != '00:00:00'){
					if($shift_early == 1){
						$shift_intime_minus_one = Date('H:i:s', strtotime($act_shift_intime .' -0 minutes'));
						if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
							$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
						} else {
							$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
						}
					} else {
						$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
					}
					$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
					// echo $shift_working_time;
					// echo '<br />';
					// echo $data['shift_id'];
					// echo '<br />';
					if($act_intime  != '00:00:00' && $act_outtime  != ''){
						$first_half = 1;
						$second_half = 1;
					}else{
						$first_half = 0;
						$second_half = 0;
					}

					// if($data['shift_id'] == 'HD'){
					// 	if( ($since_start->h == 4 && $since_start->i >= 00) || $since_start->h > 4){
					// 		$first_half = 1;
					// 		$second_half = 1;
					// 	} elseif( ($since_start->h == 2 && $since_start->i >= 00) || $since_start->h > 2){
					// 		$first_half = 1;
					// 		$second_half = 0;
					// 	} else {
					// 		$first_half = 0;
					// 		$second_half = 0;
					// 	}
					// } else {
					// 	if(strtotime($shift_working_time) == strtotime('09:30:00')){
					// 		if( ($since_start->h == 9 && $since_start->i >= 30) || $since_start->h > 9){
					// 			$first_half = 1;
					// 			$second_half = 1;
					// 		} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
					// 			$first_half = 1;
					// 			$second_half = 0;
					// 		} else {
					// 			$first_half = 0;
					// 			$second_half = 0;
					// 		}
					// 	} elseif (strtotime($shift_working_time) == strtotime('09:00:00')) {
					// 		if( ($since_start->h == 9 && $since_start->i >= 0) || $since_start->h > 9){
					// 			$first_half = 1;
					// 			$second_half = 1;
					// 		} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
					// 			$first_half = 1;
					// 			$second_half = 0;
					// 		} else {
					// 			$first_half = 0;
					// 			$second_half = 0;
					// 		}
					// 	} elseif (strtotime($shift_working_time) == strtotime('08:30:00')) {
					// 		if( ($since_start->h == 8 && $since_start->i >= 30) || $since_start->h > 8){
					// 			$first_half = 1;
					// 			$second_half = 1;
					// 		} elseif( ($since_start->h == 4 && $since_start->i >= 0) || $since_start->h > 4){
					// 			$first_half = 1;
					// 			$second_half = 0;
					// 		} else {
					// 			$first_half = 0;
					// 			$second_half = 0;
					// 		}
					// 	}
					// }

					$working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
				} else {
					$first_half = 0;
					$second_half = 0;
				}
			} else {
				$first_half = 0;
				$second_half = 0;
			}

			// echo $first_half;
			// echo '<br />';
			// echo $act_outtime;
			// echo '<br />';
			// exit;


			$early_time = '00:00:00';
			$over_time = '00:00:00';
			$over_time_amount = '0';
			$over_time_esic = '0';
			$after_shift = '0';
			if($abnormal_status == 0){ //if abnormal status is zero calculate further 
				if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and actouttime 
					$start_date = new DateTime($act_in_punch_date.' '.$act_shift_outtime_flexi);
					$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
					if($since_start->invert == 0 && ($since_start->h > 0 || $since_start->i > 0)){
						$after_shift = 1;
					} else {
						$after_shift = 0;
					}
					$shift_outtime_minus_one = Date('H:i:s', strtotime($act_shift_outtime .' -0 minutes'));
					$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
					$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
					if($since_start->h > 12){
						$shift_outtime_minus_one = Date('H:i:s', strtotime($act_shift_outtime .' -0 minutes'));
						$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
					}
					$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
					if($since_start->invert == 0){
						$early_time = '00:00:00';
					} else {
						$early_mark = 1;
						$early_time = '00:00:00';
						$early_mark = 0;
					}
					$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
					if($since_start->invert == 1){
						$over_time = '00:00:00';
					} else {
						if($data['shift_id'] == 'HD'){
							$shift_working_hours = '04:00:00';
							$start_date = new DateTime($act_in_punch_date.' '.$shift_working_hours);
							$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$working_time));
							if($since_start->invert == 0){
								$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
							} else {
								$over_time = '00:00:00';
							}
						} else {
							if(strtotime($shift_working_time) == strtotime('09:30:00')){
								$shift_working_hours = '09:30:00';
								$start_date = new DateTime($act_in_punch_date.' '.$shift_working_hours);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$working_time));
								if($since_start->invert == 0){
									$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
								} else {
									$over_time = '00:00:00';
								}
							} elseif(strtotime($shift_working_time) == strtotime('09:00:00')){
								$shift_working_hours = '09:00:00';
								$start_date = new DateTime($act_in_punch_date.' '.$shift_working_hours);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$working_time));
								if($since_start->invert == 0){
									$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
								} else {
									$over_time = '00:00:00';
								}
							} elseif(strtotime($shift_working_time) == strtotime('08:30:00')){
								$shift_working_hours = '08:30:00';
								$start_date = new DateTime($act_in_punch_date.' '.$shift_working_hours);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$working_time));
								if($since_start->invert == 0){
									$over_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
								} else {
									$over_time = '00:00:00';
								}
							}
						}
						$over_time = '00:00:00';
						/*
						if($late_time != '00:00:00'){
							$late_time_exp = explode(':', $late_time);
							$hour = $late_time_exp[0];
							$min = $late_time_exp[1];
							$hour_min = $hour.':'.$min;
							$minutes_compare = hoursToMinutes($hour_min);
							$over_time1 = $over_time;
							$over_time_exp = explode(':', $over_time);
							$hour = $over_time_exp[0];
							$min = $over_time_exp[1];
							$hour_min = $hour.':'.$min;
							$over_time_minutes_compare = hoursToMinutes($hour_min);
							if($over_time_minutes_compare > $minutes_compare){
								$over_time = Date('H:i:s', strtotime($over_time1 .' -'. $minutes_compare .' minutes'));
							} else {
								$over_time = '00:00:00';
							}
						} else {
							$over_time = $over_time;
						}
						*/
						/*
						$overtime_exp = explode(':', $over_time);
						$hour = $overtime_exp[0];
						$min = $overtime_exp[1];
						$hour_min = $hour.':'.$min;
						$minutes_compare = hoursToMinutes($hour_min);
						$over_time_hours = '';
						if($minutes_compare > 0 && $minutes_compare <= 44){
							$over_time_hours = '00';
						} elseif($minutes_compare >= 45 && $minutes_compare <= 104){
							$over_time_hours = '01';
						} elseif($minutes_compare >= 105 && $minutes_compare <= 164){
							$over_time_hours = '02';
						} elseif($minutes_compare >= 165 && $minutes_compare <= 224){
							$over_time_hours = '03';
						} elseif($minutes_compare >= 225 && $minutes_compare <= 284){
							$over_time_hours = '04';
						} elseif($minutes_compare >= 285 && $minutes_compare <= 344){
							$over_time_hours = '05';
						} elseif($minutes_compare >= 345 && $minutes_compare <= 405){
							$over_time_hours = '06';
						} elseif($minutes_compare >= 405 && $minutes_compare <= 464){
							$over_time_hours = '07';
						} elseif($minutes_compare >= 465 && $minutes_compare <= 524){
							$over_time_hours = '08';
						} elseif($minutes_compare >= 525 && $minutes_compare <= 584){
							$over_time_hours = '09';
						} elseif($minutes_compare >= 585 && $minutes_compare <= 644){
							$over_time_hours = '10';
						} elseif($minutes_compare >= 645 && $minutes_compare <= 704){
							$over_time_hours = '11';
						} elseif($minutes_compare >= 705 && $minutes_compare <= 764){
							$over_time_hours = '12';
						} elseif($minutes_compare >= 765 && $minutes_compare <= 824){
							$over_time_hours = '13';
						} elseif($minutes_compare >= 825 && $minutes_compare <= 884){
							$over_time_hours = '14';
						} elseif($minutes_compare >= 885 && $minutes_compare <= 944){
							$over_time_hours = '15';
						} elseif($minutes_compare >= 945 && $minutes_compare <= 1004){
							$over_time_hours = '16';
						}
						// if($emp_salary['basic'] > 0 && $emp_salary['vda'] > 0){
						// 	$over_time_amount_1 = (($emp_salary['basic'] + $emp_salary['vda']) / 8);
						// 	$over_time_amount = $over_time_amount_1 * 2 * $over_time_hours;
						// }
						$over_time_amount = '0.00';
						//$over_time = $over_time_hours.':00';
						$over_time_esic = round(($over_time_amount * 1.75) / 100);
						*/
					}
				}					
			} else {
				$first_half = 0;
				$second_half = 0;
			}

			$trans_data_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$this->request->post['e_name_id']."' AND `date` = '".$act_in_punch_date."' ";
			$trans_data_res = $this->db->query($trans_data_sql)->row;

			$leave_data_sql = "SELECT `leave_type`, `type` FROM `oc_leave_transaction` WHERE `emp_id` = '".$this->request->post['e_name_id']."' AND `date` = '".$act_in_punch_date."' AND `p_status` = '1' AND `a_status` = '1' ";
			$leave_data_res = $this->db->query($leave_data_sql);
			$leave_in = 0;
			if($leave_data_res->num_rows > 0){
				//$shift_inn = '1';
				if($leave_data_res->row['type'] == ''){
					$leave_in = 1;
					$first_half = $leave_data_res->row['leave_type'];
					$second_half = $leave_data_res->row['leave_type'];
				} else {
					if($leave_data_res->row['type'] == 'F'){
						$leave_in = 1;
						$first_half = $leave_data_res->row['leave_type'];
						$second_half = $leave_data_res->row['leave_type'];
					} elseif($leave_data_res->row['type'] == '1'){
						$leave_in = 2;
						$first_half = $leave_data_res->row['leave_type'];
					} elseif($leave_data_res->row['type'] == '2'){
						$leave_in = 3;
						$second_half = $leave_data_res->row['leave_type'];
					}
				}
			}

			$shift_change = 0;
			$shift_free_change = 0;
			if($data['shift_id'] != $data['hidden_shift_id']){
				if($data['shift_id'] == 'WO' || $data['shift_id'] == 'HLD' || $data['shift_id'] == 'OD'){
					$shift_free_change = 1;
				} else {
					$shift_change = 1;
				}
			}

			$act_intime_change = 0;
			if($data['actual_intime'] != $data['hidden_actual_intime']){
				$act_intime_change = 1;
			}

			$act_outtime_change = 0;
			if($data['actual_outtime'] != $data['hidden_actual_outtime']){
				$act_outtime_change = 1;
			}

			if($first_half == '1' && $second_half == '1'){
				$present_status = 1;
				$absent_status = 0;
			} elseif($first_half == '1' && $second_half == '0'){
				$present_status = 0.5;
				$absent_status = 0.5;
			} elseif($first_half == '0' && $second_half == '1'){
				$present_status = 0.5;
				$absent_status = 0.5;
			} elseif($leave_in == '1'){
				$present_status = 0;
				$absent_status = 0;
				$leave_status = 1;
			} elseif($leave_in == '2'){
				$present_status = 0.5;
				if($second_half == '0'){
					$absent_status = 0.5;
				} else {
					$absent_status = 0;
				}
				$leave_status = 0.5;
			} elseif($leave_in == '3'){
				$present_status = 0.5;
				if($first_half == '0'){
					$absent_status = 0.5;
				} else {
					$absent_status = 0;
				}
				$leave_status = 0.5;
			} else {
				$present_status = 0;
				$absent_status = 1;
			}
			
			$day_date1 = date('j', strtotime($act_in_punch_date));
			$day1 = date('j', strtotime($act_in_punch_date));
			$month1 = date('n', strtotime($act_in_punch_date));
			$year1 = date('Y', strtotime($act_in_punch_date));

			$update3 = "SELECT  `".$day_date1."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$data['e_name_id']."' AND `month` = '".$month1."' AND `year` = '".$year1."' ";
			$shift_schedule = $this->db->query($update3)->row;
			$schedule_raw = explode('_', $shift_schedule[$day_date1]);
			if(!isset($schedule_raw[2])){
				$schedule_raw[2] = 1;
			}

			if($data['shift_id'] == 'WO'){
				$absent_status = 0;
				$weekly_off = 1;
				$holiday = 0;
				$half_day = 0;
				$first_half = 'WO';
				$second_half = 'WO';
				$day_info_id = 'W_1';
				$shift_id = $schedule_raw[2];
				$act_shift_id = $schedule_raw[2];
				$shift_data = $this->model_transaction_transaction->getshiftdata($shift_id);
				$shift_code = $shift_data['shift_code'];
				$act_shift_code = $shift_code;
			} elseif($data['shift_id'] == 'HLD'){
				$absent_status = 0;
				$weekly_off = 0;
				$holiday = 1;
				$half_day = 0;
				$first_half = 'HLD';
				$second_half = 'HLD';
				$day_info_id = 'H_1';
				$shift_id = $schedule_raw[2];
				$act_shift_id = $schedule_raw[2];
				$shift_data = $this->model_transaction_transaction->getshiftdata($shift_id);
				$shift_code = $shift_data['shift_code'];
				$act_shift_code = $shift_code;
			} elseif($data['shift_id'] == 'HD'){
				$weekly_off = 0;
				$holiday = 0;
				$half_day = 1;
				$first_half = 'HD';
				$second_half = 'HD';
				$day_info_id = 'HD_1';
				$shift_id = $schedule_raw[2];
				$act_shift_id = $schedule_raw[2];
				$shift_data = $this->model_transaction_transaction->getshiftdata($shift_id);
				$shift_code = $shift_data['shift_code'];
				$act_shift_code = $shift_code;
			} else {
				$weekly_off = 0;
				$holiday = 0;
				$half_day = 0;
				$day_info_id = 'S_'.$data['shift_id'];
				$shift_id = $data['shift_id'];
				$act_shift_id = $data['shift_id'];
				$shift_data = $this->model_transaction_transaction->getshiftdata($shift_id);
				if(isset($shift_data['shift_code'])){
					$shift_code = $shift_data['shift_code'];
				} else{
					$shift_code = '';
				}
				$act_shift_code = $shift_code;
			}

			$old_data_sql = "SELECT * FROM `oc_transaction` WHERE `date` = '".$data['dot']."' AND `emp_id` = '".$data['e_name_id']."' ";
			$old_datas = $this->db->query($old_data_sql);
			if($old_datas->num_rows > 0){
				$old_data = $old_datas->row;
				if($data['actual_outtime'] != $old_data['act_outtime']){
					$sql = "UPDATE `oc_attendance` SET `status` = '0' WHERE `emp_id` = '".$data['e_name_id']."' AND `punch_time` = '".$old_data['act_outtime']."' AND `punch_date` = '".$old_data['date_out']."' ";
					$this->db->query($sql);
					//echo $sql;exit;
				}
				if($data['actual_intime'] == '00:00:00' && $data['actual_outtime'] == '00:00:00'){
					$sql = "UPDATE `oc_attendance` SET `status` = '0' WHERE `emp_id` = '".$data['e_name_id']."' AND `punch_date` > '".$old_data['date']."' ";
					$this->db->query($sql);
				}
			}

			if($data['insert'] == '0'){
				$sql = "UPDATE `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `act_shift_id` = '".$act_shift_id."', `act_shift_code` = '".$act_shift_code."', `act_shift_intime` = '".$shift_intime."', `act_shift_outtime` = '".$shift_outtime."', `act_shift_outtime_flexi` = '".$act_shift_outtime_flexi."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', `working_time` = '".$working_time."', `over_time` = '".$over_time."', `over_time_amount` = '".$over_time_amount."', `over_time_esic` = '".$over_time_esic."', `after_shift` = '".$after_shift."', `weekly_off` = '".$weekly_off."', `holiday_id` = '".$holiday."', `halfday_status` = '".$half_day."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `abnormal_status` = '".$abnormal_status."', `present_status` = '".$present_status."', `leave_status` = '".$leave_status."', `absent_status` = '".$absent_status."', `manual_status` = '1', `shift_change` = '".$shift_change."', `shift_free_change` = '".$shift_free_change."', `act_intime_change` = '".$act_intime_change."', `act_outtime_change` = '".$act_outtime_change."' WHERE `transaction_id` = '".$data['transaction_id']."' ";
				// echo $sql;
				// echo '<br />';
				// exit;
				$this->db->query($sql);	
				$this->session->data['success'] = 'You have updated the Transaction';
				$transaction_id = $data['transaction_id'];
			} else {
				$emp_data = $this->model_transaction_transaction->getEmployees_dat($data['e_name_id']);
				if(isset($emp_data['emp_code'])){
					$department = $emp_data['department'];
					$department_id = $emp_data['department_id'];
					$unit = $emp_data['unit'];
					$unit_id = $emp_data['unit_id'];
				} else {
					$department = '';
					$department_id = '';
					$unit = '';
					$unit_id = '';
				}
				if($leave_in == 1){
					$leave_status = 1;
				} elseif($leave_in == 2 || $leave_in == 3){
					$leave_status = 0.5;
				} else {
					$leave_status = 0;
				}
				$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$data['e_name_id']."', `emp_name` = '".$data['e_name']."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `act_shift_id` = '".$act_shift_id."', `act_shift_code` = '".$act_shift_code."', `act_shift_intime` = '".$shift_intime."', `act_shift_outtime` = '".$shift_outtime."', `act_shift_outtime_flexi` = '".$act_shift_outtime_flexi."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', `working_time` = '".$working_time."', `over_time` = '".$over_time."', `over_time_amount` = '".$over_time_amount."', `over_time_esic` = '".$over_time_esic."', `after_shift` = '".$after_shift."', `day` = '".$data['day']."', `month` = '".$data['month']."', `year` = '".$data['year']."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `weekly_off` = '".$weekly_off."', `holiday_id` = '".$holiday."', `halfday_status` = '".$half_day."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `abnormal_status` = '".$abnormal_status."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', `leave_status` = '".$leave_status."', `manual_status` = '1', `department` = '".$this->db->escape($department)."', department_id = '".$this->db->escape($department_id)."', `unit` = '".$this->db->escape($unit)."', unit_id = '".$this->db->escape($unit_id)."' ";
				// echo $sql;
				// echo '<br />';
				// exit;
				$this->db->query($sql);
				$transaction_id = $this->db->getLastId();
				$this->session->data['success'] = 'You have Inserted the Transaction';	
			}

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['filter_department'])) {
				$url .= '&filter_department=' . $this->request->get['filter_department'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['requestform'])) {
				$url .= '&requestform=' . $this->request->get['requestform'];
			} 

			if (isset($this->request->get['filter_unit'])) {
				$funit = $this->request->get['filter_unit'];
			} else {
				$funit = '';
			}

			//echo 'out';exit;
			if (isset($this->request->get['requestform'])) {
				$r_url = $this->url->link('transaction/manualpunch', 'token=' . $this->session->data['token'].'&transaction_id='.$transaction_id.$url, 'SSL');
				$this->redirect($r_url);
			} else {
				$r_url = $this->url->link('transaction/manualpunch', 'token=' . $this->session->data['token'].'&transaction_id='.$transaction_id, 'SSL');
				if($funit){
					$r_url .= "&filter_unit=".$funit;
				}	
				$this->redirect($r_url);
			}
		}

		$this->getForm();
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['tab_general'] = $this->language->get('tab_general');

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {	
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['e_name'])) {
			$this->data['error_employee'] = $this->error['e_name'];
		} else {
			$this->data['error_employee'] = '';
		}

		if (isset($this->error['dot'])) {
			$this->data['error_dot'] = $this->error['dot'];
		} else {
			$this->data['error_dot'] = '';
		}

		if (isset($this->error['shift_id'])) {
			$this->data['error_shift_id'] = $this->error['shift_id'];
		} else {
			$this->data['error_shift_id'] = array();
		}

		if (isset($this->error['project_id'])) {
			$this->data['error_project_id'] = $this->error['project_id'];
		} else {
			$this->data['error_project_id'] = array();
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('transaction/manualpunch', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_approval_1'])) {
			$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['requestform'])) {
			$url .= '&requestform=' . $this->request->get['requestform'];
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
			$this->data['abnormal_list'] = $this->url->link('transaction/dayprocess', 'token=' . $this->session->data['token'].'&unit=' . $unit, 'SSL');
			$this->data['request_list'] = '';
			$this->data['action'] = $this->url->link('transaction/manualpunch/update', 'token=' . $this->session->data['token'].'&unit=' . $unit.$url, 'SSL');
			$this->data['cancel'] = $this->url->link('transaction/dayprocess', 'token=' . $this->session->data['token'].'&unit=' . $unit, 'SSL');
		} elseif (isset($this->request->get['requestform'])) {
			$unit = '';
			$this->data['request_list'] = $this->url->link('transaction/requestformunit', 'token=' . $this->session->data['token'].$url, 'SSL');
			$this->data['abnormal_list'] = '';
			$this->data['action'] = $this->url->link('transaction/manualpunch/update', 'token=' . $this->session->data['token'].$url, 'SSL');
			$this->data['cancel'] = $this->url->link('transaction/requestformunit', 'token=' . $this->session->data['token'].$url, 'SSL');
		} else {
			$this->data['abnormal_list'] = '';
			$this->data['request_list'] = '';
			$unit = '';
			$this->data['action'] = $this->url->link('transaction/manualpunch/update', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['cancel'] = $this->url->link('transaction/manualpunch', 'token=' . $this->session->data['token'], 'SSL');
		}

		$transaction_data = array();
		if (isset($this->request->get['transaction_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$transaction_data = $this->model_transaction_transaction->gettransaction_data($this->request->get['transaction_id']);
		}
		//echo '<pre>';print_r($transaction_data);exit;
		$this->data['token'] = $this->session->data['token'];



		$statuses[1]['stat_id'] = '0';
		$statuses[1]['stat_name'] = 'Absent';
		$statuses[2]['stat_id'] = '1';
		$statuses[2]['stat_name'] = 'Present';
		$statuses[3]['stat_id'] = 'WO';
		$statuses[3]['stat_name'] = 'Weekly Off';
		$statuses[4]['stat_id'] = 'HD';
		$statuses[4]['stat_name'] = 'Half Day';
		$statuses[5]['stat_id'] = 'HLD';
		$statuses[5]['stat_name'] = 'Holiday';
		$statuses[6]['stat_id'] = 'TOUR';
		$statuses[6]['stat_name'] = 'TOUR';
		$statuses[7]['stat_id'] = 'OD';
		$statuses[7]['stat_name'] = 'On Duty';
		$statuses[8]['stat_id'] = 'PL';
		$statuses[8]['stat_name'] = 'PL';
		$statuses[9]['stat_id'] = 'CL';
		$statuses[9]['stat_name'] = 'CL';
		$statuses[10]['stat_id'] = 'SL';
		$statuses[10]['stat_name'] = 'SL';
		$statuses[11]['stat_id'] = 'CL';
		$statuses[11]['stat_name'] = 'CL';
		$this->data['statuses'] = $statuses;

		$datas['filter_name'] = '';

		$shift_data = array();
		$shift_datas = $this->model_catalog_shift->getshifts($datas);
		
		$shift_data[1]['shift_id'] = 'WO';
		$shift_data[1]['shift_name'] = 'Weekly Off';

		$shift_data[2]['shift_id'] = 'HLD';
		$shift_data[2]['shift_name'] = 'Holiday';

		$shift_data[3]['shift_id'] = 'HD';
		$shift_data[3]['shift_name'] = 'Half Day';

		// $shift_data[3]['shift_id'] = 'TOUR';
		// $shift_data[3]['shift_name'] = 'TOUR';

		$ckey = 6;
		foreach ($shift_datas as $skey => $svalue) {
			$shift_data[$ckey]['shift_id'] = $svalue['shift_id'];
			$shift_data[$ckey]['shift_name'] = $svalue['name'];
			$ckey ++;
		}
		$this->data['shift_data'] = $shift_data;

		if (isset($this->request->post['insert'])) {
			$this->data['insert'] = $this->request->post['insert'];
		} elseif(isset($transaction_data[0]['transaction_id'])) {
			$this->data['insert'] = 0;
		} else {
			$this->data['insert'] = '';
		}

		if (isset($this->request->post['transaction_id'])) {
			$this->data['transaction_id'] = $this->request->post['transaction_id'];
		} elseif(isset($transaction_data[0]['transaction_id'])) {
			$this->data['transaction_id'] = $transaction_data[0]['transaction_id'];
		} else {	
			$this->data['transaction_id'] = 0;
		}

		if (isset($this->request->post['od'])) {
			$this->data['od'] = $this->request->post['od'];
		} elseif(isset($transaction_data[0]['on_duty'])) {
			$this->data['od'] = $transaction_data[0]['on_duty'];
		} else {	
			$this->data['od'] = 0;
		}

		if (isset($this->request->post['dot'])) {
			$this->data['dot'] = $this->request->post['dot'];
		} elseif (isset($transaction_data[0]['date'])) {
			$this->data['dot'] = date('Y-m-d', strtotime($transaction_data[0]['date']));
		} else {	
			$this->data['dot'] = date('Y-m-d');
		}
		$this->data['date_from'] = $this->data['dot'];

		if (isset($this->request->post['date_to'])) {
			$this->data['date_to'] = $this->request->post['date_to'];
		} elseif (isset($transaction_data[0]['date_out'])) {
			$this->data['date_to'] = date('Y-m-d', strtotime($transaction_data[0]['date_out']));
		} else {	
			$this->data['date_to'] = $this->data['dot'];
		}

		if (isset($this->request->post['e_name'])) {
			$this->data['e_name'] = $this->request->post['e_name'];
			$this->data['e_name_id'] = $this->request->post['e_name_id'];
			$this->data['emp_code'] = $this->request->post['e_name_id'];
		} elseif (isset($transaction_data[0]['emp_name'])) {
			$this->data['e_name'] = $transaction_data[0]['emp_name'];
			$this->data['e_name_id'] = $transaction_data[0]['emp_id'];
			$this->data['emp_code'] = $transaction_data[0]['emp_id'];
		} else {	
			$this->data['e_name'] = '';
			$this->data['e_name_id'] = '';
			$this->data['emp_code'] = '';
		}

		if (isset($this->request->post['shift_id'])) {
			$this->data['shift_id'] = $this->request->post['shift_id'];
			$this->data['shift_intime'] = $this->request->post['shift_intime']; 
			$this->data['shift_outtime'] = $this->request->post['shift_outtime']; 
			$this->data['actual_intime'] = $this->request->post['actual_intime']; 
			$this->data['actual_outtime'] = $this->request->post['actual_outtime']; 
			$this->data['firsthalf_status'] = $this->request->post['firsthalf_status']; 
			$this->data['secondhalf_status'] = $this->request->post['secondhalf_status']; 
			$this->data['project_datas'] = $this->request->post['project_datas'];
		} elseif (isset($transaction_data[0]['emp_name'])) {
			$today_date = date('j', strtotime($transaction_data[0]['date']));
			$month = date('n', strtotime($transaction_data[0]['date']));
			$year = date('Y', strtotime($transaction_data[0]['date']));
			$shift_data = $this->model_catalog_employee->getshift_id($transaction_data[0]['emp_id'], $today_date, $month, $year);
			$shift_ids = explode('_', $shift_data);
			$shift_intime = '';
			$shift_outtime = '';
			$act_intime = '';
			$act_outtime = '';
			$shift_id = 0;
			if($transaction_data[0]['weekly_off'] != '0'){
				$shift_id = 'WO';
				$shift_intime = $transaction_data[0]['shift_intime'];
				$shift_outtime = $transaction_data[0]['shift_outtime'];
				$act_intime = $transaction_data[0]['act_intime'];
				$act_outtime = $transaction_data[0]['act_outtime'];
			} elseif($transaction_data[0]['holiday_id'] != '0'){
				$shift_id = 'HLD';
				$shift_intime = $transaction_data[0]['shift_intime'];
				$shift_outtime = $transaction_data[0]['shift_outtime'];
				$act_intime = $transaction_data[0]['act_intime'];
				$act_outtime = $transaction_data[0]['act_outtime'];
			} elseif($transaction_data[0]['halfday_status'] != '0'){
				$shift_id = 'HD';
				$shift_intime = $transaction_data[0]['shift_intime'];
				$shift_outtime = $transaction_data[0]['shift_outtime'];
				$act_intime = $transaction_data[0]['act_intime'];
				$act_outtime = $transaction_data[0]['act_outtime'];
			} elseif($transaction_data[0]['compli_status'] != '0'){
				$shift_id = 'COF';
				$shift_intime = $transaction_data[0]['shift_intime'];
				$shift_outtime = $transaction_data[0]['shift_outtime'];
				$act_intime = $transaction_data[0]['act_intime'];
				$act_outtime = $transaction_data[0]['act_outtime'];
			} elseif($transaction_data[0]['tour_id'] != '0'){
				$shift_id = 'TOUR';
				$shift_intime = $transaction_data[0]['shift_intime'];
				$shift_outtime = $transaction_data[0]['shift_outtime'];
				$act_intime = $transaction_data[0]['act_intime'];
				$act_outtime = $transaction_data[0]['act_outtime'];
			} elseif($transaction_data[0]['on_duty'] != '0'){
				$shift_id = 'OD';
				$shift_intime = $transaction_data[0]['shift_intime'];
				$shift_outtime = $transaction_data[0]['shift_outtime'];
				$act_intime = $transaction_data[0]['act_intime'];
				$act_outtime = $transaction_data[0]['act_outtime'];
			} else {
				$shift_datas_sql = "SELECT * FROM `oc_shift` WHERE `in_time` = '".$transaction_data[0]['shift_intime']."' AND `out_time` = '".$transaction_data[0]['shift_outtime']."' ";
				$shift_data_datas = $this->db->query($shift_datas_sql);
				$shift_id = 0;
				if($shift_data_datas->num_rows > 0){
					$shift_id = $shift_data_datas->row['shift_id'];
				}
				$shift_intime = $transaction_data[0]['shift_intime'];
				$shift_outtime = $transaction_data[0]['shift_outtime'];
				$act_intime = $transaction_data[0]['act_intime'];
				$act_outtime = $transaction_data[0]['act_outtime'];
			}

			//echo $shift_id;exit;

			$this->data['shift_id'] = $shift_id;
			$this->data['shift_intime'] = $shift_intime;
			$this->data['shift_outtime'] = $shift_outtime;
			$this->data['actual_intime'] = $act_intime;
			$this->data['actual_outtime'] = $act_outtime;
			$this->data['firsthalf_status'] = $transaction_data[0]['firsthalf_status'];
			$this->data['secondhalf_status'] = $transaction_data[0]['secondhalf_status'];
			
			if($shift_id == 'WO'){
				$this->data['firsthalf_status'] = 'WO';
				$this->data['secondhalf_status'] = 'WO';
			} elseif($shift_id == 'HLD'){
				$this->data['firsthalf_status'] = 'HLD';
				$this->data['secondhalf_status'] = 'HLD';
			} elseif($shift_id == 'HD'){
				$this->data['firsthalf_status'] = 'HD';
				$this->data['secondhalf_status'] = 'HD';
			} elseif($shift_id == 'TOUR'){
				$this->data['firsthalf_status'] = 'TOUR';
				$this->data['secondhalf_status'] = 'TOUR';
			} elseif($shift_id == 'COF'){
				$this->data['firsthalf_status'] = 'COF';
				$this->data['secondhalf_status'] = 'COF';
			} elseif($shift_id == 'OD'){
				$this->data['firsthalf_status'] = 'OD';
				$this->data['secondhalf_status'] = 'OD';
			}

			$leave_data_sql = "SELECT `leave_type`, `type` FROM `oc_leave_transaction` WHERE `emp_id` = '".$this->data['e_name_id']."' AND `date` = '".$this->data['dot']."' AND `p_status` = '1' AND `a_status` = '1' ";
			$leave_data_res = $this->db->query($leave_data_sql);
			$leave_in = 0;
			if($leave_data_res->num_rows > 0){
				//$shift_inn = '1';
				if($leave_data_res->row['type'] == ''){
					$leave_in = 1;
					$first_half = $leave_data_res->row['leave_type'];
					$second_half = $leave_data_res->row['leave_type'];
				} else {
					if($leave_data_res->row['type'] == 'F'){
						$leave_in = 1;
						$this->data['firsthalf_status'] = $leave_data_res->row['leave_type'];
						$this->data['secondhalf_status'] = $leave_data_res->row['leave_type'];
					} elseif($leave_data_res->row['type'] == '1'){
						$leave_in = 2;
						$this->data['firsthalf_status'] = $leave_data_res->row['leave_type'];
					} elseif($leave_data_res->row['type'] == '2'){
						$leave_in = 3;
						$this->data['secondhalf_status'] = $leave_data_res->row['leave_type'];
					}
				}
			}
			$project_data = array();
			$project_datas=$this->db->query("SELECT * FROM `oc_project_transection` pt LEFT JOIN `oc_project_employee` pe ON(pe.`project_id` = pt.`id`) WHERE pe.`employee_id` = '".$transaction_data[0]['emp_id']."' AND '".$transaction_data[0]['date']."' BETWEEN `start_date` AND `end_date` ")->rows;
			foreach($project_datas as $pkey => $pvalue){
				$project_data[] = array(
					'project_id' => $pvalue['project_id'],
					'project_name' => $pvalue['project_name'],
				);
			}
			$this->data['project_datas'] = $project_data;
			$this->data['project_id'] = $transaction_data[0]['project_id'];
		} else {	
			$this->data['project_id'] = '';
			$this->data['shift_id'] = 0;
			$this->data['shift_intime'] = '';
			$this->data['shift_outtime'] = '';
			$this->data['actual_intime'] = '';
			$this->data['actual_outtime'] = '';
			$this->data['firsthalf_status'] = '0';
			$this->data['secondhalf_status'] = '0';
			$this->data['project_datas'] = array();
		}
		
		$this->template = 'transaction/manualpunch.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}  

	protected function validateForm() {
		$this->load->model('transaction/transaction');

		if (!$this->user->hasPermission('modify', 'transaction/horse_wise')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if(strlen(utf8_decode(trim($this->request->post['e_name']))) < 1 || strlen(utf8_decode(trim($this->request->post['e_name']))) > 255){
			$this->error['e_name'] = 'Please Select Employee Name';
		} else {
			if($this->request->post['e_name_id'] == ''){
				$emp_id = $this->model_transaction_transaction->getempexist($this->request->post['e_name_id']);				
				if($emp_id == 0){
					$this->error['e_name'] = 'Employee Does Not Exist';
				}
			}
		}

		if($this->request->post['actual_intime'] != '00:00:00' && $this->request->post['actual_outtime'] != '00:00:00'){
			$date_from = $this->request->post['date_from'];
			$date_to = $this->request->post['date_to'];
			$actual_intime = $this->request->post['actual_intime'];
			$actual_outtime = $this->request->post['actual_outtime'];

			$start_date = new DateTime($date_from.' '.$actual_intime);
			$since_start = $start_date->diff(new DateTime($date_to.' '.$actual_outtime));
			if($since_start->invert == 1){
				$this->error['warning'] = 'Actual Outime cannot be less than Actual Intime';		
			}
		}

		if(strlen(utf8_decode(trim($this->request->post['dot']))) < 1 || strlen(utf8_decode(trim($this->request->post['dot']))) > 255){
			$this->error['dot'] = $this->language->get('error_dot');
		} else {
			if(strtotime($this->request->post['dot']) > strtotime(date('Y-m-d'))){
				$this->error['dot'] = 'Cannot Select Future Dates';
			}
		}

		if($this->request->post['shift_id'] === 0){
			$this->error['shift_id'] = 'Please Select Value From Shift Data';
		}

		// echo '<pre>';
		// print_r($this->error);
		// exit;

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	public function autocomplete() {
		$json = array();
		if ((isset($this->request->get['filter_name']) || isset($this->request->get['filter_name_id']) ) && isset($this->request->get['filter_date']) && isset($this->request->get['filter_project_id']) ) {
			$this->load->model('catalog/employee');
			$this->load->model('report/common_report');

			if(isset($this->request->get['filter_name'])){
				$filter_name = $this->request->get['filter_name'];
			} else {
				$filter_name = '';
			}

			if(isset($this->request->get['filter_name_id'])){
				$filter_name_id = $this->request->get['filter_name_id'];
			} else {
				$filter_name_id = '';
			}

			if(isset($this->request->get['filter_project_id'])){
				$filter_project_id = $this->request->get['filter_project_id'];
			} else {
				$filter_project_id = '';
			}

			$datas = array(
				'filter_date' => date('Y-m-d', strtotime($this->request->get['filter_date'])),
				'filter_project_id' => $filter_project_id,
				'filter_limit' => 1,
				//'month_close' => 1,
				'filter_date_start' => '',
				'filter_date_end' => '',
				'filter_unit' => '',
				'filter_department' => '',
			);
			$transaction_data = $this->model_report_common_report->gettransaction_data($filter_name_id, $datas);
			$actual_intime = '00:00:00';
			$actual_outtime = '00:00:00';
			$shift_intime = '00:00:00';
			$shift_outtime = '00:00:00';
			$secondhalf_status = 0;
			$firsthalf_status = 0;
			$shift_id = 0;
			$transaction_id = 0;
			$date_from = '0000-00-00';
			$date_to = '0000-00-00';
			$month_close = 0;
			$shift_id = 0; 
			$project_id = 0; 
			if(isset($transaction_data[0]['transaction_id'])){
				$actual_intime = $transaction_data[0]['act_intime'];
				$actual_outtime = $transaction_data[0]['act_outtime'];
				$shift_intime = $transaction_data[0]['shift_intime'];
				$shift_outtime = $transaction_data[0]['shift_outtime'];
				$firsthalf_status = $transaction_data[0]['firsthalf_status'];
				$secondhalf_status = $transaction_data[0]['secondhalf_status'];
				$transaction_id = $transaction_data[0]['transaction_id'];
				$date_from = $transaction_data[0]['date'];
				$date_to = $transaction_data[0]['date_out'];
				$month_close = $transaction_data[0]['month_close_status'];
				$project_id = $transaction_data[0]['project_id'];
				$insert_stat = 0;
				
				if($transaction_data[0]['weekly_off'] != '0'){
					$shift_id = 'WO';
				} elseif($transaction_data[0]['holiday_id'] != '0'){
					$shift_id = 'HLD';
				} elseif($transaction_data[0]['halfday_status'] != '0'){
					$shift_id = 'HD';
				} elseif($transaction_data[0]['tour_id'] != '0'){
					$shift_id = 'TOUR';
				} else {
					$shift_datas_sql = "SELECT * FROM `oc_shift` WHERE `in_time` = '".$transaction_data[0]['shift_intime']."' AND `out_time` = '".$transaction_data[0]['shift_outtime']."' ";
					$shift_data_datas = $this->db->query($shift_datas_sql);
					$shift_id = 0;
					if($shift_data_datas->num_rows > 0){
						$shift_id = $shift_data_datas->row['shift_id'];
					}
				}
			} else {
				$insert_stat = 1;
			}
			if(isset($transaction_data[0]['on_duty'])){
				$on_duty = $transaction_data[0]['on_duty'];
			} else {
				$on_duty = 0;
			}
			$json = array(
				'emp_id' => $filter_name_id,
				'project_id' =>$project_id,
				'on_duty' => $on_duty, 
				'shift_id' => $shift_id,
				'actual_intime' => $actual_intime,
				'actual_outtime' => $actual_outtime,
				'shift_intime' => $shift_intime,
				'shift_outtime' => $shift_outtime,
				'firsthalf_status' => $firsthalf_status,
				'secondhalf_status' => $secondhalf_status,
				'insert_stat' => $insert_stat,
				'date_from' => $date_from,
				'date_to' => $date_to,
				'month_close' => $month_close,
				'transaction_id' => $transaction_id
			);
		}
		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete_name() {
		$json = array();

		if ((isset($this->request->get['filter_name']) || isset($this->request->get['filter_name_id']) ) && isset($this->request->get['filter_date'])) {
			
			$this->load->model('catalog/employee');
			$this->load->model('report/common_report');

			if(isset($this->request->get['filter_name'])){
				$filter_name = $this->request->get['filter_name'];
			} else {
				$filter_name = '';
			}

			if(isset($this->request->get['filter_name_id'])){
				$filter_name_id = $this->request->get['filter_name_id'];
			} else {
				$filter_name_id = '';
			}

			$data = array(
				'filter_name' => $filter_name,
				'filter_name_id' => $filter_name_id,
				'filter_date' => date('Y-m-d', strtotime($this->request->get['filter_date'])),
				'start'       => 0,
				'limit'       => 20
			);
			$results = $this->model_catalog_employee->getemployees($data);
			// echo '<pre>';
			// print_r($results);
			// exit;
			foreach ($results as $result) {

				$project_data = array();
				$project_datas=$this->db->query("SELECT * FROM `oc_project_transection` pt LEFT JOIN `oc_project_employee` pe ON(pe.`project_id` = pt.`id`) WHERE pe.`employee_id` = '".$result['emp_code']."' AND '".$data['filter_date']."' BETWEEN `start_date` AND `end_date` ")->rows;
				$project_data[] = array(
					'project_id' => '',
					'project_name' => 'Please Select',
				);
				foreach($project_datas as $pkey => $pvalue){
					$project_data[] = array(
						'project_id' => $pvalue['project_id'],
						'project_name' => $pvalue['project_name'],
					);
				}
				$json[] = array(
					'emp_id' => $result['emp_code'],
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
					'project_datas' =>$project_data,
				);
			}		
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}
		// echo '<pre>';
		// print_r($json);
		// exit;
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	public function getshiftdata() {
		$jsondata = array();
		$jsondata['status'] = 0;
		//$this->log->write(print_r($this->request->get,true));
		$shift_data = array();
		if(isset($this->request->get['shift_id'])){
			$shift_id = $this->request->get['shift_id'];
	        $this->load->model('transaction/transaction');
			$shift_data = $this->model_transaction_transaction->getshiftdata($this->request->get['shift_id']);
			if(isset($shift_data['shift_id'])){
				$shift_data['in_time'] = $shift_data['in_time'];
				$shift_data['out_time'] = $shift_data['out_time'];
				$jsondata['shift_data'] = $shift_data;
				$jsondata['status'] = 1;
			} elseif(isset($this->request->get['transaction_id'])){
				$trans_data = $this->db->query("SELECT * FROM `oc_transaction` WHERE `transaction_id` = '".$this->request->get['transaction_id']."' ")->row;
				$shift_data['in_time'] = $trans_data['shift_intime'];
				$shift_data['out_time'] = $trans_data['shift_outtime'];
				$jsondata['shift_data'] = $shift_data;
				$jsondata['status'] = 1;
			} else {
				if(isset($this->request->get['dot']) && $this->request->get['dot'] != '0000-00-00' && isset($this->request->get['emp_code']) && $this->request->get['emp_code'] != ''){
					$emp_code = $this->request->get['emp_code'];
					$day_date = date('j', strtotime($this->request->get['dot']));
					$month = date('n', strtotime($this->request->get['dot']));
					$year = date('Y', strtotime($this->request->get['dot']));
					$shift_schedule_datas = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$emp_code."' AND `month` = '".$month."' AND `year` = '".$year."' ");
					if($shift_schedule_datas->num_rows > 0){
						$shift_schedule_data = $shift_schedule_datas->row[$day_date];
						$shift_exp = explode('_', $shift_schedule_data);
						if(isset($shift_exp[2])){
							$shift_id = $shift_exp[2];
						} else {
							$shift_id = $shift_exp[1];
						}
						$shift_data = $this->model_transaction_transaction->getshiftdata($shift_id);
						if(isset($shift_data['shift_id'])){
							$shift_data['in_time'] = $shift_data['in_time'];
							$shift_data['out_time'] = $shift_data['out_time'];
							$jsondata['shift_data'] = $shift_data;
							$jsondata['status'] = 1;
						}
					}
				}
			}
		}
	    $this->response->setOutput(Json_encode($jsondata));
	}
}
?>