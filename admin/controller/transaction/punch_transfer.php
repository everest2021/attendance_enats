<?php    
class ControllerTransactionPunchTransfer extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('transaction/punch_transfer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/punch_transfer');

		$this->getList();
	}

	public function insert() {
		$this->language->load('transaction/punch_transfer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/punch_transfer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_transaction_punch_transfer->addpunch_transfer($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			// if (isset($this->request->get['emp_name'])) {
			// 	$url .= '&emp_name=' . $this->request->get['emp_name'];
			// }

			if (isset($this->request->get['emp_name'])) {
				$url .= '&emp_name=' . $this->request->get['emp_name'];
			}

			if (isset($this->request->get['emp_code'])) {
				$url .= '&emp_code=' . $this->request->get['emp_code'];
			}



			// if (isset($this->request->get['filter_department'])) {
			// 	$url .= '&filter_department=' . $this->request->get['filter_department'];
			// }

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['filter_shift'])) {
				$url .= '&filter_shift=' . $this->request->get['filter_shift'];
			}

			if (isset($this->request->get['filter_to_device'])) {
				$url .= '&filter_to_device=' . $this->request->get['filter_to_device'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('transaction/punch_transfer', 'token=' . $this->session->data['token'] . $url . '&emp_name=' . $this->request->post['emp_name'], 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('transaction/punch_transfer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/punch_transfer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			$this->model_transaction_punch_transfer->editpunch_transfer($this->request->get['emp_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['emp_name'])) {	
				$url .= '&emp_name=' . $this->request->get['emp_name'];
			}

			if (isset($this->request->get['emp_code'])) {
				$url .= '&emp_code=' . $this->request->get['emp_code'];
			}

			// if (isset($this->request->get['filter_department'])) {
			// 	$url .= '&filter_department=' . $this->request->get['filter_department'];
			// }

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['filter_shift'])) {
				$url .= '&filter_shift=' . $this->request->get['filter_shift'];
			}

			if (isset($this->request->get['filter_to_device'])) {
				$url .= '&filter_to_device=' . $this->request->get['filter_to_device'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if(isset($this->request->get['return'])){
				$this->redirect($this->url->link('transaction/punch_transfer_data', 'token=' . $this->session->data['token'].'&h_name='.$this->request->post['emp_name'].'&h_name_id='.$this->request->get['id'], 'SSL'));
			} else {
				$this->redirect($this->url->link('transaction/punch_transfer', 'token=' . $this->session->data['token'] . $url , 'SSL'));
			}
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('transaction/punch_transfer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/punch_transfer');
		 // echo "<pre>";print_r($this->request->post);exit;
		if (isset($this->request->post['selected']) && $this->validateDelete()) {	//echo"inn1";exit;
			foreach ($this->request->post['selected'] as $emp_id) {
				$this->model_transaction_punch_transfer->deletepunch_transfer($emp_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['emp_name'])) {
				$url .= '&emp_name=' . $this->request->get['emp_name'];
			}

			if (isset($this->request->get['emp_code'])) {
				$url .= '&emp_code=' . $this->request->get['emp_code'];
			}

			// if (isset($this->request->get['filter_department'])) {
			// 	$url .= '&filter_department=' . $this->request->get['filter_department'];
			// }

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['filter_shift'])) {
				$url .= '&filter_shift=' . $this->request->get['filter_shift'];
			}

			if (isset($this->request->get['filter_to_device'])) {
				$url .= '&filter_to_device=' . $this->request->get['filter_to_device'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('transaction/punch_transfer', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		} elseif(isset($this->request->get['emp_id']) && $this->validateDelete()){ 	//echo"inn2";exit;
			$this->model_transaction_punch_transfer->deletepunch_transfer($this->request->get['emp_id']);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['emp_name'])) {
				$url .= '&emp_name=' . $this->request->get['emp_name'];
			}

			if (isset($this->request->get['emp_code'])) {
				$url .= '&emp_code=' . $this->request->get['emp_code'];
			}

			// if (isset($this->request->get['filter_department'])) {
			// 	$url .= '&filter_department=' . $this->request->get['filter_department'];
			// }

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['filter_shift'])) {
				$url .= '&filter_shift=' . $this->request->get['filter_shift'];
			}

			if (isset($this->request->get['filter_to_device'])) {
				$url .= '&filter_to_device=' . $this->request->get['filter_to_device'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('transaction/punch_transfer', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	public function export_depthead(){
		$this->language->load('transaction/punch_transfer');
		$this->load->model('transaction/punch_transfer');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_code'])) {
			$filter_code = $this->request->get['filter_code'];
		} else {
			$filter_code = '';
		}

		// if (isset($this->request->get['filter_department'])) {
		// 	$filter_department = $this->request->get['filter_department'];
		// } else {
		// 	$filter_department = '';
		// }

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = $this->request->get['filter_unit'];
		} else {
			$filter_unit = '';
		}

		if (isset($this->request->get['filter_shift'])) {
			$filter_shift = $this->request->get['filter_shift'];
		} else {
			$filter_shift = '';
		}

		if (isset($this->request->get['filter_to_device'])) {
			$filter_to_device = $this->request->get['filter_to_device'];
		} else {
			$filter_to_device = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		$data = array(
			'filter_name' => $filter_name,
			'filter_code' => $filter_code,
			'filter_unit_id' => $filter_unit,
			// 'filter_department_id' => $filter_department,
			'filter_shift_id' => $filter_shift,
			'sort'  => $sort,
			'order' => $order,
		);
		
        //$punch_transfer_total = $this->model_transaction_punch_transfer->getTotalpunch_transfers($data);
		$fp = fopen(DIR_DOWNLOAD . "punch_transfer_list.csv", "w"); 
		$row = array();
		$row = $this->model_transaction_punch_transfer->getpunch_transfers($data);
		$line = "";
		$comma = "";
		
		$line .= $comma . '"' . str_replace('"', '""', "First Name") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Middle Name") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Last Name") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Shift") . '"';
		$comma = ",";
		// $line .= $comma . '"' . str_replace('"', '""', "Shift Id") . '"';
		// $comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Designation") . '"';
		$comma = ",";
		// $line .= $comma . '"' . str_replace('"', '""', "Department") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Unit") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Gender") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Martial Status") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Blood Group") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Reporting Head") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Emp Code") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Date of Birth") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Date of Joining") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Date of Confirmation") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Date of Left") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Previous Company UAN Number") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Gross Salary") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Status") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Bank Name") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Account Number") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "IFSC Code") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Aadhar Card No") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Email Id") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Personal Number") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Emergency Number") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Emergency Contact Person") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Address") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Education") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Year of Experience") . '"';
		$comma = ",";
		$line .= "\n";
		fputs($fp, $line);
		$i ='1';
		foreach($row as $key => $value) { 
			
			$shift_data = $this->db->query("SELECT `name`, `shift_id` FROM " . DB_PREFIX . "shift WHERE shift_id = '" . (int)$value['shift_id'] . "' ");
			$shift = '';
			$shift_id = '';
			if($shift_data->num_rows > 0){
				$value['shift'] = $shift_data->row['name'];
				$value['shift_id'] = $shift_data->row['shift_id'];
			}	
			$status_value= $value['status'];
			if($status_value == '1') {
				$value['status'] = 'Active';
			} else {
				$value['status'] = 'Inactive';
			} 	
			if($value['dob'] != '0000-00-00'){
				$dob_act = new DateTime($value['dob']);
				$dob = date_format($dob_act,"d/m/Y");
			} else {
				$dob = '';
			}
			if($value['doj'] != '0000-00-00'){
				$doj_act = new DateTime($value['doj']);
				$doj = date_format($doj_act,"d/m/Y");
			} else {
				$doj = '';
			}
			if($value['doc'] != '0000-00-00'){
				$doc_act = new DateTime($value['doc']);
				$doc = date_format($doc_act,"d/m/Y");
			} else {
				$doc = '';
			}
			if($value['dol'] != '0000-00-00'){
				$dol_act = new DateTime($value['dol']);
				$dol = date_format($dol_act,"d/m/Y");
			} else {
				$dol = '';
			}
			if($value['gender'] == 'Male'){
				$value['gender'] = 'Male';
			} elseif($value['gender'] == 'Female'){
				$value['gender'] = 'Female';
			} else {
				$value['gender'] = '';
			}
			if ($value['pre_com_uan_no'] != '' && is_numeric($value['pre_com_uan_no'])) {
				$value['pre_com_uan_no'] = '`'.$value['pre_com_uan_no'];
			}
			if ($value['ac_no'] != ''  && is_numeric($value['ac_no'])) {
				$value['ac_no'] = '`'.$value['ac_no'];
			}
			if ($value['aadhar_no'] != ''  && is_numeric($value['aadhar_no'])) {
				$value['aadhar_no'] = '`'.$value['aadhar_no'];
			}
			$comma = "";
			$line = "";
			//$line .= $comma . '"' . str_replace('"', '""', $value['emp_code']) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['first_name'])) . '"';
			$comma = ",";
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['middile_name'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['last_name'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['shift'])) . '"';
			//$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['shift_id'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['designation'])) . '"';
			// $line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['department'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['unit'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['gender'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['martial_status'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['blood_group'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['reporting_to_name'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['emp_code'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', $dob) . '"';
			$line .= $comma . '"' . str_replace('"', '""', $doj) . '"';
			$line .= $comma . '"' . str_replace('"', '""', $doc) . '"';
			$line .= $comma . '"' . str_replace('"', '""', $dol) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['pre_com_uan_no'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['Gross_salary'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['status'])) . '"';
			// $line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['bank_name'])) . '"';
			// $line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['ac_no'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['ifsc_code'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['aadhar_no'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['email'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['personal_no'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['emg_no'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['emg_per'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['address'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['education'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['year_experience'])) . '"';
			$i++;
			$line .= "\n";
			//echo $line;exit;
			fputs($fp, $line);
		}
		//echo $line;exit;
		fclose($fp);
		$filename = "punch_transfer_list.csv";
		$file_path = DIR_DOWNLOAD . $filename;
		//$file_path = '/Library/WebServer/Documents/riskmanagement/'.$filename;
		$html = file_get_contents($file_path);
		header('Content-type: text/html');
		header('Content-Disposition: attachment; filename='.$filename);
		echo $html;
		exit;
	}

	protected function getList() {
		$this->data['reporting_to_name'] = '';
		
		
		// echo '<pre>';
		// print_r($this->data['reporting_to_name']);
		// exit;

		if (isset($this->request->get['emp_name'])) {
			$emp_name = $this->request->get['emp_name'];
		} else {
			$emp_name = '';
		}

		if (isset($this->request->get['emp_code'])) {
			$emp_code = $this->request->get['emp_code'];
		} else {
			$emp_code = '';
		}

		if (isset($this->request->get['to_device'])) {
			$to_device = $this->request->get['to_device'];
		} else {
			$to_device = '';
		}

		if (isset($this->request->get['from_device'])) {
			$from_device = $this->request->get['from_device'];
		} else {
			$from_device = '';
		}

		

		if (isset($this->request->get['filter_to_device'])) {
			$filter_to_device = $this->request->get['filter_to_device'];
		} else {
			$filter_to_device = '1';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'emp_code';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		
		if (isset($this->request->get['filter_to_device'])) {
			$url .= '&filter_to_device=' . $this->request->get['filter_to_device'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('transaction/punch_transfer', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('transaction/punch_transfer/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('transaction/punch_transfer/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	
		$this->data['export_depthead'] = $this->url->link('transaction/punch_transfer/export_depthead', 'token=' . $this->session->data['token'] . $url, 'SSL');	
		
		$this->data['punch_transfers'] = array();

		$data = array(
			'emp_name' => $emp_name,
			'emp_code' => $emp_code,
			'from_device' => $from_device,
			'to_device' => $to_device,
			
			// 'filter_status' => $filter_status,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$punch_transfer_total = $this->model_transaction_punch_transfer->getTotalpunch_transfers($data);

		
		$results = $this->model_transaction_punch_transfer->gettransfer_punchs($data);

		foreach ($results as $result) { 
  		   // echo "<pre>";print_r($result);exit;
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('transaction/punch_transfer/update', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, 'SSL')
			);

			
			

 			
 			// if($result['status'] == '1'){
 			// 	$status = 'Active';
 			// } else {
 			// 	$status = 'In Active';
 			// }

			$this->data['punch_transfers'][] = array(
				'id' 			  => $result['id'],
				'emp_name'        => $result['emp_name'],
				'from_device'     => $result['from_device_name'],
				'to_device'       => $result['to_device_name'],
				'emp_code' 	      => $result['emp_code'],
				'selected'        => isset($this->request->post['selected']) && in_array($result['id'], $this->request->post['selected']),
				
				
				'action'          => $action
			);
		}
				

		$results = $this->model_transaction_punch_transfer->getUnit();
		
		
		$results = $this->model_transaction_punch_transfer->getShift();
		$shifts = array();
		foreach ($results as $dvalue) {
			$shifts[$dvalue['shift_id']]= $dvalue['name'];
		}
		$this->data['shifts'] = $shifts;
		// echo '<pre>';
		// print_r($data['units']);
		// exit();

		$this->data['token'] = $this->session->data['token'];	

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_delete'] = $this->language->get('text_delete');
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_emp'] = $this->language->get('entry_emp');
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_trainer'] = $this->language->get('column_trainer');
		$this->data['column_action'] = $this->language->get('column_action');		

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		$this->data['button_filter'] = $this->language->get('button_filter');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		
		if (isset($this->request->get['filter_to_device'])) {
			$url .= '&filter_to_device=' . $this->request->get['filter_to_device'];
		}

		if (isset($this->request->get['filter_trainer'])) {
			$url .= '&filter_trainer=' . $this->request->get['filter_trainer'];
		}

		if (isset($this->request->get['filter_trainer_id'])) {
			$url .= '&filter_trainer_id=' . $this->request->get['filter_trainer_id'];
		}

		$this->data['sort_name'] = $this->url->link('transaction/punch_transfer', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_emp'] = $this->url->link('transaction/punch_transfer', 'token=' . $this->session->data['token'] . '&sort=emp' . $url, 'SSL');
		$this->data['sort_code'] = $this->url->link('transaction/punch_transfer', 'token=' . $this->session->data['token'] . '&sort=emp_code' . $url, 'SSL');
		// $this->data['sort_department'] = $this->url->link('transaction/punch_transfer', 'token=' . $this->session->data['token'] . '&sort=department' . $url, 'SSL');
		$this->data['sort_unit'] = $this->url->link('transaction/punch_transfer', 'token=' . $this->session->data['token'] . '&sort=unit' . $url, 'SSL');
		$this->data['sort_shift'] = $this->url->link('transaction/punch_transfer', 'token=' . $this->session->data['token'] . '&sort=shift_id' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('transaction/punch_transfer', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		// if (isset($this->request->get['filter_department'])) {
		// 	$url .= '&filter_department=' . $this->request->get['filter_department'];
		// }

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['filter_shift'])) {
			$url .= '&filter_shift=' . $this->request->get['filter_shift'];
		}

		if (isset($this->request->get['filter_to_device'])) {
			$url .= '&filter_to_device=' . $this->request->get['filter_to_device'];
		}

		if (isset($this->request->get['filter_trainer'])) {
			$url .= '&filter_trainer=' . $this->request->get['filter_trainer'];
		}

		if (isset($this->request->get['filter_trainer_id'])) {
			$url .= '&filter_trainer_id=' . $this->request->get['filter_trainer_id'];
		}

		$pagination = new Pagination();
		$pagination->total = $punch_transfer_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/punch_transfer', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['emp_name'] = $emp_name;
		$this->data['emp_code'] = $emp_code;
	 
		$this->data['filter_to_device'] = $filter_to_device;

		$this->template = 'transaction/punch_transfer_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		/*echo "<pre>";
		print_r($this->request->post);
	    exit;*/
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_punch_transfer_code'] = $this->language->get('entry_punch_transfer_code');
		$this->data['entry_card_no'] = $this->language->get('entry_card_no');
		$this->data['entry_device_serial_no'] = $this->language->get('entry_device_serial_no');
		$this->data['entry_device_punch_transfer_code'] = $this->language->get('entry_device_punch_transfer_code');
		$this->data['entry_remove'] = $this->language->get('entry_remove');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['tab_general'] = $this->language->get('tab_general');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}

		if (isset($this->error['card_number'])) {
			$this->data['error_card_number'] = $this->error['card_number'];
		} else {
			$this->data['error_card_number'] = '';
		}

		

		if (isset($this->error['status'])) {
			$this->data['error_status'] = $this->error['status'];
		} else {
			$this->data['error_status'] = '';
		}

		$url = '';

		// if (isset($this->request->get['emp_id'])) { 
		// 	$url .= '&emp_id=' . $this->request->get['emp_id'];
		// }

		if (isset($this->request->get['emp_name'])) {
			$url .= '&emp_name=' . $this->request->get['emp_name'];
		}

		if (isset($this->request->get['emp_code'])) {
			$url .= '&emp_code=' . $this->request->get['emp_code'];
		}


		if (isset($this->request->get['filter_to_device'])) {
			$url .= '&filter_to_device=' . $this->request->get['filter_to_device'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['emp_name'])) {
			$url .= '&emp_name=' . $this->request->get['emp_name'];
		}


		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('transaction/punch_transfer', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$punch_transfer_info = $this->model_transaction_punch_transfer->getpunch_transfer($this->request->get['id']);

			// echo "<pre>";
			// print_r($punch_transfer_info);
			// exit();
		}

		// if (isset($this->request->post['select_cmd'])) {
		// 	$this->data['select_cmd'] = $this->request->post['select_cmd'];
		// } elseif (!empty($device_commands_info)) {
		// 	$this->data['select_cmd'] = $device_commands_info['short_cmd_name'];
		// } else {
		// 	$this->data['select_cmd'] = '';
		// }

		$this->data['token'] = $this->session->data['token'];

		// if (isset($this->request->post['emp_id'])) {
		// 	$this->data['emp_id'] = $this->request->post['emp_id'];
		// } elseif (!empty($punch_transfer_info)) {
		// 	$this->data['emp_id'] = $punch_transfer_info['emp_id'];
		// } else {	
		// 	$this->data['emp_id'] = '';
		// }
		// echo "<pre>";print_r($punch_transfer_info);exit;
		if (isset($this->request->post['selected_cmd'])) {
			$this->data['selected_cmd'] = $this->request->post['from_device'];
		} elseif (!empty($punch_transfer_info)) {
			$this->data['selected_cmd'] = $punch_transfer_info['from_device_name'];
		} else {	
			$this->data['selected_cmd'] = '';
		}

		if (isset($this->request->post['select_cmd'])) {
			$this->data['select_cmd'] = $this->request->post['to_device'];
		} elseif (!empty($punch_transfer_info)) {
			$this->data['select_cmd'] = $punch_transfer_info['to_device_name'];
		} else {	
			$this->data['select_cmd'] = '';
		}

		if (isset($this->request->post['emp_name'])) {
			$this->data['emp_name'] = $this->request->post['emp_name'];
		} elseif (!empty($punch_transfer_info)) {
			$this->data['emp_name'] = $punch_transfer_info['emp_name'];
		} else {	
			$this->data['emp_name'] = '';
		}


		if (isset($this->request->post['emp_code'])) {
			$this->data['emp_code'] = $this->request->post['emp_code'];
		} elseif (!empty($punch_transfer_info)) {
			$this->data['emp_code'] = $punch_transfer_info['emp_code'];
		} else {	
			$this->data['emp_code'] = '';
		}


		if (isset($this->request->post['to_device_id'])) {
			$this->data['to_device_id'] = $this->request->post['to_device_id'];
		} elseif (!empty($punch_transfer_info)) {
			$this->data['to_device_id'] = $punch_transfer_info['to_device_id'];
		} else {	
			$this->data['to_device_id'] = '';
		}


		if (isset($this->request->post['to_device_name'])) {
			$this->data['to_device_name'] = $this->request->post['to_device_name'];
		} elseif (!empty($punch_transfer_info)) {
			$this->data['to_device_name'] = $punch_transfer_info['to_device_name'];
		} else {	
			$this->data['to_device_name'] = '';
		}



		if (isset($this->request->post['from_device_id'])) {
			$this->data['from_device_id'] = $this->request->post['from_device_id'];
		} elseif (!empty($punch_transfer_info)) {
			$this->data['from_device_id'] = $punch_transfer_info['from_device_id'];
		} else {	
			$this->data['from_device_id'] = '';
		}



		if (isset($this->request->post['from_device_name'])) {
			$this->data['from_device_name'] = $this->request->post['from_device_name'];
		} elseif (!empty($punch_transfer_info)) {
			$this->data['from_device_name'] = $punch_transfer_info['from_device_name'];
		} else {	
			$this->data['from_device_name'] = '';
		}




			// SELECT * FROM `prospectus` WHERE pros_id='".$svalue['pros_id']."'
		$this->data['devices'] = $this->db->query("SELECT * FROM `oc_devices` WHERE 1 = 1 ")->rows;
		//echo "<pre>";print_r($this->data['devices']);exit;

		// if (isset($this->request->post['emp_code'])) {
		// 	$this->data['punch_transfer_code'] = $this->request->post['emp_code'];
		// } elseif (!empty($punch_transfer_info)) {
		// 	$this->data['punch_transfer_code'] = $punch_transfer_info['device_emp_code'];
		// } else {	
		// 	$emp_codes = $this->db->query("SELECT `device_emp_code` FROM `oc_punch_transfer` ORDER BY `device_emp_code` DESC LIMIT 1");
		// 	if($emp_codes->num_rows > 0){
		// 		$emp_code = $emp_codes->row['device_emp_code'] + 1;
		// 	} else {
		// 		$emp_code = '1000';
		// 	}
		// 	$this->data['punch_transfer_code'] = $emp_code;
		// }


		// if (isset($this->request->post['punch_transfer_code'])) {
		// 	$this->data['punch_transfer_code'] = $this->request->post['punch_transfer_code'];
		// } elseif (!empty($punch_transfer_info)) {
		// 	$this->data['punch_transfer_code'] = $punch_transfer_info['device_emp_code'];
		// } else {	
		// 	$this->data['punch_transfer_code'] = '';
		// }

		// if (isset($this->request->post['emp_code'])) {
		// 	$this->data['emp_code'] = $this->request->post['emp_code'];
		// } elseif (!empty($punch_transfer_info)) {
		// 	$this->data['emp_code'] = $punch_transfer_info['card'];
		// } else {	
		// 	$this->data['emp_code'] = '';
		// }
		// echo'<pre>';
		// print_r($punch_transfer_info);
		// exit;
		if (isset($this->request->post['emp_name'])) {
			$this->data['emp_name'] = $this->request->post['emp_name'];
		} elseif (!empty($punch_transfer_info)) {
			$this->data['emp_name'] = $punch_transfer_info['emp_name'];
		} else {	
			$this->data['emp_name'] = '';
		}

		if (isset($this->request->post['middle_name'])) {
			$this->data['middle_name'] = $this->request->post['middle_name'];
		} elseif (!empty($punch_transfer_info)) {
			$this->data['middle_name'] = $punch_transfer_info['emp_name'];
		} else {	
			$this->data['middle_name'] = '';
		}


		if (isset($this->request->post['last_name'])) {
			$this->data['last_name'] = $this->request->post['last_name'];
		} elseif (!empty($punch_transfer_info)) {
			$this->data['last_name'] = $punch_transfer_info['emp_name'];
		} else {	
			$this->data['last_name'] = '';
		}
		//echo"<pre>";print_r($punch_transfer_info);exit;
		// if (isset($this->request->post['device_serial_no'])) {
		// 	$this->data['device_serial_no'] = $this->request->post['device_serial_no'];
		// } elseif (!empty($punch_transfer_info)) {
		// 	$this->data['device_serial_no'] = $punch_transfer_info['device_serial_no'];
		// } else {	
		// 	$this->data['device_serial_no'] = '';
		// }

		

		if (!isset($this->request->get['id'])) {
			$this->data['action'] = $this->url->link('transaction/punch_transfer/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			if(isset($this->request->get['return'])){
				$this->data['action'] = $this->url->link('transaction/punch_transfer/update', 'token=' . $this->session->data['token'] . '&emp_id=' . $this->request->get['id'] . '&return=1', 'SSL');
			} else {
				$this->data['action'] = $this->url->link('transaction/punch_transfer/update', 'token=' . $this->session->data['token'] . '&emp_id=' . $this->request->get['id'] . $url, 'SSL');
			}
		}

		if(isset($this->request->get['return'])){
			$this->data['cancel'] = $this->url->link('transaction/punch_transfer_data', 'token=' . $this->session->data['token'] . '&h_name=' . $this->data['name'] . '&h_name_id=' . $this->request->get['id'], 'SSL');
		} else {
			$this->data['cancel'] = $this->url->link('transaction/punch_transfer', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}

		$this->data['weeks'] = array(
			'1' => 'Sunday',
			'2' => 'Monday',
			'3' => 'Tuesday',
			'4' => 'Wednesday',
			'5' => 'Thursday',
			'6' => 'Friday',
			'7' => 'Saturday',
		);
		
		$this->template = 'transaction/punch_transfer_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}  

	public function upload() {
		$this->load->language('transaction/punch_transfer');
		$json = array();
		// Check user has permission
		if (!$this->user->hasPermission('modify', 'transaction/punch_transfer')) {
			$json['error'] = $this->language->get('error_permission');
		}
		$file_show_path = HTTP_transaction.'download/';
		$file_upload_path = DIR_DOWNLOAD.'/';
		$image_name = $this->request->get['image_name'].'_'.date('YmdHis');
		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				// Sanitize the filename
				$raw_file_name = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));
				$img_extension = strtolower(substr(strrchr($raw_file_name, '.'), 1));

				$filename = $image_name.'.'.$img_extension;//basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

				// $this->log->write(print_r($this->request->files, true));
				// $this->log->write($image_name);
				// $this->log->write($img_extension);
				// $this->log->write($filename);

				// Validate the filename length
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}

				// Allowed file extension types
				$allowed = array();

				$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

				$filetypes = explode("\n", $extension_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}
				$allowed[] = 'jpg';
				$allowed[] = 'jpeg';
				$allowed[] = 'png';
				$allowed[] = 'pdf';

				$this->log->write(print_r($allowed, true));

				if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Allowed file mime types
				$allowed = array();

				$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

				$filetypes = explode("\n", $mime_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				//$this->log->write(print_r($this->request->files,true));

				if (!in_array($this->request->files['file']['type'], $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Check to see if any PHP files are trying to be uploaded
				$content = file_get_contents($this->request->files['file']['tmp_name']);

				if (preg_match('/\<\?php/i', $content)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Return any upload error
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
				}
			} else {
				$json['error'] = $this->language->get('error_upload');
			}
		}

		if (!$json) {
			$file = $filename;
			move_uploaded_file($this->request->files['file']['tmp_name'], $file_upload_path . $file);
			$destFile = $file_upload_path . $file;
			chmod($destFile, 0777);
			$json['filename'] = $file;
			$json['link_href'] = $file_show_path . $file;

			$json['success'] = $this->language->get('text_upload');
		}
		//sleep(5);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	protected function validateForm() {
		$this->load->model('transaction/punch_transfer');

		if (!$this->user->hasPermission('modify', 'transaction/punch_transfer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// if(strlen(utf8_decode(trim($this->request->post['name']))) < 1 || strlen(utf8_decode(trim($this->request->post['name']))) > 255){
		// 	$this->error['name'] = $this->language->get('error_name');
		// }

		// if(strlen(utf8_decode(trim($this->request->post['name']))) < 1 || strlen(utf8_decode(trim($this->request->post['name']))) > 255){
		// 	$this->error['name'] = $this->language->get('error_name');
		// }

		// if(strlen(utf8_decode(trim($this->request->post['name']))) < 1 || strlen(utf8_decode(trim($this->request->post['name']))) > 255){
		// 	$this->error['name'] = $this->language->get('error_name');
		// }

		// if(strlen(utf8_decode(trim($this->request->post['name']))) < 1 || strlen(utf8_decode(trim($this->request->post['name']))) > 255){
		// 	$this->error['name'] = $this->language->get('error_name');
		// }

		// echo '<pre>';
		// print_r($this->error);
		// exit;

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'transaction/punch_transfer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('transaction/punch_transfer');

		// if(isset($this->request->post['selected'])){
		// 	foreach ($this->request->post['selected'] as $punch_transfer_id) {
		// 		$punch_transfer_total = $this->model_transaction_punch_transfer->getTotaltreatmentBypunch_transferId($punch_transfer_id);

		// 		if ($punch_transfer_total) {
		// 			$this->error['warning'] = sprintf($this->language->get('error_punch_transfer'), $punch_transfer_total);
		// 		}	
		// 	}
		// } elseif(isset($this->request->get['punch_transfer_id'])){
		// 	$punch_transfer_total = $this->model_transaction_punch_transfer->getTotaltreatmentBypunch_transferId($this->request->get['punch_transfer_id']);

		// 	if ($punch_transfer_total) {
		// 		$this->error['warning'] = sprintf($this->language->get('error_punch_transfer'), $punch_transfer_total);
		// 	}
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}  
	}

	public function getemp_name() {
		$json = array();
		// echo "<pre>";print_r($this->request->get);exit;

		if (isset($this->request->get['emp_name'])) {
			$this->load->model('transaction/punch_transfer');

			if (isset($this->session->data['emp_code']) && !$this->user->isAdmin()) {
				$device_emp_code = $this->session->data['emp_code'];
			} else {
				$emp_code = '';
			}

			$data = array(
				'emp_name' => $this->request->get['emp_name'],
				'device_emp_code' => $emp_code,
				'start'       => 0,
				'limit'       => 10
			);

			$results = $this->model_transaction_punch_transfer->getpunch_transfers($data);
			 // echo "<pre>";print_r($results);exit;
			foreach ($results as $result) {
				$json[] = array(
					'id' 		  => $result['emp_id'],
					'device_emp_code' => $result['device_emp_code'], 
					'emp_name'        => strip_tags(html_entity_decode($result['emp_name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['emp_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete_trainer() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('transaction/trainer');

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_transaction_trainer->gettrainers($data);

			foreach ($results as $result) {
				$json[] = array(
					'trainer_id' => $result['trainer_id'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}

	public function getshift_id() {
		$json = array();
		$json['shift_id'] = 2;
		if (isset($this->request->get['filter_unit_id'])) {
			$filter_unit_id = $this->request->get['filter_unit_id'];
			$shift_id = 2;
			$shift_datas = $this->db->query("SELECT `shift_id` FROM `oc_shift_weekoff` WHERE `unit_id` = '".$filter_unit_id."' ");
			if($shift_datas->num_rows > 0){
				$shift_id = $shift_datas->row['shift_id'];
			} else {
				$shift_id = 2;
			}
			$json['shift_id'] = $shift_id;		
		}
		$this->response->setOutput(json_encode($json));
	}


}
?>