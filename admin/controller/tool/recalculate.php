<?php 
class ControllerToolRecalculate extends Controller { 
	private $error = array();

	public function index() {		
		if(isset($this->request->get['filter_date_start'])){
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			/*
			$year = date('Y');
			$month = date('n');
			$day = date('j');
			if($month == 1){
				if($day >= 26 && $day <= 31){
					$prev_month = $month;
					$prev_year = $year;
				} else {
					$prev_month = 12;
					$prev_year = $year - 1;
				}
			} else {
				if($day >= 26 && $day <= 31){
					$prev_month = $month;
					$prev_year = $year;
				} else {
					$prev_month = $month - 1;
					$prev_year = $year;
				}
			}
			$filter_date_start = sprintf("%04d-%02d-%02d", $prev_year, $prev_month, '26');
			*/
			$filter_date_start = date('Y-m-01');
		}

		if(isset($this->request->get['filter_date_end'])){
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			/*
			$year = date('Y');
			$month = date('n');
			$day = date('j');
			if($month == 12){
				if($day >= 26 && $day <= 31){
					$next_month = 1;
					$next_year = $year + 1;
				} else {
					$next_month = 12;
					$next_year = $year;
				}
			} else {
				if($day >= 26 && $day <= 31){
					$next_month = $month + 1;
					$next_year = $year;
				} else {
					$next_month = $month;
					$next_year = $year;
				}
			}
			$filter_date_end = sprintf("%04d-%02d-%02d", $next_year, $next_month, '25');
			*/
			$to = date('Y-m-d');
			$filter_date_end = date('Y-m-d', strtotime($to . "-0 day"));
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = html_entity_decode($this->request->get['filter_department']);
		} else {
			$filter_department = 0;
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = $this->request->get['filter_unit'];
		} else {
			$filter_unit = 0;
		}

		$this->language->load('tool/recalculate');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->session->data['error'])) {
			$this->data['error_warning'] = $this->session->data['error'];

			unset($this->session->data['error']);
		} elseif (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),     		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('tool/recalculate', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$locations = $this->db->query("SELECT * FROM `oc_unit` ")->rows;
		$unit_data['0'] = 'All'; 
		foreach($locations as $lkey => $lvalue){
			$unit_data[$lvalue['unit_id']] = $lvalue['unit'];
		}
		$this->data['unit_data'] = $unit_data;
		
		$department_datas = $this->db->query("SELECT * FROM `oc_department` ")->rows;
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['d_name'];
		}
		$this->data['department_data'] = $department_data;

		$this->data['recalculate'] = $this->url->link('tool/recalculate/recalculate', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['token'] = $this->session->data['token'];
		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_department'] = $filter_department;
		$this->data['filter_unit'] = $filter_unit;

		$this->template = 'tool/recalculate.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function recalculate_data() {
		$this->language->load('tool/recalculate');

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');
		
		

		if(isset($this->request->get['filter_date_start'])){
			$filter_date_start = $this->request->get['filter_date_start'];
			$filter_date_start_constant = $this->request->get['filter_date_start'];
		} else {
			$current_month = date('m');
			$current_year = date('Y');
			$filter_date_start = $current_year.'-'.$current_month.'-26';
			$filter_date_start_constant = $current_year.'-'.$current_month.'-26';
		}

		if(isset($this->request->get['filter_date_end'])){
			$filter_date_end = $this->request->get['filter_date_end'];
			$filter_date_end_constant = $this->request->get['filter_date_end'];
		} else {
			$current_month = date('m');
			if($current_month == 12){
				$current_month = 1;
				$current_year = date('Y') + 1;
			} else {
				$current_month = $current_month + 1;
				$current_year = date('Y');
			}
			$current_month = sprintf("%02d", $current_month);
			$number_days = cal_days_in_month(CAL_GREGORIAN, $current_month, $current_year);
			$filter_date_end = $current_year.'-'.$current_month.'-25';
			$filter_date_end_constant = $current_year.'-'.$current_month.'-25';
			//$filter_date_end = '2018-03-17';
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = html_entity_decode($this->request->get['filter_department']);
		} else {
			$filter_department = 0;
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = $this->request->get['filter_unit'];
		} else {
			$filter_unit = 0;
		}
		
		$employee_datas_sql = "SELECT `name`, `emp_code`, `department`, `department_id`, `unit`, `unit_id`, `dol` FROM `oc_employee` WHERE 1=1 ";
		if (!empty($filter_name_id)) {
			$employee_datas_sql .= " AND `emp_code` = '" . $this->db->escape(strtolower($filter_name_id)) . "'";
		}

		if (!empty($filter_department)) {
			$employee_datas_sql .= " AND LOWER(`department_id`) = '" . $this->db->escape(strtolower($filter_department)) . "'";
		}

		if (!empty($filter_unit)) {
			$employee_datas_sql .= " AND LOWER(`unit_id`) = '" . $this->db->escape(strtolower($filter_unit)) . "'";
		}
		//echo $employee_datas_sql;exit;
		$employee_datas = $this->db->query($employee_datas_sql);
		// echo '<pre>';
		// print_r($employee_datas);
		// exit;
		if($employee_datas->num_rows > 0){
			$days = array();
			$dayss = $this->GetDays($filter_date_start, $filter_date_end);
			//$start_date = '';
			//$end_date = '';
			//$exceed = 0;
			foreach ($dayss as $dkey => $dvalue) {
				//if(strtotime($dvalue) <= strtotime(date('Y-m-d'))){
					$dates = explode('-', $dvalue);
					$days[$dkey]['day'] = ltrim($dates[2], '0');
					$days[$dkey]['month'] = $dates[1];
					$days[$dkey]['year'] = $dates[0];
					$days[$dkey]['date'] = $dvalue;
					// if($dkey == 0){
					// 	$start_date = $dvalue;
					// }
					// $end_date = $dvalue;
				//} else {
					//$exceed = 1;
				//}
			}
			// echo '<pre>';
			// print_r($days);
			// exit;
			$cnt1 = 0;
			$date_string = '';
			$employee_string = '';
			$punch_date_array = array();
			//foreach($days as $dkey => $dvalue){
				foreach($employee_datas->rows as $rkey => $rvalue){
					if($cnt1 == 0){
						$employee_string .= $rvalue['emp_code'].', ';
					}
				}
				$cnt1 ++;
			//}
			
			$employee_string = rtrim($employee_string, ', ');
			$date_string = rtrim($date_string, ', ');
			$date_string = "'" . str_replace(",", "','", $date_string) . "'";
			
			$batch_id = 0;
			foreach($employee_datas->rows as $rkey => $rvalue){
				$emp_code = $rvalue['emp_code'];
				foreach($days as $dkey => $dvalue){
					
					$filter_date_start = $dvalue['date'];
					$emp_name = $rvalue['name'];
					$day_date = date('j', strtotime($filter_date_start));
					$month = date('n', strtotime($filter_date_start));
					$year = date('Y', strtotime($filter_date_start));
					$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
					$shift_schedule = $this->db->query($update3)->row;
					$schedule_raw = explode('_', $shift_schedule[$day_date]);
					if(!isset($schedule_raw[2])){
						$schedule_raw[2]= 1;
					}
					if($schedule_raw[0] == 'S'){
						$shift_data = $this->getshiftdata($schedule_raw[1]);
						if(isset($shift_data['shift_id'])){
							$shift_intime = $shift_data['in_time'];
							$shift_outtime = $shift_data['out_time'];
							$shift_outtime_flexi = Date('H:i:s', strtotime($shift_data['out_time'] .' +60 minutes'));
							$shift_id = $shift_data['shift_id'];
							$shift_code = $shift_data['shift_code'];
							$day = date('j', strtotime($filter_date_start));
							$month = date('n', strtotime($filter_date_start));
							$year = date('Y', strtotime($filter_date_start));
							$trans_exist_sql = "SELECT `transaction_id`, `manual_status`, `leave_status` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
							$trans_exist = $this->db->query($trans_exist_sql);
							if($trans_exist->num_rows > 0){
								foreach($trans_exist->rows as $tkey => $tvalue){
									$transaction_id = $tvalue['transaction_id'];
									$leave_status = $tvalue['leave_status'];
									$manual_status = $tvalue['manual_status'];
									if($leave_status == 0 && $manual_status == 0){
										$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
										$this->db->query($sql);
										$this->log->write($sql);
									}
								}
							}
						} else {
							$shift_data = $this->getshiftdata('1');
							$shift_intime = $shift_data['in_time'];
							$shift_outtime = $shift_data['out_time'];
							$shift_outtime_flexi = Date('H:i:s', strtotime($shift_data['out_time'] .' +60 minutes'));
							$shift_id = $shift_data['shift_id'];
							$shift_code = $shift_data['shift_code'];
							$day = date('j', strtotime($filter_date_start));
							$month = date('n', strtotime($filter_date_start));
							$year = date('Y', strtotime($filter_date_start));
							$trans_exist_sql = "SELECT `transaction_id`, `manual_status`, `leave_status` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
							$trans_exist = $this->db->query($trans_exist_sql);
							if($trans_exist->num_rows > 0){
								foreach($trans_exist->rows as $tkey => $tvalue){
									$transaction_id = $tvalue['transaction_id'];
									$leave_status = $tvalue['leave_status'];
									$manual_status = $tvalue['manual_status'];
									if($leave_status == 0 && $manual_status == 0){
										$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
										$this->db->query($sql);
										$this->log->write($sql);
									}
								}
							}
						}
					} elseif ($schedule_raw[0] == 'W') {
						$shift_data = $this->getshiftdata($schedule_raw[2]);
						if(!isset($shift_data['shift_id'])){
							$shift_data = $this->getshiftdata('1');
						}
						$shift_intime = $shift_data['in_time'];
						$shift_outtime = $shift_data['out_time'];
						$shift_outtime_flexi = Date('H:i:s', strtotime($shift_data['out_time'] .' +60 minutes'));
						$shift_id = $shift_data['shift_id'];
						$shift_code = $shift_data['shift_code'];
						$day = date('j', strtotime($filter_date_start));
						$month = date('n', strtotime($filter_date_start));
						$year = date('Y', strtotime($filter_date_start));
						$trans_exist_sql = "SELECT `transaction_id`, `manual_status`, `leave_status` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
						$trans_exist = $this->db->query($trans_exist_sql);
						if($trans_exist->num_rows > 0){
							foreach($trans_exist->rows as $tkey => $tvalue){
								$transaction_id = $tvalue['transaction_id'];
								$leave_status = $tvalue['leave_status'];
								$manual_status = $tvalue['manual_status'];
								if($leave_status == 0 && $manual_status == 0){
									$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."' WHERE  `transaction_id` = '".$transaction_id."' ";
									$this->db->query($sql);
									$this->log->write($sql);
								}
							}
						}
					} elseif ($schedule_raw[0] == 'H') {
						$shift_data = $this->getshiftdata($schedule_raw[2]);
						if(!isset($shift_data['shift_id'])){
							$shift_data = $this->getshiftdata('1');
						}
						$shift_intime = $shift_data['in_time'];
						$shift_outtime = $shift_data['out_time'];
						$shift_outtime_flexi = Date('H:i:s', strtotime($shift_data['out_time'] .' +60 minutes'));
						$shift_id = $shift_data['shift_id'];
						$shift_code = $shift_data['shift_code'];
						$day = date('j', strtotime($filter_date_start));
						$month = date('n', strtotime($filter_date_start));
						$year = date('Y', strtotime($filter_date_start));
						$trans_exist_sql = "SELECT `transaction_id`, `manual_status`, `leave_status` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
						$trans_exist = $this->db->query($trans_exist_sql);
						if($trans_exist->num_rows > 0){
							foreach($trans_exist->rows as $tkey => $tvalue){
								$transaction_id = $tvalue['transaction_id'];
								$leave_status = $tvalue['leave_status'];
								$manual_status = $tvalue['manual_status'];
								if($leave_status == 0 && $manual_status == 0){
									$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."' WHERE  `transaction_id` = '".$transaction_id."' ";
									$this->db->query($sql);
									$this->log->write($sql);
								}
							}
						}
					} elseif ($schedule_raw[0] == 'HD') {
						$shift_data = $this->getshiftdata($schedule_raw[2]);
						if(!isset($shift_data['shift_id'])){
							$shift_data = $this->getshiftdata('1');
						}
						$shift_intime = $shift_data['in_time'];
						$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
						$shift_outtime_flexi = Date('H:i:s', strtotime($shift_outtime .' +60 minutes'));
						//$shift_outtime = Date('H:i:s', strtotime($shift_outtime .' +00 minutes'));
						$day = date('j', strtotime($filter_date_start));
						$month = date('n', strtotime($filter_date_start));
						$year = date('Y', strtotime($filter_date_start));
						$trans_exist_sql = "SELECT `transaction_id`, `manual_status`, `leave_status` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
						$trans_exist = $this->db->query($trans_exist_sql);
						if($trans_exist->num_rows > 0){
							foreach($trans_exist->rows as $tkey => $tvalue){
								$transaction_id = $tvalue['transaction_id'];
								$leave_status = $tvalue['leave_status'];
								$manual_status = $tvalue['manual_status'];
								if($leave_status == 0 && $manual_status == 0){
									$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `shift_outtime_flexi` = '".$shift_outtime_flexi."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '1', batch_id = '".$batch_id."', `department` = '".$rvalue['department']."', department_id = '".$rvalue['department_id']."', `unit` = '".$rvalue['unit']."', `unit_id` = '".$rvalue['unit_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
									$this->db->query($sql);
									$this->log->write($sql);
								}
							}
						}
					}
				}
			}
		}
		$this->session->data['success'] = 'You are done with process';
		$this->redirect($this->url->link('tool/recalculate', 'token=' . $this->session->data['token'], 'SSL'));
	}

	public function sortByOrder($a, $b) {
		$v1 = strtotime($a['fdate']);
		$v2 = strtotime($b['fdate']);
		return $v1 - $v2; // $v2 - $v1 to reverse direction
		// if ($a['punch_date'] == $b['punch_date']) {
			//return ($a['in_time'] > $b['in_time']) ? -1 : 1;;
		//}
		//return $a['punch_date'] - $b['punch_date'];
	}

	public function getshiftdata($shift_id) {
		$sql = "SELECT * FROM oc_shift WHERE 1=1 ";
		if($shift_id){
			$sql .= " AND `shift_id` = '".$shift_id."' ";
		}
		$query = $this->db->query($sql);
		//$query = query("SELECT * FROM oc_shift WHERE `shift_id` = '".$shift_id."' ", $conn);
		if($query->num_rows > 0){
			if($shift_id){
				return $query->row;
			} else {
				return $query->rows;
			}
		} else {
			return array();
		}
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}
}
?>