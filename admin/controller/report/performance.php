<?php
class ControllerReportPerformance extends Controller { 
	public function index() {  

		// if(isset($this->session->data['emp_code'])){
		// 	$emp_code = $this->session->data['emp_code'];
		// 	$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
		// 	if($is_set == '1'){
		// 		//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
		// 	} else {
		// 		$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
		// 	}
		// } elseif(isset($this->session->data['is_dept'])){
		// 	$emp_code = $this->session->data['d_emp_id'];
		// 	$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
		// 	if($is_set == '1'){
		// 		//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
		// 	} else {
		// 		$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
		// 	}
		// } elseif(isset($this->session->data['is_super'])){
		// 	$emp_code = $this->session->data['d_emp_id'];
		// 	$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
		// 	if($is_set == '1'){
		// 		//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
		// 	} else {
		// 		$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
		// 	}
		// }

		$this->language->load('report/performance');
		$this->load->model('report/common_report');
		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$year = date('Y');
			$month = date('n');
			$day = date('j');
			if($month == 1){
				if($day >= 26 && $day <= 31){
					$prev_month = $month;
					$prev_year = $year;
				} else {
					$prev_month = 12;
					$prev_year = $year - 1;
				}
			} else {
				if($day >= 26 && $day <= 31){
					$prev_month = $month;
					$prev_year = $year;
				} else {
					$prev_month = $month - 1;
					$prev_year = $year;
				}
			}
			$filter_date_start = sprintf("%04d-%02d-%02d", $prev_year, $prev_month, '26');
			//$filter_date_start = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			//$from = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
			// $filter_date_end1 = $this->model_report_common_report->getfilter_date_end($filter_date_start);
			// if(strtotime($filter_date_end) > strtotime($filter_date_end1) ){
			// 	$filter_date_end = $filter_date_end1;
			// }
		} else {
			$year = date('Y');
			$month = date('n');
			$day = date('j');
			if($month == 12){
				if($day >= 26 && $day <= 31){
					$next_month = 1;
					$next_year = $year + 1;
				} else {
					$next_month = 12;
					$next_year = $year;
				}
			} else {
				if($day >= 26 && $day <= 31){
					$next_month = $month + 1;
					$next_year = $year;
				} else {
					$next_month = $month;
					$next_year = $year;
				}
			}
			$filter_date_end = sprintf("%04d-%02d-%02d", $next_year, $next_month, '25');
			//echo $filter_date_end;exit;
			//$filter_date_end = date('Y-m-d');
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$filter_name_id = $this->session->data['emp_code'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = $this->request->get['filter_unit'];
		} else {
			$filter_unit = 0;
		}

		if (isset($this->request->get['filter_type'])) {
			$filter_type = $this->request->get['filter_type'];
		} else {
			$filter_type = 1;
		}

		if(isset($this->session->data['dept_names'])){
			$filter_departments = html_entity_decode($this->session->data['dept_names']);
			$filter_departments = "'" . str_replace(",", "','", html_entity_decode($filter_departments)) . "'";
		} else {
			$filter_departments = '';
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = html_entity_decode($this->request->get['filter_department']);
		} elseif(isset($this->session->data['dept_name']) && !isset($this->session->data['dept_names'])){
			$filter_department = $this->session->data['dept_name'];
		} else {
			$filter_department = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/performance', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/attendance');
		$this->load->model('transaction/transaction');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'filter_unit'			 => $filter_unit,
			'filter_department'		 => $filter_department,
			'filter_departments'	 => $filter_departments,
			'day_close'              => 1,
			'filter_dol'             => 1,
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		//$start_time = strtotime($filter_date_start);
		//$compare_time = strtotime(date('Y-m-d'));

		$day = array();
		$days = $this->GetDays($filter_date_start, $filter_date_end);
        foreach ($days as $dkey => $dvalue) {
        	$dates = explode('-', $dvalue);
        	$day[$dkey]['day'] = $dates[2];
        	$day[$dkey]['date'] = $dvalue;
        	$day[$dkey]['day_name'] = date('D', strtotime($dvalue));
        }
        $this->data['days'] = $day;

        //foreach ($day as $dkey => $dvalue) {
        	//$results = $this->model_report_common_report->getAttendance($data);
        //}
        // echo '<pre>';
        // print_r($data);
        // exit;
        $final_datas = array();
        if(isset($this->request->get['once']) && $this->request->get['once'] == 1){
	        $results1 = $this->model_report_common_report->getemployees($data);
	        $results2 = $this->model_report_common_report->getemployees_reporting($data);
	        $results = array_merge($results1, $results2);

	        foreach ($results as $rkey => $rvalue) {
				$performance_data = array();
				$transaction_datas = $this->model_report_common_report->gettransaction_data($rvalue['emp_code'], $data);
				
				$emp_names = $this->db->query("SELECT CONCAT_WS(' ',first_name,last_name) AS emp_name FROM `oc_employee` WHERE `emp_code` = '".$rvalue['emp_code']."' ");
				$emp_name = '';
				if($emp_names->num_rows > 0){
					$emp_name = $emp_names->row['emp_name'];
				}

				$final_datas[$rvalue['emp_code']]['basic_data']['name'] = $emp_name;
				$final_datas[$rvalue['emp_code']]['basic_data']['department'] = $rvalue['department'];
				$final_datas[$rvalue['emp_code']]['basic_data']['emp_code'] = $rvalue['emp_code'];
				$transaction_inn = '0';
				if($transaction_datas){
					foreach($transaction_datas as $tkey => $tvalue){
						if($tvalue['date'] <= date('Y-m-d')){
							$shift_inn = '';
							$leave_inn = '';
							$transaction_inn = '1';
							if($tvalue['firsthalf_status'] == '1'){
								$firsthalf_statuss = 'Pre';
							} elseif($tvalue['firsthalf_status'] == '0'){
								$firsthalf_statuss = 'Abs';
							} else {
								$firsthalf_statuss = $tvalue['firsthalf_status'];
							}
							if($tvalue['secondhalf_status'] == '1'){
								$secondhalf_statuss = 'Pre';
							} elseif($tvalue['secondhalf_status'] == '0'){
								$secondhalf_statuss = 'Abs';
							} else {
								$secondhalf_statuss = $tvalue['secondhalf_status'];
							}
							if($tvalue['leave_status'] == '0'){
								$shift_intime = $tvalue['shift_intime'];
								$shift_outtime = $tvalue['shift_outtime'];
								$shift_inn = '1';
							} else {
								$leave_data_sql = "SELECT `leave_type`, `type` FROM `oc_leave_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$tvalue['date']."' AND `p_status` = '1' AND `a_status` = '1' ";
								$leave_data_res = $this->db->query($leave_data_sql);
								if($leave_data_res->num_rows > 0){
									$leave_inn = '1';
									if($leave_data_res->row['type'] == ''){
										$shift_intime = $leave_data_res->row['leave_type'];
										$shift_outtime = $leave_data_res->row['leave_type'];
									} else {
										if($leave_data_res->row['type'] == 'F'){
											$shift_intime = $leave_data_res->row['leave_type'];
											$shift_outtime = $leave_data_res->row['leave_type'];
										} elseif($leave_data_res->row['type'] == '1'){
											$shift_intime = $leave_data_res->row['leave_type'];
											$shift_outtime = $secondhalf_statuss;
										} elseif($leave_data_res->row['type'] == '2'){
											$shift_intime = $firsthalf_statuss;
											$shift_outtime = $leave_data_res->row['leave_type'];
										}
									}
								} else {
									$shift_inn = '1';
									$shift_intime = $tvalue['shift_intime'];
									$shift_outtime = $tvalue['shift_outtime'];
								}
							}
							if($tvalue['act_intime'] != '00:00:00'){
								if($tvalue['late_time'] != '00:00:00'){
									$late_time = $tvalue['late_time'];
									$late_time_exp = explode(':', $late_time);
									$leave_data_sql = "SELECT `leave_type`, `type` FROM `oc_leave_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$tvalue['date']."' AND `p_status` = '1' AND `a_status` = '1' ";
									$leave_data_res = $this->db->query($leave_data_sql);
									//created the "$new_val" as $since_start->h was giving problem in reassigning.
									$new_val = $late_time_exp[0];
									$zero_stat = 0;
									if($leave_data_res->num_rows > 0){
										if($leave_data_res->row['type'] == '1'){
											if($new_val < 4){
												$zero_stat = 1;
											}
											$new_val = $new_val - 4;
										} elseif($leave_data_res->row['type'] == '2'){
											if($new_val < 4){
												$zero_stat = 1;
											}
											$new_val = $new_val - 4;
										}
									}
									if($zero_stat == 1){
										$late_time = '00:00:00';
									} else {
										$late_time = sprintf("%02d", $new_val).':'.$late_time_exp[1].':'.$late_time_exp[2];
									}
									$tvalue['late_time'] = $late_time;
								}
							}
							
							if($tvalue['after_shift'] == 1){
								$tvalue['act_outtime'] = $tvalue['shift_outtime_flexi'];
							}
							
							$working_time = '00:00:00';
							$early_time = '00:00:00';
							if($tvalue['act_outtime'] != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
								$working_time = $tvalue['working_time'];
								$early_time = $tvalue['early_time'];
								
								$shift_intime_minus_one = Date('H:i:s', strtotime($tvalue['shift_intime'] .' -0 minutes'));
								$start_date = new DateTime($tvalue['date'].' '.$shift_intime_minus_one);
								$since_start = $start_date->diff(new DateTime($tvalue['date'].' '.$tvalue['act_intime']));
								if($since_start->h > 12){
									$start_date = new DateTime($tvalue['date'].' '.$shift_intime_minus_one);
									$since_start = $start_date->diff(new DateTime($tvalue['date_out'].' '.$tvalue['act_intime']));
								}
								$shift_early = 0;
								if($since_start->invert == 1){
									$shift_early = 1;
								}
								if($shift_early == 1){
									$shift_intime_minus_one = Date('H:i:s', strtotime($tvalue['shift_intime'] .' -0 minutes'));
									if(strtotime($tvalue['act_outtime']) < strtotime($shift_intime_minus_one)){
										$start_date = new DateTime($tvalue['date'].' '.$tvalue['act_intime']);
									} else {
										$start_date = new DateTime($tvalue['date'].' '.$shift_intime_minus_one);
									}
								} else {
									$start_date = new DateTime($tvalue['date'].' '.$tvalue['act_intime']);
								}
								$since_start = $start_date->diff(new DateTime($tvalue['date'].' '.$tvalue['act_outtime']));
								$working_hour = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
								$tvalue['working_time'] = $working_hour;
							}

							$working_stat = 0;
							$late_time_stat = 0;
							if($tvalue['absent_status'] == 1 || $tvalue['weekly_off'] <> 0 || $tvalue['holiday_id'] <> 0 || $tvalue['leave_status'] <> 0 || $tvalue['tour_id'] <> 0){
								$working_stat = 1;
							}
							$loss_hours = 0;
							if($filter_type == 3){
								if($shift_inn == '1' && $transaction_inn == '1' && $tvalue['act_outtime'] != '00:00:00' && $tvalue['act_intime'] != '00:00:00'){
									$start_date = new DateTime($tvalue['date'].' '.$tvalue['shift_intime']);
									$since_start = $start_date->diff(new DateTime($tvalue['date'].' '.$tvalue['shift_outtime']));
									$expected_working_hour = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
									//$expected_working_hour = '08:30:00';
									$actual_working_hours = $tvalue['working_time'];
									//$actual_working_hours = '08:20:00';
									$first_date = new DateTime($tvalue['date'].' '.$expected_working_hour);
									$second_date = new DateTime($tvalue['date'].' '.$actual_working_hours);
									$difference = $first_date->diff($second_date);
									if($difference->invert == 0){
										$working_stat = 1;
									} else {
										$working_stat = 0;
										if($difference->h > 0){
											$format = '%1dh:%1dm';
											$loss_hours = $difference->h;
											$loss_minutes = $difference->i;
											$loss_hours = sprintf($format, $loss_hours, $loss_minutes);
										} elseif($difference->i > 0){
											$format = '%1dh:%1dm';
											$hours = floor($difference->i / 60);
										    $minutes = ($difference->i % 60);
										    $loss_hours = sprintf($format, $hours, $minutes);
										} elseif($difference->h == 0 && $difference->i == 0){
											$working_stat = 0;
										}
									}
								}
							}
							$excess_stat = 0;
							$excess_hours = 0;
							if($filter_type == 5){
								if($shift_inn == '1' && $transaction_inn == '1' && $tvalue['act_outtime'] != '00:00:00' && $tvalue['act_intime'] != '00:00:00'){
									$start_date = new DateTime($tvalue['date'].' '.$tvalue['shift_intime']);
									$since_start = $start_date->diff(new DateTime($tvalue['date'].' '.$tvalue['shift_outtime']));
									$expected_working_hour = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
									//$expected_working_hour = '08:30:00';
									$actual_working_hours = $tvalue['working_time'];
									//$actual_working_hours = '08:30:00';
									$first_date = new DateTime($tvalue['date'].' '.$expected_working_hour);
									$second_date = new DateTime($tvalue['date'].' '.$actual_working_hours);
									$difference = $first_date->diff($second_date);
									// echo '<pre>';
									// print_r($difference);
									// exit;
									if($difference->invert == 0){
										$excess_stat = 1;
										if($difference->h > 0){
											$format = '%01dh:%01dm';
											$excess_hours = $difference->h;
											$excess_minutes = $difference->i;
											$excess_hours = sprintf($format, $excess_hours, $excess_minutes);
										} elseif($difference->i > 0){
											$format = '%01dh:%01dm';
											$hours = floor($difference->i / 60);
										    $minutes = ($difference->i % 60);
										    $excess_hours = sprintf($format, $hours, $minutes);
										} elseif($difference->h == 0 && $difference->i == 0){
											$excess_stat = 0;
										}
									} else {
										$excess_stat = 0;
									}
								}
							}
							$absent_stat = '0';
							if($filter_type == 4){
								if( ($tvalue['absent_status'] == 1 || $tvalue['absent_status'] == 0.5) && $tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['tour_id'] == '0'){
									$absent_stat = '1';
								}
							}
							$manual_stat = '0';
							if($filter_type == 6){
								if($tvalue['manual_status'] == 1){
									$manual_stat = '1';
								}
							}
							if($tvalue['firsthalf_status'] == '1' || ( strtotime($tvalue['date']) == strtotime(date('Y-m-d')) && $tvalue['act_intime'] != '00:00:00')){
								$firsthalf_status = 'Pre';
							} elseif($tvalue['firsthalf_status'] == '0'){
								$firsthalf_status = 'Abs';
							} else {
								$firsthalf_status = $tvalue['firsthalf_status'];
							}
							if($tvalue['secondhalf_status'] == '1'){
								$secondhalf_status = 'Pre';
							} elseif($tvalue['secondhalf_status'] == '0'){
								$secondhalf_status = 'Abs';
							} else {
								$secondhalf_status = $tvalue['secondhalf_status'];
							}
							if(ctype_alpha($shift_intime) === false){
								$shift_intime = date('H:i', strtotime($shift_intime));
							}

							if(ctype_alpha($shift_outtime) === false){
								$shift_outtime = date('H:i', strtotime($shift_outtime));
							}

							if(ctype_alpha($tvalue['act_intime']) === false){
								$tvalue['act_intime'] = date('H:i', strtotime($tvalue['act_intime']));
							}

							if(ctype_alpha($tvalue['act_outtime']) === false){
								$tvalue['act_outtime'] = date('H:i', strtotime($tvalue['act_outtime']));
							}

							if(ctype_alpha($tvalue['late_time']) === false){
								if($tvalue['late_time'] != '00:00:00' && $tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['tour_id'] == '0' && $tvalue['leave_status'] == '0'){
									$tvalue['late_time'] = date('H:i', strtotime($tvalue['late_time']));
								} else {
									$tvalue['late_time'] = '00:00';
								}
							}

							if(ctype_alpha($tvalue['early_time']) === false){
								if($tvalue['early_time'] != '00:00:00' && $tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['tour_id'] == '0' && $tvalue['leave_status'] == '0'){
									$tvalue['early_time'] = date('H:i', strtotime($tvalue['early_time']));
								} else {
									$tvalue['early_time'] = '00:00';
								}
							}

							if(ctype_alpha($tvalue['working_time']) === false){
								$tvalue['working_time'] = date('H:i', strtotime($tvalue['working_time']));
							}

							if( ( ( $tvalue['working_time'] == '00:00' && strtotime($tvalue['date']) != strtotime(date('Y-m-d')) ) OR ( strtotime($tvalue['date']) == strtotime(date('Y-m-d')) && $tvalue['act_intime'] == '00:00' ) ) && $tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['tour_id'] == '0' && $tvalue['leave_status'] == '0'){
								if($tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0'){
									$leave_href = $this->url->link('transaction/leave_ess/insert', 'token=' . $this->session->data['token'].'&filter_name_id='.$tvalue['emp_id'].'&from='.$tvalue['date'], 'SSL');
								}
							} else {
								$leave_href = '';
								$tvalue['absent_status'] = 0;
								if($tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0'){
									$firsthalf_status = 'Pre';
								}
							}

							if($tvalue['manual_status'] == '1'){
								$firsthalf_status = $firsthalf_status.' (m)';
							}

							if(strtotime($tvalue['date']) < strtotime($rvalue['doj']) || ($rvalue['dol'] != '0000-00-00' && strtotime($tvalue['date']) > strtotime($rvalue['dol']))){
								$shift_intime = '';
								$shift_outtime = '';
								$tvalue['act_intime'] = '';
								$tvalue['act_outtime'] = '';
								$tvalue['late_time'] = '';
								$tvalue['early_time'] = '';
								$tvalue['working_time'] = '';
								$firsthalf_status = '';
								$secondhalf_status = '';
								$tvalue['absent_status'] = 0;
								$tvalue['present_status'] = 0;
								$leave_href = 0;
							}

							$performance_data[$rvalue['emp_code']]['action'][$tvalue['date']] = array(
								'name' => $tvalue['emp_name'],
								'emp_code' => $tvalue['emp_id'],
								'shift_intime' => $shift_intime,
								'shift_outtime' => $shift_outtime,
								'act_intime' => $tvalue['act_intime'],
								'act_outtime' => $tvalue['act_outtime'],
								'late_time' => $tvalue['late_time'],
								'early_time' => $tvalue['early_time'],
								'working_time' => $tvalue['working_time'],
								'date' => $tvalue['date'],
								'weekly_off' => $tvalue['weekly_off'],
								'holiday_id' => $tvalue['holiday_id'],
								'firsthalf_status' => $firsthalf_status,
								'secondhalf_status' => $secondhalf_status,
								'absent' => $tvalue['absent_status'],
								'present' => $tvalue['present_status'],
								'late_time_stat' => $late_time_stat,
								'working_stat' => $working_stat,
								'loss_hours' => $loss_hours,
								'excess_hours' => $excess_hours,
								'absent_stat' => $absent_stat,
								'excess_stat' => $excess_stat,
								'leave_href' => $leave_href,
								'manual_stat' => $manual_stat
							);
						}
					}
				} else {
					unset($final_datas[$rvalue['emp_code']]);
				}
				if($performance_data){
					foreach ($day as $dkey => $dvalue) {
						foreach ($performance_data as $pkey => $pvalue) {
							if(isset($pvalue['action'][$dvalue['date']]['date'])){
							} else {
								$leave_href = '';//$this->url->link('transaction/leave_ess/insert', 'token=' . $this->session->data['token'].'&filter_name_id='.$rvalue['emp_code'].'&from='.$dvalue['date'], 'SSL');
								$performance_data[$rvalue['emp_code']]['action'][$dvalue['date']] = array(
									'name' => $rvalue['name'],
									'emp_code' => $rvalue['emp_code'],
									'shift_intime' => '',
									'shift_outtime' => '',
									'act_intime' => '',
									'act_outtime' => '',
									'late_time' => '',
									'early_time' => '',
									'working_time' => '',
									'weekly_off' => 0,
									'holiday_id' => 0,
									'absent' => 0,
									'date' => $dvalue['date'],
									'late_time_stat' => 0,
									'loss_hours' => 0,
									'excess_hours' => 0,
									'firsthalf_status' => '',
									'secondhalf_status' => '',
									'working_stat' => 0,
									'absent_stat' => 0,
									'leave_href' => $leave_href,
									'manual_stat' => 0,
									'excess_stat' => 0,
								);			
							}
						}
					}
				}
				foreach ($performance_data as $pkey => $pvalue) {
					$per_sort_data = $this->array_sort($pvalue['action'], 'date', SORT_ASC);
					$late_inn = 0;
					$working_inn = 0;
					$abs_in = 0;
					$manual_in = 0;
					foreach($per_sort_data as $akey => $avalue){
						if($filter_type == 2 && $avalue['late_time_stat'] == 1){
							//$late_inn ++;
						} elseif($filter_type == 2 && $avalue['late_time_stat'] == 0) {
							$per_sort_data[$akey]['shift_intime'] = '';
							$per_sort_data[$akey]['shift_outtime'] = '';
							$per_sort_data[$akey]['act_intime'] = '';
							$per_sort_data[$akey]['act_outtime'] = '';
							$per_sort_data[$akey]['late_time'] = '';
							$per_sort_data[$akey]['early_time'] = '';
							$per_sort_data[$akey]['working_time'] = '';
							$per_sort_data[$akey]['firsthalf_status'] = '';
							$per_sort_data[$akey]['secondhalf_status'] = '';
							$late_inn ++;
							//$late_inn = 0;
							//unset($per_sort_data['action'][$akey]);
						}
						if($filter_type == 3 && $avalue['working_stat'] == 0){
							$working_inn ++;
						} elseif($filter_type == 3 && $avalue['working_stat'] == 1) {
							$per_sort_data[$akey]['shift_intime'] = '';
							$per_sort_data[$akey]['shift_outtime'] = '';
							$per_sort_data[$akey]['act_intime'] = '';
							$per_sort_data[$akey]['act_outtime'] = '';
							$per_sort_data[$akey]['late_time'] = '';
							$per_sort_data[$akey]['early_time'] = '';
							$per_sort_data[$akey]['working_time'] = '';
							$per_sort_data[$akey]['firsthalf_status'] = '';
							$per_sort_data[$akey]['secondhalf_status'] = '';
							$per_sort_data[$akey]['loss_hours'] = '';
							//$working_inn = 0;
							//unset($per_sort_data['action'][$akey]);
						}
						if($filter_type == 4 && $avalue['absent_stat'] == 0) {
							$abs_in ++;
							$per_sort_data[$akey]['shift_intime'] = '';
							$per_sort_data[$akey]['shift_outtime'] = '';
							$per_sort_data[$akey]['act_intime'] = '';
							$per_sort_data[$akey]['act_outtime'] = '';
							$per_sort_data[$akey]['late_time'] = '';
							$per_sort_data[$akey]['early_time'] = '';
							$per_sort_data[$akey]['working_time'] = '';
							$per_sort_data[$akey]['firsthalf_status'] = '';
							$per_sort_data[$akey]['secondhalf_status'] = '';
							//$late_inn = 0;
							//unset($per_sort_data[$akey]);
						}
						if($filter_type == 6 && $avalue['manual_stat'] == 0) {
							$manual_in ++;
							$per_sort_data[$akey]['shift_intime'] = '';
							$per_sort_data[$akey]['shift_outtime'] = '';
							$per_sort_data[$akey]['act_intime'] = '';
							$per_sort_data[$akey]['act_outtime'] = '';
							$per_sort_data[$akey]['late_time'] = '';
							$per_sort_data[$akey]['early_time'] = '';
							$per_sort_data[$akey]['working_time'] = '';
							$per_sort_data[$akey]['firsthalf_status'] = '';
							$per_sort_data[$akey]['secondhalf_status'] = '';
							//$late_inn = 0;
							//unset($per_sort_data[$akey]);
						}
						if($filter_type == 5 && $avalue['excess_stat'] == 0) {
							$per_sort_data[$akey]['shift_intime'] = '';
							$per_sort_data[$akey]['shift_outtime'] = '';
							$per_sort_data[$akey]['act_intime'] = '';
							$per_sort_data[$akey]['act_outtime'] = '';
							$per_sort_data[$akey]['late_time'] = '';
							$per_sort_data[$akey]['early_time'] = '';
							$per_sort_data[$akey]['working_time'] = '';
							$per_sort_data[$akey]['firsthalf_status'] = '';
							$per_sort_data[$akey]['secondhalf_status'] = '';
							$per_sort_data[$akey]['excess_hours'] = '';
							//$late_inn = 0;
							//unset($per_sort_data['action'][$akey]);
						}	
					}
					if($filter_type == 2){
						if($late_inn == count($per_sort_data)){
							unset($final_datas[$rvalue['emp_code']]);	
						} else {
							$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
						}
						//$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
					//} elseif($working_inn > 3 && $filter_type == 3){
					} elseif($filter_type == 3){
						$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
					} elseif($filter_type == 1 || $filter_type == 5) {
						$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
					} elseif($filter_type == 4) {
						if($abs_in == count($per_sort_data)){
							unset($final_datas[$rvalue['emp_code']]);	
						} else {
							$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
						}
					} elseif($filter_type == 6) {
						if($manual_in == count($per_sort_data)){
							unset($final_datas[$rvalue['emp_code']]);	
						} else {
							$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
						}
					} else {
						unset($final_datas[$rvalue['emp_code']]);
					}
				}
			}
		}

		$this->data['final_datas'] = $final_datas;
		// echo '<pre>';
  		// print_r($this->data['final_datas']);
  		// exit;

		//$results = $this->model_report_common_report->getAttendance($data);
		
		$type_data = array(
			'1' => 'Default',
			//'2' => 'Late',
			'3' => 'Loss',
			'4' => 'Absent',
			'5' => 'Excess',
			'6' => 'Manual'
		);

		$this->data['type_data'] = $type_data;

		$locations = $this->db->query("SELECT * FROM `oc_unit` ")->rows;
		$unit_data['0'] = 'All'; 
		foreach($locations as $lkey => $lvalue){
			$unit_data[$lvalue['unit_id']] = $lvalue['unit'];
		}
		$this->data['unit_data'] = $unit_data;
		
		$department_datas = $this->db->query("SELECT * FROM `oc_department` ")->rows;
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['d_name'];
		}
		$this->data['department_data'] = $department_data;
		
		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}

		if(isset($this->session->data['is_dept']) && $this->session->data['is_dept'] == '1'){
			$user_dept = 1;
			$is_dept = 1;
		} elseif(isset($this->session->data['is_user'])){
			$user_dept = 1;
			$is_dept = 0;
		} else {
			$user_dept = 0;
			$is_dept = 0;
		}
		$this->data['user_dept'] = $user_dept;
		$this->data['is_dept'] = $is_dept;

		$this->data['reporting_head'] = 0;
		if(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$is_reporting_datas = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE `reporting_to` = '".$this->session->data['emp_code']."' ");
			if($is_reporting_datas->num_rows > 0){
				$this->data['reporting_head'] = 1;
			} else {
				$this->data['reporting_head'] = 0;
			}
		}
		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_type'] = $filter_type;
		$this->data['filter_unit'] = $filter_unit;
		$this->data['filter_department'] = $filter_department;

		$this->template = 'report/performance.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	function array_sort($array, $on, $order=SORT_ASC){

	    $new_array = array();
	    $sortable_array = array();

	    if (count($array) > 0) {
	        foreach ($array as $k => $v) {
	            if (is_array($v)) {
	                foreach ($v as $k2 => $v2) {
	                    if ($k2 == $on) {
	                        $sortable_array[$k] = $v2;
	                    }
	                }
	            } else {
	                $sortable_array[$k] = $v;
	            }
	        }

	        switch ($order) {
	            case SORT_ASC:
	                asort($sortable_array);
	                break;
	            case SORT_DESC:
	                arsort($sortable_array);
	                break;
	        }

	        foreach ($sortable_array as $k => $v) {
	            $new_array[$k] = $array[$k];
	        }
	    }

	    return $new_array;
	}

	public function export(){
		$this->language->load('report/performance');
		$this->load->model('report/common_report');
		$this->load->model('transaction/transaction');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$year = date('Y');
			$month = date('n');
			$day = date('j');
			if($month == 1){
				if($day >= 26 && $day <= 31){
					$prev_month = $month;
					$prev_year = $year;
				} else {
					$prev_month = 12;
					$prev_year = $year - 1;
				}
			} else {
				if($day >= 26 && $day <= 31){
					$prev_month = $month;
					$prev_year = $year;
				} else {
					$prev_month = $month - 1;
					$prev_year = $year;
				}
			}
			$filter_date_start = sprintf("%04d-%02d-%02d", $prev_year, $prev_month, '26');
			//$filter_date_start = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			//$from = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
			// $filter_date_end1 = $this->model_report_common_report->getfilter_date_end($filter_date_start);
			// if(strtotime($filter_date_end) > strtotime($filter_date_end1) ){
			// 	$filter_date_end = $filter_date_end1;
			// }
		} else {
			$year = date('Y');
			$month = date('n');
			$day = date('j');
			if($month == 12){
				if($day >= 26 && $day <= 31){
					$next_month = 1;
					$next_year = $year + 1;
				} else {
					$next_month = 12;
					$next_year = $year;
				}
			} else {
				if($day >= 26 && $day <= 31){
					$next_month = $month + 1;
					$next_year = $year;
				} else {
					$next_month = $month;
					$next_year = $year;
				}
			}
			$filter_date_end = sprintf("%04d-%02d-%02d", $next_year, $next_month, '25');
			//echo $filter_date_end;exit;
			//$filter_date_end = date('Y-m-d');
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_type'])) {
			$filter_type = $this->request->get['filter_type'];
		} else {
			$filter_type = 1;
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = $this->request->get['filter_unit'];
		} else {
			$filter_unit = 0;
		}

		if(isset($this->session->data['dept_names'])){
			$filter_departments = html_entity_decode($this->session->data['dept_names']);
			$filter_departments = "'" . str_replace(",", "','", html_entity_decode($filter_departments)) . "'";
		} else {
			$filter_departments = '';
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = html_entity_decode($this->request->get['filter_department']);
		} elseif(isset($this->session->data['dept_name']) && !isset($this->session->data['dept_names'])){
			$filter_department = $this->session->data['dept_name'];
		} else {
			$filter_department = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'filter_unit'			 => $filter_unit,
			'filter_department'		 => $filter_department,
			'filter_departments'     => $filter_departments,
			'filter_dol'             => 1,
		);

		//$start_time = strtotime($filter_date_start);
		//$compare_time = strtotime(date('Y-m-d'));

		$day = array();
        $days = $this->GetDays($filter_date_start, $filter_date_end);
        foreach ($days as $dkey => $dvalue) {
        	$dates = explode('-', $dvalue);
        	$day[$dkey]['day'] = $dates[2];
        	$day[$dkey]['date'] = $dvalue;
        	$day[$dkey]['day_name'] = date('D', strtotime($dvalue));
        }
        $this->data['days'] = $day;

        //foreach ($day as $dkey => $dvalue) {
        	//$results = $this->model_report_common_report->getAttendance($data);
        //}
        $final_datas = array();
		$results1 = $this->model_report_common_report->getemployees($data);
        $results2 = $this->model_report_common_report->getemployees_reporting($data);
        $results = array_merge($results1, $results2);

        foreach ($results as $rkey => $rvalue) {
			$performance_data = array();
			$transaction_datas = $this->model_report_common_report->gettransaction_data($rvalue['emp_code'], $data);
			$emp_names = $this->db->query("SELECT CONCAT_WS(' ',first_name,last_name) AS emp_name FROM `oc_employee` WHERE `emp_code` = '".$rvalue['emp_code']."' ");
			$emp_name = '';
			if($emp_names->num_rows > 0){
				$emp_name = $emp_names->row['emp_name'];
			}

			$final_datas[$rvalue['emp_code']]['basic_data']['name'] = $emp_name;
			$final_datas[$rvalue['emp_code']]['basic_data']['department'] = $rvalue['department'];
			$final_datas[$rvalue['emp_code']]['basic_data']['emp_code'] = $rvalue['emp_code'];
			$transaction_inn = '0';
			if($transaction_datas){
				foreach($transaction_datas as $tkey => $tvalue){
					if($tvalue['date'] <= date('Y-m-d')){
						$shift_inn = '';
						$leave_inn = '';
						$transaction_inn = '1';
						if($tvalue['firsthalf_status'] == '1'){
							$firsthalf_statuss = 'Pre';
						} elseif($tvalue['firsthalf_status'] == '0'){
							$firsthalf_statuss = 'Abs';
						} else {
							$firsthalf_statuss = $tvalue['firsthalf_status'];
						}
						if($tvalue['secondhalf_status'] == '1'){
							$secondhalf_statuss = 'Pre';
						} elseif($tvalue['secondhalf_status'] == '0'){
							$secondhalf_statuss = 'Abs';
						} else {
							$secondhalf_statuss = $tvalue['secondhalf_status'];
						}
						if($tvalue['leave_status'] == '0'){
							$shift_intime = $tvalue['shift_intime'];
							$shift_outtime = $tvalue['shift_outtime'];
							$shift_inn = '1';
						} else {
							$leave_data_sql = "SELECT `leave_type`, `type` FROM `oc_leave_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$tvalue['date']."' AND `p_status` = '1' AND `a_status` = '1' ";
							$leave_data_res = $this->db->query($leave_data_sql);
							if($leave_data_res->num_rows > 0){
								$leave_inn = '1';
								if($leave_data_res->row['type'] == ''){
									$shift_intime = $leave_data_res->row['leave_type'];
									$shift_outtime = $leave_data_res->row['leave_type'];
								} else {
									if($leave_data_res->row['type'] == 'F'){
										$shift_intime = $leave_data_res->row['leave_type'];
										$shift_outtime = $leave_data_res->row['leave_type'];
									} elseif($leave_data_res->row['type'] == '1'){
										$shift_intime = $leave_data_res->row['leave_type'];
										$shift_outtime = $secondhalf_statuss;
									} elseif($leave_data_res->row['type'] == '2'){
										$shift_intime = $firsthalf_statuss;
										$shift_outtime = $leave_data_res->row['leave_type'];
									}
								}
							} else {
								$shift_inn = '1';
								$shift_intime = $tvalue['shift_intime'];
								$shift_outtime = $tvalue['shift_outtime'];
							}
						}
						if($tvalue['act_intime'] != '00:00:00'){
							if($tvalue['late_time'] != '00:00:00'){
								$late_time = $tvalue['late_time'];
								$late_time_exp = explode(':', $late_time);
								$leave_data_sql = "SELECT `leave_type`, `type` FROM `oc_leave_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$tvalue['date']."' AND `p_status` = '1' AND `a_status` = '1' ";
								$leave_data_res = $this->db->query($leave_data_sql);
								//created the "$new_val" as $since_start->h was giving problem in reassigning.
								$new_val = $late_time_exp[0];
								$zero_stat = 0;
								if($leave_data_res->num_rows > 0){
									if($leave_data_res->row['type'] == '1'){
										if($new_val < 4){
											$zero_stat = 1;
										}
										$new_val = $new_val - 4;
									} elseif($leave_data_res->row['type'] == '2'){
										if($new_val < 4){
											$zero_stat = 1;
										}
										$new_val = $new_val - 4;
									}
								}
								if($zero_stat == 1){
									$late_time = '00:00:00';
								} else {
									$late_time = sprintf("%02d", $new_val).':'.$late_time_exp[1].':'.$late_time_exp[2];
								}
								$tvalue['late_time'] = $late_time;
							}
						}
						if($tvalue['after_shift'] == 1){
							$tvalue['act_outtime'] = $tvalue['shift_outtime_flexi'];
						}
						
						$working_time = '00:00:00';
						$early_time = '00:00:00';
						if($tvalue['act_outtime'] != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
							$working_time = $tvalue['working_time'];
							$early_time = $tvalue['early_time'];
							
							$shift_intime_minus_one = Date('H:i:s', strtotime($tvalue['shift_intime'] .' -0 minutes'));
							$start_date = new DateTime($tvalue['date'].' '.$shift_intime_minus_one);
							$since_start = $start_date->diff(new DateTime($tvalue['date'].' '.$tvalue['act_intime']));
							if($since_start->h > 12){
								$start_date = new DateTime($tvalue['date'].' '.$shift_intime_minus_one);
								$since_start = $start_date->diff(new DateTime($tvalue['date_out'].' '.$tvalue['act_intime']));
							}
							$shift_early = 0;
							if($since_start->invert == 1){
								$shift_early = 1;
							}
							if($shift_early == 1){
								$shift_intime_minus_one = Date('H:i:s', strtotime($tvalue['shift_intime'] .' -0 minutes'));
								if(strtotime($tvalue['act_outtime']) < strtotime($shift_intime_minus_one)){
									$start_date = new DateTime($tvalue['date'].' '.$tvalue['act_intime']);
								} else {
									$start_date = new DateTime($tvalue['date'].' '.$shift_intime_minus_one);
								}
							} else {
								$start_date = new DateTime($tvalue['date'].' '.$tvalue['act_intime']);
							}
							$since_start = $start_date->diff(new DateTime($tvalue['date'].' '.$tvalue['act_outtime']));
							$working_hour = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
							$tvalue['working_time'] = $working_hour;
						}
											
						$working_stat = 0;
						$late_time_stat = 0;
						if($tvalue['absent_status'] == 1 || $tvalue['weekly_off'] <> 0 || $tvalue['holiday_id'] <> 0 || $tvalue['leave_status'] <> 0 || $tvalue['tour_id'] <> 0){
							$working_stat = 1;
						}
						$loss_hours = 0;
						if($filter_type == 3){
							if($shift_inn == '1' && $transaction_inn == '1' && $tvalue['act_outtime'] != '00:00:00' && $tvalue['act_intime'] != '00:00:00'){
								$start_date = new DateTime($tvalue['date'].' '.$tvalue['shift_intime']);
								$since_start = $start_date->diff(new DateTime($tvalue['date'].' '.$tvalue['shift_outtime']));
								$expected_working_hour = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
								//$expected_working_hour = '08:30:00';
								$actual_working_hours = $tvalue['working_time'];
								//$actual_working_hours = '08:20:00';
								$first_date = new DateTime($tvalue['date'].' '.$expected_working_hour);
								$second_date = new DateTime($tvalue['date'].' '.$actual_working_hours);
								$difference = $first_date->diff($second_date);
								if($difference->invert == 0){
									$working_stat = 1;
								} else {
									$working_stat = 0;
									if($difference->h > 0){
										$format = '%1dh:%1dm';
										$loss_hours = $difference->h;
										$loss_minutes = $difference->i;
										$loss_hours = sprintf($format, $loss_hours, $loss_minutes);
									} elseif($difference->i > 0){
										$format = '%1dh:%1dm';
										$hours = floor($difference->i / 60);
									    $minutes = ($difference->i % 60);
									    $loss_hours = sprintf($format, $hours, $minutes);
									} elseif($difference->h == 0 && $difference->i == 0){
										$working_stat = 0;
									}
								}
							}
						}
						$excess_stat = 0;
						$excess_hours = 0;
						if($filter_type == 5){
							if($shift_inn == '1' && $transaction_inn == '1' && $tvalue['act_outtime'] != '00:00:00' && $tvalue['act_intime'] != '00:00:00'){
								$start_date = new DateTime($tvalue['date'].' '.$tvalue['shift_intime']);
								$since_start = $start_date->diff(new DateTime($tvalue['date'].' '.$tvalue['shift_outtime']));
								$expected_working_hour = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
								//$expected_working_hour = '08:30:00';
								$actual_working_hours = $tvalue['working_time'];
								//$actual_working_hours = '08:30:00';
								$first_date = new DateTime($tvalue['date'].' '.$expected_working_hour);
								$second_date = new DateTime($tvalue['date'].' '.$actual_working_hours);
								$difference = $first_date->diff($second_date);
								// echo '<pre>';
								// print_r($difference);
								// exit;
								if($difference->invert == 0){
									$excess_stat = 1;
									if($difference->h > 0){
										$format = '%01dh:%01dm';
										$excess_hours = $difference->h;
										$excess_minutes = $difference->i;
										$excess_hours = sprintf($format, $excess_hours, $excess_minutes);
									} elseif($difference->i > 0){
										$format = '%01dh:%01dm';
										$hours = floor($difference->i / 60);
									    $minutes = ($difference->i % 60);
									    $excess_hours = sprintf($format, $hours, $minutes);
									} elseif($difference->h == 0 && $difference->i == 0){
										$excess_stat = 0;
									}
								} else {
									$excess_stat = 0;
								}
							}
						}
						$absent_stat = '0';
						if($filter_type == 4){
							if( ($tvalue['absent_status'] == 1 || $tvalue['absent_status'] == 0.5) && $tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['tour_id'] == '0'){
								$absent_stat = '1';
							}
						}
						$manual_stat = '0';
						if($filter_type == 6){
							if($tvalue['manual_status'] == 1){
								$manual_stat = '1';
							}
						}

						if($tvalue['firsthalf_status'] == '1' || ( strtotime($tvalue['date']) == strtotime(date('Y-m-d')) && $tvalue['act_intime'] != '00:00:00')){
							$firsthalf_status = 'Pre';
						} elseif($tvalue['firsthalf_status'] == '0'){
							$firsthalf_status = 'Abs';
						} else {
							$firsthalf_status = $tvalue['firsthalf_status'];
						}
						if($tvalue['secondhalf_status'] == '1'){
							$secondhalf_status = 'Pre';
						} elseif($tvalue['secondhalf_status'] == '0'){
							$secondhalf_status = 'Abs';
						} else {
							$secondhalf_status = $tvalue['secondhalf_status'];
						}
						if(ctype_alpha($shift_intime) === false){
							$shift_intime = date('H:i', strtotime($shift_intime));
						}

						if(ctype_alpha($shift_outtime) === false){
							$shift_outtime = date('H:i', strtotime($shift_outtime));
						}

						if(ctype_alpha($tvalue['act_intime']) === false){
							$tvalue['act_intime'] = date('H:i', strtotime($tvalue['act_intime']));
						}

						if(ctype_alpha($tvalue['act_outtime']) === false){
							$tvalue['act_outtime'] = date('H:i', strtotime($tvalue['act_outtime']));
						}

						if(ctype_alpha($tvalue['late_time']) === false){
							if($tvalue['late_time'] != '00:00:00' && $tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['tour_id'] == '0' && $tvalue['leave_status'] == '0'){
								$tvalue['late_time'] = date('H:i', strtotime($tvalue['late_time']));
							} else {
								$tvalue['late_time'] = '00:00';
							}
						}

						if(ctype_alpha($tvalue['early_time']) === false){
							if($tvalue['early_time'] != '00:00:00' && $tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['tour_id'] == '0' && $tvalue['leave_status'] == '0'){
								$tvalue['early_time'] = date('H:i', strtotime($tvalue['early_time']));
							} else {
								$tvalue['early_time'] = '00:00';
							}
						}

						if(ctype_alpha($tvalue['working_time']) === false){
							$tvalue['working_time'] = date('H:i', strtotime($tvalue['working_time']));
						}

						if( ( ( $tvalue['working_time'] == '00:00' && strtotime($tvalue['date']) != strtotime(date('Y-m-d')) ) OR ( strtotime($tvalue['date']) == strtotime(date('Y-m-d')) && $tvalue['act_intime'] == '00:00' ) ) && $tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['tour_id'] == '0' && $tvalue['leave_status'] == '0'){
							if($tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0'){
								$leave_href = $this->url->link('transaction/leave_ess/insert', 'token=' . $this->session->data['token'].'&filter_name_id='.$tvalue['emp_id'].'&from='.$tvalue['date'], 'SSL');
							}
						} else {
							$leave_href = '';
							$tvalue['absent_status'] = 0;
							if($tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0'){
								$firsthalf_status = 'Pre';
							}
						}

						if($tvalue['manual_status'] == '1'){
							$firsthalf_status = $firsthalf_status.' (m)';
						}

						if(strtotime($tvalue['date']) < strtotime($rvalue['doj']) || ($rvalue['dol'] != '0000-00-00' && strtotime($tvalue['date']) > strtotime($rvalue['dol']))){
							$shift_intime = '';
							$shift_outtime = '';
							$tvalue['act_intime'] = '';
							$tvalue['act_outtime'] = '';
							$tvalue['late_time'] = '';
							$tvalue['early_time'] = '';
							$tvalue['working_time'] = '';
							$firsthalf_status = '';
							$secondhalf_status = '';
							$tvalue['absent_status'] = 0;
							$tvalue['present_status'] = 0;
							$leave_href = 0;
						}

						$performance_data[$rvalue['emp_code']]['action'][$tvalue['date']] = array(
							'name' => $tvalue['emp_name'],
							'emp_code' => $tvalue['emp_id'],
							'shift_intime' => $shift_intime,
							'shift_outtime' => $shift_outtime,
							'act_intime' => $tvalue['act_intime'],
							'act_outtime' => $tvalue['act_outtime'],
							'late_time' => $tvalue['late_time'],
							'early_time' => $tvalue['early_time'],
							'working_time' => $tvalue['working_time'],
							'date' => $tvalue['date'],
							'weekly_off' => $tvalue['weekly_off'],
							'holiday_id' => $tvalue['holiday_id'],
							'firsthalf_status' => $firsthalf_status,
							'secondhalf_status' => $secondhalf_status,
							'absent' => $tvalue['absent_status'],
							'present' => $tvalue['present_status'],
							'late_time_stat' => $late_time_stat,
							'working_stat' => $working_stat,
							'loss_hours' => $loss_hours,
							'excess_hours' => $excess_hours,
							'absent_stat' => $absent_stat,
							'excess_stat' => $excess_stat,
							'leave_href' => $leave_href,
							'manual_stat' => $manual_stat
						);
					}
				}
			} else {
				unset($final_datas[$rvalue['emp_code']]);
			}
			if($performance_data){
				foreach ($day as $dkey => $dvalue) {
					foreach ($performance_data as $pkey => $pvalue) {
						if(isset($pvalue['action'][$dvalue['date']]['date'])){
						} else {
							$leave_href = '';//$this->url->link('transaction/leave/insert', 'token=' . $this->session->data['token'].'&filter_name_id='.$rvalue['emp_code'].'&from='.$dvalue['date'], 'SSL');
							$performance_data[$rvalue['emp_code']]['action'][$dvalue['date']] = array(
								'name' => $rvalue['name'],
								'emp_code' => $rvalue['emp_code'],
								'shift_intime' => '',
								'shift_outtime' => '',
								'act_intime' => '',
								'act_outtime' => '',
								'late_time' => '',
								'early_time' => '',
								'working_time' => '',
								'weekly_off' => 0,
								'holiday_id' => 0,
								'absent' => 0,
								'date' => $dvalue['date'],
								'late_time_stat' => 0,
								'loss_hours' => 0,
								'excess_hours' => 0,
								'firsthalf_status' => '',
								'secondhalf_status' => '',
								'working_stat' => 0,
								'absent_stat' => 0,
								'leave_href' => $leave_href,
								'manual_stat' => 0,
								'excess_stat' => 0,
							);			
						}
					}
				}
			}
			foreach ($performance_data as $pkey => $pvalue) {
				$per_sort_data = $this->array_sort($pvalue['action'], 'date', SORT_ASC);
				$late_inn = 0;
				$working_inn = 0;
				$abs_in = 0;
				$manual_in = 0;
				foreach($per_sort_data as $akey => $avalue){
					if($filter_type == 2 && $avalue['late_time_stat'] == 1){
						//$late_inn ++;
					} elseif($filter_type == 2 && $avalue['late_time_stat'] == 0) {
						$per_sort_data[$akey]['shift_intime'] = '';
						$per_sort_data[$akey]['shift_outtime'] = '';
						$per_sort_data[$akey]['act_intime'] = '';
						$per_sort_data[$akey]['act_outtime'] = '';
						$per_sort_data[$akey]['late_time'] = '';
						$per_sort_data[$akey]['early_time'] = '';
						$per_sort_data[$akey]['working_time'] = '';
						$per_sort_data[$akey]['firsthalf_status'] = '';
						$per_sort_data[$akey]['secondhalf_status'] = '';
						$late_inn ++;
						//$late_inn = 0;
						//unset($per_sort_data['action'][$akey]);
					}
					if($filter_type == 3 && $avalue['working_stat'] == 0){
						$working_inn ++;
					} elseif($filter_type == 3 && $avalue['working_stat'] == 1) {
						$per_sort_data[$akey]['shift_intime'] = '';
						$per_sort_data[$akey]['shift_outtime'] = '';
						$per_sort_data[$akey]['act_intime'] = '';
						$per_sort_data[$akey]['act_outtime'] = '';
						$per_sort_data[$akey]['late_time'] = '';
						$per_sort_data[$akey]['early_time'] = '';
						$per_sort_data[$akey]['working_time'] = '';
						$per_sort_data[$akey]['firsthalf_status'] = '';
						$per_sort_data[$akey]['secondhalf_status'] = '';
						$per_sort_data[$akey]['loss_hours'] = '';
						//$working_inn = 0;
						//unset($per_sort_data['action'][$akey]);
					}
					if($filter_type == 4 && $avalue['absent_stat'] == 0) {
						$abs_in ++;
						$per_sort_data[$akey]['shift_intime'] = '';
						$per_sort_data[$akey]['shift_outtime'] = '';
						$per_sort_data[$akey]['act_intime'] = '';
						$per_sort_data[$akey]['act_outtime'] = '';
						$per_sort_data[$akey]['late_time'] = '';
						$per_sort_data[$akey]['early_time'] = '';
						$per_sort_data[$akey]['working_time'] = '';
						$per_sort_data[$akey]['firsthalf_status'] = '';
						$per_sort_data[$akey]['secondhalf_status'] = '';
						//$late_inn = 0;
						//unset($per_sort_data[$akey]);
					}
					if($filter_type == 6 && $avalue['manual_stat'] == 0) {
						$manual_in ++;
						$per_sort_data[$akey]['shift_intime'] = '';
						$per_sort_data[$akey]['shift_outtime'] = '';
						$per_sort_data[$akey]['act_intime'] = '';
						$per_sort_data[$akey]['act_outtime'] = '';
						$per_sort_data[$akey]['late_time'] = '';
						$per_sort_data[$akey]['early_time'] = '';
						$per_sort_data[$akey]['working_time'] = '';
						$per_sort_data[$akey]['firsthalf_status'] = '';
						$per_sort_data[$akey]['secondhalf_status'] = '';
						//$late_inn = 0;
						//unset($per_sort_data[$akey]);
					}
					if($filter_type == 5 && $avalue['excess_stat'] == 0) {
						$per_sort_data[$akey]['shift_intime'] = '';
						$per_sort_data[$akey]['shift_outtime'] = '';
						$per_sort_data[$akey]['act_intime'] = '';
						$per_sort_data[$akey]['act_outtime'] = '';
						$per_sort_data[$akey]['late_time'] = '';
						$per_sort_data[$akey]['early_time'] = '';
						$per_sort_data[$akey]['working_time'] = '';
						$per_sort_data[$akey]['firsthalf_status'] = '';
						$per_sort_data[$akey]['secondhalf_status'] = '';
						$per_sort_data[$akey]['excess_hours'] = '';
						//$late_inn = 0;
						//unset($per_sort_data['action'][$akey]);
					}	
				}
				if($filter_type == 2){
					if($late_inn == count($per_sort_data)){
						unset($final_datas[$rvalue['emp_code']]);	
					} else {
						$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
					}
					//$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
				//} elseif($working_inn > 3 && $filter_type == 3){
				} elseif($filter_type == 3){
					$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
				} elseif($filter_type == 1 || $filter_type == 5) {
					$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
				} elseif($filter_type == 4) {
					if($abs_in == count($per_sort_data)){
						unset($final_datas[$rvalue['emp_code']]);	
					} else {
						$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
					}
				} elseif($filter_type == 6) {
					if($manual_in == count($per_sort_data)){
						unset($final_datas[$rvalue['emp_code']]);	
					} else {
						$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
					}
				} else {
					unset($final_datas[$rvalue['emp_code']]);
				}
			}
		}
		

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		$url .= '&once=1';

		$final_datas = array_chunk($final_datas, 7000);

		// echo '<pre>';
		// print_r($final_datas);
		// exit;
		
		if($final_datas){
			if($filter_type == 1){
				$report_type = 'Defult';
			} elseif($filter_type == 2){
				$report_type = 'Late';
			} elseif ($filter_type == 3) {
				$report_type = 'Less Time';
			} elseif($filter_type == 4){
				$report_type = 'Absent';
			} elseif($filter_type == 5){
				$report_type = 'Excess';
			} elseif($filter_type == 6){
				$report_type = 'Manual';
			}

			if($filter_department){
				$department_names = $this->db->query("SELECT `d_name` FROM `oc_department` WHERE `department_id` = '".$this->request->get['filter_department']."' ");
				if($department_names->num_rows > 0){
					$department_name = $department_names->row['d_name'];
				} else {
					$department_name = '';
				}
				$department_name = html_entity_decode($department_name);
				$department = $department_name;
			} else {
				$department = 'All';
			}

			if($filter_unit){
				$unit_names = $this->db->query("SELECT `unit` FROM `oc_unit` WHERE `unit_id` = '".$this->request->get['filter_unit']."' ");
				if($unit_names->num_rows > 0){
					$unit_name = $unit_names->row['unit'];
				} else {
					$unit_name = '';
				}
				$unit_name = html_entity_decode($unit_name);
				$unit = $unit_name;
			} else {
				$unit = 'All';
			}

			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$template = new Template();		
			$template->data['final_datass'] = $final_datas;
			$template->data['date_start'] = $filter_date_start;
			$template->data['filter_type'] = $filter_type;
			$template->data['date_end'] = $filter_date_end;
			$template->data['filter_department'] = $department;
			$template->data['filter_unit'] = $unit;
			$template->data['days'] = $day;
			$template->data['report_type'] = $report_type;
			$template->data['title'] = 'Periodic Report';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/performance_html.tpl');
			//echo $html;exit;
			$filename = "Periodic_Report";
			
			//if($filter_type == 6){
				header('Content-type: text/html');
				header('Content-Disposition: attachment; filename='.$filename.".html");
				echo $html;
			// } else {
			// 	header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			// 	header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			// 	header("Expires: 0");
			// 	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			// 	header("Cache-Control: private",false);
			// 	echo $html;
			// 	exit;
			// }		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/performance', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			if(isset($this->session->data['dept_names'])){
				$filter_departments = html_entity_decode($this->session->data['dept_names']);
				$filter_departments = "'" . str_replace(",", "','", html_entity_decode($filter_departments)) . "'";
			} else {
				$filter_departments = '';
			}

			if($filter_departments == ''){
				if(isset($this->session->data['dept_name'])){
					$filter_department = $this->session->data['dept_name'];
				} else {
					$filter_department = '';
				}
			} else {
				$filter_department = '';
			}

			if(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
				$filter_reporting_to = $this->session->data['emp_code'];
			} else {
				$filter_reporting_to = '';
			}



			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_department' => $filter_department,
				'filter_departments' => $filter_departments,
				'filter_reporting_to' => $filter_reporting_to,
				'filter_date_start' => $this->request->get['filter_date_start'],
				'filter_dol' => 1,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['first_name'].' '.$result['last_name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>