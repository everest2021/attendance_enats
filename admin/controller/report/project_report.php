<?php
class ControllerReportProjectreport extends Controller { 
	public function index() { 

		$this->language->load('report/project_report');
		$this->load->model('catalog/employee');
		$this->load->model('catalog/project');
		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$filter_name_id = $this->session->data['emp_code'];
		} else {
			$filter_name_id = '';
		}
		if (isset($this->request->get['filter_project_name'])) {
			$filter_project_name = $this->request->get['filter_project_name'];
		} elseif(isset($this->session->data['id']) && !$this->user->isAdmin()){
			$proj_name = $this->db->query("SELECT `name` FROM `oc_project` WHERE `id` = '".$this->session->data['id']."' ")->rows;
			$filter_project_name = $proj_name;
		} else {
			$filter_project_name = '';
		}

		if (isset($this->request->get['filter_project_id'])) {
			$filter_project_id = $this->request->get['filter_project_id'];
		} elseif(isset($this->session->data['id']) && !$this->user->isAdmin()){
			$filter_project_id = $this->session->data['id'];
		} else {
			$filter_project_id = '';
		}

		
		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_project_name'])) {
			$url .= '&filter_project_name=' . $this->request->get['filter_project_name'];
		}
		if (isset($this->request->get['filter_project_id'])) {
			$url .= '&filter_project_id=' . $this->request->get['filter_project_id'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/project_report', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);
		//$this->data['generate_today'] = HTTP_CATALOG.'service/dataprocess.php';//$this->url->link('transaction/transaction/generate_today', 'token=' . $this->session->data['token'] . $url, 'SSL');
		

		$this->load->model('catalog/expense_transaction');
		$this->load->model('catalog/employee');
		$this->load->model('catalog/project');

		$this->data['project_report'] = array();
		$final_datas = array();
		$data = array(
			'filter_name_id'	=>$filter_name_id,
			'filter_name'		=>$filter_name,
			'filter_project_id'	=>$filter_project_id,
			'filter_project_name'=>$filter_project_name,
			'filter_date_start'	=>$filter_date_start,
			'filter_date_end'	=>$filter_date_end,
			'start'             => ($page - 1) * 7000,
			'limit'             => 7000
		);
		// echo'<pre>';
		// print_r($data);
		// exit();

		$sql ="SELECT * FROM `oc_project` WHERE 1=1 ";
		if (isset($data['filter_project_id']) && !empty($data['filter_project_id'])) {
			$sql .= " AND id = '" . $data['filter_project_id'] . "' ";	
		}
		$sql .= " ORDER BY `id` ";
		$pro_id = $this->db->query($sql)->rows;
		// echo '<pre>';
		// print_r($pro_id);
		// exit;
		foreach ($pro_id as  $pvalue) {
			$emp_trans = array();
			$sql = "SELECT *, pt.`id` AS `proj_id`, pt.`project_id` AS `project_id` FROM `oc_project_transection` pt LEFT JOIN `oc_project_employee` pe ON(pt.`id` = pe.`project_id`) WHERE pt.`project_id` = '".$pvalue['id']."' ";			
			if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
				$sql .= " AND pe.`employee_id` = '" . $data['filter_name_id'] . "' ";
			}
			$pro_data = $this->db->query($sql)->rows;
			// echo '<pre>';
			// print_r($pro_data);
			// exit;
			foreach ($pro_data as $ekey => $evalue) {				
				$pro_trans = $this->db->query("SELECT `emp_name`,`leave_status`, `present_status`, `absent_status`, `holiday_id`, `weekly_off`, `date` FROM `oc_transaction` WHERE emp_id ='".$evalue['employee_id']."'AND project_id ='".$evalue['project_id']."'  ORDER BY `date` ASC ");
				if($pro_trans->num_rows > 0){
					$pro_tran = $pro_trans->rows;
					// echo '<pre>';
					// print_r($pro_tran);
					// exit;
					end($pro_tran);
					$lkey = key($pro_tran);
					$leave_count = 0;
					$holiday_count = 0;
					$week_count = 0;
					$present_count = 0;
					$absent_count = 0;
					// echo '<pre>';
					// print_r($pro_tran);
					// exit;					 
					foreach ($pro_tran as $tkey => $tvalue) {
						if($tvalue['leave_status'] != '0' && $tvalue['leave_status'] != ''){
							$leave_count ++;
						} elseif($tvalue['holiday_id'] != '0' && $tvalue['holiday_id'] != '') {
							$holiday_count ++;
						} elseif($tvalue['weekly_off'] != '0' && $tvalue['weekly_off'] != '') {
							$week_count ++;
						} elseif($tvalue['present_status'] == '1') {
							$present_count ++;
						} else {
							$absent_count ++;
						}
					}

					if(isset($pro_tran[0]['date'])){
						$start_date = date('d-m-Y', strtotime($pro_tran[0]['date']));
					} else {
						$start_date = '';
					}
					if(isset($pro_tran[$lkey]['date'])){
						$end_date = date('d-m-Y', strtotime($pro_tran[$lkey]['date']));
					} else {
						$end_date = '';
					}
					//$emp
					$emp_trans[] = array(
						'emp_name' => $tvalue['emp_name'],
						'start_date' => $start_date,
						'end_date' => $end_date,
						'present' =>$present_count,
						'absent' =>$absent_count,
						'leave' =>$leave_count,
						'holiday' =>$holiday_count,
						'week' =>$week_count,
					);	
				}
			}
			if($emp_trans){
				$final_datas[] = array(
					'project_name' => $pvalue['name'],
					'emp_trans'  =>$emp_trans
				);
			}
		}
			
		$this->data['final_datas'] = $final_datas;

		$this->load->model('catalog/employee');
		$this->load->model('catalog/project');
		$this->data['project_report'] = array();


		
		// echo '<pre>';
		// print_r($data);
		// exit;		
		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		//$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_project_name'])) {
			$url .= '&filter_project_name=' . $this->request->get['filter_project_name'];
		}
		if (isset($this->request->get['filter_project_id'])) {
			$url .= '&filter_project_id=' . $this->request->get['filter_project_id'];
		}
		
		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_project_name'] = $filter_project_name;
		$this->data['filter_project_id'] = $filter_project_id;
		

		$this->template = 'report/project_report.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function array_sort($array, $on, $order=SORT_ASC){

	    $new_array = array();
	    $sortable_array = array();

	    if (count($array) > 0) {
	        foreach ($array as $k => $v) {
	            if (is_array($v)) {
	                foreach ($v as $k2 => $v2) {
	                    if ($k2 == $on) {
	                        $sortable_array[$k] = $v2;
	                    }
	                }
	            } else {
	                $sortable_array[$k] = $v;
	            }
	        }

	        switch ($order) {
	            case SORT_ASC:
	                asort($sortable_array);
	                break;
	            case SORT_DESC:
	                arsort($sortable_array);
	                break;
	        }

	        foreach ($sortable_array as $k => $v) {
	            $new_array[$k] = $array[$k];
	        }
	    }

	    return $new_array;
	}

	public function export(){
		$this->language->load('report/project_report');
		$this->load->model('catalog/employee');
		$this->load->model('catalog/project');
		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$filter_name_id = $this->session->data['emp_code'];
		} else {
			$filter_name_id = '';
		}
		if (isset($this->request->get['filter_project_name'])) {
			$filter_project_name = $this->request->get['filter_project_name'];
		} elseif(isset($this->session->data['id']) && !$this->user->isAdmin()){
			$proj_name = $this->db->query("SELECT `name` FROM `oc_project` WHERE `id` = '".$this->session->data['id']."' ")->rows;
			$filter_project_name = $proj_name;
		} else {
			$filter_project_name = '';
		}

		if (isset($this->request->get['filter_project_id'])) {
			$filter_project_id = $this->request->get['filter_project_id'];
		} elseif(isset($this->session->data['id']) && !$this->user->isAdmin()){
			$filter_project_id = $this->session->data['id'];
		} else {
			$filter_project_id = '';
		}

		
		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_project_name'])) {
			$url .= '&filter_project_name=' . $this->request->get['filter_project_name'];
		}
		if (isset($this->request->get['filter_project_id'])) {
			$url .= '&filter_project_id=' . $this->request->get['filter_project_id'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/project_report', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);
		//$this->data['generate_today'] = HTTP_CATALOG.'service/dataprocess.php';//$this->url->link('transaction/transaction/generate_today', 'token=' . $this->session->data['token'] . $url, 'SSL');
		

		$this->load->model('catalog/expense_transaction');
		$this->load->model('catalog/employee');
		$this->load->model('catalog/project');

		$this->data['project_report'] = array();
		$final_datas = array();
		$data = array(
			'filter_name_id'	=>$filter_name_id,
			'filter_name'		=>$filter_name,
			'filter_project_id'	=>$filter_project_id,
			'filter_project_name'=>$filter_project_name,
			'filter_date_start'	=>$filter_date_start,
			'filter_date_end'	=>$filter_date_end,
			'start'             => ($page - 1) * 7000,
			'limit'             => 7000
		);
		// echo'<pre>';
		// print_r($data);
		// exit();
		$sql ="SELECT * FROM `oc_project` WHERE 1=1 ";
		if (isset($data['filter_project_id']) && !empty($data['filter_project_id'])) {
			$sql .= " AND id = '" . $data['filter_project_id'] . "' ";
			$sql .= " ORDER BY `id` ";
		}
		$pro_id = $this->db->query($sql)->rows;
		foreach ($pro_id as  $pvalue) {
			$emp_trans = array();
			$sql = "SELECT *, pt.`id` AS `proj_id`, pt.`project_id` AS `project_id` FROM `oc_project_transection` pt LEFT JOIN `oc_project_employee` pe ON(pt.`id` = pe.`project_id`) WHERE pt.`project_id` = '".$pvalue['id']."' ";			
			if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
				$sql .= " AND pe.`employee_id` = '" . $data['filter_name_id'] . "' ";
			}
			$pro_data = $this->db->query($sql)->rows;
			foreach ($pro_data as $ekey => $evalue) {				
				$pro_trans = $this->db->query("SELECT `emp_name`,`leave_status`, `present_status`, `absent_status`, `holiday_id`, `weekly_off`, `date` FROM `oc_transaction` WHERE emp_id ='".$evalue['employee_id']."'AND project_id ='".$evalue['project_id']."'  ORDER BY `date` ASC ");
				if($pro_trans->num_rows > 0){
					$pro_tran = $pro_trans->rows;
					// echo '<pre>';
					// print_r($pro_tran);
					// exit;
					end($pro_tran);
					$lkey = key($pro_tran);
					$leave_count = 0;
					$holiday_count = 0;
					$week_count = 0;
					$present_count = 0;
					$absent_count = 0;					 
					foreach ($pro_tran as $tkey => $tvalue) {
						if($tvalue['leave_status'] != '0' && $tvalue['leave_status'] != ''){
							$leave_count ++;
						} elseif($tvalue['holiday_id'] != '0' && $tvalue['holiday_id'] != '') {
							$holiday_count ++;
						} elseif($tvalue['weekly_off'] != '0' && $tvalue['weekly_off'] != '') {
							$week_count ++;
						} elseif($tvalue['present_status'] == '1') {
							$present_count ++;
						} else {
							$absent_count ++;
						}
					}
					if(isset($pro_tran[0]['date'])){
						$start_date = date('d-m-Y', strtotime($pro_tran[0]['date']));
					} else {
						$start_date = '';
					}
					if(isset($pro_tran[$lkey]['date'])){
						$end_date = date('d-m-Y', strtotime($pro_tran[$lkey]['date']));
					} else {
						$end_date = '';
					}
					//$emp
					$emp_trans[] = array(
						'emp_name' => $tvalue['emp_name'],
						'start_date' => $start_date,
						'end_date' => $end_date,
						'present' =>$present_count,
						'absent' =>$absent_count,
						'leave' =>$leave_count,
						'holiday' =>$holiday_count,
						'week' =>$week_count,
					);	
				}
			}
			if($emp_trans){
				$final_datas[] = array(
					'project_name' => $pvalue['name'],
					'emp_trans'  =>$emp_trans
				);
			}
		}
			
		$this->data['final_datas'] = $final_datas;
			
		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		
		$url .= '&once=1';

		

			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			//$template->data['date_start'] = $filter_date_start;
			//$template->data['date_end'] = $filter_date_end;
			$template->data['title'] = 'Project Wise Report';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/project_wise_report_html.tpl');
			//echo $html;exit;
			$filename = "Project_Wise_Report";
			
			//if($filter_type == 6){
				// header('Content-type: text/html');
				// header('Content-Disposition: attachment; filename='.$filename.".html");
				// echo $html;
			// } else {
				header("Content-Type: application/vnd.ms-excel; charset=utf-8");
				header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Cache-Control: private",false);
				echo $html;
				exit;
			// }		
		// } else {
		// 	$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/project_report', 'token=' . $this->session->data['token'].$url, 'SSL'));
		//}
	}


	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			if(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
				$filter_reporting_to = $this->session->data['emp_code'];
			} else {
				$filter_reporting_to = '';
			}

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				//'filter_date_start' => $this->request->get['filter_date_start'],
				//'filter_dol' => 1,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['first_name'].' '.$result['last_name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
	public function autocomplete_project() {
		$json = array();

		if (isset($this->request->get['filter_project_name'])) {
			$this->load->model('catalog/project');

			$data = array(
				'filter_project_name' => $this->request->get['filter_project_name'],
				//'id'		=>$this->request->get['filter_id'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_project->getprojects($data);

			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);		
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>

