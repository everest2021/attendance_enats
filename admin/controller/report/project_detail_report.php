<?php
class ControllerReportProjectdetailreport extends Controller { 
	public function index() { 

		$this->language->load('report/project_detail_report');
		$this->load->model('catalog/employee');
		$this->load->model('catalog/project');
		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$filter_name_id = $this->session->data['emp_code'];
		} else {
			$filter_name_id = '';
		}
		if (isset($this->request->get['filter_project_name'])) {
			$filter_project_name = $this->request->get['filter_project_name'];
		} elseif(isset($this->session->data['id']) && !$this->user->isAdmin()){
			$proj_name = $this->db->query("SELECT `name` FROM `oc_project` WHERE `id` = '".$this->session->data['id']."' ")->rows;
			$filter_project_name = $proj_name;
		} else {
			$filter_project_name = '';
		}

		if (isset($this->request->get['filter_project_id'])) {
			$filter_project_id = $this->request->get['filter_project_id'];
		} elseif(isset($this->session->data['id']) && !$this->user->isAdmin()){
			$filter_project_id = $this->session->data['id'];
		} else {
			$filter_project_id = '';
		}

		
		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_project_name'])) {
			$url .= '&filter_project_name=' . $this->request->get['filter_project_name'];
		}
		if (isset($this->request->get['filter_project_id'])) {
			$url .= '&filter_project_id=' . $this->request->get['filter_project_id'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/project_detail_report', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);
		// $this->url->link('transaction/transaction/generate_today', 'token=' . $this->session->data['token'] . $url, 'SSL');
		

		$this->load->model('catalog/expense_transaction');
		$this->load->model('catalog/employee');
		$this->load->model('catalog/project');

		$this->data['project_report'] = array();
		$final_datas = array();
		$data = array(
			'filter_name_id'	=>$filter_name_id,
			'filter_name'		=>$filter_name,
			'filter_project_id'	=>$filter_project_id,
			'filter_project_name'=>$filter_project_name,
			'filter_date_start'	=>$filter_date_start,
			'filter_date_end'	=>$filter_date_end,
			'start'             => ($page - 1) * 7000,
			'limit'             => 7000
		);
		

		$sql = "SELECT * FROM `oc_project_mb_data` WHERE 1=1 ";
		if (isset($data['filter_project_id']) && !empty($data['filter_project_id'])) {
			$sql .= " AND project_id = '" . $data['filter_project_id'] . "' ";	
		}
		$details_data = $this->db->query($sql)->rows;
		foreach ($details_data as $dkey => $dvalue) {
			$sql = "SELECT * FROM `oc_project_mb_point_data` WHERE `project_mb_data_id` = '".$dvalue['id']."' ";
			$sql .= " GROUP BY `project_mb_data_id` ";
			$map_datas = $this->db->query($sql)->rows;
			foreach($map_datas as $mvalue){
				$link = HTTP_CATALOG.'detail_map.php?project_mb_data_id='.$mvalue['project_mb_data_id'];//$this->url->link('report/employee_location/map_fun', 'token=' . $this->session->data['token'] . $url . '&emp_id='.$dvalue['emp_id'] . '&date='.$dvalue['date'], 'SSL');
				$final_datas[] = array(
					'contractor_name' => $dvalue['contractor_name'],
					'route_name' => $dvalue['route_name'],
					'project_name' => $dvalue['project_name'],
					'link_section' => $dvalue['link_section'],
					'work_order_no' => $dvalue['work_order_no'],
					'doc_no' => $dvalue['doc_no'],
					'dept' => $dvalue['dept'],
					'link' =>$link,
					'href'  => $this->url->link('report/project_detail_report/export', 'token=' . $this->session->data['token'] . $url, 'SSL')
					
				);	
			}
		}
		// echo'<pre>';
		// print_r($link);
		// exit;
			
		$this->data['final_datas'] = $final_datas;

		$this->load->model('catalog/employee');
		$this->load->model('catalog/project');
		$this->data['project_report'] = array();


		
		// echo '<pre>';
		// print_r($data);
		// exit;		
		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		//$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_project_name'])) {
			$url .= '&filter_project_name=' . $this->request->get['filter_project_name'];
		}
		if (isset($this->request->get['filter_project_id'])) {
			$url .= '&filter_project_id=' . $this->request->get['filter_project_id'];
		}
		
		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_project_name'] = $filter_project_name;
		$this->data['filter_project_id'] = $filter_project_id;
		

		$this->template = 'report/project_detail_report.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	
	public function array_sort($array, $on, $order=SORT_ASC){

	    $new_array = array();
	    $sortable_array = array();

	    if (count($array) > 0) {
	        foreach ($array as $k => $v) {
	            if (is_array($v)) {
	                foreach ($v as $k2 => $v2) {
	                    if ($k2 == $on) {
	                        $sortable_array[$k] = $v2;
	                    }
	                }
	            } else {
	                $sortable_array[$k] = $v;
	            }
	        }

	        switch ($order) {
	            case SORT_ASC:
	                asort($sortable_array);
	                break;
	            case SORT_DESC:
	                arsort($sortable_array);
	                break;
	        }

	        foreach ($sortable_array as $k => $v) {
	            $new_array[$k] = $array[$k];
	        }
	    }

	    return $new_array;
	}

	public function export(){
		$this->language->load('report/project_report');
		$this->load->model('catalog/employee');
		$this->load->model('catalog/project');
		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$filter_name_id = $this->session->data['emp_code'];
		} else {
			$filter_name_id = '';
		}
		if (isset($this->request->get['filter_project_name'])) {
			$filter_project_name = $this->request->get['filter_project_name'];
		} elseif(isset($this->session->data['id']) && !$this->user->isAdmin()){
			$proj_name = $this->db->query("SELECT `name` FROM `oc_project` WHERE `id` = '".$this->session->data['id']."' ")->rows;
			$filter_project_name = $proj_name;
		} else {
			$filter_project_name = '';
		}

		if (isset($this->request->get['filter_project_id'])) {
			$filter_project_id = $this->request->get['filter_project_id'];
		} elseif(isset($this->session->data['id']) && !$this->user->isAdmin()){
			$filter_project_id = $this->session->data['id'];
		} else {
			$filter_project_id = '';
		}

		
		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_project_name'])) {
			$url .= '&filter_project_name=' . $this->request->get['filter_project_name'];
		}
		if (isset($this->request->get['filter_project_id'])) {
			$url .= '&filter_project_id=' . $this->request->get['filter_project_id'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/project_report', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);
		//$this->data['generate_today'] = HTTP_CATALOG.'service/dataprocess.php';//$this->url->link('transaction/transaction/generate_today', 'token=' . $this->session->data['token'] . $url, 'SSL');
		

		$this->load->model('catalog/expense_transaction');
		$this->load->model('catalog/employee');
		$this->load->model('catalog/project');

		$this->data['project_report'] = array();
		$final_datas = array();
		$data = array(
			'filter_name_id'	=>$filter_name_id,
			'filter_name'		=>$filter_name,
			'filter_project_id'	=>$filter_project_id,
			'filter_project_name'=>$filter_project_name,
			'filter_date_start'	=>$filter_date_start,
			'filter_date_end'	=>$filter_date_end,
			'start'             => ($page - 1) * 7000,
			'limit'             => 7000
		);
		
		$sql = "SELECT * FROM `oc_project_mb_data` WHERE 1=1 ";
		if (isset($data['filter_project_id']) && !empty($data['filter_project_id'])) {
			$sql .= " AND project_id = '" . $data['filter_project_id'] . "' ";	
		}
		$project_mb_data = $this->db->query($sql)->rows;
		foreach ($project_mb_data as $mkey => $mvalue) {
			$project_mb_point_data = $this->db->query("SELECT * FROM `oc_project_mb_point_data` WHERE `project_mb_data_id` = '".$mvalue['id']."' ")->rows;
			// echo'<pre>';
			// print_r($project_mb_point_data);
			// exit;
			foreach ($project_mb_point_data as $pkey => $pvalue) {
				$point_datas[] = array(
					'location' =>$pvalue['location'],
					'length' =>$pvalue['length'],
					'depth' =>$pvalue['depth'],
					'lat_lon' =>$pvalue['lat'].','.$pvalue['lon'],
					'strata' =>$pvalue['strata'],
					'edge_of_road' =>$pvalue['edge_of_road'],
					'center_of_road' =>$pvalue['center_of_road'],
					'chainage_from' =>$pvalue['chainage_from'],
					'chainage_to' =>$pvalue['chainage_to'],
					'open_trenching_moiling_hdd' =>$pvalue['open_trenching_moiling_hdd'],
					'sample_abd' =>$pvalue['sample_abd'],
					'culvert_bridge_road_railway_length' =>$pvalue['culvert_bridge_road_railway_length'],
					'hdpe_duct' =>$pvalue['hdpe_duct'],
					'gi_glass_b' =>$pvalue['gi_glass_b'],
					'dwc' =>$pvalue['dwc'],
					'rcc' =>$pvalue['rcc'],
					'stone' =>$pvalue['stone'],
					'pcc' =>$pvalue['pcc'],
					'couplers' =>$pvalue['couplers'],
					'jc_ptc_hh' =>$pvalue['jc_ptc_hh'],
					'rm' =>$pvalue['rm'],
					'remarks' =>$pvalue['remarks'],
				);	
			}
		}
		$total_datas = $this->db->query("SELECT SUM(`length`) as `total_l`, SUM(`culvert_bridge_road_railway_length`) as `total_cbrrl`,SUM(`hdpe_duct`) as `total_hdpe`,SUM(`gi_glass_b`) as `total_gi`,SUM(`dwc`) as `total_dwc`,SUM(`rcc`) as `total_rcc`,SUM(`stone`) as `total_stone`,SUM(`pcc`) as `total_pcc`,SUM(`couplers`) as `total_cup`,SUM(`jc_ptc_hh`) as `total_jc`,SUM(`rm`) as `total_rm` FROM `oc_project_mb_point_data` WHERE `project_mb_data_id` = '".$mvalue['id']."' ")->row;
		// echo'<pre>';
		// print_r($total_datas);
		// exit;
		$final_datas = array(
			'dept' => $mvalue['dept'],
			'doc_no' => $mvalue['doc_no'],
			'rev_no' => $mvalue['rev_no'],
			'signature_path' => $mvalue['signature_path'],
			'date' => $mvalue['date'],
			'contractor_name' => $mvalue['contractor_name'],
			'route_name' => $mvalue['route_name'],
			'link_section' => $mvalue['link_section'],
			'work_order_no' => $mvalue['work_order_no'],
			'ofc_blowing' => $mvalue['ofc_blowing'],
			'start_meter_reading' => $mvalue['start_meter_reading'],
			'end_meter_reading' => $mvalue['end_meter_reading'],
			'total_l' =>$total_datas['total_l'],
			'total_cbrrl' =>$total_datas['total_cbrrl'],
			'total_hdpe' =>$total_datas['total_hdpe'],
			'total_gi' =>$total_datas['total_gi'],
			'total_dwc' =>$total_datas['total_dwc'],
			'total_rcc' =>$total_datas['total_rcc'],
			'total_stone' =>$total_datas['total_stone'],
			'total_pcc' =>$total_datas['total_pcc'],
			'total_cup' =>$total_datas['total_cup'],
			'total_jc' =>$total_datas['total_jc'],
			'total_rm' =>$total_datas['total_rm'],
			'point_datas'  =>$point_datas
		);
			
		$this->data['final_datas'] = $final_datas;
		// echo'<pre>';
		// print_r($final_datas);
		// exit();
			
		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		
		$url .= '&once=1';
		//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
		$template = new Template();		
		$template->data['final_datas'] = $final_datas;
		// $template->data['date_start'] = $filter_date_start;
		// $template->data['date_end'] = $filter_date_end;
		$template->data['title'] = 'Project Detail Report';
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$template->data['base'] = HTTPS_SERVER;
		} else {
			$template->data['base'] = HTTP_SERVER;
		}
		$html = $template->fetch('report/project_detail_report_html.tpl');
		//echo $html;exit;
		$filename = "Project_Detail_Report";
		header("Content-Type: application/vnd.ms-excel; charset=utf-8");
		header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false);
		echo $html;
		exit;
		$this->redirect($this->url->link('report/project_detail_report', 'token=' . $this->session->data['token'].$url, 'SSL'));
	}


	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			if(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
				$filter_reporting_to = $this->session->data['emp_code'];
			} else {
				$filter_reporting_to = '';
			}

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				//'filter_date_start' => $this->request->get['filter_date_start'],
				//'filter_dol' => 1,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['first_name'].' '.$result['last_name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
	public function autocomplete_project() {
		$json = array();

		if (isset($this->request->get['filter_project_name'])) {
			$this->load->model('catalog/project');

			$data = array(
				'filter_project_name' => $this->request->get['filter_project_name'],
				//'id'		=>$this->request->get['filter_id'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_project->getprojects($data);

			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);		
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>

