<?php
class ControllerReportToday extends Controller { 
	public function index() {  
		$this->language->load('report/today');
		$this->load->model('catalog/employee');
		$this->load->model('transaction/transaction');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}
		$is_head = 0;

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
			$emp_is_head_data = $this->db->query("SELECT * FROM `oc_employee` WHERE reporting_to = '".$this->request->get['filter_name_id']."' ");
			if($emp_is_head_data->num_rows > 0){
				$is_head = 1;
			} else {
				$is_head = 0;
			}
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$filter_name_id = $this->session->data['emp_code'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_project_name'])) {
			$filter_project_name = $this->request->get['filter_project_name'];
		} elseif(isset($this->session->data['id']) && !$this->user->isAdmin()){
			$proj_name = $this->db->query("SELECT `project_name` FROM `oc_project_transection` WHERE `id` = '".$this->session->data['id']."' ")->rows;
			
			$filter_project_name = $proj_name;
		} else {
			$filter_project_name = '';
		}

		if (isset($this->request->get['filter_project_id'])) {
			$filter_project_id = $this->request->get['filter_project_id'];
		} elseif(isset($this->session->data['id']) && !$this->user->isAdmin()){
			$filter_project_id = $this->session->data['id'];
		} else {
			$filter_project_id = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_project_name'])) {
			$url .= '&filter_project_name=' . $this->request->get['filter_project_name'];
		}
		if (isset($this->request->get['filter_project_id'])) {
			$url .= '&filter_project_id=' . $this->request->get['filter_project_id'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/today', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);
		$this->data['generate_today'] = HTTP_CATALOG.'service/dataprocess.php';//$this->url->link('transaction/transaction/generate_today', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['export'] = $this->url->link('report/today/export', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');
		$this->load->model('transaction/transaction');

		$this->data['today'] = array();
		$final_datas = array();
		$google_map = array();

		$data = array(
			'filter_name_id'	=>$filter_name_id,
			'filter_name'		=>$filter_name,
			'filter_project_id'	=>$filter_project_id,
			'filter_project_name'=>$filter_project_name,
			'start'             => ($page - 1) * 7000,
			'limit'             => 7000
		);
		// echo '<pre>';
		// print_r($data);
		// exit();
		
		$sql = "SELECT * FROM `oc_transaction` WHERE `date` = '".date('Y-m-d')."' ";
		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
				$sql .= " AND `emp_id` = '" . $data['filter_name_id'] . "' ";
		}
		if (isset($data['filter_project_id']) && !empty($data['filter_project_id'])) {
			$sql .= " AND `project_id` = '" . $data['filter_project_id'] . "' ";	
		}
		$sql .= "GROUP BY `emp_id`";
		$filt_name = $this->db->query($sql)->rows;
		foreach ($filt_name as $skey => $svalue) {	
			$sql = " SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$svalue['emp_id']."' AND date = '".date('Y-m-d')."' ";
			$head_trans_datas = $this->db->query($sql)->rows;
			$emp_tran_data = array();
			foreach($head_trans_datas as $hkey => $hvalue){
				$pro_names = $this->db->query(" SELECT `name` FROM `oc_project` WHERE `id` = '".$hvalue['project_id']."' ");
				$pro_name = '';
				if($pro_names->num_rows > 0){
					$pro_name = $pro_names->row['name'];
				}
				
				$google_map = array(
					'latitude_in' => $hvalue['latitude_in'],
					'longitude_in' => $hvalue['longitude_in']	
				);

				if($google_map['latitude_in'] != '' && $google_map['longitude_in'] != ''){
				  	$map = 'http://maps.google.com/?q= @'.$hvalue['latitude_in'].','. $hvalue['longitude_in'];
				}else{
					$map = '';
				}  	

				$emp_tran_data[] = array(
					'date' =>$svalue['date'],
					'project_name' =>$pro_name,
					'is_head' => $hvalue['is_head'],
					'photo_source' => $hvalue['photo_source'],
					'late_time' => $hvalue['late_time'],
					'act_intime' => $hvalue['act_intime'],
					'photo_src' => $map,
				);
			}
			$final_datas[] = array(	
				'name' =>$svalue['emp_name'],
				'emp_id' =>$svalue['emp_id'],
				'employee' => $emp_tran_data,							
			);
		}	
			
		$this->data['final_datas'] = $final_datas;
		// echo'<pre>';
		// print_r($final_datas);
		// exit;

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_project_name'])) {
			$url .= '&filter_project_name=' . $this->request->get['filter_project_name'];
		}
		if (isset($this->request->get['filter_project_id'])) {
			$url .= '&filter_project_id=' . $this->request->get['filter_project_id'];
		}
		
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_project_name'] = $filter_project_name;
		$this->data['filter_project_id'] = $filter_project_id;
		
		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->template = 'report/today.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function export(){
		$this->language->load('report/today');
		$this->document->setTitle($this->language->get('heading_title'));
		$filter_date_start = date('Y-m-d');

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}
		$is_head = 0;

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
			$emp_is_head_data = $this->db->query("SELECT * FROM `oc_employee` WHERE reporting_to = '".$this->request->get['filter_name_id']."' ");
			if($emp_is_head_data->num_rows > 0){
				$is_head = 1;
			} else {
				$is_head = 0;
			}
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$filter_name_id = $this->session->data['emp_code'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_project_name'])) {
			$filter_project_name = $this->request->get['filter_project_name'];
		} elseif(isset($this->session->data['id']) && !$this->user->isAdmin()){
			$proj_name = $this->db->query("SELECT `project_name` FROM `oc_project_transection` WHERE `id` = '".$this->session->data['id']."' ")->rows;
			
			$filter_project_name = $proj_name;
		} else {
			$filter_project_name = '';
		}

		if (isset($this->request->get['filter_project_id'])) {
			$filter_project_id = $this->request->get['filter_project_id'];
		} elseif(isset($this->session->data['id']) && !$this->user->isAdmin()){
			$filter_project_id = $this->session->data['id'];
		} else {
			$filter_project_id = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_project_name'])) {
			$url .= '&filter_project_name=' . $this->request->get['filter_project_name'];
		}
		if (isset($this->request->get['filter_project_id'])) {
			$url .= '&filter_project_id=' . $this->request->get['filter_project_id'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/today', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);
		$this->data['generate_today'] = HTTP_CATALOG.'service/dataprocess.php';//$this->url->link('transaction/transaction/generate_today', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['export'] = $this->url->link('report/today/export', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');
		$this->load->model('transaction/transaction');

		$this->data['today'] = array();
		$final_datas = array();
		$google_map = array();
		$data = array(
			'filter_name_id'	=>$filter_name_id,
			'filter_name'		=>$filter_name,
			'filter_project_id'	=>$filter_project_id,
			'filter_project_name'=>$filter_project_name,
			// 'filter_unit'		=> $filter_unit,
			// 'filter_department'	=> $filter_department,
			'start'             => ($page - 1) * 7000,
			'limit'             => 7000
		);

		$sql = "SELECT * FROM `oc_transaction` WHERE `date` = '".date('Y-m-d')."' ";
		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
				$sql .= " AND `emp_id` = '" . $data['filter_name_id'] . "' ";
		}
		if (isset($data['filter_project_id']) && !empty($data['filter_project_id'])) {
			$sql .= " AND `project_id` = '" . $data['filter_project_id'] . "' ";	
		}
		$sql .= "GROUP BY `emp_id`";
		$filt_name = $this->db->query($sql)->rows;
		foreach ($filt_name as $skey => $svalue) {	
			$sql = " SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$svalue['emp_id']."' AND date = '".date('Y-m-d')."' ";
			$head_trans_datas = $this->db->query($sql)->rows;
			$emp_tran_data = array();
			foreach($head_trans_datas as $hkey => $hvalue){
				$pro_names = $this->db->query(" SELECT `name` FROM `oc_project` WHERE `id` = '".$hvalue['project_id']."' ");
				$pro_name = '';
				if($pro_names->num_rows > 0){
					$pro_name = $pro_names->row['name'];
				}
				
				$google_map = array(
					'latitude_in' => $hvalue['latitude_in'],
					'longitude_in' => $hvalue['longitude_in']	
				);

				if($google_map['latitude_in'] != '' && $google_map['longitude_in'] != ''){
				  	$map = 'https://www.google.com/maps/'.'@'.$hvalue['latitude_in'].','. $hvalue['longitude_in'];
				}else{
					$map = '';
				}  	

				$emp_tran_data[] = array(
					'date' =>$svalue['date'],
					'project_name' =>$pro_name,
					'is_head' => $hvalue['is_head'],
					'photo_source' => $hvalue['photo_source'],
					'late_time' => $hvalue['late_time'],
					'act_intime' => $hvalue['act_intime'],
					'photo_src' => $map,
				);
			}
			$final_datas[] = array(	
				'name' =>$svalue['emp_name'],
				'emp_id' =>$svalue['emp_id'],
				'employee' => $emp_tran_data,							
			);
		}	
			
		$this->data['final_datas'] = $final_datas;

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_project_name'])) {
			$url .= '&filter_project_name=' . $this->request->get['filter_project_name'];
		}
		if (isset($this->request->get['filter_project_id'])) {
			$url .= '&filter_project_id=' . $this->request->get['filter_project_id'];
		}
		
		
		//if($final_datas){
			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			//$tdays = cal_days_in_month(CAL_GREGORIAN, $filter_month, $filter_year);
			$template = new Template();		
			$template->data['final_datass'] = $final_datas;
			$template->data['date_start'] = $filter_date_start;
			//$template->data['department'] = $department;
			//$template->data['unit'] = $unit;
			$template->data['title'] = 'Todays Attendance Report';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/today_html.tpl');
			//echo $html;exit;
			$filename = "Todays_Attendance.html";
			header('Content-type: text/html');
			header('Content-Disposition: attachment; filename='.$filename);
			echo $html;
			// $filename = "Todays_Attendance_".$department;
			// header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			// header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			// header("Expires: 0");
			// header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			// header("Cache-Control: private",false);
			// echo $html;
			exit;		
		// } else {
		// 	$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/today', 'token=' . $this->session->data['token'].$url, 'SSL'));
		//}
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			if(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
				$filter_reporting_to = $this->session->data['emp_code'];
			} else {
				$filter_reporting_to = '';
			}

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				//'filter_date_start' => $this->request->get['filter_date_start'],
				//'filter_dol' => 1,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['first_name'].' '.$result['last_name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete_project() {
		$json = array();

		if (isset($this->request->get['filter_project_name'])) {
			$this->load->model('catalog/project_assignment');

			$data = array(
				'filter_project_name' => $this->request->get['filter_project_name'],
				//'id'		=>$this->request->get['filter_id'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_project_assignment->getprojectassignments($data);
		
			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'], 
					'name'            => strip_tags(html_entity_decode($result['project_name'], ENT_QUOTES, 'UTF-8'))
				);		
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>
