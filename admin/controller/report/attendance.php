<?php
class ControllerReportAttendance extends Controller { 
	public function index() {  
		$this->language->load('report/attendance');
		$this->load->model('catalog/employee');
		$this->load->model('transaction/transaction');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$filter_name_id = $this->session->data['emp_code'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = date('Y-m-d');
			$filter_date_start = date('d-m-Y', strtotime( "-1 day"));
		}
		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = date('Y-m-d');
			$filter_date_end = date('d-m-Y', strtotime( "-1 day"));
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = html_entity_decode($this->request->get['filter_unit']);
		} else {
			$filter_unit = '';
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = html_entity_decode($this->request->get['filter_department']);
		} else {
			$filter_department = 0;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = 1;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/attendance', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/attendance');
		$this->load->model('transaction/transaction');

		$this->data['attendace'] = array();

		$filter_reporting_head = 0;
		$filter_reporting_head_1 = 0;
		if(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$is_reporting_datas = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE `reporting_to` = '".$this->session->data['emp_code']."' ");
			if($is_reporting_datas->num_rows > 0){
				$filter_reporting_head = 1;
			} else {
				$filter_reporting_head = 0;
				$filter_reporting_head_1 = 1;
			}
		}

		$this->data['filter_reporting_head_1'] = $filter_reporting_head_1;
		$this->load->model('transaction/transaction');
		
		$this->data['attendance'] = array();
		$final_datas = array();
		$data = array(
			'filter_name_id'	=>$filter_name_id,
			'filter_name'		=>$filter_name,
			'filter_date_start'	    => $filter_date_start,
			'filter_date_end'		=>$filter_date_end,
			'filter_unit'			=> $filter_unit,
			'filter_department'		=> $filter_department,
			'filter_status'			=> $filter_status, 
			'filter_reporting_head' => $filter_reporting_head,
			'start'                 => ($page - 1) * 7000,
			'limit'                 => 7000
		);
		
			// echo'<pre>';
			// print_r($data);
			// exit;
		$sql = "SELECT * FROM `oc_transaction` WHERE `date` >= '".(date('Y-m-d', strtotime($filter_date_start)))."' AND `date` <='".(date('Y-m-d', strtotime($filter_date_end)))."' ";
		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $data['filter_name_id'] . "' ";
		}
		$sql .= " GROUP BY `emp_id` ";
		$empl_data = $this->db->query($sql)->rows;
		foreach ($empl_data as $ekey => $evalue) {
			$trans_data = array();
			$proj_data = $this->db->query("SELECT `date`,`act_intime`,`act_outtime`,`working_time`,`early_time`,`late_time`,`firsthalf_status`,`secondhalf_status`,`project_id` FROM `oc_transaction` WHERE `date` >= '".(date('Y-m-d', strtotime($filter_date_start)))."' AND `date` <='".(date('Y-m-d', strtotime($filter_date_end)))."' AND emp_id = '".$evalue['emp_id']."' ORDER BY `date` ")->rows;
			foreach ($proj_data as $pkey => $pvalue) {
				$pro_names = $this->db->query("SELECT * FROM `oc_project` WHERE `id` = '".$pvalue['project_id']."' ");
				$pro_name = '';
				if($pro_names->num_rows > 0){
					$pro_name = $pro_names->row['name'];
				}
				$firsthalf_status = 0;
				$secondhalf_status = 0;
				$status = 0;

				if($pvalue['firsthalf_status'] == '1'){
					$firsthalf_status ='P';
				} elseif($pvalue['firsthalf_status'] == '0') {
					$firsthalf_status ='A';
				} else {
					$firsthalf_status = $pvalue['firsthalf_status'];
				}

				if($pvalue['secondhalf_status'] == '1'){
					$secondhalf_status ='P';
				} elseif($pvalue['secondhalf_status'] == '0') {
					$secondhalf_status ='A';
				} else {
					$secondhalf_status = $pvalue['secondhalf_status'];
				}
				$status = $firsthalf_status.' / '.$secondhalf_status;

				$trans_data[] = array(
					'project_name' 	=>$pro_name,
					'date'			=>date('d-m-Y', strtotime($pvalue['date'])),
					'in'			=>$pvalue['act_intime'],
					'out'			=>$pvalue['act_outtime'],
					'working_time'	=>$pvalue['working_time'],
					'early_time'	=>$pvalue['early_time'],
					'late_time'		=>$pvalue['late_time'],
					'status'		=>$status
				);
			}
			if($trans_data){	
				$final_datas[] = array(
					'emp_name'  =>$evalue['emp_name'],
					'trans_data'  =>$trans_data
				);
			}

		}

		$this->data['final_datas'] = $final_datas;

		// echo'<pre>';
		// 	print_r($final_datas);
		// 	exit;
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->data['warning'])){
			$this->data['error_warning'] = $this->data['warning'];
		} elseif(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_unit'] = $filter_unit;
		$this->data['filter_department'] = $filter_department;
		$this->data['filter_status'] = $filter_status;

		$this->template = 'report/attendance.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	function array_sort($array, $on, $order=SORT_ASC){

	    $new_array = array();
	    $sortable_array = array();

	    if (count($array) > 0) {
	        foreach ($array as $k => $v) {
	            if (is_array($v)) {
	                foreach ($v as $k2 => $v2) {
	                    if ($k2 == $on) {
	                        $sortable_array[$k] = $v2;
	                    }
	                }
	            } else {
	                $sortable_array[$k] = $v;
	            }
	        }

	        switch ($order) {
	            case SORT_ASC:
	                asort($sortable_array);
	                break;
	            case SORT_DESC:
	                arsort($sortable_array);
	                break;
	        }

	        foreach ($sortable_array as $k => $v) {
	            $new_array[$k] = $array[$k];
	        }
	    }

	    return $new_array;
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function export(){
		$this->language->load('report/attendance');
		$this->load->model('catalog/employee');
		$this->load->model('transaction/transaction');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$filter_name_id = $this->session->data['emp_code'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = date('Y-m-d');
			$filter_date_start = date('d-m-Y', strtotime( "-1 day"));
		}
		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = date('Y-m-d');
			$filter_date_end = date('d-m-Y', strtotime( "-1 day"));
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = html_entity_decode($this->request->get['filter_unit']);
		} else {
			$filter_unit = '';
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = html_entity_decode($this->request->get['filter_department']);
		} else {
			$filter_department = 0;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = 1;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/attendance', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/attendance');
		$this->load->model('transaction/transaction');

		$this->data['attendace'] = array();

		$filter_reporting_head = 0;
		$filter_reporting_head_1 = 0;
		if(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$is_reporting_datas = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE `reporting_to` = '".$this->session->data['emp_code']."' ");
			if($is_reporting_datas->num_rows > 0){
				$filter_reporting_head = 1;
			} else {
				$filter_reporting_head = 0;
				$filter_reporting_head_1 = 1;
			}
		}

		$this->data['filter_reporting_head_1'] = $filter_reporting_head_1;
		$this->load->model('transaction/transaction');
		
		$this->data['attendance'] = array();
		$final_datas = array();
		$data = array(
			'filter_name_id'	=>$filter_name_id,
			'filter_name'		=>$filter_name,
			'filter_date_start'	    => $filter_date_start,
			'filter_date_end'		=>$filter_date_end,
			'filter_unit'			=> $filter_unit,
			'filter_department'		=> $filter_department,
			'filter_status'			=> $filter_status, 
			'filter_reporting_head' => $filter_reporting_head,
			'start'                 => ($page - 1) * 7000,
			'limit'                 => 7000
		);


		$sql = "SELECT * FROM `oc_transaction` WHERE `date` >= '".(date('Y-m-d', strtotime($filter_date_start)))."' AND `date` <='".(date('Y-m-d', strtotime($filter_date_end)))."' ";
		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $data['filter_name_id'] . "' ";
		}
		$sql .= " GROUP BY `emp_id` ";
		$empl_data = $this->db->query($sql)->rows;
		foreach ($empl_data as $ekey => $evalue) {
			$trans_data = array();
			$proj_data = $this->db->query("SELECT `date`,`act_intime`,`act_outtime`,`working_time`,`early_time`,`late_time`,`firsthalf_status`,`secondhalf_status`,`project_id` FROM `oc_transaction` WHERE `date` >= '".(date('Y-m-d', strtotime($filter_date_start)))."' AND `date` <='".(date('Y-m-d', strtotime($filter_date_end)))."' AND emp_id = '".$evalue['emp_id']."' ORDER BY `date` ")->rows;
			foreach ($proj_data as $pkey => $pvalue) {
				$pro_names = $this->db->query("SELECT * FROM `oc_project` WHERE `id` = '".$pvalue['project_id']."' ");
				$pro_name = '';
				if($pro_names->num_rows > 0){
					$pro_name = $pro_names->row['name'];
				}
				$firsthalf_status = 0;
				$secondhalf_status = 0;
				$status = 0;

				if($pvalue['firsthalf_status'] == '1'){
					$firsthalf_status ='P';
				}elseif ($pvalue['firsthalf_status'] == '0') {
					$firsthalf_status ='A';
				}else{
					$firsthalf_status = $pvalue['firsthalf_status'];
				}
				if($pvalue['secondhalf_status'] == '1'){
					$secondhalf_status ='P';
				}elseif ($pvalue['secondhalf_status'] == '0') {
					$secondhalf_status ='A';
				}else{
					$secondhalf_status = $pvalue['secondhalf_status'];
				}
				$status = $firsthalf_status.' / '.$secondhalf_status;

				$trans_data[] = array(
					'project_name' 	=>$pro_name,
					'date'			=>date('d-m-Y', strtotime($pvalue['date'])),
					'in'			=>$pvalue['act_intime'],
					'out'			=>$pvalue['act_outtime'],
					'working_time'	=>$pvalue['working_time'],
					'early_time'	=>$pvalue['early_time'],
					'late_time'		=>$pvalue['late_time'],
					'status'		=>$status
				);
			}
			if($trans_data){	
				$final_datas[] = array(
					'emp_name'  =>$evalue['emp_name'],
					'trans_data'  =>$trans_data
				);
			}

		}

		$this->data['final_datas'] = $final_datas;
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->data['warning'])){
			$this->data['error_warning'] = $this->data['warning'];
		} elseif(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
		

			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			$template->data['filter_date_start'] = $filter_date_start;
			$template->data['filter_date_end'] = $filter_date_end;
			//$template->data['filter_department'] = $department;
			//$template->data['filter_unit'] = $unit;
			$template->data['filter_status'] = $filter_status;
			$template->data['title'] = 'Attendance Report';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/attendance_html.tpl');
			//echo $html;exit;
			$filename = 'Attendance Report';
			// header('Content-type: text/html');
			// header('Content-Disposition: attachment; filename='.$filename);
			// echo $html;
			//exit;
			// $filename = $statusss."_".$filter_date_start;
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;		
		// } else {
		// 	$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/attendance', 'token=' . $this->session->data['token'].$url, 'SSL'));
		//}
	}

	public function remove_transaction(){
		$this->language->load('report/attendance');
		$this->load->model('report/common_report');
		$this->load->model('report/attendance');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = html_entity_decode($this->request->get['filter_unit']);
		} else {
			$filter_unit = '';
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = html_entity_decode($this->request->get['filter_department']);
		} else {
			$filter_department = 0;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = 1;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['transaction_id'])) {
			$transaction_id = $this->request->get['transaction_id'];
		} else {
			$transaction_id = 0;
		}

		$sql = "DELETE FROM `oc_transaction` WHERE `transaction_id` = '".$transaction_id."'";
		//echo $sql;exit;
		$this->db->query($sql);

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->session->data['success'] = 'Transaction Removed Succesfully';
		$this->redirect($this->url->link('report/attendance', 'token=' . $this->session->data['token'].$url, 'SSL'));
	}
	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			if(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
				$filter_reporting_to = $this->session->data['emp_code'];
			} else {
				$filter_reporting_to = '';
			}

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				//'filter_date_start' => $this->request->get['filter_date_start'],
				//'filter_dol' => 1,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['first_name'].' '.$result['last_name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>