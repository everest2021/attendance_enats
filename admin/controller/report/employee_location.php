<?php
class ControllerReportEmployeelocation extends Controller { 
	public function index() { 

		$this->language->load('report/employee_location');
		$this->load->model('catalog/employee');
		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$filter_name_id = $this->session->data['emp_code'];
		} else {
			$filter_name_id = '';
		}
		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = date('d-m-Y');
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = date('d-m-Y');
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/employee_location', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);
		//$this->data['generate_today'] = HTTP_CATALOG.'service/dataprocess.php';//$this->url->link('transaction/transaction/generate_today', 'token=' . $this->session->data['token'] . $url, 'SSL');
		

		$this->load->model('catalog/expense_transaction');
		$this->load->model('catalog/employee');
		$this->data['expense_ledger'] = array();
		$finaldata = array();
		$data = array(
			'filter_name_id'	=>$filter_name_id,
			'filter_name'		=>$filter_name,
			'filter_date_start'	=>$filter_date_start,
			'filter_date_end'	=>$filter_date_end,
			'start'             => ($page - 1) * 7000,
			'limit'             => 7000
		);
		if(isset($this->request->get['once'])) {
			$sql = "SELECT *  FROM `oc_employee` WHERE 1=1 ";
			if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
					$sql .= " AND `emp_code` = '" . $data['filter_name_id'] . "' ";
			}
			$emp_names = $this->db->query($sql)->rows;
			foreach($emp_names as $rvalue){
				$tran_data = array();
				$sql = "SELECT * FROM `oc_transaction_punches` WHERE `emp_id` = '".$rvalue['emp_code']."' ";
				if (!empty($data['filter_date_start'])) {
					$sql .= " AND DATE(`date`) >= '" . $this->db->escape(date('Y-m-d', strtotime($filter_date_start))) . "'";
				}
				if (!empty($data['filter_date_end'])) {
					$sql .= " AND DATE(`date`) <= '" . $this->db->escape(date('Y-m-d', strtotime($filter_date_end))) . "'";
				}
				$sql .= " GROUP BY `date` ";
				$punch_data = $this->db->query($sql)->rows;
				foreach($punch_data as $dvalue){
					$link = HTTP_CATALOG.'map_page.php?emp_id='.$dvalue['emp_id'].'&date='.$dvalue['date'];//$this->url->link('report/employee_location/map_fun', 'token=' . $this->session->data['token'] . $url . '&emp_id='.$dvalue['emp_id'] . '&date='.$dvalue['date'], 'SSL');
					$tran_data[] = array(
						'date' 			=>date('d-m-Y', strtotime($dvalue['date'])),
						'date' 			=>date('d-m-Y', strtotime($dvalue['date'])),
						'link' => $link,
					);
				}
				
				$per_sort_data = $this->array_sort($tran_data, 'date', SORT_ASC);
				if($tran_data){
					$finaldata[] = array(
						'name'		 => $rvalue['name'],
						'tran_data'  =>$per_sort_data
					);
				}
			}
		}
		// echo '<pre>';
		// print_r($finaldata);
		// exit();
		$this->data['finaldata'] = $finaldata;

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		//$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		
		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		

		$this->template = 'report/employee_location.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}


	public function map_fun(){
		$this->language->load('report/employee_location');
		$this->load->model('catalog/employee');
		$this->document->setTitle($this->language->get('heading_title'));

		if(isset($this->request->get['emp_id'])){
			$emp_id = $this->request->get['emp_id'];
		} else {
			$emp_id = '';
		}

		if(isset($this->request->get['date'])){
			$date = $this->request->get['date'];
		} else {
			$date = '';
		}

		$tran_datas_punches = $this->db->query("SELECT * FROM `oc_transaction_punches` WHERE `emp_id` = '".$emp_id."' AND `date` = '".$date."' ");
		$tran_datas_punch = array();
		foreach($tran_datas_punches->rows as $tkey => $tvalue){
			$tran_datas_punch[] = array(
				'date_time' => date('d-m-Y', strtotime($tvalue['date'])).' '.date('h:i:s A', strtotime($tvalue['time'])),
				'lat' => $tvalue['lat'],
				'lon' => $tvalue['lon'],
			);
		}
		// echo'<pre>';
		// print_r($tran_datas_punch);
		// exit;
		$this->data['tran_datas_punch'] = $tran_datas_punch;

		$this->template = 'report/employee_location_map.tpl';

		$this->response->setOutput($this->render());
	}

	public function array_sort($array, $on, $order=SORT_ASC){

	    $new_array = array();
	    $sortable_array = array();

	    if (count($array) > 0) {
	        foreach ($array as $k => $v) {
	            if (is_array($v)) {
	                foreach ($v as $k2 => $v2) {
	                    if ($k2 == $on) {
	                        $sortable_array[$k] = $v2;
	                    }
	                }
	            } else {
	                $sortable_array[$k] = $v;
	            }
	        }

	        switch ($order) {
	            case SORT_ASC:
	                asort($sortable_array);
	                break;
	            case SORT_DESC:
	                arsort($sortable_array);
	                break;
	        }

	        foreach ($sortable_array as $k => $v) {
	            $new_array[$k] = $array[$k];
	        }
	    }

	    return $new_array;
	}

	public function export(){

		$this->language->load('report/employee_location');
		$this->load->model('catalog/employee');
		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}


		// if (isset($this->request->get['filter_date_start'])) {
		// 	$filter_date_start = $this->request->get['filter_date_start'];
		// } else {
		// 	$year = date('Y');
		// 	$month = date('n');
		// 	$day = date('j');
		// 	if($month == 1){
		// 		if($day >= 26 && $day <= 31){
		// 			$prev_month = $month;
		// 			$prev_year = $year;
		// 		} else {
		// 			$prev_month = 12;
		// 			$prev_year = $year - 1;
		// 		}
		// 	} else {
		// 		if($day >= 26 && $day <= 31){
		// 			$prev_month = $month;
		// 			$prev_year = $year;
		// 		} else {
		// 			$prev_month = $month - 1;
		// 			$prev_year = $year;
		// 		}
		// 	}
		// 	$filter_date_start = sprintf("%04d-%02d-%02d", $prev_year, $prev_month, '26');
		// }

		// if (isset($this->request->get['filter_date_end'])) {
		// 	$filter_date_end = $this->request->get['filter_date_end'];
		// } else {
		// 	$year = date('Y');
		// 	$month = date('n');
		// 	$day = date('j');
		// 	if($month == 12){
		// 		if($day >= 26 && $day <= 31){
		// 			$next_month = 1;
		// 			$next_year = $year + 1;
		// 		} else {
		// 			$next_month = 12;
		// 			$next_year = $year;
		// 		}
		// 	} else {
		// 		if($day >= 26 && $day <= 31){
		// 			$next_month = $month + 1;
		// 			$next_year = $year;
		// 		} else {
		// 			$next_month = $month;
		// 			$next_year = $year;
		// 		}
		// 	}
		// 	$filter_date_end = sprintf("%04d-%02d-%02d", $next_year, $next_month, '25');
			
		// }
		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$filter_name_id = $this->session->data['emp_code'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/employee_location', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);
		//$this->data['generate_today'] = HTTP_CATALOG.'service/dataprocess.php';//$this->url->link('transaction/transaction/generate_today', 'token=' . $this->session->data['token'] . $url, 'SSL');
		

		$this->load->model('catalog/expense_transaction');
		$this->load->model('catalog/employee');
		$this->data['expense_ledger'] = array();
		$finaldata = array();
		$data = array(
			'filter_name_id'	=>$filter_name_id,
			'filter_name'		=>$filter_name,
			'filter_date_start'	=>$filter_date_start,
			'filter_date_end'	=>$filter_date_end,
			'start'             => ($page - 1) * 7000,
			'limit'             => 7000
		);
		if(isset($this->request->get['once'])) {
			$sql = "SELECT *  FROM `oc_employee` WHERE 1=1 ";
			if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
					$sql .= " AND `emp_code` = '" . $data['filter_name_id'] . "' ";
			}
			$emp_names = $this->db->query($sql)->rows;

			foreach($emp_names as $rvalue){
				$tran_data = array();

				
				$sql = "SELECT * FROM `oc_transaction_punches` WHERE `emp_id` = '".$rvalue['emp_code']."' ";
				if (!empty($data['filter_date_start'])) {
					$sql .= " AND DATE(`date`) >= '" . $this->db->escape(date('Y-m-d', strtotime($filter_date_start))) . "'";
				}
				if (!empty($data['filter_date_end'])) {
					$sql .= " AND DATE(`date`) <= '" . $this->db->escape(date('Y-m-d', strtotime($filter_date_end))) . "'";
				}
				$punch_data = $this->db->query($sql)->rows;
				foreach($punch_data as $dvalue){
					$tran_data[] = array(
						'date' 			=>date('d-m-Y', strtotime($dvalue['date'])),
						'location' => 'http://maps.google.com/?q=@'.$dvalue['lat'] .','.$dvalue['lon'],
					);
				}
				
				$per_sort_data = $this->array_sort($tran_data, 'date', SORT_ASC);
				if($tran_data){
					$finaldata[] = array(
						'name'		 => $rvalue['name'],
						'tran_data'  =>$per_sort_data
					);
				}
			}
		}
		// echo '<pre>';
		// print_r($finaldata);
		// exit();
		$this->data['finaldata'] = $finaldata;

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		
		$url .= '&once=1';

		//$final_data = array_chunk($final_data, 7000);

		// echo '<pre>';
		// print_r($finaldata);
		// exit;
		
		// if($final_datas){
		// 	if($filter_department){
		// 		$department_names = $this->db->query("SELECT `d_name` FROM `oc_department` WHERE `department_id` = '".$this->request->get['filter_department']."' ");
		// 		if($department_names->num_rows > 0){
		// 			$department_name = $department_names->row['d_name'];
		// 		} else {
		// 			$department_name = '';
		// 		}
		// 		$department_name = html_entity_decode($department_name);
		// 		$department = $department_name;
		// 	} else {
		// 		$department = 'All';
		// 	}

		// 	if($filter_unit){
		// 		$unit_names = $this->db->query("SELECT `unit` FROM `oc_unit` WHERE `unit_id` = '".$this->request->get['filter_unit']."' ");
		// 		if($unit_names->num_rows > 0){
		// 			$unit_name = $unit_names->row['unit'];
		// 		} else {
		// 			$unit_name = '';
		// 		}
		// 		$unit_name = html_entity_decode($unit_name);
		// 		$unit = $unit_name;
		// 	} else {
		// 		$unit = 'All';
		// 	}

			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$template = new Template();		
			$template->data['finaldata'] = $finaldata;
			$template->data['date_start'] = $filter_date_start;
			$template->data['date_end'] = $filter_date_end;
			$template->data['title'] = 'Employee Report';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/expense_ledger_html.tpl');
			//echo $html;exit;
			$filename = "Employee_Report";
			
			//if($filter_type == 6){
				// header('Content-type: text/html');
				// header('Content-Disposition: attachment; filename='.$filename.".html");
				// echo $html;
			// } else {
				header("Content-Type: application/vnd.ms-excel; charset=utf-8");
				header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Cache-Control: private",false);
				echo $html;
				exit;
			// }		
		// } else {
		// 	$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/employee_location', 'token=' . $this->session->data['token'].$url, 'SSL'));
		//}
	}


	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			// if(isset($this->session->data['dept_names'])){
			// 	$filter_departments = html_entity_decode($this->session->data['dept_names']);
			// 	$filter_departments = "'" . str_replace(",", "','", html_entity_decode($filter_departments)) . "'";
			// } else {
			// 	$filter_departments = '';
			// }

			// if($filter_departments == ''){
			// 	if(isset($this->session->data['dept_name'])){
			// 		$filter_department = $this->session->data['dept_name'];
			// 	} else {
			// 		$filter_department = '';
			// 	}
			// } else {
			// 	$filter_department = '';
			// }

			if(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
				$filter_reporting_to = $this->session->data['emp_code'];
			} else {
				$filter_reporting_to = '';
			}

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_date_start' => $this->request->get['filter_date_start'],
				'filter_dol' => 1,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['first_name'].' '.$result['last_name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>

