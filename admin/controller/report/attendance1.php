<?php
class ControllerReportAttendance1 extends Controller { 
	public function index() {  
		date_default_timezone_set("Asia/Kolkata");
		// if(isset($this->session->data['emp_code'])){
		// 	$emp_code = $this->session->data['emp_code'];
		// 	$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
		// 	if($is_set == '1'){
		// 		//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
		// 	} else {
		// 		$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
		// 	}
		// } elseif(isset($this->session->data['is_dept'])){
		// 	$emp_code = $this->session->data['d_emp_id'];
		// 	$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
		// 	if($is_set == '1'){
		// 		//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
		// 	} else {
		// 		$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
		// 	}
		// } elseif(isset($this->session->data['is_super'])){
		// 	$emp_code = $this->session->data['d_emp_id'];
		// 	$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
		// 	if($is_set == '1'){
		// 		//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
		// 	} else {
		// 		$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
		// 	}
		// }

		$this->language->load('report/attendance1');
		$this->load->model('report/common_report');
		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			//$filter_date_start = '';
			$year = date('Y');
			$month = date('n');
			$day = date('j');
			if($month == 1){
				if($day >= 26 && $day <= 31){
					$prev_month = $month;
					$prev_year = $year;
				} else {
					$prev_month = 12;
					$prev_year = $year - 1;
				}
			} else {
				if($day >= 26 && $day <= 31){
					$prev_month = $month;
					$prev_year = $year;
				} else {
					$prev_month = $month - 1;
					$prev_year = $year;
				}
			}
			$filter_date_start = sprintf("%04d-%02d-%02d", $prev_year, $prev_month, '26');
			//$from = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime($from . "-30 day"));
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$filter_name_id = $this->session->data['emp_code'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = $this->request->get['filter_unit'];
		} else {
			$filter_unit = 0;
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
			// $filter_date_end1 = $this->model_report_common_report->getfilter_date_end($filter_date_start, $filter_name_id, $unit);
			// if(strtotime($filter_date_end) > strtotime($filter_date_end1) ){
			// 	$filter_date_end = $filter_date_end1;
			// }
		} else {
			$year = date('Y');
			$month = date('n');
			$day = date('j');
			if($month == 12){
				if($day >= 26 && $day <= 31){
					$next_month = 1;
					$next_year = $year + 1;
				} else {
					$next_month = 12;
					$next_year = $year;
				}
			} else {
				if($day >= 26 && $day <= 31){
					$next_month = $month + 1;
					$next_year = $year;
				} else {
					$next_month = $month;
					$next_year = $year;
				}
			}
			$filter_date_end = sprintf("%04d-%02d-%02d", $next_year, $next_month, '25');
			$to = date('Y-m-d');
			$filter_date_end = date('Y-m-d', strtotime($to . "-1 day"));
		}
		
		if(isset($this->session->data['dept_names'])){
			$filter_departments = html_entity_decode($this->session->data['dept_names']);
			$filter_departments = "'" . str_replace(",", "','", html_entity_decode($filter_departments)) . "'";
		} else {
			$filter_departments = '';
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = html_entity_decode($this->request->get['filter_department']);
		} elseif(isset($this->session->data['dept_name']) && !isset($this->session->data['dept_names'])){
			$filter_department = $this->session->data['dept_name'];
		} else {
			$filter_department = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/attendance1', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/attendance');
		$this->load->model('transaction/transaction');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'filter_unit'			 => $filter_unit,
			'filter_department'		 => $filter_department,
			'filter_departments'	 => $filter_departments,
			'day_close'              => 1,
			'filter_dol'             => 1,
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		//$start_time = strtotime($filter_date_start);
		//$compare_time = strtotime(date('Y-m-d'));

		$day = array();
		$days = $this->GetDays($filter_date_start, $filter_date_end);
        foreach ($days as $dkey => $dvalue) {
        	$dates = explode('-', $dvalue);
        	$day[$dkey]['day'] = $dates[2];
        	$day[$dkey]['date'] = $dvalue;
        }
        $this->data['days'] = $day;



        //foreach ($day as $dkey => $dvalue) {
        	//$results = $this->model_report_common_report->getAttendance($data);
        //}
        $final_datas = array();
        if(isset($this->request->get['once']) && $this->request->get['once'] == 1){
	       	$current_date_exp = explode('-', $filter_date_start);
	        $month = $current_date_exp[1];
			$year = $current_date_exp[0];
			$total_month_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);

	        $results1 = $this->model_report_common_report->getemployees($data);
	        $results2 = $this->model_report_common_report->getemployees_reporting($data);
	        $results = array_merge($results1, $results2);
	        foreach ($results as $rkey => $rvalue) {
				$attendance1_data = array();
				$data['filter_date_start'] = $filter_date_start;
				$data['filter_date_end'] = $filter_date_end;
				if($rvalue['doj'] != '0000-00-00' && strtotime($rvalue['doj']) > strtotime($data['filter_date_start'])){
					$data['filter_date_start'] = $rvalue['doj'];
				}
				if($rvalue['dol'] != '0000-00-00' && strtotime($rvalue['dol']) < strtotime($data['filter_date_end'])){
					$dol = date("Y-m-d", strtotime("-1 day", strtotime($rvalue['dol'])));
					$data['filter_date_end'] = $dol;
				}
				//echo $data['filter_date_end'];exit;

				$total_month_dayss = $this->GetDays($data['filter_date_start'], $data['filter_date_end']);
				$total_month_days = 0;
				foreach($total_month_dayss as $tkeys => $tvalue){
					$total_month_days ++;
				}
				$transaction_datas = $this->model_report_common_report->gettransaction_data($rvalue['emp_code'], $data);
				$emp_names = $this->db->query("SELECT CONCAT_WS(' ',first_name,last_name) AS emp_name FROM `oc_employee` WHERE `emp_code` = '".$rvalue['emp_code']."' ");
				$emp_name = '';
				if($emp_names->num_rows > 0){
					$emp_name = $emp_names->row['emp_name'];
				}

				$final_datas[$rvalue['emp_code']]['name'] = $emp_name;
				$final_datas[$rvalue['emp_code']]['emp_code'] = $rvalue['emp_code'];
				$transaction_inn = '0';
				$working_hours_array = array();
				$actual_working_hours_array = array();
				$late_early_count = 0;
				$total_absent_count = 0;
				$total_absent_raw_count = 0;
				foreach($transaction_datas as $tkey => $tvalue){
					if($tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0' && $tvalue['working_time'] != '00:00:00'){
						if($tvalue['after_shift'] == 1){
							$tvalue['act_outtime'] = $tvalue['shift_outtime_flexi'];
						}
						$shift_intime_minus_one = Date('H:i:s', strtotime($tvalue['shift_intime'] .' -0 minutes'));
						$start_date = new DateTime($tvalue['date'].' '.$shift_intime_minus_one);
						$since_start = $start_date->diff(new DateTime($tvalue['date'].' '.$tvalue['act_intime']));
						if($since_start->h > 12){
							$start_date = new DateTime($tvalue['date'].' '.$shift_intime_minus_one);
							$since_start = $start_date->diff(new DateTime($tvalue['date_out'].' '.$tvalue['act_intime']));
						}
						$shift_early = 0;
						if($since_start->invert == 1){
							$shift_early = 1;
						}
						if($shift_early == 1){
							$shift_intime_minus_one = Date('H:i:s', strtotime($tvalue['shift_intime'] .' -0 minutes'));
							if(strtotime($tvalue['act_outtime']) < strtotime($shift_intime_minus_one)){
								$start_date = new DateTime($tvalue['date'].' '.$tvalue['act_intime']);
							} else {
								$start_date = new DateTime($tvalue['date'].' '.$shift_intime_minus_one);
							}
						} else {
							$start_date = new DateTime($tvalue['date'].' '.$tvalue['act_intime']);
						}
						$since_start = $start_date->diff(new DateTime($tvalue['date'].' '.$tvalue['act_outtime']));
						$working_hour = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
						$tvalue['working_time'] = $working_hour;
						$working_hours_array[$tvalue['date']] = $tvalue['working_time'];				
					} else {
						// if($tvalue['date'] == date('Y-m-d') && $tvalue['act_intime'] != '00:00:00'){
						// 	$current_time = date('H:i:s');
						// 	$start_date = new DateTime($tvalue['date'].' '.$tvalue['act_intime']);
						// 	$since_start = $start_date->diff(new DateTime($tvalue['date_out'].' '.$current_time));
						// 	$working_hours_array[$tvalue['date']] = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);	
						// }
					}
					//if(($tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0') || ($tvalue['weekly_off'] != '0' && $tvalue['working_time'] != '00:00:00') || ($tvalue['holiday_id'] != '0' && $tvalue['working_time'] != '00:00:00') || ($tvalue['leave_status'] != '0' && $tvalue['working_time'] != '00:00:00') ){
					$current_date = date('Y-m-d');
					if($tvalue['date'] < $current_date && $tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0' && $tvalue['act_intime'] <> '00:00:00' && $tvalue['act_outtime'] <> '00:00:00'){
						$start_date = new DateTime($tvalue['date'].' '.$tvalue['shift_intime']);
						$since_start = $start_date->diff(new DateTime($tvalue['date_out'].' '.$tvalue['shift_outtime']));
						$shift_working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
						$actual_working_hours_array[$tvalue['date']] = $shift_working_time;	
					} else {
						if($tvalue['date'] >= $current_date && $tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0' && $tvalue['act_intime'] = '00:00:00' && $tvalue['act_outtime'] = '00:00:00'){
							$start_date = new DateTime($tvalue['date'].' '.$tvalue['shift_intime']);
							$since_start = $start_date->diff(new DateTime($tvalue['date_out'].' '.$tvalue['shift_outtime']));
							$shift_working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
							$actual_working_hours_array[$tvalue['date']] = $shift_working_time;	
						}
					}

					if($tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0'){
						$late_early_count = $late_early_count + $tvalue['late_mark'];
						$late_early_count = $late_early_count + $tvalue['early_mark'];
					}

					if($tvalue['date'] < $current_date){
						if($tvalue['absent_status'] == 1 && $tvalue['working_time'] == '00:00:00' && $tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0'){
							$total_absent_count = $total_absent_count + 1;
							$total_absent_raw_count = $total_absent_raw_count + 1;
						} elseif($tvalue['absent_status'] == 0.5){
							//$total_absent_count = $total_absent_count + 0.5;
						}
					}
				}
				//echo $total_absent_count;exit;

				$time = 0;
				$hours = 0;
				$minutes = 0;
				foreach ($working_hours_array as $time_val) {
					$times = explode(':', $time_val);
					$hours += $times[0];
					$minutes += $times[1];
				}
				$min_min = 0;
				$min_hours = 0;
				if($minutes > 0){
					$min_hours = floor($minutes / 60);
	    			$min_min = ($minutes % 60);
	    			$min_min = sprintf('%02d', $min_min);
	    		}
	    		$total_working_hours = sprintf('%02d', ($hours + $min_hours)).'.'.sprintf('%02d', $min_min);
				$time = 0;
				$hours = 0;
				$minutes = 0;
				foreach ($actual_working_hours_array as $time_val) {
					$times = explode(':', $time_val);
					$hours += $times[0];
					$minutes += $times[1];
				}
				$min_min = 0;
				$min_hours = 0;
				if($minutes > 0){
					$min_hours = floor($minutes / 60);
	    			$min_min = ($minutes % 60);
	    			$min_min = sprintf('%02d', $min_min);
	    		}
	    		$total_actual_working_hours = sprintf('%02d', ($hours + $min_hours)).'.'.sprintf('%02d', $min_min);
				if($total_actual_working_hours > $total_working_hours){
					$extra_hours = 0;
					//$difference_hours = $total_actual_working_hours - $total_working_hours;
					$total_actual_working_hours_min = $this->hoursToMinutes($total_actual_working_hours);
					$total_working_hours_min = $this->hoursToMinutes($total_working_hours);
					$difference_minutes = $total_actual_working_hours_min - $total_working_hours_min;
					$min_hours = floor($difference_minutes / 60);
					$min_min = ($difference_minutes % 60);
					$difference_hours = $min_hours.'.'.$min_min; 
				} else {
					$difference_hours = 0;
					//$extra_hours = $total_working_hours - $total_actual_working_hours;
					$total_actual_working_hours_min = $this->hoursToMinutes($total_actual_working_hours);
					$total_working_hours_min = $this->hoursToMinutes($total_working_hours);
					$difference_minutes = $total_working_hours_min - $total_actual_working_hours_min;
					$min_hours = floor($difference_minutes / 60);
					$min_min = ($difference_minutes % 60);
					$extra_hours = $min_hours.'.'.$min_min; 
				}
				$total_bal = $this->model_transaction_transaction->gettotal_bal_ess($rvalue['emp_code']);
				$total_previous_bal = $this->model_transaction_transaction->getprevious_balances($rvalue['emp_code'], $filter_date_start, $filter_date_end, $rvalue, 'PL');
				//echo $total_previous_bal;exit;
				$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro_ess_date($rvalue['emp_code'], 'PL', $filter_date_start, $filter_date_end);
				$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1_ess_date($rvalue['emp_code'], 'PL', $filter_date_start, $filter_date_end);
				//$difference_hours = '112.47';
				$deduction_days_rounded = floor($difference_hours / 9.00);
				$deduction_days = round($difference_hours / 9.00, 2);
				$deduction_days = sprintf("%.2f", $deduction_days);
				$deduction_days_exp = explode('.', $deduction_days);
				if(isset($deduction_days_exp[1])){
					if($deduction_days_exp[1] == '00' || $deduction_days_exp[1] == 00){
						$deduction_days_exp[1] = 0;
					}
					if($deduction_days_exp[1] > 0 && $deduction_days_exp[1] <= 50){
						$deduction_days_rounded = $deduction_days_rounded + 0.5; 
					} else {
						if($deduction_days_exp[1] != 0){
							$deduction_days_rounded = $deduction_days_rounded + 1;
						}
					}
				}
				// echo $difference_hours;
				// echo '<br />';
				// echo $deduction_days;
				// echo '<br />';
				// echo '<pre>';
				// print_r($deduction_days_exp);
				// echo '<br />';
				// echo $deduction_days_rounded;
				// echo '<br />';
				// exit;
				//$late_early_count = 9;
				$late_early_days_rounded = floor($late_early_count / 6);
				$late_early_days = round($late_early_count / 6, 2);
				$late_early_days = sprintf("%.2f", $late_early_days);
				$late_early_days_exp = explode('.', $late_early_days);
				if(isset($late_early_days_exp[1])){
					if($late_early_days_exp[1] == '00' || $late_early_days_exp[1] == 00){
						$late_early_days_exp[1] = 0;
					}
					if($late_early_days_exp[1] >= 50){
						$late_early_days_rounded = $late_early_days_rounded + 0.5; 
					}
				}
				// echo $late_early_count;
				// echo '<br />';
				// echo $late_early_days;
				// echo '<br />';
				// echo '<pre>';
				// print_r($late_early_days_exp);
				// echo '<br />';
				// echo $late_early_days_rounded;
				// echo '<br />';
				// exit;
				$leave_minus_amount = 0;
				$total_leaves_count = 0;
				$total_leave_opening = 0;
				$total_leave_balance = 0;
				$total_absent_days = 0;
				if(isset($total_bal['pl_acc'])){
					if(!isset($total_bal_pro['bal_p'])){
						$total_bal_pro['bal_p'] = 0;
					}
					if(!isset($total_bal_pro['encash'])){
						$total_bal_pro['encash'] = 0;
					}
					if(!isset($total_bal_pro1['leave_amount'])){
						$total_bal_pro1['leave_amount'] = 0;
					}	
					$total_leave_opening = $total_previous_bal;
					$total_leave_balance = $total_leave_opening - ($total_bal_pro['bal_p'] + $total_bal_pro['encash'] + $total_bal_pro1['leave_amount']) - ($late_early_days_rounded + $deduction_days_rounded);
					if($total_leave_balance < 0){
						$total_absent_days += abs($total_leave_balance);
						$total_leave_balance = 0;
					}
					$total_leaves_count = $total_bal_pro['bal_p'] + $total_bal_pro['encash'] + $total_bal_pro1['leave_amount'];
				}
				$total_absent_days += $total_absent_count;


				//echo "SELECT `punch_time` FROM `oc_attendance_new` WHERE `emp_id` = '".$result['emp_code']."' AND `punch_date` = '".date('Y-m-d')."' ";exit;
				$todays_intime_datas = $this->db->query("SELECT `punch_time` FROM `oc_attendance_new` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `punch_date` = '".date('Y-m-d')."' ");
				$todays_in_time = '00:00:00';
				$todays_working_hours = '00:00:00';
				if($todays_intime_datas->num_rows > 0){
					$todays_intime_data = $todays_intime_datas->row;
					$todays_in_time = $todays_intime_data['punch_time'];
					$todays_out_time = date('H:i:s');
					$current_date = date('Y-m-d');
					$start_date = new DateTime($current_date.' '.$todays_in_time);
					$since_start = $start_date->diff(new DateTime($current_date.' '.$todays_out_time));
					$todays_working_hours = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
				}

				$total_utitilized_plus_absent = $total_absent_raw_count + $deduction_days_rounded + $late_early_days_rounded + $total_leaves_count;

				// echo $total_actual_working_hours;
				// echo '<br />';
				// echo $total_working_hours;
				// echo '<br />';
				// echo $difference_hours;
				// echo '<br />';
				// echo $extra_hours;
				// echo '<br />';
				// echo $total_bal['pl_acc'];
				// echo '<br />';
				// echo $total_bal_pro['bal_p'];
				// echo '<br />';
				// echo $total_bal_pro['encash'];
				// echo '<br />';
				// echo $total_bal_pro1['leave_amount'];
				// echo '<br />';
				// echo $late_early_days_rounded;
				// echo '<br />';
				// echo $deduction_days_rounded;
				// echo '<br />';
				// echo $total_bal_pl;
				// echo '<br />';
				// echo $todays_in_time;
				// echo '<br />';
				// echo $todays_working_hours;
				// echo '<br />';
				// exit;
				// echo $total_month_days;
				// echo '<br />';
				// echo $total_absent_days;
				// echo '<br />';
				// exit;
				$final_datas[$rvalue['emp_code']]['total_working_hours'] = $total_actual_working_hours;
				$final_datas[$rvalue['emp_code']]['total_actual_working_hours'] = $total_working_hours;
				$final_datas[$rvalue['emp_code']]['total_pending_hours'] = $difference_hours;
				$final_datas[$rvalue['emp_code']]['total_extra_hours'] = $extra_hours;
				$final_datas[$rvalue['emp_code']]['total_deduction_days'] = $deduction_days_rounded;
				$final_datas[$rvalue['emp_code']]['total_late_early_count'] = $late_early_count;
				$final_datas[$rvalue['emp_code']]['total_late_early_days'] = $late_early_days_rounded;
				$final_datas[$rvalue['emp_code']]['total_leaves_count'] = $total_leaves_count;
				$final_datas[$rvalue['emp_code']]['total_deduction'] = $deduction_days_rounded + $late_early_days_rounded + $total_leaves_count;
				$final_datas[$rvalue['emp_code']]['total_absent_raw_count'] = $total_absent_raw_count;
				$final_datas[$rvalue['emp_code']]['total_utitilized_plus_absent'] = $total_utitilized_plus_absent;
				$final_datas[$rvalue['emp_code']]['total_leave_opening'] = $total_leave_opening;
				$final_datas[$rvalue['emp_code']]['total_leave_balance'] = $total_leave_balance;
				$final_datas[$rvalue['emp_code']]['total_absent_days'] = $total_absent_days;
				$final_datas[$rvalue['emp_code']]['total_month_days'] = $total_month_days - $total_absent_days;
			}
		}

		$this->data['final_datas'] = $final_datas;
		// echo '<pre>';
  // 		print_r($this->data['final_datas']);
  // 		exit;

		//$results = $this->model_report_common_report->getAttendance($data);
		
		$type_data = array(
			'1' => 'Default',
			//'2' => 'Late',
			'3' => 'Loss',
			'4' => 'Absent',
			'5' => 'Excess',
			'6' => 'Manual'
		);

		$this->data['type_data'] = $type_data;

		$locations = $this->db->query("SELECT * FROM `oc_unit` ")->rows;
		$unit_data['0'] = 'All'; 
		foreach($locations as $lkey => $lvalue){
			$unit_data[$lvalue['unit_id']] = $lvalue['unit'];
		}
		$this->data['unit_data'] = $unit_data;
		
		$department_datas = $this->db->query("SELECT * FROM `oc_department` ")->rows;
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['d_name'];
		}
		$this->data['department_data'] = $department_data;
		
		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}

		if(isset($this->session->data['is_dept']) && $this->session->data['is_dept'] == '1'){
			$user_dept = 1;
			$is_dept = 1;
		} elseif(isset($this->session->data['is_user'])){
			$user_dept = 1;
			$is_dept = 0;
		} else {
			$user_dept = 0;
			$is_dept = 0;
		}
		$this->data['user_dept'] = $user_dept;
		$this->data['is_dept'] = $is_dept;

		$this->data['reporting_head'] = 0;
		if(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
			$is_reporting_datas = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE `reporting_to` = '".$this->session->data['emp_code']."' ");
			if($is_reporting_datas->num_rows > 0){
				$this->data['reporting_head'] = 1;
			} else {
				$this->data['reporting_head'] = 0;
			}
		}

		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_unit'] = $filter_unit;
		$this->data['filter_department'] = $filter_department;

		$this->template = 'report/attendance1.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function hoursToMinutes($hours) {
		if (strstr($hours, '.')){
			# Split hours and minutes.
			$separatedData = explode('.', $hours);
			$minutesInHours    = $separatedData[0] * 60;
			$minutesInDecimals = $separatedData[1];
			$totalMinutes = $minutesInHours + $minutesInDecimals;
		} else {
			$totalMinutes = $hours * 60;
		}
		//echo $totalMinutes;exit;
		return $totalMinutes;
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	function array_sort($array, $on, $order=SORT_ASC){

	    $new_array = array();
	    $sortable_array = array();

	    if (count($array) > 0) {
	        foreach ($array as $k => $v) {
	            if (is_array($v)) {
	                foreach ($v as $k2 => $v2) {
	                    if ($k2 == $on) {
	                        $sortable_array[$k] = $v2;
	                    }
	                }
	            } else {
	                $sortable_array[$k] = $v;
	            }
	        }

	        switch ($order) {
	            case SORT_ASC:
	                asort($sortable_array);
	                break;
	            case SORT_DESC:
	                arsort($sortable_array);
	                break;
	        }

	        foreach ($sortable_array as $k => $v) {
	            $new_array[$k] = $array[$k];
	        }
	    }

	    return $new_array;
	}

	public function export(){
		$this->language->load('report/attendance1');
		$this->load->model('report/common_report');
		$this->load->model('transaction/transaction');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$year = date('Y');
			$month = date('n');
			$day = date('j');
			if($month == 1){
				if($day >= 26 && $day <= 31){
					$prev_month = $month;
					$prev_year = $year;
				} else {
					$prev_month = 12;
					$prev_year = $year - 1;
				}
			} else {
				if($day >= 26 && $day <= 31){
					$prev_month = $month;
					$prev_year = $year;
				} else {
					$prev_month = $month - 1;
					$prev_year = $year;
				}
			}
			$filter_date_start = sprintf("%04d-%02d-%02d", $prev_year, $prev_month, '26');
			//$filter_date_start = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			//$from = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
			// $filter_date_end1 = $this->model_report_common_report->getfilter_date_end($filter_date_start);
			// if(strtotime($filter_date_end) > strtotime($filter_date_end1) ){
			// 	$filter_date_end = $filter_date_end1;
			// }
		} else {
			$year = date('Y');
			$month = date('n');
			$day = date('j');
			if($month == 12){
				if($day >= 26 && $day <= 31){
					$next_month = 1;
					$next_year = $year + 1;
				} else {
					$next_month = 12;
					$next_year = $year;
				}
			} else {
				if($day >= 26 && $day <= 31){
					$next_month = $month + 1;
					$next_year = $year;
				} else {
					$next_month = $month;
					$next_year = $year;
				}
			}
			$filter_date_end = sprintf("%04d-%02d-%02d", $next_year, $next_month, '25');
			$to = date('Y-m-d');
			$filter_date_end = date('Y-m-d', strtotime($to . "-1 day"));
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = $this->request->get['filter_unit'];
		} else {
			$filter_unit = 0;
		}

		if(isset($this->session->data['dept_names'])){
			$filter_departments = html_entity_decode($this->session->data['dept_names']);
			$filter_departments = "'" . str_replace(",", "','", html_entity_decode($filter_departments)) . "'";
		} else {
			$filter_departments = '';
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = html_entity_decode($this->request->get['filter_department']);
		} elseif(isset($this->session->data['dept_name']) && !isset($this->session->data['dept_names'])){
			$filter_department = $this->session->data['dept_name'];
		} else {
			$filter_department = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'filter_unit'			 => $filter_unit,
			'filter_department'		 => $filter_department,
			'filter_departments'     => $filter_departments,
			'filter_dol'             => 1,
		);

		//$start_time = strtotime($filter_date_start);
		//$compare_time = strtotime(date('Y-m-d'));

		$day = array();
        $days = $this->GetDays($filter_date_start, $filter_date_end);
        foreach ($days as $dkey => $dvalue) {
        	$dates = explode('-', $dvalue);
        	$day[$dkey]['day'] = $dates[2];
        	$day[$dkey]['date'] = $dvalue;
        }

        //foreach ($day as $dkey => $dvalue) {
        	//$results = $this->model_report_common_report->getAttendance($data);
        //}
        $final_datas = array();
		$current_date_exp = explode('-', $filter_date_start);
        $month = $current_date_exp[1];
		$year = $current_date_exp[0];
		$total_month_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);


        $results1 = $this->model_report_common_report->getemployees($data);
        $results2 = $this->model_report_common_report->getemployees_reporting($data);
        $results = array_merge($results1, $results2);
        foreach ($results as $rkey => $rvalue) {
			$attendance1_data = array();
			$data['filter_date_start'] = $filter_date_start;
			$data['filter_date_end'] = $filter_date_end;
			if($rvalue['doj'] != '0000-00-00' && strtotime($rvalue['doj']) > strtotime($data['filter_date_start'])){
				$data['filter_date_start'] = $rvalue['doj'];
			}
			if($rvalue['dol'] != '0000-00-00' && strtotime($rvalue['dol']) < strtotime($data['filter_date_end'])){
				$dol = date("Y-m-d", strtotime("-1 day", strtotime($rvalue['dol'])));
				$data['filter_date_end'] = $dol;
			}
			$total_month_dayss = $this->GetDays($data['filter_date_start'], $data['filter_date_end']);
			$total_month_days = 0;
			foreach($total_month_dayss as $tkeys => $tvalue){
				$total_month_days ++;
			}
			$transaction_datas = $this->model_report_common_report->gettransaction_data($rvalue['emp_code'], $data);
			$emp_names = $this->db->query("SELECT CONCAT_WS(' ',first_name,last_name) AS emp_name FROM `oc_employee` WHERE `emp_code` = '".$rvalue['emp_code']."' ");
			$emp_name = '';
			if($emp_names->num_rows > 0){
				$emp_name = $emp_names->row['emp_name'];
			}

			$final_datas[$rvalue['emp_code']]['name'] = $emp_name;
			$final_datas[$rvalue['emp_code']]['emp_code'] = $rvalue['emp_code'];
			$transaction_inn = '0';
			$working_hours_array = array();
			$actual_working_hours_array = array();
			$late_early_count = 0;
			$total_absent_count = 0;
			$total_absent_raw_count = 0;
			foreach($transaction_datas as $tkey => $tvalue){
				if($tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0' && $tvalue['working_time'] != '00:00:00'){
					if($tvalue['after_shift'] == 1){
						$tvalue['act_outtime'] = $tvalue['shift_outtime_flexi'];
					}
					$shift_intime_minus_one = Date('H:i:s', strtotime($tvalue['shift_intime'] .' -0 minutes'));
					$start_date = new DateTime($tvalue['date'].' '.$shift_intime_minus_one);
					$since_start = $start_date->diff(new DateTime($tvalue['date'].' '.$tvalue['act_intime']));
					if($since_start->h > 12){
						$start_date = new DateTime($tvalue['date'].' '.$shift_intime_minus_one);
						$since_start = $start_date->diff(new DateTime($tvalue['date_out'].' '.$tvalue['act_intime']));
					}
					$shift_early = 0;
					if($since_start->invert == 1){
						$shift_early = 1;
					}
					if($shift_early == 1){
						$shift_intime_minus_one = Date('H:i:s', strtotime($tvalue['shift_intime'] .' -0 minutes'));
						if(strtotime($tvalue['act_outtime']) < strtotime($shift_intime_minus_one)){
							$start_date = new DateTime($tvalue['date'].' '.$tvalue['act_intime']);
						} else {
							$start_date = new DateTime($tvalue['date'].' '.$shift_intime_minus_one);
						}
					} else {
						$start_date = new DateTime($tvalue['date'].' '.$tvalue['act_intime']);
					}
					$since_start = $start_date->diff(new DateTime($tvalue['date'].' '.$tvalue['act_outtime']));
					$working_hour = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
					$tvalue['working_time'] = $working_hour;
					$working_hours_array[$tvalue['date']] = $tvalue['working_time'];				
				} else {
					// if($tvalue['date'] == date('Y-m-d') && $tvalue['act_intime'] != '00:00:00'){
					// 	$current_time = date('H:i:s');
					// 	$start_date = new DateTime($tvalue['date'].' '.$tvalue['act_intime']);
					// 	$since_start = $start_date->diff(new DateTime($tvalue['date_out'].' '.$current_time));
					// 	$working_hours_array[$tvalue['date']] = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);	
					// }
				}
				//if(($tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0') || ($tvalue['weekly_off'] != '0' && $tvalue['working_time'] != '00:00:00') || ($tvalue['holiday_id'] != '0' && $tvalue['working_time'] != '00:00:00') || ($tvalue['leave_status'] != '0' && $tvalue['working_time'] != '00:00:00') ){
				$current_date = date('Y-m-d');
				if($tvalue['date'] < $current_date && $tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0' && $tvalue['act_intime'] <> '00:00:00' && $tvalue['act_outtime'] <> '00:00:00'){
					$start_date = new DateTime($tvalue['date'].' '.$tvalue['shift_intime']);
					$since_start = $start_date->diff(new DateTime($tvalue['date_out'].' '.$tvalue['shift_outtime']));
					$shift_working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
					$actual_working_hours_array[$tvalue['date']] = $shift_working_time;	
				} else {
					if($tvalue['date'] >= $current_date && $tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0' && $tvalue['act_intime'] = '00:00:00' && $tvalue['act_outtime'] = '00:00:00'){
						$start_date = new DateTime($tvalue['date'].' '.$tvalue['shift_intime']);
						$since_start = $start_date->diff(new DateTime($tvalue['date_out'].' '.$tvalue['shift_outtime']));
						$shift_working_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
						$actual_working_hours_array[$tvalue['date']] = $shift_working_time;	
					}
				}
				if($tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0'){
					$late_early_count = $late_early_count + $tvalue['late_mark'];
					$late_early_count = $late_early_count + $tvalue['early_mark'];
				}
				
				if($tvalue['date'] < $current_date){
					if($tvalue['absent_status'] == 1 && $tvalue['working_time'] == '00:00:00' && $tvalue['weekly_off'] == '0' && $tvalue['holiday_id'] == '0' && $tvalue['leave_status'] == '0' && $tvalue['tour_id'] == '0'){
						$total_absent_count = $total_absent_count + 1;
						$total_absent_raw_count = $total_absent_raw_count + 1;
					} elseif($tvalue['absent_status'] == 0.5){
						//$total_absent_count = $total_absent_count + 0.5;
					}
				}
			}
			//echo $total_absent_count;exit;

			$time = 0;
			$hours = 0;
			$minutes = 0;
			foreach ($working_hours_array as $time_val) {
				$times = explode(':', $time_val);
				$hours += $times[0];
				$minutes += $times[1];
			}
			$min_min = 0;
			$min_hours = 0;
			if($minutes > 0){
				$min_hours = floor($minutes / 60);
    			$min_min = ($minutes % 60);
    			$min_min = sprintf('%02d', $min_min);
    		}
    		$total_working_hours = sprintf('%02d', ($hours + $min_hours)).'.'.sprintf('%02d', $min_min);
			$time = 0;
			$hours = 0;
			$minutes = 0;
			foreach ($actual_working_hours_array as $time_val) {
				$times = explode(':', $time_val);
				$hours += $times[0];
				$minutes += $times[1];
			}
			$min_min = 0;
			$min_hours = 0;
			if($minutes > 0){
				$min_hours = floor($minutes / 60);
    			$min_min = ($minutes % 60);
    			$min_min = sprintf('%02d', $min_min);
    		}
    		$total_actual_working_hours = sprintf('%02d', ($hours + $min_hours)).'.'.sprintf('%02d', $min_min);
			if($total_actual_working_hours > $total_working_hours){
				$extra_hours = 0;
				//$difference_hours = $total_actual_working_hours - $total_working_hours;
				$total_actual_working_hours_min = $this->hoursToMinutes($total_actual_working_hours);
				$total_working_hours_min = $this->hoursToMinutes($total_working_hours);
				$difference_minutes = $total_actual_working_hours_min - $total_working_hours_min;
				$min_hours = floor($difference_minutes / 60);
				$min_min = ($difference_minutes % 60);
				$difference_hours = $min_hours.'.'.$min_min; 
			} else {
				$difference_hours = 0;
				//$extra_hours = $total_working_hours - $total_actual_working_hours;
				$total_actual_working_hours_min = $this->hoursToMinutes($total_actual_working_hours);
				$total_working_hours_min = $this->hoursToMinutes($total_working_hours);
				$difference_minutes = $total_working_hours_min - $total_actual_working_hours_min;
				$min_hours = floor($difference_minutes / 60);
				$min_min = ($difference_minutes % 60);
				$extra_hours = $min_hours.'.'.$min_min; 
			}
			$total_bal = $this->model_transaction_transaction->gettotal_bal_ess($rvalue['emp_code']);
			$total_previous_bal = $this->model_transaction_transaction->getprevious_balances($rvalue['emp_code'], $filter_date_start, $filter_date_end, $rvalue, 'PL');
			$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro_ess_date($rvalue['emp_code'], 'PL', $filter_date_start, $filter_date_end);
			$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1_ess_date($rvalue['emp_code'], 'PL', $filter_date_start, $filter_date_end);
			//$difference_hours = '112.47';
			$deduction_days_rounded = floor($difference_hours / 9.00);
			$deduction_days = round($difference_hours / 9.00, 2);
			$deduction_days = sprintf("%.2f", $deduction_days);
			$deduction_days_exp = explode('.', $deduction_days);
			if(isset($deduction_days_exp[1])){
				if($deduction_days_exp[1] == '00' || $deduction_days_exp[1] == 00){
					$deduction_days_exp[1] = 0;
				}
				if($deduction_days_exp[1] > 0 && $deduction_days_exp[1] <= 50){
					$deduction_days_rounded = $deduction_days_rounded + 0.5; 
				} else {
					if($deduction_days_exp[1] != 0){
						$deduction_days_rounded = $deduction_days_rounded + 1;
					}
				}
			}
			// echo $difference_hours;
			// echo '<br />';
			// echo $deduction_days;
			// echo '<br />';
			// echo '<pre>';
			// print_r($deduction_days_exp);
			// echo '<br />';
			// echo $deduction_days_rounded;
			// echo '<br />';
			// exit;
			//$late_early_count = 9;
			$late_early_days_rounded = floor($late_early_count / 6);
			$late_early_days = round($late_early_count / 6, 2);
			$late_early_days = sprintf("%.2f", $late_early_days);
			$late_early_days_exp = explode('.', $late_early_days);
			if(isset($late_early_days_exp[1])){
				if($late_early_days_exp[1] == '00' || $late_early_days_exp[1] == 00){
					$late_early_days_exp[1] = 0;
				}
				if($late_early_days_exp[1] >= 50){
					$late_early_days_rounded = $late_early_days_rounded + 0.5; 
				}
			}
			// echo $late_early_count;
			// echo '<br />';
			// echo $late_early_days;
			// echo '<br />';
			// echo '<pre>';
			// print_r($late_early_days_exp);
			// echo '<br />';
			// echo $late_early_days_rounded;
			// echo '<br />';
			// exit;
			$leave_minus_amount = 0;
			$total_leaves_count = 0;
			$total_leave_opening = 0;
			$total_leave_balance = 0;
			$total_absent_days = 0;
			if(isset($total_bal['pl_acc'])){
				if(!isset($total_bal_pro['bal_p'])){
					$total_bal_pro['bal_p'] = 0;
				}
				if(!isset($total_bal_pro['encash'])){
					$total_bal_pro['encash'] = 0;
				}
				if(!isset($total_bal_pro1['leave_amount'])){
					$total_bal_pro1['leave_amount'] = 0;
				}	
				$total_leave_opening = $total_previous_bal;
				$total_leave_balance = $total_leave_opening - ($total_bal_pro['bal_p'] + $total_bal_pro['encash'] + $total_bal_pro1['leave_amount']) - ($late_early_days_rounded + $deduction_days_rounded);
				if($total_leave_balance < 0){
					$total_absent_days += abs($total_leave_balance);
					$total_leave_balance = 0;
				}
				$total_leaves_count = $total_bal_pro['bal_p'] + $total_bal_pro['encash'] + $total_bal_pro1['leave_amount'];
			}
			$total_absent_days += $total_absent_count;

			//echo "SELECT `punch_time` FROM `oc_attendance_new` WHERE `emp_id` = '".$result['emp_code']."' AND `punch_date` = '".date('Y-m-d')."' ";exit;
			$todays_intime_datas = $this->db->query("SELECT `punch_time` FROM `oc_attendance_new` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `punch_date` = '".date('Y-m-d')."' ");
			$todays_in_time = '00:00:00';
			$todays_working_hours = '00:00:00';
			if($todays_intime_datas->num_rows > 0){
				$todays_intime_data = $todays_intime_datas->row;
				$todays_in_time = $todays_intime_data['punch_time'];
				$todays_out_time = date('H:i:s');
				$current_date = date('Y-m-d');
				$start_date = new DateTime($current_date.' '.$todays_in_time);
				$since_start = $start_date->diff(new DateTime($current_date.' '.$todays_out_time));
				$todays_working_hours = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);
			}

			$total_utitilized_plus_absent = $total_absent_raw_count + $deduction_days_rounded + $late_early_days_rounded + $total_leaves_count;
			// echo $total_actual_working_hours;
			// echo '<br />';
			// echo $total_working_hours;
			// echo '<br />';
			// echo $difference_hours;
			// echo '<br />';
			// echo $extra_hours;
			// echo '<br />';
			// echo $total_bal['pl_acc'];
			// echo '<br />';
			// echo $total_bal_pro['bal_p'];
			// echo '<br />';
			// echo $total_bal_pro['encash'];
			// echo '<br />';
			// echo $total_bal_pro1['leave_amount'];
			// echo '<br />';
			// echo $late_early_days_rounded;
			// echo '<br />';
			// echo $deduction_days_rounded;
			// echo '<br />';
			// echo $total_bal_pl;
			// echo '<br />';
			// echo $todays_in_time;
			// echo '<br />';
			// echo $todays_working_hours;
			// echo '<br />';
			// exit;
			$final_datas[$rvalue['emp_code']]['total_working_hours'] = $total_actual_working_hours;
			$final_datas[$rvalue['emp_code']]['total_actual_working_hours'] = $total_working_hours;
			$final_datas[$rvalue['emp_code']]['total_pending_hours'] = $difference_hours;
			$final_datas[$rvalue['emp_code']]['total_extra_hours'] = $extra_hours;
			$final_datas[$rvalue['emp_code']]['total_deduction_days'] = $deduction_days_rounded;
			$final_datas[$rvalue['emp_code']]['total_late_early_count'] = $late_early_count;
			$final_datas[$rvalue['emp_code']]['total_late_early_days'] = $late_early_days_rounded;
			$final_datas[$rvalue['emp_code']]['total_leaves_count'] = $total_leaves_count;
			$final_datas[$rvalue['emp_code']]['total_deduction'] = $deduction_days_rounded + $late_early_days_rounded + $total_leaves_count;
			$final_datas[$rvalue['emp_code']]['total_absent_raw_count'] = $total_absent_raw_count;
			$final_datas[$rvalue['emp_code']]['total_utitilized_plus_absent'] = $total_utitilized_plus_absent;
			$final_datas[$rvalue['emp_code']]['total_leave_opening'] = $total_leave_opening;
			$final_datas[$rvalue['emp_code']]['total_leave_balance'] = $total_leave_balance;
			$final_datas[$rvalue['emp_code']]['total_absent_days'] = $total_absent_days;
			$final_datas[$rvalue['emp_code']]['total_month_days'] = $total_month_days - $total_absent_days;
		}
		

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		$url .= '&once=1';

		$final_datas = array_chunk($final_datas, 7000);

		// echo '<pre>';
		// print_r($final_datas);
		// exit;
		
		if($final_datas){
			if($filter_department){
				$department_names = $this->db->query("SELECT `d_name` FROM `oc_department` WHERE `department_id` = '".$this->request->get['filter_department']."' ");
				if($department_names->num_rows > 0){
					$department_name = $department_names->row['d_name'];
				} else {
					$department_name = '';
				}
				$department_name = html_entity_decode($department_name);
				$department = $department_name;
			} else {
				$department = 'All';
			}

			if($filter_unit){
				$unit_names = $this->db->query("SELECT `unit` FROM `oc_unit` WHERE `unit_id` = '".$this->request->get['filter_unit']."' ");
				if($unit_names->num_rows > 0){
					$unit_name = $unit_names->row['unit'];
				} else {
					$unit_name = '';
				}
				$unit_name = html_entity_decode($unit_name);
				$unit = $unit_name;
			} else {
				$unit = 'All';
			}

			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$template = new Template();		
			$template->data['final_datass'] = $final_datas;
			$template->data['date_start'] = $filter_date_start;
			$template->data['date_end'] = $filter_date_end;
			$template->data['filter_department'] = $department;
			$template->data['filter_unit'] = $unit;
			$template->data['title'] = 'Attendance Report';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/attendance1_html.tpl');
			//echo $html;exit;
			$filename = "Attendance_Report";
			
			//if($filter_type == 6){
				// header('Content-type: text/html');
				// header('Content-Disposition: attachment; filename='.$filename.".html");
				// echo $html;
			// } else {
				header("Content-Type: application/vnd.ms-excel; charset=utf-8");
				header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Cache-Control: private",false);
				echo $html;
				exit;
			// }		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/attendance1', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			if(isset($this->session->data['dept_names'])){
				$filter_departments = html_entity_decode($this->session->data['dept_names']);
				$filter_departments = "'" . str_replace(",", "','", html_entity_decode($filter_departments)) . "'";
			} else {
				$filter_departments = '';
			}

			if($filter_departments == ''){
				if(isset($this->session->data['dept_name'])){
					$filter_department = $this->session->data['dept_name'];
				} else {
					$filter_department = '';
				}
			} else {
				$filter_department = '';
			}

			if(isset($this->session->data['emp_code']) && !$this->user->isAdmin()){
				$filter_reporting_to = $this->session->data['emp_code'];
			} else {
				$filter_reporting_to = '';
			}

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_department' => $filter_department,
				'filter_departments' => $filter_departments,
				'filter_reporting_to' => $filter_reporting_to,
				'filter_date_start' => $this->request->get['filter_date_start'],
				'filter_dol' => 1,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['first_name'].' '.$result['last_name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>