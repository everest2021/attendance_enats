ALTER TABLE `oc_employee` ADD `send_to_device` INT(11) NOT NULL DEFAULT '0' AFTER `added_date`;

ALTER TABLE `oc_employee` ADD `first_finger` INT(11) NOT NULL DEFAULT '0' AFTER `send_to_device`, ADD `six_finger` INT(11) NOT NULL DEFAULT '0' AFTER `first_finger`;