<?php
// Heading
$_['heading_title']     = 'Cancel Bill';

$_['button_filter'] = 'Filter';

$_['text_cancel'] = 'Cancel Bill';
$_['text_active'] = 'Activate Bill';

$_['entry_bill_id'] = 'Bill Id';
// Column
$_['column_sr_no']     = 'Sr.No';
$_['column_bill_no']     = 'Bill Id';
$_['column_horse_name']     = 'Horse Name';
$_['column_owner_name']     = 'Owner Name';
$_['column_trainer_name']   = 'Trainer Name';
$_['column_total']     = 'Total';
$_['column_action']     = 'Action';

?>