<?php
// Heading
$_['heading_title']     = 'Print Receipt';

$_['button_filter'] = 'Print';
$_['button_list'] = 'Get Bills';

$_['text_print'] = 'Print';
// Column
$_['column_name'] = 'Horse Name';
$_['column_month']   = 'Month';
$_['column_year']     = 'Year';

$_['column_sr_no']     = 'Sr.No';
$_['column_bill_no']     = 'Bill Id';
$_['column_horse_name']     = 'Horse Name';
$_['column_owner_name']     = 'Owner Name';
$_['column_trainer_name']   = 'Trainer Name';
$_['column_total']     = 'Total';
$_['column_action']     = 'Action';

$_['entry_name'] = 'Horse Name';
$_['entry_trainer'] = 'Trainer Name';
$_['entry_month']   = 'Month';
$_['entry_year']     = 'Year';
$_['entry_doctor'] = 'Doctor';
$_['entry_transaction_type'] = 'Transaction type';

?>