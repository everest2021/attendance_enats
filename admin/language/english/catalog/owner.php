<?php
// Heading
$_['heading_title']      = 'Owner';

// Text
$_['text_success']       = 'Success: You have modified owner!';

// Column
$_['column_name']        = 'Owner Name';
$_['column_action']      = 'Action';

$_['button_filter']      = 'Filter';

// Entry
$_['entry_name']         = 'Owner Name:';
$_['entry_transaction_type'] = 'Clinic';

$_['text_delete']        = 'Delete';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify owner!';
$_['error_name']         = 'Owner Name must be between 3 and 64 characters!';
$_['error_horse']      = 'Warning: This owner cannot be deleted as it is currently assigned to %s horses!';
?>
