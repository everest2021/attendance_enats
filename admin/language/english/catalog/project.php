<?php
// Heading
$_['heading_title']          = 'Project';


// Text
$_['text_success']           = 'Success: You have modified Project!';
$_['text_list']              = 'Project List';
$_['text_add']               = 'Add Project';
$_['text_edit']              = 'Edit Project';
$_['text_default']           = 'Default';

// Column
$_['column_name']             = '  Name';
$_['column_description']       = '  Description';
$_['column_address']           = '  Address';
$_['column_latitude']       = ' Latitude ';
$_['column_longitude']		= 'Longitude';

$_['column_sort_order']        = 'Sort Order';
$_['column_action']            = 'Action';

// Entry
$_['entry_name']              = 'Name';
$_['entry_description']       = 'Description';
$_['entry_address'] 	      = 'Address';
$_['entry_latitude']       = 'Latitude';
$_['entry_longitude']      = 'Longitude';

$_['entry_top']              = 'Top';
$_['entry_column']           = 'Columns';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_status']           = 'Status';
$_['entry_layout']           = 'Layout Override';

// Help
$_['help_filter']            = '(Autocomplete)';
$_['help_keyword']           = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';
$_['help_top']               = 'Display in the top menu bar. Only works for the top parent Master.';
$_['help_column']            = 'Number of columns to use for the bottom 3 Project.';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify Project!';
$_['error_type']             = ' Type must be between 2 and 255 characters!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_keyword']          = 'SEO URL already in use!';
$_['error_name']             = 'Please Enter  Name!';
$_['error_description']      = ' Please Enter Discription!';
$_['error_address']          = ' Please enter Address!';
$_['error_coordinates']      = ' Please enter Coordinates!';

