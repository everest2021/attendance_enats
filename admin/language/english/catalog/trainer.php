<?php
// Heading
$_['heading_title']      = 'Trainer';

// Text
$_['text_success']       = 'Success: You have modified trainer!';

// Column
$_['column_name']        = 'Trainer Name';
$_['column_action']      = 'Action';

$_['button_filter']      = 'Filter';

// Entry
$_['entry_name']         = 'Trainer Name:';

$_['text_delete']        = 'Delete';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify Trainer!';
$_['error_name']         = 'Trainer Name must be between 3 and 64 characters!';
$_['error_horse']      = 'Warning: This trainer cannot be deleted as it is currently assigned to %s horses!';
?>
