<?php
// Heading
$_['heading_title']     = 'Project Assignment';

// Text
$_['text_success']      = 'Success: You have modified Project Assignment!';

// Column
$_['column_name']       = 'Project Name';
$_['column_start_date']       = 'Start Date';
$_['column_end_date']       = 'End Date';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']        = 'Name:';
$_['entry_start_date']        = 'Start Date:';
$_['entry_end_date']       = 'End Date';
$_['entry_access']      = 'Employee  List:';
$_['entry_modify']      = 'Modify Permission:';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify user groups!';
$_['error_name']        = 'User Group Name must be between 3 and 64 characters!';
$_['error_user']        = 'Warning: This user group cannot be deleted as it is currently assigned to %s users!';
?>
