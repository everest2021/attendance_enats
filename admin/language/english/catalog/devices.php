<?php
// Heading
$_['heading_title']     = 'Devices';

// Text
$_['text_success']      = 'Success: You have modified Devices!';

// Column
$_['column_device_serial_no']   = 'Device Serial No';
$_['column_location']     = 'Location';
$_['column_type'] = 'Type';
$_['column_action']     = 'Action';
$_['column_time_zone']     = 'Time Zone';
$_['column_status']     = 'Status';

// Entry
$_['entry_device_serial_no']   = 'Device Serial No:';
$_['entry_location']   = 'Location:';
$_['entry_type']    = 'Type:';
$_['entry_time_zone']    = 'Time Zone:';
$_['entry_status']     = 'Status:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify users!';
$_['error_account']    = 'Warning: You can not delete your own account!';
$_['error_exists']     = 'Warning: Username is already in use!';
$_['error_name']   = 'name must be between 3 and 20 characters!';
$_['error_device_serial_no']   = 'Device serial number must be between 1 and 20 characters!';
$_['error_location']    = 'Password and password confirmation do not match!';
$_['error_type']  = 'First Name must be between 1 and 32 characters!';

?>
