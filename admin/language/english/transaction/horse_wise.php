<?php
// Heading
$_['heading_title']      = 'Horse Wise treatment entry';

// Text
$_['text_success']       = 'Success: You have inserted a Entry!';

// Entry
$_['entry_select_horse_wise'] = 'Select horse:';
$_['entry_dot']       = 'Date:';
$_['entry_add_medicine'] = 'Add Treatment';
$_['entry_remove']       = 'Remove';
$_['entry_treatment']       = 'Treatment';
$_['entry_price']       = 'Price';
$_['entry_total']       = 'Total';
$_['entry_quantity']       = 'Quantity';
$_['entry_action']       = 'Action';
$_['entry_doctor']       = 'Doctor';
$_['entry_transaction_type'] = 'Transaction Type';


// Error
$_['error_permission']   = 'Warning: You do not have permission to modify Horse Wise Treatment Entry!';
$_['error_name']         = 'Horse Name must be between 1 and 255 characters!';
$_['error_name_valid']  = 'Please Enter Valid Horse Name!';
$_['error_medicine_name']  = 'Please Enter Medicine Name!';
$_['error_dot']      	 = 'Please Enter valid date!';
$_['error_medicine_exist']  = 'Please Enter Valid Medicine Name!';
$_['error_price']      	 = 'Please Enter Price!';
$_['error_total']      	 = 'Total Cannot be Empty!';
$_['error_medicine_quantity']  = 'Please Enter Quantity Greater then 0!';

?>
