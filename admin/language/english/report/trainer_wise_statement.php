<?php
// Heading
$_['heading_title']     = 'Trainer Wise Statement';

// Text

// Column
$_['column_horse_name']    = 'Horse Name';
$_['column_invoice_no']    = 'Invoice No';
$_['column_owner_name']    = 'Owner Name';
$_['column_amount']        = 'Amount';
$_['column_trainer_name']  = 'Trainer Name';
$_['column_subtotal']      = 'Sub Total';
$_['column_total']         = 'Total';

$_['button_export'] = 'Export';
$_['button_filter'] = 'Search';
$_['text_all'] = 'All';



// Entry
$_['entry_date_start']  = 'Date From:';
$_['entry_date_end']    = 'Date TO:';
$_['entry_name']       = 'Horse Name:';
$_['entry_doctor']     = 'Doctor';
$_['entry_transaction_type'] = 'Transaction type';
?>