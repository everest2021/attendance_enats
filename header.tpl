<?php

if(isset($this->request->get['route'])){
	$current_location = explode("/", $this->request->get['route']);
	if($current_location[0] == "common"){
		$is_homepage = TRUE;
	}else{
		$is_homepage = FALSE;
	}
}else{
	$is_homepage = FALSE;
}

$get_url = explode("&", $_SERVER['QUERY_STRING']);

$get_route = substr($get_url[0], 6);

$get_route = explode("/", $get_route);

$page_name = array("shoppica2","journal_banner","journal_bgslider","journal_cp","journal_filter","journal_gallery","journal_menu","journal_product_slider","journal_product_tabs","journal_rev_slider","journal_slider");

// array_push($page_name, "EDIT-ME");

if(array_intersect($page_name, $get_route)){
	$is_custom_page = TRUE;
}else{
	$is_custom_page = FALSE;
}

?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" xml:lang="<?php echo $lang; ?>">
<head>
	<meta charset="utf-8" />
	<title><?php echo $title; ?></title>
	<base href="<?php echo $base; ?>" />
	<?php if ($description) { ?>
	<meta name="description" content="<?php echo $description; ?>" />
	<?php } ?>
	<?php if ($keywords) { ?>
	<meta name="keywords" content="<?php echo $keywords; ?>" />
	<?php } ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<?php foreach ($links as $link) { ?>
	<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
	<?php } ?>

	<!-- Le styles -->
	<?php if(isset($this->request->get['route']) && $this->request->get['route'] == 'bill/bill_history/configuremail') { ?>
		<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
	<?php } ?>
	<?php if(!$is_custom_page){ ?>
	<link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/bootstrap.css" rel="stylesheet" />
	<?php
	  $base_url = DIR_APPLICATION; 
	  $file = 'view/stylesheet/admin_theme/base5builder_impulsepro/style.css';
	  $mtime = filemtime(DIR_APPLICATION . $file);
	  $file = $file.'?version='.$mtime;
  	?>
	<link type="text/css" href="<?php echo $file; ?>" rel="stylesheet" />
	
	<link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/bootstrap-responsive.css" rel="stylesheet" />
	<link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/style-responsive.css" rel="stylesheet" />
	<?php }else{ ?>
	<link rel="stylesheet" type="text/css" href="view/stylesheet/stylesheet.css" />
	<link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/style-custom-page.css" rel="stylesheet" />
	<?php
}
?>

<?php /*  ?>
<link type="text/css" href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' />
<?php */  ?>

<link type="text/css" href="view/javascript/admin_theme/base5builder_impulsepro/ui/themes/ui-lightness/jquery-ui-1.8.20.custom-min.css" rel="stylesheet" />
	  <!--[if IE 7]>
	  <link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/style-ie7.css" rel="stylesheet">
	  <![endif]-->
	  <!--[if IE 8]>
	  <link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/style-ie8.css" rel="stylesheet">
	  <![endif]-->
	  <script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/jquery.js"></script>
	  <script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/ui/jquery-ui-1.8.20.custom.min.js"></script>
	  <script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/tabs.js"></script>
	  <script type="text/javascript" src="view/javascript/jquery/ui/external/jquery.cookie.js"></script>
	  <?php foreach ($styles as $style) { ?>
	  <link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
	  <?php } ?>
	  <?php foreach ($scripts as $script) { ?>
	  <script type="text/javascript" src="<?php echo $script; ?>"></script>
	  <?php } ?>
	  <?php if($this->user->getUserName() && $is_homepage){ ?>
	  <script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/flot/jquery.flot.min.js"></script>
	  <script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/flot/jquery.flot.pie.min.js"></script>
	  <script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/flot/curvedLines.min.js"></script>
	  <script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/flot/jquery.flot.tooltip.min.js"></script>
	  <script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/modernizr.js"></script>

		<!--[if lte IE 8]>
		<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/excanvas.min.js"></script>
		<![endif]-->
		<?php } ?>
		<script type="text/javascript">
		$(document).ready(function(){

		// Signin - Button

		$(".form-signin-body-right input").click(function(){
			$(".form-signin").submit();
		});

		// Signin - Enter Key

		$('.form-signin input').keydown(function(e) {
			if (e.keyCode == 13) {
				$('.form-signin').submit();
			}
		});

		// Confirm Delete
		$('#form').submit(function(){
			if ($(this).attr('action').indexOf('delete',1) != -1) {
				if (!confirm('<?php echo $text_confirm; ?>')) {
					return false;
				}
			}
		});

		// Confirm Uninstall
		$('a').click(function(){
			if ($(this).attr('href') != null && $(this).attr('href').indexOf('uninstall', 1) != -1) {
				if (!confirm('<?php echo $text_confirm; ?>')) {
					return false;
				}
			}
		});
	});
		</script>

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/html5shiv.js"></script>
  <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="shortcut icon" href="view/image/admin_theme/base5builder_impulsepro/favicon.png" />
</head>

<body>
	<div class="container-fluid">
		<?php if ($logged) { ?>
			<div id="left-column">
				<?php /* ?>
				<div class="sidebar-logo" style="margin-left:10px !important; margin-top:-20px;display: none;">
					<a href="<?php echo $home; ?>">
						<img src="view/image/admin_theme/base5builder_impulsepro/logo_new.png" style="width:110px; height:75px;"/>
					</a>
				</div>
				<?php */ ?>
				<div id="mainnav">
					<ul class="mainnav">
						<li id="menu-control">
							<div class="menu-control-outer">
								<div class="menu-control-inner">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</div>
							</div>
						</li>
						
						<?php if($user_dept == 0){ ?>
							<li id="dashboard"><a href="<?php echo $home; ?>" class="top"><?php echo $text_dashboard; ?></a></li>
						<?php } ?>
						
						<?php if($user_dept == 0){ ?>
						<li id="catalog"><a class="top"><?php echo $text_master; ?></a>
							<ul>

								<li><a href="<?php echo $employee; ?>"><?php echo "Employee"; ?></a></li>
								<?php ?>
								<li><a href="<?php echo $department; ?>" style="display:none;"><?php echo "Department"; ?></a></li>
								<li><a href="<?php echo $designation; ?>" style="display:none;"><?php echo "Designation"; ?></a></li>
								<li style="display:none;"><a href="<?php echo $shift; ?>"><?php echo "Shift"; ?></a></li>
								<li><a href="<?php echo $holiday; ?>"><?php echo "Holiday"; ?></a></li>
								<li><a href="<?php echo $project; ?>"><?php echo "Project"; ?></a></li>
								<li><a href="<?php echo $expence; ?>"><?php echo "Expense"; ?></a></li>
								<li><a href="<?php echo $unit; ?>"  style="display:none;"><?php echo "Unit"; ?></a></li>

								<li style="display:none;" id="extension" style="" ><a class="top"><?php echo "Leave"; ?></a>
							<ul>
								<?php if($user_dept == 0){ ?>
									<li><a href="<?php echo $leave; ?>"><?php echo "Leave Master"; ?></a></li>
								<?php } ?>

								<?php if($this->user->isAdmin()){ ?>
									<li><a href="<?php echo $leave_transaction_1; ?>"><?php echo 'Admin Leave Approval'; ?></a></li>
								<?php } ?>

								<?php if($is_dept == 1 || $is_super == 1 || $this->user->isAdmin()){ ?>
									<li><a href="<?php echo $leave_transaction; ?>"><?php echo 'Self Leave Entry'; ?></a></li>
								<?php } else { ?>
									<li><a href="<?php echo $leave_transaction; ?>"><?php echo 'Leave Entry'; ?></a></li>
								<?php } ?>
								
								<?php if($is_dept == 1 || $is_super == 1 || $is_super1 == 1){ ?>
									<?php if($this->user->getId() != 193){ ?>
										<li><a href="<?php echo $leave_transaction_dept; ?>"><?php echo 'Dept Leave Approval'; ?></a></li>
									<?php } ?>
								<?php } else { ?>
									<?php if(isset($leave_transaction_super) && $this->user->getId() != 193) { ?>
										<li><a href="<?php echo $leave_transaction_super; ?>"><?php echo 'Dept Leave Approval'; ?></a></li>
									<?php } ?>
								<?php } ?>

								<?php if($user_dept == 0){ ?>
									<li><a href="<?php echo $leave_process; ?>"><?php echo 'Leave Processing'; ?></a></li>
								<?php } ?>
								<li><a href="<?php echo $leave_led; ?>"><?php echo 'Leave Ledger'; ?></a></li>
							</ul>
						</li>
						
							
							</ul>
						</li>
						<?php } ?>
						<?php ?>	
						<?php if($user_dept == 0){ ?>
						<li id="extension"><a class="top"><?php echo $text_transaction; ?></a>
							<ul> 
								<li><a href="<?php echo $project_assignment; ?>"><?php echo 'Project Assignment'; ?></a></li>
								<li><a href="<?php echo $expense_transaction; ?>"><?php echo 'Expense Transaction'; ?></a></li>
								<li style="display:none;"><a href="<?php echo $leave_entry; ?>"><?php echo 'Leave Entry'; ?></a></li>
								<li style="display:none;"><a href="<?php echo $manualpunch; ?>"><?php echo 'Manual Punch'; ?></a></li>
						
							</ul>
						</li>
						<?php } ?>
						<?php  ?>
						<?php /* ?>
						<?php if($user_dept == 0){ ?>
						<li id="extension"><a class="top"><?php echo "Reports"; ?></a>
							<ul>
								<li><a href="<?php echo $today_report; ?>"><?php echo "Live Map Attendance"; ?></a></li>
								<li><a href="<?php echo $attendance_report; ?>"><?php echo 'Daily Attendance'; ?></a></li>
								<li><a href="<?php echo $performance_report; ?>"><?php echo 'Periodic Attendance'; ?></a></li>
								<li><a href="<?php echo $project_report; ?>"><?php echo 'Project Wise Report'; ?></a></li>
								<li><a href="<?php echo $expense_ledger; ?>"><?php echo 'Expense Ledger'; ?></a></li>
								<li><a href="<?php echo $leave_ledger; ?>"><?php echo 'Leave Ledger'; ?></a></li>
								<li style="display:none;" ><a href="<?php echo $attendance1; ?>"><?php echo 'Attendance Report'; ?></a></li>
							</ul>
						</li>
						<?php } ?>
						<?php ?>
						<?php if($user_dept == 1 || $is_super1 == '1'){ ?>
							<li id="extension"><a class="top"><?php echo "Attendance"; ?></a>
								<ul>
									<li><a href="<?php echo $attendance_report; ?>"><?php echo 'Daily Attendance'; ?></a></li>
									<li><a href="<?php echo $performance_report; ?>"><?php echo 'Periodic Attendance'; ?></a></li>
									<li><a href="<?php echo $attendance1; ?>"><?php echo 'Attendance Report'; ?></a></li>
								</ul>
							</li>
						<?php } ?>
						<?php  ?>
						<li id="extension" style="display:none;" ><a class="top"><?php echo "Leave"; ?></a>
							<ul>
								<?php if($user_dept == 0){ ?>
									<li><a href="<?php echo $leave; ?>"><?php echo "Leave Master"; ?></a></li>
								<?php } ?>

								<?php if($this->user->isAdmin()){ ?>
									<li><a href="<?php echo $leave_transaction_1; ?>"><?php echo 'Admin Leave Approval'; ?></a></li>
								<?php } ?>

								<?php if($is_dept == 1 || $is_super == 1 || $this->user->isAdmin()){ ?>
									<li><a href="<?php echo $leave_transaction; ?>"><?php echo 'Self Leave Entry'; ?></a></li>
								<?php } else { ?>
									<li><a href="<?php echo $leave_transaction; ?>"><?php echo 'Leave Entry'; ?></a></li>
								<?php } ?>
								
								<?php if($is_dept == 1 || $is_super == 1 || $is_super1 == 1){ ?>
									<?php if($this->user->getId() != 193){ ?>
										<li><a href="<?php echo $leave_transaction_dept; ?>"><?php echo 'Dept Leave Approval'; ?></a></li>
									<?php } ?>
								<?php } else { ?>
									<?php if(isset($leave_transaction_super) && $this->user->getId() != 193) { ?>
										<li><a href="<?php echo $leave_transaction_super; ?>"><?php echo 'Dept Leave Approval'; ?></a></li>
									<?php } ?>
								<?php } ?>

								<?php if($user_dept == 0){ ?>
									<li><a href="<?php echo $leave_process; ?>"><?php echo 'Leave Processing'; ?></a></li>
								<?php } ?>
								<li><a href="<?php echo $leave_led; ?>"><?php echo 'Leave Ledger'; ?></a></li>
							</ul>
						</li>
						
						<?php if($this->user->isAdmin()){ ?>
							<li id="extension"><a class="top"><?php echo "Utility"; ?></a>
								<ul>
									<li style="display: none;"><a href="<?php echo $password_change; ?>"><?php echo 'Change Password'; ?></a></li>
									<?php if($this->user->isAdmin()){ ?>
										<li id="extension"><a target="_blank" href="<?php echo $user; ?>" class="top"><?php echo 'User'; ?></a></li>
										<li id="extension"><a href="<?php echo $backup; ?>" class="top"><?php echo 'Backup'; ?></a></li>
										<li id="extension"><a target="_blank" href="<?php echo $update_attendance; ?>" class="top" style="display:none;" ><?php echo 'Update New Attendance'; ?></a></li>
										<li id="extension"><a href="<?php echo $recalculate; ?>" class="top" style="display:none;" ><?php echo 'Recalculate Attendance'; ?></a></li>
									<?php } ?>
								</ul>
							</li>
						<?php } ?>
						
						<li id="extension"><a class="top" style="display:none"><?php echo "PX Record"; ?></a>
							<ul>
								<?php if($this->user->isAdmin()){ ?>
									<li><a href="<?php echo $manualpunch_ess_admin; ?>"><?php echo 'Admin PX Record Approval'; ?></a></li>
								<?php } ?>
								<?php if($is_dept == 1 || $is_super == 1 || $this->user->isAdmin()){ ?>
									<li><a href="<?php echo $manualpunch_ess; ?>"><?php echo 'Self PX Record Entry'; ?></a></li>
								<?php } else { ?>
									<li><a href="<?php echo $manualpunch_ess; ?>"><?php echo 'Self PX Record Entry'; ?></a></li>
								<?php } ?>
								<?php if(!$this->user->isAdmin()){ ?>
									<li><a href="<?php echo $manualpunch_ess_dept; ?>"><?php echo 'Dept PX Record Approval'; ?></a></li>
								<?php } ?>
							</ul>
						</li>
					
						<?php if($this->user->getId() == '99999999') { ?>
							<li id="extension"><a href="<?php echo $pass; ?>" class="top"  style="display:none;"><?php echo 'Change Password'; ?></a></li>
							<li id="extension"><a href="<?php echo $backup; ?>" class="top"  style="display:none;"><?php echo 'Backup'; ?></a></li>
							<li id="extension"><a target="_blank" href="<?php echo $update_attendance; ?>"  class="top" style="display:none"><?php echo 'Update New Attendance'; ?></a></li>
						<?php } ?>
						<?php */ ?>
						<?php //if($user_dept == 1 || $is_super1 == '1' || $this->user->isAdmin()){ ?>
					
					</ul>
				</div>
			</div>
			<div class="right-header-content clearfix">
				<div class="secondary-menu">
					<ul>
						<li id="logout"><a class="top" href="<?php echo $logout; ?>"><span><?php echo $text_logout; ?></span></a></li>
					</ul>
				</div>
				<div class="admin-info"><?php echo $logged; ?></div>
			</div>
<?php } ?>
<?php //} ?>